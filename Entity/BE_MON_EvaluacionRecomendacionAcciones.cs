﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public  class BE_MON_EvaluacionRecomendacionAcciones
    {
        public int id_evaluacionRecomendacion { get; set; }
        public int Tipo { get; set; }
        public int IdAccion { get; set; }
        public DateTime? Fecha { get; set; }
        public string Documento { get; set; }
        public string NombreDocumento { get; set; }
        public decimal Monto { get; set; }
        public bool Activo { get; set; }
         
    }
}
