﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_T_TipoDocumentoMiembroNE
    {
        private int _id_tipoDocumentoMiembroNE;
        private string _nombre;
        private bool _activo;


        public int id_tipoDocumentoMiembroNE
        {
            get { return _id_tipoDocumentoMiembroNE; }
            set { _id_tipoDocumentoMiembroNE = value; }
        }
        
        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        
        public bool activo
        {
            get { return _activo; }
            set { _activo = value; }
        }

    }
}
