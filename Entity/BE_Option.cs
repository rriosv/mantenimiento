﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_Option
    {
        public int IdOptions { get; set; }        
        public string vDescripcion { get; set; }
        public int IdItems { get; set; }
        public int nOrden { get; set; }
        public decimal nPeso { get; set; }
    }
}
