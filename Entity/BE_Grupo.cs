﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_Grupo
    {
        private int _IdGrupo;
        private string _Nombre;
        private string _Descripcion;
        private bool _Activo;

        public int IdGrupo
        {
            get { return _IdGrupo; }
            set { _IdGrupo = value; }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        
        public bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

    }
}
