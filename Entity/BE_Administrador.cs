﻿
namespace Entity
{
    public class BE_Administrador
    {
        private int _id_usuario;
        private int _id_perfil_usuario;
        private string _login;
        private string _passw;
        private string _nombre;
        private string _correo;
        private bool _activo;
        private int _valor;
        private string _vDNI;
        private string _iCodOFicina;
        private string _oficina;
        private string _nivelEstudio;

        public string nivelEstudio
        {
            get { return _nivelEstudio; }
            set { _nivelEstudio = value; }
        }
        private string _nivelMonitoreo;

        public string nivelMonitoreo
        {
            get { return _nivelMonitoreo; }
            set { _nivelMonitoreo = value; }
        }
        private string _nivelEmblematico;

        public string nivelEmblematico
        {
            get { return _nivelEmblematico; }
            set { _nivelEmblematico = value; }
        }
        public string oficina
        {
            get { return _oficina; }
            set { _oficina = value; }
        }
        public string vDNI
        {
            get { return _vDNI; }
            set { _vDNI = value; }
        }
        

        public string iCodOFicina
        {
            get { return _iCodOFicina; }
            set { _iCodOFicina = value; }
        }

        public int valor
        {
            get { return _valor; }
            set { _valor = value; }
        }
        
        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }
        
        public int id_perfil_usuario
        {
            get { return _id_perfil_usuario; }
            set { _id_perfil_usuario = value; }
        }

        public string login
        {
            get { return _login; }
            set { _login = value; }
        }
        
        public string passw
        {
            get { return _passw; }
            set { _passw = value; }
        }

        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        
        public string correo
        {
            get { return _correo; }
            set { _correo = value; }
        }
        
        public bool activo
        {
            get { return _activo; }
            set { _activo = value; }
        }



    }
}
