﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
  public class BE_MON_Financiamiento
    {
      
        #region Atributos
            private int _id_proyecto;
            private string _montoMVCS;
            private int _tipoFinanciamiento;
            private string _entidad_cooperante;
            private string _documento;
            private string _monto;
            private int _id_tipo_aporte;
            private int _id_financiamiento;
            private int _tipoGrd;
            private string _montoObra;

            public string MontoObra
            {
                get { return _montoObra; }
                set { _montoObra = value; }
            }
            private string _montoSupervision;

            public string MontoSupervision
            {
                get { return _montoSupervision; }
                set { _montoSupervision = value; }
            }
            private int _id_tipo_trans;
            private string _nro_convenio;
            private DateTime _fecha_convenio;

            private string _strFecha_convenio;

            public string strFecha_convenio
            {
                get { return _strFecha_convenio; }
                set { _strFecha_convenio = value; }
            }
            private string _montoConvenio;
            private string _dispositivo_aprobacion;
            private DateTime _fecha_aprobacion;
            private string _montoAprobacion;
            private string _docUrl;
            private string _observacion;
            private int _id_usuario;
            private int _id_financiamientoContrapartida;
            private int _tipo;
            private int _id_finaTransferencia;

            private int _siaf;

            public int Siaf
            {
                get { return _siaf; }
                set { _siaf = value; }
            }
            private string _montoCompromiso;

            public string MontoCompromiso
            {
                get { return _montoCompromiso; }
                set { _montoCompromiso = value; }
            }
            private string _montoDevengado;

            public string MontoDevengado
            {
                get { return _montoDevengado; }
                set { _montoDevengado = value; }
            }
            private string _montoGirado;

            public string MontoGirado
            {
                get { return _montoGirado; }
                set { _montoGirado = value; }
            }
            private string nroComprobante;
            public string NroComprobante
            {
                get { return nroComprobante; }
                set { nroComprobante = value; }
            }


            private DateTime _fechaCompromiso;
            private DateTime _fechaDevengado;
            private DateTime _fechaGirado;
                    
            private DateTime _fechaComprobante;
            private DateTime _fechaTransferencia;
            private DateTime _fechaemision;

            public string fechaemision
            {
                get { return _fechaemision.ToString("dd/MM/yyyy"); }
                set { _fechaemision = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_fechaemision
            {
                get { return _fechaemision; }
                set { _fechaemision = value; }
            }

            public string fechaTransferencia
            {
                get { return _fechaTransferencia.ToString("dd/MM/yyyy"); }
                set { _fechaTransferencia = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_fechaTransferencia
            {
                get { return _fechaTransferencia; }
                set { _fechaTransferencia = value; }
            }


            public string fechaComprobante
            {
                get { return _fechaComprobante.ToString("dd/MM/yyyy"); }
                set { _fechaComprobante = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_fechaComprobante
            {
                get { return _fechaComprobante; }
                set { _fechaComprobante = value; }
            }



            public string fechaGirado
            {
                get { return _fechaGirado.ToString("dd/MM/yyyy"); }
                set { _fechaGirado = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_fechaGirado
            {
                get { return _fechaGirado; }
                set { _fechaGirado = value; }
            }

            public string fechaDevengado
            {
                get { return _fechaDevengado.ToString("dd/MM/yyyy"); }
                set { _fechaDevengado = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_fechaDevengado
            {
                get { return _fechaDevengado; }
                set { _fechaDevengado = value; }
            }

            public string fechaCompromiso
            {
                get { return _fechaCompromiso.ToString("dd/MM/yyyy"); }
                set { _fechaCompromiso = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_FechaCompromiso
            {
                get { return _fechaCompromiso; }
                set { _fechaCompromiso = value; }
            }

            private string _cadena;

            public string Cadena
            {
                get { return _cadena; }
                set { _cadena = value; }
            }
            private string _meta;

            public string Meta
            {
                get { return _meta; }
                set { _meta = value; }
            }

            private string _norCarta;

            public string NroCarta
            {
                get { return _norCarta; }
                set { _norCarta = value; }
            }
      private DateTime _fechaCartaOrden;
      public string fechaCartaOrden
      {
          get { return _fechaCartaOrden.ToString("dd/MM/yyyy"); }
          set { _fechaCartaOrden = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
      }

      public DateTime Date_fechaCartaOrden
      {
          get { return _fechaCartaOrden; }
          set { _fechaCartaOrden = value; }
      }

        #endregion

      private string _id;

      public string Id
      {
          get { return _id; }
          set { _id = value; }
      }
      private int _id_CartaOrden;

      public int Id_CartaOrden
      {
          get { return _id_CartaOrden; }
          set { _id_CartaOrden = value; }
      }
         

            public int Id_finaTransferencia
            {
                get { return _id_finaTransferencia; }
                set { _id_finaTransferencia = value; }
            }

            private string _comentario;

            public string Comentario
            {
                get { return _comentario; }
                set { _comentario = value; }
            }
        #region Propiedades

            public int id_financiamientoContrapartida
            {
                get { return _id_financiamientoContrapartida; }
                set { _id_financiamientoContrapartida = value; }
            }
            public int tipo
            {
                get { return _tipo; }
                set { _tipo = value; }
            }
      
            public int id_proyecto
            {
                get { return _id_proyecto; }
                set { _id_proyecto = value; }
            }
            public string montoMVCS
            {
                get { return _montoMVCS; }
                set { _montoMVCS = value; }
            }

            public int tipoFinanciamiento
            {
                get { return _tipoFinanciamiento; }
                set { _tipoFinanciamiento = value; }
            }

            public string entidad_cooperante
            {
                get { return _entidad_cooperante; }
                set { _entidad_cooperante = value; }
            }

            public string documento
            {
                get { return _documento; }
                set { _documento = value; }
            }

            public string monto
            {
                get { return _monto; }
                set { _monto = value; }
            }



            public int id_tipo_aporte
            {
                get { return _id_tipo_aporte; }
                set { _id_tipo_aporte = value; }
            }

            public int id_financiamiento
            {
                get { return _id_financiamiento; }
                set { _id_financiamiento = value; }
            }
            public int tipoGrd
            {
                get { return _tipoGrd; }
                set { _tipoGrd = value; }
            }


            public int id_tipo_trans
            {
                get { return _id_tipo_trans; }
                set { _id_tipo_trans = value; }
            }
            public string nro_convenio
            {
                get { return _nro_convenio; }
                set { _nro_convenio = value; }
            }
            public string fecha_convenio
            {
                get { return _fecha_convenio.ToString("dd/MM/yyyy"); }
                set { _fecha_convenio = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_Fecha_Convenio
            {
                get { return _fecha_convenio; }
                set { _fecha_convenio = value; }
            }

            public string montoConvenio
            {
                get { return _montoConvenio; }
                set { _montoConvenio = value; }
            }

            public string dispositivo_aprobacion
            {
                get { return _dispositivo_aprobacion; }
                set { _dispositivo_aprobacion = value; }
            }

            public string fecha_aprobacion
            {
                get { return _fecha_aprobacion.ToString("dd/MM/yyyy"); }
                set { _fecha_aprobacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
            }

            public DateTime Date_fecha_aprobacion
            {
                get { return _fecha_aprobacion; }
                set { _fecha_aprobacion = value; }
            }

            private string _strfecha_aprobacion;

            public string strfecha_aprobacion
            {
                get { return _strfecha_aprobacion; }
                set { _strfecha_aprobacion = value; }
            }

            public string montoAprobacion
            {
                get { return _montoAprobacion; }
                set { _montoAprobacion = value; }
            }


            public string docUrl
            {
                get { return _docUrl; }
                set { _docUrl = value; }
            }

            public string observacion
            {
                get { return _observacion; }
                set { _observacion = value; }
            }

            public int id_usuario
            {
                get { return _id_usuario; }
                set { _id_usuario = value; }
            }


            private int? _id_transferenciaFinanciera;

            public int? Id_transferenciaFinanciera
            {
                get { return _id_transferenciaFinanciera; }
                set { _id_transferenciaFinanciera = value; }
            }
            private int _id_tipoProcesoFinanciero;

            public int Id_tipoProcesoFinanciero
            {
                get { return _id_tipoProcesoFinanciero; }
                set { _id_tipoProcesoFinanciero = value; }
            }
            private string _nroRegistro;

            public string NroRegistro
            {
                get { return _nroRegistro; }
                set { _nroRegistro = value; }
            }

            private string _montoRegistrado;

            public string MontoRegistrado
            {
                get { return _montoRegistrado; }
                set { _montoRegistrado = value; }
            }
            private int _id_tipoDocumentoSustento;

            public int Id_tipoDocumentoSustento
            {
                get { return _id_tipoDocumentoSustento; }
                set { _id_tipoDocumentoSustento = value; }
            }
            private string _NroDocumentoSustento;

            public string NroDocumentoSustento
            {
                get { return _NroDocumentoSustento; }
                set { _NroDocumentoSustento = value; }
            }
            private string _DocumentoPago;

            public string DocumentoPago
            {
                get { return _DocumentoPago; }
                set { _DocumentoPago = value; }
            }
            private string _NroDocPago;

            public string NroDocPago
            {
                get { return _NroDocPago; }
                set { _NroDocPago = value; }
            }

            private string _Banco;

            public string Banco
            {
                get { return _Banco; }
                set { _Banco = value; }
            }
            private string _NroCuentaBanco;

            public string NroCuentaBanco
            {
                get { return _NroCuentaBanco; }
                set { _NroCuentaBanco = value; }
            }

      private DateTime _fechaRegistro;
      private DateTime _fechaDocPago;

      public string fechaRegistro
      {
          get { return _fechaRegistro.ToString("dd/MM/yyyy"); }
          set { _fechaRegistro = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
      }

      public DateTime Date_fechaRegistro
      {
          get { return _fechaRegistro; }
          set { _fechaRegistro = value; }
      }

      public string fechaDocPago
      {
          get { return _fechaDocPago.ToString("dd/MM/yyyy"); }
          set { _fechaDocPago = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
      }

      public DateTime Date_fechaDocPago
      {
          get { return _fechaDocPago; }
          set { _fechaDocPago = value; }
      }

      private int _id_proceso_financiero;

      public int Id_proceso_financiero
      {
          get { return _id_proceso_financiero; }
          set { _id_proceso_financiero = value; }
      }

      
      private string _fte;

      public string Fte
      {
          get { return _fte; }
          set { _fte = value; }
      }
      private string _UE_destino;

      public string UE_destino
      {
          get { return _UE_destino; }
          set { _UE_destino = value; }
      }
      private string _girado;

      public string Girado
      {
          get { return _girado; }
          set { _girado = value; }
      }


      private int _id_transferencia_presupuesta;

      public int Id_transferencia_presupuesta
      {
          get { return _id_transferencia_presupuesta; }
          set { _id_transferencia_presupuesta = value; }
      }
      private string _EjecutoraHabilita;

      public string EjecutoraHabilita
      {
          get { return _EjecutoraHabilita; }
          set { _EjecutoraHabilita = value; }
      }
      private string _NotaModificatoria;

      public string NotaModificatoria
      {
          get { return _NotaModificatoria; }
          set { _NotaModificatoria = value; }
      }

      private string _EjecutoraHabilitada;

      public string EjecutoraHabilitada
      {
          get { return _EjecutoraHabilitada; }
          set { _EjecutoraHabilitada = value; }
      }
      private string _CodigoPresupuestal;

      public string CodigoPresupuestal
      {
          get { return _CodigoPresupuestal; }
          set { _CodigoPresupuestal = value; }
      }
      private string _MontoHabilitado;

      public string MontoHabilitado
      {
          get { return _MontoHabilitado; }
          set { _MontoHabilitado = value; }
      }
      private string _FteFinanciamiento;

      public string FteFinanciamiento
      {
          get { return _FteFinanciamiento; }
          set { _FteFinanciamiento = value; }
      }

      private int _flag;

      public int flag
      {
          get { return _flag; }
          set { _flag = value; }
      }
      private int _flagHabilito;

      public int FlagHabilito
      {
          get { return _flagHabilito; }
          set { _flagHabilito = value; }
      }

      private DateTime _fecha;

      public DateTime Date_Fecha
      {
          get { return _fecha; }
          set { _fecha = value; }
      }

      public string Fecha
      {
          get { return _fecha.ToString("dd/MM/yyyy"); }
          set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
      }

      private int _tipo_transferencia;

      public int Tipo_transferencia
      {
          get { return _tipo_transferencia; }
          set { _tipo_transferencia = value; }
      }

      private string anioFiscal;

      public string AnioFiscal
      {
          get { return anioFiscal; }
          set { anioFiscal = value; }
      }

        #endregion


      private string _tipo_aporte;

      public string tipo_aporte
      {
          get { return _tipo_aporte; }
          set { _tipo_aporte = value; }
      }

      private string _usuario;

      public string usuario
      {
          get { return _usuario; }
          set { _usuario = value; }
      }

      private string _strFecha_update;

      public string strFecha_update
      {
          get { return _strFecha_update; }
          set { _strFecha_update = value; }
      }


      private Int64 _numero;

      public Int64 numero
      {
          get { return _numero; }
          set { _numero = value; }
      }


      private string _strTipo_Transferencia;

      public string strTipo_Transferencia
      {
          get { return _strTipo_Transferencia; }
          set { _strTipo_Transferencia = value; }
      }

      private string _strTipo_TransferenciaFP;

      public string strTipo_TransferenciaFP
      {
          get { return _strTipo_TransferenciaFP; }
          set { _strTipo_TransferenciaFP = value; }
      }

      private int _snip;

      public int snip
      {
          get { return _snip; }
          set { _snip = value; }
      }

      private int _idEjecutora;

      public int idEjecutora
      {
          get { return _idEjecutora; }
          set { _idEjecutora = value; }
      }

        public decimal MontoReservaContingencia { get; set; }
    }
}
