using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
[Serializable]
public class BEProceso
{
    #region Atributos
    private int intId_proyecto;
    private string strContratista;
    private string strUsuario;
    private int intRUC;
    private string strNroLicitacion;
    #endregion

    #region Constructores

    #endregion

    #region Propiedades
    public int Id_proyecto
    {
        get { return intId_proyecto; }
        set { intId_proyecto = value; }
    }
    public string Contratista
    {
        get { return strContratista; }
        set { strContratista = value; }
    }
    public string Usuario
    {
        get { return strUsuario; }
        set { strUsuario = value; }
    }
    public int RUC
    {
        get { return intRUC; }
        set { intRUC = value; }
    }
    public string NroLicitacion
    {
        get { return strNroLicitacion; }
        set { strNroLicitacion = value; }
    }
    #endregion

}
}
