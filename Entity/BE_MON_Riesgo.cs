﻿using System;

namespace Entity
{
    public class BE_MON_Riesgo
    {
        public int id_proyecto { get; set; }
        public int id_usuario { get; set; }
        public int id_estado { get; set; }
        public int id_subEstado { get; set; }

        private DateTime _fecha;

        public string fecha
        {
            get { return _fecha.ToString("dd/MM/yyyy"); }
            set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime date_fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public int flagResultado { get; set; }
        public int id_item { get; set; }
        public string comentario { get; set; }

        public int id_riesgo { get; set; }

        public string observacion { get; set; }
        public string urlActa { get; set; }
    }
}
