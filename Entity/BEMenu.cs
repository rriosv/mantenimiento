﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BEMenu
    {
        private int _id_menu;

        public int id_menu
        {
            get { return _id_menu; }
            set { _id_menu = value; }
        }
        private int _id_usuario;

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }
        private string _titulo;

        public string titulo
        {
            get { return _titulo; }
            set { _titulo = value; }
        }
        private string _subtitulo;

        public string subtitulo
        {
            get { return _subtitulo; }
            set { _subtitulo = value; }
        }
        private string _pagina;

        public string pagina
        {
            get { return _pagina; }
            set { _pagina = value; }
        }

        private int _id_Acceso;

        public int id_Acceso
        {
            get { return _id_Acceso; }
            set { _id_Acceso = value; }
        }

        private string _subtitulo2;

        public string subtitulo2
        {
            get { return _subtitulo2; }
            set { _subtitulo2 = value; }
        }
    }
}
