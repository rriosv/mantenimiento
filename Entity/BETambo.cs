using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BETambo
    {
        #region Atributos
        private Int64 intid_tambo;
        private string strcod_depa;
        private string strcod_prov;
        private string strcod_dist;
        private string strcod_ccpp;
        private int intsnip;
        private string strproceso;
        private string strfec_firma_contrato;
        private string strfec_entrega_terreno;
        private string strfec_inicio_plazo;
        private string strfec_entrega_exp;
        private string strfec_aprobo_exp;
        private string strfec_culm_plazo;
        private string strfec_liquidacion;
        private string strnom_proyecto;
        private int intperiodo;
        private double dblmonto_inversion;
        private double dblmonto_snip;
        private string strfecha_viabilidad;
        private int intid_usuario;
        private string strfecha_supervision;
        private int intporcentaje;
        private string strstrCriterio;
        private string strstrBuscar;
        private int intid_estado;
        #endregion

        #region Propiedades
        public string Fecha_Liquidacion
        {
            get { return strfec_liquidacion; }
            set { strfec_liquidacion = value; }
        }
        public int Id_Estado
        {
            get { return intid_estado; }
            set { intid_estado = value; }
        }
        public string strBuscar
        {
            get { return strstrBuscar; }
            set { strstrBuscar = value; }
        }

        public string strCriterio
        {
            get { return strstrCriterio; }
            set { strstrCriterio = value; }
        }
        public string Fecha_supervision
        {
            get { return strfecha_supervision; }
            set { strfecha_supervision = value; }
        }
        public int Porcentaje
        {
            get { return intporcentaje; }
            set { intporcentaje = value; }
        }
        public int Id_usuario
        {
            get { return intid_usuario; }
            set { intid_usuario = value; }
        }
        public string Fecha_viabilidad
        {
            get { return strfecha_viabilidad; }
            set { strfecha_viabilidad = value; }
        }
        public double Monto_snip
        {
            get { return dblmonto_snip; }
            set { dblmonto_snip = value; }
        }
        public double Monto_inversion
        {
            get { return dblmonto_inversion; }
            set { dblmonto_inversion = value; }
        }
        public int Periodo
        {
            get { return intperiodo; }
            set { intperiodo = value; }
        }
        public string Nom_proyecto
        {
            get { return strnom_proyecto; }
            set { strnom_proyecto = value; }
        }
        public string Fec_culm_plazo
        {
            get { return strfec_culm_plazo; }
            set { strfec_culm_plazo = value; }
        }
        public string Fec_aprobo_exp
        {
            get { return strfec_aprobo_exp; }
            set { strfec_aprobo_exp = value; }
        }
        public string Fec_entrega_exp
        {
            get { return strfec_entrega_exp; }
            set { strfec_entrega_exp = value; }
        }
        public string Fec_inicio_plazo
        {
            get { return strfec_inicio_plazo; }
            set { strfec_inicio_plazo = value; }
        }
        public string Fec_entrega_terreno
        {
            get { return strfec_entrega_terreno; }
            set { strfec_entrega_terreno = value; }
        }
        public string Fec_firma_contrato
        {
            get { return strfec_firma_contrato; }
            set { strfec_firma_contrato = value; }
        }
        public string Proceso
        {
            get { return strproceso; }
            set { strproceso = value; }
        }
        public int Snip
        {
            get { return intsnip; }
            set { intsnip = value; }
        }
        public string Cod_ccpp
        {
            get { return strcod_ccpp; }
            set { strcod_ccpp = value; }
        }
        public string Cod_dist
        {
            get { return strcod_dist; }
            set { strcod_dist = value; }
        }
        public string Cod_prov
        {
            get { return strcod_prov; }
            set { strcod_prov = value; }
        }
        public string Cod_depa
        {
            get { return strcod_depa; }
            set { strcod_depa = value; }
        }
        public Int64 Id_Tambo
        {
            get { return intid_tambo; }
            set { intid_tambo = value; }
        }
        #endregion
    }
}
