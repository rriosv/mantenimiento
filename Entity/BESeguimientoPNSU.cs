﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BESeguimientoPNSU
    {
        #region Atributos
        private int _id_usuario ;
	    private int _anio ;
        private int _tipo_R;
	    private string _estadoViabilidad ;
        private double _montoDispositivoLegalGenFinan;
        private string _fuenteFinanciamiento;
        private string _objetoFinanciamiento;
        private string _modalidadEjecElaboracionExpTec;
        private double _montoTotalElaboracionExpTecFinanMVCS;
        private double _montoTotalSupervisionExpTecFinanMVCS;
        private double _montoTotalTransferirMVCS;
        private double _cofinanciamientoExpTec;
        private double _compromisoAnio0_FinanciamientoElaboracionExpTec;
        private double _compromisoAnio1_FinanciamientoElaboracionExpTec;
        private string _fechaResolAprobacionExpTec;
        private string _modalidadEjecucionObra;
        private double _montoTotalObraFinanciar;
        private double _montoTotalSupervisionFinanciar;
        private double _cofinanciamiento;
        private double _compromisoAnio0_FinanciamientoEjecucionObra;
        private double _compromisoAnio1_FinanciamientoEjecucionObra;
        private string _numInformeTecnico;
        private string _fechaInformeVivienda;
        private string _numConvenio;
        private string _fechaConvenio;
        private string _entidadMantenimiento;
        private string _decretoApruebaFinan;
        private string _fechaAprobacion;
        private string _tipoAnexoAdjuntoConvenio;
        private string _nombre_Autoridad;
        private string _cargo_Autoridad;
        private string _dni_Autoridad;
        private string _direccion_Autoridad;
        private string _distrito_Autoridad;
        private string _provincia_Autoridad;
        private string _departamento_Autoridad;
        private string _telefono__Autoridad;
        private string _correo_Autoridad;
        private string _presentaOmisionesDoc_SgnInformeTec;
        private string _aprobacionEstudiosAprovechamientoHidrico_SgnInformeTec;
        private string _FichaTecAmbiental_SgnInformeTec;
        private string _planMonitoreoArqueologico_SgnInformeTec;
        private string _opinionFavorableDIGESA_SgnInformeTec;
        private string _autorizacionANA_SgnInformeTec;
        private string _autorizacionSanitariaTanqueSeptico_SgnInformeTec;
        private string _otrasObservacionesDocumentarias_SgnInformeTec;
        private string _presentaOmisionesDoc_Seguimiento;
        private string _aprobacionEstudiosAprovechamientoHidrico_Seguimiento;
        private string _certificacionambiental_Seguimiento;
        private string _FichaTecAmbiental_Seguimiento;
        private string _certificadoInexistenciaRestosArqueologicos_Seguimiento;
        private string _planMonitoreoArqueologico_Seguimiento;
        private string _opinionFavorableDIGESA_Seguimiento;
        private string _autorizacionANA_Seguimiento;
        private string _autorizacionSanitariaTanqueSeptico_Seguimiento;
        private string _otrasObservacionesDocumentarias_Seguimiento;
        private string _notas;
        private int _id_snip;
        private string _tipoUE;
        private double _montoFaseInversionBcoPYSnip;
        private string _fechaRegistroFormatoSnip16;
        private string _certificacionAmbiental;
        private string _cira;

        private string _sector;
        private string _cod_depa;
        private string _cod_prov;
        private string _cod_dist;
        private string _lista;
        private int _cod_estado;
        private string _lista49;
        private int _tipoP;

        private int _id_solicitudes;
        private string _informeTecnicoEspecialista;

        public string informeTecnicoEspecialista
        {
            get { return _informeTecnicoEspecialista; }
            set { _informeTecnicoEspecialista = value; }
        }
        private string _fechaInformeTecnicoEspecialista;

        public string fechaInformeTecnicoEspecialista
        {
            get { return _fechaInformeTecnicoEspecialista; }
            set { _fechaInformeTecnicoEspecialista = value; }
        }

        private string _resolAprobacionExpTec;

        public string resolAprobacionExpTec
        {
            get { return _resolAprobacionExpTec; }
            set { _resolAprobacionExpTec = value; }
        }
        private string _periodo;

        public string periodo
        {
            get { return _periodo; }
            set { _periodo = value; }
        }
        #endregion

        #region Propiedades
        public string tipoUE
        {
            get { return _tipoUE; }
            set { _tipoUE = value; }
        }
        public double montoFaseInversionBcoPYSnip
        {
            get { return _montoFaseInversionBcoPYSnip; }
            set { _montoFaseInversionBcoPYSnip = value; }
        }
        public string fechaRegistroFormatoSnip16
        {
            get { return _fechaRegistroFormatoSnip16; }
            set { _fechaRegistroFormatoSnip16 = value; }
        }
        public string certificacionAmbiental
        {
            get { return _certificacionAmbiental; }
            set { _certificacionAmbiental = value; }
        }
        public string cira
        {
            get { return _cira; }
            set { _cira = value; }
        }

        
        public string sector
        {
            get { return _sector; }
            set { _sector = value; }
        }
        public string cod_depa
        {
            get { return _cod_depa; }
            set { _cod_depa = value; }
        }
        public string cod_prov
        {
            get { return _cod_prov; }
            set { _cod_prov = value; }
        }
        public string cod_dist
        {
            get { return _cod_dist; }
            set { _cod_dist = value; }
        }
        public string lista
        {
            get { return _lista; }
            set { _lista = value; }
        }
        public int cod_estado
        {
            get { return _cod_estado; }
            set { _cod_estado = value; }
        }
        public string lista49
        {
            get { return _lista49; }
            set { _lista49 = value; }
        }
        public int tipoP
        {
            get { return _tipoP; }
            set { _tipoP = value; }
        }
        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }
        public int anio
        {
            get { return _anio; }
            set { _anio = value; }
        }
        public int tipo_R
        {
            get { return _tipo_R; }
            set { _tipo_R = value; }
        }
        public string estadoViabilidad
        {
            get { return _estadoViabilidad; }
            set { _estadoViabilidad = value; }
        }
        public double montoDispositivoLegalGenFinan
        {
            get { return _montoDispositivoLegalGenFinan; }
            set { _montoDispositivoLegalGenFinan = value; }
        }
        public string fuenteFinanciamiento
        {
            get { return _fuenteFinanciamiento; }
            set { _fuenteFinanciamiento = value; }
        }
        public string objetoFinanciamiento
        {
            get { return _objetoFinanciamiento; }
            set { _objetoFinanciamiento = value; }
        }
        public string modalidadEjecElaboracionExpTec
        {
            get { return _modalidadEjecElaboracionExpTec; }
            set { _modalidadEjecElaboracionExpTec = value; }
        }
        public double montoTotalElaboracionExpTecFinanMVCS
        {
            get { return _montoTotalElaboracionExpTecFinanMVCS; }
            set { _montoTotalElaboracionExpTecFinanMVCS = value; }
        }
        public double montoTotalSupervisionExpTecFinanMVCS
        {
            get { return _montoTotalSupervisionExpTecFinanMVCS; }
            set { _montoTotalSupervisionExpTecFinanMVCS = value; }
        }
        public double montoTotalTransferirMVCS
        {
            get { return _montoTotalTransferirMVCS; }
            set { _montoTotalTransferirMVCS = value; }
        }
        public double cofinanciamientoExpTec
        {
            get { return _cofinanciamientoExpTec; }
            set { _cofinanciamientoExpTec = value; }
        }
        public double compromisoAnio0_FinanciamientoElaboracionExpTec
        {
            get { return _compromisoAnio0_FinanciamientoElaboracionExpTec; }
            set { _compromisoAnio0_FinanciamientoElaboracionExpTec = value; }
        }
        public double compromisoAnio1_FinanciamientoElaboracionExpTec
        {
            get { return _compromisoAnio1_FinanciamientoElaboracionExpTec; }
            set { _compromisoAnio1_FinanciamientoElaboracionExpTec = value; }
        }
        public string fechaResolAprobacionExpTec
        {
            get { return _fechaResolAprobacionExpTec; }
            set { _fechaResolAprobacionExpTec = value; }
        }
        public string modalidadEjecucionObra
        {
            get { return _modalidadEjecucionObra; }
            set { _modalidadEjecucionObra = value; }
        }
        public double montoTotalObraFinanciar
        {
            get { return _montoTotalObraFinanciar; }
            set { _montoTotalObraFinanciar = value; }
        }
        public double montoTotalSupervisionFinanciar
        {
            get { return _montoTotalSupervisionFinanciar; }
            set { _montoTotalSupervisionFinanciar = value; }
        }
        public double cofinanciamiento
        {
            get { return _cofinanciamiento; }
            set { _cofinanciamiento = value; }
        }
        public double compromisoAnio0_FinanciamientoEjecucionObra
        {
            get { return _compromisoAnio0_FinanciamientoEjecucionObra; }
            set { _compromisoAnio0_FinanciamientoEjecucionObra = value; }
        }
        public double compromisoAnio1_FinanciamientoEjecucionObra
        {
            get { return _compromisoAnio1_FinanciamientoEjecucionObra; }
            set { _compromisoAnio1_FinanciamientoEjecucionObra = value; }
        }
        public string numInformeTecnico
        {
            get { return _numInformeTecnico; }
            set { _numInformeTecnico = value; }
        }
        public string fechaInformeVivienda
        {
            get { return _fechaInformeVivienda; }
            set { _fechaInformeVivienda = value; }
        }
        public string numConvenio
        {
            get { return _numConvenio; }
            set { _numConvenio = value; }
        }
        public string fechaConvenio
        {
            get { return _fechaConvenio; }
            set { _fechaConvenio = value; }
        }
        public string entidadMantenimiento
        {
            get { return _entidadMantenimiento; }
            set { _entidadMantenimiento = value; }
        }
        public string decretoApruebaFinan
        {
            get { return _decretoApruebaFinan; }
            set { _decretoApruebaFinan = value; }
        }
        public string fechaAprobacion
        {
            get { return _fechaAprobacion; }
            set { _fechaAprobacion = value; }
        }
        public string tipoAnexoAdjuntoConvenio
        {
            get { return _tipoAnexoAdjuntoConvenio; }
            set { _tipoAnexoAdjuntoConvenio = value; }
        }
        public string nombre_Autoridad
        {
            get { return _nombre_Autoridad; }
            set { _nombre_Autoridad = value; }
        }
        public string cargo_Autoridad
        {
            get { return _cargo_Autoridad; }
            set { _cargo_Autoridad = value; }
        }
        public string dni_Autoridad
        {
            get { return _dni_Autoridad; }
            set { _dni_Autoridad = value; }
        }
        public string direccion_Autoridad
        {
            get { return _direccion_Autoridad; }
            set { _direccion_Autoridad = value; }
        }
        public string distrito_Autoridad
        {
            get { return _distrito_Autoridad; }
            set { _distrito_Autoridad = value; }
        }
        public string provincia_Autoridad
        {
            get { return _provincia_Autoridad; }
            set { _provincia_Autoridad = value; }
        }
        public string departamento_Autoridad
        {
            get { return _departamento_Autoridad; }
            set { _departamento_Autoridad = value; }
        }
        public string telefono__Autoridad
        {
            get { return _telefono__Autoridad; }
            set { _telefono__Autoridad = value; }
        }
        public string correo_Autoridad
        {
            get { return _correo_Autoridad; }
            set { _correo_Autoridad = value; }
        }
        public string presentaOmisionesDoc_SgnInformeTec
        {
            get { return _presentaOmisionesDoc_SgnInformeTec; }
            set { _presentaOmisionesDoc_SgnInformeTec = value; }
        }
        public string aprobacionEstudiosAprovechamientoHidrico_SgnInformeTec
        {
            get { return _aprobacionEstudiosAprovechamientoHidrico_SgnInformeTec; }
            set { _aprobacionEstudiosAprovechamientoHidrico_SgnInformeTec = value; }
        }
        public string FichaTecAmbiental_SgnInformeTec
        {
            get { return _FichaTecAmbiental_SgnInformeTec; }
            set { _FichaTecAmbiental_SgnInformeTec = value; }
        }
        public string planMonitoreoArqueologico_SgnInformeTec
        {
            get { return _planMonitoreoArqueologico_SgnInformeTec; }
            set { _planMonitoreoArqueologico_SgnInformeTec = value; }
        }
        public string opinionFavorableDIGESA_SgnInformeTec
        {
            get { return _opinionFavorableDIGESA_SgnInformeTec; }
            set { _opinionFavorableDIGESA_SgnInformeTec = value; }
        }
        public string autorizacionANA_SgnInformeTec
        {
            get { return _autorizacionANA_SgnInformeTec; }
            set { _autorizacionANA_SgnInformeTec = value; }
        }
        public string autorizacionSanitariaTanqueSeptico_SgnInformeTec
        {
            get { return _autorizacionSanitariaTanqueSeptico_SgnInformeTec; }
            set { _autorizacionSanitariaTanqueSeptico_SgnInformeTec = value; }
        }
        public string otrasObservacionesDocumentarias_SgnInformeTec
        {
            get { return _otrasObservacionesDocumentarias_SgnInformeTec; }
            set { _otrasObservacionesDocumentarias_SgnInformeTec = value; }
        }
        public string presentaOmisionesDoc_Seguimiento
        {
            get { return _presentaOmisionesDoc_Seguimiento; }
            set { _presentaOmisionesDoc_Seguimiento = value; }
        }
        public string aprobacionEstudiosAprovechamientoHidrico_Seguimiento
        {
            get { return _aprobacionEstudiosAprovechamientoHidrico_Seguimiento; }
            set { _aprobacionEstudiosAprovechamientoHidrico_Seguimiento = value; }
        }
        public string certificacionambiental_Seguimiento
        {
            get { return _certificacionambiental_Seguimiento; }
            set { _certificacionambiental_Seguimiento = value; }
        }
        public string FichaTecAmbiental_Seguimiento
        {
            get { return _FichaTecAmbiental_Seguimiento; }
            set { _FichaTecAmbiental_Seguimiento = value; }
        }
        public string certificadoInexistenciaRestosArqueologicos_Seguimiento
        {
            get { return _certificadoInexistenciaRestosArqueologicos_Seguimiento; }
            set { _certificadoInexistenciaRestosArqueologicos_Seguimiento = value; }
        }
        public string planMonitoreoArqueologico_Seguimiento
        {
            get { return _planMonitoreoArqueologico_Seguimiento; }
            set { _planMonitoreoArqueologico_Seguimiento = value; }
        }
        public string opinionFavorableDIGESA_Seguimiento
        {
            get { return _opinionFavorableDIGESA_Seguimiento; }
            set { _opinionFavorableDIGESA_Seguimiento = value; }
        }
        public string autorizacionANA_Seguimiento
        {
            get { return _autorizacionANA_Seguimiento; }
            set { _autorizacionANA_Seguimiento = value; }
        }
        public string autorizacionSanitariaTanqueSeptico_Seguimiento
        {
            get { return _autorizacionSanitariaTanqueSeptico_Seguimiento; }
            set { _autorizacionSanitariaTanqueSeptico_Seguimiento = value; }
        }
        public string otrasObservacionesDocumentarias_Seguimiento
        {
            get { return _otrasObservacionesDocumentarias_Seguimiento; }
            set { _otrasObservacionesDocumentarias_Seguimiento = value; }
        }
        public string notas
        {
            get { return _notas; }
            set { _notas = value; }
        }
        public int id_snip
        {
            get { return _id_snip; }
            set { _id_snip = value; }
        }

        public int id_solicitudes
        {
            get { return _id_solicitudes; }
            set { _id_solicitudes = value; }
        }


        #endregion
    }
}
