﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BE_MON_Historial
    {
        private int id_proyecto_historial;

        public int Id_proyecto_historial
        {
            get { return id_proyecto_historial; }
            set { id_proyecto_historial = value; }
        }

        private int _id_proyecto;

        public int Id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }
        private int _tipoFinanciamiento;

        public int TipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }
        private string _snip;

        public string Snip
        {
            get { return _snip; }
            set { _snip = value; }
        }

        private DateTime _fecha;

        public string Fecha
        {
            get { return _fecha.ToString("dd/MM/yyyy"); }
            set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

       
        private int _id_usuario;

        public int Id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }
        private string _comentario;

        public string Comentario
        {
            get { return _comentario; }
            set { _comentario = value; }
        }
        private string _url;

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        private int tipo;

        public int Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
    }
}
