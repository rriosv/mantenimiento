using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
[Serializable]
public class BEProblema
{
    #region Atributos
    private int intId_problema;
    private string strCriterio;
    private string strBuscar;
    private int intId_prioridad;
    private int intId_Sector;
    private int intId_Programa;
    private int intId_Oficina;
    private string strCod_depa;
    private int intPeriodo;
    private int intId_Estado;
    private int intId_Tipo_Prob;
    private int intId_Nivel_Sol;

    #endregion

    #region Constructores

    #endregion

    #region Propiedades
    public int Id_problema
    {
        get { return intId_problema; }
        set { intId_problema = value; }
    }
    public string Criterio
    {
        get { return strCriterio; }
        set { strCriterio = value; }
    }
    public string Buscar
    {
        get { return strBuscar; }
        set { strBuscar = value; }
    }
    public int Id_prioridad
    {
        get { return intId_prioridad; }
        set { intId_prioridad = value; }
    }
    public int Id_Sector
    {
        get { return intId_Sector; }
        set { intId_Sector = value; }
    }
    public int Id_Programa
    {
        get { return intId_Programa; }
        set { intId_Programa = value; }
    }
    public int Id_Oficina
    {
        get { return intId_Oficina; }
        set { intId_Oficina = value; }
    }
    public string Cod_depa
    {
        get { return strCod_depa; }
        set { strCod_depa = value; }
    }
    public int Periodo
    {
        get { return intPeriodo; }
        set { intPeriodo = value; }
    }
    public int Id_Estado
    {
        get { return intId_Estado; }
        set { intId_Estado = value; }
    }
    public int Id_Tipo_Prob
    {
        get { return intId_Tipo_Prob; }
        set { intId_Tipo_Prob = value; }
    }
    public int Id_Nivel_Sol
    {
        get { return intId_Nivel_Sol; }
        set { intId_Nivel_Sol = value; }
    }
    #endregion

}
}
