using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BECheckList
    {
        #region Atributos
        private Int64 _id_result;
        private Int64 _id_solicitudes;
        private Int32 _id_campo;
        private string _valor;
        private string _observaciones;
        private DateTime _fecha_insert;
        private DateTime _fecha_update;
        private int _cod_estado;
        private int _id_usuario;
        private Int32 _ordcampo;
        private int _codsubsector;
        private string _tiene;
        private string _expediente;
        private string _firma;

        private Int64 _id_informe;
        private string _titulo;
        private string _descripcion;
        private string _informe;

        private int _id_plantilla;
        private string _strnocorresponde;

        private DateTime _fechaInicioEvaluacion;
        private DateTime _fechaFinaEvaluacion;
        #endregion

        #region Propiedades
        public string NoCorrespode
        {
            get { return _strnocorresponde; }
            set { _strnocorresponde = value; }
        }
        public int Id_Plantilla
        {
            get { return _id_plantilla; }
            set { _id_plantilla = value; }
        }

        public string Informe
        {
            get { return _informe; }
            set { _informe = value; }
        }
        public string Titulo
        {
            get { return _titulo; }
            set { _titulo = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        public Int64 Id_Informe
        {
            get { return _id_informe; }
            set { _id_informe = value; }
        }
        public string Firma
        {
            get { return _firma; }
            set { _firma = value; }
        }
        public string Expediente
        {
            get { return _expediente; }
            set { _expediente = value; }
        }
        public string Tiene
        {
            get { return _tiene; }
            set { _tiene = value; }
        }
        public int CodSubsector
        {
            get { return _codsubsector; }
            set { _codsubsector = value; }
        }
        public Int32 OrdCampo
        {
            get { return _ordcampo; }
            set { _ordcampo = value; }
        }
        public int Id_Usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }
        public int Cod_Estado
        {
            get { return _cod_estado; }
            set { _cod_estado = value; }
        }
        public DateTime Fecha_Update
        {
            get { return _fecha_update; }
            set { _fecha_update = value; }
        }
        public DateTime Fecha_Insert
        {
            get { return _fecha_insert; }
            set { _fecha_insert = value; }
        }
        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; }
        }
        public string Valor
        {
            get { return _valor; }
            set { _valor = value; }
        }
        public Int32 Id_Campo
        {
            get { return _id_campo; }
            set { _id_campo = value; }
        }
        public Int64 Id_Solicitudes
        {
            get { return _id_solicitudes; }
            set { _id_solicitudes = value; }
        }
        public Int64 Id_Result
        {
            get { return _id_result; }
            set { _id_result = value; }
        }


        public DateTime fechaInicioEvaluacion
        {
            get
            {
                return _fechaInicioEvaluacion;
            }

            set
            {
                _fechaInicioEvaluacion = value;
            }
        }

        public DateTime fechaFinEvaluacion
        {
            get
            {
                return _fechaFinaEvaluacion;
            }

            set
            {
                _fechaFinaEvaluacion = value;
            }
        }

        #endregion
        public string Expediente_Sitrad { get; set; }

        public Int32 snip { get; set; }

        public int idFichaRevision { get; set; }

        public string nombreProyecto { get; set; }
    }
}

