using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BETransferencia
    {
        #region Atributos
        private Int64 _id_transf;
        private string _nro_ds;

        private double _monto_disponible;
        private double _monto_disponible1;
        private double _porcentaje;
        private double _porcentaje1;
        private double _monto_transf;
        private double _monto_transf1;
        private double _saldo_transf;
        private double _saldo_transf1;

        private string _correos;
        private int _id_usuario;
        private int _id_subsector;

        private Int64 _id_det_transf;
        private int _snip;
        private double _total_inversion;
        private int _cod_estado;
        private int _tipo;
        private Int64 _id_solicitudes;

        private string _fechaOficio;

        private int anioDS;

        public int AnioDS
        {
            get { return anioDS; }
            set { anioDS = value; }
        }
        public string fechaOficio
        {
            get { return _fechaOficio; }
            set { _fechaOficio = value; }
        }
        private int _check;

        public int check
        {
            get { return _check; }
            set { _check = value; }
        }
        public Int64 id_solicitudes
        {
            get { return _id_solicitudes; }
            set { _id_solicitudes = value; }
        }
        private string _urlConvenio;

        public string urlConvenio
        {
            get { return _urlConvenio; }
            set { _urlConvenio = value; }
        }

        private string _nro_oficio;

        private double _montoTransferidoMef;

        public double montoTransferidoMef
        {
            get { return _montoTransferidoMef; }
            set { _montoTransferidoMef = value; }
        }

        private string _oficioNro;

        public string oficioNro
        {
            get { return _oficioNro; }
            set { _oficioNro = value; }
        }
        private string _oficioAnio;

        public string oficioAnio
        {
            get { return _oficioAnio; }
            set { _oficioAnio = value; }
        }

        public decimal ReservaContingencia { get; set; }
        #endregion

        #region Propiedades

        public string nro_oficio
        {
            get { return _nro_oficio; }
            set { _nro_oficio = value; }
        }

        public int Id_Subsector
        {
            get { return _id_subsector; }
            set { _id_subsector = value; }
        }
        public int Id_Usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }
        public Int64 Id_Det_Transf
        {
            get { return _id_det_transf; }
            set { _id_det_transf = value; }
        }
        public int Snip
        {
            get { return _snip; }
            set { _snip = value; }
        }
        public double Total_Inversion
        {
            get { return _total_inversion; }
            set { _total_inversion = value; }
        }
        public int Cod_Estado
        {
            get { return _cod_estado; }
            set { _cod_estado = value; }
        }
        public int Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }


        public Int64 Id_Transf
        {
            get { return _id_transf; }
            set { _id_transf = value; }
        }
        public string Nro_Ds
        {
            get { return _nro_ds; }
            set { _nro_ds = value; }
        }
        public double Monto_Disponible
        {
            get { return _monto_disponible; }
            set { _monto_disponible = value; }
        }
        public double Monto_Disponible1
        {
            get { return _monto_disponible1; }
            set { _monto_disponible1 = value; }
        }


        public double Porcentaje
        {
            get { return _porcentaje; }
            set { _porcentaje = value; }
        }
        public double Porcentaje1
        {
            get { return _porcentaje1; }
            set { _porcentaje1 = value; }
        }

        public double Monto_Transf
        {
            get { return _monto_transf; }
            set { _monto_transf = value; }
        }
        public double Monto_Transf1
        {
            get { return _monto_transf1; }
            set { _monto_transf1 = value; }
        }
        public double Saldo_Transf
        {
            get { return _saldo_transf; }
            set { _saldo_transf = value; }
        }
        public double Saldo_Transf1
        {
            get { return _saldo_transf1; }
            set { _saldo_transf1 = value; }
        }
        public string Correos
        {
            get { return _correos; }
            set { _correos = value; }
        }

        public decimal monto_ReservaContingencia { get; set; }
        #endregion
    }
}
