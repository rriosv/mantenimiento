using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEAcciones
    {
        #region Atributos
        private int intId_Acciones;
        private int intCod_Programa;
        private int intCod_Prioridad;
        private int intCod_Estrategia;
        private int intCod_SubEstado;
        private string strCod_proyecto;
        private int intPeriodo;
        private int intMes;
        private int intDia;
        private string strNombre;
        private string strCod_Dep;
        private string strCod_Prov;
        private string strCod_Dist;
        private string strCentroPoblado;
        private int intCap_nivel;
        private int strAcc_programa;
        private double dblCap_duracion;
        private string strUbigeo;
        private DateTime dtFecha_Inicio;
     
        private double dblProg_anual;
        private double dblProg_Mes;
        private double dblInscritos_H;
        private double dblAprobado_H;
        private double dblRetenido_H;
        private double dblDesaprobado_H;
        private double dblInscritos_M;
        private double dblAprobado_M;
        private double dblRetenido_M;
        private double dblDesaprobado_M;
  
        private double dblImporte_dol;
        private double dblImporte_sol;
        private double dblTipoCambio;
        private string strDestino;
        private string strTipoIfi;
        private string strIfi;
        private double dblMonto_Aprb2009;
        private double dblMonto_Anticrisis;
        private int intMoneda;
        private double dblMontoTot_Sol;
        private double dblMontoTot_Dol;
        private int intNro_Empl;
        private string strAmbito;
        private int intTipoDoc;
        private string strNro_doc;
        private string strUsuario;
        private DateTime dtFechaCorte;
        private int intEstado;
        private int intZonal;
        private double dblMonto_Inv;
        private string strTipo_Reg;
        private int intCodFinanc;
        private int strCod_estado;
        private string intCod_proyBono;
        private double dblMontoFinan;
        

        #endregion

        #region Constructores

        #endregion

        #region Propiedades

        public int IdAcciones
        {
            get { return intId_Acciones; }
            set { intId_Acciones = value; }
        }
        public int Cod_Programa
        {
            get { return intCod_Programa; }
            set { intCod_Programa = value; }
        }
        public int Cod_Prioridad
        {
            get { return intCod_Prioridad; }
            set { intCod_Prioridad = value; }
        }
        public int Cod_estrategia
        {
            get { return intCod_Estrategia; }
            set { intCod_Estrategia = value; }
        }
        public int Cod_SubEstado
        {
            get { return intCod_SubEstado; }
            set { intCod_SubEstado = value; }
        }
        public string Cod_proyecto
        {
            get { return strCod_proyecto; }
            set { strCod_proyecto = value; }
        }
        public int Periodo
        {
            get { return intPeriodo; }
            set { intPeriodo = value; }
        }
        public int Mes
        {
            get { return intMes; }
            set { intMes = value; }
        }
        public int Dia
        {
            get { return intDia; }
            set { intDia = value; }
        }
        public string Nombre
        {
            get { return strNombre; }
            set { strNombre = value; }
        }
        public string Cod_Dep
        {
            get { return strCod_Dep; }
            set { strCod_Dep = value; }
        }
        public string Cod_Prov
        {
            get { return strCod_Prov; }
            set { strCod_Prov = value; }
        }
        public string Cod_Dist
        {
            get { return strCod_Dist; }
            set { strCod_Dist = value; }
        }
        public string CentroPoblado
        {
            get { return strCentroPoblado; }
            set { strCentroPoblado = value; }
        }
        public int Cap_nivel
        {
            get { return intCap_nivel; }
            set { intCap_nivel = value; }
        }
        public int Acc_programa
        {
            get { return strAcc_programa; }
            set { strAcc_programa = value; }
        }
        public double Cap_duracion
        {
            get { return dblCap_duracion; }
            set { dblCap_duracion = value; }
        }
        public string Ubigeo
        {
            get { return strUbigeo; }
            set { strUbigeo = value; }
        }
        public DateTime Fecha_Inicio
        {
            get { return dtFecha_Inicio; }
            set { dtFecha_Inicio = value; }
        }
        public DateTime Fecha_Termino
        {
            get { return Fecha_Termino; }
            set { Fecha_Termino = value; }
        }
        public double Prog_anual
        {
            get { return dblProg_anual; }
            set { dblProg_anual = value; }
        }
        public double Prog_Mes
        {
            get { return dblProg_Mes; }
            set { dblProg_Mes = value; }
        }
        public double Inscritos_H
        {
            get { return dblInscritos_H; }
            set { dblInscritos_H = value; }
        }
        public double Aprobado_H
        {
            get { return dblAprobado_H; }
            set { dblAprobado_H = value; }
        }
        public double Retenido_H
        {
            get { return dblRetenido_H; }
            set { dblRetenido_H = value; }
        }
        public double Desaprobado_H
        {
            get { return dblDesaprobado_H; }
            set { dblDesaprobado_H = value; }
        }
        public double Inscritos_M
        {
            get { return dblInscritos_M; }
            set { dblInscritos_M = value; }
        }
        public double Aprobado_M
        {
            get { return dblAprobado_M; }
            set { dblAprobado_M = value; }
        }
        public double Retenido_M
        {
            get { return dblRetenido_M; }
            set { dblRetenido_M = value; }
        }
        public double Desaprobado_M
        {
            get { return dblDesaprobado_M; }
            set { dblDesaprobado_M = value; }
        }
        public double Importe_dol
        {
            get { return dblImporte_dol; }
            set { dblImporte_dol = value; }
        }
        public double Importe_sol
        {
            get { return dblImporte_sol; }
            set { dblImporte_sol = value; }
        }
        public double TipoCambio
        {
            get { return dblTipoCambio; }
            set { dblTipoCambio = value; }
        }
        public string Destino
        {
            get { return strDestino; }
            set { strDestino = value; }
        }
        public string TipoIfi
        {
            get { return strTipoIfi; }
            set { strTipoIfi = value; }
        }
        public string Ifi
        {
            get { return strIfi; }
            set { strIfi = value; }
        }
        public double Monto_Aprb2009
        {
            get { return dblMonto_Aprb2009; }
            set { dblMonto_Aprb2009 = value; }
        }
        public double Monto_Anticrisis
        {
            get { return dblMonto_Anticrisis; }
            set { dblMonto_Anticrisis = value; }
        }
        public int Moneda
        {
            get { return intMoneda; }
            set { intMoneda = value; }
        }
        public double MontoTot_Sol
        {
            get { return dblMontoTot_Sol; }
            set { dblMontoTot_Sol = value; }
        }
        public double MontoTot_Dol
        {
            get { return dblMontoTot_Dol; }
            set { dblMontoTot_Dol = value; }
        }
        public int Nro_Empl
        {
            get { return intNro_Empl; }
            set { intNro_Empl = value; }
        }
        public string Ambito
        {
            get { return strAmbito; }
            set { strAmbito = value; }
        }
        public int TipoDoc
        {
            get { return intTipoDoc; }
            set { intTipoDoc = value; }
        }
        public string Nro_doc
        {
            get { return strNro_doc; }
            set { strNro_doc = value; }
        }
        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }
        public DateTime FechaCorte
        {
            get { return dtFechaCorte; }
            set { dtFechaCorte = value; }
        }
        public int Estado
        {
            get { return intEstado; }
            set { intEstado = value; }
        }
        public int Zonal
        {
            get { return intZonal; }
            set { intZonal = value; }
        }
        public double Monto_Inv
        {
            get { return dblMonto_Inv; }
            set { dblMonto_Inv = value; }
        }
        public string Tipo_Reg
        {
            get { return strTipo_Reg; }
            set { strTipo_Reg = value; }
        }
        public int Cod_Financ
        {
            get { return intCodFinanc; }
            set { intCodFinanc = value; }
        }
        public int Cod_estado
        {
            get { return strCod_estado; }
            set { strCod_estado = value; }
        }
        public string Cod_proyBono
        {
            get { return intCod_proyBono; }
            set { intCod_proyBono = value; }
        }
        public double MontoFinan
        {
            get { return dblMontoFinan; }
            set { dblMontoFinan = value; }
        }
        #endregion
    }
}
