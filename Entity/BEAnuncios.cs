using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEAnuncios
    {
        #region Atributos
        private string strBuscar;
        private string strFechaIni;
        private string strFechaFin;
        private int intTipoAnuncio;
        private int intId_Anuncio;
        private string strTitulo;
        private string strbuscarCod_depa;
        private string strBuscarCod_prov;
        private string strBuscarCod_dist;
        private int intId_visita;
        private string strDes_visita;
        private string strFecha_visita;
        private string strCod_departamento;
        private string strCod_provincia;
        private string strCod_distrito;
        private int intId_acciones;
        private string strDes_acciones;
        private string strActores;
        private int intTipoAccion;
        private int intSNIP;
        private int intCodProgramaA;
        private int intCantidad;

        private int intId_usuario;
        private int intCod_tipo_usuario;
        private string strUsuario;
        private int intCod_perfil_usuario;

        #endregion

        #region Constructores

        #endregion

        #region Propiedades

        public string Buscar
        {
            get { return strBuscar; }
            set { strBuscar = value; }
        }
        public string FechaIni
        {
            get { return strFechaIni; }
            set { strFechaIni = value; }
        }
        public string FechaFin
        {
            get { return strFechaFin; }
            set { strFechaFin = value; }
        }
        public int TipoAnuncio
        {
            get { return intTipoAnuncio; }
            set { intTipoAnuncio = value; }
        }
        public int Id_Anuncio
        {
            get { return intId_Anuncio; }
            set { intId_Anuncio = value; }
        }
        public string Titulo
        {
            get { return strTitulo; }
            set { strTitulo = value; }
        }
        public string buscarCod_depa
        {
            get { return strbuscarCod_depa; }
            set { strbuscarCod_depa = value; }
        }
        public string BuscarCod_prov
        {
            get { return strBuscarCod_prov; }
            set { strBuscarCod_prov = value; }
        }
        public string BuscarCod_dist
        {
            get { return strBuscarCod_dist; }
            set { strBuscarCod_dist = value; }
        }
        public int Id_visita
        {
            get { return intId_visita; }
            set { intId_visita = value; }
        }
        public string Des_visita
        {
            get { return strDes_visita; }
            set { strDes_visita = value; }
        }
        public string Fecha_visita
        {
            get { return strFecha_visita; }
            set { strFecha_visita = value; }
        }
        public string Cod_departamento
        {
            get { return strCod_departamento; }
            set { strCod_departamento = value; }
        }
        public string Cod_provincia
        {
            get { return strCod_provincia; }
            set { strCod_provincia = value; }
        }
        public string Cod_distrito
        {
            get { return strCod_distrito; }
            set { strCod_distrito = value; }
        }
        public int Id_acciones
        {
            get { return intId_acciones; }
            set { intId_acciones = value; }
        }
        public string Des_acciones
        {
            get { return strDes_acciones; }
            set { strDes_acciones = value; }
        }
        public string Actores
        {
            get { return strActores; }
            set { strActores = value; }
        }
        public int TipoAccion
        {
            get { return intTipoAccion; }
            set { intTipoAccion = value; }
        }
        public int SNIP
        {
            get { return intSNIP; }
            set { intSNIP = value; }
        }
        public int CodProgramaA
        {
            get { return intCodProgramaA; }
            set { intCodProgramaA = value; }
        }
        public int Cantidad
        {
            get { return intCantidad; }
            set { intCantidad = value; }
        }


        public int Id_usuario
        {
            get { return intId_usuario; }
            set { intId_usuario = value; }
        }
        public int Cod_tipo_usuario
        {
            get { return intCod_tipo_usuario; }
            set { intCod_tipo_usuario = value; }
        }
        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }
       
        public int Cod_perfil_usuario
        {
            get { return intCod_perfil_usuario; }
            set { intCod_perfil_usuario = value; }
        }

        #endregion

    }
}
