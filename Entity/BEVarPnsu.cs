using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEVarPnsu
    {
        #region Atributos
        private Int64 intid_solicitudes; //
        public Int64 Id_Solicitudes
        {
            get { return intid_solicitudes; }
            set { intid_solicitudes = value; }
        }

        private int inta_captacion_nuc; //
        public int A_captacion_nuc
        {
            get { return inta_captacion_nuc; }
            set { inta_captacion_nuc = value; }
        }

        private double dbla_captacion_cacl;//
        public double A_captacion_cacl
        {
            get { return dbla_captacion_cacl; }
            set { dbla_captacion_cacl = value; }
        }

        private int inta_tratoPotable_num;

        public int A_tratoPotable_num
        {
            get { return inta_tratoPotable_num; }
            set { inta_tratoPotable_num = value; }
        }
        private double dbla_tratoPotable_cap;
        public double A_tratoPotable_cap
        {
            get { return dbla_tratoPotable_cap; }
            set { dbla_tratoPotable_cap = value; }
        }

        private int inta_conduccion_nuc;
        public int A_conduccion_nuc
        {
            get { return inta_conduccion_nuc; }
            set { inta_conduccion_nuc = value; }
        }

        private double dbla_conduccion_cacd;
        public double A_conduccion_cacd
        {
            get { return dbla_conduccion_cacd; }
            set { dbla_conduccion_cacd = value; }
        }
     
        private int inta_reservorio_nur;
        public int A_reservorio_nur
        {
            get { return inta_reservorio_nur; }
            set { inta_reservorio_nur = value; }
        }

        private double dbla_reservorio_carm;
        public double A_reservorio_carm
        {
            get { return dbla_reservorio_carm; }
            set { dbla_reservorio_carm = value; }
        }

        public double A_aduccion_caad
        {
            get { return dbla_aduccion_caad; }
            set { dbla_aduccion_caad = value; }
        }
        public int A_aduccion_nua
        {
            get { return inta_aduccion_nua; }
            set { inta_aduccion_nua = value; }
        }


        private int inta_aduccion_nua;
        private double dbla_aduccion_caad;
        

        private int inta_redes_nur;
        private double dbla_redes_card;
        private double dbla_redes_carl;

        public double A_redes_carl
        {
            get { return dbla_redes_carl; }
            set { dbla_redes_carl = value; }
        }

        public double A_redes_card
        {
            get { return dbla_redes_card; }
            set { dbla_redes_card = value; }
        }
        public int A_redes_nur
        {
            get { return inta_redes_nur; }
            set { inta_redes_nur = value; }
        }

        private int inta_conexion_nucn;
        private int inta_conexion_nucr;
        private int inta_conexion_nucp;

        public int A_conexion_nucp
        {
            get { return inta_conexion_nucp; }
            set { inta_conexion_nucp = value; }
        }
        public int A_conexion_nucr
        {
            get { return inta_conexion_nucr; }
            set { inta_conexion_nucr = value; }
        }

        public int A_conexion_nucn
        {
            get { return inta_conexion_nucn; }
            set { inta_conexion_nucn = value; }
        }

        private int a_impulsion_num;

        public int A_impulsion_num
        {
            get { return a_impulsion_num; }
            set { a_impulsion_num = value; }
        }
        private double a_impulsion_cap;

        public double A_impulsion_cap
        {
            get { return a_impulsion_cap; }
            set { a_impulsion_cap = value; }
        }
        private int a_estacion_num;

        public int A_estacion_num
        {
            get { return a_estacion_num; }
            set { a_estacion_num = value; }
        }


        private int ints_colectores_num;

        public int S_colectores_num
        {
            get { return ints_colectores_num; }
            set { ints_colectores_num = value; }
        }
        private double dbls_colectores_cap;

        public double S_colectores_cap
        {
            get { return dbls_colectores_cap; }
            set { dbls_colectores_cap = value; }
        }

        private int ints_agua_nua;
        private double dbls_agua_caad;
        private double dbls_agua_caal;


        public double S_agua_caal
        {
            get { return dbls_agua_caal; }
            set { dbls_agua_caal = value; }
        }

        public double S_agua_caad
        {
            get { return dbls_agua_caad; }
            set { dbls_agua_caad = value; }
        }

        public int S_agua_nua
        {
            get { return ints_agua_nua; }
            set { ints_agua_nua = value; }
        }

        private int ints_planta_nup;
        private double dbls_planta_capd;
        private double dbls_planta_capl;

        public double S_planta_capl
        {
            get { return dbls_planta_capl; }
            set { dbls_planta_capl = value; }
        }

        public double S_planta_capd
        {
            get { return dbls_planta_capd; }
            set { dbls_planta_capd = value; }
        }
        public int S_planta_nup
        {
            get { return ints_planta_nup; }
            set { ints_planta_nup = value; }
        }

        private int ints_conexion_nucn;
        private int ints_conexion_nucr;
        private int ints_conexion_nuclo;

        public int S_conexion_nuclo
        {
            get { return ints_conexion_nuclo; }
            set { ints_conexion_nuclo = value; }
        }
        public int S_conexion_nucr
        {
            get { return ints_conexion_nucr; }
            set { ints_conexion_nucr = value; }
        }


        public int S_conexion_nucn
        {
            get { return ints_conexion_nucn; }
            set { ints_conexion_nucn = value; }
        }

        private int ints_camara_num;

        public int S_camara_num
        {
            get { return ints_camara_num; }
            set { ints_camara_num = value; }
        }
        private double dbls_camara_cap;

        public double S_camara_cap
        {
            get { return dbls_camara_cap; }
            set { dbls_camara_cap = value; }
        }


        private int s_efluente_num;

        public int S_efluente_num
        {
            get { return s_efluente_num; }
            set { s_efluente_num = value; }
        }
        private double s_efluente_cap;

        public double S_efluente_cap
        {
            get { return s_efluente_cap; }
            set { s_efluente_cap = value; }
        }
        private int s_impulsion_num;

        public int S_impulsion_num
        {
            get { return s_impulsion_num; }
            set { s_impulsion_num = value; }
        }
        private double s_impulsion_cap;

        public double S_impulsion_cap
        {
            get { return s_impulsion_cap; }
            set { s_impulsion_cap = value; }
        }

        private int d_tuberia_num;
        public int D_tuberia_num
        {
            get { return d_tuberia_num; }
            set { d_tuberia_num = value; }
        }
        private double d_tuberia_cap;
        public double D_tuberia_cap
        {
            get { return d_tuberia_cap; }
            set { d_tuberia_cap = value; }
        }
        private int d_cuneta_num;

        public int D_cuneta_num
        {
            get { return d_cuneta_num; }
            set { d_cuneta_num = value; }
        }

        private double d_cuneta_cap;
        public double D_cuneta_cap
        {
            get { return d_cuneta_cap; }
            set { d_cuneta_cap = value; }
        }
        private int d_tormenta_num;
        public int D_tormenta_num
        {
            get { return d_tormenta_num; }
            set { d_tormenta_num = value; }
        }

        private int d_conexion_num;

        public int D_conexion_num
        {
            get { return d_conexion_num; }
            set { d_conexion_num = value; }
        }
        private int d_inspeccion_num;

        public int D_inspeccion_num
        {
            get { return d_inspeccion_num; }
            set { d_inspeccion_num = value; }
        }

        private int d_secundario_num;

        public int D_secundario_num
        {
            get { return d_secundario_num; }
            set { d_secundario_num = value; }
        }
        private double d_secundario_cap;

        public double D_secundario_cap
        {
            get { return d_secundario_cap; }
            set { d_secundario_cap = value; }
        }
        private int d_principal_num;

        public int D_principal_num
        {
            get { return d_principal_num; }
            set { d_principal_num = value; }
        }
        private double d_principal_cap;

        public double D_principal_cap
        {
            get { return d_principal_cap; }
            set { d_principal_cap = value; }
        }

        private double a_estacion_costo;

        public double A_estacion_costo
        {
            get { return a_estacion_costo; }
            set { a_estacion_costo = value; }
        }
        private double a_captacion_costo;

        public double A_captacion_costo
        {
            get { return a_captacion_costo; }
            set { a_captacion_costo = value; }
        }
        private double a_trataPotable_costo;

        public double A_trataPotable_costo
        {
            get { return a_trataPotable_costo; }
            set { a_trataPotable_costo = value; }
        }
        private double a_conduccion_costo;

        public double A_conduccion_costo
        {
            get { return a_conduccion_costo; }
            set { a_conduccion_costo = value; }
        }
        private double a_reservorio_costo;

        public double A_reservorio_costo
        {
            get { return a_reservorio_costo; }
            set { a_reservorio_costo = value; }
        }
        private double a_aduccion_costo;

        public double A_aduccion_costo
        {
            get { return a_aduccion_costo; }
            set { a_aduccion_costo = value; }
        }
        private double a_redes_costo;

        public double A_redes_costo
        {
            get { return a_redes_costo; }
            set { a_redes_costo = value; }
        }
        private double a_conexion_nucnCosto;

        public double A_conexion_nucnCosto
        {
            get { return a_conexion_nucnCosto; }
            set { a_conexion_nucnCosto = value; }
        }
        private double a_conexion_nucrCosto;

        public double A_conexion_nucrCosto
        {
            get { return a_conexion_nucrCosto; }
            set { a_conexion_nucrCosto = value; }
        }
        private double a_conexion_nucpCosto;

        public double A_conexion_nucpCosto
        {
            get { return a_conexion_nucpCosto; }
            set { a_conexion_nucpCosto = value; }
        }
        private double a_impulsion_costo;

        public double A_impulsion_costo
        {
            get { return a_impulsion_costo; }
            set { a_impulsion_costo = value; }
        }
        private double s_colectores_costo;

        public double S_colectores_costo
        {
            get { return s_colectores_costo; }
            set { s_colectores_costo = value; }
        }
        private double s_agua_costo;

        public double S_agua_costo
        {
            get { return s_agua_costo; }
            set { s_agua_costo = value; }
        }
        private double s_planta_costo;

        public double S_planta_costo
        {
            get { return s_planta_costo; }
            set { s_planta_costo = value; }
        }
        private double s_conexion_nucnCosto;

        public double S_conexion_nucnCosto
        {
            get { return s_conexion_nucnCosto; }
            set { s_conexion_nucnCosto = value; }
        }
        private double s_conexion_nucrCosto;

        public double S_conexion_nucrCosto
        {
            get { return s_conexion_nucrCosto; }
            set { s_conexion_nucrCosto = value; }
        }
        private double s_conexion_nucloCosto;

        public double S_conexion_nucloCosto
        {
            get { return s_conexion_nucloCosto; }
            set { s_conexion_nucloCosto = value; }
        }

        private double s_camara_costo;

        public double S_camara_costo
        {
            get { return s_camara_costo; }
            set { s_camara_costo = value; }
        }
        private double s_efluente_costo;

        public double S_efluente_costo
        {
            get { return s_efluente_costo; }
            set { s_efluente_costo = value; }
        }
        private double s_impulsion_costo;

        public double S_impulsion_costo
        {
            get { return s_impulsion_costo; }
            set { s_impulsion_costo = value; }
        }
        private double d_tuberia_costo;

        public double D_tuberia_costo
        {
            get { return d_tuberia_costo; }
            set { d_tuberia_costo = value; }
        }
        private double d_cuneta_costo;

        public double D_cuneta_costo
        {
            get { return d_cuneta_costo; }
            set { d_cuneta_costo = value; }
        }
        private double d_tormenta_costo;

        public double D_tormenta_costo
        {
            get { return d_tormenta_costo; }
            set { d_tormenta_costo = value; }
        }
        private double d_conexion_costo;

        public double D_conexion_costo
        {
            get { return d_conexion_costo; }
            set { d_conexion_costo = value; }
        }
        private double d_inspeccion_costo;

        public double D_inspeccion_costo
        {
            get { return d_inspeccion_costo; }
            set { d_inspeccion_costo = value; }
        }
        private double d_secundarios_costo;

        public double D_secundarios_costo
        {
            get { return d_secundarios_costo; }
            set { d_secundarios_costo = value; }
        }
        private double d_principal_costo;

        public double D_principal_costo
        {
            get { return d_principal_costo; }
            set { d_principal_costo = value; }
        }

        private int v_cant;

        public int V_cant
        {
            get { return v_cant; }
            set { v_cant = value; }
        }
        private double v_costo;

        public double V_costo
        {
            get { return v_costo; }
            set { v_costo = value; }
        }

       

        #endregion


    }
}
