﻿namespace Entity
{
    public class BE_MON_Menu
    {
        public int idProyecto { get; set; }
        public int idTipoFinanciamiento { get; set; }
        public string tipoFinanciamiento { get; set; }
        public int flagUltimo { get; set; }
        public int idModalidadFinanciamiento { get; set; }
    }
}

