﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
   public class BE_MON_Liquidacion
    {
       private int _id_proyecto;
        private int _idTipoFormato;
        private string _urlDocumento;
        private int _tipoFinanciamiento;
       private string _resolucion;
       private DateTime _fecha;
       private string _monto;
       private string _saldoDevolver;
       private string _urlLiquidacion;
       private string _urlActaCierre;

       private string _flagCapacidad;
       private string _comentarioCapacidad;
       private string _unidadReceptora;

       private string _flagEduacion;
       private string _flagIntraconexion;
       private string _urlDocSostenibilidad;

       private string _otrosMontos;
       private string _montoAdicional;
       private string _urlDocDevolucion;

       private int _tipoReceptora;

       private string _resolucionSupervision;

        private int _id_tipoEstadoSaldoEntidad;
        private int _id_tipoEstadoSaldoEmpresa;
        private int _id_tipoEstadoSaldoEntidadSupervision;
        private int _id_tipoEstadoSaldoEmpresaSupervision;

        private string _urlDeclaracionViabilidad;
        private string _urlAprobacionExpediente;

        private string _flagOperatividad;

        public string ResolucionSupervision
       {
           get { return _resolucionSupervision; }
           set { _resolucionSupervision = value; }
       }
       private DateTime _fechaLiqSupervision;

       public string FechaLiqSupervision
       {
           get { return _fechaLiqSupervision.ToString("dd/MM/yyyy"); }
           set { _fechaLiqSupervision = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaLiqSupervision
       {
           get { return _fechaLiqSupervision; }
           set { _fechaLiqSupervision = value; }
       }

       private string _montoLiqSupervision;

       public string MontoLiqSupervision
       {
           get { return _montoLiqSupervision; }
           set { _montoLiqSupervision = value; }
       }

       private string _resolucionObraComplementaria;

       public string ResolucionObraComplementaria
       {
           get { return _resolucionObraComplementaria; }
           set { _resolucionObraComplementaria = value; }
       }
       private DateTime _fechaLiqObraComplem;

       public string FechaLiqObraComplem
       {
           get { return _fechaLiqObraComplem.ToString("dd/MM/yyyy"); }
           set { _fechaLiqObraComplem = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaLiqObraComplem
       {
           get { return _fechaLiqObraComplem; }
           set { _fechaLiqObraComplem = value; }
       }

       private string _montoLiqObraComplementaria;

       public string MontoLiqObraComplementaria
       {
           get { return _montoLiqObraComplementaria; }
           set { _montoLiqObraComplementaria = value; }
       }

       private string _urlLiquidacionSupervision;

       public string UrlLiquidacionSupervision
       {
           get { return _urlLiquidacionSupervision; }
           set { _urlLiquidacionSupervision = value; }
       }
       private string _urlLiquidacionObraComplem;

       public string UrlLiquidacionObraComplem
       {
           get { return _urlLiquidacionObraComplem; }
           set { _urlLiquidacionObraComplem = value; }
       }

       public int tipoReceptora
       {
           get { return _tipoReceptora; }
           set { _tipoReceptora = value; }
       }
       public string otrosMontos
       {
           get { return _otrosMontos; }
           set { _otrosMontos = value; }
       }
       public string montoAdicional
       {
           get { return _montoAdicional; }
           set { _montoAdicional = value; }
       }
       public string urlDocDevolucion
       {
           get { return _urlDocDevolucion; }
           set { _urlDocDevolucion = value; }
       }
       private int _id_usuario;

       public string flagEduacion
       {
           get { return _flagEduacion; }
           set { _flagEduacion = value; }
       }
       public string flagIntraconexion
       {
           get { return _flagIntraconexion; }
           set { _flagIntraconexion = value; }
       }
       public string urlDocSostenibilidad
       {
           get { return _urlDocSostenibilidad; }
           set { _urlDocSostenibilidad = value; }
       }
       public int id_proyecto
       {
           get { return _id_proyecto; }
           set { _id_proyecto = value; }
       }

       public int tipoFinanciamiento
       {
           get { return _tipoFinanciamiento; }
           set { _tipoFinanciamiento = value; }
       }

       public string resolucion
       {
           get { return _resolucion; }
           set { _resolucion = value; }
       }
       private string _resolucionAprobacion;
       public string resolucionAprobacion
       {
           get { return _resolucionAprobacion; }
           set { _resolucionAprobacion = value; }
       }

       public string fecha
       {
           get { return _fecha.ToString("dd/MM/yyyy"); }
           set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_Fecha
       {
           get { return _fecha; }
           set { _fecha = value; }
       }

       private string _montoTransferencia;

       public string montoTransferencia
       {
           get { return _montoTransferencia; }
           set { _montoTransferencia = value; }
       }
       public string monto
       {
           get { return _monto; }
           set { _monto = value; }
       }
       public string saldoDevolver
       {
           get { return _saldoDevolver; }
           set { _saldoDevolver = value; }
       }
       public string urlLiquidacion
       {
           get { return _urlLiquidacion; }
           set { _urlLiquidacion = value; }
       }
       public string urlActaCierre
       {
           get { return _urlActaCierre; }
           set { _urlActaCierre = value; }
       }

       public string flagCapacidad
       {
           get { return _flagCapacidad; }
           set { _flagCapacidad = value; }
       }

       public string comentarioCapacidad
       {
           get { return _comentarioCapacidad; }
           set { _comentarioCapacidad = value; }
       }

       public int id_usuario
       {
           get { return _id_usuario; }
           set { _id_usuario = value; }
       }


       public string unidadReceptora
       {
           get { return _unidadReceptora; }
           set { _unidadReceptora = value; }
       }

       private DateTime _fechaSuscripcionTerminacionObra;

       public string fechaSuscripcionTerminacionObra
       {
           get { return _fechaSuscripcionTerminacionObra.ToString("dd/MM/yyyy"); }
           set { _fechaSuscripcionTerminacionObra = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaSuscripcionTerminacionObra
       {
           get { return _fechaSuscripcionTerminacionObra; }
           set { _fechaSuscripcionTerminacionObra = value; }
       }

       private DateTime _fechaDocumentacion;

       public string fechaDocumentacion
       {
           get { return _fechaDocumentacion.ToString("dd/MM/yyyy"); }
           set { _fechaDocumentacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaDocumentacion
       {
           get { return _fechaDocumentacion; }
           set { _fechaDocumentacion = value; }
       }

       private DateTime _fechaFirma;

       public string fechaFirma
       {
           get { return _fechaFirma.ToString("dd/MM/yyyy"); }
           set { _fechaFirma = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaFirma
       {
           get { return _fechaFirma; }
           set { _fechaFirma = value; }
       }

       private DateTime _fechaTerminoObraInicioLiquidacion;

       public string fechaTerminoObraInicioLiquidacion
       {
           get { return _fechaTerminoObraInicioLiquidacion.ToString("dd/MM/yyyy"); }
           set { _fechaTerminoObraInicioLiquidacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaTerminoObraInicioLiquidacion
       {
           get { return _fechaTerminoObraInicioLiquidacion; }
           set { _fechaTerminoObraInicioLiquidacion = value; }
       }

       private DateTime _fechaSuscripcion;

       public string fechaSuscripcion
       {
           get { return _fechaSuscripcion.ToString("dd/MM/yyyy"); }
           set { _fechaSuscripcion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaSuscripcion
       {
           get { return _fechaSuscripcion; }
           set { _fechaSuscripcion = value; }
       }

       private DateTime _fechaBajaContable;

       public string fechaBajaContable
       {
           get { return _fechaBajaContable.ToString("dd/MM/yyyy"); }
           set { _fechaBajaContable = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaBajaContable
       {
           get { return _fechaBajaContable; }
           set { _fechaBajaContable = value; }
       }

       private DateTime _fechaCumplimiento;

       public string fechaCumplimiento
       {
           get { return _fechaCumplimiento.ToString("dd/MM/yyyy"); }
           set { _fechaCumplimiento = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaCumplimiento
       {
           get { return _fechaCumplimiento; }
           set { _fechaCumplimiento = value; }
       }

       private DateTime _fechaProvisional;

       public string fechaProvisional
       {
           get { return _fechaProvisional.ToString("dd/MM/yyyy"); }
           set { _fechaProvisional = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaProvisional
       {
           get { return _fechaProvisional; }
           set { _fechaProvisional = value; }
       }

       private DateTime _fechaFinal;

       public string fechaFinal
       {
           get { return _fechaFinal.ToString("dd/MM/yyyy"); }
           set { _fechaFinal = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaFinal
       {
           get { return _fechaFinal; }
           set { _fechaFinal = value; }
       }

       private DateTime _fechaRecepcion;

       public string fechaRecepcion
       {
           get { return _fechaRecepcion.ToString("dd/MM/yyyy"); }
           set { _fechaRecepcion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaRecepcion
       {
           get { return _fechaRecepcion; }
           set { _fechaRecepcion = value; }
       }

       private string _urlDocComunicacion;

       public string urlDocComunicacion
       {
           get { return _urlDocComunicacion; }
           set { _urlDocComunicacion = value; }
       }
       private string _urlDocDesignacion;

       public string urlDocDesignacion
       {
           get { return _urlDocDesignacion; }
           set { _urlDocDesignacion = value; }
       }

       private string _urlDocCuadernoObra;

       public string urlDocCuadernoObra
       {
           get { return _urlDocCuadernoObra; }
           set { _urlDocCuadernoObra = value; }
       }

       private DateTime _fechaPresentacion;

       public string fechaPresentacion
       {
           get { return _fechaPresentacion.ToString("dd/MM/yyyy"); }
           set { _fechaPresentacion = DateTime.ParseExact(value, "dd/MM/yyyy", null);  }
       }

       public DateTime Date_fechaPresentacion
       {
           get { return _fechaPresentacion; }
           set { _fechaPresentacion = value; }
       }

       private DateTime _fechaAprobacion;

       public string fechaAprobacion
       {
           get { return _fechaAprobacion.ToString("dd/MM/yyyy"); }
           set { _fechaAprobacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaAprobacion
       {
           get { return _fechaAprobacion; }
           set { _fechaAprobacion = value; }
       }

       private DateTime _fechaPresentacionSupervision;

       public string fechaPresentacionSupervision
       {
           get { return _fechaPresentacionSupervision.ToString("dd/MM/yyyy"); }
           set { _fechaPresentacionSupervision = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaPresentacionSupervision
       {
           get { return _fechaPresentacionSupervision; }
           set { _fechaPresentacionSupervision = value; }
       }

       private DateTime _fechaAprobacionSupervision;

       public string fechaAprobacionSupervision
       {
           get { return _fechaAprobacionSupervision.ToString("dd/MM/yyyy"); }
           set { _fechaAprobacionSupervision = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fechaAprobacionSupervision
       {
           get { return _fechaAprobacionSupervision; }
           set { _fechaAprobacionSupervision = value; }
       }

       private string _saldoEntidad;

       public string saldoEntidad
       {
           get { return _saldoEntidad; }
           set { _saldoEntidad = value; }
       }
       private string _saldoEmpresa;

       public string saldoEmpresa
       {
           get { return _saldoEmpresa; }
           set { _saldoEmpresa = value; }
       }
       private string _saldoEntidadSupervision;

       public string saldoEntidadSupervision
       {
           get { return _saldoEntidadSupervision; }
           set { _saldoEntidadSupervision = value; }
       }
       private string _saldoEmpresaSupervision;

       public string saldoEmpresaSupervision
       {
           get { return _saldoEmpresaSupervision; }
           set { _saldoEmpresaSupervision = value; }
       }

        public int id_tipoEstadoSaldoEntidad
        {
            get
            {
                return _id_tipoEstadoSaldoEntidad;
            }

            set
            {
                _id_tipoEstadoSaldoEntidad = value;
            }
        }

        public int id_tipoEstadoSaldoEmpresa
        {
            get
            {
                return _id_tipoEstadoSaldoEmpresa;
            }

            set
            {
                _id_tipoEstadoSaldoEmpresa = value;
            }
        }

        public int id_tipoEstadoSaldoEntidadSupervision
        {
            get
            {
                return _id_tipoEstadoSaldoEntidadSupervision;
            }

            set
            {
                _id_tipoEstadoSaldoEntidadSupervision = value;
            }
        }

        public int id_tipoEstadoSaldoEmpresaSupervision
        {
            get
            {
                return _id_tipoEstadoSaldoEmpresaSupervision;
            }

            set
            {
                _id_tipoEstadoSaldoEmpresaSupervision = value;
            }
        }

        public string urlDeclaracionViabilidad
        {
            get
            {
                return _urlDeclaracionViabilidad;
            }

            set
            {
                _urlDeclaracionViabilidad = value;
            }
        }

        public string urlAprobacionExpediente
        {
            get
            {
                return _urlAprobacionExpediente;
            }

            set
            {
                _urlAprobacionExpediente = value;
            }
        }

        public int idTipoFormato { get => _idTipoFormato; set => _idTipoFormato = value; }
        public string urlDocumento { get => _urlDocumento; set => _urlDocumento = value; }
        public string flagOperatividad { get => _flagOperatividad; set => _flagOperatividad = value; }
    }

   public class BE_MON_Calidad
   {
       private int _id_proyecto;
       private string _evaluacionFinal;
       private string _deficiencias;
       private DateTime _fecha;
       private string _tipoPrueba;
       private string _estado;
       private string _resultadoFinal;
       private string _urlDoc;

       private int _id_prueba_verificada;

       private int _id_usuario;




       public int id_proyecto
       {
           get { return _id_proyecto; }
           set { _id_proyecto = value; }
       }

       public int id_prueba_verificada
       {
           get { return _id_prueba_verificada; }
           set { _id_prueba_verificada = value; }
       }

       public string evaluacionFinal
       {
           get { return _evaluacionFinal; }
           set { _evaluacionFinal = value; }
       }

       public string deficiencias
       {
           get { return _deficiencias; }
           set { _deficiencias = value; }
       }

       public string fecha
       {
           get { return _fecha.ToString("dd/MM/yyyy"); }
           set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_Fecha
       {
           get { return _fecha; }
           set { _fecha = value; }
       }

       public string tipoPrueba
       {
           get { return _tipoPrueba; }
           set { _tipoPrueba = value; }
       }

       public string estado
       {
           get { return _estado; }
           set { _estado = value; }
       }

       public string resultadoFinal
       {
           get { return _resultadoFinal; }
           set { _resultadoFinal = value; }
       }
       public string urlDoc
       {
           get { return _urlDoc; }
           set { _urlDoc = value; }
       }
       public int id_usuario
       {
           get { return _id_usuario; }
           set { _id_usuario = value; }
       }


       
   }
}
