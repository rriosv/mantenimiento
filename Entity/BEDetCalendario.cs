using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEDet_Calendario
    {
        #region Atributos
        private int intId_DetCalendario;
        private int intId_Calendario;
        private int intId_Acciones;
        private int intCod_TipoCal;
        private int intCod_TipoMonto;
        private int intCod_Fase;
        private int intId_Unidad;
        private int intFinancia;

        private int intAnio;
        private double dblEnero;
        private double dblFebrero;
        private double dblMarzo;
        private double dblAbril;
        private double dblMayo;
        private double dblJunio;
        private double dblJulio;
        private double dblAgosto;
        private double dblSeptiembre;
        private double dblOctubre;
        private double dblNoviembre;
        private double dblDiciembre;
        private string strUsuario;
      
        #endregion

        #region Constructores

        #endregion

        #region Propiedades

        public int Id_DetCalendario
        {
            get { return intId_DetCalendario; }
            set { intId_DetCalendario = value; }
        }
        public int Id_Calendario
        {
            get { return intId_Calendario; }
            set { intId_Calendario = value; }
        }
        public int Id_Acciones
        {
            get { return intId_Acciones; }
            set { intId_Acciones = value; }
        }
        public int Cod_TipoCal
        {
            get { return intCod_TipoCal; }
            set { intCod_TipoCal = value; }
        }
        public int Cod_TipoMonto
        {
            get { return intCod_TipoMonto; }
            set { intCod_TipoMonto = value; }
        }
        public int Cod_Fase
        {
            get { return intCod_Fase; }
            set { intCod_Fase = value; }
        }
        public int Id_Unidad
        {
            get { return intId_Unidad; }
            set { intId_Unidad = value; }
        }
        public int Id_Financiamiento
        {
            get { return intFinancia; }
            set { intFinancia = value; }
        }
        public int Anio
        {
            get { return intAnio; }
            set { intAnio = value; }
        }
        public double Enero
        {
            get { return dblEnero; }
            set { dblEnero = value; }
        }
        public double Febrero
        {
            get { return dblFebrero; }
            set { dblFebrero = value; }
        }
        public double Marzo
        {
            get { return dblMarzo; }
            set { dblMarzo = value; }
        }
        public double Abril
        {
            get { return dblAbril; }
            set { dblAbril = value; }
        }
        public double Mayo
        {
            get { return dblMayo; }
            set { dblMayo = value; }
        }
        public double Junio
        {
            get { return dblJunio; }
            set { dblJunio = value; }
        }
        public double Julio
        {
            get { return dblJulio; }
            set { dblJulio = value; }
        }
        public double Agosto
        {
            get { return dblAgosto; }
            set { dblAgosto = value; }
        }
        public double Septiembre
        {
            get { return dblSeptiembre; }
            set { dblSeptiembre = value; }
        }
        public double Octubre
        {
            get { return dblOctubre; }
            set { dblOctubre = value; }
        }
        public double Noviembre
        {
            get { return dblNoviembre; }
            set { dblNoviembre = value; }
        }
        public double Diciembre
        {
            get { return dblDiciembre; }
            set { dblDiciembre = value; }
        }
        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }
        
        #endregion
    }
}

