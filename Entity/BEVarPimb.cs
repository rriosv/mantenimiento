using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEVarPimb
    {
        private Int64 id_Solicitudes;

        public Int64 Id_Solicitudes
        {
            get { return id_Solicitudes; }
            set { id_Solicitudes = value; }
        }
        private double p_concreto_m2;

        public double P_concreto_m2
        {
            get { return p_concreto_m2; }
            set { p_concreto_m2 = value; }
        }
        private double p_concreto_co;

        public double P_concreto_co
        {
            get { return p_concreto_co; }
            set { p_concreto_co = value; }
        }
        private double p_asfalto_m2;

        public double P_asfalto_m2
        {
            get { return p_asfalto_m2; }
            set { p_asfalto_m2 = value; }
        }
        private double p_asfalto_co;

        public double P_asfalto_co
        {
            get { return p_asfalto_co; }
            set { p_asfalto_co = value; }
        }
        private double p_emboquillado_m2;

        public double P_emboquillado_m2
        {
            get { return p_emboquillado_m2; }
            set { p_emboquillado_m2 = value; }
        }
        private double p_emboquillado_co;

        public double P_emboquillado_co
        {
            get { return p_emboquillado_co; }
            set { p_emboquillado_co = value; }
        }
        private double p_adoquinado_m2;

        public double P_adoquinado_m2
        {
            get { return p_adoquinado_m2; }
            set { p_adoquinado_m2 = value; }
        }
        private double p_adoquinado_co;

        public double P_adoquinado_co
        {
            get { return p_adoquinado_co; }
            set { p_adoquinado_co = value; }
        }

        private double v_concreto_m2;

        public double V_concreto_m2
        {
            get { return v_concreto_m2; }
            set { v_concreto_m2 = value; }
        }
        private double v_concreto_co;

        public double V_concreto_co
        {
            get { return v_concreto_co; }
            set { v_concreto_co = value; }
        }
        private double v_adoquinado_m2;

        public double V_adoquinado_m2
        {
            get { return v_adoquinado_m2; }
            set { v_adoquinado_m2 = value; }
        }
        private double v_adoquinado_co;

        public double V_adoquinado_co
        {
            get { return v_adoquinado_co; }
            set { v_adoquinado_co = value; }
        }
        private double v_piedra_m2;

        public double V_piedra_m2
        {
            get { return v_piedra_m2; }
            set { v_piedra_m2 = value; }
        }
        private double v_piedra_co;

        public double V_piedra_co
        {
            get { return v_piedra_co; }
            set { v_piedra_co = value; }
        }
        private double v_emboquillado_m2;

        public double V_emboquillado_m2
        {
            get { return v_emboquillado_m2; }
            set { v_emboquillado_m2 = value; }
        }
        private double v_emboquillado_co;

        public double V_emboquillado_co
        {
            get { return v_emboquillado_co; }
            set { v_emboquillado_co = value; }
        }

        private double d_concreto_ml;

        public double D_concreto_ml
        {
            get { return d_concreto_ml; }
            set { d_concreto_ml = value; }
        }
        private double d_concreto_co;

        public double D_concreto_co
        {
            get { return d_concreto_co; }
            set { d_concreto_co = value; }
        }
        private double d_piedra_ml;

        public double D_piedra_ml
        {
            get { return d_piedra_ml; }
            set { d_piedra_ml = value; }
        }
        private double d_piedra_co;

        public double D_piedra_co
        {
            get { return d_piedra_co; }
            set { d_piedra_co = value; }
        }

        private double o_muro_m2;

        public double O_muro_m2
        {
            get { return o_muro_m2; }
            set { o_muro_m2 = value; }
        }
        private double o_muro_co;

        public double O_muro_co
        {
            get { return o_muro_co; }
            set { o_muro_co = value; }
        }
        private double o_ponton_m2;

        public double O_ponton_m2
        {
            get { return o_ponton_m2; }
            set { o_ponton_m2 = value; }
        }
        private double o_ponton_co;

        public double O_ponton_co
        {
            get { return o_ponton_co; }
            set { o_ponton_co = value; }
        }
        private double o_bermas_m2;

        public double O_bermas_m2
        {
            get { return o_bermas_m2; }
            set { o_bermas_m2 = value; }
        }
        private double o_bermas_co;

        public double O_bermas_co
        {
            get { return o_bermas_co; }
            set { o_bermas_co = value; }
        }
        private double o_verdes_m2;

        public double O_verdes_m2
        {
            get { return o_verdes_m2; }
            set { o_verdes_m2 = value; }
        }
        private double o_verdes_co;

        public double O_verdes_co
        {
            get { return o_verdes_co; }
            set { o_verdes_co = value; }
        }

        private int o_jardineras_u;

        public int O_jardineras_u
        {
            get { return o_jardineras_u; }
            set { o_jardineras_u = value; }
        }
        private double o_jardineras_co;

        public double O_jardineras_co
        {
            get { return o_jardineras_co; }
            set { o_jardineras_co = value; }
        }
        private int o_plantones_u;

        public int O_plantones_u
        {
            get { return o_plantones_u; }
            set { o_plantones_u = value; }
        }
        private double o_plantones_co;

        public double O_plantones_co
        {
            get { return o_plantones_co; }
            set { o_plantones_co = value; }
        }
        private int o_sanitarias_u;

        public int O_sanitarias_u
        {
            get { return o_sanitarias_u; }
            set { o_sanitarias_u = value; }
        }
        private double o_sanitarias_co;

        public double O_sanitarias_co
        {
            get { return o_sanitarias_co; }
            set { o_sanitarias_co = value; }
        }
        private int o_electricas_u;

        public int O_electricas_u
        {
            get { return o_electricas_u; }
            set { o_electricas_u = value; }
        }
        private double o_electricas_co;

        public double O_electricas_co
        {
            get { return o_electricas_co; }
            set { o_electricas_co = value; }
        }

        private int o_bancas_u;

        public int O_bancas_u
        {
            get { return o_bancas_u; }
            set { o_bancas_u = value; }
        }
        private double o_bancas_co;

        public double O_bancas_co
        {
            get { return o_bancas_co; }
            set { o_bancas_co = value; }
        }
        private int o_basureros_u;

        public int O_basureros_u
        {
            get { return o_basureros_u; }
            set { o_basureros_u = value; }
        }
        private double o_basureros_co;

        public double O_basureros_co
        {
            get { return o_basureros_co; }
            set { o_basureros_co = value; }
        }
        private int o_pergolas_u;

        public int O_pergolas_u
        {
            get { return o_pergolas_u; }
            set { o_pergolas_u = value; }
        }
        private double o_pergolas_co;

        public double O_pergolas_co
        {
            get { return o_pergolas_co; }
            set { o_pergolas_co = value; }
        }
        private int o_glorietas_u;

        public int O_glorietas_u
        {
            get { return o_glorietas_u; }
            set { o_glorietas_u = value; }
        }
        private double o_glorietas_co;

        public double O_glorietas_co
        {
            get { return o_glorietas_co; }
            set { o_glorietas_co = value; }
        }
        private double o_otros_u;

        public double O_otros_u
        {
            get { return o_otros_u; }
            set { o_otros_u = value; }
        }
        private double o_otros_co;

        public double O_otros_co
        {
            get { return o_otros_co; }
            set { o_otros_co = value; }
        }

        private string _observaciones;

        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; }
        }

        private int _id_usuario;

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        private double _l_losa_m2;
        public double l_losa_m2
        {
            get { return _l_losa_m2; }
            set { _l_losa_m2 = value; }
        }

        public double l_losa_co
        {
            get { return _l_losa_co; }
            set { _l_losa_co = value; }
        }

        public double l_tribuna_m2
        {
            get { return _l_tribuna_m2; }
            set { _l_tribuna_m2 = value; }
        }

        public double l_tribuna_co
        {
            get { return _l_tribuna_co; }
            set { _l_tribuna_co = value; }
        }

        public double l_administracion_m2
        {
            get { return _l_administracion_m2; }
            set { _l_administracion_m2 = value; }
        }

        public double l_administracion_co
        {
            get { return _l_administracion_co; }
            set { _l_administracion_co = value; }
        }

        public double l_cerco_ml
        {
            get { return _l_cerco_ml; }
            set { _l_cerco_ml = value; }
        }

        public double l_cerco_co
        {
            get { return _l_cerco_co; }
            set { _l_cerco_co = value; }
        }

        public double l_iluminacion_u
        {
            get { return _l_iluminacion_u; }
            set { _l_iluminacion_u = value; }
        }

        public double l_iluminacion_co
        {
            get { return _l_iluminacion_co; }
            set { _l_iluminacion_co = value; }
        }

        public double c_puestas_m2
        {
            get { return _c_puestas_m2; }
            set { _c_puestas_m2 = value; }
        }

        public double c_puestas_co
        {
            get { return _c_puestas_co; }
            set { _c_puestas_co = value; }
        }

        public double c_administracion_m2
        {
            get { return _c_administracion_m2; }
            set { _c_administracion_m2 = value; }
        }

        public double c_administracion_co
        {
            get { return _c_administracion_co; }
            set { _c_administracion_co = value; }
        }

        public double c_cerco_ml
        {
            get { return _c_cerco_ml; }
            set { _c_cerco_ml = value; }
        }

        public double c_cerco_co
        {
            get { return _c_cerco_co; }
            set { _c_cerco_co = value; }
        }

        public double o_puente_m2
        {
            get { return _o_puente_m2; }
            set { _o_puente_m2 = value; }
        }

        public double o_puente_co
        {
            get { return _o_puente_co; }
            set { _o_puente_co = value; }
        }

        private double _l_losa_co;
        private double _l_tribuna_m2;
        private double _l_tribuna_co;
        private double _l_administracion_m2;
        private double _l_administracion_co;
        private double _l_cerco_ml;
        private double _l_cerco_co;
        private double _l_iluminacion_u;
        private double _l_iluminacion_co;

        private double _c_puestas_m2;
        private double _c_puestas_co;
        private double _c_administracion_m2;
        private double _c_administracion_co;
        private double _c_cerco_ml;
        private double _c_cerco_co;

        private double _o_puente_m2;
        private double _o_puente_co;


    }
}
