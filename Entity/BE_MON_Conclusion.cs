﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
   public class BE_MON_Conclusion
    {
        private int _id_proyecto;
        private int _tipoFinanciamiento;
        private int _id_usuario;

        private string _conclusion;
        private string _observacion;
        private string _urlDoc;


        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

        public int tipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        public string conclusion
        {
            get { return _conclusion; }
            set { _conclusion = value; }
        }

        public string observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public string urlDoc
          {
            get { return _urlDoc; }
            set { _urlDoc = value; }
        }
    }
}
