﻿using System;

namespace Entity
{
    public class BE_MON_CONVENIO_DIRECTA
    {
        public int nro { get; set; }
        public int id_proyecto { get; set; }
        public int id_convenioDirecta { get; set; }
        public int iTipoConvenio { get; set; }
        public string tipo { get; set; }
        public string detalle { get; set; }
        public string vFechaConvenio { get; set; }
        public string entidadFirmante { get; set; }
        public string objetoConvenio { get; set; }
        public string vFechaVencimiento { get; set; }
        public string usuario { get; set; }
        public int id_usuario { get; set; }
        public string fecha_update { get; set; }
    }
}
