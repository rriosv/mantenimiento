﻿using System;

namespace Entity
{
   public class BE_Emblematicos
   {
       #region Tables Memebers
       private long _id_actividad;

       
       public long id_actividad
       {
           get
           {
               try
               {
                   return _id_actividad;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_actividad", err);
               }
           }
           set
           {
               try
               {
                   _id_actividad = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_actividad", err);
               }
           }
       }

       private long _id_proyecto;

       public long id_proyecto
       {
           get
           {
               try
               {
                   return _id_proyecto;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_proyecto", err);
               }
           }
           set
           {
               try
               {
                   _id_proyecto = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_proyecto", err);
               }
           }
       }

       private string _componente;

     
       public string componente
       {
           get
           {
               try
               {
                   return _componente;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting componente", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 200))
                   {
                       _componente = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting componente, Length of value is to long. Maximum Length: 200");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting componente", err);
               }
           }
       }

       private string _NewComponente;

       public string NewComponente
       {
           get { return _NewComponente; }
           set { _NewComponente = value; }
       }
       private string _nombreActividad;

      
       public string nombreActividad
       {
           get
           {
               return _nombreActividad;
              
           }
           set
           {
               _nombreActividad = value;
           }
       }

       private System.DateTime _fechaInicio;

     
       public System.DateTime fechaInicio
       {
           get
           {
               try
               {
                   return _fechaInicio;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting fechaInicio", err);
               }
           }
           set
           {
               try
               {
                   _fechaInicio = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting fechaInicio", err);
               }
           }
       }

       private DateTime? _fechaInformacion;


       public DateTime? fechaInformacion
       {
           get
           {
               try
               {
                   return _fechaInformacion;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting _fechaInformacion", err);
               }
           }
           set
           {
               try
               {
                   _fechaInformacion = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting _fechaInformacion", err);
               }
           }
       }

       private System.DateTime _fechaFin;

     
       public System.DateTime fechaFin
       {
           get
           {
               try
               {
                   return _fechaFin;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting fechaFin", err);
               }
           }
           set
           {
               try
               {
                   _fechaFin = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting fechaFin", err);
               }
           }
       }

       private int _flagUltimo;

       public int flagUltimo
       {
           get { return _flagUltimo; }
           set { _flagUltimo = value; }
       }
       private string _flagHitoDescripcion;
       

public string flagHitoDescripcion
{
  get { return _flagHitoDescripcion; }
  set { _flagHitoDescripcion = value; }
}private string _strSituacion;

       public string situacion
       {
           get { return _strSituacion; }
           set { _strSituacion = value; }
       }
       private int _id_situacion;

      
       public int id_situacion
       {
           get
           {
               try
               {
                   return _id_situacion;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_situacion", err);
               }
           }
           set
           {
               try
               {
                   _id_situacion = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_situacion", err);
               }
           }
       }

       private string _flagRelevante;

       public string flagRelevante
       {
           get { return _flagRelevante; }
           set { _flagRelevante = value; }
       }
       private string _descripcionActividad;

       public string descripcionActividad
       {
           get
           {
               try
               {
                   return _descripcionActividad;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting descripcionActividad", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 1000))
                   {
                       _descripcionActividad = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting descripcionActividad, Length of value is to long. Maximum Length: 1000");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting descripcionActividad", err);
               }
           }
       }

       private decimal _avance;

      
       public decimal avance
       {
           get
           {
               try
               {
                   return _avance;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting avance", err);
               }
           }
           set
           {
               try
               {
                   _avance = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting avance", err);
               }
           }
       }

       private decimal _avanceProgramado;

       public decimal avanceProgramado
       {
           get { return _avanceProgramado; }
           set { _avanceProgramado = value; }
       }

       private string _propuestaSolucion;

      
       public string propuestaSolucion
       {
           get
           {
               try
               {
                   return _propuestaSolucion;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting propuestaSolucion", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 1000))
                   {
                       _propuestaSolucion = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting propuestaSolucion, Length of value is to long. Maximum Length: 1000");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting propuestaSolucion", err);
               }
           }
       }

       private Int32 _activo;


       public Int32 activo
       {
           get
           {
               try
               {
                   return _activo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting activo", err);
               }
           }
           set
           {
               try
               {
                   _activo = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting activo", err);
               }
           }
       }

    

       private System.DateTime _fecha;

       public System.DateTime fecha
       {
           get
           {
               try
               {
                   return _fecha;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting fecha_update", err);
               }
           }
           set
           {
               try
               {
                   _fecha = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting fecha_update", err);
               }
           }
       }

       private System.DateTime? _fecha_update;

       public System.DateTime? fecha_update
       {
           get
           {
               try
               {
                   return _fecha_update;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting fecha_update", err);
               }
           }
           set
           {
               try
               {
                   _fecha_update = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting fecha_update", err);
               }
           }
       }

       private int _usr_registro;

       public int usr_registro
       {
           get
           {
               try
               {
                   return _usr_registro;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting usr_registro", err);
               }
           }
           set
           {
               try
               {
                   _usr_registro = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting usr_registro", err);
               }
           }
       }

       private int _usr_update;

       public int usr_update
       {
           get
           {
               try
               {
                   return _usr_update;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting usr_update", err);
               }
           }
           set
           {
               try
               {
                   _usr_update = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting usr_update", err);
               }
           }
       }

       private string _problemas;

       public string problemas
       {
           get
           {
               try
               {
                   return _problemas;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting problemas", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 1000))
                   {
                       _problemas = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting problemas, Length of value is to long. Maximum Length: 1000");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting problemas", err);
               }
           }
       }

       private int _id_usuario;
       public int id_usuario
       {
           get { return _id_usuario; }
           set { _id_usuario = value; }
       }

       private string _usuario;

       public string usuario
       {
           get { return _usuario; }
           set { _usuario = value; }
       }
             
       private int _id_seguimiento;

       public int id_seguimiento
       {
           get
           {
               try
               {
                   return _id_seguimiento;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_seguimiento", err);
               }
           }
           set
           {
               try
               {
                   _id_seguimiento = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_seguimiento", err);
               }
           }
       }

       private string _vfecha;

       public string vfecha
       {
           get { return _vfecha; }
           set { _vfecha = value; }
       }

       private int _anio;

       public int anio
       {
           get
           {
               try
               {
                   return _anio;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting anio", err);
               }
           }
           set
           {
               try
               {
                   _anio = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting anio", err);
               }
           }
       }

       private decimal _pia;

       public decimal pia
       {
           get
           {
               try
               {
                   return _pia;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting pia", err);
               }
           }
           set
           {
               try
               {
                   _pia = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting pia", err);
               }
           }
       }

       private decimal? _pimAcumulado;
        
       public decimal? pimAcumulado
       {
           get
           {
               try
               {
                   return _pimAcumulado;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting pimAcumulado", err);
               }
           }
           set
           {
               try
               {
                   _pimAcumulado = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting pimAcumulado", err);
               }
           }
       }

       private decimal _enero;

       public decimal enero
       {
           get
           {
               try
               {
                   return _enero;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting enero", err);
               }
           }
           set
           {
               try
               {
                   _enero = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting enero", err);
               }
           }
       }

       private decimal _febrero;

       public decimal febrero
       {
           get
           {
               try
               {
                   return _febrero;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting febrero", err);
               }
           }
           set
           {
               try
               {
                   _febrero = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting febrero", err);
               }
           }
       }

       private decimal _marzo;

       public decimal marzo
       {
           get
           {
               try
               {
                   return _marzo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting marzo", err);
               }
           }
           set
           {
               try
               {
                   _marzo = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting marzo", err);
               }
           }
       }

       private decimal _abril;

       public decimal abril
       {
           get
           {
               try
               {
                   return _abril;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting abril", err);
               }
           }
           set
           {
               try
               {
                   _abril = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting abril", err);
               }
           }
       }

       private decimal _mayo;

       public decimal mayo
       {
           get
           {
               try
               {
                   return _mayo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting mayo", err);
               }
           }
           set
           {
               try
               {
                   _mayo = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting mayo", err);
               }
           }
       }

       private decimal _junio;

       public decimal junio
       {
           get
           {
               try
               {
                   return _junio;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting junio", err);
               }
           }
           set
           {
               try
               {
                   _junio = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting junio", err);
               }
           }
       }

       private decimal _julio;

       public decimal julio
       {
           get
           {
               try
               {
                   return _julio;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting julio", err);
               }
           }
           set
           {
               try
               {
                   _julio = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting julio", err);
               }
           }
       }

       private decimal _agosto;

       public decimal agosto
       {
           get
           {
               try
               {
                   return _agosto;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting agosto", err);
               }
           }
           set
           {
               try
               {
                   _agosto = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting agosto", err);
               }
           }
       }

       private decimal _setiembre;

       public decimal setiembre
       {
           get
           {
               try
               {
                   return _setiembre;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting setiembre", err);
               }
           }
           set
           {
               try
               {
                   _setiembre = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting setiembre", err);
               }
           }
       }

       private decimal _octubre;

       public decimal octubre
       {
           get
           {
               try
               {
                   return _octubre;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting octubre", err);
               }
           }
           set
           {
               try
               {
                   _octubre = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting octubre", err);
               }
           }
       }

       private decimal _noviembre;

       public decimal noviembre
       {
           get
           {
               try
               {
                   return _noviembre;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting noviembre", err);
               }
           }
           set
           {
               try
               {
                   _noviembre = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting noviembre", err);
               }
           }
       }

       private decimal _diciembre;

       public decimal diciembre
       {
           get
           {
               try
               {
                   return _diciembre;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting diciembre", err);
               }
           }
           set
           {
               try
               {
                   _diciembre = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting diciembre", err);
               }
           }
       }

       private decimal? _devengadoAnual;

       public decimal? devengadoAnual
       {
           get
           {
               try
               {
                   return _devengadoAnual;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting devengadoAnual", err);
               }
           }
           set
           {
               try
               {
                   _devengadoAnual = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting devengadoAnual", err);
               }
           }
       }

    
    


       private int _flagEjecutado;

   
       public int flagEjecutado
       {
           get
           {
               try
               {
                   return _flagEjecutado;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting flagEjecutado", err);
               }
           }
           set
           {
               try
               {
                   _flagEjecutado = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting flagEjecutado", err);
               }
           }
       }



       private string _observacion;

       public string observacion
       {
           get
           {
               try
               {
                   return _observacion;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting problemas", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 500))
                   {
                       _observacion = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting problemas, Length of value is to long. Maximum Length: 1000");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting problemas", err);
               }
           }
       }


       #region Tables Memebers
    

       private int _id_grupo;

   
       public int id_grupo
       {
           get
           {
               try
               {
                   return _id_grupo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_grupo", err);
               }
           }
           set
           {
               try
               {
                   _id_grupo = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_grupo", err);
               }
           }
       }

       private int _codigo;

     
       public int codigo
       {
           get
           {
               try
               {
                   return _codigo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting codigo", err);
               }
           }
           set
           {
               try
               {
                   _codigo = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting codigo", err);
               }
           }
       }

       private string _proyectos;

      
       public string proyectos
       {
           get
           {
               try
               {
                   return _proyectos;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting proyectos", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 200))
                   {
                       _proyectos = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting proyectos, Length of value is to long. Maximum Length: 200");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting proyectos", err);
               }
           }
       }

       private int _id_responsable;

       public int id_responsable
       {
           get
           {
               try
               {
                   return _id_responsable;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_responsable", err);
               }
           }
           set
           {
               try
               {
                   _id_responsable = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_responsable", err);
               }
           }
       }

      


      

       private string _objetivo;

    
       public string objetivo
       {
           get
           {
               try
               {
                   return _objetivo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting objetivo", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 2000))
                   {
                       _objetivo = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting objetivo, Length of value is to long. Maximum Length: 2000");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting objetivo", err);
               }
           }
       }

       private decimal _costo;

       public decimal costo
       {
           get
           {
               try
               {
                   return _costo;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting costo", err);
               }
           }
           set
           {
               try
               {
                   _costo = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting costo", err);
               }
           }
       }
       #endregion


       private string _cod_depa;


       public string cod_depa
       {
           get
           {
               try
               {
                   return _cod_depa;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting _cod_depa", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 2000))
                   {
                       _cod_depa = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting _cod_depa, Length of value is to long. Maximum Length: 2000");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting _cod_depa", err);
               }
           }
       }

     
       private long _id_documento;

     
       public long id_documento
       {
           get
           {
               try
               {
                   return _id_documento;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_documento", err);
               }
           }
           set
           {
               try
               {
                   _id_documento = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_documento", err);
               }
           }
       }


       private string _descripcion;

       public string descripcion
       {
           get
           {
               try
               {
                   return _descripcion;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting descripcion", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 200))
                   {
                       _descripcion = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting descripcion, Length of value is to long. Maximum Length: 200");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting descripcion", err);
               }
           }
       }

       private string _urlDoc;

       public string urlDoc
       {
           get
           {
               try
               {
                   return _urlDoc;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting urlDoc", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 200))
                   {
                       _urlDoc = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting urlDoc, Length of value is to long. Maximum Length: 200");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting urlDoc", err);
               }
           }
       }

       private int _id_tipoDocumento;

       public int id_tipoDocumento
       {
           get
           {
               try
               {
                   return _id_tipoDocumento;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting id_tipoDocumento", err);
               }
           }
           set
           {
               try
               {
                   _id_tipoDocumento = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting id_tipoDocumento", err);
               }
           }
       }

       private string _extension;

       public string extension
       {
           get
           {
               try
               {
                   return _extension;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting _extension", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 200))
                   {
                       _extension = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting _extension, Length of value is to long. Maximum Length: 200");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting _extension", err);
               }
           }
       }


       private int _flagAccion;


       public int flagAccion
       {
           get
           {
               try
               {
                   return _flagAccion;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting flagAccion", err);
               }
           }
           set
           {
               try
               {
                   _flagAccion = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting flagAccion", err);
               }
           }
       }

       private string _nombre;


       public string nombre
       {
           get
           {
               try
               {
                   return _nombre;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting nombre", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 200))
                   {
                       _nombre = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting nombre, Length of value is to long. Maximum Length: 200");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting nombre", err);
               }
           }
       }

       private string _resultado;


       public string resultado
       {
           get
           {
               try
               {
                   return _resultado;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting resultado", err);
               }
           }
           set
           {
               try
               {
                   if ((value.Length <= 500))
                   {
                       _resultado = value;
                   }
                   else
                   {
                       throw new OverflowException("Error setting resultado, Length of value is to long. Maximum Length: 500");
                   }
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting resultado", err);
               }
           }
       }

       private string _flagComponente;

       public string flagComponente
       {
           get { return _flagComponente; }
           set { _flagComponente = value; }
       }

       private string _flagPresupuesto;

       public string flagPresupuesto
       {
           get { return _flagPresupuesto; }
           set { _flagPresupuesto = value; }
       }


       private int _id_calendarioActividad;

       public int id_calendarioActividad
       {
           get { return _id_calendarioActividad; }
           set { _id_calendarioActividad = value; }
       }
       private int _flagHito;


       public int flagHito
       {
           get
           {
               try
               {
                   return _flagHito;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error getting _flagHito", err);
               }
           }
           set
           {
               try
               {
                   _flagHito = value;
               }
               catch (System.Exception err)
               {
                   throw new Exception("Error setting _flagHito", err);
               }
           }
       }

       private string _meta;

       public string meta
       {
           get { return _meta; }
           set { _meta = value; }
       }


       private Int64 _numero;

       public Int64 numero
       {
           get { return _numero; }
           set { _numero = value; }
       }
       #endregion

       private string _strGrupo;

       public string strGrupo
       {
           get { return _strGrupo; }
           set { _strGrupo = value; }
       }

       private string _strRegion;

       public string strRegion
       {
           get { return _strRegion; }
           set { _strRegion = value; }
       }

       private string _strResponsable;

       public string strResponsable
       {
           get { return _strResponsable; }
           set { _strResponsable = value; }
       }

       private decimal _dEjecucion;

       public decimal dEjecucion
       {
           get { return _dEjecucion; }
           set { _dEjecucion = value; }
       }

       private decimal? _dsaldo;

       public decimal? dsaldo
       {
           get { return _dsaldo; }
           set { _dsaldo = value; }
       }

       private string _strUserSkype;

       public string strUserSkype
       {
           get { return _strUserSkype; }
           set { _strUserSkype = value; }
       }

       private string _urlPresupuesto;

       public string urlPresupuesto
       {
           get { return _urlPresupuesto; }
           set { _urlPresupuesto = value; }
       }

       private string _strVisible;

       public string strVisible
       {
           get { return _strVisible; }
           set { _strVisible = value; }
       }

       private int _flagReunion;

       public int flagReunion
       {
           get { return _flagReunion; }
           set { _flagReunion = value; }
       }

       private bool _boolVisible;


       public bool boolVisible
       {
           get { return _boolVisible; }
           set { _boolVisible = value; }
       }

       private int _id_actividadHistorial;

       public int id_actividadHistorial
       {
           get { return _id_actividadHistorial; }
           set { _id_actividadHistorial = value; }
       }

       private DateTime _dtFechaHistorial;

       public DateTime dtFechaHistorial
       {
           get { return _dtFechaHistorial; }
           set { _dtFechaHistorial = value; }
       }

       private string _documento;

       public string documento
       {
           get { return _documento; }
           set { _documento = value; }
       }
       private int _id_impresion;

       public int id_impresion
       {
           get { return _id_impresion; }
           set { _id_impresion = value; }
       }
   }
}
