﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BE_MON_PROCESO
    {
        private int _id_proyecto;
        private int _tipoFinanciamiento;
        private int _id_usuario;

        private int _id_tipoModalidad;
        private int _id_tipoAdjudicacion;
        private string _norLic;
        private string _anio;
        private string _detalle;
        private DateTime _buenaPro;
        private DateTime _buenaProConsentida;
        private string _valorReferencial;
        private int _id_tipoEmpresa;
        private string _nombreContratista;
        private string _rucContratista;
        private string _montoConsorcio;
        private string _NroContratoConsorcio;
        private string _urlCartaConsorcio;
        private string _urlContrato;

        private int _id_subContratos;
        private int _id_responsable;
        private int _id_grupo_consorcio;
        private int _id_tipoMoneda;

        public int id_tipoMoneda
        {
            get { return _id_tipoMoneda; }
            set { _id_tipoMoneda = value; }
        }

        private string _convocatoria;
        private DateTime _fechaPublicacion;
        private string _resultado;
        private string _urlDoc;
        private string _observacion;

        private string _nroResolucion;
        private string _cip;
        private string _representante;
        private string _flagObra;
        private string _TipoContratacion;

        private string _nombre;
        private string _cargo;

        private int _id_rubro;
        private string _NroRuc;
        private string _monto;
        private int _plazo;

        private string _entidadEjecutora;
        private string _coordinadorEJ;
        private string _telefonoCoordEJ;
        private string _correoCoordEJ;
        private string _convenioContrato;

        private string _montoAprobadoExpediente;

        private string _entidadContratante;

        private string _dni;
        private string _especialidad;
        private string _telefono;
        private string _correo;

        public string entidadContratante
        {
            get { return _entidadContratante; }
            set { _entidadContratante = value; }
        }
        public string montoAprobadoExpediente
        {
            get { return _montoAprobadoExpediente; }
            set { _montoAprobadoExpediente = value; }
        }
        private string _montoFinanciadoMVCS;

        public string montoFinanciadoMVCS
        {
            get { return _montoFinanciadoMVCS; }
            set { _montoFinanciadoMVCS = value; }
        }
        private string _montoTransferencia;

        public string montoTransferencia
        {
            get { return _montoTransferencia; }
            set { _montoTransferencia = value; }
        }

       

        private string _strFechaAprobacionExpediente;

        public string strFechaAprobacionExpediente
        {
            get { return _strFechaAprobacionExpediente; }
            set { _strFechaAprobacionExpediente = value; }
        }

        private string _strFechaPresentacion;

        public string strFechaPresentacion
        {
            get { return _strFechaPresentacion; }
            set { _strFechaPresentacion = value; }
        }

        private string _strResultadoSoli;

        public string strResultadoSoli
        {
            get { return _strResultadoSoli; }
            set { _strResultadoSoli = value; }
        }

        private string _strFechaAproba;

        public string strFechaAproba
        {
            get { return _strFechaAproba; }
            set { _strFechaAproba = value; }
        }

        private string _strFechaAprobaExpediente;

        public string strFechaAprobaExpediente
        {
            get { return _strFechaAprobaExpediente; }
            set { _strFechaAprobaExpediente = value; }
        }

        public string EntidadEjecutora
        {
            get { return _entidadEjecutora; }
            set { _entidadEjecutora = value; }
        }
       
        public string CoordinadorEJ
        {
            get { return _coordinadorEJ; }
            set { _coordinadorEJ = value; }
        }
       
        public string TelefonoCoordEJ
        {
            get { return _telefonoCoordEJ; }
            set { _telefonoCoordEJ = value; }
        }
        
        public string CorreoCoordEJ
        {
            get { return _correoCoordEJ; }
            set { _correoCoordEJ = value; }
        }
      
        public string ConvenioContrato
        {
            get { return _convenioContrato; }
            set { _convenioContrato = value; }
        }

        private string _urlConvenio;

        public string urlConvenio
        {
            get { return _urlConvenio; }
            set { _urlConvenio = value; }
        }

        private int _flagResultado;

        public int flagResultado
        {
            get { return _flagResultado; }
            set { _flagResultado = value; }
        }

        private DateTime _fechaSolicitud;

        public string fechaSolicitud
        {
            get { return _fechaSolicitud.ToString("dd/MM/yyyy"); }
            set { _fechaSolicitud = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaSolicitud
        {
            get { return _fechaSolicitud; }
            set { _fechaSolicitud = value; }
        }

        private DateTime _fechaContrato;

        public string fechaContrato
        {
            get { return _fechaContrato.ToString("dd/MM/yyyy"); }
            set { _fechaContrato = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaContrato
        {
            get { return _fechaContrato; }
            set { _fechaContrato = value; }
        }
        private string _urlSolicitud;

        public string urlSolicitud
        {
            get { return _urlSolicitud; }
            set { _urlSolicitud = value; }
        }

        private DateTime _fechaConvenio;
        
        public string fechaConvenio
        {
            get { return _fechaConvenio.ToString("dd/MM/yyyy"); }
            set { _fechaConvenio = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaConvenio
        {
            get { return _fechaConvenio; }
            set { _fechaConvenio = value; }
        }

        private string _strFechaConvenio;

        public string strFechaConvenio
        {
            get { return _strFechaConvenio; }
            set { _strFechaConvenio = value; }
        }
        
        private int _id_seguimiento;

        private string _nombreConsorcio;

        public string nombreConsorcio
        {
            get { return _nombreConsorcio; }
            set { _nombreConsorcio = value; }
        }
        private string _rucConsorcio;

        public string rucConsorcio
        {
            get { return _rucConsorcio; }
            set { _rucConsorcio = value; }
        }

        private string _representanteContratista;

        public string representanteContratista
        {
            get { return _representanteContratista; }
            set { _representanteContratista = value; }
        }
        private string _representanteConsorcio;

        public string representanteConsorcio
        {
            get { return _representanteConsorcio; }
            set { _representanteConsorcio = value; }
        }
        private string _telefonoConsorcio;

        public string telefonoConsorcio
        {
            get { return _telefonoConsorcio; }
            set { _telefonoConsorcio = value; }
        }

        public int id_rubro
        {
            get { return _id_rubro; }
            set { _id_rubro = value; }
        }

        public int id_subContratos
        {
            get { return _id_subContratos; }
            set { _id_subContratos = value; }
        }


        public int id_seguimiento
        {
            get { return _id_seguimiento; }
            set { _id_seguimiento = value; }
        }

        public int id_responsable
        {
            get { return _id_responsable; }
            set { _id_responsable = value; }
        }

        public int id_grupo_consorcio
        {
            get { return _id_grupo_consorcio; }
            set { _id_grupo_consorcio = value; }
        }

        public string NroRuc
        {
            get { return _NroRuc; }
            set { _NroRuc = value; }
        }

        public string monto
        {
            get { return _monto; }
            set { _monto = value; }
        }

        public int plazo
        {
            get { return _plazo; }
            set { _plazo = value; }
        }
        
        public string nroResolucion
        {
            get { return _nroResolucion; }
            set { _nroResolucion = value; }
        }
        
        public string cip
        {
            get { return _cip; }
            set { _cip = value; }
        }


        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public string cargo
        {
            get { return _cargo; }
            set { _cargo = value; }
        }

        

        public string TipoContratacion
        {
            get { return _TipoContratacion; }
            set { _TipoContratacion = value; }
        }
        public string flagObra
        {
            get { return _flagObra; }
            set { _flagObra = value; }
        }

        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

        public int tipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        public int id_tipoModalidad
        {
            get { return _id_tipoModalidad; }
            set { _id_tipoModalidad = value; }
        }


        public int id_tipoAdjudicacion
        {
            get { return _id_tipoAdjudicacion; }
            set { _id_tipoAdjudicacion = value; }
        }


        public string norLic
        {
            get { return _norLic; }
            set { _norLic = value; }
        }

        public string anio
        {
            get { return _anio; }
            set { _anio = value; }
        }

        public string detalle
        {
            get { return _detalle; }
            set { _detalle = value; }
        }

        public string buenaPro
        {
            get { return _buenaPro.ToString("dd/MM/yyyy"); }
            set { _buenaPro = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_buenapro
        {
            get { return _buenaPro; }
            set { _buenaPro = value; }
        }

        public string buenaProConsentida
        {
            get { return _buenaProConsentida.ToString("dd/MM/yyyy"); }
            set { _buenaProConsentida = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_buenaProConsentida
        {
            get { return _buenaProConsentida; }
            set { _buenaProConsentida = value; }
        }

        public string valorReferencial
        {
            get { return _valorReferencial; }
            set { _valorReferencial = value; }
        }

        public int id_tipoEmpresa
        {
            get { return _id_tipoEmpresa; }
            set { _id_tipoEmpresa = value; }
        }

       public string nombreContratista
        {
            get { return _nombreContratista; }
            set { _nombreContratista = value; }
        }

        public string rucContratista
        {
            get { return _rucContratista; }
            set { _rucContratista = value; }
        }
        
        public string montoConsorcio
        {
            get { return _montoConsorcio; }
            set { _montoConsorcio = value; }
        }

        public string NroContratoConsorcio
        {
            get { return _NroContratoConsorcio; }
            set { _NroContratoConsorcio = value; }
        }

        public string urlCartaConsorcio
        {
            get { return _urlCartaConsorcio; }
            set { _urlCartaConsorcio = value; }
        }


        public string urlContrato
        {
            get { return _urlContrato; }
            set { _urlContrato = value; }
        }
        protected string _urlExpediente;
        public string urlExpediente
        {
            get { return _urlExpediente; }
            set { _urlExpediente = value; }
        }
        protected string _urlOrientacion;
        public string urlOrientacion
        {
            get { return _urlOrientacion; }
            set { _urlOrientacion = value; }
        }

 
        public string convocatoria
        {
            get { return _convocatoria; }
            set { _convocatoria = value; }
        }

        public string fechaPublicacion
        {
            get { return _fechaPublicacion.ToString("dd/MM/yyyy"); }
            set { _fechaPublicacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaPublicacion
        {
            get { return _fechaPublicacion; }
            set { _fechaPublicacion = value; }
        }

        public string urlDoc
        {
            get { return _urlDoc; }
            set { _urlDoc = value; }
        }

        public string observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public string resultado
        {
            get { return _resultado; }
            set { _resultado = value; }
        }

        public string representante
        {
            get { return _representante; }
            set { _representante = value; }
        }

        private int _numero;

        public int numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        private int _id_proceso_seleccion;

        public int id_proceso_seleccion
        {
            get { return _id_proceso_seleccion; }
            set { _id_proceso_seleccion = value; }
        }

        private string _usuario;

        public string usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }
        private string _strFecha_update;

        public string strFecha_update
        {
            get { return _strFecha_update; }
            set { _strFecha_update = value; }
        }

        private string _tipo_adjudicacion;

        public string tipo_adjudicacion
        {
            get { return _tipo_adjudicacion; }
            set { _tipo_adjudicacion = value; }
        }

        private int _idResultado;

        public int idResultado
        {
            get { return _idResultado; }
            set { _idResultado = value; }
        }

        private string _rubro;

        public string rubro
        {
            get { return _rubro; }
            set { _rubro = value; }
        }
        private string _modalidad;

        public string modalidad
        {
            get { return _modalidad; }
            set { _modalidad = value; }
        }

        private int _idTipoAlcance;

        public int idTipoAlcance
        {
            get { return _idTipoAlcance; }
            set { _idTipoAlcance = value; }
        }

        private int _hombres;

        public int hombres
        {
            get { return _hombres; }
            set { _hombres = value; }
        }

        private int _mujeres;

        public int mujeres
        {
            get { return _mujeres; }
            set { _mujeres = value; }
        }
        private string _urlActaAsamblea;

        public string urlActaAsamblea
        {
            get { return _urlActaAsamblea; }
            set { _urlActaAsamblea = value; }
        }

        private string _strFechaActaConstitucionNE;

        public string strFechaActaConstitucionNE
        {
            get { return _strFechaActaConstitucionNE; }
            set { _strFechaActaConstitucionNE = value; }
        }
        private DateTime _fechaActaConstitucionNE;
        public string fechaActaConstitucionNE
        {
            get { return _fechaActaConstitucionNE.ToString("dd/MM/yyyy"); }
            set { _fechaActaConstitucionNE = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAprobacionSolicitud
        {
            get { return _fechaAprobacionSolicitud; }
            set { _fechaAprobacionSolicitud = value; }
        }

        private DateTime _fechaAprobacionSolicitud;
        public string fechaAprobacionSolicitud
        {
            get { return _fechaAprobacionSolicitud.ToString("dd/MM/yyyy"); }
            set { _fechaActaConstitucionNE = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }
        private string _strFechaAprobacionSolicitud;

        public string strFechaAprobacionSolicitud
        {
            get { return _strFechaAprobacionSolicitud; }
            set { _strFechaAprobacionSolicitud = value; }
        }
        public DateTime Date_fechActaConstitucionNE
        {
            get { return _fechaActaConstitucionNE; }
            set { _fechaActaConstitucionNE = value; }
        }

        private string _comentario;
        private int _tipoDocumento;

        public int tipoDocumento
        {
            get { return _tipoDocumento; }
            set { _tipoDocumento = value; }
        }


        private string _nroDocumento;

        public string nroDocumento
        {
            get { return _nroDocumento; }
            set { _nroDocumento = value; }
        }

        private string _apellidoPaterno;

        public string apellidoPaterno
        {
            get { return _apellidoPaterno; }
            set { _apellidoPaterno = value; }
        }
        private string _apellidoMaterno;

        public string apellidoMaterno
        {
            get { return _apellidoMaterno; }
            set { _apellidoMaterno = value; }
        }

        private string _domicilio;

        public string domicilio
        {
            get { return _domicilio; }
            set { _domicilio = value; }
        }

        private string _cod_depa;

        public string cod_depa
        {
            get { return _cod_depa; }
            set { _cod_depa = value; }
        }
        private string _cod_prov;

        public string cod_prov
        {
            get { return _cod_prov; }
            set { _cod_prov = value; }
        }

        private string _cod_dist;

        public string cod_dist
        {
            get { return _cod_dist; }
            set { _cod_dist = value; }
        }

        private string _cod_CCPP;

        public string cod_CCPP
        {
            get { return _cod_CCPP; }
            set { _cod_CCPP = value; }
        }

        private DateTime _fechaNacimiento;
        public string fechaNacimiento
        {
            get { return _fechaNacimiento.ToString("dd/MM/yyyy"); }
            set { _fechaNacimiento = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaNacimiento
        {
            get { return _fechaNacimiento; }
            set { _fechaNacimiento = value; }
        }

        private int _tipoGrado;

        public int tipoGrado
        {
            get { return _tipoGrado; }
            set { _tipoGrado = value; }
        }

        private int _tipoOcupacion;

        public int tipoOcupacion
        {
            get { return _tipoOcupacion; }
            set { _tipoOcupacion = value; }
        }

        private string _sexo;

        public string sexo
        {
            get { return _sexo; }
            set { _sexo = value; }
        }

        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _id_tipoDocumentoidentificacion;

        public int id_tipoDocumentoidentificacion
        {
            get { return _id_tipoDocumentoidentificacion; }
            set { _id_tipoDocumentoidentificacion = value; }
        }

        private int _id_TipoCargo;

        public int id_TipoCargo
        {
            get { return _id_TipoCargo; }
            set { _id_TipoCargo = value; }
        }
        private int _id_TipoEstadoAsignacion;

        private string _tipoCargo;

        public string tipoCargo
        {
            get { return _tipoCargo; }
            set { _tipoCargo = value; }
        }
        public int id_TipoEstadoAsignacion
        {
            get { return _id_TipoEstadoAsignacion; }
            set { _id_TipoEstadoAsignacion = value; }
        }

        private string _tipoEstadoAsignacion;

        public string tipoEstadoAsignacion
        {
            get { return _tipoEstadoAsignacion; }
            set { _tipoEstadoAsignacion = value; }
        }
        private DateTime _fechaAsignacion;
        public string fechaAsignacion
        {
            get { return _fechaAsignacion.ToString("dd/MM/yyyy"); }
            set { _fechaAsignacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAsignacion
        {
            get { return _fechaAsignacion; }
            set { _fechaAsignacion = value; }
        }

        private DateTime _fechaDesignacion;
        public string fechaDesignacion
        {
            get { return _fechaDesignacion.ToString("dd/MM/yyyy"); }
            set { _fechaDesignacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaDesignacion
        {
            get { return _fechaDesignacion; }
            set { _fechaDesignacion = value; }
        }

        private int _id_padronMiembroNE;

        public int id_padronMiembroNE
        {
            get { return _id_padronMiembroNE; }
            set { _id_padronMiembroNE = value; }
        }

        private int _id_tipo;

        public int id_tipo
        {
            get { return _id_tipo; }
            set { _id_tipo = value; }
        }

        private DateTime? _dtFechaDesignacion;
        public DateTime? dtFechaDesignacion
        {
            get { return _dtFechaDesignacion; }
            set { _dtFechaDesignacion = value; }
        }

        private DateTime? _dtfechaAsignacion;
        public DateTime? dtFechaAsignacion
        {
            get { return _dtfechaAsignacion; }
            set { _dtfechaAsignacion = value; }
        }

        private string _nombrePresidente;

        public string nombrePresidente
        {
            get { return _nombrePresidente; }
            set { _nombrePresidente = value; }
        }

        private string _sexoPresi;

        public string sexoPresi
        {
            get { return _sexoPresi; }
            set { _sexoPresi = value; }
        }

        private string _estadoPresi;

        public string estadoPresi
        {
            get { return _estadoPresi; }
            set { _estadoPresi = value; }
        }

        private string _fechaPresi;

        public string fechaPresi
        {
            get { return _fechaPresi; }
            set { _fechaPresi = value; }
        }

        private string _nombreTesorero;

        public string nombreTesorero
        {
            get { return _nombreTesorero; }
            set { _nombreTesorero = value; }
        }
        private string _sexoTesorero;

        public string sexoTesorero
        {
            get { return _sexoTesorero; }
            set { _sexoTesorero = value; }
        }

        private string _estadoTesorero;

        public string estadoTesorero
        {
            get { return _estadoTesorero; }
            set { _estadoTesorero = value; }
        }

        private string _fechaTesorero;

        public string fechaTesorero
        {
            get { return _fechaTesorero; }
            set { _fechaTesorero = value; }
        }
        private string _nombreSecretario;

        public string nombreSecretario
        {
            get { return _nombreSecretario; }
            set { _nombreSecretario = value; }
        }
        private string _sexoSecretario;

        public string sexoSecretario
        {
            get { return _sexoSecretario; }
            set { _sexoSecretario = value; }
        }
        private string _estadoSecretario;

        public string estadoSecretario
        {
            get { return _estadoSecretario; }
            set { _estadoSecretario = value; }
        }
        private string _fechaSecretario;

        public string fechaSecretario
        {
            get { return _fechaSecretario; }
            set { _fechaSecretario = value; }
        }
        private string _nombreFiscal;

        public string nombreFiscal
        {
            get { return _nombreFiscal; }
            set { _nombreFiscal = value; }
        }
        private string _sexoFiscal;

        public string sexoFiscal
        {
            get { return _sexoFiscal; }
            set { _sexoFiscal = value; }
        }
        private string _estadoFiscal;

        public string estadoFiscal
        {
            get { return _estadoFiscal; }
            set { _estadoFiscal = value; }
        }
        private string _fechaFiscal;

        public string fechaFiscal
        {
            get { return _fechaFiscal; }
            set { _fechaFiscal = value; }
        }
        private string _nombreVeedor;

        public string nombreVeedor
        {
            get { return _nombreVeedor; }
            set { _nombreVeedor = value; }
        }
        private string _sexoVeedor;

        public string sexoVeedor
        {
            get { return _sexoVeedor; }
            set { _sexoVeedor = value; }
        }
        private string _estadoVeedor;

        public string estadoVeedor
        {
            get { return _estadoVeedor; }
            set { _estadoVeedor = value; }
        }
        private string _fechaVeedor;

        public string fechaVeedor
        {
            get { return _fechaVeedor; }
            set { _fechaVeedor = value; }
        }
        private string _nombreRepresentanteMVCS;

        public string nombreRepresentanteMVCS
        {
            get { return _nombreRepresentanteMVCS; }
            set { _nombreRepresentanteMVCS = value; }
        }
        private string _sexoRepresentateMVCS;

        public string sexoRepresentateMVCS
        {
            get { return _sexoRepresentateMVCS; }
            set { _sexoRepresentateMVCS = value; }
        }
        private string _estadoRepresentanteMVCS;

        public string estadoRepresentanteMVCS
        {
            get { return _estadoRepresentanteMVCS; }
            set { _estadoRepresentanteMVCS = value; }
        }
        private string _fechaRepresentanteMVCS;

        public string fechaRepresentanteMVCS
        {
            get { return _fechaRepresentanteMVCS; }
            set { _fechaRepresentanteMVCS = value; }
        }

        private string _strNumHombres;

        public string strNumHombres
        {
            get { return _strNumHombres; }
            set { _strNumHombres = value; }
        }
        private string _strNumMujeres;

        public string strNumMujeres
        {
            get { return _strNumMujeres; }
            set { _strNumMujeres = value; }
        }

        private string _banco;

        public string banco
        {
            get { return _banco; }
            set { _banco = value; }
        }

        private string _strNumeroCuenta;

        public string numeroCuenta
        {
            get { return _strNumeroCuenta; }
            set { _strNumeroCuenta = value; }
        }

        private string _strFechaCuenta;

        public string strFechaCuenta
        {
            get { return _strFechaCuenta; }
            set { _strFechaCuenta = value; }
        }

        private string _strDepartamentoCuenta;

        public string strDepartamentoCuenta
        {
            get { return _strDepartamentoCuenta; }
            set { _strDepartamentoCuenta = value; }
        }

        private string _strProvinciaCuenta;

        public string strProvinciaCuenta
        {
            get { return _strProvinciaCuenta; }
            set { _strProvinciaCuenta = value; }
        }

        private string _strDistritoCuenta;

        public string strDistritoCuenta
        {
            get { return _strDistritoCuenta; }
            set { _strDistritoCuenta = value; }
        }

        private string _strNombreAgencia;

        public string strNombreAgencia
        {
            get { return _strNombreAgencia; }
            set { _strNombreAgencia = value; }
        }

        private string _strFechaOrientacion;

        public string strFechaOrientacion
        {
            get { return _strFechaOrientacion; }
            set { _strFechaOrientacion = value; }
        }

        private string _strHombresOrientacion;

        public string strHombresOrientacion
        {
            get { return _strHombresOrientacion; }
            set { _strHombresOrientacion = value; }
        }

        private string _strMujeresOrientacion;

        public string strMujeresOrientacion
        {
            get { return _strMujeresOrientacion; }
            set { _strMujeresOrientacion = value; }
        }


        private DateTime _fechaApertura;
        public string fechaApertura
        {
            get { return _fechaApertura.ToString("dd/MM/yyyy"); }
            set { _fechaApertura = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaApertura
        {
            get { return _fechaApertura; }
            set { _fechaApertura = value; }
        }

        private DateTime _fecha;
        public string fecha
        {
            get { return _fecha.ToString("dd/MM/yyyy"); }
            set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public string comentario
        {
            get
            {
                return _comentario;
            }

            set
            {
                _comentario = value;
            }
        }

        public string dni { get => _dni; set => _dni = value; }
        public string especialidad { get => _especialidad; set => _especialidad = value; }
        public string telefono { get => _telefono; set => _telefono = value; }
        public string correo { get => _correo; set => _correo = value; }
    }
}
