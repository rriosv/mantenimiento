﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_MON_PadronBeneficiario
    {
        

        private int _id_padronBeneficiario;
        private int _id_proyecto;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        private string _nombres;
        private int _id_sexo;
        private DateTime _fechaNacimiento;
        private int _id_tipoDocumentoMiembroNE;
        private string _dni;
        private int _id_tipoDomicilio;
        private string _direccionDomicilio;
        private string _nroDomicilio;
        private int _id_tipoBeneficio;
        private int _id_estadoBeneficiario;
        private int _nroMiembrosHombres;
        private int _nroMiembrosMujeres;
        private int _id_conexioAgua;
        private int _id_ubs;
        private int _id_tipoUbs;
        private int _id_estadoPredio;
        private string _observaciones;
        private DateTime _fechaRegistro;
        private DateTime _fechaModificacion;

        private int _usr_registro;
        private int _usr_update;


        public int id_padronBeneficiario
        {
            get { return _id_padronBeneficiario; }
            set { _id_padronBeneficiario = value; }
        }

        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

        public string apellidoPaterno
        {
            get { return _apellidoPaterno; }
            set { _apellidoPaterno = value; }
        }

        public string apellidoMaterno
        {
            get { return _apellidoMaterno; }
            set { _apellidoMaterno = value; }
        }

        public string nombres
        {
            get { return _nombres; }
            set { _nombres = value; }
        }

        private String _sexo;

        public String Sexo
        {
            get { return _sexo; }
            set { _sexo = value; }
        }

        public int id_sexo
        {
            get { return _id_sexo; }
            set { _id_sexo = value; }
        }

        public DateTime fechaNacimiento
        {
            get { return _fechaNacimiento; }
            set { _fechaNacimiento = value; }
        }

        private string _vfechaNacimiento;

        public string vfechaNacimiento
        {
            get { return _vfechaNacimiento; }
            set { _vfechaNacimiento = value; }
        }

        public int id_tipoDocumentoMiembroNE
        {
            get { return _id_tipoDocumentoMiembroNE; }
            set { _id_tipoDocumentoMiembroNE = value; }
        }
        
        public string dni
        {
            get { return _dni; }
            set { _dni = value; }
        }
        
        public int id_tipoDomicilio
        {
            get { return _id_tipoDomicilio; }
            set { _id_tipoDomicilio = value; }
        }
        
        public string direccionDomicilio
        {
            get { return _direccionDomicilio; }
            set { _direccionDomicilio = value; }
        }

        public string nroDomicilio
        {
            get { return _nroDomicilio; }
            set { _nroDomicilio = value; }
        }

        public int id_tipoBeneficio
        {
            get { return _id_tipoBeneficio; }
            set { _id_tipoBeneficio = value; }
        }

        public int id_estadoBeneficiario
        {
            get { return _id_estadoBeneficiario; }
            set { _id_estadoBeneficiario = value; }
        }

        public int nroMiembrosHombres
        {
            get { return _nroMiembrosHombres; }
            set { _nroMiembrosHombres = value; }
        }

        public int nroMiembrosMujeres
        {
            get { return _nroMiembrosMujeres; }
            set { _nroMiembrosMujeres = value; }
        }
        
        public int id_conexioAgua
        {
            get { return _id_conexioAgua; }
            set { _id_conexioAgua = value; }
        }
        
        public int id_ubs
        {
            get { return _id_ubs; }
            set { _id_ubs = value; }
        }
        
        public int id_tipoUbs
        {
            get { return _id_tipoUbs; }
            set { _id_tipoUbs = value; }
        }
        
        public int id_estadoPredio
        {
            get { return _id_estadoPredio; }
            set { _id_estadoPredio = value; }
        }
        
        public string observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; }
        }

        public DateTime fechaRegistro
        {
            get { return _fechaRegistro; }
            set { _fechaRegistro = value; }
        }
        
        public DateTime fechaModificacion
        {
            get { return _fechaModificacion; }
            set { _fechaModificacion = value; }
        }

        private string _tipo_DocMiembroNE;

        public string Tipo_DocMiembroNE
        {
            get { return _tipo_DocMiembroNE; }
            set { _tipo_DocMiembroNE = value; }
        }

        private string _tipo_Domicilio;

        public string Tipo_Domicilio
        {
            get { return _tipo_Domicilio; }
            set { _tipo_Domicilio = value; }
        }

        private string _tipo_Beneficio;

        public string Tipo_Beneficio
        {
            get { return _tipo_Beneficio; }
            set { _tipo_Beneficio = value; }
        }

        private string _estado_Beneficiario;

        public string Estado_Beneficiario
        {
            get { return _estado_Beneficiario; }
            set { _estado_Beneficiario = value; }
        }

        private string _conexion_Agua;

        public string Conexion_Agua
        {
            get { return _conexion_Agua; }
            set { _conexion_Agua = value; }
        }

        private string _ubs;

        public string Ubs
        {
            get { return _ubs; }
            set { _ubs = value; }
        }

        private string _tipo_Ubs;

        public string Tipo_Ubs
        {
            get { return _tipo_Ubs; }
            set { _tipo_Ubs = value; }
        }

        private string _estado_Predio;
       
        public string Estado_Predio
        {
            get { return _estado_Predio; }
            set { _estado_Predio = value; }
        }

        public string Detalle_Error { get; set; }
        public long Numero { get; set; }


        public int usr_registro
        {
            get { return _usr_registro; }
            set { _usr_registro = value; }
        }

        public int usr_update
        {
            get { return _usr_update; }
            set { _usr_update = value; }
        }
    }
}
