using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEProyecto
    {
        #region Atributos
        private string strCriterio;
        private string strBuscar;
        private int intId_usuario;
        private int intCod_tipo_usuario;
        private int intCod_subsector;
        private int intCod_subsector_filtro;
        private int intCod_programa;
        private int intCod_programa_filtro;
        private int intCod_prioridad;
        private int intCod_estrategia;
        private int intCod_clasificacion;
        private string strCod_depa;
        private int intPeriodo;
        private int intCod_estado;
        private int intId_proyecto;
        private string strAmbito;
        private int intBenef_direc;
        private int intBenef_indir;
        private int intCod_snip;
        private int intCodUnificado;
        private string strEntidad_ejec;
        private string lista;
        private string _strDepa;
        private string _strFecha;
        private string _descripcion;

        public string descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        public string strFecha
        {
            get { return _strFecha; }
            set { _strFecha = value; }
        }

        public string strDepa
        {
            get { return _strDepa; }
            set { _strDepa = value; }
        }
        private string _strDist;

        public string strDist
        {
            get { return _strDist; }
            set { _strDist = value; }
        }
        private string _strProv;

        public string strProv
        {
            get { return _strProv; }
            set { _strProv = value; }
        }
        public string Lista
        {
            get { return lista; }
            set { lista = value; }
        }
        private string strCod_prov;
        private string strCod_dist;
        private int intEmpleos;
        private string strFecha_viabilidad;

        private string _strFechaUpdateMEF;
        public string strFechaUpdateMEF
        {
            get { return _strFechaUpdateMEF; }
            set { _strFechaUpdateMEF = value; }
        }

        private int intId_WS;
        private int intMes;

        private string strCodigo;

        private int intIdEstrategia;

        private double dblMonto_ejec_2006;
        private double dblMonto_ejec_2007;
        private double dblMonto_ejec_2008;
        private double dblMonto_ejec_2009;
        private double dblMonto_inv_2006;
        private double dblMonto_inv_2007;
        private double dblMonto_inv_2008;
        private double dblMonto_inv_2009;
        private double dblMonto_snip;
        private double dblMonto_obra;
        private double dblMonto_Supervision;
        private string strNombre;
        private int intNro_pips;
        private string strObservacion;
        private int intTipo_Proy;
        private string strUsuario;
        private int intIntervencion;
        private int intCod_subprograma;
        private int intFoto;
        private int intCod_Sector;
        private int intCod_SubEstado;
        private int intCod_perfil_usuario;
        private string strDS;
        private Int64 intidsolicitudes;
        private int inttipo;
        private int intIdCoordinador;
        private int intIdtecnico;
        private int intCod_estadoTecnico;
        private string strcoordinador;
        private string strdocumentacion;
        private int intporcentaje;
        private int intperfil;
        private double dblMonto_Inversion;
        private int inttipotranf;
        private int intid_documentacion;

        private int intagua_nuevas;
        private int intagua_rehabilitacion;
        private int intagua_piletas;
        private int intdesague;
        private int intletrinas;
        private double dblpistas;
        private double dblveredas;
        private double dblparques;
        private string strprioridad;
        private int intprioridad;
        private string strinauguracion;

        private int intidestado_snip;
        private string strestado_snip;

        private string strresp_elaboracion;
        private string strresp_supervision;
        private string strfecha_pres_obra;
        private int intid_ambito;

        private double dblobra_total;
        private double dblsupervision_total;

        private double _poblacion;

        private int _user;

        private string _motivo;

        private string _crecer;

        private string _flagFonie;
        private string _flagRevisorFonie;

        private string _nivelViabilidad;

        public string nivelViabilidad
        {
            get { return _nivelViabilidad; }
            set { _nivelViabilidad = value; }
        }
        private string _funcion;

        public string funcion
        {
            get { return _funcion; }
            set { _funcion = value; }
        }
        private string _programa;

        public string programa
        {
            get { return _programa; }
            set { _programa = value; }
        }
        private string _subPrograma;

        public string subPrograma
        {
            get { return _subPrograma; }
            set { _subPrograma = value; }
        }
        private string _sectorUF;

        public string sectorUF
        {
            get { return _sectorUF; }
            set { _sectorUF = value; }
        }
        private string _pliegoUF;

        public string pliegoUF
        {
            get { return _pliegoUF; }
            set { _pliegoUF = value; }
        }

        public string FlagRevisorFonie
        {
            get { return _flagRevisorFonie; }
            set { _flagRevisorFonie = value; }
        }
        private string _PoblacionPromedio;

        private string _sector;

        private string fechaSITRAD;

        public string FechaSITRAD
        {
            get { return fechaSITRAD; }
            set { fechaSITRAD = value; }
        }
        public string Sector
        {
            get { return _sector; }
            set { _sector = value; }
        }

        public string PoblacionPromedio
        {
            get { return _PoblacionPromedio; }
            set { _PoblacionPromedio = value; }
        }

        public string FlagFonie
        {
            get { return _flagFonie; }
            set { _flagFonie = value; }
        }
        public string crecer
        {
            get { return _crecer; }
            set { _crecer = value; }
        }
        private string _sisfoh;

        public string sisfoh
        {
            get { return _sisfoh; }
            set { _sisfoh = value; }
        }
        private string _fonie;

        public string fonie
        {
            get { return _fonie; }
            set { _fonie = value; }
        }
        private string _vrae;

        public string vrae
        {
            get { return _vrae; }
            set { _vrae = value; }
        }
        private string _vahpp;

        public string vahpp
        {
            get { return _vahpp; }
            set { _vahpp = value; }
        }
        public string MOTIVO
        {
            get { return _motivo; }
            set { _motivo = value; }
        }


        public int USER
        {
            get { return _user; }
            set { _user = value; }
        }

        public double poblacion
        {

            get { return _poblacion; }
            set { _poblacion = value; }
        }

        private int _iPoblacion;

        private string _estado;

        public string estado
        {
            get { return _estado; }
            set { _estado = value; }
        }

        public int iPoblacion
        {
            get { return _iPoblacion; }
            set { _iPoblacion = value; }
        }

        private double _ratio;
        public double ratio
        {

            get { return _ratio; }
            set { _ratio = value; }
        }

        private string _idfinancimiento;

        public string IDFINANCIMIENTO
        {
            get { return _idfinancimiento; }
            set { _idfinancimiento = value; }
        }
        #endregion

        private int _id_ModalidadFinanciamiento;
        private int _id_SubModalidadFinanciamiento;

        private Int32 _flagCondicionado;

        public Int32 flagCondicionado
        {
            get { return _flagCondicionado; }
            set { _flagCondicionado = value; }
        }
        private string _flagAntiguo;

        public string FlagAntiguo
        {
            get { return _flagAntiguo; }
            set { _flagAntiguo = value; }
        }

        private int _idTipoProyecto;

        public int idTipoProyecto
        {
            get { return _idTipoProyecto; }
            set { _idTipoProyecto = value; }
        }

        private int _idProceso;




        #region Propiedades

        public double Supervision_Total
        {
            get { return dblsupervision_total; }
            set { dblsupervision_total = value; }
        }

        public string Codigo
        {
            get { return strCodigo; }
            set { strCodigo = value; }
        }

        public double Obra_Total
        {
            get { return dblobra_total; }
            set { dblobra_total = value; }
        }


        public int Id_Ambito
        {
            get { return intid_ambito; }
            set { intid_ambito = value; }
        }

        public string Fecha_Pres_Obra
        {
            get { return strfecha_pres_obra; }
            set { strfecha_pres_obra = value; }
        }

        public string Resp_Supervision
        {
            get { return strresp_supervision; }
            set { strresp_supervision = value; }
        }

        public string Resp_Elaboracion
        {
            get { return strresp_elaboracion; }
            set { strresp_elaboracion = value; }
        }

        public int IdEstado_Snip
        {
            get { return intidestado_snip; }
            set { intidestado_snip = value; }
        }

        public string Estado_Snip
        {
            get { return strestado_snip; }
            set { strestado_snip = value; }
        }

        public string Inauguracion
        {
            get { return strinauguracion; }
            set { strinauguracion = value; }
        }
        public int IntPrioridad
        {
            get { return intprioridad; }
            set { intprioridad = value; }
        }

        public string StrPrioridad
        {
            get { return strprioridad; }
            set { strprioridad = value; }
        }

        public double Parques
        {
            get { return dblparques; }
            set { dblparques = value; }
        }

        public double Veredas
        {
            get { return dblveredas; }
            set { dblveredas = value; }
        }

        public double Pistas
        {
            get { return dblpistas; }
            set { dblpistas = value; }
        }

        public int Letrinas
        {
            get { return intletrinas; }
            set { intletrinas = value; }
        }

        public int Desague
        {
            get { return intdesague; }
            set { intdesague = value; }
        }

        public int Agua_Piletas
        {
            get { return intagua_piletas; }
            set { intagua_piletas = value; }
        }

        public int Agua_Rehabilitacion
        {
            get { return intagua_rehabilitacion; }
            set { intagua_rehabilitacion = value; }
        }

        public int Agua_Nuevas
        {
            get { return intagua_nuevas; }
            set { intagua_nuevas = value; }
        }

        public int Id_Documentacio
        {
            get { return intid_documentacion; }
            set { intid_documentacion = value; }
        }
        public int TipoTransf
        {
            get { return inttipotranf; }
            set { inttipotranf = value; }
        }
        public double Monto_Inversion
        {
            get { return dblMonto_Inversion; }
            set { dblMonto_Inversion = value; }
        }
        public int Perfil
        {
            get { return intperfil; }
            set { intperfil = value; }
        }
        public string Documentacion
        {
            get { return strdocumentacion; }
            set { strdocumentacion = value; }
        }
        public int Porcentaje
        {
            get { return intporcentaje; }
            set { intporcentaje = value; }
        }
        public double Monto_Supervision
        {
            get { return dblMonto_Supervision; }
            set { dblMonto_Supervision = value; }
        }
        public double Monto_Obra
        {
            get { return dblMonto_obra; }
            set { dblMonto_obra = value; }
        }
        public string Coordinador
        {
            get { return strcoordinador; }
            set { strcoordinador = value; }
        }
        public int IdCoordinador
        {
            get { return intIdCoordinador; }
            set { intIdCoordinador = value; }
        }
        public int IdTecnico
        {
            get { return intIdtecnico; }
            set { intIdtecnico = value; }
        }

        public int IdEstrategia
        {
            get { return intIdEstrategia; }
            set { intIdEstrategia = value; }
        }

        public int Cod_EstadoTecnico
        {
            get { return intCod_estadoTecnico; }
            set { intCod_estadoTecnico = value; }
        }

        public Int64 Id_Solicitudes
        {
            get { return intidsolicitudes; }
            set { intidsolicitudes = value; }
        }
        public int Tipo
        {
            get { return inttipo; }
            set { inttipo = value; }
        }

        public string Criterio
        {
            get { return strCriterio; }
            set { strCriterio = value; }
        }
        public string Buscar
        {
            get { return strBuscar; }
            set { strBuscar = value; }
        }
        public int Id_usuario
        {
            get { return intId_usuario; }
            set { intId_usuario = value; }
        }
        public int Cod_tipo_usuario
        {
            get { return intCod_tipo_usuario; }
            set { intCod_tipo_usuario = value; }
        }
        public int Cod_subsector
        {
            get { return intCod_subsector; }
            set { intCod_subsector = value; }
        }
        public int Cod_subsector_filtro
        {
            get { return intCod_subsector_filtro; }
            set { intCod_subsector_filtro = value; }
        }
        public int Cod_programa
        {
            get { return intCod_programa; }
            set { intCod_programa = value; }
        }
        public int Cod_programa_filtro
        {
            get { return intCod_programa_filtro; }
            set { intCod_programa_filtro = value; }
        }
        public int Cod_prioridad
        {
            get { return intCod_prioridad; }
            set { intCod_prioridad = value; }
        }
        public int Cod_estrategia
        {
            get { return intCod_estrategia; }
            set { intCod_estrategia = value; }
        }
        public int Cod_clasificacion
        {
            get { return intCod_clasificacion; }
            set { intCod_clasificacion = value; }
        }
        public string Cod_depa
        {
            get { return strCod_depa; }
            set { strCod_depa = value; }
        }
        public int Periodo
        {
            get { return intPeriodo; }
            set { intPeriodo = value; }
        }
        public int Cod_estado
        {
            get { return intCod_estado; }
            set { intCod_estado = value; }
        }

        public int Id_proyecto
        {
            get { return intId_proyecto; }
            set { intId_proyecto = value; }
        }
        public string Ambito
        {
            get { return strAmbito; }
            set { strAmbito = value; }
        }
        public int Benef_direc
        {
            get { return intBenef_direc; }
            set { intBenef_direc = value; }
        }
        public int Benef_indir
        {
            get { return intBenef_indir; }
            set { intBenef_indir = value; }
        }
        public int Cod_snip
        {
            get { return intCod_snip; }
            set { intCod_snip = value; }
        }

        public int CodUnificado
        {
            get { return intCodUnificado; }
            set { intCodUnificado = value; }
        }
        public string Entidad_ejec
        {
            get { return strEntidad_ejec; }
            set { strEntidad_ejec = value; }
        }
        public string Cod_prov
        {
            get { return strCod_prov; }
            set { strCod_prov = value; }
        }
        public string Cod_dist
        {
            get { return strCod_dist; }
            set { strCod_dist = value; }
        }
        public int Empleos
        {
            get { return intEmpleos; }
            set { intEmpleos = value; }
        }
        public string Fecha_viabilidad
        {
            get { return strFecha_viabilidad; }
            set { strFecha_viabilidad = value; }
        }
        public int Id_WS
        {
            get { return intId_WS; }
            set { intId_WS = value; }
        }
        public int Mes
        {
            get { return intMes; }
            set { intMes = value; }
        }
        public double Monto_ejec_2006
        {
            get { return dblMonto_ejec_2006; }
            set { dblMonto_ejec_2006 = value; }
        }
        public double Monto_ejec_2007
        {
            get { return dblMonto_ejec_2007; }
            set { dblMonto_ejec_2007 = value; }
        }
        public double Monto_ejec_2008
        {
            get { return dblMonto_ejec_2008; }
            set { dblMonto_ejec_2008 = value; }
        }
        public double Monto_ejec_2009
        {
            get { return dblMonto_ejec_2009; }
            set { dblMonto_ejec_2009 = value; }
        }
        public double Monto_inv_2006
        {
            get { return dblMonto_inv_2006; }
            set { dblMonto_inv_2006 = value; }
        }
        public double Monto_inv_2007
        {
            get { return dblMonto_inv_2007; }
            set { dblMonto_inv_2007 = value; }
        }
        public double Monto_inv_2008
        {
            get { return dblMonto_inv_2008; }
            set { dblMonto_inv_2008 = value; }
        }
        public double Monto_inv_2009
        {
            get { return dblMonto_inv_2009; }
            set { dblMonto_inv_2009 = value; }
        }
        public double Monto_snip
        {
            get { return dblMonto_snip; }
            set { dblMonto_snip = value; }
        }


        public string Nombre
        {
            get { return strNombre; }
            set { strNombre = value; }
        }
        public int Nro_pips
        {
            get { return intNro_pips; }
            set { intNro_pips = value; }
        }
        public string Observacion
        {
            get { return strObservacion; }
            set { strObservacion = value; }
        }
        public int Tipo_Proy
        {
            get { return intTipo_Proy; }
            set { intTipo_Proy = value; }
        }
        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }
        public int Intervencion
        {
            get { return intIntervencion; }
            set { intIntervencion = value; }
        }
        public int Cod_subprograma
        {
            get { return intCod_subprograma; }
            set { intCod_subprograma = value; }
        }
        public int Foto
        {
            get { return intFoto; }
            set { intFoto = value; }
        }
        public int Cod_Sector
        {
            get { return intCod_Sector; }
            set { intCod_Sector = value; }
        }
        public int Cod_SubEstado
        {
            get { return intCod_SubEstado; }
            set { intCod_SubEstado = value; }
        }
        public int Cod_perfil_usuario
        {
            get { return intCod_perfil_usuario; }
            set { intCod_perfil_usuario = value; }
        }
        public string DS
        {
            get { return strDS; }
            set { strDS = value; }
        }



        #endregion


        private string _codigoPostal;

        public string codigoPostal
        {
            get { return _codigoPostal; }
            set { _codigoPostal = value; }
        }
        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _direccion;

        public string direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        private string _fax;

        public string fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        private string _cargo;

        public string cargo
        {
            get { return _cargo; }
            set { _cargo = value; }
        }

        private int _totalFolios;

        public int TotalFolios
        {
            get { return _totalFolios; }
            set { _totalFolios = value; }
        }
        private DateTime _fecha;

        public string fecha
        {
            get { return _fecha.ToString("dd/MM/yyyy"); }
            set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        private string _municipalidad;

        public string Municipalidad
        {
            get { return _municipalidad; }
            set { _municipalidad = value; }
        }
        private string _alcalde;

        public string Alcalde
        {
            get { return _alcalde; }
            set { _alcalde = value; }
        }
        private string _correoAlcalde;

        public string CorreoAlcalde
        {
            get { return _correoAlcalde; }
            set { _correoAlcalde = value; }
        }
        private string _telefonoAlcalde;

        public string TelefonoAlcalde
        {
            get { return _telefonoAlcalde; }
            set { _telefonoAlcalde = value; }
        }
        private string _responsable;

        public string Responsable
        {
            get { return _responsable; }
            set { _responsable = value; }
        }
        private string _correoResponsable;

        public string CorreoResponsable
        {
            get { return _correoResponsable; }
            set { _correoResponsable = value; }
        }
        private string _telefonoResponsable;

        public string TelefonoResponsable
        {
            get { return _telefonoResponsable; }
            set { _telefonoResponsable = value; }
        }
        private double _costoProyecto;

        public double CostoProyecto
        {
            get { return _costoProyecto; }
            set { _costoProyecto = value; }
        }
        //private int _poblacion;

        //public int Poblacion
        //{
        //    get { return _poblacion; }
        //    set { _poblacion = value; }
        //}
        private string _comentario;

        public string Comentario
        {
            get { return _comentario; }
            set { _comentario = value; }
        }

        private int _chek49;

        public int Chek49
        {
            get { return _chek49; }
            set { _chek49 = value; }
        }

        public int id_Proceso
        {
            get
            {
                return _idProceso;
            }

            set
            {
                _idProceso = value;
            }
        }

        public int id_EstadoProceso
        {
            get
            {
                return _id_EstadoProceso;
            }

            set
            {
                _id_EstadoProceso = value;
            }
        }

        public int id_ModalidadFinanciamiento
        {
            get
            {
                return _id_ModalidadFinanciamiento;
            }

            set
            {
                _id_ModalidadFinanciamiento = value;
            }
        }

        public int id_SubModalidadFinanciamiento
        {
            get
            {
                return _id_SubModalidadFinanciamiento;
            }

            set
            {
                _id_SubModalidadFinanciamiento = value;
            }
        }

        private int _id_EstadoProceso;

        public string Expediente_Sitrad { get; set; }
    }

    public class BEArchivoPRESET
    {
        private string _vDescReqMin;
        private string _xidReqMin;
        private string _vDescCriterio;
        private string _xidCriterio;
        private string _vValorEvaluacion;
        private string _xidEvaluacion;

        public string vDescReqMin
        {
            get
            {
                return _vDescReqMin;
            }

            set
            {
                _vDescReqMin = value;
            }
        }

        public string xidReqMin
        {
            get
            {
                return _xidReqMin;
            }

            set
            {
                _xidReqMin = value;
            }
        }

        public string vDescCriterio
        {
            get
            {
                return _vDescCriterio;
            }

            set
            {
                _vDescCriterio = value;
            }
        }

        public string xidCriterio
        {
            get
            {
                return _xidCriterio;
            }

            set
            {
                _xidCriterio = value;
            }
        }

        public string vValorEvaluacion
        {
            get
            {
                return _vValorEvaluacion;
            }

            set
            {
                _vValorEvaluacion = value;
            }
        }

        public string xidEvaluacion
        {
            get
            {
                return _xidEvaluacion;
            }

            set
            {
                _xidEvaluacion = value;
            }
        }


    }

    
}
