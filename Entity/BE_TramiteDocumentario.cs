﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_TramiteDocumentario
    {
        public string DNIREMITENTE { get; set; }
        public Int32? COD_DOCUMENTO { get; set; }
        public Int32? IDAREAREMITE { get; set; }
        public Int32? NUMERO_DOCUMENTO { get; set; }
        public string SNIP { get; set; }
        public Int32 FOLIOS { get; set; }
        public string DNIDESTINATARIO { get; set; }
        public Int32? IDAREADESTINATARIO { get; set; }
        public string DESTINATARIOEXTERNO { get; set; }
        public string ASUNTO { get; set; }
        public string OBSERVACIONES { get; set; }
        public string Nomenclatura { get; set; }
        public string NRO_TRAMITE { get; set; }
        public string ID_ACCION { get; set; }
        public string CONSINDOCUMENTO { get; set; }
        public Int32 Reingreso { get; set; }
        public string REMITENTEEXTERNO { get; set; }
        public int ESTADO { get; set; }
    }
}
