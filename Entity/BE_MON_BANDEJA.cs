﻿using System;
using System.Collections.Generic;

namespace Entity
{
    public class BE_MON_BANDEJA
    {
        private int _metaRealizada;

        public int metaRealizada
        {
            get { return _metaRealizada; }
            set { _metaRealizada = value; }
        }

        private int _meta;

        public string subPrograma { get; set; }
        public int meta
        {
            get { return _meta; }
            set { _meta = value; }
        }

        private string _observacion;

        public string observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }
        private string _correo;

        public string correo
        {
            get { return _correo; }
            set { _correo = value; }
        }

        private string _nivel;

        public string nivel
        {
            get { return _nivel; }
            set { _nivel = value; }
        }
        private int _valor;

        public int valor
        {
            get { return _valor; }
            set { _valor = value; }
        }
        private string _nombre;


        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        private string _tecnico;

        public string tecnico
        {
            get { return _tecnico; }
            set { _tecnico = value; }
        }

        private string _codigo;

        public string codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }
        private int _id_proyecto;
        private int _tipoFinanciamiento;
        private int _id_usuario;
        private int _estadoProyecto;
        private string _tipo;
        private int _id_tecnico;
        private string _nombProyecto;
        private string _tecnicofiltro;
        private string _snip;
        private string _depa;
        private string _prov;
        private string _dist;
        private string _sector;
        private string _tipoFinanciamientoFiltro;
        private int _estrategia;
        private string _anio;
        private string _estado;
        private string _strEstrategia;
        private string _unidadEjecutora;
        private string _costo_MVCS;
        private string _tipoEstadoEjecucion;
        private string _strIdSolicitudes;
        private string _infoEstudios;
        private string _Preinversion;
        private string _Espediente;
        private string _Obra;
        private string urlDoc;
        private int _id_sector;
        private int _id;
        private string _centro_poblado;

        private int _flagEmergencia;

        private int _idTipoInversion;

        public string Mantenimiento { get; set; }

        public string centro_poblado
        {
            get { return _centro_poblado; }
            set { _centro_poblado = value; }
        }

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _unidadFormuladora;

        private string _ubigeoCCPP;

        public string ubigeoCCPP
        {
            get { return _ubigeoCCPP; }
            set { _ubigeoCCPP = value; }
        }

        public string unidadFormuladora
        {
            get { return _unidadFormuladora; }
            set { _unidadFormuladora = value; }
        }
        private string _tambo;

        public string tambo
        {
            get { return _tambo; }
            set { _tambo = value; }
        }

        private string _tipoTipologia;

        public string tipoTipologia
        {
            get { return _tipoTipologia; }
            set { _tipoTipologia = value; }
        }
        private int _flagActivo;

        public int flagActivo
        {
            get { return _flagActivo; }
            set { _flagActivo = value; }
        }
        private string _CCPP;

        public string CCPP
        {
            get { return _CCPP; }
            set { _CCPP = value; }
        }
        public int id_sector
        {
            get { return _id_sector; }
            set { _id_sector = value; }
        }

        public string Preinversion
        {
            get { return _Preinversion; }
            set { _Preinversion = value; }
        }

        public string Espediente
        {
            get { return _Espediente; }
            set { _Espediente = value; }
        }


        public string Obra
        {
            get { return _Obra; }
            set { _Obra = value; }
        }

        public string infoEstudios
        {
            get { return _infoEstudios; }
            set { _infoEstudios = value; }
        }

        public string strIdSolicitudes
        {
            get { return _strIdSolicitudes; }
            set { _strIdSolicitudes = value; }
        }
        public string tipoEstadoEjecucion
        {
            get { return _tipoEstadoEjecucion; }
            set { _tipoEstadoEjecucion = value; }
        }


        public string costo_MVCS
        {
            get { return _costo_MVCS; }
            set { _costo_MVCS = value; }
        }
        public string unidadEjecutora
        {
            get { return _unidadEjecutora; }
            set { _unidadEjecutora = value; }
        }

        public string StrEstrategia
        {
            get { return _strEstrategia; }
            set { _strEstrategia = value; }
        }

        public string Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }

        public string Anio
        {
            get { return _anio; }
            set { _anio = value; }
        }

        public int Estrategia
        {
            get { return _estrategia; }
            set { _estrategia = value; }
        }

        public string tecnicofiltro
        {
            get { return _tecnicofiltro; }
            set { _tecnicofiltro = value; }
        }

        public string snip
        {
            get { return _snip; }
            set { _snip = value; }
        }

        public string depa
        {
            get { return _depa; }
            set { _depa = value; }
        }
        public string prov
        {
            get { return _prov; }
            set { _prov = value; }
        }
        public string dist
        {
            get { return _dist; }
            set { _dist = value; }
        }
        public string sector
        {
            get { return _sector; }
            set { _sector = value; }
        }
        public string tipoFinanciamientoFiltro
        {
            get { return _tipoFinanciamientoFiltro; }
            set { _tipoFinanciamientoFiltro = value; }
        }

        private string _modalidadFinanciamiento;

        public string modalidadFinanciamiento
        {
            get { return _modalidadFinanciamiento; }
            set { _modalidadFinanciamiento = value; }
        }
        public string nombProyecto
        {
            get { return _nombProyecto; }
            set { _nombProyecto = value; }
        }


        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

        public int tipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        public int estadoProyecto
        {
            get { return _estadoProyecto; }
            set { _estadoProyecto = value; }
        }

        public string tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public int id_tecnico
        {
            get { return _id_tecnico; }
            set { _id_tecnico = value; }
        }

        private int id_tipo;

        public int Id_tipo
        {
            get { return id_tipo; }
            set { id_tipo = value; }
        }
        private string nombreArchivo;

        public string NombreArchivo
        {
            get { return nombreArchivo; }
            set { nombreArchivo = value; }
        }
        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }


        public string UrlDoc
        {
            get { return urlDoc; }
            set { urlDoc = value; }
        }
        private DateTime _fecha2;

        public string Fecha2
        {
            get { return _fecha2.ToString("dd/MM/yyyy"); }
            set { _fecha2 = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fecha
        {
            get { return _fecha2; }
            set { _fecha2 = value; }
        }
        private string _fecha;

        public string fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        private int _id_documentoMonitor;

        public int Id_documentoMonitor
        {
            get { return _id_documentoMonitor; }
            set { _id_documentoMonitor = value; }
        }


        private Int64 _numero;

        public Int64 numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        private string _usuario;

        public string usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }
        private DateTime? _fecha_update;

        public DateTime? fecha_update
        {
            get { return _fecha_update; }
            set { _fecha_update = value; }
        }
        private string _id_tipoDocumentoMonitor;

        public string id_tipoDocumentoMonitor
        {
            get { return _id_tipoDocumentoMonitor; }
            set { _id_tipoDocumentoMonitor = value; }
        }

        private string _tipoDocumentacion;

        public string tipoDocumentacion
        {
            get { return _tipoDocumentacion; }
            set { _tipoDocumentacion = value; }
        }

        private int _id_motivoDevolucion;

        public int id_motivoDevolucion
        {
            get { return _id_motivoDevolucion; }
            set { _id_motivoDevolucion = value; }
        }
        private int _id_tipoDevolucion;

        public int id_tipoDevolucion
        {
            get { return _id_tipoDevolucion; }
            set { _id_tipoDevolucion = value; }
        }
        private string _nroTramite;

        private string _tipoDevolucion;

        public string tipoDevolucion
        {
            get { return _tipoDevolucion; }
            set { _tipoDevolucion = value; }
        }
        private string _motivoDevolucion;

        public string motivoDevolucion
        {
            get { return _motivoDevolucion; }
            set { _motivoDevolucion = value; }
        }

        public string nroTramite
        {
            get { return _nroTramite; }
            set { _nroTramite = value; }
        }
        private string _nroOficio;

        public string nroOficio
        {
            get { return _nroOficio; }
            set { _nroOficio = value; }
        }

        private string _monto_SNIP;

        public string monto_SNIP
        {
            get { return _monto_SNIP; }
            set { _monto_SNIP = value; }
        }

        private string _fisicoEjecutado;

        public string fisicoEjecutado
        {
            get { return _fisicoEjecutado; }
            set { _fisicoEjecutado = value; }
        }

        private string _strFechaFinContractual;

        public string strFechaFinContractual
        {
            get { return _strFechaFinContractual; }
            set { _strFechaFinContractual = value; }
        }
        private string _strFechaFinReal;

        public string strFechaFinReal
        {
            get { return _strFechaFinReal; }
            set { _strFechaFinReal = value; }
        }

        private DateTime? _dtFechaInauguracion;

        public DateTime? dtFechaInauguracion
        {
            get { return _dtFechaInauguracion; }
            set { _dtFechaInauguracion = value; }
        }

        private string _monto_inversion;

        public string monto_inversion
        {
            get { return _monto_inversion; }
            set { _monto_inversion = value; }
        }
        private string _monto_transferencia;
        public string monto_transferencia
        {
            get { return _monto_transferencia; }
            set { _monto_transferencia = value; }
        }

        private string _nroDS;
        public string nroDS
        {
            get { return _nroDS; }
            set { _nroDS = value; }
        }

        public Int32 poblacionSnip
        {
            get { return _poblacionSNIP; }
            set { _poblacionSNIP = value; }
        }

        public Int32 id_TipoEstadoEjecucion
        {
            get { return _id_tipoEstadoEjecucion; }
            set { _id_tipoEstadoEjecucion = value; }
        }

        public string horaInicio
        {
            get { return _horaInicio; }
            set { _horaInicio = value; }
        }

        public string representanteMVCS
        {
            get { return _representanteMVCS; }
            set { _representanteMVCS = value; }
        }

        public string imgFicha
        {
            get { return _imgFicha; }
            set { _imgFicha = value; }
        }

        public string imgExtension
        {
            get { return _imgExtension; }
            set { _imgExtension = value; }
        }

        private Int32 _poblacionSNIP;
        private Int32 _id_tipoEstadoEjecucion;
        private string _horaInicio;
        private string _representanteMVCS;
        private string _imgFicha;
        private string _imgExtension;
        private string _FlagCMD;
        public string flagCMD
        {
            get { return _FlagCMD; }
            set { _FlagCMD = value; }
        }

        private decimal _UTMX;

        public decimal UTMX
        {
            get { return _UTMX; }
            set { _UTMX = value; }
        }
        private decimal _UTMY;

        public decimal UTMY
        {
            get { return _UTMY; }
            set { _UTMY = value; }
        }
        private decimal _UTMZ;

        public decimal UTMZ
        {
            get { return _UTMZ; }
            set { _UTMZ = value; }
        }
        private int _tipoDenominacion;

        public int tipoDenominacion
        {
            get { return _tipoDenominacion; }
            set { _tipoDenominacion = value; }
        }
        private decimal _caudalFuente;

        public decimal caudalFuente
        {
            get { return _caudalFuente; }
            set { _caudalFuente = value; }
        }
        private decimal _nroColiformesFuente;

        public decimal nroColiformesFuente
        {
            get { return _nroColiformesFuente; }
            set { _nroColiformesFuente = value; }
        }
        private string _resultadoAnalisisFuente;

        public string resultadoAnalisisFuente
        {
            get { return _resultadoAnalisisFuente; }
            set { _resultadoAnalisisFuente = value; }
        }

        private int _ambito;

        public int ambito
        {
            get { return _ambito; }
            set { _ambito = value; }
        }

        private int _id_tipoModalidad;

        public int id_tipoModalidad
        {
            get { return _id_tipoModalidad; }
            set { _id_tipoModalidad = value; }
        }

        public int id_checklist
        {
            get
            {
                return _id_checklist;
            }

            set
            {
                _id_checklist = value;
            }
        }

        public int id_archivo
        {
            get
            {
                return _id_archivo;
            }

            set
            {
                _id_archivo = value;
            }
        }

        public string etapa
        {
            get
            {
                return _etapa;
            }

            set
            {
                _etapa = value;
            }
        }

        public int flagSaldo
        {
            get
            {
                return _flagSaldo;
            }

            set
            {
                _flagSaldo = value;
            }
        }

        private int _flagSaldoObraExpediente;

        public int flagSaldoObraExpediente
        {
            get
            {
                return _flagSaldoObraExpediente;
            }

            set
            {
                _flagSaldoObraExpediente = value;
            }
        }

        public int flagEtapa
        {
            get
            {
                return _flagEtapa;
            }

            set
            {
                _flagEtapa = value;
            }
        }

        public string orden
        {
            get
            {
                return _orden;
            }

            set
            {
                _orden = value;
            }
        }

        private int _id_checklist;
        private int _id_archivo;

        private string _etapa;
        private int _flagSaldo;
        private int _flagEtapa;

        private string _orden;
        public Boolean flagReconstruccion { get; set; }
        public Boolean flagServicio { get; set; }

        public String vNroPreset { get; set; }
        public String vDescripcionItemPreset { get; set; }

        public int id_subPrograma { get; set; }

        public List<Ubigeos> ListaUbigeos { get; set; }
        public List<CodigosSeace> ListaCodigosSeace { get; set; }
        public List<CodigosARCC> ListaCodigosARCC { get; set; }
        public int? CodigoIdea { get; set; }
        public int? CodigoUnificado { get; set; }
        public int flagEmergencia { get => _flagEmergencia; set => _flagEmergencia = value; }

        public int? id_declaratoria { get; set; }
        public string Declaratoria { get; set; }

        public string Subsector { get; set; }
        public int idTipoInversion { get => _idTipoInversion; set => _idTipoInversion = value; }
    }

    public class Ubigeos {
       
        public Int64 id_proyecto { get; set; }
        public string CCPP { get; set; }
        public int id_usuario { get; set; }
        public int flagActivo { get; set; }
    }

    public class CodigosSeace
    {

        public Int64 id_proyecto { get; set; }
        public Int64 idEtapa { get; set; }
        public int Codigo { get; set; }
        public bool Activo { get; set; }
        public int id_usuario { get; set; }
    }

    public class CodigosARCC
    {
        public Int64 id_proyecto { get; set; }
        public string Codigo { get; set; }
        public bool Activo { get; set; }
    }


    public class ConsultaMonitoreo
    {
        public string snip { get; set; }
        public string unico { get; set; }
        public string nomProyecto { get; set; }
        public string depa { get; set; }
        public string prov { get; set; }
        public string dist { get; set; }
        public string unidad { get; set; }
        public string tecnico { get; set; }
        public string etapaProyecto { get; set; }
        public string nomEstado { get; set; }
        public string nroPagina { get; set; }
        public string tamanioPagina { get; set; }
    }
}
