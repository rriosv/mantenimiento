using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEPresupuesto
    {
        #region Atributos
        private Int64 _id_presupuesto;
        private string _siglas;
        private int _periodo;
        private string _fecha;
        private double _monto;
        private double _aumento;
        private double _descuento;
        private double _monto_calculado;
        private int _cod_subsector;
        private int _id_usuario;
        private int _tipo;
        private string _descripcion;
        private Int64 _id_solicitudes;
        #endregion

        #region Propiedades

        public Int64 Id_Solcitudes
        {
            get { return _id_solicitudes; }
            set { _id_solicitudes = value; }
        }

        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        public int Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public int Id_Usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        public Int64 Id_Presupuesto
        {
            get { return _id_presupuesto; }
            set { _id_presupuesto = value; }
        }

        public string Siglas
        {
            get { return _siglas; }
            set { _siglas = value; }
        }

        public int Periodo
        {
            get { return _periodo; }
            set { _periodo = value; }
        }

        public string Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public double Monto
        {
            get { return _monto; }
            set { _monto = value; }
        }

        public double Aumento
        {
            get { return _aumento; }
            set { _aumento = value; }
        }

        public double Descuento
        {
            get { return _descuento; }
            set { _descuento = value; }
        }

        public double Monto_Calculado
        {
            get { return _monto_calculado; }
            set { _monto_calculado = value; }
        }

        public int Cod_SubSector
        {
            get { return _cod_subsector; }
            set { _cod_subsector = value; }
        }
        #endregion

    }
}
