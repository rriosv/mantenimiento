using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BEDocumento
    {
        #region Atributos
        private Int64 intid_solicitudes;
        private Int64 intid_doc;
        private string strfecha;
        private string strdocumento;
        private string strdescripcion;
        private int intid_usuario;
        private int tipoDoc;

        public int TipoDoc
        {
            get { return tipoDoc; }
            set { tipoDoc = value; }
        }

        #endregion
        #region Propiedades
        public int Id_Usuario
        {
            get { return intid_usuario; }
            set { intid_usuario = value; }
        }
        public string Descripcion
        {
            get { return strdescripcion; }
            set { strdescripcion = value; }
        }
        public string Documento
        {
            get { return strdocumento; }
            set { strdocumento = value; }
        }
        public string Fecha
        {
            get { return strfecha; }
            set { strfecha = value; }
        }
        public Int64 Id_Solicitudes
        {
            get { return intid_solicitudes; }
            set { intid_solicitudes = value; }
        }
        public Int64 Id_Doc
        {
            get { return intid_doc; }
            set { intid_doc = value; }
        }
        #endregion
    }
}
