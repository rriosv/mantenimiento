﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_Item
    {
        public int IdItems { get; set; }
        public string vNro { get; set; }
        public string vDescripcion { get; set; }
        public bool bPadre { get; set; }
        public BE_Option objOption { get; set; }
        public List<BE_Cliche> listaCliches { get; set; }
        public int TieneHijos { get; set; }
    }
}
