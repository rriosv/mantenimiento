﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_Cliche
    {
        public int IdCliches { get; set; }
        public string vTipo { get; set; }
        public int IdItems { get; set; }
        public string vDescripcion { get; set; }
    }
}
