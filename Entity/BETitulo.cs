using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    [Serializable]
    public class BETitulo
    {
        #region Atributos
        private int intId_Titulos;
        private int intCod_Programa;
        private int intCod_Prioridad;
        private int intCod_Estrategia;
        private int intPeriodo;
        private int intMes;
        private int intDia;
        private string strCod_Dep;
        private string strCod_Prov;
        private string strCod_Dist;
        private string strUbigeo;
        private int intProd_urba_ins;
        private int intProd_urba_tit;
        private double dblInv_ejec_urb;
        private double dblBenef_directo_urb;
        private int intProd_rur_tit;
        private double dblInv_ejec_rur;
        private double dblBenef_directo_rur;
        private int intFlag_cierre;
        private string strNro_resolucion;
        private DateTime dtFecha_emision;
        private string strBeneficiario;
        private string strActo;
        private string strCod_registro;
        private string strEstado_reg;
        private double dblMonto_soles;
        private double dblMonto_dolar;
        private string strUsuario;
        private DateTime dtFechaCorte;
        private int intEstado;
        private DateTime dtFecha_reg;
        private DateTime dtFecha_Mod;
        private double dblArea;
        private int intSuma_reso;
        private int intSuma_benef;
        private double dblSuma_Soles;
        private double dblSuma_Dolares;
        private double dblSuma_Area;

        #endregion

        #region Constructores

        #endregion

        #region Propiedades
        public double Area
        {
            get { return dblArea; }
            set { dblArea = value; }
        }
        public int Id_Titulos
        {
            get { return intId_Titulos; }
            set { intId_Titulos = value; }
        }
        public int Cod_Programa
        {
            get { return intCod_Programa; }
            set { intCod_Programa = value; }
        }
        public int Cod_Prioridad
        {
            get { return intCod_Prioridad; }
            set { intCod_Prioridad = value; }
        }
        public int Cod_estrategia
        {
            get { return intCod_Estrategia; }
            set { intCod_Estrategia = value; }
        }
        public int Periodo
        {
            get { return intPeriodo; }
            set { intPeriodo = value; }
        }
        public int Mes
        {
            get { return intMes; }
            set { intMes = value; }
        }
        public int Dia
        {
            get { return intDia; }
            set { intDia = value; }
        }
        public string Cod_Dep
        {
            get { return strCod_Dep; }
            set { strCod_Dep = value; }
        }
        public string Cod_Prov
        {
            get { return strCod_Prov; }
            set { strCod_Prov = value; }
        }
        public string Cod_Dist
        {
            get { return strCod_Dist; }
            set { strCod_Dist = value; }
        }
        public string Ubigeo
        {
            get { return strUbigeo; }
            set { strUbigeo = value; }
        }
        public int Prod_urba_ins
        {
            get { return intProd_urba_ins; }
            set { intProd_urba_ins = value; }
        }
        public int Prod_urba_tit
        {
            get { return intProd_urba_tit; }
            set { intProd_urba_tit = value; }
        }
        public double Inv_ejec_urb
        {
            get { return dblInv_ejec_urb; }
            set { dblInv_ejec_urb = value; }
        }
        public double Benef_directo_urb
        {
            get { return dblBenef_directo_urb; }
            set { dblBenef_directo_urb = value; }
        }
        public int Prod_rur_tit
        {
            get { return intProd_rur_tit; }
            set { intProd_rur_tit = value; }
        }
        public double Inv_ejec_rur
        {
            get { return dblInv_ejec_rur; }
            set { dblInv_ejec_rur = value; }
        }
        public double Benef_directo_rur
        {
            get { return dblBenef_directo_rur; }
            set { dblBenef_directo_rur = value; }
        }
        public int Flag_cierre
        {
            get { return intFlag_cierre; }
            set { intFlag_cierre = value; }
        }
        public string Nro_resolucion
        {
            get { return strNro_resolucion; }
            set { strNro_resolucion = value; }
        }
        public DateTime Fecha_emision
        {
            get { return dtFecha_emision; }
            set { dtFecha_emision = value; }
        }
        public string Beneficiario
        {
            get { return strBeneficiario; }
            set { strBeneficiario = value; }
        }
        public string Acto
        {
            get { return strActo; }
            set { strActo = value; }
        }
        public string Cod_registro
        {
            get { return strCod_registro; }
            set { strCod_registro = value; }
        }
        public string Estado_reg
        {
            get { return strEstado_reg; }
            set { strEstado_reg = value; }
        }
        public double Monto_soles
        {
            get { return dblMonto_soles; }
            set { dblMonto_soles = value; }
        }
        public double Monto_dolar
        {
            get { return dblMonto_dolar; }
            set { dblMonto_dolar = value; }
        }
        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }
        public DateTime FechaCorte
        {
            get { return dtFechaCorte; }
            set { dtFechaCorte = value; }
        }
        public int Estado
        {
            get { return intEstado; }
            set { intEstado = value; }
        }
        public DateTime Fecha_reg
        {
            get { return dtFecha_reg; }
            set { dtFecha_reg = value; }
        }
        public DateTime Fecha_Mod
        {
            get { return dtFecha_Mod; }
            set { dtFecha_Mod = value; }
        }
        public int Suma_reso
        {
            get { return intSuma_reso; }
            set { intSuma_reso = value; }
        }
        public int Suma_benef
        {
            get { return intSuma_benef; }
            set { intSuma_benef = value; }
        }
        public double Suma_Dolares
        {
            get { return dblSuma_Dolares; }
            set { dblSuma_Dolares = value; }
        }
        public double Suma_Soles
        {
            get { return dblSuma_Soles; }
            set { dblSuma_Soles = value; }
        }
        public double Suma_Area
        {
            get { return dblSuma_Area; }
            set { dblSuma_Area = value; }
        }

     #endregion

    }
}
