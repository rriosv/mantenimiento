﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BE_MON_SOSEM
    {
        private int _id_proyecto;
        private int _tipoFinanciamiento;

        private int _id_seguimiento_sosem;
        private string _anio;
        private string _pia;
        private string _pimAcumulado;
        private string _devAcumulado;
        private string _ene;
        private string _feb;
        private string _mar;
        private string _abr;
        private string _may;
        private string _jun;
        private string _jul;
        private string _ago;
        private string _sep;
        private string _oct;
        private string _nov;
        private string _dic;

        private string _compromisoAnual;
        private string _certificado;
        private int _id_usuario;

        private string _usuario;

        public string usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }
        private string _strFechaUpdate;

        public string strFechaUpdate
        {
            get { return _strFechaUpdate; }
            set { _strFechaUpdate = value; }
        }

        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

 

        public int tipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }

        public int id_seguimiento_sosem
        {
            get { return _id_seguimiento_sosem; }
            set { _id_seguimiento_sosem = value; }
        }

        public string anio
        {
            get { return _anio; }
            set { _anio = value; }
        }

        public string pia
        {
            get { return _pia; }
            set { _pia = value; }
        }

        public string pimAcumulado
        {
            get { return _pimAcumulado; }
            set { _pimAcumulado = value; }
        }

        public string devAcumulado
        {
            get { return _devAcumulado; }
            set { _devAcumulado = value; }
        }

        public string ene
        {
            get { return _ene; }
            set { _ene = value; }
        }

        public string feb
        {
            get { return _feb; }
            set { _feb = value; }
        }

        public string mar
        {
            get { return _mar; }
            set { _mar = value; }
        }

        public string abr
        {
            get { return _abr; }
            set { _abr = value; }
        }
        public string may
        {
            get { return _may; }
            set { _may = value; }
        }
        public string jun
        {
            get { return _jun; }
            set { _jun = value; }
        }
        public string jul
        {
            get { return _jul; }
            set { _jul = value; }
        }
        public string ago
        {
            get { return _ago; }
            set { _ago = value; }
        }
        public string sep
        {
            get { return _sep; }
            set { _sep = value; }
        }
        public string oct
        {
            get { return _oct; }
            set { _oct = value; }
        }
        public string nov
        {
            get { return _nov; }
            set { _nov = value; }
        }
        public string dic
        {
            get { return _dic; }
            set { _dic = value; }
        }
        public string compromisoAnual
        {
            get { return _compromisoAnual; }
            set { _compromisoAnual = value; }
        }

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        private int _snip;

        public int snip
        {
            get { return _snip; }
            set { _snip = value; }
        }

        private int _idEjecutora;

        public int idEjecutora
        {
            get { return _idEjecutora; }
            set { _idEjecutora = value; }
        }

        private string _NombreEjecutora;

        public string nombreEjecutora
        {
            get { return _NombreEjecutora; }
            set { _NombreEjecutora = value; }
        }

        private string _fuenteFinanciamiento;

        public string fuenteFinanciamiento
        {
            get { return _fuenteFinanciamiento; }
            set { _fuenteFinanciamiento = value; }
        }

        public string certificado
        {
            get
            {
                return _certificado;
            }

            set
            {
                _certificado = value;
            }
        }
    }
}
