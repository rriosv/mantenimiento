﻿namespace Entity
{
    public class BE_MON_SEACE
    {
        private int _snip;
        private string _TipoSeleccion;
        private string _NOMENCLATURA;
        private string _FechaPublicacion;
        private string _EstadoItem;
        private string _FECHA_ACCION;
        private string _ValorReferencial;
        private string _DescripcionItem;
        private string _ValorReferencialItem;
        private int _item;
        private string _IDENTIFICADOR;
               
        //private string _ENTIDAD;
        private string _objeto;
        //private string _DescripcionProceso;
        //private string _MODALIDAD;
        //private string _NORMATIVA;
        //private string _fechaBuenaPro;
        //private string _fechaBuenaProConsentida;
        //private decimal _ValorAdjudicadoItem;
        //private string _Moneda;
        //private string _NroContrato;
        //private string _FechaContrato;
        //private string _urlContrato;
        
        public int snip
        {
            get { return _snip; }
            set { _snip = value; }
        }
        public string TipoSeleccion
        {
            get { return _TipoSeleccion; }
            set { _TipoSeleccion = value; }
        }
        
        public string NOMENCLATURA
        {
            get { return _NOMENCLATURA; }
            set { _NOMENCLATURA = value; }
        }

        public string FechaPublicacion
        {
            get { return _FechaPublicacion; }
            set { _FechaPublicacion = value; }
        }

        public string EstadoItem
        {
            get { return _EstadoItem; }
            set { _EstadoItem = value; }
        }

        public string FECHA_ACCION
        {
            get { return _FECHA_ACCION; }
            set { _FECHA_ACCION = value; }
        }

        public string ValorReferencial
        {
            get { return _ValorReferencial; }
            set { _ValorReferencial = value; }
        }

        public string DescripcionItem
        {
            get { return _DescripcionItem; }
            set { _DescripcionItem = value; }
        }

        public string ValorReferencialItem
        {
            get { return _ValorReferencialItem; }
            set { _ValorReferencialItem = value; }
        }

        public int item
        {
            get { return _item; }
            set { _item = value; }
        }
        public string IDENTIFICADOR
        {
            get { return _IDENTIFICADOR;  }
            set { _IDENTIFICADOR = value; }
        }

        public string Objeto { get => _objeto; set => _objeto = value; }
    }
}
