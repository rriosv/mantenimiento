﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_GrupoDetalle
    {
        private int _IdGrupoDetalle;
        private int _IdGrupo;
        private string _Nombre;
        private string _Codigo;
        private string _Descripcion;
        private bool _Activo;

        public int IdGrupoDetalle
        {
            get { return _IdGrupoDetalle; }
            set { _IdGrupoDetalle = value; }
        }
        
        public int IdGrupo
        {
            get { return _IdGrupo; }
            set { _IdGrupo = value; }
        }
        
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        
        public string Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }
        
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        
        public bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

    }
}
