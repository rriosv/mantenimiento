using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
[Serializable]
    public class BEObra
    {
        #region Atributos
        private int intId_proyecto;
        private int intAvance_obra;
        private string strFechaIni_obra;
        private string strFechaFin_obra;
        private string strFechaPrev_inaug;
        private string strFechaReal_inaug;
        private string strObra_lista;
        private double dblPeriodo_ejec_obra;
        private string strUsuario;
        #endregion

        #region Constructores

        #endregion

        #region Propiedades

        public int Id_proyecto
        {
            get { return intId_proyecto; }
            set { intId_proyecto = value; }
        }
        public int Avance_obra
        {
            get { return intAvance_obra; }
            set { intAvance_obra = value; }
        }
        public string FechaIni_obra
        {
            get { return strFechaIni_obra; }
            set { strFechaIni_obra = value; }
        }
        public string FechaFin_obra
        {
            get { return strFechaFin_obra; }
            set { strFechaFin_obra = value; }
        }
        public string FechaPrev_inaug
        {
            get { return strFechaPrev_inaug; }
            set { strFechaPrev_inaug = value; }
        }
        public string FechaReal_inaug
        {
            get { return strFechaReal_inaug; }
            set { strFechaReal_inaug = value; }
        }
        public string Obra_lista
        {
            get { return strObra_lista; }
            set { strObra_lista = value; }
        }
        public double Periodo_ejec_obra
        {
            get { return dblPeriodo_ejec_obra; }
            set { dblPeriodo_ejec_obra = value; }
        }
        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }
        #endregion
    }
}
