﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BE_MON_Ejecucion
    {
        public string urlDocAccion { get; set; }
        public string idTipoAccion { get; set; }
        public string nroAdelanto { get; set; }
        public int idTipoAdelanto { get; set; }

        private string _tipoPrioridad;

        private int _id_empleado_obra;

        public int Id_empleado_obra
        {
            get { return _id_empleado_obra; }
            set { _id_empleado_obra = value; }
        }

        private string _detalleProblematica;
        public string detalleProblematica
        {
            get { return _detalleProblematica; }
            set { _detalleProblematica = value; }
        }

        private int _id_etapaCompromiso;
        private int _id_gradoCumplimiento;
        private int _id_cierreBrecha;
        private int _id_proyecto;
        private int _id_tabla;
        private int _tipoFinanciamiento;
        private string _snip;
        private int _flag;
        private string _urlActatermino;
        private string _accionesRealizadas;
        private string _accionesPorRealizar;

        private string _diagnostico;
        public string diagnostico
        {
            get { return _diagnostico; }
            set { _diagnostico = value; }
        }

        private string _vFechaPago;

        public string urlActatermino
        {
            get { return _urlActatermino; }
            set { _urlActatermino = value; }
        }

        public int flag
        {
            get { return _flag; }
            set { _flag = value; }
        }
        private int _id_estadoProyecto;

        private int _id_tipoModalidad;

        public int id_tipoModalidad
        {
            get { return _id_tipoModalidad; }
            set { _id_tipoModalidad = value; }
        }
        private int _poblacion;

        public int poblacion
        {
            get { return _poblacion; }
            set { _poblacion = value; }
        }
        public int id_estadoProyecto
        {
            get { return _id_estadoProyecto; }
            set { _id_estadoProyecto = value; }
        }
        private bool _estado;

        private string _tipoProfesion;

        public string tipoProfesion
        {
            get { return _tipoProfesion; }
            set { _tipoProfesion = value; }
        }
        public bool estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        public string snip
        {
            get { return _snip; }
            set { _snip = value; }
        }

        private string _urlActa;

        public string UrlActa
        {
            get { return _urlActa; }
            set { _urlActa = value; }
        }
        private string urlOficio;

        public string UrlOficio
        {
            get { return urlOficio; }
            set { urlOficio = value; }
        }
        private string urlInforme;

        private string _antecedentes;

        public string UrlInforme
        {
            get { return urlInforme; }
            set { urlInforme = value; }
        }
        private string _coordinadorMVCS;
        private int _id_tipoEstadoEjecuccion;
        private string _id_tipoSubEstadoEjecucion;
        private DateTime _fechaInicio;
        private int _plazoEjecuccion;
        private DateTime _fechaInicioContractual;
        private DateTime _fechaInicioReal;
        private DateTime _fechaFin;
        private DateTime _fechaFinContractual;
        private DateTime _fechaFinReal;
        private DateTime _fechaFinProbable;
        private string _urlMetasFisicas;
        private string _urlMemoriasDescriptivas;

        private string _informe;
        private DateTime _fecha;
        private string _avance;
        private string _situacion;

        private int _id_usuario;

        private string _id_tipomonitoreo;
        private string _id_tipoSubMonitoreo;
        private string _id_tipodocumento;

        private string _nom_tipomonitoreo;

        private DateTime _fechaEntregaTerreno;
        private DateTime _fechaRecepcion;
        private string _supervisorDesignado;
        private string _residenteObra;
        private string _inspector;
        private string _coordinadorUE;
        private string _telefonoUE;
        private string _correoUE;
        private string _telefonocoordinador;
        private string _correocoordinador;

        private string _monto;
        private string _fisicoReal;
        private string _fisicoProgramado;
        private string _financieroReal;

        private string _fisicoProgramadoAcumulado;
        private string _financieroProgramado;
        private int _idEstadoSituacional;
        private int _idEstadoPreLiquidacion;

        private string _gerenteObra;

        public string gerenteObra
        {
            get { return _gerenteObra; }
            set { _gerenteObra = value; }
        }
        private string _montoContrapartida;

        public string montoContrapartida
        {
            get { return _montoContrapartida; }
            set { _montoContrapartida = value; }
        }
        private string _montoRE;

        public string montoRE
        {
            get { return _montoRE; }
            set { _montoRE = value; }
        }

        public int idEstadoSituacional
        {
            get { return _idEstadoSituacional; }
            set { _idEstadoSituacional = value; }
        }
        public int idEstadoPreLiquidacion
        {
            get { return _idEstadoPreLiquidacion; }
            set { _idEstadoPreLiquidacion = value; }
        }
        private string _estadoSituacional;
        private string _estadoPreLiquidacion;
        private string _urlDoc;

        private string _id_tipoValorizacion;
        private string _NroAdicional;

        private int _id_carta_fianza;

        private string _montoAdelantoDirecto;
        private string _montoAdelantoMaterial;
        private DateTime _fechaAdelantoDirecto;
        private DateTime _fechaAdelantoMaterial;
        private DateTime _fechafinalR;

        private string _strFechaTerminoCuaderoObra;

        public string strFechaTerminoCuaderoObra
        {
            get { return _strFechaTerminoCuaderoObra; }
            set { _strFechaTerminoCuaderoObra = value; }
        }
        private int _tipoempleado;
        private string _empleadoObra;
        private string _telefono;
        private string _correo;
        private string _cap;
        private string _nrotramite;
        private string _tipodocumento;
        private string _asunto;

        private string _entidadFinanciera;
        private string _nroCarta;
        private int _tipoCarta;
        private string _porcentaje;
        private string _flagVerificado;
        private string _flagRenovado;

        private string _observacion;
        private DateTime _fechaRenovacion;
        private int _id_avanceFisico;
        private int _id_tecnico;
        private string _flagAyuda;

        private string _usuario;

        private int _diasProgramado;
        public int diasProgramado
        {
            get { return _diasProgramado; }
            set { _diasProgramado = value; }
        }

        private int _diasReal;

        public int diasReal
        {
            get { return _diasReal; }
            set { _diasReal = value; }
        }
        private int _numero;

        private string _cargo;

        public string cargo
        {
            get { return _cargo; }
            set { _cargo = value; }
        }
        private string _montoReajuste;
        private string _amortizacionDirecto;

        public string amortizacionDirecto
        {
            get { return _amortizacionDirecto; }
            set { _amortizacionDirecto = value; }
        }
        private string _amortizacionMateriales;

        public string amortizacionMateriales
        {
            get { return _amortizacionMateriales; }
            set { _amortizacionMateriales = value; }
        }
        public string montoReajuste
        {
            get { return _montoReajuste; }
            set { _montoReajuste = value; }
        }
        private string _montoTotal;

        public string montoTotal
        {
            get { return _montoTotal; }
            set { _montoTotal = value; }
        }
        public int numero
        {
            get { return _numero; }
            set { _numero = value; }
        }
        public string usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }
        private DateTime? _fecha_update;

        public DateTime? fecha_update
        {
            get { return _fecha_update; }
            set { _fecha_update = value; }
        }

        private DateTime? _dtFechaProgramada;

        public DateTime? dtFechaProgramada
        {
            get { return _dtFechaProgramada; }
            set { _dtFechaProgramada = value; }
        }

        private string _strFechaProgramada;

        public string strFechaProgramada
        {
            get { return _strFechaProgramada; }
            set { _strFechaProgramada = value; }
        }
        private DateTime _fechaActaAsignacion;
        public string fechaActaAsignacion
        {
            get { return _fechaActaAsignacion.ToString("dd/MM/yyyy"); }
            set { _fechaActaAsignacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaActaAsignacion
        {
            get { return _fechaActaAsignacion; }
            set { _fechaActaAsignacion = value; }
        }


        private DateTime _fechaPago;
        public string fechaPago
        {
            get { return _fechaPago.ToString("dd/MM/yyyy"); }
            set { _fechaPago = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaPago
        {
            get { return _fechaPago; }
            set { _fechaPago = value; }
        }

        public string strFechaPago { get; set; }
        private string _nroContrato;

        public string nroContrato
        {
            get { return _nroContrato; }
            set { _nroContrato = value; }
        }

        private string _tipoAgente;

        public string tipoAgente
        {
            get { return _tipoAgente; }
            set { _tipoAgente = value; }
        }

        private string _agente;

        public string agente
        {
            get { return _agente; }
            set { _agente = value; }
        }

        private string _tipoEstado;

        public string tipoEstado
        {
            get { return _tipoEstado; }
            set { _tipoEstado = value; }
        }

        private int _id_tipoAgente;

        public int id_tipoAgente
        {
            get { return _id_tipoAgente; }
            set { _id_tipoAgente = value; }
        }
        private int _id_estado;
        public int id_estado
        {
            get { return _id_estado; }
            set { _id_estado = value; }
        }
        public string flagAyuda
        {
            get { return _flagAyuda; }
            set { _flagAyuda = value; }
        }
        public string id_tipoValorizacion
        {
            get { return _id_tipoValorizacion; }
            set { _id_tipoValorizacion = value; }
        }

        private string _tipoValorizacion;

        public string tipoValorizacion
        {
            get { return _tipoValorizacion; }
            set { _tipoValorizacion = value; }
        }
        public int id_tecnico
        {
            get { return _id_tecnico; }
            set { _id_tecnico = value; }
        }
        public string NroAdicional
        {
            get { return _NroAdicional; }
            set { _NroAdicional = value; }
        }

        public string telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        private int _ampliacion;

        public int ampliacion
        {
            get { return _ampliacion; }
            set { _ampliacion = value; }
        }
        private int _id_proyectoTecnico;

        public int id_proyectoTecnico
        {
            get { return _id_proyectoTecnico; }
            set { _id_proyectoTecnico = value; }
        }

        private string _nombre;

        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public string correo
        {
            get { return _correo; }
            set { _correo = value; }
        }
        public string cap
        {
            get { return _cap; }
            set { _cap = value; }
        }

        public string nrotramite
        {
            get { return _nrotramite; }
            set { _nrotramite = value; }
        }
        public string tipodocumento
        {
            get { return _tipodocumento; }
            set { _tipodocumento = value; }
        }
        public string asunto
        {
            get { return _asunto; }
            set { _asunto = value; }
        }

        public string observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public string fechaRenovacion
        {
            get { return _fechaRenovacion.ToString("dd/MM/yyyy"); }
            set { _fechaRenovacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaRenovacion
        {
            get { return _fechaRenovacion; }
            set { _fechaRenovacion = value; }
        }

        public int id_tabla
        {
            get { return _id_tabla; }
            set { _id_tabla = value; }
        }

        public int id_carta_fianza
        {
            get { return _id_carta_fianza; }
            set { _id_carta_fianza = value; }
        }
        public int id_avanceFisico
        {
            get { return _id_avanceFisico; }
            set { _id_avanceFisico = value; }
        }
        public string entidadFinanciera
        {
            get { return _entidadFinanciera; }
            set { _entidadFinanciera = value; }
        }

        public string nroCarta
        {
            get { return _nroCarta; }
            set { _nroCarta = value; }
        }

        public int tipoCarta
        {
            get { return _tipoCarta; }
            set { _tipoCarta = value; }
        }

        public string porcentaje
        {
            get { return _porcentaje; }
            set { _porcentaje = value; }
        }

        public string flagVerificado
        {
            get { return _flagVerificado; }
            set { _flagVerificado = value; }
        }

        public string flagRenovado
        {
            get { return _flagRenovado; }
            set { _flagRenovado = value; }
        }

        public string empleadoObra
        {
            get { return _empleadoObra; }
            set { _empleadoObra = value; }
        }

        public int tipoempleado
        {
            get { return _tipoempleado; }
            set { _tipoempleado = value; }
        }

        public string fechaAdelantoDirecto
        {
            get { return _fechaAdelantoDirecto.ToString("dd/MM/yyyy"); }
            set { _fechaAdelantoDirecto = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAdelantoDirecto
        {
            get { return _fechaAdelantoDirecto; }
            set { _fechaAdelantoDirecto = value; }
        }

        public string fechafinalR
        {
            get { return _fechafinalR.ToString("dd/MM/yyyy"); }
            set { _fechafinalR = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechafinalR
        {
            get { return _fechafinalR; }
            set { _fechafinalR = value; }
        }


        public string fechaAdelantoMaterial
        {
            get { return _fechaAdelantoMaterial.ToString("dd/MM/yyyy"); }
            set { _fechaAdelantoMaterial = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAdelantoMaterial
        {
            get { return _fechaAdelantoMaterial; }
            set { _fechaAdelantoMaterial = value; }
        }

        public string montoAdelantoDirecto
        {
            get { return _montoAdelantoDirecto; }
            set { _montoAdelantoDirecto = value; }
        }

        public string montoAdelantoMaterial
        {
            get { return _montoAdelantoMaterial; }
            set { _montoAdelantoMaterial = value; }
        }

        public string monto
        {
            get { return _monto; }
            set { _monto = value; }
        }

        private string _montoEjecutado;
        private string _mes;

        public string mes
        {
            get { return _mes; }
            set { _mes = value; }
        }
        private string _anio;

        public string anio
        {
            get { return _anio; }
            set { _anio = value; }
        }

        public string montoEjecutado
        {
            get { return _montoEjecutado; }
            set { _montoEjecutado = value; }
        }

        private string _materialReal;

        public string materialReal
        {
            get { return _materialReal; }
            set { _materialReal = value; }
        }
        private string _materialProgramado;

        public string materialProgramado
        {
            get { return _materialProgramado; }
            set { _materialProgramado = value; }
        }
        public string fisicoReal
        {
            get { return _fisicoReal; }
            set { _fisicoReal = value; }
        }

        public string fisicoProgramado
        {
            get { return _fisicoProgramado; }
            set { _fisicoProgramado = value; }
        }

        public string financieroReal
        {
            get { return _financieroReal; }
            set { _financieroReal = value; }
        }

        public string financieroProgramado
        {
            get { return _financieroProgramado; }
            set { _financieroProgramado = value; }
        }

        public string estadoSituacional
        {
            get { return _estadoSituacional; }
            set { _estadoSituacional = value; }
        }

        public string estadoPreLiquidacion
        {
            get { return _estadoPreLiquidacion; }
            set { _estadoPreLiquidacion = value; }
        }

        public string urlDoc
        {
            get { return _urlDoc; }
            set { _urlDoc = value; }
        }

        public string correoUE
        {
            get { return _correoUE; }
            set { _correoUE = value; }
        }

        public string telefonoUE
        {
            get { return _telefonoUE; }
            set { _telefonoUE = value; }
        }

        public string telefonocoordinador
        {
            get { return _telefonocoordinador; }
            set { _telefonocoordinador = value; }
        }

        public string correocoordinador
        {
            get { return _correocoordinador; }
            set { _correocoordinador = value; }
        }


        public string coordinadorUE
        {
            get { return _coordinadorUE; }
            set { _coordinadorUE = value; }
        }

        public string inspector
        {
            get { return _inspector; }
            set { _inspector = value; }
        }

        public string residenteObra
        {
            get { return _residenteObra; }
            set { _residenteObra = value; }
        }

        public string supervisorDesignado
        {
            get { return _supervisorDesignado; }
            set { _supervisorDesignado = value; }
        }

        public string fechaRecepcion
        {
            get { return _fechaRecepcion.ToString("dd/MM/yyyy"); }
            set { _fechaRecepcion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaRecepcion
        {
            get { return _fechaRecepcion; }
            set { _fechaRecepcion = value; }
        }

        public string fechaEntregaTerreno
        {
            get { return _fechaEntregaTerreno.ToString("dd/MM/yyyy"); }
            set { _fechaEntregaTerreno = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaEntregaTerreno
        {
            get { return _fechaEntregaTerreno; }
            set { _fechaEntregaTerreno = value; }
        }

        private DateTime? _dtFecha;

        public DateTime? dtFecha
        {
            get { return _dtFecha; }
            set { _dtFecha = value; }
        }

        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

        public int tipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }

        public string coordinadorMVCS
        {
            get { return _coordinadorMVCS; }
            set { _coordinadorMVCS = value; }
        }

        public int id_tipoEstadoEjecuccion
        {
            get { return _id_tipoEstadoEjecuccion; }
            set { _id_tipoEstadoEjecuccion = value; }
        }

        public string id_tipoSubEstadoEjecucion
        {
            get { return _id_tipoSubEstadoEjecucion; }
            set { _id_tipoSubEstadoEjecucion = value; }
        }

        private string _id_tipoEstadoParalizado;
        public string id_tipoEstadoParalizado
        {
            get { return _id_tipoEstadoParalizado; }
            set { _id_tipoEstadoParalizado = value; }
        }

        private string _id_tipoSubEstadoParalizado;

        private string _strFechaInforme;

        public string strFechaInforme
        {
            get { return _strFechaInforme; }
            set { _strFechaInforme = value; }
        }
        private string _strFechaEntregaTerreno;

        public string strFechaEntregaTerreno
        {
            get { return _strFechaEntregaTerreno; }
            set { _strFechaEntregaTerreno = value; }
        }
        private string _strFechaRetiro;

        public string strFechaRetiro
        {
            get { return _strFechaRetiro; }
            set { _strFechaRetiro = value; }
        }
        private string _strFechaInicioContractual;

        public string strFechaInicioContractual
        {
            get { return _strFechaInicioContractual; }
            set { _strFechaInicioContractual = value; }
        }
        private string _strFechaInicioReal;

        public string strFechaInicioReal
        {
            get { return _strFechaInicioReal; }
            set { _strFechaInicioReal = value; }
        }
        private string _strFechaFinContractual;

        public string strFechaFinContractual
        {
            get { return _strFechaFinContractual; }
            set { _strFechaFinContractual = value; }
        }
        private string _strFechaFinReal;

        public string strFechaFinReal
        {
            get { return _strFechaFinReal; }
            set { _strFechaFinReal = value; }
        }
        private string _strFechaRecepcion;

        public string strFechaRecepcion
        {
            get { return _strFechaRecepcion; }
            set { _strFechaRecepcion = value; }
        }


        public string fechaInicio
        {
            get { return _fechaInicio.ToString("dd/MM/yyyy"); }
            set { _fechaInicio = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaInicio
        {
            get { return _fechaInicio; }
            set { _fechaInicio = value; }
        }

        public int plazoEjecuccion
        {
            get { return _plazoEjecuccion; }
            set { _plazoEjecuccion = value; }
        }

        private int _plazoProyecto;

        public int plazoProyecto
        {
            get { return _plazoProyecto; }
            set { _plazoProyecto = value; }
        }



        private string _montoPreLiquidacion;

        private string _nroValorizacionAdicional;

        public string nroValorizacionAdicional
        {
            get { return _nroValorizacionAdicional; }
            set { _nroValorizacionAdicional = value; }
        }
        public string montoPreLiquidacion
        {
            get { return _montoPreLiquidacion; }
            set { _montoPreLiquidacion = value; }
        }
        public string strFechaLimite
        {
            get { return strFechaLimite; }
            set { strFechaLimite = value; }
        }

        public string fechaInicioContractual
        {
            get { return _fechaInicioContractual.ToString("dd/MM/yyyy"); }
            set { _fechaInicioContractual = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaInicioContractual
        {
            get { return _fechaInicioContractual; }
            set { _fechaInicioContractual = value; }
        }

        public string fechaInicioReal
        {
            get { return _fechaInicioReal.ToString("dd/MM/yyyy"); }
            set { _fechaInicioReal = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaInicioReal
        {
            get { return _fechaInicioReal; }
            set { _fechaInicioReal = value; }
        }

        public string fechaFin
        {
            get { return _fechaFin.ToString("dd/MM/yyyy"); }
            set { _fechaFin = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaFin
        {
            get { return _fechaFin; }
            set { _fechaFin = value; }
        }

        public string fechaFinContractual
        {
            get { return _fechaFinContractual.ToString("dd/MM/yyyy"); }
            set { _fechaFinContractual = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaFinContractual
        {
            get { return _fechaFinContractual; }
            set { _fechaFinContractual = value; }
        }

        public string fechaFinReal
        {
            get { return _fechaFinReal.ToString("dd/MM/yyyy"); }
            set { _fechaFinReal = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaFinReal
        {
            get { return _fechaFinReal; }
            set { _fechaFinReal = value; }
        }


        public string fechaFinProbable
        {
            get { return _fechaFinProbable.ToString("dd/MM/yyyy"); }
            set { _fechaFinProbable = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaFinProbable
        {
            get { return _fechaFinProbable; }
            set { _fechaFinProbable = value; }
        }
        public string urlMetasFisicas
        {
            get { return _urlMetasFisicas; }
            set { _urlMetasFisicas = value; }
        }

        public string urlMemoriasDescriptivas
        {
            get { return _urlMemoriasDescriptivas; }
            set { _urlMemoriasDescriptivas = value; }
        }

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }


        public string informe
        {
            get { return _informe; }
            set { _informe = value; }
        }

        public string avance
        {
            get { return _avance; }
            set { _avance = value; }
        }

        public string fecha
        {
            get { return _fecha.ToString("dd/MM/yyyy"); }
            set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
        public string situacion
        {
            get { return _situacion; }
            set { _situacion = value; }
        }

        private string _CIP;

        public string CIP
        {
            get { return _CIP; }
            set { _CIP = value; }
        }

        private string _eia;

        public string Eia
        {
            get { return _eia; }
            set { _eia = value; }
        }
        private string _legal;

        public string Legal
        {
            get { return _legal; }
            set { _legal = value; }
        }
        private string _fuente;

        public string Fuente
        {
            get { return _fuente; }
            set { _fuente = value; }
        }
        private string _residuales;

        public string Residuales
        {
            get { return _residuales; }
            set { _residuales = value; }
        }
        private string _infobras;

        public string Infobras
        {
            get { return _infobras; }
            set { _infobras = value; }
        }

        private string _pruebas;

        public string pruebas
        {
            get { return _pruebas; }
            set { _pruebas = value; }
        }

        private string codigoInfobra;

        public string CodigoInfobra
        {
            get { return codigoInfobra; }
            set { codigoInfobra = value; }
        }

        private DateTime _fechaVisita;
        public string fechaVisita
        {
            get { return _fechaVisita.ToString("dd/MM/yyyy"); }
            set { _fechaVisita = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaVisita
        {
            get { return _fechaVisita; }
            set { _fechaVisita = value; }
        }

        private DateTime _fechaAprobacion;
        public string fechaAprobacion
        {
            get { return _fechaAprobacion.ToString("dd/MM/yyyy"); }
            set { _fechaAprobacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAprobacion
        {
            get { return _fechaAprobacion; }
            set { _fechaAprobacion = value; }
        }

        private DateTime _fechaObsPreLiquidacion;
        public string fechaObsPreLiquidacion
        {
            get { return _fechaObsPreLiquidacion.ToString("dd/MM/yyyy"); }
            set { _fechaObsPreLiquidacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaObsPreLiquidacion
        {
            get { return _fechaObsPreLiquidacion; }
            set { _fechaObsPreLiquidacion = value; }
        }

        private DateTime _fechaPreLiquidacion;
        public string fechaPreLiquidacion
        {
            get { return _fechaPreLiquidacion.ToString("dd/MM/yyyy"); }
            set { _fechaPreLiquidacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaPreLiquidacion
        {
            get { return _fechaPreLiquidacion; }
            set { _fechaPreLiquidacion = value; }
        }
        private string _evaluacion;

        public string Evaluacion
        {
            get { return _evaluacion; }
            set { _evaluacion = value; }
        }
        private string _recomendacion;

        private string _jefeUnidadImplementacion;

        public string jefeUnidadImplementacion
        {
            get { return _jefeUnidadImplementacion; }
            set { _jefeUnidadImplementacion = value; }
        }
        public string Recomendacion
        {
            get { return _recomendacion; }
            set { _recomendacion = value; }
        }
        private string _fisicoEjec;

        public string FisicoEjec
        {
            get { return _fisicoEjec; }
            set { _fisicoEjec = value; }
        }
        private string _fisicoProg;

        public string FisicoProg
        {
            get { return _fisicoProg; }
            set { _fisicoProg = value; }
        }

        private int _id_ejecucionRecomendacion;

        public int Id_ejecucionRecomendacion
        {
            get { return _id_ejecucionRecomendacion; }
            set { _id_ejecucionRecomendacion = value; }
        }

        private int _tipo;

        public int Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public string id_tipomonitoreo
        {
            get { return _id_tipomonitoreo; }
            set { _id_tipomonitoreo = value; }
        }

        public string nom_tipomonitoreo
        {
            get { return _nom_tipomonitoreo; }
            set { _nom_tipomonitoreo = value; }
        }

        public string id_tipodocumento
        {
            get { return _id_tipodocumento; }
            set { _id_tipodocumento = value; }
        }

        public string urlFichaRecepcion
        {
            get { return _urlFichaRecepcion; }
            set { _urlFichaRecepcion = value; }
        }


        private string _urlFichaRecepcion;


        private DateTime _fechaInforme;
        public string fechaInforme
        {
            get { return _fechaInforme.ToString("dd/MM/yyyy"); }
            set { _fechaInforme = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaInforme
        {
            get { return _fechaInforme; }
            set { _fechaInforme = value; }
        }

        private DateTime _fechaRetiro;
        public string fechaRetiro
        {
            get { return _fechaRetiro.ToString("dd/MM/yyyy"); }
            set { _fechaRetiro = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaRetiro
        {
            get { return _fechaRetiro; }
            set { _fechaRetiro = value; }
        }

        private DateTime _fechaAprobacionPMA;
        public string fechaAprobacionPMA
        {
            get { return _fechaAprobacionPMA.ToString("dd/MM/yyyy"); }
            set { _fechaAprobacionPMA = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAprobacionPMA
        {
            get { return _fechaAprobacionPMA; }
            set { _fechaAprobacionPMA = value; }
        }

        private string _urlActaTerreno;

        public string UrlActaTerreno
        {
            get { return _urlActaTerreno; }
            set { _urlActaTerreno = value; }
        }

        private string _urlInformeInicial;

        public string UrlInformeInicial
        {
            get { return _urlInformeInicial; }
            set { _urlInformeInicial = value; }
        }


        private string _estadoHijo;

        public string estadoHijo
        {
            get { return _estadoHijo; }
            set { _estadoHijo = value; }
        }


        private string _estadoSupervisor;

        public string estadoSupervisor
        {
            get { return _estadoSupervisor; }
            set { _estadoSupervisor = value; }
        }
        private string _strSupervisorFechaAsignacion;

        public string strSupervisorFechaAsignacion
        {
            get { return _strSupervisorFechaAsignacion; }
            set { _strSupervisorFechaAsignacion = value; }
        }
        private string _contratoSupervisor;

        public string contratoSupervisor
        {
            get { return _contratoSupervisor; }
            set { _contratoSupervisor = value; }
        }
        private string _montoContratoSupervisor;

        public string montoContratoSupervisor
        {
            get { return _montoContratoSupervisor; }
            set { _montoContratoSupervisor = value; }
        }

        private string _estadoResidente;

        public string estadoResidente
        {
            get { return _estadoResidente; }
            set { _estadoResidente = value; }
        }
        private string _strResidenteFechaAsignacion;

        public string strResidenteFechaAsignacion
        {
            get { return _strResidenteFechaAsignacion; }
            set { _strResidenteFechaAsignacion = value; }
        }
        private string _contratoResidente;

        public string contratoResidente
        {
            get { return _contratoResidente; }
            set { _contratoResidente = value; }
        }
        private string _montoContratoResidente;

        public string montoContratoResidente
        {
            get { return _montoContratoResidente; }
            set { _montoContratoResidente = value; }
        }

        private DateTime _fechaInicioProyecto;
        public string fechaInicioProyecto
        {
            get { return _fechaInicioProyecto.ToString("dd/MM/yyyy"); }
            set { _fechaInicioProyecto = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaInicioProyecto
        {
            get { return _fechaInicioProyecto; }
            set { _fechaInicioProyecto = value; }
        }

        private DateTime _fechaCulminacionProyecto;
        public string fechaCulminacionProyecto
        {
            get { return _fechaCulminacionProyecto.ToString("dd/MM/yyyy"); }
            set { _fechaCulminacionProyecto = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaCulminacionProyecto
        {
            get { return _fechaCulminacionProyecto; }
            set { _fechaCulminacionProyecto = value; }
        }

        private DateTime _fechaProbableInicio;
        public string fechaProbableInicio
        {
            get { return _fechaProbableInicio.ToString("dd/MM/yyyy"); }
            set { _fechaProbableInicio = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaProbableInicio
        {
            get { return _fechaProbableInicio; }
            set { _fechaProbableInicio = value; }
        }

        private int _id_Item;

        public int id_item
        {
            get { return _id_Item; }
            set { _id_Item = value; }
        }

        private string _administrador;

        public string administrador
        {
            get { return _administrador; }
            set { _administrador = value; }
        }

        private string _DNI;

        public string DNI
        {
            get { return _DNI; }
            set { _DNI = value; }
        }

        private int _id_tipoMoneda;

        public int id_tipoMoneda
        {
            get { return _id_tipoMoneda; }
            set { _id_tipoMoneda = value; }
        }
        private int _id_tipoMonedaContrapartida;

        public int id_tipoMonedaContrapartida
        {
            get { return _id_tipoMonedaContrapartida; }
            set { _id_tipoMonedaContrapartida = value; }
        }
        private int _id_tipoMonedaRE;

        public int id_tipoMonedaRE
        {
            get { return _id_tipoMonedaRE; }
            set { _id_tipoMonedaRE = value; }
        }

        private int _nroAgua;
        private int _nroAlcantarillado;

        //PersonalNE===========================================================================
        #region Aaron



        private string _nivel;

        public string nivel
        {
            get { return _nivel; }
            set { _nivel = value; }
        }


        private bool _activo;

        public bool activo
        {
            get { return _activo; }
            set { _activo = value; }
        }

        private string _activoS;

        public string activoS
        {
            get { return _activoS; }
            set { _activoS = value; }
        }

        private int _id_tipoPrograma;

        public int id_tipoPrograma
        {
            get { return _id_tipoPrograma; }
            set { _id_tipoPrograma = value; }
        }

        public String tipoPrograma { get; set; }

        private int _id_tipoProfesion;

        public int id_tipoProfesion
        {
            get { return _id_tipoProfesion; }
            set { _id_tipoProfesion = value; }
        }

        private string _nroColegiatura;

        public string nroColegiatura
        {
            get { return _nroColegiatura; }
            set { _nroColegiatura = value; }
        }

        public long Numero { get; set; }
        public string Detalle_Error { get; set; }

        public int nroAgua
        {
            get
            {
                return _nroAgua;
            }

            set
            {
                _nroAgua = value;
            }
        }

        public int nroAlcantarillado
        {
            get
            {
                return _nroAlcantarillado;
            }

            set
            {
                _nroAlcantarillado = value;
            }
        }

        #endregion

        //=========================================================================================

        public string dirigidoA
        {
            get
            {
                return _dirigidoA;
            }

            set
            {
                _dirigidoA = value;
            }
        }

        public string referencia
        {
            get
            {
                return _referencia;
            }

            set
            {
                _referencia = value;
            }
        }

        public string cabecera
        {
            get
            {
                return _cabecera;
            }

            set
            {
                _cabecera = value;
            }
        }

        public string metas
        {
            get
            {
                return _metas;
            }

            set
            {
                _metas = value;
            }
        }

        public string analisis
        {
            get
            {
                return _analisis;
            }

            set
            {
                _analisis = value;
            }
        }

        private string _comentario;
        public string antecedentes
        {
            get
            {
                return _antecedentes;
            }

            set
            {
                _antecedentes = value;
            }
        }

        public string comentario
        {
            get
            {
                return _comentario;
            }

            set
            {
                _comentario = value;
            }
        }

        public string id_tipoSubMonitoreo
        {
            get
            {
                return _id_tipoSubMonitoreo;
            }

            set
            {
                _id_tipoSubMonitoreo = value;
            }
        }

        private string _dirigidoA;
        private string _referencia;
        private string _cabecera;
        private string _metas;
        private string _analisis;
        //private string _conclucion;

        private DateTime _fechaDiagnostico;
        public string fechaDiagnostico
        {
            get { return _fechaDiagnostico.ToString("dd/MM/yyyy"); }
            set { _fechaDiagnostico = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaDiagnostico
        {
            get { return _fechaDiagnostico; }
            set { _fechaDiagnostico = value; }
        }

        private DateTime _fechaParalizacion;
        public string fechaParalizacion
        {
            get { return _fechaParalizacion.ToString("dd/MM/yyyy"); }
            set { _fechaParalizacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaParalizacion
        {
            get { return _fechaParalizacion; }
            set { _fechaParalizacion = value; }
        }

        private DateTime _fechaReinicio;
        public string fechaReinicio
        {
            get { return _fechaReinicio.ToString("dd/MM/yyyy"); }
            set { _fechaReinicio = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaReinicio
        {
            get { return _fechaReinicio; }
            set { _fechaReinicio = value; }
        }

        public string accionesRealizadas
        {
            get
            {
                return _accionesRealizadas;
            }

            set
            {
                _accionesRealizadas = value;
            }
        }

        public string accionesPorRealizar
        {
            get
            {
                return _accionesPorRealizar;
            }

            set
            {
                _accionesPorRealizar = value;
            }
        }



        public string id_tipoSubEstadoParalizado
        {
            get
            {
                return _id_tipoSubEstadoParalizado;
            }

            set
            {
                _id_tipoSubEstadoParalizado = value;
            }
        }

        public string vFechaPago
        {
            get => _vFechaPago;
            set => _vFechaPago = value;
        }
        public string fisicoProgramadoAcumulado { get => _fisicoProgramadoAcumulado; set => _fisicoProgramadoAcumulado = value; }

        public int? Concluidas { get; set; }
        public int? EnEjecucion { get; set; }
        public int? PorIniciar { get; set; }
        public int? Paralizadas { get; set; }
        public string tipoPrioridad { get => _tipoPrioridad; set => _tipoPrioridad = value; }
        public int id_etapaCompromiso { get => _id_etapaCompromiso; set => _id_etapaCompromiso = value; }
        public int id_gradoCumplimiento { get => _id_gradoCumplimiento; set => _id_gradoCumplimiento = value; }
        public int id_cierreBrecha { get => _id_cierreBrecha; set => _id_cierreBrecha = value; }
    }


    public class BE_MON_VarPNSU
    {
        private int id_proyecto;
        private int tipoFinanciamiento;
        private int a_captacion;
        private int a_trata;
        private int a_conduccion;
        private int a_reservorio;
        private int a_Aduccion;
        private int a_redes;
        private int a_conexion_nueva;
        private int a_conexion_rehab;
        private int a_conexion_piletas;
        private int a_impulsion;
        private int a_estacion;
        private int s_efluente;
        private int s_camara;
        private int s_emisor;
        private int s_planta;
        private int s_conexion_nueva;


        private int s_redes;
        private int s_conexion_unid;
        private int s_conexion_rehab;

        private int d_secundario;
        private int d_principal;
        private int d_inspeccion;
        private int d_conexion;
        private int d_tormenta;
        private int d_cuneta;
        private int d_tuberia;
        private int _id_usuario;
        private int s_impulsion;

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        private float a_captacion_p;

        public float A_captacion_p
        {
            get { return a_captacion_p; }
            set { a_captacion_p = value; }
        }
        private float a_trata_p;

        public float A_trata_p
        {
            get { return a_trata_p; }
            set { a_trata_p = value; }
        }
        private float a_conduccion_p;

        public float A_conduccion_p
        {
            get { return a_conduccion_p; }
            set { a_conduccion_p = value; }
        }
        private float a_reservorio_p;

        public float A_reservorio_p
        {
            get { return a_reservorio_p; }
            set { a_reservorio_p = value; }
        }
        private float a_Aduccion_p;

        public float A_Aduccion_p
        {
            get { return a_Aduccion_p; }
            set { a_Aduccion_p = value; }
        }
        private float a_redes_p;

        public float A_redes_p
        {
            get { return a_redes_p; }
            set { a_redes_p = value; }
        }
        private float a_conexion_nueva_p;

        public float A_conexion_nueva_p
        {
            get { return a_conexion_nueva_p; }
            set { a_conexion_nueva_p = value; }
        }
        private float a_conexion_rehab_p;

        public float A_conexion_rehab_p
        {
            get { return a_conexion_rehab_p; }
            set { a_conexion_rehab_p = value; }
        }
        private float a_conexion_piletas_p;

        public float A_conexion_piletas_p
        {
            get { return a_conexion_piletas_p; }
            set { a_conexion_piletas_p = value; }
        }
        private float a_impulsion_p;

        public float A_impulsion_p
        {
            get { return a_impulsion_p; }
            set { a_impulsion_p = value; }
        }
        private float a_estacion_p;

        public float A_estacion_p
        {
            get { return a_estacion_p; }
            set { a_estacion_p = value; }
        }
        private float s_efluente_p;

        public float S_efluente_p
        {
            get { return s_efluente_p; }
            set { s_efluente_p = value; }
        }
        private float s_camara_p;

        public float S_camara_p
        {
            get { return s_camara_p; }
            set { s_camara_p = value; }
        }
        private float s_emisor_p;

        public float S_emisor_p
        {
            get { return s_emisor_p; }
            set { s_emisor_p = value; }
        }
        private float s_planta_p;

        public float S_planta_p
        {
            get { return s_planta_p; }
            set { s_planta_p = value; }
        }
        private float s_conexion_nueva_p;

        public float S_conexion_nueva_p
        {
            get { return s_conexion_nueva_p; }
            set { s_conexion_nueva_p = value; }
        }


        private float s_redes_p;

        public float S_redes_p
        {
            get { return s_redes_p; }
            set { s_redes_p = value; }
        }
        private float s_conexion_unid_p;

        public float S_conexion_unid_p
        {
            get { return s_conexion_unid_p; }
            set { s_conexion_unid_p = value; }
        }
        private float s_conexion_rehab_p;

        public float S_conexion_rehab_p
        {
            get { return s_conexion_rehab_p; }
            set { s_conexion_rehab_p = value; }
        }

        private float d_secundario_p;

        public float D_secundario_p
        {
            get { return d_secundario_p; }
            set { d_secundario_p = value; }
        }
        private float d_principal_p;

        public float D_principal_p
        {
            get { return d_principal_p; }
            set { d_principal_p = value; }
        }
        private float d_inspeccion_p;

        public float D_inspeccion_p
        {
            get { return d_inspeccion_p; }
            set { d_inspeccion_p = value; }
        }
        private float d_conexion_p;

        public float D_conexion_p
        {
            get { return d_conexion_p; }
            set { d_conexion_p = value; }
        }
        private float d_tormenta_p;

        public float D_tormenta_p
        {
            get { return d_tormenta_p; }
            set { d_tormenta_p = value; }
        }
        private float d_cuneta_p;

        public float D_cuneta_p
        {
            get { return d_cuneta_p; }
            set { d_cuneta_p = value; }
        }
        private float d_tuberia_p;

        public float D_tuberia_p
        {
            get { return d_tuberia_p; }
            set { d_tuberia_p = value; }
        }
        private float s_impulsion_p;

        public float S_impulsion_p
        {
            get { return s_impulsion_p; }
            set { s_impulsion_p = value; }
        }

        public int Id_proyecto
        {
            get { return id_proyecto; }
            set { id_proyecto = value; }
        }
        public int TipoFinanciamiento
        {
            get { return tipoFinanciamiento; }
            set { tipoFinanciamiento = value; }
        }
        public int A_captacion
        {
            get { return a_captacion; }
            set { a_captacion = value; }
        }
        public int A_trata
        {
            get { return a_trata; }
            set { a_trata = value; }
        }
        public int A_conduccion
        {
            get { return a_conduccion; }
            set { a_conduccion = value; }
        }
        public int A_reservorio
        {
            get { return a_reservorio; }
            set { a_reservorio = value; }
        }
        public int A_Aduccion
        {
            get { return a_Aduccion; }
            set { a_Aduccion = value; }
        }
        public int A_redes
        {
            get { return a_redes; }
            set { a_redes = value; }
        }
        public int A_conexion_nueva
        {
            get { return a_conexion_nueva; }
            set { a_conexion_nueva = value; }
        }
        public int A_conexion_rehab
        {
            get { return a_conexion_rehab; }
            set { a_conexion_rehab = value; }
        }

        public int A_conexion_piletas
        {
            get { return a_conexion_piletas; }
            set { a_conexion_piletas = value; }
        }

        public int A_impulsion
        {
            get { return a_impulsion; }
            set { a_impulsion = value; }
        }

        public int A_estacion
        {
            get { return a_estacion; }
            set { a_estacion = value; }
        }

        public int S_efluente
        {
            get { return s_efluente; }
            set { s_efluente = value; }
        }

        public int S_camara
        {
            get { return s_camara; }
            set { s_camara = value; }
        }

        public int S_emisor
        {
            get { return s_emisor; }
            set { s_emisor = value; }
        }

        public int S_planta
        {
            get { return s_planta; }
            set { s_planta = value; }
        }

        public int S_conexion_nueva
        {
            get { return s_conexion_nueva; }
            set { s_conexion_nueva = value; }
        }

        public int S_conexion_rehab
        {
            get { return s_conexion_rehab; }
            set { s_conexion_rehab = value; }
        }


        public int S_conexion_unid
        {
            get { return s_conexion_unid; }
            set { s_conexion_unid = value; }
        }



        public int S_redes
        {
            get { return s_redes; }
            set { s_redes = value; }
        }


        public int S_impulsion
        {
            get { return s_impulsion; }
            set { s_impulsion = value; }
        }


        public int D_tuberia
        {
            get { return d_tuberia; }
            set { d_tuberia = value; }
        }


        public int D_cuneta
        {
            get { return d_cuneta; }
            set { d_cuneta = value; }
        }



        public int D_tormenta
        {
            get { return d_tormenta; }
            set { d_tormenta = value; }
        }



        public int D_conexion
        {
            get { return d_conexion; }
            set { d_conexion = value; }
        }


        public int D_inspeccion
        {
            get { return d_inspeccion; }
            set { d_inspeccion = value; }
        }


        public int D_secundario
        {
            get { return d_secundario; }
            set { d_secundario = value; }
        }

        public int D_principal
        {
            get { return d_principal; }
            set { d_principal = value; }
        }



    }

    public class BE_MON_SIAF_PNSR
    {
        public string ANO_EJE { get; set; }
        public string EXPEDIENTE { get; set; }
        public string SEC_AREA { get; set; }
        public string FASE { get; set; }
        public string COD_DOC { get; set; }
        public string COD_DOC_NOMBRE { get; set; }
        public string NUM_DOC { get; set; }
        public string NOTAS { get; set; }
        public string FECHA_DOC { get; set; }
        public string SEC_FUNC { get; set; }
        public string SEC_FUNC_NOMBRE { get; set; }
        public string PROD_PRY { get; set; }
        public string META { get; set; }
        public string FINALIDAD { get; set; }
        public string MONTO { get; set; }
        public string MONTO_NACIONAL { get; set; }
        public string FECHA_AUTORIZACION { get; set; }
        public string NUM_DOC_B { get; set; }
        public string ANO_PROCESO { get; set; }
        public string MES_PROCESO { get; set; }
        public string DIA_PROCESO { get; set; }
        public string CERTIFICADO_GLOSA { get; set; }
        public string USUARIO_CREACION_CLT { get; set; }
        public string FECHA_FIN_CONTRATO { get; set; }
    }
}
