﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class BE_FichaResumen
    {
        public int IdFicha { get; set; }
        public string CodSnip { get; set; }
        public string CodFicha { get; set; }
        public string vUsuario { get; set; }
        public decimal nAvanceD { get; set; }
        public decimal nAvanceT { get; set; }
        public List<BE_Item> listadoItems { get; set; }
    }
}
