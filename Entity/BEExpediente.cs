using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
[Serializable]
    public class BEExpediente
    {

     #region Atributos
        private int intId_proyecto;
        private int intAvance_Expe;
        private string strFechaIni_Expe;
        private string strFechaFin_Expe;
        private double dblPeriodo_ejec_Expe;
        private string strUsuario;
     #endregion

     #region Constructores

     #endregion

     #region Propiedades

     public int Id_proyecto
     {
         get { return intId_proyecto; }
         set { intId_proyecto = value; }
     }
     public int Avance_Expe
     {
         get { return intAvance_Expe; }
         set { intAvance_Expe = value; }
     }
     public string FechaIni_Expe
     {
         get { return strFechaIni_Expe; }
         set { strFechaIni_Expe = value; }
     }
     public string FechaFin_Expe
     {
         get { return strFechaFin_Expe; }
         set { strFechaFin_Expe = value; }
     }
     public double Periodo_ejec_Expe
     {
         get { return dblPeriodo_ejec_Expe; }
         set { dblPeriodo_ejec_Expe = value; }
     }
     public string Usuario
     {
         get { return strUsuario; }
         set { strUsuario = value; }
     }
     #endregion

    }
}
