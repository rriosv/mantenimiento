﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BE_MON_Ampliacion
    {
        private int _id_proyecto;
        private int _tipoFinanciamiento;
        private int _id_usuario;
        private int _id_tipo_ampliacion;

        private int _id_ampliacion;

        private string _ampliacion;
        private string _resolAprob;
        private DateTime _fechaResolucion;
        private string _concepto;
        private string _observacion;
        private string _montoPresup;
        private string _vinculacion;
        private string _porcentajeIncidencia;
        private string _urlDoc;

        private string _urlInforme;
        private string _urlOficio;

        private int _id_tipoMoneda;

        //Tabla detalle Paralizacion
        private int _id_detalleParalizacion;
        private int _id_ampliaciones;
        private DateTime _fecha;
        private string _accion;
        private DateTime _fecha_registro;
        private int _usr_registro;
        private DateTime _fecha_updateParalizac;
        private int _usr_update;
        private int _flag_ayuda;

        //AvanceFotos
        private int _id_avance_fisico_foto;
        private int _id_avance_fisico;
        private string _v_nombre;
        private string _v_archivo;
        private int _activo;

        public int id_avance_fisico_foto
        {
            get { return _id_avance_fisico_foto; }
            set { _id_avance_fisico_foto = value; }
        }

        public int id_avance_fisico
        {
            get { return _id_avance_fisico; }
            set { _id_avance_fisico = value; }
        }

        public string v_nombre
        {
            get { return _v_nombre; }
            set { _v_nombre = value; }
        }

        public string v_archivo
        {
            get { return _v_archivo; }
            set { _v_archivo = value; }
        }

        public int activo
        {
            get { return _activo; }
            set { _activo = value; }
        }





        public int flag_ayuda
        {
            get { return _flag_ayuda; }
            set { _flag_ayuda = value; }
        }

        public int id_detalleParalizacion
        {
            get { return _id_detalleParalizacion; }
            set { _id_detalleParalizacion = value; }
        }

        public int id_ampliaciones
        {
            get { return _id_ampliaciones; }
            set { _id_ampliaciones = value; }
        }

        public DateTime fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public string accion
        {
            get { return _accion; }
            set { _accion = value; }
        }

        public DateTime fecha_registro
        {
            get { return _fecha_registro; }
            set { _fecha_registro = value; }
        }

        public int usr_registro
        {
            get { return _usr_registro; }
            set { _usr_registro = value; }
        }

        public DateTime fecha_updateParalizac
        {
            get { return _fecha_updateParalizac; }
            set { _fecha_updateParalizac = value; }
        }

        public int usr_update
        {
            get { return _usr_update; }
            set { _usr_update = value; }
        }



        public int id_tipoMoneda
        {
            get { return _id_tipoMoneda; }
            set { _id_tipoMoneda = value; }
        }

        public int id_proyecto
        {
            get { return _id_proyecto; }
            set { _id_proyecto = value; }
        }

        public int id_ampliacion
        {
            get { return _id_ampliacion; }
            set { _id_ampliacion = value; }
        }

        public int tipoFinanciamiento
        {
            get { return _tipoFinanciamiento; }
            set { _tipoFinanciamiento = value; }
        }

        public int id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        public int id_tipo_ampliacion
        {
            get { return _id_tipo_ampliacion; }
            set { _id_tipo_ampliacion = value; }
        }


        public string ampliacion
        {
            get { return _ampliacion; }
            set { _ampliacion = value; }
        }
        public string resolAprob
        {
            get { return _resolAprob; }
            set { _resolAprob = value; }
        }

        public string fechaResolucion
        {
            get { return _fechaResolucion.ToString("dd/MM/yyyy"); }
            set { _fechaResolucion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaResolucion
        {
            get { return _fechaResolucion; }
            set { _fechaResolucion = value; }
        }

        private DateTime fechaInicio;

        public string FechaInicio
        {
            get { return fechaInicio.ToString("dd/MM/yyyy"); }
            set { fechaInicio = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaInicio
        {
            get { return fechaInicio; }
            set { fechaInicio = value; }
        }

        private DateTime fechaFin;

        public string FechaFin
        {
            get { return fechaFin.ToString("dd/MM/yyyy"); }
            set { fechaFin = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_FechaFin
        {
            get { return fechaFin; }
            set { fechaFin = value; }
        }
        public string concepto
        {
            get { return _concepto; }
            set { _concepto = value; }
        }
        public string observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }
        public string montoPresup
        {
            get { return _montoPresup; }
            set { _montoPresup = value; }
        }
        public string vinculacion
        {
            get { return _vinculacion; }
            set { _vinculacion = value; }
        }
        public string porcentajeIncidencia
        {
            get { return _porcentajeIncidencia; }
            set { _porcentajeIncidencia = value; }
        }
        public string urlDoc
        {
            get { return _urlDoc; }
            set { _urlDoc = value; }
        }

        public string urlInforme
        {
            get { return _urlInforme; }
            set { _urlInforme = value; }
        }

        public string urlOficio
        {
            get { return _urlOficio; }
            set { _urlOficio = value; }
        }

        private DateTime _fechaCuadernoObra;
        public string fechaCuadernoObra
        {
            get { return _fechaCuadernoObra.ToString("dd/MM/yyyy"); }
            set { _fechaCuadernoObra = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaCuadernoObra
        {
            get { return _fechaCuadernoObra; }
            set { _fechaCuadernoObra = value; }
        }

        private DateTime _fechaSolicitudContratista;
        public string fechaSolicitudContratista
        {
            get { return _fechaSolicitudContratista.ToString("dd/MM/yyyy"); }
            set { _fechaSolicitudContratista = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaSolicitudContratista
        {
            get { return _fechaSolicitudContratista; }
            set { _fechaSolicitudContratista = value; }
        }

        private DateTime _fechaPresentacionInforme;
        public string fechaPresentacionInforme
        {
            get { return _fechaPresentacionInforme.ToString("dd/MM/yyyy"); }
            set { _fechaPresentacionInforme = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaPresentacionInforme
        {
            get { return _fechaPresentacionInforme; }
            set { _fechaPresentacionInforme = value; }
        }

        private DateTime _fechaAdenda;

        public string fechaAdenda
        {
            get { return _fechaAdenda.ToString("dd/MM/yyyy"); }
            set { _fechaAdenda = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
        }

        public DateTime Date_fechaAdenda
        {
            get { return _fechaAdenda; }
            set { _fechaAdenda = value; }
        }

        private int _tipo;

        public int tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        private string _usuario;
        private int _id_tabla;
        private string _entidad;

        public string entidad
        {
            get { return _entidad; }
            set { _entidad = value; }
        }

        public int id_tabla
        {
            get { return _id_tabla; }
            set { _id_tabla = value; }
        }
        public string usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }

        private DateTime? _fecha_update;

        public DateTime? fecha_update
        {
            get { return _fecha_update; }
            set { _fecha_update = value; }
        }
    }
}

