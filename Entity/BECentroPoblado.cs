﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class BECentroPoblado
    {
        private string _nombre;
        private string _ubigeo_ccpp;
        private string _ubigeo;
        private string _area;
        private double _total;
        private double _hombre;
        private double _mujer;
        private int _idSolicitud;
        private bool _check;
        public string nombre
        {
            get { return _nombre.Trim(); }
            set { _nombre = value; }
        }
        public int idSolicitud
        {
            get { return _idSolicitud; }
            set { _idSolicitud = value; }
        }
        public bool check
        {
            get { return _check; }
            set { _check = value; }
        }
        public string ubigeo_ccpp
        {
            get { return _ubigeo_ccpp; }
            set { _ubigeo_ccpp = value; }
        }
        public string ubigeo
        {
            get { return _ubigeo; }
            set { _ubigeo = value; }
        }
        public string area
        {
            get { return _area; }
            set { _area = value; }
        }
        public double total
        {
            get { return _total; }
            set { _total = value; }
        }
        public double hombre
        {
            get { return _hombre; }
            set { _hombre = value; }
        }
        public double mujer
        {
            get { return _mujer; }
            set { _mujer = value; }
        }
    }
}

