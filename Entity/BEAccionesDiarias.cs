﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    class BEAccionesDiarias
    {
        private int _id_accion;
        private DateTime _fecha;
        private string _snip;
        private string _observacion;
        private string _comunicacion;

        public int ID_ACCION
        {
            get { return _id_accion; }
            set { _id_accion = value; }
        }

        public string SNIP
        {
            get { return _snip; }
            set { _snip = value; }
        }

        public string OBSERVACION
        {
            get { return _observacion; }
            set { _observacion = value; }

        }

        public DateTime FECHA
        {
            get {
                return _fecha;
                }
            set { _fecha = value; }
        }

        public string COMUNICACION
        {
            get { return _comunicacion; }
            set { _comunicacion = value; }

        }

    }
}
