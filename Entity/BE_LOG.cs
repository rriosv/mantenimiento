﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
   public class BE_LOG
    {
        private int id_usuario;

        public int Id_usuario
        {
            get { return id_usuario; }
            set { id_usuario = value; }
        }
        private string ip;

        public string Ip
        {
            get { return ip; }
            set { ip = value; }
        }
        private string hostName;

        public string HostName
        {
            get { return hostName; }
            set { hostName = value; }
        }
        private string pagina;

        public string Pagina
        {
            get { return pagina; }
            set { pagina = value; }
        }

        private string tipoDispositivo;
        public string TipoDispositivo
        {
            get { return tipoDispositivo; }
            set { tipoDispositivo = value; }
        }

        private string agente;
        public string Agente
        {
            get { return agente; }
            set { agente = value; }
        }
    }

}
