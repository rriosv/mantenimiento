﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
   public class BE_MON_Finan_Transparencia
   {
       #region Atributos

       private int _id_finan_transferencia;
       private int _id_tipo_transferencia;
       private string _nroconvenio;
       private DateTime _fecha;
       private string _montoconvenio;
       private string _dispositivo_aprobacion;
       private DateTime _fecha_aprobacion;
       private string _monto_aprobacion;
       private string _observacion;
       private string _urlDoc;
       private int _tipo;
       private int _id_usuario;

      #endregion

       #region Propiedades

       public int id_finan_transferencia
       {
           get { return _id_finan_transferencia; }
           set { _id_finan_transferencia = value; }
       }

       public int id_tipo_transferencia
       {
           get { return _id_tipo_transferencia; }
           set { _id_tipo_transferencia = value; }
       }

       public string nroconvenio
       {
           get { return _nroconvenio; }
           set { _nroconvenio = value; }
       }

       public string fecha
       {
           get { return _fecha.ToString("dd/MM/yyyy"); }
           set { _fecha = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_Fecha
            {
                get { return _fecha; }
                set { _fecha = value; }
            }

       public string montoconvenio
       {
           get { return _montoconvenio; }
           set { _montoconvenio = value; }
       }

       public string dispositivo_aprobacion
       {
           get { return _dispositivo_aprobacion; }
           set { _dispositivo_aprobacion = value; }
       }


       public string fecha_aprobacion
       {
           get { return _fecha_aprobacion.ToString("dd/MM/yyyy"); }
           set { _fecha_aprobacion = DateTime.ParseExact(value, "dd/MM/yyyy", null); }
       }

       public DateTime Date_fecha_aprobacion
       {
           get { return _fecha_aprobacion; }
           set { _fecha_aprobacion = value; }
       }

 

       public string monto_aprobacion
       {
           get { return _monto_aprobacion; }
           set { _monto_aprobacion = value; }
       }

       public string observacion
       {
           get { return _observacion; }
           set { _observacion = value; }
       }

       public string urlDoc
       {
           get { return _urlDoc; }
           set { _urlDoc = value; }
       }


       public int tipo
       {
           get { return _tipo; }
           set { _tipo = value; }
       }

       public int id_usuario
       {
           get { return _id_usuario; }
           set { _id_usuario = value; }
       }
        #endregion
   }
}

 