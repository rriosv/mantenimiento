﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class BE_HitosProyecto
    { 
        public int iId { get; set; }
        public int EtapaRegistro { get; set; }
        public DateTime? dFechaInicioProyecto { get; set; }
        public DateTime? dFechaFinProyecto { get; set; }
        public DateTime? dFechaConvocatoria { get; set; }
        public DateTime? dFechaRegistroParticipantes { get; set; }
        public DateTime? dFechaFormulacion { get; set; }
        public DateTime? dFechaIntegracionBases { get; set; }
        public DateTime? dFechaPreparacionOfertas { get; set; }
        public DateTime? dFechaEvaluacionOferta { get; set; }
        public DateTime? dFechaOtorgamientoBuenaPro { get; set; }
        public DateTime? dFechaConsentimientoBuenaPro { get; set; }
        public DateTime? dFechaPresentacionDocumentoContrato { get; set; }
        public DateTime? dFechaFirmaContrato { get; set; }
        public DateTime? dFechaActoPosteriorFirmaContrato { get; set; }
        public int IdUsuario { get; set; }

    }
}
