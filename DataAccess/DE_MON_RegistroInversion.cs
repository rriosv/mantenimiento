﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DE_MON_RegistroInversion
    {
        private string strCadenaConexion = ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public DataSet spMON_FaseInversion(int id_proyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_FaseInversion ]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
       
        public int spi_MON_FaseInversion(int id_proyecto, int iTipoRegistro, decimal montoRegistrado, DateTime fechaRegistro, string enlace, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FaseInversion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@iTipoRegistro", DbType.Int32, iTipoRegistro);
                objDE.AddInParameter(objComando, "@montoRegistrado", DbType.Decimal, montoRegistrado);
                objDE.AddInParameter(objComando, "@fechaRegistro", DbType.DateTime, fechaRegistro);
                objDE.AddInParameter(objComando, "@enlace", DbType.String, enlace);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_FaseInversion(int id_proyecto, int iTipoRegistro, decimal montoRegistrado, DateTime fechaRegistro, string enlace, int id_usuario, int id_faseInversion,int tipo)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_FaseInversion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@iTipoRegistro", DbType.Int32, iTipoRegistro);
                objDE.AddInParameter(objComando, "@montoRegistrado", DbType.Decimal, montoRegistrado);
                objDE.AddInParameter(objComando, "@fechaRegistro", DbType.DateTime, fechaRegistro);
                objDE.AddInParameter(objComando, "@enlace", DbType.String, enlace);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                objDE.AddInParameter(objComando, "@id_faseInversion", DbType.Int32, id_faseInversion);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
