﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using Entity;

namespace DataAccess
{
    public class DE_MON_Riesgo
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public DataSet spMON_Riesgos(int pIdProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Riesgos]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, pIdProyecto);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_RiesgosFicha(int pIdProyecto, int pIdRiesgo)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_RiesgosFicha]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, pIdProyecto);
                objDE.AddInParameter(objComando, "@id_riesgo", DbType.Int32, pIdRiesgo);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiu_MON_Riesgos( BE_MON_Riesgo pEntidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_Riesgos]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, pEntidad.id_proyecto);
                objDE.AddInParameter(objComando, "@id_riesgo", DbType.Int32, pEntidad.id_riesgo);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, pEntidad.date_fecha);
                objDE.AddInParameter(objComando, "@idEstado", DbType.Int32, pEntidad.id_estado);
                objDE.AddInParameter(objComando, "@idSubEstado", DbType.Int32, pEntidad.id_subEstado);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, pEntidad.id_usuario);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, pEntidad.observacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, pEntidad.urlActa);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiu_MON_RiesgosFicha(BE_MON_Riesgo pEntidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_RiesgosFicha]");
                objDE.AddInParameter(objComando, "@id_riesgo", DbType.Int32, pEntidad.id_riesgo);
                objDE.AddInParameter(objComando, "@id_item", DbType.Int32, pEntidad.id_item);
                objDE.AddInParameter(objComando, "@flagResultado", DbType.Int32, pEntidad.flagResultado);
                objDE.AddInParameter(objComando, "@detalle", DbType.String, pEntidad.comentario);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, pEntidad.id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spd_MON_Riesgos(BE_MON_Riesgo pEntidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spd_MON_Riesgos]");
                objDE.AddInParameter(objComando, "@id_riesgo", DbType.Int32, pEntidad.id_riesgo);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, pEntidad.id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_RiesgosChart(int pIdProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_RiesgosChart]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, pIdProyecto);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
