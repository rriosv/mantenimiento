﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess
{
    public class DE_MON_SOSEM
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

    public int spi_MON_FinanSOSEM(BE_MON_SOSEM _BE_SOSEM)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FinanSOSEM]");

            objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_SOSEM.id_proyecto);
            objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_SOSEM.tipoFinanciamiento);
            objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_SOSEM.anio);
            objDE.AddInParameter(objComando, "@pia", DbType.String, _BE_SOSEM.pia);
            objDE.AddInParameter(objComando, "@pimAcumulado", DbType.String, _BE_SOSEM.pimAcumulado);
            objDE.AddInParameter(objComando, "@devAcumulado", DbType.String, _BE_SOSEM.devAcumulado);

            objDE.AddInParameter(objComando, "@ene", DbType.String, _BE_SOSEM.ene);
            objDE.AddInParameter(objComando, "@feb", DbType.String, _BE_SOSEM.feb);
            objDE.AddInParameter(objComando, "@mar", DbType.String, _BE_SOSEM.mar);
            objDE.AddInParameter(objComando, "@abr", DbType.String, _BE_SOSEM.abr);
            objDE.AddInParameter(objComando, "@may", DbType.String, _BE_SOSEM.may);
            objDE.AddInParameter(objComando, "@jun", DbType.String, _BE_SOSEM.jun);
            objDE.AddInParameter(objComando, "@jul", DbType.String, _BE_SOSEM.jul);
            objDE.AddInParameter(objComando, "@ago", DbType.String, _BE_SOSEM.ago);
            objDE.AddInParameter(objComando, "@sep", DbType.String, _BE_SOSEM.sep);
            objDE.AddInParameter(objComando, "@oct", DbType.String, _BE_SOSEM.oct);
            objDE.AddInParameter(objComando, "@nov", DbType.String, _BE_SOSEM.nov);
            objDE.AddInParameter(objComando, "@dic", DbType.String, _BE_SOSEM.dic);
            objDE.AddInParameter(objComando, "@compromisoAnual", DbType.String, _BE_SOSEM.compromisoAnual);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_SOSEM.id_usuario);


            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spud_MON_SOSEM_ELIMINAR(BE_MON_SOSEM _BE_SOSEM)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_SOSEM_ELIMINAR]");

            objDE.AddInParameter(objComando, "@id_seguimiento_sosem", DbType.Int32, _BE_SOSEM.id_seguimiento_sosem);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_SOSEM.id_usuario);
                        return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spud_MON_SOSEM_EDITAR(BE_MON_SOSEM _BE_SOSEM)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_SOSEM_EDITAR]");

            objDE.AddInParameter(objComando, "@id_seguimiento_sosem", DbType.Int32, _BE_SOSEM.id_seguimiento_sosem);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_SOSEM.id_usuario);
            objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_SOSEM.anio);
            objDE.AddInParameter(objComando, "@pia", DbType.String, _BE_SOSEM.pia);
            objDE.AddInParameter(objComando, "@pimAcumulado", DbType.String, _BE_SOSEM.pimAcumulado);
            objDE.AddInParameter(objComando, "@devAcumulado", DbType.String, _BE_SOSEM.devAcumulado);

            objDE.AddInParameter(objComando, "@ene", DbType.String, _BE_SOSEM.ene);
            objDE.AddInParameter(objComando, "@feb", DbType.String, _BE_SOSEM.feb);
            objDE.AddInParameter(objComando, "@mar", DbType.String, _BE_SOSEM.mar);
            objDE.AddInParameter(objComando, "@abr", DbType.String, _BE_SOSEM.abr);
            objDE.AddInParameter(objComando, "@may", DbType.String, _BE_SOSEM.may);
            objDE.AddInParameter(objComando, "@jun", DbType.String, _BE_SOSEM.jun);
            objDE.AddInParameter(objComando, "@jul", DbType.String, _BE_SOSEM.jul);
            objDE.AddInParameter(objComando, "@ago", DbType.String, _BE_SOSEM.ago);
            objDE.AddInParameter(objComando, "@sep", DbType.String, _BE_SOSEM.sep);
            objDE.AddInParameter(objComando, "@oct", DbType.String, _BE_SOSEM.oct);
            objDE.AddInParameter(objComando, "@nov", DbType.String, _BE_SOSEM.nov);
            objDE.AddInParameter(objComando, "@dic", DbType.String, _BE_SOSEM.dic);
            objDE.AddInParameter(objComando, "@compromisoAnual", DbType.String, _BE_SOSEM.compromisoAnual);
            


            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    }
}
