﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DE_MON_Dec_Viabilidad
    {
        private string strCadenaConexion = ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
        #region PREINVERSION
        public DataSet spMON_Aprobacion(int id_proyecto, int id_tipoAprobacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Aprobacion ]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoAprobacion", DbType.Int32, id_tipoAprobacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_TipoDocumentoAprobacion(int id_tipoAprobacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_TipoDocumentoAprobacion]");
                objDE.AddInParameter(objComando, "@id_tipoAprobacion", DbType.Int32, id_tipoAprobacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_Aprobacion(int id_proyecto, int id_tipoAprobacion, int id_tipoDocumentoAprobacion, string detalleDocumento, DateTime fecha, string urlDoc, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Aprobacion]");
                
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoAprobacion", DbType.Int32, id_tipoAprobacion);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoAprobacion", DbType.Int32, id_tipoDocumentoAprobacion);
                objDE.AddInParameter(objComando, "@detalleDocumento", DbType.String, detalleDocumento);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, fecha);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, urlDoc);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Aprobacion(int id_aprobacion, int id_tipoDocumentoAprobacion, string detalleDocumento, DateTime fecha, string urlDoc, int tipo, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Aprobacion]");
                objDE.AddInParameter(objComando, "@id_aprobacion", DbType.Int32, id_aprobacion);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoAprobacion", DbType.Int32, id_tipoDocumentoAprobacion);
                objDE.AddInParameter(objComando, "@detalleDocumento", DbType.String, detalleDocumento);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, fecha);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, urlDoc);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        #endregion

    }
}
