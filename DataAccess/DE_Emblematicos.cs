﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
namespace DataAccess
{
    public class DE_Emblematicos
    {

         private string strCadenaConexion =System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
         public SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

             
         public List<BE_Emblematicos> F_spEMB_MetasProyectos(BE_Emblematicos _BE)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_MetasProyectos";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.AddWithValue("@id_proyecto", _BE.id_proyecto);

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.anio = Convert.ToInt32(Reader["anio"].ToString());
                         objENT.meta = Reader["meta"].ToString();

                         //objENT.Requerimiento = Reader("REQUERIMIENTO");
                         //objENT.id_objetivo = Reader("id_objetivo");
                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_BandejaProyectos(BE_Emblematicos _BE)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_BandejaProyectos";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.AddWithValue("@id_usuario", _BE.id_usuario);
             
                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_proyecto = Convert.ToInt64(Reader["id_proyecto"].ToString());
                         objENT.numero  = Convert.ToInt32(Reader["numero"].ToString());
                         objENT.codigo = Convert.ToInt32(Reader["codigo"].ToString());
                         objENT.strGrupo = Reader["grupo"].ToString();
                         objENT.id_grupo = Convert.ToInt32(Reader["id_grupo"].ToString());
                         objENT.proyectos = Reader["proyectos"].ToString();
                         objENT.strRegion = Reader["region"].ToString();
                         objENT.strResponsable = Reader["responsable"].ToString();
                         objENT.pimAcumulado = (Reader["pim"].ToString()=="" ? 0 : Convert.ToDecimal(Reader["pim"].ToString()));
                         objENT.devengadoAnual = (Reader["devengado"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["devengado"].ToString()));
                         objENT.dEjecucion = Convert.ToDecimal(Reader["ejecucion"].ToString());
                         objENT.dsaldo = (Reader["saldo"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["saldo"].ToString()));
                         objENT.usuario = Reader["fechaUltimo"].ToString();
                         objENT.urlDoc = (Reader["url"].ToString());
                         objENT.urlPresupuesto = (Reader["urlPresupuesto"].ToString());
                         objENT.strVisible = (Reader["VisibilityPresupuesto"].ToString());
                         objENT.boolVisible = Convert.ToBoolean((Reader["VisibilityPresupuesto"].ToString()));
                         objENT.flagComponente = (Reader["flagComponente"].ToString());
                         objENT.strUserSkype = (Reader["user_skype"].ToString());
                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_CalendarioActividad";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.AddWithValue("@id_actividad", _BE.id_actividad);
                 cmd.Parameters.AddWithValue("@flagAccion", _BE.flagAccion);
                 cmd.Parameters.AddWithValue("@fechaInicio", _BE.fechaInicio);
                 cmd.Parameters.AddWithValue("@fechaFin", _BE.fechaFin);

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_calendarioActividad = Convert.ToInt32(Reader["id_calendarioActividad"].ToString());
                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.flagAccion = Convert.ToInt32(Reader["flagAccion"].ToString());
                         objENT.flagRelevante = Reader["flagRelevante"].ToString();
                         objENT.nombre = Reader["nombre"].ToString();
                         objENT.resultado = Reader["resultado"].ToString();
                         objENT.observacion = Reader["observacion"].ToString();
                         objENT.usuario = Reader["usuario"].ToString();
                         objENT.fecha = Convert.ToDateTime(Reader["fecha"].ToString());
                         objENT.vfecha = Reader["fecha"].ToString();
                         objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         //objENT.Requerimiento = Reader("REQUERIMIENTO");
                         //objENT.id_objetivo = Reader("id_objetivo");
                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_CalendarioProyecto(BE_Emblematicos _BE)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_CalendarioProyecto";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.AddWithValue("@id_proyecto", _BE.id_proyecto);
                 cmd.Parameters.AddWithValue("@flagAccion", _BE.flagAccion);

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_calendarioActividad = Convert.ToInt32(Reader["id_calendarioActividad"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.fecha = Convert.ToDateTime(Reader["fecha"].ToString());
                         objENT.vfecha = Reader["fecha"].ToString();
                         objENT.nombre = Reader["nombre"].ToString();
                         objENT.observacion = Reader["observacion"].ToString();
                         objENT.resultado = Reader["resultado"].ToString();
                         objENT.flagRelevante = Reader["flagRelevante"].ToString();
                         objENT.flagAccion = Convert.ToInt32(Reader["flagAccion"].ToString());
                         objENT.usuario = Reader["usuario"].ToString();
                         objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }
  
         public List<BE_Emblematicos> F_spEmb_Seguimiento(int id_proyecto, int flagEjecutado)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEmb_Seguimiento";
                 cmd.CommandTimeout = 36000;

                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id_proyecto;
                 cmd.Parameters.Add("@flagEjecutado", SqlDbType.Int, 2).Value = flagEjecutado;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.id_seguimiento = Convert.ToInt32(Reader["id_seguimiento"].ToString());
                         objENT.anio = Convert.ToInt32(Reader["anio"].ToString());
                         objENT.pia = Reader["pia"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["pia"].ToString());
                         objENT.pimAcumulado = Reader["pimAcumulado"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["pimAcumulado"].ToString());
                         objENT.enero = Reader["enero"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["enero"].ToString());
                         objENT.febrero = Reader["febrero"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["febrero"].ToString());
                         objENT.marzo = Reader["marzo"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["marzo"].ToString());
                         objENT.abril = Reader["abril"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["abril"].ToString());
                         objENT.mayo = Reader["mayo"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["mayo"].ToString());
                         objENT.junio = Reader["junio"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["junio"].ToString());
                         objENT.julio = Reader["julio"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["julio"].ToString());
                         objENT.agosto = Reader["agosto"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["agosto"].ToString());
                         objENT.setiembre = Reader["setiembre"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["setiembre"].ToString());
                         objENT.octubre = Reader["octubre"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["octubre"].ToString());
                         objENT.noviembre = Reader["noviembre"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["noviembre"].ToString());
                         objENT.diciembre = Reader["diciembre"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["diciembre"].ToString());
                         objENT.devengadoAnual = Reader["devengadoAnual"].ToString() == "" ? 0 : Convert.ToDecimal(Reader["devengadoAnual"].ToString());
                        
                         objENT.usuario = Reader["usuario"].ToString();

                         if (Reader["fecha_update"].ToString() == "")
                         {
                             objENT.fecha_update = null;
                         }
                         else
                         {
                             objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                         }

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_MON_BANDEJA> F_spEMB_Responsable()
         {
             BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
             List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_Responsable";
                 cmd.CommandTimeout = 36000;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {

                     int valor = Reader.GetOrdinal("codigo");
                     int nombre = Reader.GetOrdinal("nombre");

                     while (Reader.Read())
                     {
                         objENT = new BE_MON_BANDEJA();

                         objENT.valor = Reader.GetInt32(valor);
                         objENT.nombre = Reader.GetString(nombre);

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_Actividad(BE_Emblematicos _BE)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_Actividad";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.AddWithValue("@id", _BE.id_proyecto);

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.fechaInicio = Convert.ToDateTime(Reader["FechaInicio"].ToString());
                         objENT.fechaFin = Convert.ToDateTime(Reader["FechaFin"].ToString());
                         objENT.id_situacion = Convert.ToInt32(Reader["id_situacion"].ToString());
                         objENT.situacion = Reader["situacion"].ToString();
                         objENT.descripcionActividad = Reader["descripcionActividad"].ToString();
                         objENT.avance = Convert.ToDecimal(Reader["avance"].ToString());
                         objENT.avanceProgramado = Convert.ToDecimal(Reader["avanceProgramado"].ToString());
                         objENT.problemas = Reader["problemas"].ToString();
                         objENT.propuestaSolucion = Reader["propuestaSolucion"].ToString();
                         objENT.flagHitoDescripcion = Reader["flagHito"].ToString();

                         objENT.flagUltimo = Convert.ToInt32(Reader["flagUltimo"].ToString());

                         if (Reader["fechaInformacion"].ToString() == "")
                         {
                             objENT.fechaInformacion = null;
                         }
                         else
                         {
                             objENT.fechaInformacion = Convert.ToDateTime(Reader["fechaInformacion"].ToString());
                         }

                         objENT.usuario = Reader["usuario"].ToString();
                         // objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         if (Reader["fecha_update"].ToString() == "")
                         {
                             objENT.fecha_update = null;
                         }
                         else
                         {
                             objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                         }
                         //objENT.Requerimiento = Reader("REQUERIMIENTO");
                         //objENT.id_objetivo = Reader("id_objetivo");
                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }
        
         public List<BE_Emblematicos> F_spEMB_ActividadDetalle(int id_proyecto, string componente)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_ActividadDetalle";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id_proyecto;
                 cmd.Parameters.Add("@componente", SqlDbType.VarChar, 200).Value = componente;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();

                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.fechaInicio = Convert.ToDateTime(Reader["FechaInicio"].ToString());
                         objENT.fechaFin = Convert.ToDateTime(Reader["FechaFin"].ToString());
                         objENT.id_situacion = Convert.ToInt32(Reader["id_situacion"].ToString());
                         objENT.situacion = Reader["situacion"].ToString();
                         objENT.descripcionActividad = Reader["descripcionActividad"].ToString();
                         objENT.avance = Convert.ToDecimal(Reader["avance"].ToString() == "" ? "0" : Reader["avance"].ToString());
                         objENT.problemas = Reader["problemas"].ToString();
                         objENT.propuestaSolucion = Reader["propuestaSolucion"].ToString();

                         objENT.flagHitoDescripcion = Reader["flagHito"].ToString();



                         objENT.usuario = Reader["usuario"].ToString();

                         if (Reader["fecha_update"].ToString() == "")
                         {
                             objENT.fecha_update = null;
                         }
                         else
                         {
                             objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                         }

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_ActividadHistorial(int id)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_ActividadHistorial";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_actividadHistorial = Convert.ToInt32(Reader["id_actividadHistorial"].ToString());
                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.fechaInicio = Convert.ToDateTime(Reader["FechaInicio"].ToString());
                         objENT.fechaFin = Convert.ToDateTime(Reader["FechaFin"].ToString());
                         objENT.id_situacion = Convert.ToInt32(Reader["id_situacion"].ToString());
                         objENT.situacion = Reader["situacion"].ToString();
                         objENT.descripcionActividad = Reader["descripcionActividad"].ToString();
                         objENT.avance = Convert.ToDecimal(Reader["avance"].ToString() == "" ? "0" : Reader["avance"].ToString());
                         objENT.problemas = Reader["problemas"].ToString();
                         objENT.propuestaSolucion = Reader["propuestaSolucion"].ToString();

                         objENT.dtFechaHistorial = Convert.ToDateTime(Reader["fecha_historial"].ToString());



                         objENT.usuario = Reader["usuario"].ToString();

                         if (Reader["fecha_update"].ToString() == "")
                         {
                             objENT.fecha_update = null;
                         }
                         else
                         {
                             objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                         }

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }
        
         public List<BE_Emblematicos> F_spEMB_Grupo()
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_Grupo";
                 cmd.CommandTimeout = 36000;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     int codigo = Reader.GetOrdinal("codigo");
                     int nombre = Reader.GetOrdinal("nombre");

                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();

                         objENT.codigo = Reader.GetInt32(codigo);
                         objENT.nombre = Reader.GetString(nombre);


                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_getActividad(int idActividad)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_getActividad";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@idActividad", SqlDbType.Int).Value = idActividad;
                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();
                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.fechaInicio = Convert.ToDateTime(Reader["FechaInicio"].ToString());
                         objENT.fechaFin = Convert.ToDateTime(Reader["FechaFin"].ToString());
                         objENT.id_situacion = Convert.ToInt32(Reader["id_situacion"].ToString());
                         objENT.situacion = Reader["situacion"].ToString();
                         objENT.descripcionActividad = Reader["descripcionActividad"].ToString();
                         objENT.avance = Convert.ToDecimal(Reader["avance"].ToString());
                         objENT.avanceProgramado = Convert.ToDecimal(Reader["avanceProgramado"].ToString());
                         objENT.problemas = Reader["problemas"].ToString();
                         objENT.propuestaSolucion = Reader["propuestaSolucion"].ToString();
                         objENT.flagHitoDescripcion = Reader["flagHito"].ToString();

                         objENT.usuario = Reader["usuario"].ToString();
                         // objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         if (Reader["fecha_update"].ToString() == "")
                         {
                             objENT.fecha_update = null;
                         }
                         else
                         {
                             objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                         }
                         //objENT.Requerimiento = Reader("REQUERIMIENTO");
                         //objENT.id_objetivo = Reader("id_objetivo");
                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_ActividadComponente(int id_proyecto)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_ActividadComponente";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id_proyecto;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();

                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.componente = Reader["componente"].ToString();

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_Documento(int id_proyecto)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_Documento";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id_proyecto;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();

                         objENT.id_documento = Convert.ToInt32(Reader["id_documento"].ToString());
                         objENT.documento = Reader["documento"].ToString();
                         objENT.fecha = Convert.ToDateTime(Reader["fecha"].ToString());
                         objENT.descripcion = Reader["descripcion"].ToString();
                         objENT.id_tipoDocumento = Convert.ToInt32(Reader["id_tipoDocumento"].ToString());
                         objENT.urlDoc = Reader["urlDoc"].ToString();
                         objENT.extension = Reader["extension"].ToString();
                         objENT.usuario = Reader["usuario"].ToString();
                         objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }
                
         public DataSet spEMB_Proyecto(int id)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_Proyecto]");
                 objDE.AddInParameter(objComando, "@id", DbType.Int32, id);
                
                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         //public DataSet spEMB_Reporte_Resumen(int id)
         //{
         //    try
         //    {
         //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
         //        DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_Reporte_Resumen]");
         //        objDE.AddInParameter(objComando, "@id", DbType.Int32, id);

         //        return objDE.ExecuteDataSet(objComando);

         //    }
         //    catch (DbException ex)
         //    {
         //        throw ex;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {

         //    }
         //}

         public List<BE_Emblematicos> F_spEMB_Reporte_Resumen(int id_proyecto)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_Reporte_Resumen";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id_proyecto;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();

                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.fechaInicio =Convert.ToDateTime(Reader["FechaInicio"].ToString());
                         objENT.fechaFin = Convert.ToDateTime(Reader["FechaFin"].ToString());
                         objENT.id_situacion = Convert.ToInt32(Reader["id_situacion"].ToString());
                         objENT.situacion = Reader["situacion"].ToString();
                         objENT.avance = Convert.ToDecimal(Reader["avance"].ToString());
                         objENT.avanceProgramado = Convert.ToDecimal(Reader["avanceProgramado"].ToString());
                         objENT.problemas = Reader["problemas"].ToString();
                         objENT.propuestaSolucion = Reader["propuestaSolucion"].ToString();
                         objENT.flagHito = Convert.ToInt32(Reader["flagHito"].ToString());
                         objENT.usuario = Reader["usuario"].ToString();
                         objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

         public List<BE_Emblematicos> F_spEMB_Reporte_Gantt(int id_proyecto)
         {
             BE_Emblematicos objENT = new BE_Emblematicos();
             List<BE_Emblematicos> objCollection = new List<BE_Emblematicos>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spEMB_Reporte_Gantt";
                 cmd.CommandTimeout = 36000;
                 cmd.Parameters.Add("@id", SqlDbType.Int).Value = id_proyecto;

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Emblematicos();

                         objENT.id_actividad = Convert.ToInt32(Reader["id_actividad"].ToString());
                         objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                         objENT.componente = Reader["componente"].ToString();
                         objENT.nombreActividad = Reader["nombreActividad"].ToString();
                         objENT.fechaInicio = Convert.ToDateTime(Reader["FechaInicio"].ToString());
                         objENT.fechaFin = Convert.ToDateTime(Reader["FechaFin"].ToString());
                         objENT.id_situacion = Convert.ToInt32(Reader["id_situacion"].ToString());
                         objENT.situacion = Reader["situacion"].ToString();
                         objENT.avance = Convert.ToDecimal(Reader["avance"].ToString());
                         objENT.avanceProgramado = Convert.ToDecimal(Reader["avanceProgramado"].ToString());
                         objENT.problemas = Reader["problemas"].ToString();
                         objENT.propuestaSolucion = Reader["propuestaSolucion"].ToString();
                         objENT.flagHito = Convert.ToInt32(Reader["flagHito"].ToString());
                         objENT.usuario = Reader["usuario"].ToString();
                         objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                         objCollection.Add(objENT);

                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

      

         public int spEMB_getValidaRegistroSeguimiento(int idProyecto, int flagEjecutado, int idSeguimiento, string anio)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_getValidaRegistroSeguimiento]");

                 objDE.AddInParameter(objComando, "@idProyecto", DbType.Int32, idProyecto);
                 objDE.AddInParameter(objComando, "@flagEjecutado", DbType.Int32, flagEjecutado);
                 objDE.AddInParameter(objComando, "@id_seguimiento", DbType.Int32, idSeguimiento);
                 objDE.AddInParameter(objComando, "@anio", DbType.String, anio);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spiEMB_Actividad(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_Actividad]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@componente", DbType.String, _BE.componente);
                 objDE.AddInParameter(objComando, "@nombreActividad", DbType.String, _BE.nombreActividad);
                 objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE.fechaInicio);
                 objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE.fechaFin);
                 objDE.AddInParameter(objComando, "@id_situacion", DbType.Int32, _BE.id_situacion);
                 objDE.AddInParameter(objComando, "@descripcionActividad", DbType.String, _BE.descripcionActividad);
                 objDE.AddInParameter(objComando, "@avance", DbType.Decimal, _BE.avance);
                 objDE.AddInParameter(objComando, "@avanceProgramado", DbType.Decimal, _BE.avanceProgramado);
                 objDE.AddInParameter(objComando, "@propuestaSolucion", DbType.String, _BE.propuestaSolucion);
                 objDE.AddInParameter(objComando, "@problemas", DbType.String, _BE.problemas);
                 objDE.AddInParameter(objComando, "@flagHito", DbType.Int32, _BE.flagHito);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
                 
                 

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spuEMB_Actividad(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_Actividad]");

                 objDE.AddInParameter(objComando, "@id_actividad", DbType.Int64, _BE.id_actividad);
                 objDE.AddInParameter(objComando, "@componente", DbType.String, _BE.componente);
                 objDE.AddInParameter(objComando, "@nombreActividad", DbType.String, _BE.nombreActividad);
                 objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE.fechaInicio);
                 objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE.fechaFin);
                 objDE.AddInParameter(objComando, "@id_situacion", DbType.Int32, _BE.id_situacion);
                 objDE.AddInParameter(objComando, "@descripcionActividad", DbType.String, _BE.descripcionActividad);
                 objDE.AddInParameter(objComando, "@avance", DbType.Decimal, _BE.avance);
                 objDE.AddInParameter(objComando, "@avanceProgramado", DbType.Decimal, _BE.avanceProgramado);
                 objDE.AddInParameter(objComando, "@propuestaSolucion", DbType.String, _BE.propuestaSolucion);
                 objDE.AddInParameter(objComando, "@problemas", DbType.String, _BE.problemas);
                 objDE.AddInParameter(objComando, "@flagHito", DbType.Int32, _BE.flagHito);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spuEMB_ActividadDescripcion(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_ActividadDescripcion]");

                 objDE.AddInParameter(objComando, "@id_actividad", DbType.Int64, _BE.id_actividad);
                 objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE.fecha);
                 objDE.AddInParameter(objComando, "@descripcionActividad", DbType.String, _BE.descripcionActividad);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spuEMB_NombreComponente(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_NombreComponente]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@componente", DbType.String, _BE.componente);
                 objDE.AddInParameter(objComando, "@NewComponente", DbType.String, _BE.NewComponente);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spdEMB_Actividad(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spdEMB_Actividad]");

                 objDE.AddInParameter(objComando, "@id_actividad", DbType.Int64, _BE.id_actividad);
   
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spiEMB_Seguimiento(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_Seguimiento]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@anio", DbType.String, _BE.anio);
                 objDE.AddInParameter(objComando, "@pia", DbType.Decimal, _BE.pia);
                 objDE.AddInParameter(objComando, "@pimAcumulado", DbType.Decimal, _BE.pimAcumulado);
               
                 objDE.AddInParameter(objComando, "@ene", DbType.Decimal, _BE.enero);
                 objDE.AddInParameter(objComando, "@feb", DbType.Decimal, _BE.febrero);
                 objDE.AddInParameter(objComando, "@mar", DbType.Decimal, _BE.marzo);
                 objDE.AddInParameter(objComando, "@abr", DbType.Decimal, _BE.abril);
                 objDE.AddInParameter(objComando, "@may", DbType.Decimal, _BE.mayo);
                 objDE.AddInParameter(objComando, "@jun", DbType.Decimal, _BE.junio);
                 objDE.AddInParameter(objComando, "@jul", DbType.Decimal, _BE.julio);
                 objDE.AddInParameter(objComando, "@ago", DbType.Decimal, _BE.agosto);
                 objDE.AddInParameter(objComando, "@sep", DbType.Decimal, _BE.setiembre);
                 objDE.AddInParameter(objComando, "@oct", DbType.Decimal, _BE.octubre);
                 objDE.AddInParameter(objComando, "@nov", DbType.Decimal, _BE.noviembre);
                 objDE.AddInParameter(objComando, "@dic", DbType.Decimal, _BE.diciembre);
                 objDE.AddInParameter(objComando, "@devAcumulado", DbType.Decimal, _BE.devengadoAnual);
                 objDE.AddInParameter(objComando, "@flagEjecutado", DbType.Int32, _BE.flagEjecutado);

                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);


                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spuEMB_Seguimiento(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_Seguimiento]");

                 objDE.AddInParameter(objComando, "@id_seguimiento", DbType.Int64, _BE.id_seguimiento);
                 objDE.AddInParameter(objComando, "@anio", DbType.String, _BE.anio);
                 objDE.AddInParameter(objComando, "@pia", DbType.Decimal, _BE.pia);
                 objDE.AddInParameter(objComando, "@pimAcumulado", DbType.Decimal, _BE.pimAcumulado);

                 objDE.AddInParameter(objComando, "@ene", DbType.Decimal, _BE.enero);
                 objDE.AddInParameter(objComando, "@feb", DbType.Decimal, _BE.febrero);
                 objDE.AddInParameter(objComando, "@mar", DbType.Decimal, _BE.marzo);
                 objDE.AddInParameter(objComando, "@abr", DbType.Decimal, _BE.abril);
                 objDE.AddInParameter(objComando, "@may", DbType.Decimal, _BE.mayo);
                 objDE.AddInParameter(objComando, "@jun", DbType.Decimal, _BE.junio);
                 objDE.AddInParameter(objComando, "@jul", DbType.Decimal, _BE.julio);
                 objDE.AddInParameter(objComando, "@ago", DbType.Decimal, _BE.agosto);
                 objDE.AddInParameter(objComando, "@sep", DbType.Decimal, _BE.setiembre);
                 objDE.AddInParameter(objComando, "@oct", DbType.Decimal, _BE.octubre);
                 objDE.AddInParameter(objComando, "@nov", DbType.Decimal, _BE.noviembre);
                 objDE.AddInParameter(objComando, "@dic", DbType.Decimal, _BE.diciembre);
                 objDE.AddInParameter(objComando, "@devAcumulado", DbType.Decimal, _BE.devengadoAnual);
                 objDE.AddInParameter(objComando, "@flagEjecutado", DbType.Int32, _BE.flagEjecutado);

                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);


                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spdEMB_Seguimiento(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spdEMB_Seguimiento]");

                 objDE.AddInParameter(objComando, "@id_seguimiento", DbType.Int64, _BE.id_seguimiento);
             
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);


                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public DataSet spEMB_getCalendarioActividades(Int64 id, DateTime fecha, Int32 tipo, Int32 flagAccion)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_getCalendarioActividades]");
                 objDE.AddInParameter(objComando, "@id_actividad", DbType.Int64, id);
                 objDE.AddInParameter(objComando, "@fecha", DbType.DateTime,fecha);
                 objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                 objDE.AddInParameter(objComando, "@flagAccion", DbType.Int32, flagAccion);
                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spiEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_CalendarioActividad]");

                 objDE.AddInParameter(objComando, "@id_actividad", DbType.Int64, _BE.id_actividad);
                 objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE.fecha);
                 objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE.observacion);
                 objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE.nombre);
                 objDE.AddInParameter(objComando, "@flagAccion", DbType.Int32, _BE.flagAccion);
                 objDE.AddInParameter(objComando, "@resultado", DbType.String, _BE.resultado);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);
                 objDE.AddInParameter(objComando, "@flagRelevante", DbType.Int32, _BE.flagRelevante);
                 objDE.AddInParameter(objComando, "@flagReunion", DbType.Int32, _BE.flagReunion);
         
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spuEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_CalendarioActividad]");

                 objDE.AddInParameter(objComando, "@id_calendarioActividad", DbType.Int64, _BE.id_calendarioActividad);
             
                 objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE.observacion);
                 objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE.nombre);
                 objDE.AddInParameter(objComando, "@flagAccion", DbType.Int32, _BE.flagAccion);
                 objDE.AddInParameter(objComando, "@resultado", DbType.String, _BE.resultado);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);
                 objDE.AddInParameter(objComando, "@flagRelevante", DbType.Int32, _BE.flagRelevante);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spdEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spdEMB_CalendarioActividad]");

                 objDE.AddInParameter(objComando, "@id_actividad", DbType.Int64, _BE.id_actividad);
                 objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE.fecha);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         //public DataSet spEMB_Responsable()
         //{
         //    try
         //    {
         //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
         //        DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_Responsable]");
       
         //        return objDE.ExecuteDataSet(objComando);

         //    }
         //    catch (DbException ex)
         //    {
         //        throw ex;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {

         //    }
         //}

         public int spiEMB_Proyecto(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_Proyecto]");

                 objDE.AddInParameter(objComando, "@id_grupo", DbType.Int32, _BE.id_grupo);
                 objDE.AddInParameter(objComando, "@proyectos", DbType.String, _BE.proyectos);
                 objDE.AddInParameter(objComando, "@objetivo", DbType.String, _BE.objetivo);
                 objDE.AddInParameter(objComando, "@costo", DbType.Decimal, _BE.costo);
                 objDE.AddInParameter(objComando, "@flagComponente", DbType.String, _BE.flagComponente);
                 objDE.AddInParameter(objComando, "@flagPresupuesto", DbType.String, _BE.flagPresupuesto);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);


                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spuEMB_Proyecto(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_Proyecto]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@id_grupo", DbType.Int32, _BE.id_grupo);
                 objDE.AddInParameter(objComando, "@proyectos", DbType.String, _BE.proyectos);
                 objDE.AddInParameter(objComando, "@objetivo", DbType.String, _BE.objetivo);
                 objDE.AddInParameter(objComando, "@costo", DbType.Decimal, _BE.costo);
                 objDE.AddInParameter(objComando, "@flagComponente", DbType.String, _BE.flagComponente);
                 objDE.AddInParameter(objComando, "@flagPresupuesto", DbType.String, _BE.flagPresupuesto);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);


                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spdEMB_Proyecto(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spdEMB_Proyecto]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);


                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spiEMB_DetalleProyectoRegiones(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_DetalleProyectoRegiones]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BE.cod_depa);
             
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spiEMB_DetalleResponsableProyecto(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_DetalleResponsableProyecto]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@id_responsable", DbType.String, _BE.id_responsable);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spuEMB_DetalleProyectoRegiones(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_DetalleProyectoRegiones]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BE.cod_depa);
                 objDE.AddInParameter(objComando, "@flagActivo", DbType.Int32, _BE.activo);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spuEMB_DetalleResponsableProyecto(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_DetalleResponsableProyecto]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@id_responsable", DbType.String, _BE.id_responsable);
                 objDE.AddInParameter(objComando, "@flagActivo", DbType.Int32, _BE.activo);
                 

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         //public DataSet spEMB_Documento(Int64 id)
         //{
         //    try
         //    {
         //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
         //        DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_Documento]");
         //        objDE.AddInParameter(objComando, "@id", DbType.Int64, id);

         //        return objDE.ExecuteDataSet(objComando);

         //    }
         //    catch (DbException ex)
         //    {
         //        throw ex;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {

         //    }
         //}
                
         public int spiEMB_Documento(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_Documento]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE.urlDoc);
                 objDE.AddInParameter(objComando, "@id_tipo", DbType.Int32, _BE.id_tipoDocumento);
                 objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE.fecha);
                 objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BE.descripcion);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
                 objDE.AddInParameter(objComando, "@extension", DbType.String, _BE.extension);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spuEMB_Documento(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spuEMB_Documento]");

                 objDE.AddInParameter(objComando, "@id_documento", DbType.Int64, _BE.id_documento);
                 objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE.urlDoc);
                 objDE.AddInParameter(objComando, "@id_tipo", DbType.Int32, _BE.id_tipoDocumento);
                 objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE.fecha);
                 objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BE.descripcion);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);
                 objDE.AddInParameter(objComando, "@extension", DbType.String, _BE.extension);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
                
         public int spdEMB_Documento(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spdEMB_Documento]");

                 objDE.AddInParameter(objComando, "@id_documento", DbType.Int64, _BE.id_documento);
           
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE.id_usuario);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
        
         public int spEMB_NivelAcceso(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_NivelAcceso]");
                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spiEMB_MetasProyectos(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_MetasProyectos]");
                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@anio", DbType.String, _BE.anio);
                 objDE.AddInParameter(objComando, "@meta", DbType.String, _BE.meta);
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spdEMB_MetasProyectos(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spdEMB_MetasProyectos]");
                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int64, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@anio", DbType.String, _BE.anio);
               
                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spiEMB_Impresion(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_Impresion]");

                 objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
              
                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public int spiEMB_DetalleImpresion(BE_Emblematicos _BE)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[spiEMB_DetalleImpresion]");

                 objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE.id_proyecto);
                 objDE.AddInParameter(objComando, "@id_impresion", DbType.Int32, _BE.id_impresion);

                 return Convert.ToInt32(objDE.ExecuteScalar(objComando));
             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
         //public DataSet spEMB_Grupo()
         //{
         //    try
         //    {
         //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
         //        DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_Grupo]");


         //        return objDE.ExecuteDataSet(objComando);

         //    }
         //    catch (DbException ex)
         //    {
         //        throw ex;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {

         //    }
         //}

         //public DataSet spEMB_getDetalleComponente(int idActividad)
         //{
         //    try
         //    {
         //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
         //        DbCommand objComando = objDE.GetStoredProcCommand("[spEMB_getDetalleComponente]");
         //        objDE.AddInParameter(objComando, "@idActividad", DbType.Int32, idActividad);

         //        return objDE.ExecuteDataSet(objComando);

         //    }
         //    catch (DbException ex)
         //    {
         //        throw ex;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {

         //    }
         //}
        
        

    }
}
