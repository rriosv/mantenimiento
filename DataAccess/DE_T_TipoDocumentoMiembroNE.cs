﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;

namespace DataAccess
{
    public class DE_T_TipoDocumentoMiembroNE
    {
        public List<BE_T_TipoDocumentoMiembroNE> ListarTipoDocMiemNE(int id_tipoDocMiemNE)
        {
            var lista = new List<BE_T_TipoDocumentoMiembroNE>();
            lista.Add(new BE_T_TipoDocumentoMiembroNE { id_tipoDocumentoMiembroNE = 1, nombre = "DNI", activo = true });
            lista.Add(new BE_T_TipoDocumentoMiembroNE { id_tipoDocumentoMiembroNE = 2, nombre = "Tramite en DNI", activo = true });

            lista.FindAll(t => t.id_tipoDocumentoMiembroNE == id_tipoDocMiemNE);
            return lista;
        }
    }
}
