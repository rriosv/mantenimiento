﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DE_MON_Financiamiento
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
        //SqlConnection cnn = null;
        //SqlCommand cmd = null;
        // public SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        #region SELECT

        public DataSet F_spMON_FinanciamientoDirecta(int id_proyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_FinanciamientoDirecta ]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_Financiamiento> F_spMON_FinanContrapartida(BE_MON_Financiamiento _BE)
        {
            BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_FinanContrapartida";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_Id_proyecto = Reader.GetOrdinal("id_proyecto");
                    int Ordinal_id_financiamiento_contrapartida = Reader.GetOrdinal("id_financiamiento_contrapartida");
                    int Ordinal_entidad_cooperante = Reader.GetOrdinal("entidad_cooperante");
                    int Ordinal_id_tipo_aporte = Reader.GetOrdinal("id_tipo_aporte");
                    int Ordinal_aporte = Reader.GetOrdinal("aporte");
                    int Ordinal_monto = Reader.GetOrdinal("monto");
                    int Ordinal_documento = Reader.GetOrdinal("documento");
                    int Ordinal_montoObra = Reader.GetOrdinal("montoObra");
                    int Ordinal_montoSupervision = Reader.GetOrdinal("montoSupervision");
                    int Ordinal_usuario = Reader.GetOrdinal("usuario");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");
                    int Ordinal_IDmoneda = Reader.GetOrdinal("id_tipoMoneda");
                    int Ordinal_moneda = Reader.GetOrdinal("moneda");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Financiamiento();
                        objENT.id_proyecto = Convert.ToInt32(Reader.GetInt32(Ordinal_Id_proyecto));
                        objENT.id_financiamientoContrapartida = Reader.GetInt32(Ordinal_id_financiamiento_contrapartida);
                        objENT.entidad_cooperante = Reader.GetString(Ordinal_entidad_cooperante);
                        objENT.id_tipo_aporte = Reader.GetInt32(Ordinal_id_tipo_aporte);
                        objENT.tipo_aporte = Reader.GetString(Ordinal_aporte);
                        objENT.monto = (Reader.GetDecimal(Ordinal_monto)).ToString("N2");
                        objENT.documento = Reader.GetString(Ordinal_documento);
                        objENT.MontoObra = Reader.IsDBNull(Ordinal_montoObra) ? "" : (Reader.GetDecimal(Ordinal_montoObra)).ToString("N2");
                        //objENT.MontoObra = (Reader.GetDouble(Ordinal_montoObra)).ToString();
                        objENT.MontoSupervision = Reader.IsDBNull(Ordinal_montoSupervision) ? "" : (Reader.GetDecimal(Ordinal_montoSupervision)).ToString("N2");
                        // objENT.MontoSupervision = (Reader.GetDouble(Ordinal_montoSupervision)).ToString();
                        objENT.tipo = Convert.ToInt32(Reader.GetInt32(Ordinal_IDmoneda));
                        objENT.observacion = (Reader.GetString(Ordinal_moneda));
                        objENT.usuario = Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario);
                        objENT.strFecha_update = Reader.IsDBNull(Ordinal_fecha_update) ? "" : Reader.GetDateTime(Ordinal_fecha_update).ToString();



                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Financiamiento> F_spMON_TransferenciaFinanciera(BE_MON_Financiamiento _BE)
        {
            BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TransferenciaFinanciera";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.Id;


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_id_transferenciaFinanciera = Reader.GetOrdinal("id_transferenciaFinanciera");
                    int Ordinal_meta = Reader.GetOrdinal("meta");
                    int Ordinal_cadena = Reader.GetOrdinal("cadena");
                    int Ordinal_fte = Reader.GetOrdinal("fte");
                    int Ordinal_UE_destino = Reader.GetOrdinal("UE_destino");
                    int Ordinal_girado = Reader.GetOrdinal("girado");
                    int Ordinal_anioFiscal = Reader.GetOrdinal("anioFiscal");
                    int Ordinal_usuario = Reader.GetOrdinal("usuario");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Financiamiento();
                        objENT.Id_transferenciaFinanciera = Convert.ToInt32(Reader.GetInt32(Ordinal_id_transferenciaFinanciera));
                        objENT.Meta = (Reader.IsDBNull(Ordinal_meta) ? "" : Reader.GetString(Ordinal_meta));
                        objENT.Cadena = (Reader.IsDBNull(Ordinal_cadena) ? "" : Reader.GetString(Ordinal_cadena));
                        objENT.Fte = (Reader.IsDBNull(Ordinal_fte) ? "" : Reader.GetString(Ordinal_fte));
                        objENT.UE_destino = (Reader.IsDBNull(Ordinal_UE_destino) ? "" : Reader.GetString(Ordinal_UE_destino));
                        objENT.Girado = (Reader.IsDBNull(Ordinal_girado) ? "" : Reader.GetDecimal(Ordinal_girado).ToString("N2"));

                        objENT.AnioFiscal = (Reader.IsDBNull(Ordinal_anioFiscal) ? "" : Reader.GetString(Ordinal_anioFiscal));
                        //objENT.usuario = Reader.GetString(Ordinal_usuario);
                        //objENT.strFecha_update = Reader.GetDateTime(Ordinal_fecha_update).ToString();
                        objENT.usuario = Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario);
                        objENT.strFecha_update = Reader.IsDBNull(Ordinal_fecha_update) ? "" : Reader.GetDateTime(Ordinal_fecha_update).ToString();

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public DataSet spMON_ProcesoFinanciero(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ProcesoFinanciero]");
                objDE.AddInParameter(objComando, "@id_transferenciaFinanciera", DbType.String, _BE_MON_Financiamiento.Id_transferenciaFinanciera);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_TransferenciaPresupuestal(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_TransferenciaPresupuestal]");
                objDE.AddInParameter(objComando, "@id_transferencia_presupuestal", DbType.String, _BE_MON_Financiamiento.Id_transferencia_presupuesta);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_Financiamiento> F_spMON_FinanTransferencia(BE_MON_Financiamiento _BE)
        {
            BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_FinanTransferencia";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_numero = Reader.GetOrdinal("numero");
                    int Ordinal_id_proyecto = Reader.GetOrdinal("id_proyecto");
                    int Ordinal_id_finan_transferencia = Reader.GetOrdinal("id_finan_transferencia");
                    int Ordinal_nro_convenio = Reader.GetOrdinal("nro_convenio");
                    int Ordinal_id_tipo_transferencia = Reader.GetOrdinal("id_tipo_transferencia");
                    int Ordinal_fecha_convenio = Reader.GetOrdinal("fecha_convenio");
                    int Ordinal_tipo_transferencias = Reader.GetOrdinal("tipo_transferencias");
                    int Ordinal_montoConvenio = Reader.GetOrdinal("montoConvenio");
                    int Ordinal_dispositivo_aprobacion = Reader.GetOrdinal("dispositivo_aprobacion");
                    int Ordinal_fecha_aprobacion = Reader.GetOrdinal("fecha_aprobacion");
                    int Ordinal_monto_aprobacion = Reader.GetOrdinal("monto_aprobacion");
                    int Ordinal_observacion = Reader.GetOrdinal("observacion");
                    int Ordinal_urlDoc = Reader.GetOrdinal("urlDoc");
                    int Ordinal_id_tipoTransferenciaFP = Reader.GetOrdinal("id_tipoTransferenciaFP");
                    int Ordinal_tipoTransferencia = Reader.GetOrdinal("tipoTransferencia");
                    int Ordinal_id_transferencia_financiera = Reader.GetOrdinal("id_transferencia_financiera");
                    int Ordinal_id_transferencia_presupuestal = Reader.GetOrdinal("id_transferencia_presupuestal");
                    int Ordinal_montoGirado = Reader.GetOrdinal("montoGirado");
                    int Ordinal_usuario = Reader.GetOrdinal("usuario");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Financiamiento();
                        objENT.numero = Reader.GetInt64(Ordinal_numero);
                        objENT.id_proyecto = Convert.ToInt32(Reader.GetInt32(Ordinal_id_proyecto));
                        objENT.Id_finaTransferencia = Reader.GetInt32(Ordinal_id_finan_transferencia);
                        //objENT.nro_convenio = Reader.GetString(Ordinal_nro_convenio);
                        objENT.nro_convenio = Reader.IsDBNull(Ordinal_nro_convenio) ? "" : Reader.GetString(Ordinal_nro_convenio);
                        objENT.Tipo_transferencia = (Reader.IsDBNull(Ordinal_id_tipo_transferencia) ? 0 : Reader.GetInt32(Ordinal_id_tipo_transferencia));
                        objENT.strFecha_convenio = (Reader.IsDBNull(Ordinal_fecha_convenio) ? "" : Reader.GetString(Ordinal_fecha_convenio));
                        objENT.strTipo_Transferencia = (Reader.IsDBNull(Ordinal_tipo_transferencias) ? "" : Reader.GetString(Ordinal_tipo_transferencias));
                        objENT.montoConvenio = (Reader.IsDBNull(Ordinal_montoConvenio) ? "" : Reader.GetDecimal(Ordinal_montoConvenio).ToString("N"));
                        objENT.dispositivo_aprobacion = Reader.GetString(Ordinal_dispositivo_aprobacion);
                        objENT.strfecha_aprobacion = (Reader.IsDBNull(Ordinal_fecha_aprobacion) ? "" : Reader.GetString(Ordinal_fecha_aprobacion));
                        objENT.montoAprobacion = Reader.GetDecimal(Ordinal_monto_aprobacion).ToString("N");
                        objENT.observacion = Reader.GetString(Ordinal_observacion);
                        objENT.docUrl = Reader.GetString(Ordinal_urlDoc);
                        objENT.id_tipo_trans = Reader.IsDBNull(Ordinal_id_tipoTransferenciaFP) ? 0 : Reader.GetInt32(Ordinal_id_tipoTransferenciaFP);
                        objENT.strTipo_TransferenciaFP = (Reader.IsDBNull(Ordinal_tipoTransferencia) ? "" : Reader.GetString(Ordinal_tipoTransferencia));
                        objENT.Id_transferenciaFinanciera = Reader.IsDBNull(Ordinal_id_transferencia_financiera) ? 0 : Reader.GetInt32(Ordinal_id_transferencia_financiera);
                        objENT.Id_transferencia_presupuesta = Reader.IsDBNull(Ordinal_id_transferencia_presupuestal) ? 0 : Reader.GetInt32(Ordinal_id_transferencia_presupuestal);
                        objENT.MontoGirado = Reader.GetDecimal(Ordinal_montoGirado).ToString("N");
                        objENT.usuario = Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario);
                        objENT.strFecha_update = Reader.IsDBNull(Ordinal_fecha_update) ? "" : Reader.GetDateTime(Ordinal_fecha_update).ToString();


                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_SOSEM> F_spMON_SeguimientoSOSEM(BE_MON_Financiamiento _BE)
        {
            BE_MON_SOSEM objENT = new BE_MON_SOSEM();
            List<BE_MON_SOSEM> objCollection = new List<BE_MON_SOSEM>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_SeguimientoSOSEM";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_id_proyecto = Reader.GetOrdinal("id_proyecto");
                    int Ordinal_id_seguimiento_sosem = Reader.GetOrdinal("id_seguimiento_sosem");
                    int Ordinal_anio = Reader.GetOrdinal("anio");
                    int Ordinal_pia = Reader.GetOrdinal("pia");
                    int Ordinal_pimAcumulado = Reader.GetOrdinal("pimAcumulado");
                    int Ordinal_devAcumulado = Reader.GetOrdinal("devAcumulado");
                    int Ordinal_enero = Reader.GetOrdinal("enero");
                    int Ordinal_febrero = Reader.GetOrdinal("febrero");
                    int Ordinal_marzo = Reader.GetOrdinal("marzo");
                    int Ordinal_abril = Reader.GetOrdinal("abril");
                    int Ordinal_mayo = Reader.GetOrdinal("mayo");
                    int Ordinal_junio = Reader.GetOrdinal("junio");
                    int Ordinal_julio = Reader.GetOrdinal("julio");
                    int Ordinal_agosto = Reader.GetOrdinal("agosto");
                    int Ordinal_setiembre = Reader.GetOrdinal("setiembre");
                    int Ordinal_octubre = Reader.GetOrdinal("octubre");
                    int Ordinal_noviembre = Reader.GetOrdinal("noviembre");
                    int Ordinal_diciembre = Reader.GetOrdinal("diciembre");
                    int Ordinal_compromisoAnual = Reader.GetOrdinal("compromisoAnual");

                    int Ordinal_usuario = Reader.GetOrdinal("usuario");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_SOSEM();
                        objENT.id_proyecto = Convert.ToInt32(Reader.GetInt32(Ordinal_id_proyecto));
                        objENT.id_seguimiento_sosem = Reader.GetInt32(Ordinal_id_seguimiento_sosem);
                        objENT.anio = Reader.GetInt32(Ordinal_anio).ToString();
                        objENT.pia = Reader.GetDecimal(Ordinal_pia).ToString("N");
                        objENT.pimAcumulado = Reader.GetDecimal(Ordinal_pimAcumulado).ToString("N");
                        objENT.devAcumulado = Reader.GetDecimal(Ordinal_devAcumulado).ToString("N");
                        objENT.ene = Reader.GetDecimal(Ordinal_enero).ToString("N");
                        objENT.feb = Reader.GetDecimal(Ordinal_febrero).ToString("N");
                        objENT.mar = Reader.GetDecimal(Ordinal_marzo).ToString("N");
                        objENT.abr = Reader.GetDecimal(Ordinal_abril).ToString("N");
                        objENT.may = Reader.GetDecimal(Ordinal_mayo).ToString("N");
                        objENT.jun = Reader.GetDecimal(Ordinal_junio).ToString("N");
                        objENT.jul = Reader.GetDecimal(Ordinal_julio).ToString("N");
                        objENT.ago = Reader.GetDecimal(Ordinal_agosto).ToString("N");
                        objENT.sep = Reader.GetDecimal(Ordinal_setiembre).ToString("N");
                        objENT.oct = Reader.GetDecimal(Ordinal_octubre).ToString("N");
                        objENT.nov = Reader.GetDecimal(Ordinal_noviembre).ToString("N");
                        objENT.dic = Reader.GetDecimal(Ordinal_diciembre).ToString("N");

                        objENT.compromisoAnual = Reader.GetDecimal(Ordinal_compromisoAnual).ToString("N");


                        //objENT.usuario = Reader.GetString(Ordinal_usuario);
                        //objENT.strFechaUpdate = Reader.GetDateTime(Ordinal_fecha_update).ToString();
                        objENT.usuario = Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario);
                        objENT.strFechaUpdate = Reader.IsDBNull(Ordinal_fecha_update) ? "" : Reader.GetDateTime(Ordinal_fecha_update).ToString();

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_CONVENIO_DIRECTA> F_spMON_ConvenioDirecta(int id_proyecto)
        {
            BE_MON_CONVENIO_DIRECTA objENT = new BE_MON_CONVENIO_DIRECTA();
            List<BE_MON_CONVENIO_DIRECTA> objCollection = new List<BE_MON_CONVENIO_DIRECTA>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ConvenioDirecta";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_id_convenioDirecta = Reader.GetOrdinal("id_convenioDirecta");
                    int Ordinal_nro = Reader.GetOrdinal("nro");
                    int Ordinal_iTipoConvenio = Reader.GetOrdinal("iTipoConvenio");
                    int Ordinal_tipo = Reader.GetOrdinal("tipo");
                    int Ordinal_detalle = Reader.GetOrdinal("detalle");
                    int Ordinal_vFechaConvenio = Reader.GetOrdinal("vFechaConvenio");
                    int Ordinal_entidadFirmante = Reader.GetOrdinal("entidadFirmante");
                    int Ordinal_objetoConvenio = Reader.GetOrdinal("objetoConvenio");
                    int Ordinal_vFechaVencimiento = Reader.GetOrdinal("vFechaVencimiento");

                    int Ordinal_usuario = Reader.GetOrdinal("usuario");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_CONVENIO_DIRECTA();
                        objENT.id_convenioDirecta = Reader.GetInt32(Ordinal_id_convenioDirecta);
                        objENT.nro = Convert.ToInt32(Reader.GetInt64(Ordinal_nro));
                        objENT.iTipoConvenio = Reader.GetInt32(Ordinal_iTipoConvenio);
                        objENT.tipo = Reader.GetString(Ordinal_tipo);
                        objENT.detalle = Reader.GetString(Ordinal_detalle);
                        objENT.vFechaConvenio = Reader.IsDBNull(Ordinal_vFechaConvenio) ? "" : Reader.GetString(Ordinal_vFechaConvenio);
                        objENT.entidadFirmante = Reader.GetString(Ordinal_entidadFirmante);
                        objENT.objetoConvenio = Reader.GetString(Ordinal_objetoConvenio);
                        objENT.vFechaVencimiento = Reader.IsDBNull(Ordinal_vFechaVencimiento) ? "" : Reader.GetString(Ordinal_vFechaVencimiento);

                        objENT.usuario = Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario);
                        objENT.fecha_update = Reader.IsDBNull(Ordinal_fecha_update) ? "" : Reader.GetDateTime(Ordinal_fecha_update).ToString();

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }
        }
        public DataSet spMON_rParametro(int idParametroGeneral)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_MON_rParametroDetalle]");
                objDE.AddInParameter(objComando, "@idParametroGeneral", DbType.Int32, idParametroGeneral);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_SOSEM> F_spMON_SosemEjecutora(int snip)
        {
            BE_MON_SOSEM objENT = new BE_MON_SOSEM();
            List<BE_MON_SOSEM> objCollection = new List<BE_MON_SOSEM>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_SosemEjecutora";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@snip", SqlDbType.Int).Value = snip;


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_idEjecutora = Reader.GetOrdinal("CodigoEjecutora");
                    int Ordinal_Ejecutora = Reader.GetOrdinal("NombreEjecutora");
                    int Ordinal_pimAcumulado = Reader.GetOrdinal("PimAcumulado");
                    int Ordinal_devAcumulado = Reader.GetOrdinal("DevengadoAcumulado");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_actualiza");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_SOSEM();
                        objENT.idEjecutora = Convert.ToInt32(Reader.GetString(Ordinal_idEjecutora));
                        objENT.nombreEjecutora = Reader.GetString(Ordinal_Ejecutora);
                        objENT.pimAcumulado = Reader.GetDecimal(Ordinal_pimAcumulado).ToString("N");
                        objENT.devAcumulado = Reader.GetDecimal(Ordinal_devAcumulado).ToString("N");

                        objENT.strFechaUpdate = Reader.GetString(Ordinal_fecha_update).ToString();

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_SOSEM> F_spMON_SosemDevengadoMensualizado(int snip, int idEjecutora)
        {
            BE_MON_SOSEM objENT = new BE_MON_SOSEM();
            List<BE_MON_SOSEM> objCollection = new List<BE_MON_SOSEM>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_SosemDevengadoMensualizado";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@snip", SqlDbType.Int).Value = snip;
                cmd.Parameters.Add("@idEjecutora", SqlDbType.Int).Value = idEjecutora;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int Ordinal_anio = Reader.GetOrdinal("anio");
                    int Ordinal_pia = Reader.GetOrdinal("pia");
                    int Ordinal_pimAcumulado = Reader.GetOrdinal("pimAcumulado");
                    int Ordinal_devAcumulado = Reader.GetOrdinal("devAcumulado");
                    int Ordinal_enero = Reader.GetOrdinal("enero");
                    int Ordinal_febrero = Reader.GetOrdinal("febrero");
                    int Ordinal_marzo = Reader.GetOrdinal("marzo");
                    int Ordinal_abril = Reader.GetOrdinal("abril");
                    int Ordinal_mayo = Reader.GetOrdinal("mayo");
                    int Ordinal_junio = Reader.GetOrdinal("junio");
                    int Ordinal_julio = Reader.GetOrdinal("julio");
                    int Ordinal_agosto = Reader.GetOrdinal("agosto");
                    int Ordinal_setiembre = Reader.GetOrdinal("setiembre");
                    int Ordinal_octubre = Reader.GetOrdinal("octubre");
                    int Ordinal_noviembre = Reader.GetOrdinal("noviembre");
                    int Ordinal_diciembre = Reader.GetOrdinal("diciembre");
                    int Ordinal_compromisoAnual = Reader.GetOrdinal("CompromisoAnual");
                    int Ordinal_certificadoAnual = Reader.GetOrdinal("certificadoAnual");

                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_actualiza");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_SOSEM();
                        objENT.anio = Reader.GetString(Ordinal_anio).ToString();
                        objENT.pia = Reader.GetDecimal(Ordinal_pia).ToString("N");
                        objENT.pimAcumulado = Reader.GetDecimal(Ordinal_pimAcumulado).ToString("N");
                        objENT.devAcumulado = Reader.GetDecimal(Ordinal_devAcumulado).ToString("N");
                        objENT.ene = Reader.GetDecimal(Ordinal_enero).ToString("N");
                        objENT.feb = Reader.GetDecimal(Ordinal_febrero).ToString("N");
                        objENT.mar = Reader.GetDecimal(Ordinal_marzo).ToString("N");
                        objENT.abr = Reader.GetDecimal(Ordinal_abril).ToString("N");
                        objENT.may = Reader.GetDecimal(Ordinal_mayo).ToString("N");
                        objENT.jun = Reader.GetDecimal(Ordinal_junio).ToString("N");
                        objENT.jul = Reader.GetDecimal(Ordinal_julio).ToString("N");
                        objENT.ago = Reader.GetDecimal(Ordinal_agosto).ToString("N");
                        objENT.sep = Reader.GetDecimal(Ordinal_setiembre).ToString("N");
                        objENT.oct = Reader.GetDecimal(Ordinal_octubre).ToString("N");
                        objENT.nov = Reader.GetDecimal(Ordinal_noviembre).ToString("N");
                        objENT.dic = Reader.GetDecimal(Ordinal_diciembre).ToString("N");
                        objENT.compromisoAnual = Reader.GetDecimal(Ordinal_compromisoAnual).ToString("N");
                        objENT.certificado = Reader.GetDecimal(Ordinal_certificadoAnual).ToString("N");

                        objENT.strFechaUpdate = Reader.GetString(Ordinal_fecha_update);

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_SOSEM> F_spMON_SosemFuenteFinanciamiento(int snip, int idEjecutora)
        {
            BE_MON_SOSEM objENT = new BE_MON_SOSEM();
            List<BE_MON_SOSEM> objCollection = new List<BE_MON_SOSEM>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_SosemFuenteFinanciamiento";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@snip", SqlDbType.Int).Value = snip;
                cmd.Parameters.Add("@idEjecutora", SqlDbType.Int).Value = idEjecutora;


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_FuenteFinanciamiento = Reader.GetOrdinal("FuenteFinanciamiento");
                    int Ordinal_anio = Reader.GetOrdinal("anio");
                    int Ordinal_MontoPim = Reader.GetOrdinal("MontoPim");
                    int Ordinal_MontoDevengado = Reader.GetOrdinal("MontoDevengado");
                    int Ordinal_fecha_update = Reader.GetOrdinal("fecha_actualiza");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_SOSEM();
                        objENT.fuenteFinanciamiento = Reader.GetString(Ordinal_FuenteFinanciamiento);
                        objENT.anio = Reader.GetString(Ordinal_anio).ToString();
                        objENT.pimAcumulado = Reader.GetDecimal(Ordinal_MontoPim).ToString("N");
                        objENT.devAcumulado = Reader.GetDecimal(Ordinal_MontoDevengado).ToString("N");

                        objENT.strFechaUpdate = Reader.GetString(Ordinal_fecha_update).ToString();

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoAporte()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoAporte";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoProcesoFinanciero()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoProcesoFinanciero";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDocumentoSustento()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoDocumentoSustento";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoFInanTransferencia()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoFInanTransferencia";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_CartaOrden(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_CartaOrden]");
                objDE.AddInParameter(objComando, "@id", DbType.Int32, _BE_MON_Financiamiento.Id_finaTransferencia);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public String F_spMON_MontoTotalGrd(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            //  BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            //  List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            String total = "0";
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_MontoTotalGrd";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE_MON_Financiamiento.id_proyecto;
                cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE_MON_Financiamiento.tipoGrd;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int Ordinario_total = Reader.GetOrdinal("total");

                    if (Reader.Read())
                    {
                        // objENT = new BE_MON_BANDEJA();

                        total = Reader.GetString(Ordinario_total);

                        //objCollection.Add(objENT);

                        //objENT = null;
                    }
                }
                return total;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Financiamiento> F_spSOL_TransferenciaByProyecto(int IdSolicitud)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
                List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();

                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spSOL_TransferenciaByProyecto";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = IdSolicitud;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_numero = Reader.GetOrdinal("docNro");
                        int Ordinal_id_proyecto = Reader.GetOrdinal("fechaDoc");
                        int Ordinal_id_finan_transferencia = Reader.GetOrdinal("monto_transferidoMEF");
                        int Ordinal_MontoReservaContingencia = Reader.GetOrdinal("monto_ReservaContingencia");
                        int ordinal_id = Reader.GetOrdinal("id");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_Financiamiento();
                            objENT.dispositivo_aprobacion = Reader.GetString(Ordinal_numero);
                            objENT.strfecha_aprobacion = (Reader.GetString(Ordinal_id_proyecto));
                            objENT.montoAprobacion = Reader.GetDecimal(Ordinal_id_finan_transferencia).ToString("N");
                            objENT.MontoReservaContingencia = Reader.GetDecimal(Ordinal_MontoReservaContingencia);
                            objENT.Id = Reader.GetInt32(ordinal_id).ToString();

                            //objENT.nro_convenio = Reader.GetString(Ordinal_nro_convenio);
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Financiamiento> F_spMON_PresupuestoPNVR(int IdProyecto)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
                List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_PresupuestoPNVR";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = IdProyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_id_partida = Reader.GetOrdinal("id_partida");
                        int Ordinal_codRubro = Reader.GetOrdinal("codRubro");
                        int Ordinal_descripcion = Reader.GetOrdinal("descripcion");
                        int Ordinal_monto = Reader.GetOrdinal("monto");
                        int Ordinal_usuario = Reader.GetOrdinal("usuario");
                        int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");
                        int Ordinal_region = Reader.GetOrdinal("region");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_Financiamiento();
                            objENT.numero = Reader.GetInt32(Ordinal_id_partida);
                            objENT.Id = Reader.GetString(Ordinal_codRubro);
                            objENT.observacion = Reader.GetString(Ordinal_descripcion);
                            objENT.monto = Reader.GetDecimal(Ordinal_monto).ToString();
                            objENT.usuario = Reader.GetString(Ordinal_usuario);
                            objENT.strFecha_update = Reader.IsDBNull(Ordinal_fecha_update) ? "" : Reader.GetDateTime(Ordinal_fecha_update).ToString();
                            objENT.tipo = Reader.GetInt32(Ordinal_region);
                            //objENT.nro_convenio = Reader.GetString(Ordinal_nro_convenio);
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_GastosSAV_NE(int id_proyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_GastosSAV_NE]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.String, id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        // -- ANTIGUOS
        //public DataSet spMON_FinanContrapartida(BE_MON_Financiamiento _BE_MON_Financiamiento)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_FinanContrapartida]");
        //        objDE.AddInParameter(objComando, "@id", DbType.Int32, _BE_MON_Financiamiento.id_proyecto);
        //        objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_MON_Financiamiento.tipoFinanciamiento);

        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        //public DataSet spMON_TransferenciaFinanciera(BE_MON_Financiamiento _BE_MON_Financiamiento)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_TransferenciaFinanciera]");
        //        objDE.AddInParameter(objComando, "@id", DbType.String, _BE_MON_Financiamiento.Id);

        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        //public DataSet spMON_SeguimientoSOSEM(BE_MON_Financiamiento _BE_MON_Financiamiento)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_SeguimientoSOSEM]");
        //        objDE.AddInParameter(objComando, "@id", DbType.Int32, _BE_MON_Financiamiento.id_proyecto);
        //        objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_MON_Financiamiento.tipoFinanciamiento);

        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        //public DataSet spMON_FinanTransferencia(BE_MON_Financiamiento _BE_MON_Financiamiento)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_FinanTransferencia]");
        //        objDE.AddInParameter(objComando, "@id", DbType.Int32, _BE_MON_Financiamiento.id_proyecto);
        //        objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_MON_Financiamiento.tipoFinanciamiento);

        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        #endregion

        public int spi_MON_ConvenioDirecta(BE_MON_CONVENIO_DIRECTA _BE_MON_CONVENIO_DIRECTA)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_ConvenioDirecta]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_MON_CONVENIO_DIRECTA.id_proyecto);
                objDE.AddInParameter(objComando, "@idTipoConvenio", DbType.Int32, _BE_MON_CONVENIO_DIRECTA.iTipoConvenio);
                objDE.AddInParameter(objComando, "@detalle", DbType.String, _BE_MON_CONVENIO_DIRECTA.detalle);
                objDE.AddInParameter(objComando, "@fechaConvenio", DbType.DateTime, _BE_MON_CONVENIO_DIRECTA.vFechaConvenio);
                objDE.AddInParameter(objComando, "@entidadFirmante", DbType.String, _BE_MON_CONVENIO_DIRECTA.entidadFirmante);
                objDE.AddInParameter(objComando, "@objetoConvenio", DbType.String, _BE_MON_CONVENIO_DIRECTA.objetoConvenio);
                objDE.AddInParameter(objComando, "@fechaVencimiento", DbType.DateTime, _BE_MON_CONVENIO_DIRECTA.vFechaVencimiento);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_MON_CONVENIO_DIRECTA.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spu_MON_ConvenioDirecta(BE_MON_CONVENIO_DIRECTA _BE_MON_CONVENIO_DIRECTA, int tipo)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_ConvenioDirecta]");

                objDE.AddInParameter(objComando, "@id_convenioDirecta", DbType.Int32, _BE_MON_CONVENIO_DIRECTA.id_convenioDirecta);
                objDE.AddInParameter(objComando, "@idTipoConvenio", DbType.Int32, _BE_MON_CONVENIO_DIRECTA.iTipoConvenio);
                objDE.AddInParameter(objComando, "@detalle", DbType.String, _BE_MON_CONVENIO_DIRECTA.detalle);
                objDE.AddInParameter(objComando, "@fechaConvenio", DbType.DateTime, _BE_MON_CONVENIO_DIRECTA.vFechaConvenio);
                objDE.AddInParameter(objComando, "@entidadFirmante", DbType.String, _BE_MON_CONVENIO_DIRECTA.entidadFirmante);
                objDE.AddInParameter(objComando, "@objetoConvenio", DbType.String, _BE_MON_CONVENIO_DIRECTA.objetoConvenio);
                objDE.AddInParameter(objComando, "@fechaVencimiento", DbType.DateTime, _BE_MON_CONVENIO_DIRECTA.vFechaVencimiento);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_MON_CONVENIO_DIRECTA.id_usuario);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_FinanciamientoDirecta(int id_proyecto, decimal costoEtapa, decimal costoSupervision, string urlCostoEtapa, string urlCostoSupervision, string urlElaboracionEtapa, string urlElaboracionSupervision, string urlPresupuesto, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_FinanciamientoDirecta ]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@costoEtapa", DbType.Decimal, costoEtapa);
                objDE.AddInParameter(objComando, "@costoSupervision", DbType.Decimal, costoSupervision);

                objDE.AddInParameter(objComando, "@urlCostoEtapa", DbType.String, urlCostoEtapa);
                objDE.AddInParameter(objComando, "@urlCostoSupervision", DbType.String, urlCostoSupervision);
                objDE.AddInParameter(objComando, "@urlElaboracionEtapa", DbType.String, urlElaboracionEtapa);
                objDE.AddInParameter(objComando, "@urlElaboracionSupervision", DbType.String, urlElaboracionSupervision);
                objDE.AddInParameter(objComando, "@urlPresupuesto", DbType.String, urlPresupuesto);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spu_MON_SistemaInversion(int id_proyecto, int iTipoSistema, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_SistemaInversion ]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@iTipoSistema", DbType.Int32, iTipoSistema);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spu_MON_PMIServicio(int id_proyecto, int bInfraestructuraAguaPotable, int bInfraestructuraAlcantarillado, int bInfraestructuraAguaResidual, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spu_MON_PMIServicio");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@flagInfraestructuraAguaPotable", DbType.Int32, bInfraestructuraAguaPotable);
                objDE.AddInParameter(objComando, "@flagInfraestructuraAlcantarillado", DbType.Int32, bInfraestructuraAlcantarillado);
                objDE.AddInParameter(objComando, "@flagInfraestructuraAguaResidual", DbType.Int32, bInfraestructuraAguaResidual);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int spi_MON_FinanContrapartida(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FinanContrapartida]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Financiamiento.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Financiamiento.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@entidad_cooperane", DbType.String, _BE_Financiamiento.entidad_cooperante);
                objDE.AddInParameter(objComando, "@aporte", DbType.String, _BE_Financiamiento.id_tipo_aporte);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Financiamiento.monto);
                objDE.AddInParameter(objComando, "@documento", DbType.String, _BE_Financiamiento.documento);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);
                objDE.AddInParameter(objComando, "@montoobra", DbType.String, _BE_Financiamiento.MontoObra);
                objDE.AddInParameter(objComando, "@montosupervison", DbType.String, _BE_Financiamiento.MontoSupervision);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_FinanContrapartidaPE(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FinanContrapartidaPE]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Financiamiento.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Financiamiento.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@entidad_cooperane", DbType.String, _BE_Financiamiento.entidad_cooperante);
                objDE.AddInParameter(objComando, "@aporte", DbType.String, _BE_Financiamiento.id_tipo_aporte);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Financiamiento.monto);
                objDE.AddInParameter(objComando, "@documento", DbType.String, _BE_Financiamiento.documento);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);
                objDE.AddInParameter(objComando, "@id_tipoMoneda", DbType.String, _BE_Financiamiento.tipo);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_ProcesoFinanciero(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_ProcesoFinanciero]");

                objDE.AddInParameter(objComando, "@id_transferenciaFinanciera", DbType.Int32, _BE_Financiamiento.Id_transferenciaFinanciera);
                objDE.AddInParameter(objComando, "@id_tipoProcesoFinanciero", DbType.Int32, _BE_Financiamiento.Id_tipoProcesoFinanciero);
                objDE.AddInParameter(objComando, "@nroRegistro", DbType.String, _BE_Financiamiento.NroRegistro);
                objDE.AddInParameter(objComando, "@fechaRegistro", DbType.DateTime, _BE_Financiamiento.Date_fechaRegistro);
                objDE.AddInParameter(objComando, "@montoRegistrado", DbType.String, _BE_Financiamiento.MontoRegistrado);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoSustento", DbType.Int32, _BE_Financiamiento.Id_tipoDocumentoSustento);
                objDE.AddInParameter(objComando, "@NroDocumentoSustento", DbType.String, _BE_Financiamiento.NroDocumentoSustento);
                objDE.AddInParameter(objComando, "@DocumentoPago", DbType.String, _BE_Financiamiento.DocumentoPago);
                objDE.AddInParameter(objComando, "@NroDocPago", DbType.String, _BE_Financiamiento.NroDocPago);
                objDE.AddInParameter(objComando, "@fechaDocPago", DbType.DateTime, _BE_Financiamiento.Date_fechaDocPago);
                objDE.AddInParameter(objComando, "@Banco", DbType.String, _BE_Financiamiento.Banco);
                objDE.AddInParameter(objComando, "@NroCuentaBanco", DbType.String, _BE_Financiamiento.NroCuentaBanco);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_ProcesoFinanciero(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_ProcesoFinanciero]");

                objDE.AddInParameter(objComando, "@id_proceso_financiero", DbType.Int32, _BE_Financiamiento.Id_proceso_financiero);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Financiamiento.tipo);
                objDE.AddInParameter(objComando, "@id_tipoProcesoFinanciero", DbType.Int32, _BE_Financiamiento.Id_tipoProcesoFinanciero);
                objDE.AddInParameter(objComando, "@nroRegistro", DbType.String, _BE_Financiamiento.NroRegistro);
                objDE.AddInParameter(objComando, "@fechaRegistro", DbType.DateTime, _BE_Financiamiento.Date_fechaRegistro);
                objDE.AddInParameter(objComando, "@montoRegistrado", DbType.String, _BE_Financiamiento.MontoRegistrado);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoSustento", DbType.Int32, _BE_Financiamiento.Id_tipoDocumentoSustento);
                objDE.AddInParameter(objComando, "@NroDocumentoSustento", DbType.String, _BE_Financiamiento.NroDocumentoSustento);
                objDE.AddInParameter(objComando, "@DocumentoPago", DbType.String, _BE_Financiamiento.DocumentoPago);
                objDE.AddInParameter(objComando, "@NroDocPago", DbType.String, _BE_Financiamiento.NroDocPago);
                objDE.AddInParameter(objComando, "@fechaDocPago", DbType.DateTime, _BE_Financiamiento.Date_fechaDocPago);
                objDE.AddInParameter(objComando, "@Banco", DbType.String, _BE_Financiamiento.Banco);
                objDE.AddInParameter(objComando, "@NroCuentaBanco", DbType.String, _BE_Financiamiento.NroCuentaBanco);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_TransferenciaFinanciero(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_TransferenciaFinanciero]");

                objDE.AddInParameter(objComando, "@id_transferenciaFinanciera", DbType.Int32, _BE_Financiamiento.Id_transferenciaFinanciera);
                objDE.AddInParameter(objComando, "@meta", DbType.String, _BE_Financiamiento.Meta);
                objDE.AddInParameter(objComando, "@cadena", DbType.String, _BE_Financiamiento.Cadena);
                objDE.AddInParameter(objComando, "@fte", DbType.String, _BE_Financiamiento.Fte);
                objDE.AddInParameter(objComando, "@UE_destino", DbType.String, _BE_Financiamiento.UE_destino);
                objDE.AddInParameter(objComando, "@girado", DbType.String, _BE_Financiamiento.Girado);
                objDE.AddInParameter(objComando, "@anioFiscal", DbType.String, _BE_Financiamiento.AnioFiscal);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_TransferenciaPresupuestal(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_TransferenciaPresupuestal]");

                objDE.AddInParameter(objComando, "@id_transferencia_presupuestal", DbType.Int32, _BE_Financiamiento.Id_transferencia_presupuesta);
                objDE.AddInParameter(objComando, "@EjecutoraHabilita", DbType.String, _BE_Financiamiento.EjecutoraHabilita);
                objDE.AddInParameter(objComando, "@NotaModificatoria", DbType.String, _BE_Financiamiento.NotaModificatoria);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Financiamiento.Date_Fecha);
                objDE.AddInParameter(objComando, "@EjecutoraHabilitada", DbType.String, _BE_Financiamiento.EjecutoraHabilitada);
                objDE.AddInParameter(objComando, "@CodigoPresupuestal", DbType.String, _BE_Financiamiento.CodigoPresupuestal);
                objDE.AddInParameter(objComando, "@MontoHabilitado", DbType.String, _BE_Financiamiento.MontoHabilitado);
                objDE.AddInParameter(objComando, "@FteFinanciamiento", DbType.String, _BE_Financiamiento.FteFinanciamiento);
                objDE.AddInParameter(objComando, "@flagHabilito", DbType.Int32, _BE_Financiamiento.FlagHabilito);
                objDE.AddInParameter(objComando, "@anioFiscal", DbType.String, _BE_Financiamiento.AnioFiscal);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Financiamiento.id_usuario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_FinanContrapartida(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_FinanContrapartida]");

                objDE.AddInParameter(objComando, "@id_financiamientoContrapartida", DbType.Int32, _BE_Financiamiento.id_financiamientoContrapartida);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Financiamiento.tipo);
                objDE.AddInParameter(objComando, "@entidad_cooperane", DbType.String, _BE_Financiamiento.entidad_cooperante);
                objDE.AddInParameter(objComando, "@aporte", DbType.String, _BE_Financiamiento.id_tipo_aporte);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Financiamiento.monto);
                objDE.AddInParameter(objComando, "@documento", DbType.String, _BE_Financiamiento.documento);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);
                objDE.AddInParameter(objComando, "@montoobra", DbType.String, _BE_Financiamiento.MontoObra);
                objDE.AddInParameter(objComando, "@montosupervision", DbType.String, _BE_Financiamiento.MontoSupervision);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_FinanTransferencia(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FinanTransferencia]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Financiamiento.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Financiamiento.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@id_tipo_transf", DbType.Int32, _BE_Financiamiento.id_tipo_trans);
                objDE.AddInParameter(objComando, "@Nro_convenio", DbType.String, _BE_Financiamiento.nro_convenio);
                objDE.AddInParameter(objComando, "@fecha_convenio", DbType.DateTime, _BE_Financiamiento.Date_Fecha_Convenio);
                objDE.AddInParameter(objComando, "@montoConvenio", DbType.String, _BE_Financiamiento.montoConvenio);
                objDE.AddInParameter(objComando, "@dispositivo", DbType.String, _BE_Financiamiento.dispositivo_aprobacion);
                objDE.AddInParameter(objComando, "@fecha_aprobacion", DbType.DateTime, _BE_Financiamiento.Date_fecha_aprobacion);
                objDE.AddInParameter(objComando, "@monto_aprobacion", DbType.String, _BE_Financiamiento.montoAprobacion);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Financiamiento.observacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);
                objDE.AddInParameter(objComando, "@id_tipoTransferenciaFP", DbType.Int32, _BE_Financiamiento.Tipo_transferencia);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Financiamiento.docUrl);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_FinanTransferencia_Editar(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_FinanTransferencia_Editar]");

                objDE.AddInParameter(objComando, "@id_finan_transferencia", DbType.Int32, _BE_Financiamiento.id_financiamiento);
                objDE.AddInParameter(objComando, "@id_tipo_transf", DbType.Int32, _BE_Financiamiento.id_tipo_trans);
                objDE.AddInParameter(objComando, "@Nro_convenio", DbType.String, _BE_Financiamiento.nro_convenio);
                objDE.AddInParameter(objComando, "@fecha_convenio", DbType.DateTime, _BE_Financiamiento.Date_Fecha_Convenio);
                objDE.AddInParameter(objComando, "@montoConvenio", DbType.String, _BE_Financiamiento.montoConvenio);
                objDE.AddInParameter(objComando, "@dispositivo", DbType.String, _BE_Financiamiento.dispositivo_aprobacion);
                objDE.AddInParameter(objComando, "@fecha_aprobacion", DbType.DateTime, _BE_Financiamiento.Date_fecha_aprobacion);
                objDE.AddInParameter(objComando, "@monto_aprobacion", DbType.String, _BE_Financiamiento.montoAprobacion);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Financiamiento.observacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Financiamiento.docUrl);
                objDE.AddInParameter(objComando, "@id_tipoTransferenciaFP", DbType.Int32, _BE_Financiamiento.Tipo_transferencia);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BE_Financiamiento.id_usuario);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_CartaObra(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_CartaObra]");

                objDE.AddInParameter(objComando, "@id_finan_transferencia", DbType.Int32, _BE_Financiamiento.Id_finaTransferencia);
                objDE.AddInParameter(objComando, "@siaf", DbType.Int32, _BE_Financiamiento.Siaf);

                objDE.AddInParameter(objComando, "@montoCompromiso", DbType.String, _BE_Financiamiento.MontoCompromiso);
                objDE.AddInParameter(objComando, "@montoDevengado", DbType.String, _BE_Financiamiento.MontoDevengado);
                objDE.AddInParameter(objComando, "@montoGirado", DbType.String, _BE_Financiamiento.MontoGirado);

                objDE.AddInParameter(objComando, "@fechaCompromiso", DbType.DateTime, _BE_Financiamiento.Date_FechaCompromiso);
                objDE.AddInParameter(objComando, "@fechaDevengado", DbType.DateTime, _BE_Financiamiento.Date_fechaDevengado);
                objDE.AddInParameter(objComando, "@fechaGirado", DbType.DateTime, _BE_Financiamiento.Date_fechaGirado);

                objDE.AddInParameter(objComando, "@nroCarta", DbType.String, _BE_Financiamiento.NroCarta);
                objDE.AddInParameter(objComando, "@fechaCartaOrden", DbType.DateTime, _BE_Financiamiento.Date_fechaCartaOrden);
                objDE.AddInParameter(objComando, "@nroComprobante", DbType.String, _BE_Financiamiento.NroComprobante);

                objDE.AddInParameter(objComando, "@fechaComprobante", DbType.DateTime, _BE_Financiamiento.Date_fechaComprobante);
                objDE.AddInParameter(objComando, "@fechaTransferencia", DbType.DateTime, _BE_Financiamiento.Date_fechaTransferencia);
                objDE.AddInParameter(objComando, "@fechaEmision", DbType.DateTime, _BE_Financiamiento.Date_fechaemision);

                objDE.AddInParameter(objComando, "@cadena", DbType.String, _BE_Financiamiento.Cadena);
                objDE.AddInParameter(objComando, "@meta", DbType.String, _BE_Financiamiento.Meta);

                objDE.AddInParameter(objComando, "@comentario", DbType.String, _BE_Financiamiento.Comentario);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Financiamiento.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_CartaOrden(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_CartaOrden]");

                objDE.AddInParameter(objComando, "@id_cartaOrden", DbType.Int32, _BE_Financiamiento.Id_CartaOrden);
                objDE.AddInParameter(objComando, "@siaf", DbType.Int32, _BE_Financiamiento.Siaf);

                objDE.AddInParameter(objComando, "@montoCompromiso", DbType.String, _BE_Financiamiento.MontoCompromiso);
                objDE.AddInParameter(objComando, "@montoDevengado", DbType.String, _BE_Financiamiento.MontoDevengado);
                objDE.AddInParameter(objComando, "@montoGirado", DbType.String, _BE_Financiamiento.MontoGirado);

                objDE.AddInParameter(objComando, "@fechaCompromiso", DbType.DateTime, _BE_Financiamiento.Date_FechaCompromiso);
                objDE.AddInParameter(objComando, "@fechaDevengado", DbType.DateTime, _BE_Financiamiento.Date_fechaDevengado);
                objDE.AddInParameter(objComando, "@fechaGirado", DbType.DateTime, _BE_Financiamiento.Date_fechaGirado);

                objDE.AddInParameter(objComando, "@nroCarta", DbType.String, _BE_Financiamiento.NroCarta);
                objDE.AddInParameter(objComando, "@fechaCartaOrden", DbType.DateTime, _BE_Financiamiento.Date_fechaCartaOrden);
                objDE.AddInParameter(objComando, "@nroComprobante", DbType.String, _BE_Financiamiento.NroComprobante);

                objDE.AddInParameter(objComando, "@fechaComprobante", DbType.DateTime, _BE_Financiamiento.Date_fechaComprobante);
                objDE.AddInParameter(objComando, "@fechaTransferencia", DbType.DateTime, _BE_Financiamiento.Date_fechaTransferencia);
                objDE.AddInParameter(objComando, "@fechaEmision", DbType.DateTime, _BE_Financiamiento.Date_fechaemision);

                objDE.AddInParameter(objComando, "@cadena", DbType.String, _BE_Financiamiento.Cadena);
                objDE.AddInParameter(objComando, "@meta", DbType.String, _BE_Financiamiento.Meta);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Financiamiento.id_usuario);
                objDE.AddInParameter(objComando, "@comentario", DbType.String, _BE_Financiamiento.Comentario);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Financiamiento.tipo);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiu_MON_PresupuestoPNVR(BE_MON_Financiamiento _BE_Financiamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_PresupuestoPNVR]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Financiamiento.id_proyecto);
                objDE.AddInParameter(objComando, "@id_partida", DbType.Int32, _BE_Financiamiento.Id);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Financiamiento.monto);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Financiamiento.id_usuario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int U_spu_MON_FlagPresupuestoPNVR(BE_MON_Financiamiento _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_FlagPresupuestoPNVR", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@flagFinalizo", SqlDbType.Int).Value = _BE.flag;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_CodigoSeace(int idProyecto, int idEtapa)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spMON_CodigoSeace]");
                objDE.AddInParameter(objComando, "@idProyecto", DbType.Int32, idProyecto);
                objDE.AddInParameter(objComando, "@idEtapa", DbType.Int32, idEtapa);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_SEACE_Cronograma(int idCodigoConvocatoria)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spMON_SEACE_Cronograma]");
                objDE.AddInParameter(objComando, "@codigoConvocatoria", DbType.Int32, idCodigoConvocatoria);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_SEACE_Obra(int snip, int idProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spMON_SEACE_Obra]");
                objDE.AddInParameter(objComando, "@snip", DbType.Int32, snip);
                objDE.AddInParameter(objComando, "@IdProyecto", DbType.Int32, idProyecto);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        //public List<BE_MON_SEACE> spMON_SEACE_Obra(int snip)
        //{
        //    BE_MON_SEACE objENT = new BE_MON_SEACE();
        //    List<BE_MON_SEACE> objCollection = new List<BE_MON_SEACE>();
        //    using (SqlConnection Cnx = new SqlConnection(strCadenaConexion))
        //    {
        //        SqlCommand cmd = default(SqlCommand);
        //        try
        //        {
        //            cmd = new SqlCommand();
        //            cmd.Connection = Cnx;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.CommandText = "spMON_SEACE_Obra";
        //            cmd.CommandTimeout = 36000;

        //            cmd.Parameters.Add("@snip", SqlDbType.Int).Value = snip;


        //            cmd.Connection.Open();
        //            using (SqlDataReader Reader = cmd.ExecuteReader())
        //            {
        //                while (Reader.Read())
        //                {
        //                    objENT = new BE_MON_SEACE();
                            
        //                    objENT.item = Convert.ToInt32(Reader["item"].ToString());
        //                    objENT.Objeto = Convert.ToString(Reader["TipoContratacion"].ToString());
        //                    objENT.IDENTIFICADOR = Convert.ToString(Reader["IDENTIFICADOR"].ToString());
        //                    objENT.TipoSeleccion = Convert.ToString(Reader["TipoSeleccion"].ToString());
        //                    objENT.NOMENCLATURA = Convert.ToString(Reader["NOMENCLATURA"].ToString());
        //                    objENT.FechaPublicacion = Convert.ToString(Reader["FechaPublicacion"].ToString());
        //                    objENT.EstadoItem = Convert.ToString(Reader["EstadoItem"].ToString());
        //                    if (Reader["FECHA_ACCION"] != DBNull.Value)
        //                    {
        //                        objENT.FECHA_ACCION = Convert.ToString(Reader["FECHA_ACCION"].ToString());
        //                    }

        //                    objENT.ValorReferencial = Convert.ToString(Reader["ValorReferencial"].ToString());
        //                    objCollection.Add(objENT);

        //                    objENT = null;
        //                }

        //            }
        //            //   Reader.Close();
        //            return objCollection;

        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;

        //        }
        //        finally
        //        {
        //            cmd = null;
        //            if (Cnx.State == ConnectionState.Open)
        //            {
        //                Cnx.Close();
        //            }
        //        }
        //    }
        //}

        //public List<BE_MON_SEACE> spMON_SEACE_Consultoria(int snip)
        //{
        //    BE_MON_SEACE objENT = new BE_MON_SEACE();
        //    List<BE_MON_SEACE> objCollection = new List<BE_MON_SEACE>();
        //    SqlConnection Cnx = new SqlConnection(strCadenaConexion);
        //    SqlCommand cmd = default(SqlCommand);
        //    try
        //    {
        //        cmd = new SqlCommand();
        //        cmd.Connection = Cnx;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "spMON_SEACE_Consultoria";
        //        cmd.CommandTimeout = 36000;

        //        cmd.Parameters.Add("@snip", SqlDbType.Int).Value = snip;


        //        cmd.Connection.Open();
        //        using (SqlDataReader Reader = cmd.ExecuteReader())
        //        {
        //            while (Reader.Read())
        //            {
        //                objENT = new BE_MON_SEACE();
        //                objENT.item = Convert.ToInt32(Reader["item"].ToString());
        //                objENT.IDENTIFICADOR = Convert.ToString(Reader["IDENTIFICADOR"].ToString());
        //                objENT.TipoSeleccion = Convert.ToString(Reader["TipoSeleccion"].ToString());
        //                objENT.NOMENCLATURA = Convert.ToString(Reader["NOMENCLATURA"].ToString());
        //                objENT.FechaPublicacion = Convert.ToString(Reader["FechaPublicacion"].ToString());
        //                objENT.DescripcionItem = Convert.ToString(Reader["DescripcionItem"].ToString());
        //                objENT.EstadoItem = Convert.ToString(Reader["EstadoItem"].ToString());
        //                if (Reader["FECHA_ACCION"] != DBNull.Value)
        //                {
        //                    objENT.FECHA_ACCION = Convert.ToString(Reader["FECHA_ACCION"].ToString());
        //                }
        //                objENT.ValorReferencial = Convert.ToString(Reader["ValorReferencial"].ToString());
        //                objCollection.Add(objENT);
        //                objENT = null;
        //            }

        //        }
        //        //   Reader.Close();
        //        return objCollection;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;

        //    }
        //    finally
        //    {
        //        cmd = null;
        //        if (Cnx.State == ConnectionState.Open)
        //        {
        //            Cnx.Close();
        //        }
        //    }

        //}

        //public List<BE_MON_SEACE> spMON_SEACE_BienesServicios(int snip)
        //{
        //    BE_MON_SEACE objENT = new BE_MON_SEACE();
        //    List<BE_MON_SEACE> objCollection = new List<BE_MON_SEACE>();
        //    SqlConnection Cnx = new SqlConnection(strCadenaConexion);
        //    SqlCommand cmd = default(SqlCommand);
        //    try
        //    {
        //        cmd = new SqlCommand();
        //        cmd.Connection = Cnx;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "spMON_SEACE_BienesServicios";
        //        cmd.CommandTimeout = 36000;

        //        cmd.Parameters.Add("@snip", SqlDbType.Int).Value = snip;


        //        cmd.Connection.Open();
        //        using (SqlDataReader Reader = cmd.ExecuteReader())
        //        {
        //            while (Reader.Read())
        //            {
        //                objENT = new BE_MON_SEACE();
        //                objENT.IDENTIFICADOR = Convert.ToString(Reader["IDENTIFICADOR"].ToString());
        //                objENT.item = Convert.ToInt32(Reader["item"].ToString());
        //                objENT.TipoSeleccion = Convert.ToString(Reader["TipoSeleccion"].ToString());
        //                objENT.NOMENCLATURA = Convert.ToString(Reader["NOMENCLATURA"].ToString());
        //                objENT.FechaPublicacion = Convert.ToString(Reader["FechaPublicacion"].ToString());
        //                objENT.DescripcionItem = Convert.ToString(Reader["DescripcionItem"].ToString());
        //                objENT.EstadoItem = Convert.ToString(Reader["EstadoItem"].ToString());
        //                objENT.ValorReferencialItem = Convert.ToString(Reader["ValorReferencialItem"].ToString());
        //                objCollection.Add(objENT);
        //                objENT = null;
        //            }

        //        }
        //        //   Reader.Close();
        //        return objCollection;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;

        //    }
        //    finally
        //    {
        //        cmd = null;
        //        if (Cnx.State == ConnectionState.Open)
        //        {
        //            Cnx.Close();
        //        }
        //    }

        //}

        public DataSet spMON_SEACE_BienesServicio_Detalle(int identificador)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_SEACE_Obra_Detalle]");
                objDE.AddInParameter(objComando, "@IDENTIFICADOR", DbType.String, identificador);

                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Metodo que devuelve info detallada de financiamiento de un proyecto como tal
        /// </summary>
        /// <param name="idProyecto">Id del proyecto a obtener info de financiamiento</param>
        /// <seealso cref="String">
        /// https://trello.com/c/uqVY2SZM/41-obras
        /// </seealso>
        public DataSet paSSP_MON_rFinanciamiento(int idProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[convenio].[paSSP_rFinanciamiento]");
                objDE.AddInParameter(objComando, "@idProyecto", DbType.Int32, idProyecto);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        /// <summary>
        /// Obtiene los tipos de financiamiento para ser usados en el form de nueva etapa
        /// (https://trello-attachments.s3.amazonaws.com/58eba3ab0c937aa30d911773/59419673674176b5f2030b9e/aeb697025dcb59a10a8ef4dceb5c7ded/2017-06-21_18_03_06-Obra.png)
        /// </summary>
        /// <param name="idTipoFinanciamiento"></param>
        /// <returns></returns>
        public DataSet paSSP_MON_rTipoFinanciamientos(int idTipoFinanciamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_MON_rTipoFinanciamientos]");
                objDE.AddInParameter(objComando, "@idTipoFinanciamiento", DbType.Int32, idTipoFinanciamiento);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Invoca a un SP para guardar una etapa de proyecto (obra, exp tecnico, etc)
        /// https://trello.com/c/uqVY2SZM/41-obras
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idTipoFinanciamiento"></param>
        /// <param name="vEtapa"></param>
        /// <param name="idEtapa"></param>
        /// <returns></returns>
        public DataSet paSSP_MON_gPROYECTO(int idProyecto, int idUsuario, int idTipoFinanciamiento, int vEtapa, int idEtapa)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_MON_gPROYECTO]");
                objDE.AddInParameter(objComando, "@idProyecto", DbType.Int32, idProyecto);
                objDE.AddInParameter(objComando, "@idUsuario", DbType.Int32, idUsuario);
                objDE.AddInParameter(objComando, "@idTipoFinanciamiento", DbType.Int32, idTipoFinanciamiento);
                objDE.AddInParameter(objComando, "@bEtapa", DbType.Int32, vEtapa);
                objDE.AddInParameter(objComando, "@idEtapa", DbType.Int32, idEtapa);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            finally
            { }
        }


        public DataSet paSSP_spiu_MON_VigenciaVencida(int idProyecto, int idUsuario, int flagVigenciaVencida, int idTipoAccion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_VigenciaVencida]");
                objDE.AddInParameter(objComando, "@idProyecto", DbType.Int32, idProyecto);
                objDE.AddInParameter(objComando, "@idUsuario", DbType.Int32, idUsuario);
                objDE.AddInParameter(objComando, "@flagVigenciaVencida", DbType.Int32, flagVigenciaVencida);
                objDE.AddInParameter(objComando, "@idTipoAccion", DbType.Int32, idTipoAccion);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public DataSet spMON_VigenciaVencida(int idProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_VigenciaVencida]");
                objDE.AddInParameter(objComando, "@idProyecto", DbType.Int32, idProyecto);
          
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            finally
            { }
        }


        /// <summary>
        /// Obtiene las metas de un proyecto como tal
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <returns>Dataset</returns>
        public DataSet paSSP_MON_rMetas(int idProyecto,  int etapaRegistro , int tipoMeta)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_MON_rResumenMetas]");
                objDE.AddInParameter(objComando, "@id", DbType.Int32, idProyecto);
                objDE.AddInParameter(objComando, "@TipoMeta", DbType.Int32, tipoMeta);
                objDE.AddInParameter(objComando, "@EtapaRegistro", DbType.Int32, etapaRegistro);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public DataSet spMEF_Financiamiento(int pSnip)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMEF_Devengados]");
                objDE.AddInParameter(objComando, "@snip", DbType.Int32, pSnip);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public DataSet spMON_SeguimientoFinancieroPriorizados()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spMON_SeguimientoFinancieroPriorizados]");
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

 
        public DataSet spd_MON_IncorporacionRegistro(int id_proyecto, int Incorporado, DateTime? FechaIncorporacion, decimal? MontoIncorporado, string ActaResolucionAlcaldia, int UsuarioRegistro,
            int RequiereActualizacionETCOVID19, int ETCuentaPartidaCOVID19, int TieneResolucionAprobacionET)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spd_MON_IncorporacionRegistro");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@Incorporado", DbType.Int16, Incorporado);
                objDE.AddInParameter(objComando, "@FechaIncorporacion", DbType.Date, FechaIncorporacion);
                objDE.AddInParameter(objComando, "@MontoIncorporado", DbType.Decimal, MontoIncorporado);
                objDE.AddInParameter(objComando, "@ActaResolucionAlcaldia", DbType.String, ActaResolucionAlcaldia);
                objDE.AddInParameter(objComando, "@UsuarioRegistro", DbType.Int32, UsuarioRegistro);
                objDE.AddInParameter(objComando, "@RequiereActualizacionETCOVID19", DbType.Int16, RequiereActualizacionETCOVID19);
                objDE.AddInParameter(objComando, "@ETCuentaPartidaCOVID19", DbType.Int16, ETCuentaPartidaCOVID19);
                objDE.AddInParameter(objComando, "@TieneResolucionAprobacionET", DbType.Int16, TieneResolucionAprobacionET);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw new Exception(ex.Message); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }

        public DataSet spd_MON_ObtenerIncorporacionRegistro(int id_proyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spd_MON_ObtenerIncorporacionRegistro");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
    }
}
