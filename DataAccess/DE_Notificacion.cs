﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DE_Notificacion
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public DataSet spMON_Notificacion(string pSnip, string pUnificado, int pIdUsuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Notificacion]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, pSnip);
                objDE.AddInParameter(objComando, "@unificado", DbType.String, pUnificado);
                objDE.AddInParameter(objComando, "@idUsuario", DbType.Int32, pIdUsuario);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
