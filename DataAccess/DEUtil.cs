using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
namespace DataAccess
{
    public class DEUtil
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        #region Consulta

        //public DataSet sp_listaSubSector(int intCodSubSec)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[sp_validarUsuario]");
        //        objDE.AddInParameter(objComando, "login", DbType.String, strLogin);
        //        objDE.AddInParameter(objComando, "passw", DbType.String, strPassword);
        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}
        /// <summary>
        /// camilo verificacion del ambito fonie
        /// </summary>
        /// <param name="ubigeo"></param>
        /// <returns></returns>
        public bool sp_GetFlagAmbitoFonie(string ubigeo)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                //buscar por codigo o nombre
                DbCommand objComando = objDE.GetStoredProcCommand("sp_GetFlagAmbitoFonie");
                objDE.AddInParameter(objComando, "@ubigeo", DbType.String, ubigeo);
                return Convert.ToBoolean(Convert.ToInt32(objDE.ExecuteScalar(objComando)) >= 1 ? true : false);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// camilo retorno del centro poblado
        /// </summary>
        /// <param name="ubigeo"></param>
        /// <returns></returns>
        //public List<BECentroPoblado> sp_GetCentroPoblado(string ubigeo)
        //{
        //    try
        //    {
        //        List<BECentroPoblado> result = new List<BECentroPoblado>();
        //        DataTable dt = null;
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        //buscar por codigo o nombre
        //        DbCommand objComando = objDE.GetStoredProcCommand("sp_GetCentroPoblado");
        //        objDE.AddInParameter(objComando, "@ubigeo", DbType.String, ubigeo);
        //        dt = objDE.ExecuteDataSet(objComando).Tables[0];
        //        BECentroPoblado ccpp = new BECentroPoblado();
        //        /*ccpp.nombre = "-SELECCIONAR-";
        //        ccpp.ubigeo = "00";
        //        ccpp.ubigeo_ccpp = "00";
        //        ccpp.area = "";
        //        ccpp.total = 0;
        //        ccpp.hombre = 0;
        //        ccpp.mujer = 0;
        //        result.Add(ccpp);
        //        ccpp = null;*/
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            ccpp = new BECentroPoblado();
        //            ccpp.nombre = row[0].ToString();
        //            ccpp.ubigeo = row[1].ToString();
        //            ccpp.ubigeo_ccpp = row[2].ToString();
        //            ccpp.area = row[3].ToString();
        //            ccpp.total = Convert.ToDouble(row[4].ToString());
        //            ccpp.hombre = Convert.ToDouble(row[5].ToString());
        //            ccpp.mujer = Convert.ToDouble(row[6].ToString());
        //            ccpp.idSolicitud = Convert.ToInt32(row[7].ToString());                    
        //            result.Add(ccpp);
        //        }

        //        return result;
        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<BECentroPoblado> sp_GetCentroPoblado(string ubigeo, int IdSolicitud)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BECentroPoblado objENT = new BECentroPoblado();
            List<BECentroPoblado> objCollection = new List<BECentroPoblado>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_GetCentroPobladoBySolicitud";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@ubigeo", SqlDbType.VarChar, 6).Value = ubigeo;
                cmd.Parameters.Add("@idSolicitud", SqlDbType.Int).Value = IdSolicitud;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BECentroPoblado();

                        objENT.nombre = Reader["nombre"].ToString();
                        objENT.ubigeo = Reader["ubigeo"].ToString();
                        objENT.ubigeo_ccpp = Reader["ubigeo_ccpp"].ToString();
                        objENT.area = Reader["area"].ToString();
                        objENT.total = Convert.ToDouble(Reader["total"].ToString());
                        objENT.hombre = Convert.ToDouble(Reader["hombre"].ToString());
                        objENT.mujer = Convert.ToDouble(Reader["mujer"].ToString());
                        objENT.idSolicitud = Convert.ToInt32(Reader["id_Solicitud"].ToString());
                        //Reader.GetInt32(codigo).ToString();


                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            
        /// <summary>
        /// camilo verificación de PNSR
        /// </summary>
        /// <param name="idsolicitud"></param>
        /// <param name="ubigeo"></param>
        /// <returns></returns>
        public bool sp_verifica_Ubigeo_pnsr(string ubigeo)
        //carga banco de proyectos
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("sp_verifica_Ubigeo_pnsr");
                objDE.AddInParameter(objComando, "@ubigeo", DbType.String, ubigeo);
                return Convert.ToBoolean(Convert.ToInt32(objDE.ExecuteScalar(objComando)) >= 1 ? true : false);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int sp_obtenerCodigoItem(string strCodigo, int intIdTabla, int intCod_prog, int intCelda)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                //buscar por codigo o nombre

                DbCommand objComando = objDE.GetStoredProcCommand("sp_obtenerIdItem");
                objDE.AddInParameter(objComando, "@strCodigo", DbType.String, strCodigo);
                objDE.AddInParameter(objComando, "@intIdTabla", DbType.Int32, intIdTabla);
                objDE.AddInParameter(objComando, "@intCodPrograma", DbType.Int32, intCod_prog);
                objDE.AddInParameter(objComando, "@intCelda", DbType.Int32, intCelda);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet sp_listarDepartamento(int tipo)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_listarDepartamento]");
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        public DataTable sp_getDepartamento()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getDepartamento]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        public DataTable sp_getEstado()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getEstado]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getNivelSol()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getNivelSol]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getOficina()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getOficina]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getPrograma()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getPrograma]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getSector()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getSector]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getTipoProb()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getTipoProb]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getTipoProy()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getTipoProy]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getPrioridad()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getPrioridad]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        public DataTable sp_getNivelProb()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[sp_getNivelProb]");
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //NACIONAL
        public DataTable spN_ListarCombos(string id_tabla, string param)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spN_ListarCombos]");
                objDE.AddInParameter(objComando, "@tabla", DbType.String, id_tabla);
                objDE.AddInParameter(objComando, "@param1", DbType.String, param);
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //EMERGENCIA
        public DataTable spN_ListarCombosODN(string id_tabla, string param)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                if (id_tabla.Equals("1"))//sectores entidades
                {
                    objComando = objDE.GetStoredProcCommand("[sp_D_Segui_sectores]");
                    objDE.AddInParameter(objComando, "@id_privilegios", DbType.Int32, Convert.ToInt32(param));
                }
                else
                {
                    objComando = objDE.GetStoredProcCommand("[sp_SSP_ListarCombos]");
                    objDE.AddInParameter(objComando, "@tabla", DbType.String, id_tabla);
                    objDE.AddInParameter(objComando, "@param1", DbType.String, param);
                }


                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //SOLICITUD
        public DataTable sp_ListarCombosSOL_SSP(string id_tabla, string param)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("[spSOL_listarCombosSOLIC]");
                objDE.AddInParameter(objComando, "@tabla", DbType.String, id_tabla);
                objDE.AddInParameter(objComando, "@param", DbType.String, param);


                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        public DataTable sp_ListarCombosSOL(string id_tabla, string param)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("[sp_listarCombos]");
                objDE.AddInParameter(objComando, "@tabla", DbType.String, id_tabla);
                objDE.AddInParameter(objComando, "@param", DbType.String, param);


                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //ANUNCIOS
        public DataTable sp_ListarCombosANUN(string id_tabla, string param)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("[sp_listarCombosANUNCIOS]");
                objDE.AddInParameter(objComando, "@tabla", DbType.String, id_tabla);
                objDE.AddInParameter(objComando, "@param", DbType.String, param);


                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //DS TRANSFERENCIAS
        public DataTable sp_ListarCombosDS(string id_tabla, string param, string param1)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("[DS_sp_listarCombos]");
                objDE.AddInParameter(objComando, "@tabla", DbType.String, id_tabla);
                objDE.AddInParameter(objComando, "@param", DbType.String, param);
                objDE.AddInParameter(objComando, "@param1", DbType.String, param1);

                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
                
        public List<BEProyecto> F_spSOL_Listar_Departamento(int tipo, string cod_depa, string cod_prov, string cod_dist)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BEProyecto objENT = new BEProyecto();
            List<BEProyecto> objCollection = new List<BEProyecto>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_Listar_Departamento";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = tipo;
                cmd.Parameters.Add("@cod_depa", SqlDbType.Char, 2).Value = cod_depa;
                cmd.Parameters.Add("@cod_prov", SqlDbType.Char, 2).Value = cod_prov;
                cmd.Parameters.Add("@cod_dist", SqlDbType.Char, 2).Value = cod_dist;


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int codigo = Reader.GetOrdinal("codigo");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.Codigo = Reader.GetString(codigo);
                        objENT.Nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            

        //public DataTable spSOL_obtieneID_estado_snip(string id_estado_snip)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando;
        //        objComando = objDE.GetStoredProcCommand("spSOL_obtieneID_estado_snip");
        //        objDE.AddInParameter(objComando, "@estado_snip", DbType.String, id_estado_snip);
        //        return objDE.ExecuteDataSet(objComando).Tables[0];
        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}

        public List<BEProyecto> F_spSOL_obtieneID_estado_snip(string id_estado_snip)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BEProyecto objENT = new BEProyecto();
            List<BEProyecto> objCollection = new List<BEProyecto>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_obtieneID_estado_snip";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@estado_snip", SqlDbType.VarChar,100).Value = id_estado_snip;
       

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int codigo = Reader.GetOrdinal("codigo");
                    int nombre = Reader.GetOrdinal("estado");

                    while (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.Codigo = Reader.GetInt32(codigo).ToString();
                        objENT.Nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            

        public List<BEProyecto> F_spSOL_Listar_Estrategia()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BEProyecto objENT = new BEProyecto();
            List<BEProyecto> objCollection = new List<BEProyecto>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_Listar_Estrategia";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int codigo = Reader.GetOrdinal("codigo");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.Codigo = Reader.GetInt32(codigo).ToString();
                        objENT.Nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            
    
        public List<BEProyecto> F_spSOL_Listar_Estado_Snip()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BEProyecto objENT = new BEProyecto();
            List<BEProyecto> objCollection = new List<BEProyecto>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_Listar_Estado_Snip";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int codigo = Reader.GetOrdinal("codigo");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.Codigo = Reader.GetInt32(codigo).ToString();
                        objENT.Nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            

        public DataTable spSOL_Informacion(Int32 tipo, Int64 codigo, string variable, int cod_estado)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("[spSOL_Informacion]");
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                objDE.AddInParameter(objComando, "@codigo", DbType.Int64, codigo);
                objDE.AddInParameter(objComando, "@variable", DbType.String, variable);
                objDE.AddInParameter(objComando, "@cod_estado", DbType.String, cod_estado);
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //public DataTable spSOL_ListaTecnicoRevision(string codigo)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando;
        //        objComando = objDE.GetStoredProcCommand("[spSOL_ListaTecnicoRevision]");

        //        objDE.AddInParameter(objComando, "@codigo", DbType.String, codigo);
        //        return objDE.ExecuteDataSet(objComando).Tables[0];
        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }

        //}

        public List<BEProyecto> F_spSOL_ListaTecnicoRevision(string codigo)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BEProyecto objENT = new BEProyecto();
            List<BEProyecto> objCollection = new List<BEProyecto>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_ListaTecnicoRevision";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@codigo", SqlDbType.VarChar, 2).Value = codigo;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int icodigo = Reader.GetOrdinal("codigo");
                    int inombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.Codigo = Reader.GetInt32(icodigo).ToString();
                        objENT.Nombre = Reader.GetString(inombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            

        //TAMBOS

        public DataTable spFOES_Estados(Int32 tipo)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("[spFOES_Estados]");
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }


        public DataTable spSOL_obtieneID_departamento(int tipo, string cod_depa, string cod_prov, string cod_dist)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando;
                objComando = objDE.GetStoredProcCommand("spSOL_obtieneID_departamento");
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                objDE.AddInParameter(objComando, "@cod_depa", DbType.String, cod_depa);
                objDE.AddInParameter(objComando, "@cod_prov", DbType.String, cod_prov);
                objDE.AddInParameter(objComando, "@cod_dist", DbType.String, cod_dist);
                return objDE.ExecuteDataSet(objComando).Tables[0];
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }
        #endregion


     
    }
}
