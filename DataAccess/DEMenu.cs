using System;
using System.Collections.Generic;
using System.Data;
using Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

public class DEMenu
{
    private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

     public List<BEMenu> F_sp_ListarOpcionesMenu(int usr_usuario)
    {
        using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
        {
            BEMenu objENT = new BEMenu();
            List<BEMenu> objCollection = new List<BEMenu>();
            SqlCommand cmd = default(SqlCommand);

            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_ListarOpcionesMenu";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = usr_usuario;


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int id_menu = Reader.GetOrdinal("id_menu");
                    int id_usuario = Reader.GetOrdinal("id_usuario");
                    int titulo = Reader.GetOrdinal("titulo");
                    int subtitulo = Reader.GetOrdinal("subtitulo");
                    int pagina = Reader.GetOrdinal("pagina");
                    int id_acceso = Reader.GetOrdinal("id_Acceso");
                    //int subtitulo2 = Reader.GetOrdinal("subtitulo2");

                    while (Reader.Read())
                    {
                        objENT = new BEMenu();

                        objENT.id_menu = Reader.GetInt32(id_menu);
                        objENT.id_usuario = Reader.GetInt32(id_usuario);
                        objENT.titulo = Reader.GetString(titulo);
                        objENT.subtitulo = Reader.GetString(subtitulo);
                        objENT.pagina = Reader.GetString(pagina);
                        objENT.id_Acceso = Reader.GetInt32(id_acceso);
                      //  objENT.subtitulo2 = Reader.GetString(subtitulo2);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }
        }
    }

    public DataSet sp_ListarMenusxUsuario(int usr_usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_ListarMenusxUsuario]");
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, usr_usuario);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sp_ListarMenusxUsuarioNew(int usr_usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_ListarMenusxUsuarioNew]");
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, usr_usuario);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


}

