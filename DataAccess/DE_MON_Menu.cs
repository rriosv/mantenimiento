﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace DataAccess
{
    public class DE_MON_Menu
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
        public List<BE_MON_Menu> paSSP_MON_rEtapas(int snip, int pPantalla)
        {
            BE_MON_Menu objENT = new BE_MON_Menu();
            List<BE_MON_Menu> objCollection = new List<BE_MON_Menu>();
            using (SqlConnection Cnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "paSSP_MON_rEtapas";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = snip;
                    cmd.Parameters.Add("@pPantalla", SqlDbType.Int).Value = pPantalla;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        while (Reader.Read())
                        {
                            objENT = new BE_MON_Menu();                      
                            objENT.idProyecto = Convert.ToInt32(Reader["idProyecto"].ToString());
                            objENT.idTipoFinanciamiento = Convert.ToInt32(Reader["idTipoFinanciamiento"].ToString());
                            objENT.tipoFinanciamiento = Convert.ToString(Reader["vtipoFinanciamiento"].ToString());
                            objENT.flagUltimo = Convert.ToInt32(Reader["FlagUltimo"].ToString());
                            objENT.idModalidadFinanciamiento = Convert.ToInt32(Reader["idModalidadFinanciamiento"].ToString());
                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
    }
}
