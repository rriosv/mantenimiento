﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Data;
using System.Data.Common;
namespace DataAccess
{
    public class DE_MON_Historial
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public DataSet spMON_Proyecto_HistorialMonitoreo(int id_proyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Proyecto_HistorialMonitoreo]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                //objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_PROYECTO_HistorialMonitoreo(BE_MON_Historial _BE_Historial)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_PROYECTO_HistorialMonitoreo]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Historial.Id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Historial.TipoFinanciamiento);

                objDE.AddInParameter(objComando, "@snip", DbType.Int32, _BE_Historial.Snip);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Historial.Date_fecha);
                objDE.AddInParameter(objComando, "@comentario", DbType.String, _BE_Historial.Comentario);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Historial.Url);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Historial.Id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Proyecto_HistorialMonitoreo(BE_MON_Historial _BE_Historial)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Proyecto_HistorialMonitoreo]");

                objDE.AddInParameter(objComando, "@id_proyecto_historial", DbType.Int32, _BE_Historial.Id_proyecto_historial);
            
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Historial.Date_fecha);
                objDE.AddInParameter(objComando, "@comentario", DbType.String, _BE_Historial.Comentario);
                objDE.AddInParameter(objComando, "@url", DbType.String, _BE_Historial.Url);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Historial.Id_usuario);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Historial.Tipo);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
