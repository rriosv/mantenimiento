using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;

public class DETransferencia
{
    private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
    #region Consulta

    public DataSet spSOL_Seguimiento_Det_Transferenica(BETransferencia _BETransferencia)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Det_Transferenica]");
            objDE.AddInParameter(objComando, "@id_transf", DbType.Int64, _BETransferencia.Id_Transf);
            objDE.AddInParameter(objComando, "@id_det_tranf", DbType.Int32, _BETransferencia.Id_Det_Transf);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BETransferencia.Tipo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Seguimiento_Transferencia(BETransferencia _BETransferencia)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Transferencia]");
            objDE.AddInParameter(objComando, "@id_transf", DbType.Int64, _BETransferencia.Id_Transf);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _BETransferencia.Id_Subsector);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BETransferencia.Cod_Estado);
            objDE.AddInParameter(objComando, "@nro", DbType.String, _BETransferencia.Nro_Ds);
            objDE.AddInParameter(objComando, "@anioDS", DbType.Int32, _BETransferencia.AnioDS);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public DataSet spSOL_Reporte_Transferencia(BETransferencia _BETransferencia)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Reporte_Transferencia]");
            
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _BETransferencia.Id_Subsector);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BETransferencia.Cod_Estado);
            objDE.AddInParameter(objComando, "@nro", DbType.String, _BETransferencia.Nro_Ds);
            objDE.AddInParameter(objComando, "@anioDS", DbType.Int32, _BETransferencia.AnioDS);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Seguimiento_Presupuesto(BEPresupuesto _BEPresupuesto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Presupuesto]");
            objDE.AddInParameter(objComando, "@id_presupuesto", DbType.Int64, _BEPresupuesto.Id_Presupuesto);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _BEPresupuesto.Periodo);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _BEPresupuesto.Cod_SubSector);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    #endregion
    #region "Transaccion"

    public void spiuSOL_Ingresar_Presupuesto(BEPresupuesto _BEPresupuesto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_Ingresar_Presupuesto]");
            objDE.AddInParameter(objComando, "@id_presupuesto", DbType.Int64, _BEPresupuesto.Id_Presupuesto);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BEPresupuesto.Tipo);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEPresupuesto.Id_Usuario);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _BEPresupuesto.Cod_SubSector);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _BEPresupuesto.Periodo);
            objDE.AddInParameter(objComando, "@monto", DbType.Double, _BEPresupuesto.Monto);
            objDE.AddInParameter(objComando, "@aumento", DbType.Double, _BEPresupuesto.Aumento);
            objDE.AddInParameter(objComando, "@descuento", DbType.Double, _BEPresupuesto.Descuento);
            objDE.AddInParameter(objComando, "@monto_calculo", DbType.Double, _BEPresupuesto.Monto_Calculado);
            objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BEPresupuesto.Descripcion);
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEPresupuesto.Id_Solcitudes);
            objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }
    }

    public void spu_Actualizar_Montos_Tranf_MEF(Int64 id_transf, string ds, string oficio, int tipoDoc, string OficioNro, string OficioAnio, string DocNro, string DocAnio,string FechaOficio,string FechaDoc,int IdEntidadFinanciera)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spu_Actualizar_Montos_Tranf_MEF]");
            objDE.AddInParameter(objComando, "@id_transf", DbType.Int64, id_transf);
            objDE.AddInParameter(objComando, "@ds", DbType.String, ds);
            objDE.AddInParameter(objComando, "@oficio", DbType.String, oficio);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipoDoc);
            objDE.AddInParameter(objComando, "@DocNro", DbType.String, DocNro);
            objDE.AddInParameter(objComando, "@DocAnio", DbType.String, DocAnio);
            objDE.AddInParameter(objComando, "@OficioNro", DbType.String, OficioNro);
            objDE.AddInParameter(objComando, "@OficioAnio", DbType.String, OficioAnio);
            objDE.AddInParameter(objComando, "@fechaOficio", DbType.String, FechaOficio);
            objDE.AddInParameter(objComando, "@fechaDoc", DbType.String, FechaDoc);
            objDE.AddInParameter(objComando, "@idEntidadFinanciera", DbType.Int32, IdEntidadFinanciera);
            objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }
    }

    public void spuSOL_Transferencia_MEF(BETransferencia _BETransferencia)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_Transferencia_MEF]");
            objDE.AddInParameter(objComando, "@id_transf", DbType.Int64, _BETransferencia.Id_Transf);
            objDE.AddInParameter(objComando, "@id_det_transf", DbType.Int64, _BETransferencia.Id_Det_Transf);
            objDE.AddInParameter(objComando, "@snip", DbType.Int32, _BETransferencia.Snip);
            objDE.AddInParameter(objComando, "@monto_transf", DbType.Double, _BETransferencia.Monto_Transf);
            objDE.AddInParameter(objComando, "@saldo_transf", DbType.Double, _BETransferencia.Saldo_Transf);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BETransferencia.Tipo);
            objDE.AddInParameter(objComando, "@check", DbType.Int32, _BETransferencia.check);
            objDE.AddInParameter(objComando, "@urlConvenio", DbType.String, _BETransferencia.urlConvenio);
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, _BETransferencia.id_solicitudes);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _BETransferencia.Id_Usuario);
            objDE.AddInParameter(objComando, "@monto_ReservaContingencia", DbType.Decimal, _BETransferencia.monto_ReservaContingencia);

            objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }
    }

    public Int64 spiuSOL_Transferencia(BETransferencia _BETransferencia)
    {
        try
        {
            Int64 intResultado = 0;

            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_Transferencia]");
            objDE.AddInParameter(objComando, "@id_transf", DbType.Int64, _BETransferencia.Id_Transf);
            objDE.AddInParameter(objComando, "@nro_ds", DbType.String, _BETransferencia.Nro_Ds);

           // objDE.AddInParameter(objComando, "@monto_transf", DbType.Double, _BETransferencia.Monto_Transf);
            //objDE.AddInParameter(objComando, "@saldo_transf", DbType.Double, _BETransferencia.Saldo_Transf);
            
            //objDE.AddInParameter(objComando, "@monto_transf1", DbType.Double, _BETransferencia.Monto_Transf1);
            //objDE.AddInParameter(objComando, "@saldo_transf1", DbType.Double, _BETransferencia.Saldo_Transf1);

            objDE.AddInParameter(objComando, "@correos", DbType.String, _BETransferencia.Correos);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BETransferencia.Id_Usuario);
            objDE.AddInParameter(objComando, "@subsector", DbType.Int32, _BETransferencia.Id_Subsector);

            objDE.AddInParameter(objComando, "@nro_oficio", DbType.String, _BETransferencia.nro_oficio);
            objDE.AddInParameter(objComando, "@oficioNro", DbType.String, _BETransferencia.oficioNro);
            objDE.AddInParameter(objComando, "@oficioAnio", DbType.String, _BETransferencia.oficioAnio);

            objDE.AddInParameter(objComando, "@fechaOficio", DbType.String, _BETransferencia.fechaOficio);
            intResultado = (Int64)objDE.ExecuteScalar(objComando);

            return intResultado;
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }
    }

    public Int64 spiuSOL_DetTransferencia(BETransferencia _BETransferencia)
    {
        try
        {
            Int64 intResultado = 0;

            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_DetTransferencia]");
            objDE.AddInParameter(objComando, "@id_det_transf", DbType.Int64, _BETransferencia.Id_Det_Transf);
            objDE.AddInParameter(objComando, "@id_transf", DbType.Int64, _BETransferencia.Id_Transf);
            objDE.AddInParameter(objComando, "@total_inversion", DbType.Double, _BETransferencia.Total_Inversion);
           // objDE.AddInParameter(objComando, "@porcentaje", DbType.Double, _BETransferencia.Porcentaje);
            objDE.AddInParameter(objComando, "@monto_transf", DbType.Double, _BETransferencia.Monto_Transf);
            objDE.AddInParameter(objComando, "@saldo_transf", DbType.Double, _BETransferencia.Saldo_Transf);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BETransferencia.Cod_Estado);
            objDE.AddInParameter(objComando, "@snip", DbType.Int32, _BETransferencia.Snip);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BETransferencia.Tipo);
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BETransferencia.id_solicitudes);
            objDE.AddInParameter(objComando, "@urlconvenio", DbType.String, _BETransferencia.urlConvenio);
            objDE.AddInParameter(objComando, "@monto_ReservaContingencia", DbType.Decimal, _BETransferencia.ReservaContingencia);
            //  objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BETransferencia.Id_Usuario);
            intResultado = (Int64)objDE.ExecuteScalar(objComando);

            return intResultado;
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }
    }

    public int spuSOL_Convenio_DetTransferencia(BETransferencia _BETransferencia)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_Convenio_DetTransferencia]");

            objDE.AddInParameter(objComando, "@id_det_transf", DbType.Int64, _BETransferencia.Id_Det_Transf);
            objDE.AddInParameter(objComando, "@urlconvenio", DbType.String, _BETransferencia.urlConvenio);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    #endregion

}

