﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DE_MON_Liquidacion
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public int spi_MON_Liquidacion(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Liquidacion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_LIQUIDACION.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@resolucion", DbType.String, _BE_LIQUIDACION.resolucion);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_LIQUIDACION.Date_Fecha);
                objDE.AddInParameter(objComando, "@montoLiq", DbType.String, _BE_LIQUIDACION.monto);
                objDE.AddInParameter(objComando, "@saldoDevolver", DbType.String, _BE_LIQUIDACION.saldoDevolver);
                objDE.AddInParameter(objComando, "@urlLiquidacion", DbType.String, _BE_LIQUIDACION.urlLiquidacion);
                objDE.AddInParameter(objComando, "@urlActaCierre", DbType.String, _BE_LIQUIDACION.urlActaCierre);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_LIQUIDACION.id_usuario);

                //objDE.AddInParameter(objComando, "@flag", DbType.String, _BE_LIQUIDACION.flagCapacidad);
                //objDE.AddInParameter(objComando, "@comentario", DbType.String, _BE_LIQUIDACION.comentarioCapacidad);
                //objDE.AddInParameter(objComando, "@UnidadReceptora", DbType.String, _BE_LIQUIDACION.unidadReceptora);

                //objDE.AddInParameter(objComando, "@flagEducacion", DbType.String, _BE_LIQUIDACION.flagEduacion);
                //objDE.AddInParameter(objComando, "@flagIntradomiciliaria", DbType.String, _BE_LIQUIDACION.flagIntraconexion);
                //objDE.AddInParameter(objComando, "@urlDocSostenibilidad", DbType.String, _BE_LIQUIDACION.urlDocSostenibilidad);
                //objDE.AddInParameter(objComando, "@id_tipoReceptora", DbType.String, _BE_LIQUIDACION.tipoReceptora);

                objDE.AddInParameter(objComando, "@montoAdicional", DbType.String, _BE_LIQUIDACION.montoAdicional);
                objDE.AddInParameter(objComando, "@otrosMontos", DbType.String, _BE_LIQUIDACION.otrosMontos);
                objDE.AddInParameter(objComando, "@urlDocDevolucion", DbType.String, _BE_LIQUIDACION.urlDocDevolucion);

                objDE.AddInParameter(objComando, "@resolucionSupervision", DbType.String, _BE_LIQUIDACION.ResolucionSupervision);
                objDE.AddInParameter(objComando, "@fechaLiqSupervision", DbType.DateTime, _BE_LIQUIDACION.Date_fechaLiqSupervision);
                objDE.AddInParameter(objComando, "@montoLiqSupervision", DbType.String, _BE_LIQUIDACION.MontoLiqSupervision);
                objDE.AddInParameter(objComando, "@resolucionObraComplementaria", DbType.String, _BE_LIQUIDACION.ResolucionObraComplementaria);
                objDE.AddInParameter(objComando, "@fechaLiqObraComplem", DbType.DateTime, _BE_LIQUIDACION.Date_fechaLiqObraComplem);
                objDE.AddInParameter(objComando, "@montoLiqObraComplementaria", DbType.String, _BE_LIQUIDACION.MontoLiqObraComplementaria);

                objDE.AddInParameter(objComando, "@urlLiquidacionObraComplem", DbType.String, _BE_LIQUIDACION.UrlLiquidacionObraComplem);
                objDE.AddInParameter(objComando, "@urlLiquidacionSupervision", DbType.String, _BE_LIQUIDACION.UrlLiquidacionSupervision);

                objDE.AddInParameter(objComando, "@urlDeclaracionViabilidad", DbType.String, _BE_LIQUIDACION.urlDeclaracionViabilidad);
                objDE.AddInParameter(objComando, "@urlAprobacionExpedienteTecnico", DbType.String, _BE_LIQUIDACION.urlAprobacionExpediente);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet sp_MON_Liquidacion(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Liquidacion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_LIQUIDACION.tipoFinanciamiento);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_Listado_Proyecto(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Listado_Proyecto]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_LIQUIDACION.tipoFinanciamiento);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_Ayuda_Memoria(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Ayuda_Memoria]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_LIQUIDACION.tipoFinanciamiento);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet paSSP_rFichaTecnica(string pSNIP)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[cobel].[paSSP_rFichaTecnica]");
                objDE.AddInParameter(objComando, "@pSNIP", DbType.String, pSNIP);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_Ayuda_MemoriaNE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Ayuda_MemoriaNE]");
                objDE.AddInParameter(objComando, "@ID_PROYECTO", DbType.Int32, _BE_LIQUIDACION.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_Ayuda_MemoriaPE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Ayuda_MemoriaPE]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    
        public List<BE_MON_BANDEJA> F_spMON_TipoReceptora()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoReceptora";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int IU_spiu_MON_CierreProyecto_NE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_CierreProyecto_NE]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_LIQUIDACION.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@iTipoFormato", DbType.Int32, _BE_LIQUIDACION.idTipoFormato);
                objDE.AddInParameter(objComando, "@fechaFormato14", DbType.DateTime, _BE_LIQUIDACION.Date_Fecha);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_LIQUIDACION.urlDocumento);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_LIQUIDACION.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int IU_spiu_MON_LiquidacionNE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_LiquidacionNE]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_LIQUIDACION.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_LIQUIDACION.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@fechaDocumentacionComplementaria", DbType.DateTime, _BE_LIQUIDACION.Date_fechaDocumentacion);
                objDE.AddInParameter(objComando, "@fechaFirmaLiquidadorPrograma", DbType.DateTime, _BE_LIQUIDACION.Date_fechaFirma);
                objDE.AddInParameter(objComando, "@saldoDevolver", DbType.String, _BE_LIQUIDACION.saldoDevolver);
                objDE.AddInParameter(objComando, "@urlDocDevolucion", DbType.String, _BE_LIQUIDACION.urlDocDevolucion);
                objDE.AddInParameter(objComando, "@resolucion", DbType.String, _BE_LIQUIDACION.resolucion);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_LIQUIDACION.Date_Fecha);
                objDE.AddInParameter(objComando, "@montoLiq", DbType.String, _BE_LIQUIDACION.monto);
                objDE.AddInParameter(objComando, "@montoTransferencia", DbType.String, _BE_LIQUIDACION.montoTransferencia);
                objDE.AddInParameter(objComando, "@urlLiquidacion", DbType.String, _BE_LIQUIDACION.urlLiquidacion);
                objDE.AddInParameter(objComando, "@urlActaCierre", DbType.String, _BE_LIQUIDACION.urlActaCierre);

                objDE.AddInParameter(objComando, "@fechaSuscripcionTerminacionObra", DbType.DateTime, _BE_LIQUIDACION.Date_fechaSuscripcionTerminacionObra);
                objDE.AddInParameter(objComando, "@fechaTerminoObraInicioLiquidacion", DbType.DateTime, _BE_LIQUIDACION.Date_fechaTerminoObraInicioLiquidacion);
                objDE.AddInParameter(objComando, "@fechaSuscripcion", DbType.DateTime, _BE_LIQUIDACION.Date_fechaSuscripcion);
                objDE.AddInParameter(objComando, "@nroResolucionAprobacion", DbType.String, _BE_LIQUIDACION.resolucionAprobacion);
                objDE.AddInParameter(objComando, "@fechaBajaContable", DbType.DateTime, _BE_LIQUIDACION.Date_fechaBajaContable);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_LIQUIDACION.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_MON_LiquidacionPE(BE_MON_Liquidacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_LiquidacionPE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@fechaRecepcion", SqlDbType.DateTime).Value = _BE.Date_fechaDocumentacion;
                    cmd.Parameters.Add("@fechaCertificadoCumplimiento", SqlDbType.DateTime).Value = _BE.Date_fechaCumplimiento;
                    cmd.Parameters.Add("@fechaActaProvisional", SqlDbType.DateTime).Value = _BE.Date_fechaProvisional;
                    cmd.Parameters.Add("@fechaActaFinal", SqlDbType.DateTime).Value = _BE.Date_fechaFinal;

                    cmd.Parameters.Add("@resolucion", SqlDbType.VarChar, 200).Value = _BE.resolucion;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_Fecha;
                    cmd.Parameters.Add("@montoLiq", SqlDbType.VarChar, 50).Value = _BE.monto;
                    cmd.Parameters.Add("@urlLiquidacion", SqlDbType.VarChar, 200).Value = _BE.urlLiquidacion;
                    cmd.Parameters.Add("@saldoFavorEntidad", SqlDbType.VarChar, 50).Value = _BE.saldoEntidad;
                    cmd.Parameters.Add("@saldoFavorEmpresa", SqlDbType.VarChar, 50).Value = _BE.saldoEmpresa;

                    cmd.Parameters.Add("@id_tipoEstadoSaldoEntidad", SqlDbType.Int).Value = _BE.id_tipoEstadoSaldoEntidad;
                    cmd.Parameters.Add("@id_tipoEstadoSaldoEmpresa", SqlDbType.Int).Value = _BE.id_tipoEstadoSaldoEmpresa;
                    cmd.Parameters.Add("@id_tipoEstadoSaldoEntidadSupervision", SqlDbType.Int).Value = _BE.id_tipoEstadoSaldoEntidadSupervision;
                    cmd.Parameters.Add("@id_tipoEstadoSaldoEmpresaSupervision", SqlDbType.Int).Value = _BE.id_tipoEstadoSaldoEmpresaSupervision;
                     
                    cmd.Parameters.Add("@resolucionSupervision", SqlDbType.VarChar, 200).Value = _BE.ResolucionSupervision;
                    cmd.Parameters.Add("@fechaLiqSupervision", SqlDbType.DateTime).Value = _BE.Date_fechaLiqSupervision;
                    cmd.Parameters.Add("@montoLiqSupervision", SqlDbType.VarChar, 50).Value = _BE.MontoLiqSupervision;
                    cmd.Parameters.Add("@urlLiquidacionSupervision", SqlDbType.VarChar, 200).Value = _BE.UrlLiquidacionSupervision;
                    cmd.Parameters.Add("@saldoFavorEntidadSupervision", SqlDbType.VarChar, 50).Value = _BE.saldoEntidadSupervision;
                    cmd.Parameters.Add("@saldoFavorEmpresaSupervision", SqlDbType.VarChar, 50).Value = _BE.saldoEmpresaSupervision;

                    cmd.Parameters.Add("@comentarioSupervision", SqlDbType.VarChar, 500).Value = _BE.comentarioCapacidad;
                    cmd.Parameters.Add("@UnidadReceptora", SqlDbType.VarChar, 200).Value = _BE.unidadReceptora;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
     
        public int IU_MON_RecepcionObra_PorContrata(BE_MON_Liquidacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_RecepcionObraPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@fechaCulminacionObraCuadernoObra", SqlDbType.DateTime).Value = _BE.Date_fechaCumplimiento;
                    cmd.Parameters.Add("@fechaComunicacionEntidad", SqlDbType.DateTime).Value = _BE.Date_fechaDocumentacion;
                    cmd.Parameters.Add("@fechaDesignacionComite", SqlDbType.DateTime).Value = _BE.Date_fechaSuscripcion;
                    cmd.Parameters.Add("@fechaActaRecepcionFinal", SqlDbType.DateTime).Value = _BE.Date_fechaFinal;
                    cmd.Parameters.Add("@flagObservacionRecepcion", SqlDbType.Int).Value = _BE.flagCapacidad;
                    cmd.Parameters.Add("@fechaObservacionRecepcion", SqlDbType.DateTime).Value = _BE.Date_fechaRecepcion;

                    cmd.Parameters.Add("@urlCuadernoObra", SqlDbType.VarChar, 200).Value = _BE.urlDocCuadernoObra;
                    cmd.Parameters.Add("@urlDocumentoComunicacionEntidad", SqlDbType.VarChar, 200).Value = _BE.urlDocComunicacion;
                    cmd.Parameters.Add("@urlDocumentoDesignacionComite", SqlDbType.VarChar, 200).Value = _BE.urlDocDesignacion;
                    cmd.Parameters.Add("@urlActaRecepcionFinal", SqlDbType.VarChar, 200).Value = _BE.urlActaCierre;
                    cmd.Parameters.Add("@urlActaObservacionRecepcion", SqlDbType.VarChar, 200).Value = _BE.urlDocDevolucion;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_MON_SostenibilidadPorContrata(BE_MON_Liquidacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_SostenibilidadPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@fechaActaTransferenciaObra", SqlDbType.DateTime).Value = _BE.Date_Fecha;
                    cmd.Parameters.Add("@id_tipoReceptora", SqlDbType.Int).Value = _BE.tipoReceptora;
                    cmd.Parameters.Add("@unidadReceptora", SqlDbType.VarChar, 200).Value = _BE.unidadReceptora;

                    cmd.Parameters.Add("@flag", SqlDbType.VarChar, 200).Value = _BE.flagCapacidad;
                    cmd.Parameters.Add("@iFlagOperativo", SqlDbType.Int).Value = _BE.flagOperatividad;
                    
                    cmd.Parameters.Add("@comentario", SqlDbType.VarChar, 2000).Value = _BE.comentarioCapacidad;
                    cmd.Parameters.Add("@urlDocSostenibilidad", SqlDbType.VarChar, 200).Value = _BE.urlDocSostenibilidad;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public int IU_MON_LiquidacionPorContrata(BE_MON_Liquidacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_LiquidacionPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@fechaPresentacionLiquidacion", SqlDbType.DateTime).Value = _BE.Date_fechaPresentacion;
                    cmd.Parameters.Add("@fechaAprobacionLiquidacion", SqlDbType.DateTime).Value = _BE.Date_fechaAprobacion;
                    cmd.Parameters.Add("@resolucion", SqlDbType.VarChar, 200).Value = _BE.resolucion;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_Fecha;
                    cmd.Parameters.Add("@montoLiq", SqlDbType.VarChar, 50).Value = _BE.monto;
                    cmd.Parameters.Add("@urlLiquidacion", SqlDbType.VarChar, 200).Value = _BE.urlLiquidacion;
                    cmd.Parameters.Add("@saldoFavorEntidad", SqlDbType.VarChar, 50).Value = _BE.saldoEntidad;
                    cmd.Parameters.Add("@saldoFavorEmpresa", SqlDbType.VarChar, 50).Value = _BE.saldoEmpresa;

                    cmd.Parameters.Add("@fechaPresentacionLiqSupervision", SqlDbType.DateTime).Value = _BE.Date_fechaPresentacionSupervision;
                    cmd.Parameters.Add("@fechaAprobacionLiqSupervision", SqlDbType.DateTime).Value = _BE.Date_fechaAprobacionSupervision;
                    cmd.Parameters.Add("@resolucionSupervision", SqlDbType.VarChar, 200).Value = _BE.ResolucionSupervision;
                    cmd.Parameters.Add("@fechaLiqSupervision", SqlDbType.DateTime).Value = _BE.Date_fechaLiqSupervision;
                    cmd.Parameters.Add("@montoLiqSupervision", SqlDbType.VarChar, 50).Value = _BE.MontoLiqSupervision;
                    cmd.Parameters.Add("@urlLiquidacionSupervision", SqlDbType.VarChar, 200).Value = _BE.UrlLiquidacionSupervision;
                    cmd.Parameters.Add("@saldoFavorEntidadSupervision", SqlDbType.VarChar, 50).Value = _BE.saldoEntidadSupervision;
                    cmd.Parameters.Add("@saldoFavorEmpresaSupervision", SqlDbType.VarChar, 50).Value = _BE.saldoEmpresaSupervision;
                    
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Liquidacion> F_WEB_LiquidadosNE(BE_MON_BANDEJA _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Liquidacion objENT = new BE_MON_Liquidacion();
            List<BE_MON_Liquidacion> objCollection = new List<BE_MON_Liquidacion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "paWEB_LiquidadosNE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_programa", SqlDbType.VarChar,2).Value = _BE.sector;
                cmd.Parameters.Add("@departamento", SqlDbType.VarChar, 2).Value = _BE.depa;
                cmd.Parameters.Add("@provincia", SqlDbType.VarChar, 2).Value = _BE.prov;
                cmd.Parameters.Add("@distrito", SqlDbType.VarChar, 2).Value = _BE.dist;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Liquidacion();

                        objENT.comentarioCapacidad = (Reader["nom_proyecto"].ToString());
                        objENT.resolucion = (Reader["resolucion"].ToString());
                        objENT.monto = (Reader["montoLiquidacion"].ToString());
                        objENT.urlLiquidacion = (Reader["urlLiquidacion"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Financiamiento> F_WEB_EstadosNE(BE_MON_BANDEJA _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "paWEB_EstadosNE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_programa", SqlDbType.VarChar, 2).Value = _BE.sector;
                cmd.Parameters.Add("@departamento", SqlDbType.VarChar, 2).Value = _BE.depa;
                cmd.Parameters.Add("@provincia", SqlDbType.VarChar, 2).Value = _BE.prov;
                cmd.Parameters.Add("@distrito", SqlDbType.VarChar, 2).Value = _BE.dist;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Financiamiento();

                        objENT.Comentario = (Reader["Estado"].ToString());
                        objENT.AnioFiscal = (Reader["total2014"].ToString());
                        objENT.Banco = (Reader["total2015"].ToString());
                        objENT.Cadena = (Reader["total2016"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Financiamiento> F_MontoInversionNE(BE_MON_BANDEJA _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Financiamiento objENT = new BE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = new List<BE_MON_Financiamiento>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "paWEB_MontoInversionNE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_programa", SqlDbType.VarChar, 2).Value = _BE.sector;
                cmd.Parameters.Add("@departamento", SqlDbType.VarChar, 2).Value = _BE.depa;
                cmd.Parameters.Add("@provincia", SqlDbType.VarChar, 2).Value = _BE.prov;
                cmd.Parameters.Add("@distrito", SqlDbType.VarChar, 2).Value = _BE.dist;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Financiamiento();

                        objENT.Comentario = (Reader["TipoMonto"].ToString());
                        objENT.AnioFiscal = (Reader["total2014"].ToString());
                        objENT.Banco = (Reader["total2015"].ToString());
                        objENT.Cadena = (Reader["total2016"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public DataSet spMON_FichaPriorizados(string pIdProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spMON_FichaPriorizados]");
                objDE.AddInParameter(objComando, "@IdProyecto", DbType.Int32, pIdProyecto);
                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }



}
