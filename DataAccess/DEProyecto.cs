using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Newtonsoft.Json;
using Entity;
using DataAccess;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


public class DEProyecto
{


    private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

    #region Consulta
    /// <summary>
    /// camilo sp_inserta_solicitud_ubigeo
    /// </summary>
    /// <param name="idsolicitud"></param>
    /// <param name="ubigeo"></param>
    /// <returns></returns>
    public int sp_inserta_solicitud_ubigeo(long idsolicitud, string ubigeo, bool estado)
    //carga banco de proyectos
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("sp_inserta_solicitud_ubigeo");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, idsolicitud);
            objDE.AddInParameter(objComando, "@UBIGEO", DbType.String, ubigeo);
            objDE.AddInParameter(objComando, "@estado", DbType.Boolean, estado);
            return Convert.ToInt32(objDE.ExecuteScalar(objComando));

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoProyectos(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoProyectos]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEProyecto.Id_usuario);
            objDE.AddInParameter(objComando, "@cod_tipo_usuario", DbType.Int32, _BEProyecto.Cod_tipo_usuario);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _BEProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_subsector_filtro", DbType.Int32, _BEProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_programa", DbType.Int32, _BEProyecto.Cod_programa);
            objDE.AddInParameter(objComando, "@cod_programa_filtro", DbType.Int32, _BEProyecto.Cod_programa_filtro);
            objDE.AddInParameter(objComando, "@cod_prioridad", DbType.Int32, _BEProyecto.Cod_prioridad);
            objDE.AddInParameter(objComando, "@cod_estrategia", DbType.Int32, _BEProyecto.Cod_estrategia);
            objDE.AddInParameter(objComando, "@cod_clasificacion", DbType.Int32, _BEProyecto.Cod_clasificacion);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _BEProyecto.Periodo);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BEProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@intervencion", DbType.Int32, _BEProyecto.Intervencion);
            objDE.AddInParameter(objComando, "@foto", DbType.Int32, _BEProyecto.Foto);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _BEProyecto.Cod_Sector);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_SubEstado", DbType.Int32, _BEProyecto.Cod_SubEstado);
            objDE.AddInParameter(objComando, "@cod_perfil", DbType.Int32, _BEProyecto.Cod_perfil_usuario);
            objDE.AddInParameter(objComando, "@nro_ds", DbType.String, _BEProyecto.DS);
            objDE.AddInParameter(objComando, "@inauguracion", DbType.String, _BEProyecto.Inauguracion);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    //public DataSet sp_listadoProyectosSISEM(BEProyecto _BEProyecto)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[listadoProyectos]");
    //        objDE.AddInParameter(objComando, "strCriterio", DbType.String, _BEProyecto.Criterio);
    //        objDE.AddInParameter(objComando, "strBuscar", DbType.String, _BEProyecto.Buscar);
    //        if (_BEProyecto.Periodo > 0)
    //            objDE.AddInParameter(objComando, "periodo", DbType.Int32, _BEProyecto.Periodo);

    //        objDE.AddInParameter(objComando, "cod_depa", DbType.String, _BEProyecto.Cod_depa);
    //        objDE.AddInParameter(objComando, "cod_estado", DbType.Int32, _BEProyecto.Cod_estado);
    //        return objDE.ExecuteDataSet(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }

    //}

    public DataSet paSSP_SOL_rProyectoIntegral(int snip)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_SOL_rProyectoIntegral]");
            objDE.AddInParameter(objComando, "pSnip", DbType.Int32, snip);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    #endregion

    #region Transaccion

    public int spiu_Proyectos(BEProyecto _BEProyecto)
    {
        try
        {
            int intResultado = 0;

            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiu_datosProyecto_SEDAPAL]");
            objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BEProyecto.Id_proyecto);
            objDE.AddInParameter(objComando, "@cprog", DbType.Int32, _BEProyecto.Cod_programa);
            objDE.AddInParameter(objComando, "@cpri", DbType.String, _BEProyecto.Cod_prioridad);
            objDE.AddInParameter(objComando, "@cestra", DbType.Int32, _BEProyecto.Cod_estrategia);
            objDE.AddInParameter(objComando, "@csest", DbType.Int32, _BEProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@nompr", DbType.String, _BEProyecto.Nombre);
            objDE.AddInParameter(objComando, "@perio", DbType.Int32, _BEProyecto.Periodo);
            objDE.AddInParameter(objComando, "@ambito", DbType.String, _BEProyecto.Ambito);
            if (_BEProyecto.Fecha_viabilidad.Length > 6)
                objDE.AddInParameter(objComando, "@fviab", DbType.DateTime, Convert.ToDateTime(_BEProyecto.Fecha_viabilidad));
            objDE.AddInParameter(objComando, "@ctobr", DbType.Int32, _BEProyecto.Tipo_Proy);
            objDE.AddInParameter(objComando, "@csnip", DbType.Int32, _BEProyecto.Cod_snip);
            objDE.AddInParameter(objComando, "@msnip", DbType.Double, _BEProyecto.Monto_snip);
            objDE.AddInParameter(objComando, "@npips", DbType.Int32, _BEProyecto.Nro_pips);
            objDE.AddInParameter(objComando, "@pbdir", DbType.Int32, _BEProyecto.Benef_direc);
            objDE.AddInParameter(objComando, "@pbind", DbType.Int32, _BEProyecto.Benef_indir);
            objDE.AddInParameter(objComando, "@minvi2006", DbType.Double, _BEProyecto.Monto_inv_2006);
            objDE.AddInParameter(objComando, "@minvi2007", DbType.Double, _BEProyecto.Monto_inv_2007);
            objDE.AddInParameter(objComando, "@minvi2008", DbType.Double, _BEProyecto.Monto_inv_2008);
            objDE.AddInParameter(objComando, "@minvi2009", DbType.Double, _BEProyecto.Monto_inv_2009);

            objDE.AddInParameter(objComando, "@cdep", DbType.String, _BEProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cprov", DbType.String, _BEProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cdist", DbType.String, _BEProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@obpry", DbType.String, _BEProyecto.Observacion);
            objDE.AddInParameter(objComando, "@nom_usu", DbType.String, _BEProyecto.Usuario);
            objDE.AddInParameter(objComando, "@n_empleo", DbType.Int32, _BEProyecto.Empleos);
            objDE.AddInParameter(objComando, "@id", DbType.Int32, _BEProyecto.Id_WS);
            objDE.AddInParameter(objComando, "@interven", DbType.Int32, _BEProyecto.Intervencion);
            objDE.AddInParameter(objComando, "@csubprog", DbType.Int32, _BEProyecto.Cod_subprograma);
            objDE.AddInParameter(objComando, "@cclasif", DbType.Int32, _BEProyecto.Cod_clasificacion);

            intResultado = Convert.ToInt32(objDE.ExecuteDataSet(objComando).Tables[0].Rows[0][0]);

            if (_BEProyecto.Monto_ejec_2006 != 0)
                sp_agregarCalendario(0, intResultado, 2, 4, 3, 2006, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _BEProyecto.Monto_ejec_2006, "supergen", 1);
            if (_BEProyecto.Monto_ejec_2007 != 0)
                sp_agregarCalendario(0, intResultado, 2, 4, 3, 2007, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _BEProyecto.Monto_ejec_2007, "supergen", 1);
            if (_BEProyecto.Monto_ejec_2008 != 0)
                sp_agregarCalendario(0, intResultado, 2, 4, 3, 2008, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _BEProyecto.Monto_ejec_2008, "supergen", 1);
            if (_BEProyecto.Monto_ejec_2009 != 0)
                sp_agregarCalendario(0, intResultado, 2, 4, 3, 2009, 0, 0, 0, 0, 0, 0, 0, 0, 0, _BEProyecto.Monto_ejec_2009, 0, 0, 0, "supergen", 1);

            return intResultado;
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }
    }

    public void sp_agregarCalendario(int id_calendario, int id_proyecto, int cod_tipoCal, int cod_tipoMonto, int cod_fase,
    int anio, int id_unidad, double enero, double febrero, double marzo, double abril, double mayo, double junio,
    double julio, double agosto, double septiembre, double octubre, double noviembre, double diciembre, string usuario, int estado)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiu_Calendario]");
            //DbCommand objComando = objDE.GetStoredProcCommand("[spiu_Calendario_Sedapal]");
            objDE.AddInParameter(objComando, "id_detcalendario", DbType.Int32, id_calendario);
            objDE.AddInParameter(objComando, "id_proyecto", DbType.Int32, id_proyecto);
            objDE.AddInParameter(objComando, "cod_tipoCal", DbType.Int32, cod_tipoCal);
            objDE.AddInParameter(objComando, "cod_tipoMonto", DbType.Int32, cod_tipoMonto);
            objDE.AddInParameter(objComando, "cod_fase", DbType.Int32, cod_fase);
            objDE.AddInParameter(objComando, "anio", DbType.Int32, anio);
            objDE.AddInParameter(objComando, "id_unidad", DbType.Int32, id_unidad);
            objDE.AddInParameter(objComando, "enero", DbType.Double, enero);
            objDE.AddInParameter(objComando, "febrero", DbType.Double, febrero);
            objDE.AddInParameter(objComando, "marzo", DbType.Double, marzo);
            objDE.AddInParameter(objComando, "abril", DbType.Double, abril);
            objDE.AddInParameter(objComando, "mayo", DbType.Double, mayo);
            objDE.AddInParameter(objComando, "junio", DbType.Double, junio);
            objDE.AddInParameter(objComando, "julio", DbType.Double, julio);
            objDE.AddInParameter(objComando, "agosto", DbType.Double, agosto);
            objDE.AddInParameter(objComando, "septiembre", DbType.Double, septiembre);
            objDE.AddInParameter(objComando, "octubre", DbType.Double, octubre);
            objDE.AddInParameter(objComando, "noviembre", DbType.Double, noviembre);
            objDE.AddInParameter(objComando, "diciembre", DbType.Double, diciembre);
            objDE.AddInParameter(objComando, "usuario", DbType.String, usuario);
            objDE.AddInParameter(objComando, "estado", DbType.Int32, estado);
            objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    //public void spiu_ProcesoSeleccion(BEProceso _BEProceso)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spiu_ProcesoSeleccion]");

    //        objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BEProceso.Id_proyecto);
    //        objDE.AddInParameter(objComando, "@contra_bpro", DbType.String, _BEProceso.Contratista);
    //        objDE.AddInParameter(objComando, "@situacion_actual_bpro", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@problema_bpro", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@obs_bpro", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_sac", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_pro", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_obs", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@nom_usu", DbType.String, _BEProceso.Usuario);
    //        objDE.AddInParameter(objComando, "@ruc", DbType.Int32, _BEProceso.RUC);
    //        objDE.AddInParameter(objComando, "@nro_lici", DbType.String, _BEProceso.NroLicitacion);
    //        objDE.ExecuteNonQuery(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}

    //public void spiu_DatosExpediente(BEExpediente _BEExpediente)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spiu_DatosExpediente]");

    //        objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BEExpediente.Id_proyecto);
    //        if (_BEExpediente.FechaIni_Expe.Length > 0)
    //            objDE.AddInParameter(objComando, "@fecha_ini_exp_tec", DbType.DateTime, Convert.ToDateTime(_BEExpediente.FechaIni_Expe));
    //        if (_BEExpediente.FechaFin_Expe.Length > 0)
    //            objDE.AddInParameter(objComando, "@fecha_fin_exp_tec", DbType.DateTime, Convert.ToDateTime(_BEExpediente.FechaFin_Expe));
    //        objDE.AddInParameter(objComando, "@plazo_ejec_exp_tec", DbType.Double, _BEExpediente.Periodo_ejec_Expe);
    //        objDE.AddInParameter(objComando, "@avance_exp_tec", DbType.Int32, _BEExpediente.Avance_Expe);
    //        objDE.AddInParameter(objComando, "@situacion_actual_exp", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@problema_exp", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@obs_exp", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_sac", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_pro", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_obs", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@nom_usu", DbType.String, _BEExpediente.Usuario);
    //        objDE.ExecuteNonQuery(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}

    //public void spiu_DatosObra(BEObra _BEObra)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spiu_DatosObra]");
    //        objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BEObra.Id_proyecto);
    //        objDE.AddInParameter(objComando, "@meta_obra", DbType.String, "");
    //        if (_BEObra.FechaIni_obra.Length > 0)
    //            objDE.AddInParameter(objComando, "@fecha_ini_obra", DbType.DateTime, _BEObra.FechaIni_obra);
    //        if (_BEObra.FechaFin_obra.Length > 0)
    //            objDE.AddInParameter(objComando, "@fecha_fin_obra", DbType.DateTime, _BEObra.FechaFin_obra);

    //        objDE.AddInParameter(objComando, "@plazo_ejec_obra", DbType.Double, _BEObra.Periodo_ejec_obra);
    //        objDE.AddInParameter(objComando, "@avance_obra", DbType.Int32, _BEObra.Avance_obra);
    //        objDE.AddInParameter(objComando, "@obra_lista", DbType.String, _BEObra.Obra_lista);
    //        objDE.AddInParameter(objComando, "@situacion_actual_obra", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@problema_obra", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@obs_obra", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_sac", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_pro", DbType.String, "");
    //        objDE.AddInParameter(objComando, "@archivo_obs", DbType.String, "");
    //        if (_BEObra.FechaPrev_inaug.Length > 0)
    //            objDE.AddInParameter(objComando, "@fecha_prev_inaug", DbType.DateTime, _BEObra.FechaPrev_inaug);
    //        if (_BEObra.FechaReal_inaug.Length > 0)
    //            objDE.AddInParameter(objComando, "@fecha_real_inaug", DbType.DateTime, _BEObra.FechaReal_inaug);

    //        objDE.AddInParameter(objComando, "@nom_usu", DbType.String, _BEObra.Usuario);

    //        objDE.ExecuteNonQuery(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}

    #endregion



    #region Concesiones

    public DataSet sp_listadoConcesiones(int tipo_concesion, int id_concesion, int flag_reporte)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoConcesiones]");
            objDE.AddInParameter(objComando, "@tipo_conce", DbType.Int32, tipo_concesion);
            objDE.AddInParameter(objComando, "@id_conce", DbType.Int32, id_concesion);
            objDE.AddInParameter(objComando, "@flag_report", DbType.Int32, flag_reporte);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoAcuerdos(int id_concesion, int tipo_concesion, int etapa)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoAcuerdos]");
            objDE.AddInParameter(objComando, "@id_conce", DbType.Int32, id_concesion);
            objDE.AddInParameter(objComando, "@tipo_conce", DbType.Int32, tipo_concesion);
            objDE.AddInParameter(objComando, "@etapa", DbType.Int32, etapa);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoSituacion(int id_concesion)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoSituacionActual]");
            objDE.AddInParameter(objComando, "@id_conce", DbType.Int32, id_concesion);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoProyectosConcesiones(int id_concesion)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoMegaProyectos]");
            objDE.AddInParameter(objComando, "@id_concesiones", DbType.Int32, id_concesion);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoReprogramacion(int id_acuerdo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoReprogramacion]");
            objDE.AddInParameter(objComando, "@id_acuerdo", DbType.Int32, id_acuerdo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoAudiConcesiones(int id_concesion, int opcion)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoAudiConcesiones]");
            objDE.AddInParameter(objComando, "@id_conce", DbType.Int32, id_concesion);
            objDE.AddInParameter(objComando, "@flag_opcion", DbType.Int32, opcion);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiu_SituacionActual(int id_situacion, int id_concesiones, int semana, string acc_ejec, string acc_pen, string comentario, string fecha, int estado, int usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiu_SituacionActual]");
            objDE.AddInParameter(objComando, "@id_situacion", DbType.Int32, id_situacion);
            objDE.AddInParameter(objComando, "@id_concesiones", DbType.Int32, id_concesiones);
            objDE.AddInParameter(objComando, "@nro_semana", DbType.Int32, semana);
            objDE.AddInParameter(objComando, "@accion_ejecutada", DbType.String, acc_ejec);
            objDE.AddInParameter(objComando, "@accion_pendiente ", DbType.String, acc_pen);
            objDE.AddInParameter(objComando, "@comentario ", DbType.String, comentario);
            objDE.AddInParameter(objComando, "@fecha_ejecutada ", DbType.DateTime, (fecha.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha));
            objDE.AddInParameter(objComando, "@estado", DbType.Int32, estado);
            objDE.AddInParameter(objComando, "@usuario", DbType.Int32, usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public int spiu_Acuerdos(int id_acuerdo, int id_concesiones, int nro, string acuerdo, string fecha_acuerdo, string fecha_fin, int etapa, int estado_vb, int estado, int usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiu_Acuerdos]");
            objDE.AddInParameter(objComando, "@id_acuerdo", DbType.Int32, id_acuerdo);
            objDE.AddInParameter(objComando, "@id_concesiones", DbType.Int32, id_concesiones);
            objDE.AddInParameter(objComando, "@nro", DbType.Int32, nro);
            objDE.AddInParameter(objComando, "@acuerdo", DbType.String, acuerdo);
            objDE.AddInParameter(objComando, "@fecha_acuerdo", DbType.DateTime, (fecha_acuerdo.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha_acuerdo));
            objDE.AddInParameter(objComando, "@fecha_fin", DbType.DateTime, (fecha_fin.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha_fin));
            objDE.AddInParameter(objComando, "@etapa", DbType.Int32, etapa);
            objDE.AddInParameter(objComando, "@estado_vb", DbType.Int32, estado_vb);
            objDE.AddInParameter(objComando, "@estado", DbType.Int32, estado);
            objDE.AddInParameter(objComando, "@usuario", DbType.Int32, usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public int spiu_Reprogramacion(int id_acuerdo, string fecha_antigua, string fecha_nueva, int usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiu_Reprogramacion]");
            objDE.AddInParameter(objComando, "@id_acuerdo", DbType.Int32, id_acuerdo);
            objDE.AddInParameter(objComando, "@fecha_fin_old", DbType.DateTime, (fecha_antigua.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha_antigua));
            objDE.AddInParameter(objComando, "@fecha_fin_new", DbType.DateTime, (fecha_nueva.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha_nueva));
            objDE.AddInParameter(objComando, "@usuario", DbType.Int32, usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    #endregion

    #region Problemas
    public DataSet sp_listadoProblemas(BEProblema _beProblema)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listadoProblemas]");
            objDE.AddInParameter(objComando, "@id_problema", DbType.Int32, _beProblema.Id_problema);
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProblema.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProblema.Buscar);
            objDE.AddInParameter(objComando, "@id_prioridad", DbType.Int32, _beProblema.Id_prioridad);
            objDE.AddInParameter(objComando, "@id_sector", DbType.Int32, _beProblema.Id_Sector);
            objDE.AddInParameter(objComando, "@id_programa", DbType.Int32, _beProblema.Id_Programa);
            objDE.AddInParameter(objComando, "@id_oficina", DbType.Int32, _beProblema.Id_Oficina);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProblema.Cod_depa);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _beProblema.Periodo);
            objDE.AddInParameter(objComando, "@id_estado", DbType.Int32, _beProblema.Id_Estado);
            objDE.AddInParameter(objComando, "@id_tipo_prob", DbType.Int32, _beProblema.Id_Tipo_Prob);
            objDE.AddInParameter(objComando, "@nivel_sol", DbType.Int32, _beProblema.Id_Nivel_Sol);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public int spiu_Problema(BEProblema _beProblema)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiu_Problemas]");
            objDE.AddInParameter(objComando, "@id_problema", DbType.Int32, _beProblema.Id_problema);

            //objDE.AddInParameter(objComando, "@fecha_fin_old", DbType.DateTime, (fecha_antigua.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha_antigua));
            //objDE.AddInParameter(objComando, "@fecha_fin_new", DbType.DateTime, (fecha_nueva.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fecha_nueva));
            //objDE.AddInParameter(objComando, "@usuario", DbType.Int32, usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    #endregion

    #region Solicitud


    public DataSet spSOL_ReporteMinistro()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteMinistro]");
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteMinistroFiltro(string departamento, string snip, string proyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteMinistroFiltro]");
            objDE.AddInParameter(objComando, "@departamento", DbType.String, departamento);
            objDE.AddInParameter(objComando, "@snip", DbType.String, snip);
            objDE.AddInParameter(objComando, "@proyecto", DbType.String, proyecto);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Documentos(BEDocumento _BEDocumento)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Documentos]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEDocumento.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@id_doc", DbType.Int64, _BEDocumento.Id_Doc);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ConsultaPry(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ConsultaPry]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);
            objDE.AddInParameter(objComando, "@id_estado_snip", DbType.Int32, _BEProyecto.IdEstado_Snip);
            objDE.AddInParameter(objComando, "@id_documentacion", DbType.Int32, _BEProyecto.Id_Documentacio);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_subsector ", DbType.Int32, _BEProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _BEProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@id_proceso", DbType.Int32, _BEProyecto.id_Proceso);
            objDE.AddInParameter(objComando, "@estadoProceso", DbType.String, _BEProyecto.estado);
            objDE.AddInParameter(objComando, "@id_ModalidadFinan", DbType.Int32, _BEProyecto.id_ModalidadFinanciamiento);
            objDE.AddInParameter(objComando, "@id_SubModalidadFinan", DbType.Int32, _BEProyecto.id_SubModalidadFinanciamiento);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_BancaPry(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_BancaPry]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);
            objDE.AddInParameter(objComando, "@id_estado_snip", DbType.Int32, _BEProyecto.IdEstado_Snip);
            objDE.AddInParameter(objComando, "@id_documentacion", DbType.Int32, _BEProyecto.Id_Documentacio);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_subsector ", DbType.Int32, _BEProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _BEProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BEProyecto.Tipo_Proy);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public List<BEProyecto> F_spSOL_ListarTipoFinanciamiento()
    {
        using (SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            BEProyecto objENT = new BEProyecto();
            List<BEProyecto> objCollection = new List<BEProyecto>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_ListarTipoFinanciamiento";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int codigo = Reader.GetOrdinal("codigo");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.Codigo = Reader.GetInt32(codigo).ToString();
                        objENT.Nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }
        }
    }

    public int paSSP_SOL_rNumMetasRegistradas(int pId, int pEtapaRegistro, int pTipoMeta)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_SOL_rNumMetasRegistradas]");
            objDE.AddInParameter(objComando, "@pId", DbType.Int64, pId);
            objDE.AddInParameter(objComando, "@pEtapaRegistro", DbType.Int32, pEtapaRegistro);
            objDE.AddInParameter(objComando, "@pTipoMeta", DbType.Int32, pTipoMeta);
            return Convert.ToInt32(objDE.ExecuteScalar(objComando));

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public DataSet spSOL_Seguimiento_Variable_PNSU(BEVarPnsu _BEVarPnsu)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Variable_PNSU]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEVarPnsu.Id_Solicitudes);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Seguimiento_Variable_PMIB(BEVarPimb _BEVarPimb)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Variable_PMIB]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEVarPimb.Id_Solicitudes);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet paSSP_SOL_rMetasAyudaMemoria(int pId, int pEtapaRegistro, int pTipoMeta)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_SOL_rMetasAyudaMemoria]");
            objDE.AddInParameter(objComando, "@pId", DbType.Int64, pId);
            objDE.AddInParameter(objComando, "@pEtapaRegistro", DbType.Int32, pEtapaRegistro);
            objDE.AddInParameter(objComando, "@pTipoMeta", DbType.Int32, pTipoMeta);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Seguimiento_Historial(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Historial]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BEProyecto.Tipo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet spSOL_Plantilla(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Plantilla]");
            objDE.AddInParameter(objComando, "@id_plantilla", DbType.Int64, _BECheckList.Id_Plantilla);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet spSOL_Seguimiento_Informe(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_Informe]");
            objDE.AddInParameter(objComando, "@id_informe", DbType.Int64, _BECheckList.Id_Informe);
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BECheckList.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BECheckList.Cod_Estado);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sp_listadoSolicitudes(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_listarSolicitudes]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@id_programa", DbType.Int32, _beProyecto.Cod_clasificacion);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _beProyecto.Periodo);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sp_listadoSolicitudes_SSP(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_tipo", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_fase", DbType.Int32, _beProyecto.Cod_clasificacion);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _beProyecto.Periodo);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_listadoSolicitudes_SSP1(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _beProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_tipo", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _beProyecto.Periodo);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _beProyecto.Id_usuario);
            objDE.AddInParameter(objComando, "@cod_tipo_usuario", DbType.Int32, _beProyecto.Cod_tipo_usuario);
            objDE.AddInParameter(objComando, "@porcentaje", DbType.Int32, _beProyecto.Porcentaje);
            objDE.AddInParameter(objComando, "@tipo_usuario", DbType.Int32, _beProyecto.Cod_tipo_usuario);
            objDE.AddInParameter(objComando, "@tipo_tranf", DbType.Int32, _beProyecto.TipoTransf);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _beProyecto.Tipo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Apto_Transferencia(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Apto_Transferencia]");

            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_tipo", DbType.Int32, _beProyecto.Cod_subsector);

            objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _beProyecto.Tipo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    //public DataSet spSOL_listadoProyEjecucion(BEProyecto _beProyecto)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoProyEjecucion]");
    //        objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
    //        objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
    //        objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_subsector);
    //        objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
    //        objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
    //        objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
    //        objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
    //        objDE.AddInParameter(objComando, "@cod_subestado", DbType.Int32, _beProyecto.Cod_SubEstado);
    //        objDE.AddInParameter(objComando, "@porcentaje", DbType.Int32, _beProyecto.Porcentaje);
    //        objDE.AddInParameter(objComando, "@tipo_tranf", DbType.Int32, _beProyecto.TipoTransf);
    //        return objDE.ExecuteDataSet(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}

    public DataSet spSOL_TransferenciaContinuidad(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_TransferenciaContinuidad]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);


            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_NewAsignacion(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_NewAsignacion]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@idtecnico", DbType.Int32, _beProyecto.IdTecnico);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sp_solicitudesAsignadas(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_SolicitudesNoAsignadas]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _beProyecto.Periodo);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@idtecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@cod_estado_asig", DbType.Int32, _beProyecto.Cod_EstadoTecnico);
            objDE.AddInParameter(objComando, "@crecer", DbType.String, _beProyecto.crecer);
            objDE.AddInParameter(objComando, "@sisfoh", DbType.String, _beProyecto.sisfoh);
            objDE.AddInParameter(objComando, "@fonie", DbType.String, _beProyecto.fonie);
            objDE.AddInParameter(objComando, "@vrae", DbType.String, _beProyecto.vrae);
            objDE.AddInParameter(objComando, "@vahpp", DbType.String, _beProyecto.vahpp);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_BaseNegativa_UE(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[BN_SP_UNIDAD_EJECUTORA]");
            objDE.AddInParameter(objComando, "@UNIDAD_EJEC", DbType.String, _beProyecto.Observacion);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet sp_BaseNegativa_DetalleEmpresas(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[BN_SP_DETALLE_EMPRESAS]");
            objDE.AddInParameter(objComando, "@ID", DbType.String, _beProyecto.Observacion);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sp_Listado_Asignacion_Expediente(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_Listado_Asignacion_Expediente]");
            //objDE.AddInParameter(objComando, "@ID", DbType.String, _beProyecto.Observacion);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiuSOL_IngresarVariablePnsu(BEVarPnsu _BEVarPnsu)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_IngresarVariablePnsu]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEVarPnsu.Id_Solicitudes);

            objDE.AddInParameter(objComando, "@a_captacion_nuc", DbType.Int32, _BEVarPnsu.A_captacion_nuc);
            objDE.AddInParameter(objComando, "@a_captacion_cacl", DbType.Double, _BEVarPnsu.A_captacion_cacl);

            objDE.AddInParameter(objComando, "@a_impulsion_num", DbType.Int32, _BEVarPnsu.A_impulsion_num);
            objDE.AddInParameter(objComando, "@a_impulsion_cap", DbType.Double, _BEVarPnsu.A_impulsion_cap);
            objDE.AddInParameter(objComando, "@a_estacion_num", DbType.Int32, _BEVarPnsu.A_estacion_num);

            objDE.AddInParameter(objComando, "@a_trataPotable_num", DbType.Int32, _BEVarPnsu.A_tratoPotable_num);
            objDE.AddInParameter(objComando, "@a_trataPotable_cap", DbType.Double, _BEVarPnsu.A_tratoPotable_cap);

            objDE.AddInParameter(objComando, "@a_conduccion_nuc", DbType.Int32, _BEVarPnsu.A_conduccion_nuc);
            objDE.AddInParameter(objComando, "@a_conduccion_cacd", DbType.Double, _BEVarPnsu.A_conduccion_cacd);

            objDE.AddInParameter(objComando, "@a_reservorio_nur", DbType.Int32, _BEVarPnsu.A_reservorio_nur);
            objDE.AddInParameter(objComando, "@a_reservorio_carm", DbType.Double, _BEVarPnsu.A_reservorio_carm);

            objDE.AddInParameter(objComando, "@a_aduccion_nua", DbType.Int32, _BEVarPnsu.A_aduccion_nua);
            objDE.AddInParameter(objComando, "@a_aduccion_caad", DbType.Double, _BEVarPnsu.A_aduccion_caad);

            objDE.AddInParameter(objComando, "@a_redes_nur", DbType.Int32, _BEVarPnsu.A_redes_nur);
            objDE.AddInParameter(objComando, "@a_redes_card", DbType.Double, _BEVarPnsu.A_redes_card);

            objDE.AddInParameter(objComando, "@a_conexion_nucn", DbType.Int32, _BEVarPnsu.A_conexion_nucn);
            objDE.AddInParameter(objComando, "@a_conexion_nucr", DbType.Int32, _BEVarPnsu.A_conexion_nucr);
            objDE.AddInParameter(objComando, "@a_conexion_nucp", DbType.Int32, _BEVarPnsu.A_conexion_nucp);

            objDE.AddInParameter(objComando, "@s_colectores_num", DbType.Int32, _BEVarPnsu.S_colectores_num);
            objDE.AddInParameter(objComando, "@s_colectores_cap", DbType.Double, _BEVarPnsu.S_colectores_cap);

            objDE.AddInParameter(objComando, "@s_agua_nua", DbType.Double, _BEVarPnsu.S_agua_nua);
            objDE.AddInParameter(objComando, "@s_agua_caad", DbType.Double, _BEVarPnsu.S_agua_caad);


            objDE.AddInParameter(objComando, "@s_planta_nup", DbType.Int32, _BEVarPnsu.S_planta_nup);
            objDE.AddInParameter(objComando, "@s_planta_capl", DbType.Double, _BEVarPnsu.S_planta_capl);

            objDE.AddInParameter(objComando, "@s_conexion_nucn", DbType.Int32, _BEVarPnsu.S_conexion_nucn);
            objDE.AddInParameter(objComando, "@s_conexion_nucr", DbType.Int32, _BEVarPnsu.S_conexion_nucr);
            objDE.AddInParameter(objComando, "@s_conexion_nuclo", DbType.Int32, _BEVarPnsu.S_conexion_nuclo);

            objDE.AddInParameter(objComando, "@s_camara_num", DbType.Double, _BEVarPnsu.S_camara_num);
            objDE.AddInParameter(objComando, "@s_camara_cap", DbType.Double, _BEVarPnsu.S_camara_cap);

            objDE.AddInParameter(objComando, "@s_efluente_num", DbType.Int32, _BEVarPnsu.S_efluente_num);

            objDE.AddInParameter(objComando, "@s_efluente_cap", DbType.Double, _BEVarPnsu.S_efluente_cap);
            objDE.AddInParameter(objComando, "@s_impulsion_num", DbType.Int32, _BEVarPnsu.S_impulsion_num);
            objDE.AddInParameter(objComando, "@s_impulsion_cap", DbType.Double, _BEVarPnsu.S_impulsion_cap);

            objDE.AddInParameter(objComando, "@d_tuberia_num", DbType.Int32, _BEVarPnsu.D_tuberia_num);
            objDE.AddInParameter(objComando, "@d_tuberia_cap", DbType.Double, _BEVarPnsu.D_tuberia_cap);


            objDE.AddInParameter(objComando, "@d_cuneta_num", DbType.Int32, _BEVarPnsu.D_cuneta_num);
            objDE.AddInParameter(objComando, "@d_cuneta_cap", DbType.Double, _BEVarPnsu.D_cuneta_cap);

            objDE.AddInParameter(objComando, "@d_tormenta_num", DbType.Int32, _BEVarPnsu.D_tormenta_num);
            objDE.AddInParameter(objComando, "@d_conexion_num", DbType.Int32, _BEVarPnsu.D_conexion_num);
            objDE.AddInParameter(objComando, "@d_inspeccion_num", DbType.Int32, _BEVarPnsu.D_inspeccion_num);


            objDE.AddInParameter(objComando, "@d_secundarios_num", DbType.Int32, _BEVarPnsu.D_secundario_num);
            objDE.AddInParameter(objComando, "@d_secundarios_cap", DbType.Double, _BEVarPnsu.D_secundario_cap);

            objDE.AddInParameter(objComando, "@d_principal_num", DbType.Int32, _BEVarPnsu.D_principal_num);
            objDE.AddInParameter(objComando, "@d_principal_cap", DbType.Double, _BEVarPnsu.D_principal_cap);

            objDE.AddInParameter(objComando, "@a_captacion_costo", DbType.Double, _BEVarPnsu.A_captacion_costo);
            objDE.AddInParameter(objComando, "@a_impulsion_costo", DbType.Double, _BEVarPnsu.A_impulsion_costo);
            objDE.AddInParameter(objComando, "@a_estacion_costo", DbType.Double, _BEVarPnsu.A_estacion_costo);
            objDE.AddInParameter(objComando, "@a_trataPotable_costo", DbType.Double, _BEVarPnsu.A_trataPotable_costo);
            objDE.AddInParameter(objComando, "@a_conduccion_costo", DbType.Double, _BEVarPnsu.A_conduccion_costo);
            objDE.AddInParameter(objComando, "@a_reservorio_costo", DbType.Double, _BEVarPnsu.A_reservorio_costo);
            objDE.AddInParameter(objComando, "@a_aduccion_costo", DbType.Double, _BEVarPnsu.A_aduccion_costo);
            objDE.AddInParameter(objComando, "@a_redes_costo", DbType.Double, _BEVarPnsu.A_redes_costo);
            objDE.AddInParameter(objComando, "@a_conexion_nucnCosto", DbType.Double, _BEVarPnsu.A_conexion_nucnCosto);
            objDE.AddInParameter(objComando, "@a_conexion_nucrCosto", DbType.Double, _BEVarPnsu.A_conexion_nucrCosto);
            objDE.AddInParameter(objComando, "@a_conexion_nucpCosto", DbType.Double, _BEVarPnsu.A_conexion_nucpCosto);

            objDE.AddInParameter(objComando, "@s_colectores_costo", DbType.Double, _BEVarPnsu.S_colectores_costo);
            objDE.AddInParameter(objComando, "@s_agua_costo", DbType.Double, _BEVarPnsu.S_agua_costo);
            objDE.AddInParameter(objComando, "@s_planta_costo", DbType.Double, _BEVarPnsu.S_planta_costo);
            objDE.AddInParameter(objComando, "@s_conexion_nucnCosto", DbType.Double, _BEVarPnsu.S_conexion_nucnCosto);
            objDE.AddInParameter(objComando, "@s_conexion_nucrCosto", DbType.Double, _BEVarPnsu.S_conexion_nucrCosto);
            objDE.AddInParameter(objComando, "@s_conexion_nucloCosto", DbType.Double, _BEVarPnsu.S_conexion_nucloCosto);
            objDE.AddInParameter(objComando, "@s_camara_costo", DbType.Double, _BEVarPnsu.S_camara_costo);
            objDE.AddInParameter(objComando, "@s_efluente_costo", DbType.Double, _BEVarPnsu.S_efluente_costo);
            objDE.AddInParameter(objComando, "@s_impulsion_costo", DbType.Double, _BEVarPnsu.S_impulsion_costo);

            objDE.AddInParameter(objComando, "@d_tuberia_costo", DbType.Double, _BEVarPnsu.D_tuberia_costo);
            objDE.AddInParameter(objComando, "@d_cuneta_costo", DbType.Double, _BEVarPnsu.D_cuneta_costo);
            objDE.AddInParameter(objComando, "@d_tormenta_costo", DbType.Double, _BEVarPnsu.D_tormenta_costo);
            objDE.AddInParameter(objComando, "@d_conexion_costo", DbType.Double, _BEVarPnsu.D_conexion_costo);
            objDE.AddInParameter(objComando, "@d_inspeccion_costo", DbType.Double, _BEVarPnsu.D_inspeccion_costo);
            objDE.AddInParameter(objComando, "@d_secundarios_costo", DbType.Double, _BEVarPnsu.D_secundarios_costo);
            objDE.AddInParameter(objComando, "@d_principal_costo", DbType.Double, _BEVarPnsu.D_principal_costo);

            objDE.AddInParameter(objComando, "@v_cant", DbType.Int32, _BEVarPnsu.V_cant);
            objDE.AddInParameter(objComando, "@v_costo", DbType.Double, _BEVarPnsu.V_costo);


            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiuSOL_IngresarVariablePimb(BEVarPimb _BEVarPimb)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_IngresarVariablePimb]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEVarPimb.Id_Solicitudes);

            objDE.AddInParameter(objComando, "@p_concreto_m2", DbType.Double, _BEVarPimb.P_concreto_m2);
            objDE.AddInParameter(objComando, "@p_concreto_co", DbType.Double, _BEVarPimb.P_concreto_co);
            objDE.AddInParameter(objComando, "@p_asfalto_m2", DbType.Double, _BEVarPimb.P_asfalto_m2);
            objDE.AddInParameter(objComando, "@p_asfalto_co", DbType.Double, _BEVarPimb.P_asfalto_co);
            objDE.AddInParameter(objComando, "@p_emboquillado_m2", DbType.Double, _BEVarPimb.P_emboquillado_m2);
            objDE.AddInParameter(objComando, "@p_emboquillado_co", DbType.Double, _BEVarPimb.P_emboquillado_co);
            objDE.AddInParameter(objComando, "@p_adoquinado_m2", DbType.Double, _BEVarPimb.P_adoquinado_m2);
            objDE.AddInParameter(objComando, "@p_adoquinado_co", DbType.Double, _BEVarPimb.P_adoquinado_co);

            objDE.AddInParameter(objComando, "@v_concreto_m2", DbType.Double, _BEVarPimb.V_concreto_m2);
            objDE.AddInParameter(objComando, "@v_concreto_co", DbType.Double, _BEVarPimb.V_concreto_co);
            objDE.AddInParameter(objComando, "@v_adoquinado_m2", DbType.Double, _BEVarPimb.V_adoquinado_m2);
            objDE.AddInParameter(objComando, "@v_adoquinado_co", DbType.Double, _BEVarPimb.V_adoquinado_co);
            objDE.AddInParameter(objComando, "@v_piedra_m2", DbType.Double, _BEVarPimb.V_piedra_m2);
            objDE.AddInParameter(objComando, "@v_piedra_co", DbType.Double, _BEVarPimb.V_piedra_co);
            objDE.AddInParameter(objComando, "@v_emboquillado_m2", DbType.Double, _BEVarPimb.V_emboquillado_m2);
            objDE.AddInParameter(objComando, "@v_emboquillado_co", DbType.Double, _BEVarPimb.V_emboquillado_co);

            objDE.AddInParameter(objComando, "@d_concreto_ml", DbType.Double, _BEVarPimb.D_concreto_ml);
            objDE.AddInParameter(objComando, "@d_concreto_co", DbType.Double, _BEVarPimb.D_concreto_co);
            objDE.AddInParameter(objComando, "@d_piedra_ml", DbType.Double, _BEVarPimb.D_piedra_ml);
            objDE.AddInParameter(objComando, "@d_piedra_co", DbType.Double, _BEVarPimb.D_piedra_co);

            objDE.AddInParameter(objComando, "@o_muro_m2", DbType.Double, _BEVarPimb.O_muro_m2);
            objDE.AddInParameter(objComando, "@o_muro_co", DbType.Double, _BEVarPimb.O_muro_co);
            objDE.AddInParameter(objComando, "@o_ponton_m2", DbType.Double, _BEVarPimb.O_ponton_m2);
            objDE.AddInParameter(objComando, "@o_ponton_co", DbType.Double, _BEVarPimb.O_ponton_co);
            objDE.AddInParameter(objComando, "@o_berma_m2", DbType.Double, _BEVarPimb.O_bermas_m2);
            objDE.AddInParameter(objComando, "@o_berma_co", DbType.Double, _BEVarPimb.O_bermas_co);
            objDE.AddInParameter(objComando, "@o_verdes_m2", DbType.Double, _BEVarPimb.O_verdes_m2);
            objDE.AddInParameter(objComando, "@o_verdes_co", DbType.Double, _BEVarPimb.O_verdes_co);

            objDE.AddInParameter(objComando, "@o_jardineras_u", DbType.Int32, _BEVarPimb.O_jardineras_u);
            objDE.AddInParameter(objComando, "@o_jardineras_co", DbType.Double, _BEVarPimb.O_jardineras_co);
            objDE.AddInParameter(objComando, "@o_plantones_u", DbType.Int32, _BEVarPimb.O_plantones_u);
            objDE.AddInParameter(objComando, "@o_plantones_co", DbType.Double, _BEVarPimb.O_plantones_co);
            objDE.AddInParameter(objComando, "@o_sanitarias_u", DbType.Int32, _BEVarPimb.O_sanitarias_u);
            objDE.AddInParameter(objComando, "@o_sanitarias_co", DbType.Double, _BEVarPimb.O_sanitarias_co);
            objDE.AddInParameter(objComando, "@o_electricas_u", DbType.Int32, _BEVarPimb.O_electricas_u);
            objDE.AddInParameter(objComando, "@o_electricas_co", DbType.Double, _BEVarPimb.O_electricas_co);

            objDE.AddInParameter(objComando, "@o_bancas_u", DbType.Int32, _BEVarPimb.O_bancas_u);
            objDE.AddInParameter(objComando, "@o_bancas_co", DbType.Double, _BEVarPimb.O_bancas_co);
            objDE.AddInParameter(objComando, "@o_basureros_u", DbType.Int32, _BEVarPimb.O_basureros_u);
            objDE.AddInParameter(objComando, "@o_basureros_co", DbType.Double, _BEVarPimb.O_basureros_co);
            objDE.AddInParameter(objComando, "@o_pergolas_u", DbType.Int32, _BEVarPimb.O_pergolas_u);
            objDE.AddInParameter(objComando, "@o_pergolas_co", DbType.Double, _BEVarPimb.O_pergolas_co);
            objDE.AddInParameter(objComando, "@o_glorietas_u", DbType.Int32, _BEVarPimb.O_glorietas_u);
            objDE.AddInParameter(objComando, "@o_glorietas_co", DbType.Double, _BEVarPimb.O_glorietas_co);
            objDE.AddInParameter(objComando, "@o_otros_u", DbType.Double, _BEVarPimb.O_otros_u);
            objDE.AddInParameter(objComando, "@o_otros_co", DbType.Double, _BEVarPimb.O_otros_co);

            objDE.AddInParameter(objComando, "@o_puente_m2", DbType.Double, _BEVarPimb.o_puente_m2);
            objDE.AddInParameter(objComando, "@o_puente_co", DbType.Double, _BEVarPimb.o_puente_co);

            objDE.AddInParameter(objComando, "@l_losa_m2", DbType.Double, _BEVarPimb.l_losa_m2);
            objDE.AddInParameter(objComando, "@l_losa_co", DbType.Double, _BEVarPimb.l_losa_co);
            objDE.AddInParameter(objComando, "@l_tribuna_m2", DbType.Double, _BEVarPimb.l_tribuna_m2);
            objDE.AddInParameter(objComando, "@l_tribuna_co", DbType.Double, _BEVarPimb.l_tribuna_co);
            objDE.AddInParameter(objComando, "@l_administracion_m2", DbType.Double, _BEVarPimb.l_administracion_m2);
            objDE.AddInParameter(objComando, "@l_administracion_co", DbType.Double, _BEVarPimb.l_administracion_co);
            objDE.AddInParameter(objComando, "@l_cerco_ml", DbType.Double, _BEVarPimb.l_cerco_ml);
            objDE.AddInParameter(objComando, "@l_cerco_co", DbType.Double, _BEVarPimb.l_cerco_co);
            objDE.AddInParameter(objComando, "@l_iluminacion_u", DbType.Double, _BEVarPimb.l_iluminacion_u);
            objDE.AddInParameter(objComando, "@l_iluminacion_co", DbType.Double, _BEVarPimb.l_iluminacion_co);
            objDE.AddInParameter(objComando, "@c_puestas_m2", DbType.Double, _BEVarPimb.c_puestas_m2);
            objDE.AddInParameter(objComando, "@c_puestas_co", DbType.Double, _BEVarPimb.c_puestas_co);
            objDE.AddInParameter(objComando, "@c_administracion_m2", DbType.Double, _BEVarPimb.c_administracion_m2);
            objDE.AddInParameter(objComando, "@c_administracion_co", DbType.Double, _BEVarPimb.c_administracion_co);
            objDE.AddInParameter(objComando, "@c_cerco_ml", DbType.Double, _BEVarPimb.c_cerco_ml);
            objDE.AddInParameter(objComando, "@c_cerco_co", DbType.Double, _BEVarPimb.c_cerco_co);


            objDE.AddInParameter(objComando, "@observaciones", DbType.String, _BEVarPimb.Observaciones);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEVarPimb.id_usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiu_AsignarSolicitud(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_AsignarSolicitud]");
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _beProyecto.Nro_pips);
            objDE.AddInParameter(objComando, "@lstIdProyectos", DbType.String, _beProyecto.Observacion);
            objDE.AddInParameter(objComando, "@nom_usu", DbType.String, _beProyecto.Usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public int spiu_QuitarSolicitud(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_QuitarSolicitudes]");
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _beProyecto.Nro_pips);
            objDE.AddInParameter(objComando, "@lstIdProyectos", DbType.String, _beProyecto.Observacion);
            objDE.AddInParameter(objComando, "@nom_usu", DbType.String, _beProyecto.Usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spuSOL_Actualizar_Estados(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_Actualizar_Estados]");
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@prioridad", DbType.Int32, _beProyecto.IntPrioridad);
            objDE.AddInParameter(objComando, "@codigo", DbType.Int64, _beProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_coordinador", DbType.Int32, _beProyecto.IdCoordinador);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@estado_asig", DbType.Int32, _beProyecto.Cod_EstadoTecnico);
            objDE.AddInParameter(objComando, "@user", DbType.Int32, _beProyecto.USER);
            objDE.AddInParameter(objComando, "@motivo", DbType.String, _beProyecto.MOTIVO);
            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            //return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiuSOL_CheckList_PMIB(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_CheckList_PMIB]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BECheckList.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@ordcampo", DbType.Int32, _BECheckList.OrdCampo);
            objDE.AddInParameter(objComando, "@valor", DbType.String, _BECheckList.Valor);
            objDE.AddInParameter(objComando, "@tiene", DbType.String, _BECheckList.Tiene);
            objDE.AddInParameter(objComando, "@observaciones", DbType.String, _BECheckList.Observaciones);
            objDE.AddInParameter(objComando, "@fecha_insert", DbType.DateTime, _BECheckList.Fecha_Insert);
            objDE.AddInParameter(objComando, "@fecha_update", DbType.DateTime, _BECheckList.Fecha_Update);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BECheckList.Cod_Estado);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BECheckList.Id_Usuario);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _BECheckList.CodSubsector);
            objDE.AddInParameter(objComando, "@nocorresponde", DbType.String, _BECheckList.NoCorrespode);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Seguimiento_ChecklistPMIB(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_ChecklistPMIB]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BECheckList.Id_Solicitudes);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_Seguimiento_ChecklistPMIB_Hist_Observaciones(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_Seguimiento_ChecklistPMIB_Hist_Observaciones]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BECheckList.Id_Solicitudes);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiuSOL_CheckList_PNSU(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_CheckList_PNSU]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BECheckList.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@ordcampo", DbType.Int32, _BECheckList.OrdCampo);
            objDE.AddInParameter(objComando, "@valor", DbType.String, _BECheckList.Valor);
            objDE.AddInParameter(objComando, "@observaciones", DbType.String, _BECheckList.Observaciones);
            objDE.AddInParameter(objComando, "@tiene", DbType.String, _BECheckList.Tiene);
            objDE.AddInParameter(objComando, "@expediente", DbType.String, _BECheckList.Expediente);
            objDE.AddInParameter(objComando, "@firma", DbType.String, _BECheckList.Firma);
            objDE.AddInParameter(objComando, "@fecha_insert", DbType.DateTime, _BECheckList.Fecha_Insert);
            objDE.AddInParameter(objComando, "@fecha_update", DbType.DateTime, _BECheckList.Fecha_Update);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BECheckList.Cod_Estado);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BECheckList.Id_Usuario);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _BECheckList.CodSubsector);
            objDE.AddInParameter(objComando, "@nocorresponde", DbType.String, _BECheckList.NoCorrespode);
            objDE.AddInParameter(objComando, "@fechaInicioEvaluacion", DbType.DateTime, _BECheckList.fechaInicioEvaluacion);
            objDE.AddInParameter(objComando, "@fechaFinEvaluacion", DbType.DateTime, _BECheckList.fechaFinEvaluacion);

            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiuSOL_Ingresar_Informe(BECheckList _BECheckList)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_Ingresar_Informe]");
            objDE.AddInParameter(objComando, "@id_informe", DbType.Int64, _BECheckList.Id_Informe);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _BECheckList.Cod_Estado);
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BECheckList.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BECheckList.Descripcion);
            objDE.AddInParameter(objComando, "@informe", DbType.String, _BECheckList.Informe);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _BECheckList.Id_Usuario);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spuSOL_ActualizarMontos(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_ActualizarMontos]");
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@costo_obra", DbType.Double, _beProyecto.Monto_Obra);
            objDE.AddInParameter(objComando, "@costo_supervision", DbType.Double, _beProyecto.Monto_Supervision);
            objDE.AddInParameter(objComando, "@poblacion", DbType.String, _beProyecto.PoblacionPromedio);
            objDE.AddInParameter(objComando, "@monto_inversion", DbType.Double, _beProyecto.Monto_Inversion);
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _beProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@resp_elaboracion", DbType.String, _beProyecto.Resp_Elaboracion);
            objDE.AddInParameter(objComando, "@resp_supervision", DbType.String, _beProyecto.Resp_Supervision);
            objDE.AddInParameter(objComando, "@fecha_pres_obra", DbType.String, _beProyecto.Fecha_Pres_Obra);
            objDE.AddInParameter(objComando, "@obra_total", DbType.Double, _beProyecto.Obra_Total);
            objDE.AddInParameter(objComando, "@supervision_total", DbType.Double, _beProyecto.Supervision_Total);
            objDE.AddInParameter(objComando, "@flagFonie", DbType.String, _beProyecto.FlagFonie);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiSOL_Historial_Solicitudes(Int64 id_solicitudes, int tipo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiSOL_Historial_Solicitudes]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, id_solicitudes);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiSOL_IngresarBanco(BEProyecto _beProyecto)
    //carga banco de proyectos
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiSOL_IngresarBanco]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _beProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_Sector);
            objDE.AddInParameter(objComando, "@snip", DbType.Int32, _beProyecto.Cod_snip);
            objDE.AddInParameter(objComando, "@unificado", DbType.Int32, _beProyecto.CodUnificado);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@unidad_ejec", DbType.String, _beProyecto.Entidad_ejec);
            objDE.AddInParameter(objComando, "@nombre_proyecto", DbType.String, _beProyecto.Nombre);
            objDE.AddInParameter(objComando, "@monto_snip", DbType.Double, _beProyecto.Monto_snip);
            objDE.AddInParameter(objComando, "@documentacion", DbType.String, _beProyecto.Documentacion);
            objDE.AddInParameter(objComando, "@id_documentacion", DbType.Int32, _beProyecto.Id_Documentacio);
            objDE.AddInParameter(objComando, "@id_estado_snip", DbType.Int32, _beProyecto.IdEstado_Snip);
            objDE.AddInParameter(objComando, "@estado_snip", DbType.String, _beProyecto.Estado_Snip);
            objDE.AddInParameter(objComando, "@aprovacion", DbType.String, _beProyecto.Fecha_viabilidad);
            objDE.AddInParameter(objComando, "@id_ambito", DbType.Int32, _beProyecto.Id_Ambito);
            objDE.AddInParameter(objComando, "@poblacion", DbType.Double, _beProyecto.poblacion);
            objDE.AddInParameter(objComando, "@id_financiamiento", DbType.Int32, _beProyecto.IDFINANCIMIENTO);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.String, _beProyecto.Id_usuario);
            objDE.AddInParameter(objComando, "@observacionModificacion", DbType.String, _beProyecto.Observacion);
            objDE.AddInParameter(objComando, "@idTipoPMIB", DbType.Int32, _beProyecto.idTipoProyecto);
            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteResumenFONIE()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteResumenFONIE]");

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteDetalleFONIE(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteDetalleFONIE]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            // objDE.AddInParameter(objComando, "@flagRevision", DbType.Int32, _beProyecto.FlagRevisorFonie);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteBandejaRevision(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteBandejaRevision]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@lista49", DbType.Int32, _beProyecto.Chek49);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteBandejaRevisionPNSU(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteBandejaRevisionPNSU]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@lista49", DbType.Int32, _beProyecto.Chek49);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@flagCondicionado", DbType.Int32, _beProyecto.flagCondicionado);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet spSOL_ReporteBandejaRevisionPMIB(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteBandejaRevisionPMIB]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            // objDE.AddInParameter(objComando, "@lista49", DbType.Int32, _beProyecto.Chek49);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteComponentePNSU(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteComponentePNSU]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            // objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@flagCondicionado", DbType.Int32, _beProyecto.flagCondicionado);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteBandejaRevisionAccionesDiarias(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteBandejaRevisionAccionesDiarias]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@lista49", DbType.Int32, _beProyecto.Chek49);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _beProyecto.Date_Fecha);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet spSOL_listadoSolicitudesAntiguos(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudesAntiguos]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _beProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@cod_tipo", DbType.Int32, _beProyecto.Cod_subsector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            //objDE.AddInParameter(objComando, "@periodo", DbType.Int32, 0); // default 0
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _beProyecto.Id_usuario);
            //objDE.AddInParameter(objComando, "@cod_tipo_usuario", DbType.Int32, _beProyecto.Cod_tipo_usuario);
            //objDE.AddInParameter(objComando, "@porcentaje", DbType.Int32, _beProyecto.Porcentaje);
            objDE.AddInParameter(objComando, "@tipo_usuario", DbType.Int32, _beProyecto.Cod_tipo_usuario);
            //objDE.AddInParameter(objComando, "@tipo_tranf", DbType.Int32, 0); //default 0
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);
            objDE.AddInParameter(objComando, "@lista", DbType.String, _beProyecto.Lista);
            //objDE.AddInParameter(objComando, "@lista49", DbType.Int32, _beProyecto.Chek49);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@flagCondicionado", DbType.Int32, _beProyecto.flagCondicionado);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteBandejaRevisionHistorialEvaluaciones(BEProyecto _beProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            // DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoSolicitudes1]");
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteBandejaRevisionHistorialEvaluaciones]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _beProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _beProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.Int32, _beProyecto.Cod_subsector_filtro);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _beProyecto.Tipo);
            objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, _beProyecto.Cod_estado);
            objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _beProyecto.IdTecnico);
            objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _beProyecto.IdEstrategia);

            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
     
    }
    public int spiSOL_PreliminarIngresarBanco(BEProyecto _beProyecto)

    //carga banco de proyectos
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("spiSOL_PreliminarIngresarBanco");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _beProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, _beProyecto.Cod_Sector);
            objDE.AddInParameter(objComando, "@snip", DbType.Int32, _beProyecto.Cod_snip);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _beProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _beProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _beProyecto.Cod_dist);
            objDE.AddInParameter(objComando, "@unidad_ejec", DbType.String, _beProyecto.Entidad_ejec);
            objDE.AddInParameter(objComando, "@nombre_proyecto", DbType.String, _beProyecto.Nombre);
            objDE.AddInParameter(objComando, "@monto_snip", DbType.Double, _beProyecto.Monto_snip);
            objDE.AddInParameter(objComando, "@documentacion", DbType.String, _beProyecto.Documentacion);
            objDE.AddInParameter(objComando, "@id_documentacion", DbType.Int32, _beProyecto.Id_Documentacio);
            objDE.AddInParameter(objComando, "@id_estado_snip", DbType.Int32, _beProyecto.IdEstado_Snip);
            objDE.AddInParameter(objComando, "@estado_snip", DbType.String, _beProyecto.Estado_Snip);
            objDE.AddInParameter(objComando, "@aprovacion", DbType.String, _beProyecto.Fecha_viabilidad);
            objDE.AddInParameter(objComando, "@id_ambito", DbType.Int32, _beProyecto.Id_Ambito);
            objDE.AddInParameter(objComando, "@poblacion", DbType.Double, _beProyecto.poblacion);
            objDE.AddInParameter(objComando, "@id_financiamiento", DbType.Int32, _beProyecto.IDFINANCIMIENTO);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _beProyecto.Id_usuario);
            objDE.AddInParameter(objComando, "@idTipoPMIB", DbType.Int32, _beProyecto.idTipoProyecto);
            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public List<BEProyecto> F_spSOL_TipoDocumento()
    {
        SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        BEProyecto objENT = new BEProyecto();
        List<BEProyecto> objCollection = new List<BEProyecto>();
        SqlCommand cmd = default(SqlCommand);
        try
        {
            cmd = new SqlCommand();
            cmd.Connection = Cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSOL_TipoDocumento";
            cmd.CommandTimeout = 36000;

            cmd.Connection.Open();
            using (SqlDataReader Reader = cmd.ExecuteReader())
            {

                int valor = Reader.GetOrdinal("valor");
                int nombre = Reader.GetOrdinal("nombre");

                while (Reader.Read())
                {
                    objENT = new BEProyecto();

                    objENT.Codigo = Reader.GetInt32(valor).ToString();
                    objENT.Nombre = Reader.GetString(nombre);

                    objCollection.Add(objENT);

                    objENT = null;
                }
            }
            return objCollection;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            cmd = null;
            if (Cnx.State == ConnectionState.Open)
            {
                Cnx.Close();
            }
        }

    }

    public List<BEProyecto> F_spSOL_Lista()
    {
        SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        BEProyecto objENT = new BEProyecto();
        List<BEProyecto> objCollection = new List<BEProyecto>();
        SqlCommand cmd = default(SqlCommand);
        try
        {
            cmd = new SqlCommand();
            cmd.Connection = Cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSOL_Lista";
            cmd.CommandTimeout = 36000;

            cmd.Connection.Open();
            using (SqlDataReader Reader = cmd.ExecuteReader())
            {

                int valor = Reader.GetOrdinal("valor");
                int nombre = Reader.GetOrdinal("nombre");

                while (Reader.Read())
                {
                    objENT = new BEProyecto();

                    objENT.Codigo = Reader.GetInt32(valor).ToString();
                    objENT.Nombre = Reader.GetString(nombre);

                    objCollection.Add(objENT);

                    objENT = null;
                }
            }
            return objCollection;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            cmd = null;
            if (Cnx.State == ConnectionState.Open)
            {
                Cnx.Close();
            }
        }

    }

    public int spiuSOL_IngresarDocumento(BEDocumento _BEDocumento)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_IngresarDocumento]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int64, _BEDocumento.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@id_doc", DbType.Int64, _BEDocumento.Id_Doc);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEDocumento.Id_Usuario);
            objDE.AddInParameter(objComando, "@fecha", DbType.String, _BEDocumento.Fecha);
            objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BEDocumento.Descripcion);
            objDE.AddInParameter(objComando, "@documento", DbType.String, _BEDocumento.Documento);
            objDE.AddInParameter(objComando, "@tipoDoc", DbType.Int32, _BEDocumento.TipoDoc);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spuSOL_Eliminar(Int64 codigo, Int32 tipo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_Eliminar]");
            objDE.AddInParameter(objComando, "@codigo", DbType.Int64, codigo);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    #endregion

    #region Reporte
    //GENERAL
    public DataSet DS_rep_seguimiento(string cod_subsector, string cod_depa, string cod_prov)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[DS_rep_seguimiento]");
            objDE.AddInParameter(objComando, "@codSubsector", DbType.String, cod_subsector);
            objDE.AddInParameter(objComando, "@depa", DbType.String, cod_depa);
            objDE.AddInParameter(objComando, "@prov", DbType.String, cod_prov);
            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sabana_consolidado(string cod_depa, int categ)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sabana_consolidado]");
            objDE.AddInParameter(objComando, "cod_depa", DbType.String, cod_depa);
            objDE.AddInParameter(objComando, "categ", DbType.Int32, categ);
            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Programas(int subsector, int periodo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Programas]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, periodo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteComponentePMIB()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteComponentePMIB]");
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_semanalProy(int programa)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_seg_proyecto]");
            objDE.AddInParameter(objComando, "cod_programa", DbType.Int32, programa);
            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    //PIMBP
    public DataSet rep_Proy_Estados(int subsector, int usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_EstadosUsuario]");
            //DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_Estados]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, usuario);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_EstadosDept(int subsector, int usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_EstadosDept]");
            //DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_Estados]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, usuario);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_EstadosZonales(int subsector, int usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_EstadosZonales]");
            //DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_Estados]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, usuario);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_FechasRecepcion()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_FechadeRecepcion]");
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_FechasRecepcionZonales(int subsector, int id_usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_FechadeRecepcionZonales]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_EstadosSituacional2009()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_Estadosituacional2009]");
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_EstadosSituacionalZonales(int subsector, int id_usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_Estadosituacionalzonales]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet rep_Proy_AvanceExpTecnicoNucleos(int subsector, int id_usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_Proy_exptecnucleos]");
            objDE.AddInParameter(objComando, "@cod_subsector", DbType.Int32, subsector);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    //FORSUR
    public DataSet rep_ListadoProyFORSUR()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[rep_ListadoProyFORSUR]");
            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    #endregion

    #region tambo
    public DataSet spFOES_Reporte_Resumen_Ministro(int tipo, int periodo, int mes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spFOES_Reporte_Resumen_Ministro]");
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, periodo);
            objDE.AddInParameter(objComando, "@mes", DbType.Int32, mes);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet spFOES_Reporte_Avance(BETambo _BETambo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spFOES_Reporte_Avance]");
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BETambo.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BETambo.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BETambo.Cod_dist);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spFOES_Detalle_Porcentaje(BETambo _BETambo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spFOES_Detalle_Porcentaje]");
            objDE.AddInParameter(objComando, "@id_tambo", DbType.Int64, _BETambo.Id_Tambo);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    public DataSet spFoes_Seguimiento_Tambos(BETambo _BETambo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spFoes_Seguimiento_Tambos]");
            objDE.AddInParameter(objComando, "@id_tambo", DbType.Int64, _BETambo.Id_Tambo);
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BETambo.strCriterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BETambo.strBuscar);
            objDE.AddInParameter(objComando, "@id_estado", DbType.Int32, _BETambo.Id_Estado);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _BETambo.Periodo);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BETambo.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BETambo.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BETambo.Cod_dist);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiuFOES_Ingresar_ProyectoTambo(BETambo _BETambo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiuFOES_Ingresar_ProyectoTambo]");
            objDE.AddInParameter(objComando, "@id_tambo", DbType.Int64, _BETambo.Id_Tambo);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BETambo.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BETambo.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BETambo.Cod_dist);
            objDE.AddInParameter(objComando, "@cod_ccpp", DbType.String, _BETambo.Cod_ccpp);
            objDE.AddInParameter(objComando, "@snip", DbType.Int32, _BETambo.Snip);
            objDE.AddInParameter(objComando, "@proceso", DbType.String, _BETambo.Proceso);
            objDE.AddInParameter(objComando, "@fec_firma_contrato", DbType.String, _BETambo.Fec_firma_contrato);
            objDE.AddInParameter(objComando, "@fec_entrega_terreno", DbType.String, _BETambo.Fec_entrega_terreno);
            objDE.AddInParameter(objComando, "@fec_inicio_plazo", DbType.String, _BETambo.Fec_inicio_plazo);
            objDE.AddInParameter(objComando, "@fec_entrega_exp", DbType.String, _BETambo.Fec_entrega_exp);
            objDE.AddInParameter(objComando, "@fec_aprobo_exp", DbType.String, _BETambo.Fec_aprobo_exp);
            objDE.AddInParameter(objComando, "@fec_culm_plazo", DbType.String, _BETambo.Fec_culm_plazo);
            objDE.AddInParameter(objComando, "@fec_liquidacion", DbType.String, _BETambo.Fecha_Liquidacion);
            objDE.AddInParameter(objComando, "@nom_proyecto", DbType.String, _BETambo.Nom_proyecto);
            objDE.AddInParameter(objComando, "@periodo", DbType.Int32, _BETambo.Periodo);
            objDE.AddInParameter(objComando, "@monto_inversion", DbType.Double, _BETambo.Monto_inversion);
            objDE.AddInParameter(objComando, "@monto_snip", DbType.Double, _BETambo.Monto_snip);
            objDE.AddInParameter(objComando, "@fecha_viabilidad", DbType.String, _BETambo.Fecha_viabilidad);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BETambo.Id_usuario);
            objDE.AddInParameter(objComando, "@id_estado", DbType.Int32, _BETambo.Id_Estado);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public int spiFOES_Ingresar_Porcentaje_Foes(BETambo _BETambo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiFOES_Ingresar_Porcentaje_Foes]");
            objDE.AddInParameter(objComando, "@id_tambo", DbType.Int64, _BETambo.Id_Tambo);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BETambo.Id_usuario);
            objDE.AddInParameter(objComando, "@fecha_supervision", DbType.String, _BETambo.Fecha_supervision);
            objDE.AddInParameter(objComando, "@porcentaje", DbType.Int32, _BETambo.Porcentaje);
            return objDE.ExecuteNonQuery(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    #endregion

    public DataSet spSOL_ReporteErrores(string valor, string id_solicitud, string snip, string flaglista)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteErrores]");
            objDE.AddInParameter(objComando, "@valor", DbType.String, valor);
            objDE.AddInParameter(objComando, "@id_soli", DbType.String, id_solicitud);
            objDE.AddInParameter(objComando, "@snip", DbType.String, snip);
            objDE.AddInParameter(objComando, "@flag", DbType.String, flaglista);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet sp_validarSNIP(string valor)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_validarSNIP]");
            objDE.AddInParameter(objComando, "@SNIP", DbType.String, valor);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    //public DataSet spSOL_listadoProyEliminados()
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_listadoProyEliminados]");

    //        return objDE.ExecuteDataSet(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}

    public DataSet spSOL_CheckListFONIE(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_CheckListFONIE]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiSOL_CheckListFONIE(int id_solicitudes, int id_usuario, int id_campo, string valor)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiSOL_CheckListFONIE]");

            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, id_solicitudes);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
            objDE.AddInParameter(objComando, "@id_campo", DbType.Int32, id_campo);
            objDE.AddInParameter(objComando, "@valor", DbType.String, valor);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteFONIE(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteFONIE]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    //public DataSet spSOL_SeguimientoSITRAD(BEProyecto _BEProyecto)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_SeguimientoSITRAD]");
    //        objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
    //        objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);
    //        objDE.AddInParameter(objComando, "@sector", DbType.String, _BEProyecto.Sector);
    //        objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
    //        objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
    //        objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);
    //        objDE.AddInParameter(objComando, "@fecha", DbType.String, _BEProyecto.FechaSITRAD);
    //        return objDE.ExecuteDataSet(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}

    public DataSet spSOL_RequisitoMinimoET_Info(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_RequisitoMinimoET_Info]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_RequisitoMinimoET_CheckList(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_RequisitoMinimoET_CheckList]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public int spiSOL_RequisitoMinimoET_Info(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiSOL_RequisitoMinimoET_Info]");

            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, _BEProyecto.Id_Solicitudes);
            objDE.AddInParameter(objComando, "@totalFolios", DbType.Int32, _BEProyecto.TotalFolios);
            objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BEProyecto.Date_Fecha);
            objDE.AddInParameter(objComando, "@unidadEjecutora", DbType.String, _BEProyecto.Entidad_ejec);
            objDE.AddInParameter(objComando, "@municipalidad", DbType.String, _BEProyecto.Municipalidad);
            objDE.AddInParameter(objComando, "@alcalde", DbType.String, _BEProyecto.Alcalde);
            objDE.AddInParameter(objComando, "@correoAlcalde", DbType.String, _BEProyecto.CorreoAlcalde);
            objDE.AddInParameter(objComando, "@telefonoAlcalde", DbType.String, _BEProyecto.TelefonoAlcalde);
            objDE.AddInParameter(objComando, "@responsable", DbType.String, _BEProyecto.Responsable);
            objDE.AddInParameter(objComando, "@correoResponsable", DbType.String, _BEProyecto.CorreoResponsable);
            objDE.AddInParameter(objComando, "@telefonoResponsable", DbType.String, _BEProyecto.TelefonoResponsable);
            objDE.AddInParameter(objComando, "@costoProyecto", DbType.Double, _BEProyecto.CostoProyecto);
            objDE.AddInParameter(objComando, "@poblacion", DbType.Int32, _BEProyecto.poblacion);
            objDE.AddInParameter(objComando, "@comentario", DbType.String, _BEProyecto.Comentario);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEProyecto.Id_usuario);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public int spiSOL_RequisitoMinimoET_CheckList(int id_solicitudes, int id_usuario, int id_campo, string valor)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiSOL_RequisitoMinimoET_CheckList]");

            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, id_solicitudes);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
            objDE.AddInParameter(objComando, "@id_campo", DbType.Int32, id_campo);
            objDE.AddInParameter(objComando, "@valor", DbType.String, valor);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public DataSet spSOL_ReporteRequisitoMinimoET(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteRequisitoMinimoET]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteDemanda(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteDemanda]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.String, _BEProyecto.Sector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_ReporteDemandaPMIB(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteDemandaPMIB]");
            objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
            objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);
            objDE.AddInParameter(objComando, "@sector", DbType.String, _BEProyecto.Sector);
            objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
            objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
            objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    //public DataSet spMON_ReporteGeneralMINISTRO(BEProyecto _BEProyecto)
    //{
    //    try
    //    {
    //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
    //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteGeneralMINISTRO]");
    //        objDE.AddInParameter(objComando, "@strCriterio", DbType.String, _BEProyecto.Criterio);
    //        objDE.AddInParameter(objComando, "@strBuscar", DbType.String, _BEProyecto.Buscar);

    //        objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BEProyecto.Cod_depa);
    //        objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BEProyecto.Cod_prov);
    //        objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BEProyecto.Cod_dist);

    //        return objDE.ExecuteDataSet(objComando);

    //    }
    //    catch (DbException ex)
    //    {
    //        throw ex;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {

    //    }
    //}


    public int spuSOL_FlagBandeja(int id_solicitudes, string flagNuevo, string flagRevision, int id_usuario)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_FlagBandeja]");

            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, id_solicitudes);
            objDE.AddInParameter(objComando, "@flagNuevo", DbType.String, flagNuevo);
            objDE.AddInParameter(objComando, "@flagRevision", DbType.String, flagRevision);
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet spSOL_obtieneDirectorioUE(string unidadEjecutora, int id_solicitud)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_obtieneDirectorioUE]");
            objDE.AddInParameter(objComando, "@unidadEjecutora", DbType.String, unidadEjecutora);
            objDE.AddInParameter(objComando, "@id_solicitud", DbType.Int32, id_solicitud);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }

    }


    public DataSet spSOL_DirectorioUE()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("spSOL_DirectorioUE");
            // objDE.AddInParameter(objComando, "@unidadEjecutora", DbType.String, unidadEjecutora);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }

    }
    public DataSet spSOL_ObtieneProyecto(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ObtieneProyecto]");
            objDE.AddInParameter(objComando, "@id", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public DataSet spSOL_HistorialSolicitudes(int id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_HistorialSolicitudes]");
            objDE.AddInParameter(objComando, "@id", DbType.String, id_solicitudes);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    #region Proyecto_NEW

    public DataSet spMON_ObtieneProyectos(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ObtieneProyecto]");
            objDE.AddInParameter(objComando, "@id", DbType.Int32, _BEProyecto.Id_proyecto);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public DataSet spSOL_SNIP_Relacionados(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_SNIP_Relacionados]");
            objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BEProyecto.Id_proyecto);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spiSOL_SNIP_Relacionados(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spiSOL_SNIP_Relacionados]");
            objDE.AddInParameter(objComando, "@id_solicitud", DbType.Int32, _BEProyecto.Id_proyecto);
            objDE.AddInParameter(objComando, "@cod_snip", DbType.Int32, _BEProyecto.Cod_snip);

            return objDE.ExecuteNonQuery(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spdSOL_SNIP_Relacionados(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spdSOL_SNIP_Relacionados]");
            objDE.AddInParameter(objComando, "@cod", DbType.String, _BEProyecto.Id_proyecto);


            return objDE.ExecuteNonQuery(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }


    public DataSet spSOL_SNIP_RelacionadosBuscar(BEProyecto _BEProyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_SNIP_RelacionadosBuscar]");
            objDE.AddInParameter(objComando, "@cod_snip", DbType.Int32, _BEProyecto.Cod_snip);
            objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BEProyecto.Tipo);

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    #endregion

    public BEProyecto F_spSOL_AyudaMemoria(BEProyecto _BE)
    {
        BEProyecto objENT = new BEProyecto();
        List<BEProyecto> objCollection = new List<BEProyecto>();
        using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
        {
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_AyudaMemoria";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.Id_Solicitudes;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_snip = Reader.GetOrdinal("snip");
                    int Ordinal_IDprograma = Reader.GetOrdinal("cod_subsector");
                    int Ordinal_nombre_proyecto = Reader.GetOrdinal("nombre_proyecto");
                    int Ordinal_nom_depa = Reader.GetOrdinal("nom_depa");
                    int Ordinal_nom_prov = Reader.GetOrdinal("nom_prov");
                    int Ordinal_nom_dist = Reader.GetOrdinal("nom_dist");
                    int Ordinal_monto_snip = Reader.GetOrdinal("monto_snip");
                    int Ordinal_poblacion = Reader.GetOrdinal("poblacionSNIP");
                    int Ordinal_unidad_ejec = Reader.GetOrdinal("unidad_ejec");
                    int Ordinal_aprobacion_viabilidad = Reader.GetOrdinal("aprobacion_viabilidad");
                    int Ordinal_estado_snip = Reader.GetOrdinal("estado_snip");
                    int Ordinal_documentacion = Reader.GetOrdinal("documentacion");
                    int Ordinal_estado = Reader.GetOrdinal("estado");
                    int Ordinal_codEstado = Reader.GetOrdinal("cod_estado");
                    int Ordinal_funcion = Reader.GetOrdinal("funcion");
                    int Ordinal_programa = Reader.GetOrdinal("programa");
                    int Ordinal_subprograma = Reader.GetOrdinal("subprograma");
                    int Ordinal_sectorUF = Reader.GetOrdinal("sectorUF");
                    int Ordinal_pliegoUF = Reader.GetOrdinal("pliegoUF");
                    int Ordinal_lista = Reader.GetOrdinal("lista");
                    int Ordinal_CostoProyecto = Reader.GetOrdinal("CostoProyecto");
                    int Ordinal_nivelViabilidad = Reader.GetOrdinal("nivelViabilidad");
                    int Ordinal_observacion = Reader.GetOrdinal("observaciones");
                    int Ordinal_fechaRevision = Reader.GetOrdinal("FechaRevision");
                    int Ordinal_DescripcionTecnica = Reader.GetOrdinal("DescripcionTecnica");
                    int Ordinal_estadoPIP = Reader.GetOrdinal("EstadoPIP");
                    int Ordinal_Ambito = Reader.GetOrdinal("Ambito");
                    int Ordinal_strFechaUpdateMEF = Reader.GetOrdinal("FechaMEFUpdate");

                    if (Reader.Read())
                    {
                        objENT = new BEProyecto();
                        objENT.Cod_snip = Reader.GetInt32(Ordinal_snip);
                        objENT.Cod_subsector = Reader.GetInt16(Ordinal_IDprograma);
                        objENT.Nombre = Reader.GetString(Ordinal_nombre_proyecto);
                        objENT.strDepa = Reader.GetString(Ordinal_nom_depa);
                        objENT.strProv = Reader.GetString(Ordinal_nom_prov);
                        objENT.strDist = Reader.GetString(Ordinal_nom_dist);
                        objENT.Monto_snip = Reader.GetDouble(Ordinal_monto_snip);
                        objENT.iPoblacion = Reader.GetInt32(Ordinal_poblacion);
                        objENT.Entidad_ejec = Reader.GetString(Ordinal_unidad_ejec);
                        objENT.Fecha_viabilidad = Reader.GetString(Ordinal_aprobacion_viabilidad);
                        objENT.Estado_Snip = Reader.GetString(Ordinal_estado_snip);
                        objENT.Documentacion = Reader.GetString(Ordinal_documentacion);
                        objENT.estado = Reader.GetString(Ordinal_estado);
                        objENT.Cod_estado = Reader.GetInt32(Ordinal_codEstado);
                        objENT.funcion = Reader.GetString(Ordinal_funcion);
                        objENT.programa = Reader.GetString(Ordinal_programa);
                        objENT.subPrograma = Reader.GetString(Ordinal_subprograma);
                        objENT.sectorUF = Reader.GetString(Ordinal_sectorUF);
                        objENT.pliegoUF = Reader.GetString(Ordinal_pliegoUF);
                        objENT.Lista = Reader.GetString(Ordinal_lista);
                        objENT.CostoProyecto = Reader.GetDouble(Ordinal_CostoProyecto);
                        objENT.nivelViabilidad = Reader.GetString(Ordinal_nivelViabilidad);
                        objENT.Observacion = Reader.GetString(Ordinal_observacion);
                        objENT.strFecha = Reader.GetString(Ordinal_fechaRevision);
                        objENT.descripcion = Reader.GetString(Ordinal_DescripcionTecnica);
                        objENT.StrPrioridad = Reader.GetString(Ordinal_estadoPIP);
                        objENT.Ambito = Reader.GetString(Ordinal_Ambito);
                        objENT.strFechaUpdateMEF = Reader.GetString(Ordinal_strFechaUpdateMEF);
                        //objCollection.Add(objENT);
                        //objENT = null;

                    }

                }
                //   Reader.Close();

                return objENT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }
        }
    }

    public int U_SOL_DirectorioUE(BEProyecto _BE)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("spuSOL_DirectorioUE", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@idUE", SqlDbType.Int).Value = _BE.id;
                cmd.Parameters.Add("@cargo", SqlDbType.VarChar, 255).Value = _BE.cargo;
                cmd.Parameters.Add("@encargado_UE", SqlDbType.VarChar, 255).Value = _BE.Alcalde;
                cmd.Parameters.Add("@direccion", SqlDbType.VarChar, 255).Value = _BE.direccion;
                cmd.Parameters.Add("@codigoPostal", SqlDbType.VarChar, 255).Value = _BE.codigoPostal;
                cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 255).Value = _BE.TelefonoAlcalde;
                cmd.Parameters.Add("@fax", SqlDbType.VarChar, 255).Value = _BE.fax;
                cmd.Parameters.Add("@correo", SqlDbType.VarChar, 255).Value = _BE.CorreoAlcalde;
                cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.Id_usuario;

                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }



    public BEProyecto F_MON_ObtieneModalidad(BEProyecto _BE)
    {
        BEProyecto objENT = new BEProyecto();
        List<BEProyecto> objCollection = new List<BEProyecto>();
        using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
        {
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ObtieneModalidad";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_id_tipoModalidad = Reader.GetOrdinal("id_tipoModalidad");
                    int Ordinal_id_TipoFinanciamiento = Reader.GetOrdinal("id_TipoFinanciamiento");
                    int Ordinal_id_tipoPrograma = Reader.GetOrdinal("id_tipoPrograma");
                    int Ordinal_id_TipoSubModalidadFinanciamiento = Reader.GetOrdinal("id_tipoSubModalidadFinanciamiento");

                    if (Reader.Read())
                    {
                        objENT = new BEProyecto();

                        objENT.idTipoProyecto = Reader.GetInt32(Ordinal_id_tipoModalidad);
                        objENT.IDFINANCIMIENTO = Reader.GetInt32(Ordinal_id_TipoFinanciamiento).ToString();
                        objENT.Cod_programa = Reader.GetInt32(Ordinal_id_tipoPrograma);
                        objENT.id_ModalidadFinanciamiento = Reader.GetInt32(Ordinal_id_TipoSubModalidadFinanciamiento);

                        //objCollection.Add(objENT);
                        //objENT = null;

                    }

                }
                //   Reader.Close();

                return objENT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }
        }
    }

    public int U_MON_UnidadEjecutora(BEProyecto _BE)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("spu_MON_UnidadEjecutora", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id;
                cmd.Parameters.Add("@id_solicitud", SqlDbType.Int).Value = _BE.Id_Solicitudes;
                cmd.Parameters.Add("@UE", SqlDbType.VarChar, 255).Value = _BE.Entidad_ejec;
                //cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.Id_usuario;

                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }

    public int U_MON_MontosMVCS(BEProyecto _BE)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("spu_MON_MontosMVCS", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id;
                cmd.Parameters.Add("@id_solicitud", SqlDbType.Int).Value = _BE.Id_Solicitudes;
                cmd.Parameters.Add("@CostoObra", SqlDbType.VarChar, 255).Value = _BE.Monto_Obra;
                cmd.Parameters.Add("@CostoSupervision", SqlDbType.VarChar, 255).Value = _BE.Monto_Supervision;
                //cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.Id_usuario;

                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }

    public List<BE_MON_BANDEJA> F_SOL_EquivalenciaArchivos()
    {
        //  Este bloque using cierra y libera autom�ticamente los recursos utilizados por "conn"
        using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_EquivalenciaArchivos";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int Ordinal_id_equivalenciaFichaARchivos = Reader.GetOrdinal("id_equivalenciaFichaARchivos");
                    int Ordinal_id_checklist = Reader.GetOrdinal("id_checklist");
                    int Ordinal_id_archivosPreset = Reader.GetOrdinal("id_archivosPreset");
                    int Ordinal_des_campo = Reader.GetOrdinal("des_campo");
                    int Ordinal_iNumeracionPreset = Reader.GetOrdinal("iNumeracionPreset");
                    int Ordinal_vDesCampoPreset = Reader.GetOrdinal("vDesCampoPreset");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.id = Reader.GetInt32(Ordinal_id_equivalenciaFichaARchivos);
                        objENT.id_checklist = Reader.GetInt32(Ordinal_id_checklist);
                        objENT.id_archivo = Reader.GetInt32(Ordinal_id_archivosPreset);
                        objENT.Descripcion = Reader.GetString(Ordinal_des_campo);

                        objENT.vNroPreset = Reader.GetString(Ordinal_iNumeracionPreset);
                        objENT.vDescripcionItemPreset = Reader.GetString(Ordinal_vDesCampoPreset);

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }
        }
    }



    #region Aprobaci�n de expedientes
    public DataSet spSOL_BandejaAprobacion(string Estados, string snip, string proyecto, string tipoPrograma)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_BandejaAprobacion]");
            objDE.AddInParameter(objComando, "@Estados", DbType.String, Estados);
            objDE.AddInParameter(objComando, "@snip", DbType.String, snip);
            objDE.AddInParameter(objComando, "@proyecto", DbType.String, proyecto);
            objDE.AddInParameter(objComando, "@iTipoPrograma", DbType.String, tipoPrograma);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    //public void Aprobar()
    //{
    //    using (SqlConnection connectionSitrad = new SqlConnection(strCadenaConexionSitrad))
    //    {
    //        connectionSitrad.Open();

    //        SqlTransaction transaction;

    //        SqlTransaction transactionSitrad;
    //        transactionSitrad = connectionSitrad.BeginTransaction("SampleTransaction2");
    //        DE_TramiteDocumentario _DE_TramiteDocumentario = new DE_TramiteDocumentario();

    //        try
    //        {
    //            //112 PNSU - EQUIPO DE ESTUDIOS DE INVERSION
    //            //116 PNSU - UNIDAD DE ESTUDIOS

    //            string DESTINATARIO = null;
    //            Int32? IDAREADESTINATARIO = null;
    //            Int32? IDAREAREMITE = null;
    //            string ID_ACCION = null;
    //            string DNIREMITENTE = null;
    //            string EXPEDIENTE = "";

    //            string snip = "";
    //            int Id_Solicitudes = 0;



    //            DNIREMITENTE = spSol_ObtenerValoresDerivacion("JEFEUNIDAD", Id_Solicitudes);
    //            IDAREAREMITE = 116;//116 PNSU - UNIDAD DE ESTUDIOS
    //            DESTINATARIO = spSol_ObtenerValoresDerivacion("JEFEESTUDIOSINVERSION", Id_Solicitudes);
    //            IDAREADESTINATARIO = 112; //112 PNSU - EQUIPO DE ESTUDIOS DE INVERSION
    //            ID_ACCION = "21";//6 Conocimiento


    //            //BE_TramiteDocumentario _BE_TramiteDocumentario = new BE_TramiteDocumentario();
    //            //_BE_TramiteDocumentario.DNIREMITENTE = DNIREMITENTE;
    //            //_BE_TramiteDocumentario.COD_DOCUMENTO = null;
    //            //_BE_TramiteDocumentario.IDAREAREMITE = IDAREAREMITE;
    //            //_BE_TramiteDocumentario.NUMERO_DOCUMENTO = null;
    //            //_BE_TramiteDocumentario.SNIP = snip;
    //            //_BE_TramiteDocumentario.FOLIOS = 1;
    //            //_BE_TramiteDocumentario.DNIDESTINATARIO = DESTINATARIO;
    //            //_BE_TramiteDocumentario.IDAREADESTINATARIO = IDAREADESTINATARIO;
    //            //_BE_TramiteDocumentario.DESTINATARIOEXTERNO = null;
    //            //_BE_TramiteDocumentario.ASUNTO = "Solicitud de Evaluaci�n de Expediente T�cnico de Proyecto con SNIP " + snip;

    //            //_BE_TramiteDocumentario.Nomenclatura = "";
    //            //_BE_TramiteDocumentario.NRO_TRAMITE = EXPEDIENTE;
    //            //_BE_TramiteDocumentario.CONSINDOCUMENTO = "SD";
    //            //_BE_TramiteDocumentario.Reingreso = 0;
    //            //_BE_TramiteDocumentario.REMITENTEEXTERNO = null;
    //            //_BE_TramiteDocumentario.ID_ACCION = ID_ACCION;
    //            //_BE_TramiteDocumentario.OBSERVACIONES = "Movimiento generado por la PRESET. Derivaci�n al Jefe de EEI";

    //            ////validar-----------------------------------------------------------------------------------------
    //            //if (_BE_TramiteDocumentario.NRO_TRAMITE.Trim() == "")
    //            //{
    //            //    throw new Exception("No se le asigno ningun tr�mite al documento");
    //            //}
    //            //if (_BE_TramiteDocumentario.DESTINATARIOEXTERNO == null & _BE_TramiteDocumentario.DNIDESTINATARIO == null)
    //            //{
    //            //    throw new Exception("No tiene un destinatario v�lido");
    //            //}
    //            //----------------------------------------------------------------------------------------------
    //            //recepcionar                        
    //            //BE_TramiteDocumentario _BE_TramiteDocumentarioR = new BE_TramiteDocumentario();
    //            //_BE_TramiteDocumentarioR.NRO_TRAMITE = EXPEDIENTE;
    //            //_BE_TramiteDocumentarioR.IDAREAREMITE = 116;
    //            //_BE_TramiteDocumentarioR.DNIREMITENTE = DNIREMITENTE;
    //            //_BE_TramiteDocumentarioR.ESTADO = 1; //derivado;                                              
    //            //_DE_TramiteDocumentario.sp_ActualizaEstadoVentanillaVirtual(_BE_TramiteDocumentarioR, transactionSitrad); //actualizsa el estado a derivado

    //            //string RETVALDOCUMENTO = _DE_TramiteDocumentario.sp_TramiteVentanillaVirtual(_BE_TramiteDocumentario, transactionSitrad);

    //            //segunda derivacion=========================================================================================

    //            DNIREMITENTE = spSol_ObtenerValoresDerivacion("JEFEESTUDIOSINVERSION", Id_Solicitudes);
    //            IDAREAREMITE = 112;//116 PNSU - UNIDAD DE ESTUDIOS
    //            DESTINATARIO = spSol_ObtenerValoresDerivacion("COORDINADOR", Id_Solicitudes);
    //            IDAREADESTINATARIO = 112; //112 PNSU - EQUIPO DE ESTUDIOS DE INVERSION
    //            ID_ACCION = "21";//6 Conocimiento

    //            //BE_TramiteDocumentario _BE_TramiteDocumentario2 = new BE_TramiteDocumentario();
    //            //_BE_TramiteDocumentario2.DNIREMITENTE = DNIREMITENTE;
    //            //_BE_TramiteDocumentario2.COD_DOCUMENTO = null;
    //            //_BE_TramiteDocumentario2.IDAREAREMITE = IDAREAREMITE;
    //            //_BE_TramiteDocumentario2.NUMERO_DOCUMENTO = null;
    //            //_BE_TramiteDocumentario2.SNIP = snip;
    //            //_BE_TramiteDocumentario2.FOLIOS = 1;
    //            //_BE_TramiteDocumentario2.DNIDESTINATARIO = DESTINATARIO;
    //            //_BE_TramiteDocumentario2.IDAREADESTINATARIO = IDAREADESTINATARIO;
    //            //_BE_TramiteDocumentario2.DESTINATARIOEXTERNO = null;
    //            //_BE_TramiteDocumentario2.ASUNTO = "Solicitud de Evaluaci�n de Expediente T�cnico de Proyecto con SNIP " + snip;

    //            //_BE_TramiteDocumentario2.Nomenclatura = "";
    //            //_BE_TramiteDocumentario2.NRO_TRAMITE = EXPEDIENTE;
    //            //_BE_TramiteDocumentario2.CONSINDOCUMENTO = "SD";
    //            //_BE_TramiteDocumentario2.Reingreso = 0;
    //            //_BE_TramiteDocumentario2.REMITENTEEXTERNO = null;
    //            //_BE_TramiteDocumentario2.ID_ACCION = ID_ACCION;
    //            //_BE_TramiteDocumentario2.OBSERVACIONES = "Movimiento generado por la PRESET. Derivaci�n al Coordinador de EEI";

    //            ////validar-----------------------------------------------------------------------------------------
    //            //if (_BE_TramiteDocumentario2.NRO_TRAMITE.Trim() == "")
    //            //{
    //            //    throw new Exception("No se le asigno ningun tr�mite al documento");
    //            //}
    //            //if (_BE_TramiteDocumentario2.DESTINATARIOEXTERNO == null & _BE_TramiteDocumentario2.DNIDESTINATARIO == null)
    //            //{
    //            //    throw new Exception("No tiene un destinatario v�lido");
    //            //}
    //            //----------------------------------------------------------------------------------------------
    //            //recepcionar                        
    //            //BE_TramiteDocumentario _BE_TramiteDocumentarioR2 = new BE_TramiteDocumentario();
    //            //_BE_TramiteDocumentarioR2.NRO_TRAMITE = EXPEDIENTE;
    //            //_BE_TramiteDocumentarioR2.IDAREAREMITE = 116;
    //            //_BE_TramiteDocumentarioR2.DNIREMITENTE = DNIREMITENTE;
    //            //_BE_TramiteDocumentarioR2.ESTADO = 1; //derivado;                                              
    //            //_DE_TramiteDocumentario.sp_ActualizaEstadoVentanillaVirtual(_BE_TramiteDocumentarioR2, transactionSitrad); //actualizsa el estado a derivado

    //            //string RETVALDOCUMENTO2 = _DE_TramiteDocumentario.sp_TramiteVentanillaVirtual(_BE_TramiteDocumentario2, transactionSitrad);

    //            transactionSitrad.Commit();
    //        }
    //        catch (Exception ex)
    //        {
    //            transactionSitrad.Rollback();
    //            throw new Exception(ex.Message);
    //        }

    //    }
    //}


 
   
    public string spSol_ObtenerValoresDerivacion(string Tipo, Int64 id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSol_ObtenerValoresDerivacion]");
            objDE.AddInParameter(objComando, "@Tipo ", DbType.String, Tipo);
            objDE.AddInParameter(objComando, "@id_solicitudes ", DbType.Int64, id_solicitudes);
            return objDE.ExecuteScalar(objComando).ToString();

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string spSol_ObtenerExpediente_Sitrad(Int64 id_solicitudes)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spSol_ObtenerExpediente_Sitrad]");
            objDE.AddInParameter(objComando, "@id_solicitudes ", DbType.Int32, id_solicitudes);
            return objDE.ExecuteScalar(objComando).ToString();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void spSol_ActualizarDocumentosAprobaciones(Int64 Id_Solicitudes, string Url, string Tipo, string nroDocumento, Int32 usr_registro)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("spSol_ActualizarDocumentosAprobaciones", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@id_solicitudes", SqlDbType.BigInt).Value = Id_Solicitudes;
                cmd.Parameters.Add("@Url", SqlDbType.VarChar, 200).Value = Url;
                cmd.Parameters.Add("@Tipo", SqlDbType.VarChar, 50).Value = Tipo;
                cmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 150).Value = nroDocumento;
                cmd.Parameters.Add("@usr_registro", SqlDbType.Int, 50).Value = usr_registro;

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }


    public void spSol_ActualizarDocumentosAprobaciones(Int64 Id_Solicitudes, string Url, string Tipo, string nroDocumento, Int32 usr_registro, SqlTransaction transaccion)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spSol_ActualizarDocumentosAprobaciones]");
            objDE.AddInParameter(objComando, "@id_solicitudes", DbType.String, Id_Solicitudes);
            objDE.AddInParameter(objComando, "@Url", DbType.String, Url);
            objDE.AddInParameter(objComando, "@Tipo", DbType.String, Tipo);
            objDE.AddInParameter(objComando, "@nroDocumento", DbType.String, nroDocumento);
            objDE.AddInParameter(objComando, "@usr_registro", DbType.Int32, usr_registro);
            objDE.ExecuteNonQuery(objComando, transaccion);

        }
        catch (DbException ex)
        {
            throw new Exception(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    public Boolean spSol_VerificarCreacion(Int64 id_solicitudes, string tipoDocumento, int usr_registro)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("spSol_VerificarCreacion", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@id_solicitudes", SqlDbType.BigInt).Value = id_solicitudes;
                cmd.Parameters.Add("@tipoDocumento", SqlDbType.VarChar, 50).Value = tipoDocumento;
                cmd.Parameters.Add("@usr_registro", SqlDbType.Int, 255).Value = usr_registro;

                cmd.Connection.Open();
                return Convert.ToBoolean(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }
    #endregion

    public int F_SOL_ACCESO(BEProyecto _BE)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("spSOL_Acceso", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@id_solicitudes", SqlDbType.Int).Value = _BE.Id_Solicitudes;
                cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.Id_usuario;

                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }

    public DataTable spu_MON_UltimaActualizacion(Int64 id_proyecto)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("spu_MON_UltimaActualizacion");
            objDE.AddInParameter(objComando, "@id_proyecto", DbType.String, id_proyecto);

            return objDE.ExecuteDataSet(objComando).Tables[0];


        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public DataSet paAdm_ReporteAdmisibilidad()
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[pnsu].[paAdm_ReporteAdmisibilidad]");
       

            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

}
