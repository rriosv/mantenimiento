﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccess
{
    public class DE_Resumen
    {        
        public SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());    

        public BE_FichaResumen fnObtenerUltimaFichaResumen(string CodSnip)
        {
            BE_FichaResumen oFichaResuemen = new BE_FichaResumen();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_SOL_RES_Obtener_UltimaFichaResumen";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add("@CodSnip", SqlDbType.Int).Value = CodSnip;
                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {

                        int _IdFicha = 0;
                        string _CodSnip = "";
                        string _CodFicha = "";
                        decimal _AvanceD = 0;
                        decimal _AvanceT = 0;
                        List<BE_Item> listadoItems = new List<BE_Item>();
                        while (dr.Read())
                        {
                            _IdFicha = Convert.ToInt32(dr["IdFicha"].ToString());
                            _CodSnip = dr["CodSnip"].ToString(); ;
                            _CodFicha = dr["CodFicha"].ToString();
                            _AvanceD = Convert.ToDecimal(dr["nAvanceD"].ToString());
                            _AvanceT = Convert.ToDecimal(dr["nAvanceT"].ToString()); ;

                            BE_Item oItem = new BE_Item();
                            oItem.IdItems = Convert.ToInt32(dr["IdItems"].ToString());
                            oItem.objOption = new BE_Option { IdItems = oItem.IdItems, IdOptions = Convert.ToInt32(dr["IdOptions"].ToString()) };
                            oItem.TieneHijos = Convert.ToInt32(dr["TieneCliches"].ToString());
                            listadoItems.Add(oItem);

                        }
                        if (_IdFicha == 0) return null;

                        oFichaResuemen.IdFicha = _IdFicha;
                        oFichaResuemen.CodSnip = _CodSnip;
                        oFichaResuemen.CodFicha = _CodFicha;
                        oFichaResuemen.nAvanceD = _AvanceD;
                        oFichaResuemen.nAvanceT = _AvanceT;
                        oFichaResuemen.listadoItems = listadoItems;

                    }

                    foreach (BE_Item item in oFichaResuemen.listadoItems)
                    {
                        if (item.TieneHijos == 1)
                        {
                            if (Cnx.State == ConnectionState.Open) Cnx.Close();
                            List<BE_Cliche> listadoCliches = new List<BE_Cliche>();
                            cmd = new SqlCommand();
                            cmd.Connection = Cnx;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "sp_SOL_RES_ListarClichesPorFicha";
                            cmd.CommandTimeout = 36000;
                            cmd.Parameters.Add("@IdFicha", SqlDbType.Int).Value = oFichaResuemen.IdFicha;
                            cmd.Parameters.Add("@IdItems", SqlDbType.Int).Value = item.IdItems;
                            cmd.Connection.Open();

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    BE_Cliche oCliche = new BE_Cliche();
                                    oCliche.IdCliches = Convert.ToInt32(dr["IdCliches"].ToString());
                                    oCliche.vDescripcion = dr["vDescripcion"].ToString();
                                    listadoCliches.Add(oCliche);
                                }
                            }
                            item.listaCliches = listadoCliches;
                        }
                    }

                }
                catch (Exception ex)
                {
                    oFichaResuemen = null;
                    throw ex;
                }
                finally
                {
                    cmd = null;                    
                    if (Cnx.State == ConnectionState.Open) Cnx.Close();
                }
            }

            return oFichaResuemen;
        }

        public bool fn_Insert_ResFicha(BE_FichaResumen oFichaResumen) {
            bool bRpta = false;
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                Cnx.Open();
                SqlCommand cmd = new SqlCommand();
                SqlTransaction transaction = Cnx.BeginTransaction();

                cmd.Connection = Cnx;
                cmd.Transaction = transaction;
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spi_SOL_RES_Insert_Ficha";
                    cmd.CommandTimeout = 36000;                    
                    cmd.Parameters.AddWithValue("@IdFicha", 0).Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@CodSnip", oFichaResumen.CodSnip);
                    cmd.Parameters.AddWithValue("@CodFicha", oFichaResumen.CodFicha);
                    cmd.Parameters.AddWithValue("@Usuario", oFichaResumen.vUsuario);
                    cmd.ExecuteNonQuery();
                    oFichaResumen.IdFicha = Convert.ToInt32(cmd.Parameters["@IdFicha"].Value);

                    foreach (BE_Item item in oFichaResumen.listadoItems) {
                        cmd = new SqlCommand();
                        cmd.Connection = Cnx;
                        cmd.Transaction = transaction;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "spi_SOL_RES_Insert_FichaItemOption";
                        cmd.CommandTimeout = 36000;
                        cmd.Parameters.AddWithValue("@IdFicha", oFichaResumen.IdFicha);
                        cmd.Parameters.AddWithValue("@IdItems", item.IdItems);
                        cmd.Parameters.AddWithValue("@IdOptions", item.objOption.IdOptions);                        
                        cmd.ExecuteNonQuery();
                        if (item.listaCliches != null && item.listaCliches.Count > 0){
                            foreach (BE_Cliche cliche in item.listaCliches)
                            {
                                cmd = new SqlCommand();
                                cmd.Connection = Cnx;
                                cmd.Transaction = transaction;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandText = "spi_SOL_RES_Insert_FichaItemCliche";
                                cmd.CommandTimeout = 36000;
                                cmd.Parameters.AddWithValue("@IdFicha", oFichaResumen.IdFicha);
                                cmd.Parameters.AddWithValue("@IdItems", item.IdItems);
                                cmd.Parameters.AddWithValue("@IdCliches", cliche.IdCliches);
                                cmd.Parameters.AddWithValue("@vDescripcion", cliche.vDescripcion);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.Transaction = transaction;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_SOL_RES_Obtener_Ficha";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.AddWithValue("@IdFicha", oFichaResumen.IdFicha);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read()) {
                        oFichaResumen.nAvanceD = Convert.ToDecimal(dr["nAvanceD"].ToString());
                        oFichaResumen.nAvanceT = Convert.ToDecimal(dr["nAvanceT"].ToString());
                    }
                    dr.Close();
                    transaction.Commit();
                    bRpta = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            return bRpta;
        }
    }
}
