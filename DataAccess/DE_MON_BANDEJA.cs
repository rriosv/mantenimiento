﻿using System;
using System.Collections.Generic;
using System.Data;
using Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace DataAccess
{
    public class DE_MON_BANDEJA
    {

        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        #region SELECT

        public DataSet spMON_BandejaCoordinador(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_BandejaCordinador]");
                objDE.AddInParameter(objComando, "@estado", DbType.Int32, _BE_Bandeja.estadoProyecto);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);

                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@sector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                objDE.AddInParameter(objComando, "@id_modalidadFinanciamiento", DbType.String, _BE_Bandeja.modalidadFinanciamiento);
                objDE.AddInParameter(objComando, "@Subsector", DbType.String, _BE_Bandeja.Subsector);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarUbigeo(string tipo, string depa, string prov, string dist)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListarUbigeo";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = tipo;
                    cmd.Parameters.Add("@cod_depa", SqlDbType.VarChar, 2).Value = depa;
                    cmd.Parameters.Add("@cod_prov", SqlDbType.VarChar, 2).Value = prov;
                    cmd.Parameters.Add("@cod_dist", SqlDbType.VarChar, 2).Value = dist;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int codigo = Reader.GetOrdinal("codigo");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.codigo = Reader.GetString(codigo);
                            objENT.nombre = Reader.GetString(nombre);


                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoPrograma()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);

                try
                {

                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoPrograma";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoSubPrograma(int id_tipoPrograma)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);

                try
                {

                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoSubPrograma";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add("@id_tipoPrograma", SqlDbType.Int).Value = id_tipoPrograma;
                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarEstado()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListarEstado";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTecnico(string sector)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListarTecnico";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@sector", SqlDbType.VarChar, 2).Value = sector;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");
                        int tecnico = Reader.GetOrdinal("tecnico");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);
                            objENT.tecnico = Reader.GetString(tecnico);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTecnicoxTipo(string strSector, string strTipoAgente)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListarTecnicoxTipo";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@sector", SqlDbType.VarChar, 2).Value = strSector;
                    cmd.Parameters.Add("@tipoAgente", SqlDbType.VarChar, 2).Value = strTipoAgente;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");
                        int tecnico = Reader.GetOrdinal("tecnico");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);
                            objENT.tecnico = Reader.GetString(tecnico);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTipoEjecucion()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListarTipoEjecucion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int tecnico = Reader.GetOrdinal("tecnico");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.tecnico = Reader.GetString(tecnico);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoFinanciamiento()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoFinanciamiento";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_FiltroEstadoEjecucion()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_FiltroEstadoEjecucion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Convert.ToInt32(Reader.GetInt64(valor));
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }

                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ObtenerCorreo(BE_MON_BANDEJA _BE)
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ObtenerCorreo";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_tecnico", SqlDbType.Int).Value = _BE.id_tecnico;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.tipo;
                    cmd.Parameters.Add("@sector", SqlDbType.VarChar, 1).Value = _BE.sector;


                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_correo = Reader.GetOrdinal("correo");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.correo = Reader.GetString(Ordinal_correo);
                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_BandejaMonitor(BE_MON_BANDEJA _BE)
        {

            //  Este bloque using cierra y libera automáticamente los recursos utilizados por "conn"
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {

                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_BandejaMonitor";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@snip", SqlDbType.VarChar, 10).Value = _BE.snip;
                    cmd.Parameters.Add("@proyecto", SqlDbType.VarChar, 100).Value = _BE.nombProyecto;
                    cmd.Parameters.Add("@depa", SqlDbType.VarChar, 2).Value = _BE.depa;
                    cmd.Parameters.Add("@prov", SqlDbType.VarChar, 2).Value = _BE.prov;
                    cmd.Parameters.Add("@dist", SqlDbType.VarChar, 2).Value = _BE.dist;
                    cmd.Parameters.Add("@sector", SqlDbType.VarChar, 2).Value = _BE.sector;
                    cmd.Parameters.Add("@tecnico", SqlDbType.VarChar, 4).Value = _BE.tecnicofiltro;
                    cmd.Parameters.Add("@tipo", SqlDbType.VarChar, 2).Value = _BE.tipoFinanciamientoFiltro;
                    cmd.Parameters.Add("@anio", SqlDbType.VarChar, 4).Value = _BE.Anio;
                    cmd.Parameters.Add("@estrategia", SqlDbType.VarChar, 2).Value = _BE.StrEstrategia;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int Ordinal_Id_proyecto = Reader.GetOrdinal("id_proyecto");
                        int Ordinal_anio = Reader.GetOrdinal("anio");
                        int Ordinal_subsector = Reader.GetOrdinal("subsector");
                        int Ordinal_cod_subsector = Reader.GetOrdinal("cod_subsector");
                        int Ordinal_cod_snip = Reader.GetOrdinal("cod_snip");
                        int Ordinal_nom_depa = Reader.GetOrdinal("nom_depa");
                        int Ordinal_nom_prov = Reader.GetOrdinal("nom_prov");
                        int Ordinal_nom_dist = Reader.GetOrdinal("nom_dist");
                        int Ordinal_nom_proyecto = Reader.GetOrdinal("nom_proyecto");
                        int Ordinal_entidad_ejec = Reader.GetOrdinal("entidad_ejec");
                        int Ordinal_tecnico = Reader.GetOrdinal("tecnico");
                        int Ordinal_costo_MVCS = Reader.GetOrdinal("costo_MVCS");
                        int Ordinal_id_TipoFinanciamiento = Reader.GetOrdinal("id_TipoFinanciamiento");

                        int Ordinal_tipoEstadoEjecucion = Reader.GetOrdinal("tipoEstadoEjecucion");
                        int Ordinal_vEstado = Reader.GetOrdinal("vEstado");

                        int Ordinal_id_solicitudes = Reader.GetOrdinal("id_solicitudes");
                        int Ordinal_infoEstudios = Reader.GetOrdinal("infoEstudios");
                        int Ordinal_Preinversion = Reader.GetOrdinal("Preinversion");
                        int Ordinal_Espediente = Reader.GetOrdinal("Espediente");
                        int Ordinal_Obra = Reader.GetOrdinal("Obra");
                        int Ordinal_mantenimiento = Reader.GetOrdinal("Mantenimiento");
                        int Ordinal_flagCMD = Reader.GetOrdinal("flagCMD");
                        int Ordinal_idTipoModalidad = Reader.GetOrdinal("idTipoModalidad");
                        int Ordinal_FechaActualizado = Reader.GetOrdinal("FechaActualizado");
                        int Ordinal_flagHistorico = Reader.GetOrdinal("FlagHistorico");
                        int Ordinal_ModalidadFinanciamiento = Reader.GetOrdinal("ModalidadFinanciamiento");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.Anio = Reader.IsDBNull(Ordinal_anio) ? "" : Reader.GetString(Ordinal_anio);
                            objENT.id_proyecto = Reader.GetInt32(Ordinal_Id_proyecto);
                            objENT.sector = Reader.GetString(Ordinal_subsector);
                            objENT.id_sector = Reader.GetInt32(Ordinal_cod_subsector);
                            objENT.snip = Reader.GetInt32(Ordinal_cod_snip).ToString();
                            objENT.depa = (Reader.IsDBNull(Ordinal_nom_depa) ? "" : (Reader.GetString(Ordinal_nom_depa)));
                            objENT.prov = (Reader.IsDBNull(Ordinal_nom_prov) ? "" : (Reader.GetString(Ordinal_nom_prov)));
                            objENT.dist = (Reader.IsDBNull(Ordinal_nom_dist) ? "" : (Reader.GetString(Ordinal_nom_dist)));
                            objENT.nombProyecto = Reader.GetString(Ordinal_nom_proyecto);
                            objENT.unidadEjecutora = Reader.GetString(Ordinal_entidad_ejec);
                            objENT.tecnicofiltro = Reader.GetString(Ordinal_tecnico);
                            objENT.costo_MVCS = (Reader.GetDouble(Ordinal_costo_MVCS)).ToString();
                            objENT.tipoFinanciamiento = Reader.GetInt32(Ordinal_id_TipoFinanciamiento);
                            objENT.tipoEstadoEjecucion = Reader.GetString(Ordinal_tipoEstadoEjecucion);
                            objENT.Estado = Reader.GetString(Ordinal_vEstado);

                            objENT.strIdSolicitudes = (Reader.IsDBNull(Ordinal_id_solicitudes) ? "" : (Reader.GetInt64(Ordinal_id_solicitudes)).ToString());
                            objENT.infoEstudios = Reader.GetString(Ordinal_infoEstudios);
                            objENT.Preinversion = Reader.GetInt32(Ordinal_Preinversion).ToString();
                            objENT.Espediente = Reader.GetInt32(Ordinal_Espediente).ToString();
                            objENT.Obra = Reader.GetInt32(Ordinal_Obra).ToString();
                            objENT.Mantenimiento = Reader.GetInt32(Ordinal_mantenimiento).ToString();
                            objENT.flagCMD = Reader.GetString(Ordinal_flagCMD);
                            objENT.Id_tipo = (Reader.IsDBNull(Ordinal_idTipoModalidad) ? 0 : (Reader.GetInt32(Ordinal_idTipoModalidad)));
                            objENT.fecha = Reader.GetString(Ordinal_FechaActualizado);
                            objENT.estadoProyecto = Reader.GetInt32(Ordinal_flagHistorico);
                            objENT.modalidadFinanciamiento = Reader.GetString(Ordinal_ModalidadFinanciamiento);
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_BancoMonitoreo()
        {

            //  Este bloque using cierra y libera automáticamente los recursos utilizados por "conn"
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {

                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_BancoMonitoreo";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_Id_proyecto = Reader.GetOrdinal("id_proyecto");
                        int Ordinal_id_solicitudes = Reader.GetOrdinal("id_solicitudes");
                        int Ordinal_subsector = Reader.GetOrdinal("Programa");
                        int Ordinal_cod_subsector = Reader.GetOrdinal("cod_subsector");
                        int Ordinal_cod_snip = Reader.GetOrdinal("cod_snip");
                        int Ordinal_codUnificado = Reader.GetOrdinal("iCodUnificado");
                        int Ordinal_id_subPrograma = Reader.GetOrdinal("id_tipoSubPrograma");
                        int Ordinal_subPrograma = Reader.GetOrdinal("subPrograma");
                        //int Ordinal_nom_prov = Reader.GetOrdinal("nom_prov");
                        //int Ordinal_nom_dist = Reader.GetOrdinal("nom_dist");
                        int Ordinal_nom_proyecto = Reader.GetOrdinal("nom_proyecto");
                        int Ordinal_entidad_ejec = Reader.GetOrdinal("entidad_ejec");
                        int Ordinal_id_TipoFinanciamiento = Reader.GetOrdinal("id_tipoFinanciamiento");
                        int Ordinal_tipo = Reader.GetOrdinal("tipo");
                        int Ordinal_monto_snip = Reader.GetOrdinal("monto_snip");
                        int Ordinal_ambito = Reader.GetOrdinal("ambito");
                        int Ordinal_id_ambito = Reader.GetOrdinal("id_ambito");
                        int Ordinal_anio = Reader.GetOrdinal("anio");
                        int Ordinal_num_hab_snip = Reader.GetOrdinal("num_hab_snip");

                        int Ordinal_flagReconstruccion = Reader.GetOrdinal("flagReconstruccion");
                        int Ordinal_flagServicio = Reader.GetOrdinal("flagServicio");
                        int Ordinal_flagEmergencia = Reader.GetOrdinal("flagEmergencia");
                        int Ordinal_vEstado = Reader.GetOrdinal("vEstado");
                        int Ordinal_idEstado = Reader.GetOrdinal("idEstado");
                        //int Ordinal_idSubEstado = Reader.GetOrdinal("idSubEstado");
                        int Ordinal_idTipoInversion = Reader.GetOrdinal("idTipoInversion");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.Anio = Reader.IsDBNull(Ordinal_anio) ? "" : Reader.GetString(Ordinal_anio);
                            objENT.id_proyecto = Reader.GetInt32(Ordinal_Id_proyecto);
                            objENT.strIdSolicitudes = (Reader.IsDBNull(Ordinal_id_solicitudes) ? "" : (Reader.GetInt32(Ordinal_id_solicitudes)).ToString());
                            objENT.sector = Reader.GetString(Ordinal_subsector);
                            objENT.id_sector = Reader.GetInt32(Ordinal_cod_subsector);
                            objENT.subPrograma = Reader.GetString(Ordinal_subPrograma);
                            objENT.id_subPrograma = Reader.GetInt32(Ordinal_id_subPrograma);
                            objENT.snip = Reader.GetInt32(Ordinal_cod_snip).ToString();
                            objENT.CodigoUnificado = Reader.GetInt32(Ordinal_codUnificado);
                            //objENT.depa = (Reader.IsDBNull(Ordinal_nom_depa) ? "" : (Reader.GetString(Ordinal_nom_depa)));
                            //objENT.prov = (Reader.IsDBNull(Ordinal_nom_prov) ? "" : (Reader.GetString(Ordinal_nom_prov)));
                            //objENT.dist = (Reader.IsDBNull(Ordinal_nom_dist) ? "" : (Reader.GetString(Ordinal_nom_dist)));
                            objENT.nombProyecto = Reader.GetString(Ordinal_nom_proyecto);
                            objENT.unidadEjecutora = Reader.GetString(Ordinal_entidad_ejec);
                            objENT.tipoFinanciamiento = Reader.GetInt32(Ordinal_id_TipoFinanciamiento);
                            objENT.tipo = Reader.GetString(Ordinal_tipo);
                            objENT.monto_SNIP = (Reader.GetDouble(Ordinal_monto_snip)).ToString();

                            objENT.ambito = (Reader.GetInt32(Ordinal_id_ambito));
                            objENT.observacion = (Reader.GetString(Ordinal_ambito)).ToString();
                            objENT.poblacionSnip = Reader.GetInt32(Ordinal_num_hab_snip);
                            objENT.flagReconstruccion = Convert.ToBoolean(Reader.GetInt32(Ordinal_flagReconstruccion));
                            objENT.flagServicio = Convert.ToBoolean(Reader.GetInt32(Ordinal_flagServicio));
                            objENT.flagEmergencia = Convert.ToInt32(Reader.GetInt32(Ordinal_flagEmergencia));
                            //objENT.flagReconstruccion = Convert.ToBoolean(Reader["flagReconstruccion"]);

                            objENT.Estado = Reader.GetString(Ordinal_vEstado);
                            //bjENT.id_ = Reader.GetString(Ordinal_idSubEstado);
                            objENT.id_TipoEstadoEjecucion = Convert.ToInt32(Reader.GetInt32(Ordinal_idEstado));
                            objENT.idTipoInversion = Convert.ToInt32(Reader.GetInt32(Ordinal_idTipoInversion));

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_BandejaConcluidos(BE_MON_BANDEJA _BE)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_BandejaConcluidos";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@snip", SqlDbType.VarChar, 10).Value = _BE.snip;
                    cmd.Parameters.Add("@proyecto", SqlDbType.VarChar, 50).Value = _BE.nombProyecto;
                    cmd.Parameters.Add("@depa", SqlDbType.VarChar, 2).Value = _BE.depa;
                    cmd.Parameters.Add("@prov", SqlDbType.VarChar, 2).Value = _BE.prov;
                    cmd.Parameters.Add("@dist", SqlDbType.VarChar, 2).Value = _BE.dist;
                    cmd.Parameters.Add("@sector", SqlDbType.VarChar, 2).Value = _BE.sector;
                    cmd.Parameters.Add("@anio", SqlDbType.VarChar, 4).Value = _BE.Anio;
                    cmd.Parameters.Add("@flagConcluido", SqlDbType.VarChar, 1).Value = _BE.tipo;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_Id_proyecto = Reader.GetOrdinal("id_proyecto");
                        int Ordinal_anio = Reader.GetOrdinal("ini_periodo");
                        int Ordinal_subsector = Reader.GetOrdinal("subsector");
                        int Ordinal_Id_solicitud = Reader.GetOrdinal("id_solicitud");
                        int Ordinal_cod_snip = Reader.GetOrdinal("cod_snip");
                        int Ordinal_nom_depa = Reader.GetOrdinal("nom_depa");
                        int Ordinal_nom_prov = Reader.GetOrdinal("nom_prov");
                        int Ordinal_nom_dist = Reader.GetOrdinal("nom_dist");
                        int Ordinal_nom_proyecto = Reader.GetOrdinal("nom_proyecto");
                        int Ordinal_entidad_ejec = Reader.GetOrdinal("entidad_ejec");
                        // int Ordinal_tecnico = Reader.GetOrdinal("tecnico");
                        int Ordinal_TipoFinanciamiento = Reader.GetOrdinal("tipo");
                        int Ordinal_monto_snip = Reader.GetOrdinal("monto_snip");
                        int Ordinal_costo_MVCS = Reader.GetOrdinal("costo_MVCS");
                        int Ordinal_idTipoFinanciamiento = Reader.GetOrdinal("id_tipoFinanciamiento");
                        int Ordinal_tipoEstadoEjecucion = Reader.GetOrdinal("tipoEstadoEjecucion");
                        int Ordinal_fisicoejecutado = Reader.GetOrdinal("fisicoejecutado");
                        int Ordinal_FechaFinContractual = Reader.GetOrdinal("FechaFinContractual");
                        int Ordinal_FechaFinReal = Reader.GetOrdinal("FechaFinReal");
                        int Ordinal_imgFicha = Reader.GetOrdinal("imgFicha");
                        int Ordinal_fechaInauguracion = Reader.GetOrdinal("fechaInauguracion");
                        int Ordinal_urlFichaRecepcion = Reader.GetOrdinal("urlFichaRecepcion");


                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.Anio = Reader.IsDBNull(Ordinal_anio) ? "" : Reader.GetString(Ordinal_anio);
                            objENT.id_proyecto = Reader.GetInt32(Ordinal_Id_proyecto);
                            objENT.id = Reader.GetInt32(Ordinal_Id_solicitud);
                            objENT.sector = Reader.GetString(Ordinal_subsector);
                            objENT.snip = Reader.GetInt32(Ordinal_cod_snip).ToString();
                            objENT.depa = (Reader.IsDBNull(Ordinal_nom_depa) ? "" : (Reader.GetString(Ordinal_nom_depa)));
                            objENT.prov = (Reader.IsDBNull(Ordinal_nom_prov) ? "" : (Reader.GetString(Ordinal_nom_prov)));
                            objENT.dist = (Reader.IsDBNull(Ordinal_nom_dist) ? "" : (Reader.GetString(Ordinal_nom_dist)));
                            objENT.nombProyecto = Reader.GetString(Ordinal_nom_proyecto);
                            objENT.unidadEjecutora = Reader.GetString(Ordinal_entidad_ejec);
                            objENT.tipoFinanciamientoFiltro = Reader.GetString(Ordinal_TipoFinanciamiento);
                            objENT.tipoFinanciamiento = Reader.GetInt32(Ordinal_idTipoFinanciamiento);
                            objENT.monto_SNIP = (Reader.GetDouble(Ordinal_monto_snip)).ToString();
                            objENT.costo_MVCS = (Reader.GetDouble(Ordinal_costo_MVCS)).ToString();
                            objENT.tipoEstadoEjecucion = Reader.GetString(Ordinal_tipoEstadoEjecucion);

                            objENT.fisicoEjecutado = Reader.GetDecimal(Ordinal_fisicoejecutado).ToString();
                            objENT.strFechaFinContractual = (Reader.IsDBNull(Ordinal_FechaFinContractual) ? "" : Reader.GetString(Ordinal_FechaFinContractual));
                            objENT.strFechaFinReal = (Reader.IsDBNull(Ordinal_FechaFinReal) ? "" : Reader.GetString(Ordinal_FechaFinReal));
                            objENT.imgFicha = Reader.GetString(Ordinal_imgFicha);
                            objENT.dtFechaInauguracion = Reader.IsDBNull(Ordinal_fechaInauguracion) ? (DateTime?)null : Reader.GetDateTime(Ordinal_fechaInauguracion);
                            objENT.UrlDoc = (Reader.IsDBNull(Ordinal_urlFichaRecepcion) ? "" : Reader.GetString(Ordinal_urlFichaRecepcion));

                            if (objENT.UrlDoc != "")
                            {
                                string ext = objENT.UrlDoc.Substring(objENT.UrlDoc.Length - 3, 3);

                                ext = ext.ToLower();

                                if (ext == "pdf")
                                {
                                    objENT.imgExtension = "~/img/pdf.gif";
                                }

                                if (ext == "xls" || ext == "lsx")
                                {
                                    objENT.imgExtension = "~/img/xls.gif";

                                }

                                if (ext == "doc" || ext == "ocx")
                                {
                                    objENT.imgExtension = "~/img/doc.gif";

                                }

                                if (ext == "png" || ext == "PNG")
                                {
                                    objENT.imgExtension = "~/img/jpg.gif";

                                }
                                if (ext == "jpg" || ext == "JPG")
                                {
                                    objENT.imgExtension = "~/img/jpg.gif";

                                }

                            }
                            else
                            {
                                objENT.imgExtension = "~/img/blanco.png";
                            }

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        //public DataSet spMON_Reporte_BandejaConcluidos(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Reporte_BandejaConcluidos]");
        //        objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
        //        objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
        //        objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
        //        objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
        //        objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
        //        objDE.AddInParameter(objComando, "@sector", DbType.String, _BE_Bandeja.sector);
        //        objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
        //        objDE.AddInParameter(objComando, "@flagConcluido", DbType.String, _BE_Bandeja.tipo);

        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_Reporte_BandejaConcluidos(BE_MON_BANDEJA _BE)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_Reporte_BandejaConcluidos";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@snip", SqlDbType.VarChar, 10).Value = _BE.snip;
                    cmd.Parameters.Add("@proyecto", SqlDbType.VarChar, 50).Value = _BE.nombProyecto;
                    cmd.Parameters.Add("@depa", SqlDbType.VarChar, 2).Value = _BE.depa;
                    cmd.Parameters.Add("@prov", SqlDbType.VarChar, 2).Value = _BE.prov;
                    cmd.Parameters.Add("@dist", SqlDbType.VarChar, 2).Value = _BE.dist;
                    cmd.Parameters.Add("@sector", SqlDbType.VarChar, 2).Value = _BE.sector;
                    cmd.Parameters.Add("@anio", SqlDbType.VarChar, 4).Value = _BE.Anio;
                    cmd.Parameters.Add("@flagConcluido", SqlDbType.VarChar, 1).Value = _BE.tipo;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_Id_proyecto = Reader.GetOrdinal("id_proyecto");
                        int Ordinal_subsector = Reader.GetOrdinal("subsector");
                        int Ordinal_anio = Reader.GetOrdinal("ini_periodo");
                        int Ordinal_cod_snip = Reader.GetOrdinal("cod_snip");
                        int Ordinal_nom_proyecto = Reader.GetOrdinal("nom_proyecto");
                        int Ordinal_monto_inversion = Reader.GetOrdinal("monto_inversion");
                        int Ordinal_monto_transferencia = Reader.GetOrdinal("monto_transferencia");
                        int Ordinal_entidad_ejec = Reader.GetOrdinal("entidad_ejec");
                        int Ordinal_NroDS = Reader.GetOrdinal("NroDS");
                        int Ordinal_poblacion_snip = Reader.GetOrdinal("poblacion_snip");
                        int Ordinal_fisicoejecutado = Reader.GetOrdinal("fisicoejecutado");
                        int Ordinal_id_tipoEstadoEjecucion = Reader.GetOrdinal("id_tipoEstadoEjecuccion");
                        int Ordinal_tipoEstadoEjecucion = Reader.GetOrdinal("tipoEstadoEjecucion");
                        int Ordinal_monitor = Reader.GetOrdinal("monitor");
                        int Ordinal_fechaInauguracion = Reader.GetOrdinal("fechaInauguracion");
                        int Ordinal_horaInicio = Reader.GetOrdinal("horaInicio");
                        int Ordinal_representanteMVCS = Reader.GetOrdinal("representanteMVCS");


                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.id_proyecto = Reader.GetInt32(Ordinal_Id_proyecto);
                            objENT.sector = Reader.GetString(Ordinal_subsector);
                            objENT.Anio = Reader.IsDBNull(Ordinal_anio) ? "" : Reader.GetString(Ordinal_anio);
                            objENT.snip = Reader.GetInt32(Ordinal_cod_snip).ToString();
                            objENT.nombProyecto = Reader.GetString(Ordinal_nom_proyecto);
                            objENT.monto_inversion = (Reader.GetDouble(Ordinal_monto_inversion)).ToString();
                            objENT.monto_transferencia = (Reader.GetDouble(Ordinal_monto_transferencia)).ToString();
                            objENT.unidadEjecutora = Reader.GetString(Ordinal_entidad_ejec);

                            objENT.nroDS = Reader.IsDBNull(Ordinal_NroDS) ? "" : Reader.GetString(Ordinal_NroDS);
                            objENT.poblacionSnip = Reader.GetInt32(Ordinal_poblacion_snip);
                            objENT.fisicoEjecutado = Reader.GetDecimal(Ordinal_fisicoejecutado).ToString();
                            objENT.id_TipoEstadoEjecucion = Reader.GetInt32(Ordinal_id_tipoEstadoEjecucion);
                            objENT.tipoEstadoEjecucion = Reader.GetString(Ordinal_tipoEstadoEjecucion);
                            objENT.tecnico = Reader.GetString(Ordinal_monitor);
                            objENT.dtFechaInauguracion = Reader.IsDBNull(Ordinal_fechaInauguracion) ? (DateTime?)null : Reader.GetDateTime(Ordinal_fechaInauguracion);

                            // objENT.fecha = Reader.IsDBNull(Ordinal_fechaInauguracion) ? "" : Reader.GetString(Ordinal_fechaInauguracion);
                            if (objENT.dtFechaInauguracion.ToString().Length > 0)
                            {
                                objENT.fecha = Convert.ToDateTime(objENT.dtFechaInauguracion).ToString("dd/MM/yyyy");
                            }

                            objENT.horaInicio = Reader.IsDBNull(Ordinal_horaInicio) ? "" : Reader.GetString(Ordinal_horaInicio);
                            objENT.representanteMVCS = Reader.IsDBNull(Ordinal_representanteMVCS) ? "" : Reader.GetString(Ordinal_representanteMVCS);
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ReporteObrasDesactualizadas(BE_MON_BANDEJA _BE)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ReporteObrasDesactualizadas";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@snip", SqlDbType.VarChar, 10).Value = _BE.snip;
                    cmd.Parameters.Add("@depa", SqlDbType.VarChar, 2).Value = _BE.depa;
                    cmd.Parameters.Add("@IdPrograma", SqlDbType.VarChar, 1).Value = _BE.sector;
                    cmd.Parameters.Add("@IdTecnico", SqlDbType.VarChar, 3).Value = _BE.tecnico;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_programa = Reader.GetOrdinal("programa");
                        int Ordinal_SNIP = Reader.GetOrdinal("SNIP");
                        int Ordinal_Depa = Reader.GetOrdinal("Depa");
                        int Ordinal_proyecto = Reader.GetOrdinal("proyecto");
                        int Ordinal_estado = Reader.GetOrdinal("estado");
                        int Ordinal_vFecha = Reader.GetOrdinal("vFecha");
                        int Ordinal_FISICOREAL = Reader.GetOrdinal("FISICOREAL");
                        int Ordinal_tecnico = Reader.GetOrdinal("tecnico");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.sector = Reader.GetString(Ordinal_programa);
                            objENT.snip = Reader.GetInt32(Ordinal_SNIP).ToString();
                            objENT.depa = Reader.GetString(Ordinal_Depa);
                            objENT.nombProyecto = Reader.GetString(Ordinal_proyecto);
                            objENT.Estado = Reader.GetString(Ordinal_estado);
                            objENT.fecha = Reader.GetString(Ordinal_vFecha);
                            objENT.fisicoEjecutado = Reader.GetDecimal(Ordinal_FISICOREAL).ToString("N");
                            objENT.tecnico = Reader.GetString(Ordinal_tecnico);

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoAgente(int tipoAgente)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);

                try
                {

                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoAgente";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = tipoAgente;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public DataSet spProc_Rep_ShockInversion(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spPROC_Rep_ShocInversion]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@sector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anioMayor", DbType.String, _BE_Bandeja.estadoProyecto);
                objDE.AddInParameter(objComando, "@anioMenor", DbType.String, _BE_Bandeja.tipo);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteDetalladoPersonalizable(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteDetalladoPersonalizable]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@sector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anioMayor", DbType.String, _BE_Bandeja.estadoProyecto);
                objDE.AddInParameter(objComando, "@anioMenor", DbType.String, _BE_Bandeja.tipo);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ObtenerIDProyectosFinalizado(string snip, string tipoFinanciamiento)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ObtenerIDProyectosFinalizado]");
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@snip", DbType.String, snip);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ReporteGeneralPMIB(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteGeneralPMIB]");
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ReporteDetallePMIB(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteDetallePMIB]");
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteParalizados(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteParalizados]");
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteMetasPMIB(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteMetasPMIB]");
                //objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ReporteSuperGeneralPNSU(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteSuperGeneralPNSU]");
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteMonitoreoPNSUparaPNSR()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteMonitoreoPNSUparaPNSR]");
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteGeneral(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteGeneral]");
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spSOL_RegistroPreliminar(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_RegistroPreliminar]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@cod_subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@estrategia", DbType.Int32, _BE_Bandeja.Estrategia);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public List<BE_MON_BANDEJA> F_spMON_DocumentoMonitor(BE_MON_BANDEJA _BEBandeja)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_DocumentoMonitor";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BEBandeja.id_proyecto;
                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int numero = Reader.GetOrdinal("numero");
                        int id_documento_monitor = Reader.GetOrdinal("id_documento_monitor");
                        int nombre = Reader.GetOrdinal("nombre");
                        int urlDoc = Reader.GetOrdinal("urlDoc");
                        int id_tipoDocumentoMonitor = Reader.GetOrdinal("id_tipoDocumentoMonitor");
                        int tipo = Reader.GetOrdinal("tipo");
                        int descripcion = Reader.GetOrdinal("descripcion");
                        int fecha = Reader.GetOrdinal("fecha");
                        int usuario = Reader.GetOrdinal("usuario");
                        int fecha_update = Reader.GetOrdinal("fecha_update");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.numero = Reader.GetInt64(numero);
                            objENT.Id_documentoMonitor = Reader.GetInt32(id_documento_monitor);
                            objENT.nombre = Reader.GetString(nombre);
                            objENT.UrlDoc = Reader.GetString(urlDoc);
                            objENT.id_tipoDocumentoMonitor = Reader.GetInt32(id_tipoDocumentoMonitor).ToString();
                            objENT.tipo = Reader.GetString(tipo);
                            objENT.Descripcion = Reader.GetString(descripcion);
                            objENT.fecha = (Reader.IsDBNull(fecha) ? "" : (Reader.GetString(fecha)).ToString());
                            // objENT.Fecha = Reader.GetString(fecha);
                            objENT.usuario = Reader.GetString(usuario);
                            objENT.fecha_update = Reader.GetDateTime(fecha_update);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_FotoDocumentoMonitor(BE_MON_BANDEJA _BEBandeja)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_FotoDocumentoMonitor";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BEBandeja.id_proyecto;
                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int id_documento_monitor = Reader.GetOrdinal("id_documento_monitor");
                        int nombre = Reader.GetOrdinal("nombre");
                        int urlDoc = Reader.GetOrdinal("urlDoc");


                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.Id_documentoMonitor = Reader.GetInt32(id_documento_monitor);
                            objENT.nombre = Reader.GetString(nombre);
                            objENT.UrlDoc = Reader.GetString(urlDoc);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoDocumentoMonitor()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoDocumentoMonitor";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public DataSet spMON_Revisor(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Revisor]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@sector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_InauguracionProyecto(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_InauguracionProyecto]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Bandeja.id_proyecto);

                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_InauguracionCalendario(string fecha)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_InauguracionCalendario]");

                objDE.AddInParameter(objComando, "@fecha", DbType.String, fecha);

                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_InauguracionAll()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_InauguracionAll]");

                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoInaugura()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoInaugura";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spSOL_MotivoDevolucion()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spSOL_MotivoDevolucion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spSOL_TipoDevolucion()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spSOL_TipoDevolucion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoTipologia()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoTipologia";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTambos()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListarTambos";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDenominacionFuente()
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoDenominacionFuente";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public DataSet spSOL_BandejaEntregaExpedientes(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_BandejaEntregaExpedientes]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@cod_subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spSOL_BandejaCambiarEstado(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_BandejaCambiarEstado]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@cod_depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@cod_prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@cod_dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@cod_subsector", DbType.String, _BE_Bandeja.sector);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Bandeja.tipoFinanciamientoFiltro);
                objDE.AddInParameter(objComando, "@estado", DbType.String, _BE_Bandeja.Estado);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_BANDEJA> F_spSOL_BandejaDevolucion(BE_MON_BANDEJA _BE)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spSOL_BandejaDevolucion";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.AddWithValue("@snip", _BE.snip);
                cmd.Parameters.AddWithValue("@proyecto", _BE.nombProyecto);
                cmd.Parameters.AddWithValue("@cod_depa", _BE.depa);
                cmd.Parameters.AddWithValue("@cod_prov", _BE.prov);
                cmd.Parameters.AddWithValue("@cod_dist", _BE.dist);
                cmd.Parameters.AddWithValue("@cod_subsector", _BE.sector);
                cmd.Parameters.AddWithValue("@tipo", _BE.tipoFinanciamientoFiltro);
                cmd.Parameters.AddWithValue("@estado", _BE.Estado);


                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();
                        objENT.id_proyecto = Convert.ToInt32(Reader["id_solicitudes"].ToString());
                        objENT.sector = Reader["subsector"].ToString();
                        objENT.snip = Reader["snip"].ToString();
                        objENT.depa = Reader["departamento"].ToString();
                        objENT.prov = Reader["provincia"].ToString();
                        objENT.dist = Reader["distrito"].ToString();
                        objENT.nombProyecto = Reader["nombre_proyecto"].ToString();
                        objENT.unidadEjecutora = Reader["unidad_ejec"].ToString();
                        objENT.tipoDocumentacion = Reader["documentacion"].ToString();
                        objENT.Estado = Reader["estado"].ToString();
                        objENT.tecnico = Reader["tecnico"].ToString();
                        objENT.tipoFinanciamientoFiltro = Reader["financiamiento"].ToString();

                        objENT.id_tipoDevolucion = Reader["id_tipoDevolucion"].ToString() == "" ? 0 : Convert.ToInt32(Reader["id_tipoDevolucion"].ToString());
                        objENT.tipoDevolucion = Reader["tipoDevolucion"].ToString();
                        objENT.id_motivoDevolucion = Reader["id_motivoDevolucion"].ToString() == "" ? 0 : Convert.ToInt32(Reader["id_motivoDevolucion"].ToString());
                        objENT.motivoDevolucion = Reader["motivoDevolucion"].ToString();
                        objENT.nroTramite = Reader["nroTramite"].ToString();
                        objENT.nroOficio = Reader["nroOficio"].ToString();
                        objENT.usuario = Reader["usuario"].ToString();
                        objENT.fecha = Reader["fecha_update"].ToString();
                        objENT.flagActivo = Convert.ToInt32(Reader["flagFinanciado"].ToString());

                        if (Reader["fecha_registro"].ToString() == "")
                        {
                        }
                        else
                        {
                            objENT.dtFechaInauguracion = Convert.ToDateTime(Reader["fecha_registro"].ToString());
                        }

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public BE_MON_BANDEJA F_spMON_ObtieneInfo(BE_MON_BANDEJA _BE_Bandeja)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ObtieneInfo";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE_Bandeja.id_usuario;
                cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE_Bandeja.tipo;

                cmd.Connection.Open();
                SqlDataReader Reader = cmd.ExecuteReader();

                int id_tecnico = Reader.GetOrdinal("id_tecnico");
                int nivel = Reader.GetOrdinal("nivel");

                if (Reader.Read())
                {
                    objENT = new BE_MON_BANDEJA();

                    objENT.id_tecnico = Reader.GetInt32(id_tecnico);
                    objENT.nivel = Reader.GetString(nivel);

                }

                return objENT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TecnicoSkype(int id_proyecto)
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TecnicoSkype";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_nombre = Reader.GetOrdinal("nombre");
                        int Ordinal_skype = Reader.GetOrdinal("user_skype");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.nombre = Reader.GetString(Ordinal_nombre);
                            objENT.usuario = (Reader.IsDBNull(Ordinal_skype) ? "" : Reader.GetString(Ordinal_skype));
                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_CorreobyIdProyecto(int id_proyecto)
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_CorreobyIdProyecto";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_nombre = Reader.GetOrdinal("nombre");
                        int Ordinal_correo = Reader.GetOrdinal("correo");
                        int Ordinal_nivel = Reader.GetOrdinal("nivel");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.nombre = Reader.GetString(Ordinal_nombre);
                            objENT.correo = Reader.GetString(Ordinal_correo);
                            objENT.nivel = Reader.GetString(Ordinal_nivel);
                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_ReporteMonitoreoOEEE()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteMonitoreoOEEE]");

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ReporteNE(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteNE]");

                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                //objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                //objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_Reporte_PorContrata()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReportePorContrata]");

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ReporteValorFinanciadoNE(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteValorFinanciadoNE]");

                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                //objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                //objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteSeguimientoNE(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteSeguimientoNE]");

                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                //objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                //objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteDetalleDevengadoNE(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteDetalleDevengadoNE]");

                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, _BE_Bandeja.tecnicofiltro);
                objDE.AddInParameter(objComando, "@subsector", DbType.String, _BE_Bandeja.sector);
                //objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Bandeja.Anio);
                //objDE.AddInParameter(objComando, "@flagCMD", DbType.String, _BE_Bandeja.flagCMD);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteSedapal()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteSedapal]");

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_BANDEJA> F_spMON_UbigeosMetas(int id_proyecto)
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_UbigeosMetas";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = id_proyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_id = Reader.GetOrdinal("id_ubigeo_proy");
                        int Ordinal_cod_depa = Reader.GetOrdinal("cod_depa");
                        int Ordinal_cod_prov = Reader.GetOrdinal("cod_prov");
                        int Ordinal_cod_dist = Reader.GetOrdinal("cod_dist");
                        int Ordinal_cod_ccpp = Reader.GetOrdinal("cod_ccpp");
                        int Ordinal_nom_depa = Reader.GetOrdinal("nom_depa");
                        int Ordinal_nom_prov = Reader.GetOrdinal("nom_prov");
                        int Ordinal_nom_dist = Reader.GetOrdinal("nom_dist");
                        int Ordinal_CCPP = Reader.GetOrdinal("CCPP");
                        int Ordinal_meta = Reader.GetOrdinal("meta");
                        int Ordinal_metaRealizada = Reader.GetOrdinal("metaRealizada");
                        int Ordinal_observacion = Reader.GetOrdinal("observacion");
                        int Ordinal_usuario = Reader.GetOrdinal("usuario");
                        int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.id = Reader.GetInt32(Ordinal_id);
                            objENT.codigo = Reader.GetString(Ordinal_cod_depa) + Reader.GetString(Ordinal_cod_prov) + Reader.GetString(Ordinal_cod_dist);
                            objENT.ubigeoCCPP = (Reader.IsDBNull(Ordinal_cod_ccpp) ? "" : Reader.GetString(Ordinal_cod_ccpp));
                            objENT.depa = Reader.GetString(Ordinal_nom_depa);
                            objENT.prov = Reader.GetString(Ordinal_nom_prov);
                            objENT.dist = Reader.GetString(Ordinal_nom_dist);
                            objENT.CCPP = (Reader.IsDBNull(Ordinal_CCPP) ? "" : Reader.GetString(Ordinal_CCPP));
                            objENT.meta = Reader.GetInt32(Ordinal_meta);
                            objENT.metaRealizada = Reader.GetInt32(Ordinal_metaRealizada);
                            objENT.observacion = Reader.GetString(Ordinal_observacion);
                            objENT.usuario = (Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario));

                            if (Reader.IsDBNull(Ordinal_fecha_update))
                            {
                                objENT.fecha_update = null;
                            }
                            else
                            {
                                objENT.fecha_update = Reader.GetDateTime(Ordinal_fecha_update);
                            }


                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }


        public List<BE_MON_BANDEJA> F_spMON_Ubigeo(int id_proyecto)
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_Ubigeo";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = id_proyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_id = Reader.GetOrdinal("id_ubigeo_proy");
                        int Ordinal_cod_depa = Reader.GetOrdinal("cod_depa");
                        int Ordinal_cod_prov = Reader.GetOrdinal("cod_prov");
                        int Ordinal_cod_dist = Reader.GetOrdinal("cod_dist");
                        int Ordinal_cod_ccpp = Reader.GetOrdinal("cod_ccpp");
                        int Ordinal_nom_depa = Reader.GetOrdinal("nom_depa");
                        int Ordinal_nom_prov = Reader.GetOrdinal("nom_prov");
                        int Ordinal_nom_dist = Reader.GetOrdinal("nom_dist");
                        int Ordinal_CCPP = Reader.GetOrdinal("CCPP");

                        int Ordinal_usuario = Reader.GetOrdinal("usuario");
                        int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.id = Reader.GetInt32(Ordinal_id);
                            objENT.codigo = Reader.GetString(Ordinal_cod_depa) + Reader.GetString(Ordinal_cod_prov) + Reader.GetString(Ordinal_cod_dist);
                            objENT.ubigeoCCPP = (Reader.IsDBNull(Ordinal_cod_ccpp) ? "" : Reader.GetString(Ordinal_cod_ccpp));
                            objENT.depa = Reader.GetString(Ordinal_nom_depa);
                            objENT.prov = Reader.GetString(Ordinal_nom_prov);
                            objENT.dist = Reader.GetString(Ordinal_nom_dist);
                            objENT.CCPP = (Reader.IsDBNull(Ordinal_CCPP) ? "" : Reader.GetString(Ordinal_CCPP));

                            objENT.usuario = (Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario));

                            if (Reader.IsDBNull(Ordinal_fecha_update))
                            {
                                objENT.fecha_update = null;
                            }
                            else
                            {
                                objENT.fecha_update = Reader.GetDateTime(Ordinal_fecha_update);
                            }


                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TamboProyecto(int id_proyecto)
        {

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TamboProyecto";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_id = Reader.GetOrdinal("id_proyectoTambo");
                        int Ordinal_codTambo = Reader.GetOrdinal("idTambo");
                        int Ordinal_NomTambo = Reader.GetOrdinal("tambo");

                        int Ordinal_usuario = Reader.GetOrdinal("usuario");
                        int Ordinal_fecha_update = Reader.GetOrdinal("fecha_update");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.id = Reader.GetInt32(Ordinal_id);

                            objENT.Id_tipo = Reader.GetInt32(Ordinal_codTambo);
                            objENT.tambo = Reader.GetString(Ordinal_NomTambo);

                            objENT.usuario = (Reader.IsDBNull(Ordinal_usuario) ? "" : Reader.GetString(Ordinal_usuario));

                            if (Reader.IsDBNull(Ordinal_fecha_update))
                            {
                                objENT.fecha_update = null;
                            }
                            else
                            {
                                objENT.fecha_update = Reader.GetDateTime(Ordinal_fecha_update);
                            }


                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_ReporteSaneamiento2016(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteSaneamiento2016]");
                objDE.AddInParameter(objComando, "@id_Programa", DbType.Int32, _BE_Bandeja.id_sector);
                objDE.AddInParameter(objComando, "@id_Tipo", DbType.String, _BE_Bandeja.Id_tipo);
                objDE.AddInParameter(objComando, "@estado", DbType.String, _BE_Bandeja.Estado);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteSaneamientoObras(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteSaneamientoObras]");
                objDE.AddInParameter(objComando, "@id_Programa", DbType.Int32, _BE_Bandeja.id_sector);
                objDE.AddInParameter(objComando, "@id_Tipo", DbType.String, _BE_Bandeja.Id_tipo);
                objDE.AddInParameter(objComando, "@estado", DbType.String, _BE_Bandeja.Estado);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        #endregion

        public int spi_MON_AsignarProyecto(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_AsignarProyecto]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Bandeja.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tecnico", DbType.Int32, _BE_Bandeja.id_tecnico);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Bandeja.tipo);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Bandeja.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spuSOL_PreliminarSNIPSolicitudes(BE_MON_BANDEJA _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spuSOL_PreliminarSNIPSolicitudes]");

                objDE.AddInParameter(objComando, "@id_solicitud", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_DocumentoMonitor(BE_MON_BANDEJA _BEBandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_DocumentoMonitor]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BEBandeja.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipo", DbType.Int32, _BEBandeja.Id_tipo);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BEBandeja.NombreArchivo);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BEBandeja.UrlDoc);
                objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BEBandeja.Descripcion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEBandeja.id_usuario);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BEBandeja.Date_fecha);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_DocumentoMonitor(BE_MON_BANDEJA _BEBandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_DocumentoMonitor]");

                objDE.AddInParameter(objComando, "@id_documentoMonitor", DbType.Int32, _BEBandeja.Id_documentoMonitor);
                objDE.AddInParameter(objComando, "@id_tipo", DbType.Int32, _BEBandeja.Id_tipo);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BEBandeja.NombreArchivo);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BEBandeja.UrlDoc);
                objDE.AddInParameter(objComando, "@descripcion", DbType.String, _BEBandeja.Descripcion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BEBandeja.id_usuario);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BEBandeja.Date_fecha);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BEBandeja.tipo);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_InauguracionProyecto(int id_proyecto, int id_tipoInaugura, DateTime fechaInagura, string HoraInicio, string HoraFin, string representanteMVCS, string id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_InauguracionProyecto]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoInaugura", DbType.Int32, id_tipoInaugura);
                objDE.AddInParameter(objComando, "@fechaInagura", DbType.DateTime, fechaInagura);
                objDE.AddInParameter(objComando, "@horaInicio", DbType.String, HoraInicio);
                objDE.AddInParameter(objComando, "@horaFin", DbType.String, HoraFin);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
                objDE.AddInParameter(objComando, "@representanteMVCS", DbType.String, representanteMVCS);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiuSOL_EntregaExpedientes(int id_solicitudes, DateTime fechaEntrega, string observacion, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_EntregaExpedientes]");

                objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, id_solicitudes);
                objDE.AddInParameter(objComando, "@fecha_entrega", DbType.DateTime, fechaEntrega);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, observacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiuSOL_CambioEstado(int id_solicitudes, string id_usuario, int estado)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_CambioEstado]");

                objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, id_solicitudes);
                objDE.AddInParameter(objComando, "@cod_estado", DbType.Int32, estado);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiuSOL_DevolucionSolicitud(BE_MON_BANDEJA _BE)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiuSOL_DevolucionSolicitud]");

                objDE.AddInParameter(objComando, "@id_solicitudes", DbType.Int32, _BE.id_proyecto);
                objDE.AddInParameter(objComando, "@id_motivoDevolucion", DbType.Int32, _BE.id_motivoDevolucion);
                objDE.AddInParameter(objComando, "@id_tipoDevolucion", DbType.Int32, _BE.id_tipoDevolucion);
                objDE.AddInParameter(objComando, "@nroOficio", DbType.String, _BE.nroOficio);
                objDE.AddInParameter(objComando, "@nroTramite", DbType.String, _BE.nroTramite);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int IU_spiu_MON_ProyectoUbigeoCCPP(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_ProyectoUbigeoCCPP", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@CCPP", SqlDbType.VarChar, 10).Value = _BE.CCPP;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@flagActivo", SqlDbType.Int).Value = _BE.flagActivo;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int U_MON_ProyectoBanco(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_ProyectoBanco", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar, 800).Value = _BE.nombProyecto;
                    cmd.Parameters.Add("@periodo", SqlDbType.VarChar, 4).Value = _BE.Anio;
                    cmd.Parameters.Add("@id_ambito", SqlDbType.Int).Value = _BE.ambito;
                    cmd.Parameters.Add("@UE", SqlDbType.VarChar, 300).Value = _BE.unidadEjecutora;
                    cmd.Parameters.Add("@poblacion", SqlDbType.Int).Value = _BE.poblacionSnip;
                    cmd.Parameters.Add("@id_subPrograma", SqlDbType.Int).Value = _BE.id_subPrograma;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@flagReconstruccion", SqlDbType.Int).Value = _BE.flagReconstruccion;
                    cmd.Parameters.Add("@flagServicio", SqlDbType.Int).Value = _BE.flagServicio;
                    cmd.Parameters.Add("@flagEmergencia", SqlDbType.Int).Value = _BE.flagEmergencia;
                    cmd.Parameters.Add("@idTipoInversion", SqlDbType.Int).Value = _BE.idTipoInversion;
                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_UbigeoMetas(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_UbigeoMetas", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@CCPP", SqlDbType.VarChar, 10).Value = _BE.CCPP;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@meta", SqlDbType.Int).Value = _BE.meta;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int UD_spud_MON_UbigeoMetas(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_UbigeoMetas", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUbigeoProyecto", SqlDbType.Int).Value = _BE.id;
                    cmd.Parameters.Add("@CCPP", SqlDbType.VarChar, 10).Value = _BE.CCPP;
                    cmd.Parameters.Add("@meta", SqlDbType.Int).Value = _BE.meta;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.tipo;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int U_MON_UbigeoMetasTerminadas(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_UbigeoMetasTerminadas", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idUbigeoProyecto", SqlDbType.Int).Value = _BE.id;
                    cmd.Parameters.Add("@metaTerminada", SqlDbType.Int).Value = _BE.metaRealizada;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int I_spi_MON_RegistrarProyecto(BE_MON_BANDEJA _BE)
        {
            SqlTransaction trans;
            Int64 id = 0;
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                strCnx.Open();

                trans = strCnx.BeginTransaction();
            
                try
                {
                    SqlCommand cmd;
                 

                    cmd = new SqlCommand("spi_MON_RegistrarProyecto", strCnx, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@snip", SqlDbType.Int).Value = _BE.snip;
                    cmd.Parameters.Add("@cod_subsector", SqlDbType.Int).Value = _BE.sector;
                    cmd.Parameters.Add("@id_tipoSubPrograma", SqlDbType.Int).Value = _BE.Id_tipo;
                    cmd.Parameters.Add("@cod_depa", SqlDbType.Char, 2).Value = _BE.depa;
                    cmd.Parameters.Add("@cod_prov", SqlDbType.Char, 2).Value = _BE.prov;
                    cmd.Parameters.Add("@cod_dist", SqlDbType.Char, 2).Value = _BE.dist;
                    cmd.Parameters.Add("@cod_ccpp", SqlDbType.Char, 4).Value = _BE.CCPP;
                    cmd.Parameters.Add("@unidad_ejec", SqlDbType.VarChar, 250).Value = _BE.unidadEjecutora;
                    cmd.Parameters.Add("@nombre_proyecto", SqlDbType.VarChar, 8000).Value = _BE.nombProyecto;
                    cmd.Parameters.Add("@monto_snip", SqlDbType.Float).Value = _BE.monto_SNIP;

                    cmd.Parameters.Add("@fechaAprobacion", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@id_ambito", SqlDbType.Int).Value = _BE.ambito;
                    cmd.Parameters.Add("@poblacion", SqlDbType.Int).Value = _BE.poblacionSnip;
                    cmd.Parameters.Add("@id_financiamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@id_modalidadFinanciamiento", SqlDbType.Int).Value = _BE.id_tipoModalidad;
                    cmd.Parameters.Add("@periodo", SqlDbType.Int).Value = _BE.Anio;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@flagReconstruccion", SqlDbType.Int).Value = (_BE.flagReconstruccion == false) ? 0 : 1;
                    cmd.Parameters.Add("@flagServicio", SqlDbType.Int).Value = (_BE.flagServicio == false) ? 0 : 1;
                    cmd.Parameters.Add("@flagEmergencia", SqlDbType.Int).Value = _BE.flagEmergencia;
                    cmd.Parameters.Add("@CodigoIdea", SqlDbType.Int).Value = _BE.CodigoIdea;
                    cmd.Parameters.Add("@CodigoUnificado", SqlDbType.Int).Value = _BE.CodigoUnificado;
                    cmd.Parameters.Add("@id_declaratoria", SqlDbType.Int).Value = _BE.id_declaratoria;
                    cmd.Parameters.Add("@Declaratoria", SqlDbType.VarChar).Value = _BE.Declaratoria;
                    cmd.Parameters.Add("@idTipoInversion", SqlDbType.Int).Value = _BE.idTipoInversion;



                    id = Convert.ToInt64(cmd.ExecuteScalar());

                    foreach (Ubigeos UBI in _BE.ListaUbigeos)
                    {
                        SqlCommand cmd2 = new SqlCommand("spiu_MON_ProyectoUbigeoCCPP", strCnx, trans);
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id;
                        cmd2.Parameters.Add("@CCPP", SqlDbType.VarChar, 10).Value = UBI.CCPP;
                        cmd2.Parameters.Add("@idUsuario", SqlDbType.Int).Value = UBI.id_usuario;
                        cmd2.Parameters.Add("@flagActivo", SqlDbType.Int).Value = UBI.flagActivo;
                        cmd2.ExecuteNonQuery();
                    }

                    foreach (CodigosSeace CSE in _BE.ListaCodigosSeace)
                    {
                        SqlCommand cmd2 = new SqlCommand("spi_MON_RegistrarCodigosSeace", strCnx, trans);
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id;
                        cmd2.Parameters.Add("@Codigo", SqlDbType.Int).Value = CSE.Codigo;
                        cmd.Parameters.Add("@idEtapa", SqlDbType.Int).Value = 1;//EJECUCION (OBRA, PERFIL, EXPEDIENTE)
                        cmd2.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                        cmd2.Parameters.Add("@Activo", SqlDbType.Int).Value = CSE.Activo;
                        cmd2.ExecuteNonQuery();
                    }

                    foreach (CodigosARCC CAR in _BE.ListaCodigosARCC)
                    {
                        SqlCommand cmd2 = new SqlCommand("spi_MON_RegistrarCodigosARCC", strCnx, trans);
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id;
                        cmd2.Parameters.Add("@Codigo", SqlDbType.VarChar, 50).Value = CAR.Codigo;
                        cmd2.Parameters.Add("@Activo", SqlDbType.Int).Value = CAR.Activo;
                        cmd2.ExecuteNonQuery();
                    }

                    trans.Commit();
                    return 1;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception(ex.Message);
                }
            }
            
 
        }

        public int spi_MON_RegistrarCodigosSeace(CodigosSeace _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_RegistrarCodigosSeace", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@idEtapa", SqlDbType.Int).Value = _BE.idEtapa;
                    cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = _BE.Codigo;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@Activo", SqlDbType.Int).Value = _BE.Activo;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_spiu_MON_ProyectoTipologia(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_ProyectoTipologia", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@idTipologia", SqlDbType.Int).Value = _BE.tipoTipologia;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@flagActivo", SqlDbType.Int).Value = _BE.flagActivo;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public int U_spu_MON_DatosComplementariosPNSR(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_DatosComplementariosPNSR", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@UTMX", SqlDbType.Decimal).Value = _BE.UTMX;
                    cmd.Parameters.Add("@UTMY", SqlDbType.Decimal).Value = _BE.UTMY;
                    cmd.Parameters.Add("@UTMZ", SqlDbType.Decimal).Value = _BE.UTMZ;
                    cmd.Parameters.Add("@TipoDenominacion", SqlDbType.Int).Value = _BE.tipoDenominacion;
                    cmd.Parameters.Add("@caudalFuente", SqlDbType.Decimal).Value = _BE.caudalFuente;
                    cmd.Parameters.Add("@nroColiformesFuente", SqlDbType.Decimal).Value = _BE.nroColiformesFuente;
                    cmd.Parameters.Add("@resultadoAnalisisFuente", SqlDbType.VarChar, 800).Value = _BE.resultadoAnalisisFuente;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@centro_poblado", SqlDbType.VarChar, 200).Value = _BE.centro_poblado;


                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_TamboProyecto(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_TamboProyecto", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@id_tambo", SqlDbType.Int).Value = _BE.id;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int D_spd_MON_TamboProyecto(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spd_MON_TamboProyecto", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyectoTambo", SqlDbType.Int).Value = _BE.id;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TecnicosInformeNE(int id_proyecto)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TecnicosInformeNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");


                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }

        }

        public List<BE_MON_BANDEJA> F_SOL_validarSNIP(string snip)
        {
            //  Este bloque using cierra y libera automáticamente los recursos utilizados por "conn"
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {

                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();

                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spSOL_validarSNIP";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@SNIP", SqlDbType.VarChar, 10).Value = snip;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int Ordinal_cod_snip = Reader.GetOrdinal("snip");
                        int Ordinal_codUnificado = Reader.GetOrdinal("iCodUnificado");
                        int Ordinal_nom_proyecto = Reader.GetOrdinal("nombre_proyecto");
                        int Ordinal_id_TipoFinanciamiento = Reader.GetOrdinal("tipo");
                        int Ordinal_tipoEstadoEjecucion = Reader.GetOrdinal("estado");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.snip = Reader.GetInt32(Ordinal_cod_snip).ToString();
                            objENT.CodigoUnificado = Reader.GetInt32(Ordinal_codUnificado);
                            objENT.nombProyecto = Reader.GetString(Ordinal_nom_proyecto);
                            objENT.tipo = Reader.GetString(Ordinal_id_TipoFinanciamiento);
                            objENT.tipoEstadoEjecucion = Reader.GetString(Ordinal_tipoEstadoEjecucion);
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public int U_MON_InformacionGeneralJICA(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_InformacionGeneralJICA", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idTipoProyecto", SqlDbType.Int).Value = _BE.Id_tipo;
                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 800).Value = _BE.nombProyecto;
                    cmd.Parameters.Add("@entidadPrestaria", SqlDbType.VarChar, 200).Value = _BE.unidadFormuladora;
                    cmd.Parameters.Add("@unidadEjecutora", SqlDbType.VarChar, 500).Value = _BE.unidadEjecutora;
                    cmd.Parameters.Add("@financiamiento", SqlDbType.VarChar, 20).Value = _BE.Descripcion;
                    cmd.Parameters.Add("@monto", SqlDbType.VarChar, 20).Value = _BE.monto_inversion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public DataSet spMON_ListaProyectosDetalle(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ListaProyectosDetalle]");
                //objDE.AddInParameter(objComando, "@estado", DbType.Int32, _BE_Bandeja.estadoProyecto);
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Bandeja.snip);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, _BE_Bandeja.nombProyecto);

                objDE.AddInParameter(objComando, "@depa", DbType.String, _BE_Bandeja.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, _BE_Bandeja.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, _BE_Bandeja.dist);
                objDE.AddInParameter(objComando, "@sector", DbType.String, _BE_Bandeja.sector);

                return objDE.ExecuteDataSet(objComando);
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int U_MON_AdminitradorReporte(BE_MON_BANDEJA _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_AdminitradorReporte", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@flagEtapa", SqlDbType.Int).Value = _BE.flagEtapa;
                    cmd.Parameters.Add("@id_Etapa", SqlDbType.VarChar, 1).Value = _BE.etapa;
                    cmd.Parameters.Add("@flagSaldo", SqlDbType.Int).Value = _BE.flagSaldo;
                    cmd.Parameters.Add("@flagSaldoObraExpediente", SqlDbType.Int).Value = _BE.flagSaldoObraExpediente;
                    cmd.Parameters.Add("@orden", SqlDbType.VarChar, 2).Value = _BE.orden;
                    cmd.Parameters.Add("@FlagReporte", SqlDbType.Int).Value = _BE.flagActivo;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spSOL_ReporteDemandaSG()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spSOL_ReporteDemandaSG]");
                //objDE.AddInParameter(objComando, "@estado", DbType.Int32, _BE_Bandeja.estadoProyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ReporteIntervencionesSG(string id_programa)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ReporteIntervencionesSG]");
                objDE.AddInParameter(objComando, "@subsector", DbType.String, id_programa);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet pa_rBandejaMonitoreo(ConsultaMonitoreo pConsulta)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);

                DbCommand objComando = objDE.GetStoredProcCommand("[pa_rBandejaMonitoreo]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, pConsulta.snip);
                objDE.AddInParameter(objComando, "@CodUnificado", DbType.String, pConsulta.unico);
                objDE.AddInParameter(objComando, "@proyecto", DbType.String, pConsulta.nomProyecto);
                objDE.AddInParameter(objComando, "@depa", DbType.String, pConsulta.depa);
                objDE.AddInParameter(objComando, "@prov", DbType.String, pConsulta.prov);
                objDE.AddInParameter(objComando, "@dist", DbType.String, pConsulta.dist);
                objDE.AddInParameter(objComando, "@sector", DbType.String, pConsulta.unidad);
                objDE.AddInParameter(objComando, "@tecnico", DbType.String, pConsulta.tecnico);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, pConsulta.etapaProyecto);
                objDE.AddInParameter(objComando, "@estado", DbType.String, pConsulta.nomEstado);
                objDE.AddInParameter(objComando, "@PageNumber", DbType.String, pConsulta.nroPagina);
                objDE.AddInParameter(objComando, "@PageSize", DbType.String, pConsulta.tamanioPagina);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
