using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;

public class DETramite
{
    private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
    #region "consulta"
    public DataSet sp_tramite(string codigo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetSqlStringCommand("select * from (select distinct nroexp,des_documento + ' - ' + documento as documento,apepat,apemat,nombre,razsoc,asunto,dia + '/' + mes + '/' + ano as fecha,tipo_tramite,razsoct from v_detmae where asunto like '%" + codigo + "%')s order by convert(smalldatetime,fecha,103) desc ");
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }

    }

    public DataSet sp_tramiteVerifica(string codigo)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //    DbCommand objComando = objDE.GetSqlStringCommand("select top 10 * from v_detmae");

            DbCommand objComando = objDE.GetSqlStringCommand("select distinct nroexp,(des_documento + ' - ' + documento) as documento,razsoc,razsoct,(dia + '/' + mes + '/' + ano) as fecha, asunto  from v_detmae where nroexp ='" + codigo + "'");
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }

    } 
    #endregion
}