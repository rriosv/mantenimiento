﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


namespace DataAccess
{
    public class DE_MON_Ampliacion
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();


        public DataSet sp_MON_Penalidad(int id_proyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Penalidad]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_Penalidad( int id_proyecto, int iTipoPenalidad, string detalleIncumplimiento, DateTime fechaPenalidad, decimal montoPenalidad, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Penalidad]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, id_proyecto);
                objDE.AddInParameter(objComando, "@iTipoPenalidad", DbType.Int32, iTipoPenalidad);
                objDE.AddInParameter(objComando, "@detalleIncumplimiento", DbType.String, detalleIncumplimiento);
                objDE.AddInParameter(objComando, "@fechaPenalidad", DbType.DateTime, fechaPenalidad);
                objDE.AddInParameter(objComando, "@montoPenalidad", DbType.Decimal, montoPenalidad);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spud_MON_Penalidad(int id_penalidad, int tipo, int iTipoPenalidad, string detalleIncumplimiento, DateTime fechaPenalidad, decimal montoPenalidad, int id_usuario)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Penalidad]");
                objDE.AddInParameter(objComando, "@id_penalidad", DbType.Int32, id_penalidad);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, tipo);
                objDE.AddInParameter(objComando, "@iTipoPenalidad", DbType.Int32, iTipoPenalidad);
                objDE.AddInParameter(objComando, "@detalleIncumplimiento", DbType.String, detalleIncumplimiento);
                objDE.AddInParameter(objComando, "@fechaPenalidad", DbType.DateTime, fechaPenalidad);
                objDE.AddInParameter(objComando, "@montoPenalidad", DbType.Decimal, montoPenalidad);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public DataSet sp_MON_Ampliacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Ampliacion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ampliacion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ampliacion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@id_tipo_ampliacion", DbType.Int32, _BE_Ampliacion.id_tipo_ampliacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_Ampliacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Ampliacion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ampliacion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ampliacion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@id_tipo_ampliacion", DbType.Int32, _BE_Ampliacion.id_tipo_ampliacion);

                objDE.AddInParameter(objComando, "@ampliacion", DbType.String, _BE_Ampliacion.ampliacion);
                objDE.AddInParameter(objComando, "@resolAprob", DbType.String, _BE_Ampliacion.resolAprob);
                objDE.AddInParameter(objComando, "@fechaResolucion", DbType.DateTime, _BE_Ampliacion.Date_fechaResolucion);
                objDE.AddInParameter(objComando, "@concepto", DbType.String, _BE_Ampliacion.concepto);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ampliacion.observacion);

                objDE.AddInParameter(objComando, "@montoPresup", DbType.String, _BE_Ampliacion.montoPresup);
                objDE.AddInParameter(objComando, "@vinculacion", DbType.String, _BE_Ampliacion.vinculacion);
                objDE.AddInParameter(objComando, "@porcentajeIncidencia", DbType.String, _BE_Ampliacion.porcentajeIncidencia);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ampliacion.urlDoc);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ampliacion.id_usuario);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ampliacion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ampliacion.Date_FechaFin);
                objDE.AddInParameter(objComando, "@urlInforme", DbType.String, _BE_Ampliacion.urlInforme);
                objDE.AddInParameter(objComando, "@urlOficio", DbType.String, _BE_Ampliacion.urlOficio);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_AmpliacionPE(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_AmpliacionPE]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ampliacion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ampliacion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@id_tipo_ampliacion", DbType.Int32, _BE_Ampliacion.id_tipo_ampliacion);

                objDE.AddInParameter(objComando, "@ampliacion", DbType.String, _BE_Ampliacion.ampliacion);
                objDE.AddInParameter(objComando, "@resolAprob", DbType.String, _BE_Ampliacion.resolAprob);
                objDE.AddInParameter(objComando, "@fechaResolucion", DbType.DateTime, _BE_Ampliacion.Date_fechaResolucion);
                objDE.AddInParameter(objComando, "@concepto", DbType.String, _BE_Ampliacion.concepto);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ampliacion.observacion);

                objDE.AddInParameter(objComando, "@id_tipoMoneda", DbType.Int32, _BE_Ampliacion.id_tipoMoneda);
                objDE.AddInParameter(objComando, "@montoPresup", DbType.String, _BE_Ampliacion.montoPresup);
                objDE.AddInParameter(objComando, "@vinculacion", DbType.String, _BE_Ampliacion.vinculacion);
                objDE.AddInParameter(objComando, "@porcentajeIncidencia", DbType.String, _BE_Ampliacion.porcentajeIncidencia);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ampliacion.urlDoc);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ampliacion.id_usuario);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ampliacion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ampliacion.Date_FechaFin);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int D_MON_Ampliacion(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spd_MON_Ampliacion", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_ampliacion", SqlDbType.Int).Value = _BE.id_ampliacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int spud_MON_Seguimiento_Ampliacion_Editar(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Ampliacion_Editar]");

                objDE.AddInParameter(objComando, "@id_ampliacion", DbType.Int32, _BE_Ampliacion.id_ampliacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ampliacion.id_usuario);

                objDE.AddInParameter(objComando, "@ampliacion", DbType.String, _BE_Ampliacion.ampliacion);
                objDE.AddInParameter(objComando, "@resolAprob", DbType.String, _BE_Ampliacion.resolAprob);
                objDE.AddInParameter(objComando, "@fechaResolucion", DbType.DateTime, _BE_Ampliacion.Date_fechaResolucion);
                objDE.AddInParameter(objComando, "@concepto", DbType.String, _BE_Ampliacion.concepto);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ampliacion.observacion);

                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ampliacion.urlDoc);
                objDE.AddInParameter(objComando, "@urlInforme", DbType.String, _BE_Ampliacion.urlInforme);
                objDE.AddInParameter(objComando, "@urlOficio", DbType.String, _BE_Ampliacion.urlOficio);
                
                

                objDE.AddInParameter(objComando, "@fechaInico", DbType.DateTime, _BE_Ampliacion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ampliacion.Date_FechaFin);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar]");

                objDE.AddInParameter(objComando, "@id_ampliacion", DbType.Int32, _BE_Ampliacion.id_ampliacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ampliacion.id_usuario);
                objDE.AddInParameter(objComando, "@montoPresup", DbType.String, _BE_Ampliacion.montoPresup);
                objDE.AddInParameter(objComando, "@vinculacion", DbType.String, _BE_Ampliacion.vinculacion);
                objDE.AddInParameter(objComando, "@resolAprob", DbType.String, _BE_Ampliacion.resolAprob);
                objDE.AddInParameter(objComando, "@fechaResolucion", DbType.DateTime, _BE_Ampliacion.Date_fechaResolucion);
                objDE.AddInParameter(objComando, "@porcentajeIncidencia", DbType.String, _BE_Ampliacion.porcentajeIncidencia);
                objDE.AddInParameter(objComando, "@concepto", DbType.String, _BE_Ampliacion.concepto);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ampliacion.observacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ampliacion.urlDoc);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Ampliacion_Deductivo_Editar]");

                objDE.AddInParameter(objComando, "@id_ampliacion", DbType.Int32, _BE_Ampliacion.id_ampliacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ampliacion.id_usuario);
                objDE.AddInParameter(objComando, "@montoPresup", DbType.String, _BE_Ampliacion.montoPresup);
                objDE.AddInParameter(objComando, "@vinculacion", DbType.String, _BE_Ampliacion.vinculacion);
                objDE.AddInParameter(objComando, "@resolAprob", DbType.String, _BE_Ampliacion.resolAprob);
                objDE.AddInParameter(objComando, "@porcentajeIncidencia", DbType.String, _BE_Ampliacion.porcentajeIncidencia);
                objDE.AddInParameter(objComando, "@concepto", DbType.String, _BE_Ampliacion.concepto);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ampliacion.observacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ampliacion.urlDoc);
                objDE.AddInParameter(objComando, "@fechaResolucion", DbType.DateTime, _BE_Ampliacion.Date_fechaResolucion);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_AmpliacionPE(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_AmpliacionPE]");

                objDE.AddInParameter(objComando, "@id_ampliacion", DbType.Int32, _BE_Ampliacion.id_ampliacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ampliacion.id_usuario);
                objDE.AddInParameter(objComando, "@id_tipoMoneda", DbType.Int32, _BE_Ampliacion.id_tipoMoneda);
                objDE.AddInParameter(objComando, "@montoPresup", DbType.String, _BE_Ampliacion.montoPresup);
                objDE.AddInParameter(objComando, "@vinculacion", DbType.String, _BE_Ampliacion.vinculacion);
                objDE.AddInParameter(objComando, "@resolAprob", DbType.String, _BE_Ampliacion.resolAprob);
                objDE.AddInParameter(objComando, "@porcentajeIncidencia", DbType.String, _BE_Ampliacion.porcentajeIncidencia);
                objDE.AddInParameter(objComando, "@concepto", DbType.String, _BE_Ampliacion.concepto);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ampliacion.observacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ampliacion.urlDoc);
                objDE.AddInParameter(objComando, "@fechaResolucion", DbType.DateTime, _BE_Ampliacion.Date_fechaResolucion);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_MON_AmpliacionPlazoPorContrata(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_AmpliacionPlazoPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int, 2).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@fechaCuadernoObra", SqlDbType.DateTime).Value = _BE.Date_fechaCuadernoObra;
                    cmd.Parameters.Add("@concepto", SqlDbType.VarChar, 200).Value = _BE.concepto;
                    cmd.Parameters.Add("@ampliacion", SqlDbType.Int).Value = _BE.ampliacion;
                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@fechaInicio", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    cmd.Parameters.Add("@fechaFin", SqlDbType.DateTime).Value = _BE.Date_FechaFin;
                    cmd.Parameters.Add("@fechaSolicitudContratista", SqlDbType.DateTime).Value = _BE.Date_fechaSolicitudContratista;
                    cmd.Parameters.Add("@fechaPresentacionInforme", SqlDbType.DateTime).Value = _BE.Date_fechaPresentacionInforme;
                    cmd.Parameters.Add("@resolAprob", SqlDbType.VarChar, 200).Value = _BE.resolAprob;
                    cmd.Parameters.Add("@fechaResolucion", SqlDbType.DateTime).Value = _BE.Date_fechaResolucion;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int U_MON_AmpliacionPlazoPorContrata(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_AmpliacionPlazoPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_ampliacion", SqlDbType.Int).Value = _BE.id_ampliacion;

                    cmd.Parameters.Add("@fechaCuadernoObra", SqlDbType.DateTime).Value = _BE.Date_fechaCuadernoObra;
                    cmd.Parameters.Add("@concepto", SqlDbType.VarChar, 200).Value = _BE.concepto;
                    cmd.Parameters.Add("@ampliacion", SqlDbType.Int).Value = _BE.ampliacion;
                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@fechaInicio", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    cmd.Parameters.Add("@fechaFin", SqlDbType.DateTime).Value = _BE.Date_FechaFin;
                    cmd.Parameters.Add("@fechaSolicitudContratista", SqlDbType.DateTime).Value = _BE.Date_fechaSolicitudContratista;
                    cmd.Parameters.Add("@fechaPresentacionInforme", SqlDbType.DateTime).Value = _BE.Date_fechaPresentacionInforme;
                    cmd.Parameters.Add("@resolAprob", SqlDbType.VarChar, 200).Value = _BE.resolAprob;
                    cmd.Parameters.Add("@fechaResolucion", SqlDbType.DateTime).Value = _BE.Date_fechaResolucion;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_MON_Adendas(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_Adendas", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@fechaAdenda", SqlDbType.DateTime).Value = _BE.Date_fechaAdenda;
                    cmd.Parameters.Add("@concepto", SqlDbType.VarChar, 200).Value = _BE.concepto;

                    cmd.Parameters.Add("@resolAprob", SqlDbType.VarChar, 200).Value = _BE.resolAprob;
                    cmd.Parameters.Add("@fechaResolucion", SqlDbType.DateTime).Value = _BE.Date_fechaResolucion;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_MON_Adendas(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_Adendas", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_ampliacion", SqlDbType.Int).Value = _BE.id_ampliacion;

                    cmd.Parameters.Add("@fechaAdenda", SqlDbType.DateTime).Value = _BE.Date_fechaAdenda;
                    cmd.Parameters.Add("@concepto", SqlDbType.VarChar, 200).Value = _BE.concepto;

                    cmd.Parameters.Add("@resolAprob", SqlDbType.VarChar, 200).Value = _BE.resolAprob;
                    cmd.Parameters.Add("@fechaResolucion", SqlDbType.DateTime).Value = _BE.Date_fechaResolucion;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.tipo;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ampliacion> F_MON_CertificacionAutorizacionPorContrata(BE_MON_Ampliacion _BE)
        {
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);

            BE_MON_Ampliacion objENT = new BE_MON_Ampliacion();
            List<BE_MON_Ampliacion> objCollection = new List<BE_MON_Ampliacion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_DetalleCertificacionAutorizacionPorContrata";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ampliacion();

                        objENT.id_tabla = Convert.ToInt32(Reader["id_detalleCertificacionAutorizacion"].ToString());
                        objENT.concepto = (Reader["tipo"].ToString());

                        objENT.entidad = Reader["entidadEmite"].ToString();
                        objENT.Date_fechaPresentacionInforme = Convert.ToDateTime(Reader["fechaAprobacion"].ToString());


                        objENT.urlDoc = Reader["urlDocumentoSustento"].ToString();

                        objENT.usuario = Reader["usuario"].ToString();
                        objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int I_MON_DetalleCertificacionAutorizacionPorContrata(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_DetalleCertificacionAutorizacionPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@tipo", SqlDbType.VarChar, 50).Value = _BE.concepto;

                    cmd.Parameters.Add("@entidadEmite", SqlDbType.VarChar, 100).Value = _BE.entidad;
                    cmd.Parameters.Add("@fechaAprobacionEmision", SqlDbType.DateTime).Value = _BE.Date_fechaPresentacionInforme;

                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_MON_DetalleCertificacionAutorizacionPorContrata(BE_MON_Ampliacion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_DetalleCertificacionAutorizacionPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.id_tabla;

                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.tipo;
                    cmd.Parameters.Add("@tipoCertificacion", SqlDbType.VarChar, 50).Value = _BE.concepto;

                    cmd.Parameters.Add("@entidadEmite", SqlDbType.VarChar, 100).Value = _BE.entidad;
                    cmd.Parameters.Add("@fechaAprobacionEmision", SqlDbType.DateTime).Value = _BE.Date_fechaPresentacionInforme;

                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public int spi_Detalle_Paralizacion(BE_MON_Ampliacion _BE_MON_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spi_Acciones_Registrar");

                objDE.AddInParameter(objComando, "@id_detalleParalizacion", DbType.Int64, _BE_MON_Ampliacion.id_detalleParalizacion);
                objDE.AddInParameter(objComando, "@id_ampliaciones", DbType.Int32, _BE_MON_Ampliacion.id_ampliaciones);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_MON_Ampliacion.fecha);
                objDE.AddInParameter(objComando, "@accion", DbType.String, _BE_MON_Ampliacion.accion);
                objDE.AddInParameter(objComando, "@usr_registro", DbType.Int32, _BE_MON_Ampliacion.usr_registro);
                objDE.AddInParameter(objComando, "@usr_update", DbType.Int32, _BE_MON_Ampliacion.usr_update);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public DataSet spMON_T_Detalle_Paralizacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_T_Detalle_Paralizacion]");
                objDE.AddInParameter(objComando, "@id_ampliacion", DbType.Int32, _BE_Ampliacion.id_ampliacion);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }



        public int spu_MON_Detalle_Paralizacion(BE_MON_Ampliacion _BE_MON_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_Detalle_Paralizacion]");

                objDE.AddInParameter(objComando, "@id_detalleParalizacion", DbType.Int32, _BE_MON_Ampliacion.id_detalleParalizacion);
                objDE.AddInParameter(objComando, "@usr_registro", DbType.Int32, _BE_MON_Ampliacion.usr_registro);
                objDE.AddInParameter(objComando, "@flagayuda", DbType.Int32, _BE_MON_Ampliacion.flag_ayuda);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int spu_MON_Elim_Detalle_Paralizacion(BE_MON_Ampliacion _BE_MON_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_Elim_Detalle_Paralizacion]");

                objDE.AddInParameter(objComando, "@id_detalleParalizacion", DbType.Int32, _BE_MON_Ampliacion.id_detalleParalizacion);
                objDE.AddInParameter(objComando, "@usr_registro", DbType.Int32, _BE_MON_Ampliacion.usr_registro);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int spud_MON_Detalle_Paralizacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Detalle_Paralizacion]");

                objDE.AddInParameter(objComando, "@id_detalleParalizacion", DbType.Int32, _BE_Ampliacion.id_detalleParalizacion);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ampliacion.fecha);
                objDE.AddInParameter(objComando, "@accion", DbType.String, _BE_Ampliacion.accion);
                objDE.AddInParameter(objComando, "@usr_update", DbType.String, _BE_Ampliacion.usr_update);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public DataSet spMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_AvanceFisicoFoto]");
                objDE.AddInParameter(objComando, "@id_avance_fisico", DbType.Int32, _BE_Ampliacion.id_avance_fisico);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int spiMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiMON_AvanceFisicoFoto]");

                objDE.AddInParameter(objComando, "@id_avance_fisico", DbType.Int32, _BE_Ampliacion.id_avance_fisico);
                objDE.AddInParameter(objComando, "@v_nombre", DbType.String, _BE_Ampliacion.v_archivo);
                objDE.AddInParameter(objComando, "@v_archivo", DbType.String, _BE_Ampliacion.v_archivo);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ampliacion.fecha);
                objDE.AddInParameter(objComando, "@usr_registro", DbType.Int32, _BE_Ampliacion.usr_registro);
                objDE.AddInParameter(objComando, "@usr_update", DbType.Int32, _BE_Ampliacion.usr_update);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int spuMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spuMON_AvanceFisicoFoto]");

                objDE.AddInParameter(objComando, "@id_avance_fisico_foto", DbType.Int32, _BE_Ampliacion.id_avance_fisico_foto);
                objDE.AddInParameter(objComando, "@v_nombre", DbType.String, _BE_Ampliacion.v_archivo);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ampliacion.fecha);
                objDE.AddInParameter(objComando, "@v_archivo", DbType.String, _BE_Ampliacion.v_archivo);
                objDE.AddInParameter(objComando, "@usr_update", DbType.Int32, _BE_Ampliacion.usr_update);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int speMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spdMON_AvanceFisicoFoto]");

                objDE.AddInParameter(objComando, "@id_avance_fisico_foto", DbType.Int32, _BE_Ampliacion.id_avance_fisico_foto);
                objDE.AddInParameter(objComando, "@usr_update", DbType.Int32, _BE_Ampliacion.usr_update);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
