﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public  class DEAccionesDiarias
   {
      private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

         public DataSet sp_CalendarioAccionesDiarias(string strSNIP)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[sp_CalendarioAccionesDiarias]");
                 objDE.AddInParameter(objComando, "@snip", DbType.String, strSNIP);
                 
                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }


         public DataSet sp_lista_TipoComunicacion()
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[sp_lista_TipoComunicacion]");
                 //objDE.AddInParameter(objComando, "@snip", DbType.String, strSNIP);

                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
      
         public DataSet sp_obtiene_InfoBusqueda(string strSNIP, string fecha)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[sp_obtiene_CalendarioAccionesDiarias]");
                 objDE.AddInParameter(objComando, "@snip", DbType.String, strSNIP);
                 objDE.AddInParameter(objComando, "@fecha", DbType.String, fecha);
                 objDE.AddInParameter(objComando, "@tipo", DbType.String, '1');
                 
                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public DataSet sp_obtiene_ComunicacionesRealiazadas(string strSNIP, string fecha)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[sp_obtiene_CalendarioAccionesDiarias]");
                 objDE.AddInParameter(objComando, "@snip", DbType.String, strSNIP);
                 objDE.AddInParameter(objComando, "@fecha", DbType.String, fecha);
                 objDE.AddInParameter(objComando, "@tipo", DbType.String, '2');

                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }
      
         public int spi_AccionesDiarias(string snip, string fecha, string observacion, string correo, string telef, string oficio, string reu, string otros, int user,string id_solicitud, string flagAyudaMemoria)
         {
             int result;
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spi_AccionesDiarias");
                objDE.AddInParameter(objComando, "@snip", DbType.String, snip);
                objDE.AddInParameter(objComando, "@fecha", DbType.String, fecha);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, observacion);
                objDE.AddInParameter(objComando, "@correo", DbType.String, correo);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, telef);
                objDE.AddInParameter(objComando, "@oficio", DbType.String, oficio);
                objDE.AddInParameter(objComando, "@reu", DbType.String, reu);
                objDE.AddInParameter(objComando, "@otros", DbType.String, otros);
                objDE.AddInParameter(objComando, "@user", DbType.String, user);
                objDE.AddInParameter(objComando, "@id_solicitud", DbType.String, id_solicitud);
                objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, flagAyudaMemoria);


                result = objDE.ExecuteNonQuery(objComando);

                return result;

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
         }

         public DataSet sp_ValidaNombreAcciones(string snip)
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[sp_ValidaNombreAcciones]");
                 objDE.AddInParameter(objComando, "@snip", DbType.String, snip);
                              
                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

         public DataSet spSOL_ReporteListaDepartamento()
         {
             try
             {
                 SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                 DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[spSOL_ReporteListaDepartamento]");
                 //objDE.AddInParameter(objComando, "@snip", DbType.String, strSNIP);

                 return objDE.ExecuteDataSet(objComando);

             }
             catch (DbException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }


         //public DataSet sp_Acciones(string strSNIP)
         //{
         //    try
         //    {
         //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
         //        DbCommand objComando = objDE.GetStoredProcCommand("[dbo].[sp_Acciones]");
         //       objDE.AddInParameter(objComando, "@snip", DbType.String, strSNIP);

         //        return objDE.ExecuteDataSet(objComando);

         //    }
         //    catch (DbException ex)
         //    {
         //        throw ex;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {

         //    }
         //}

        public List<BE_MON_BANDEJA> F_SOL_Acciones(string id_solicitud)
        {
            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_Acciones";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@snip", SqlDbType.Int).Value = id_solicitud;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int Ordinal_fecha = Reader.GetOrdinal("FECHA_OBSERVACION");
                        int Ordinal_observacion = Reader.GetOrdinal("OBSERVACION");
                        //int Ordinal_tipoComunicacion = Reader.GetOrdinal("TIPO_COMUNICACION");
                        int Ordinal_correo = Reader.GetOrdinal("CORREO");
                        int Ordinal_oficio = Reader.GetOrdinal("OFICIO");
                        int Ordinal_reunion = Reader.GetOrdinal("REUNION");
                        int Ordinal_telefono = Reader.GetOrdinal("TELEFONO");
                        int Ordinal_otros = Reader.GetOrdinal("OTROS");
                        int Ordinal_FlagAyuda = Reader.GetOrdinal("FlagAyuda");
                        

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();
                            objENT.strFechaFinReal = Reader.GetString(Ordinal_fecha);
                            objENT.Date_fecha = Convert.ToDateTime(Reader.GetString(Ordinal_fecha));
                            objENT.observacion = Reader.GetString(Ordinal_observacion);
                            //objENT.tipo = Reader.GetString(Ordinal_tipoComunicacion);
                            objENT.correo = Reader.GetString(Ordinal_correo);
                            objENT.nroOficio = Reader.GetString(Ordinal_oficio);
                            objENT.representanteMVCS = Reader.GetString(Ordinal_reunion);
                            objENT.nroTramite = Reader.GetString(Ordinal_telefono);
                            objENT.Estado = Reader.GetString(Ordinal_otros);
                            objENT.flagActivo = Reader.GetInt32(Ordinal_FlagAyuda);

                            objCollection.Add(objENT);
                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public int spdSOL_Acciones(string SNIP, string fecha, int user)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spdSOL_Acciones]");

            objDE.AddInParameter(objComando, "@snip", DbType.String, SNIP);
            objDE.AddInParameter(objComando, "@fecha", DbType.String, fecha);
            objDE.AddInParameter(objComando, "@user", DbType.Int32, user);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }
    }


}
