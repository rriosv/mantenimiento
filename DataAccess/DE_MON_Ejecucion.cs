﻿using System;
using System.Collections.Generic;
using System.Data;
using Entity;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using DataAccess.MappingType;

namespace DataAccess
{
    public class DE_MON_Ejecucion
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public DataSet spMON_EstadoEjecuccion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EstadoEjecuccion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_DetalleEstadoEjecuccionPE35(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_DetalleEstadoEjecuccionPE35]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_EjecucionSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EjecucionSupervision]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        //public List<BE_MON_BANDEJA> F_spMON_EstadoEjecuccion(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        //    BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
        //    List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
        //    SqlCommand cmd = default(SqlCommand);
        //    try
        //    {
        //        cmd = new SqlCommand();
        //        cmd.Connection = Cnx;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "spMON_EstadoEjecuccion";
        //        cmd.CommandTimeout = 36000;
        //        cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE_Ejecucion.id_proyecto;
        //        cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE_Ejecucion.tipoFinanciamiento;
        //        cmd.Connection.Open();
        //        using (SqlDataReader Reader = cmd.ExecuteReader())
        //        {

        //            int valor = Reader.GetOrdinal("valor");
        //            int nombre = Reader.GetOrdinal("nombre");

        //            while (Reader.Read())
        //            {
        //                objENT = new BE_MON_BANDEJA();

        //                objENT.valor = Reader.GetInt32(valor);
        //                objENT.nombre = Reader.GetString(nombre);

        //                objCollection.Add(objENT);

        //                objENT = null;
        //            }
        //        }
        //        return objCollection;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        cmd = null;
        //        if (Cnx.State == ConnectionState.Open)
        //        {
        //            Cnx.Close();
        //        }
        //    }

        //}            

        public List<BE_MON_BANDEJA> F_spMON_TipoSubEstadoEjecucion(int estado)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoSubEstadoEjecucion";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@TipoEstadoEjecucion", SqlDbType.Int).Value = estado;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoSubEstado2(int pIdSubEstado)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoSubEstado2";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@idTipoSubEstado", SqlDbType.Int).Value = pIdSubEstado;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoSubEstadoParalizado(int piEstadoParalizado)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoSubEstadoParalizado";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_TipoEstadoParalizado", SqlDbType.Int).Value = piEstadoParalizado;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoEjecucion(int tipoProyecto, int tipoModalidadFinanciamiento)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoEstadoEjecucion";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = tipoProyecto;
                cmd.Parameters.Add("@TipoModalidadFinanciamiento", SqlDbType.Int).Value = tipoModalidadFinanciamiento;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoResultado()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoResultado";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoValorizacion()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoValorizacion";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTipoMonitoreo()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ListarTipoMonitoreo";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("id_tipomonitoreo");
                    int nombre = Reader.GetOrdinal("nom_tipomonitoreo");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_ListarSubTipoMonitoreo(int iTipoMonitoreo)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ListarSubTipoMonitoreo";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_tipoMonitoreo", SqlDbType.Int).Value = iTipoMonitoreo;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoAsignacionAgente()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoEstadoAsignacionAgente";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public List<BE_MON_BANDEJA> F_spMON_ListarTipoDocumentoEvaluacionRecomendacion()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ListarTipoDocumentoEvaluacionRecomendacion";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("id_tipodocumento");
                    int nombre = Reader.GetOrdinal("nom_tipodocumento");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public DataSet spMON_AvanceFisico(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_AvanceFisico]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_AvanceFisicoAdicional(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_AvanceFisicoAdicional]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_Plazos(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Plazos]");
                objDE.AddInParameter(objComando, "@id_plazo", DbType.Int32, _BE_Ejecucion.Id_empleado_obra);
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@fecha", SqlDbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@estado", DbType.String, _BE_Ejecucion.estado);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        //public DataSet spMON_Plazos(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Plazos]");

        //        objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);

        //        return objDE.ExecuteDataSet(objComando);
        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        public List<BE_MON_Ejecucion> F_spMON_Plazos(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_Plazos";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.Id_empleado_obra = Convert.ToInt32(Reader["id_PlazosEjecucion"].ToString());
                        objENT.tipoFinanciamiento = Convert.ToInt32(Reader["Plazos"].ToString());
                        objENT.Date_fecha = Convert.ToDateTime(Reader["Fecha"].ToString());
                        objENT.estado = Convert.ToBoolean(Reader["Estado"].ToString());
                        objENT.id_proyecto = Convert.ToInt32(Reader["id_Proyecto"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int spi_MON_AvanceFisico(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_AvanceFisico]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@informe", DbType.String, _BE_Ejecucion.informe);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@avance", DbType.String, _BE_Ejecucion.avance);
                objDE.AddInParameter(objComando, "@situacion", DbType.String, _BE_Ejecucion.situacion);
                objDE.AddInParameter(objComando, "@accionesPrevistas", DbType.String, _BE_Ejecucion.accionesPorRealizar);
                objDE.AddInParameter(objComando, "@accionesRealizadas", DbType.String, _BE_Ejecucion.accionesRealizadas);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@fisicoReal", DbType.String, _BE_Ejecucion.fisicoReal);
                objDE.AddInParameter(objComando, "@fisicoProgramado", DbType.String, _BE_Ejecucion.fisicoProgramado);
                objDE.AddInParameter(objComando, "@financieroReal", DbType.String, _BE_Ejecucion.financieroReal);
                objDE.AddInParameter(objComando, "@financieroProgramado", DbType.String, _BE_Ejecucion.financieroProgramado);
                objDE.AddInParameter(objComando, "@estadoSituacion", DbType.Int32, _BE_Ejecucion.estadoSituacional);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ejecucion.observacion);

                objDE.AddInParameter(objComando, "@tipoValorizacion", DbType.String, _BE_Ejecucion.id_tipoValorizacion);
                objDE.AddInParameter(objComando, "@NroValorizacion", DbType.String, _BE_Ejecucion.NroAdicional);

                objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, _BE_Ejecucion.flagAyuda);

                objDE.AddInParameter(objComando, "@id_tipomonitoreo", DbType.Int32, _BE_Ejecucion.id_tipomonitoreo);
                objDE.AddInParameter(objComando, "@id_tipoSubMonitoreo", DbType.Int32, _BE_Ejecucion.id_tipoSubMonitoreo);
                objDE.AddInParameter(objComando, "@idEstado", DbType.Int32, _BE_Ejecucion.id_tipoEstadoEjecuccion);
                objDE.AddInParameter(objComando, "@idSubEstado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoEjecucion);
                //objDE.AddInParameter(objComando, "@idSubEstado2", DbType.String, _BE_Ejecucion.id_tipoEstadoParalizado);
                objDE.AddInParameter(objComando, "@urlActa", DbType.String, _BE_Ejecucion.UrlActa);
                objDE.AddInParameter(objComando, "@urlInforme", DbType.String, _BE_Ejecucion.UrlInforme);
                objDE.AddInParameter(objComando, "@urlOficio", DbType.String, _BE_Ejecucion.UrlOficio);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_EmpleadoObraExp(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EmpleadoObraExp]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_EmpleadoObra(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EmpleadoObra]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_EmpleadoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EmpleadoParalizacion]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);
                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_EmpleadoObraSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EmpleadoObraSupervision]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_EmpleadoObraPre(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EmpleadoObraPre]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoCertificacion()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoCertificacion";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public List<BE_MON_BANDEJA> F_spMON_TipoCartaFianza()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoCartaFianza";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_BANDEJA> F_spMON_TipoAccionCarta()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoAccionCarta";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoSituacional()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoEstadoSituacional";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }


        public int spi_MON_EmpleadoObra(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EmpleadoObra]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_EmpleadoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spi_MON_EmpleadoParalizacion");

                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_EmpleadoObraSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EmpleadoObraSupervision]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spud_MON_EmpleadoObra(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_EmpleadoObra]");

                objDE.AddInParameter(objComando, "@id_empleado_Obra", DbType.Int32, _BE_Ejecucion.Id_empleado_obra);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                // objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Ejecucion.Tipo);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_EmpleadoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_EmpleadoParalizacion]");

                objDE.AddInParameter(objComando, "@id_empleadoParalizacion", DbType.Int32, _BE_Ejecucion.Id_empleado_obra);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                // objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_Ejecucion.Tipo);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);




                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_EmpleadoObraPre(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EmpleadoObraPre]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);




                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_EmpleadoObraExp(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EmpleadoObraExp]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);
                objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@cap", DbType.String, _BE_Ejecucion.cap);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);




                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spu_MON_EstadoProyecto(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_EstadoProyecto]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_estadoProyecto", DbType.String, _BE_Ejecucion.id_estadoProyecto);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spu_MON_EstadoEjecucion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_EstadoEjecucion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_estadoProyecto", DbType.String, _BE_Ejecucion.id_tipoEstadoEjecuccion);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_CartaFianza(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_CartaFianza]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_CartaFianzaSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_CartaFianzaSupervision]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_CartaFianza(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_CartaFianza]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@entidadFinanciera", DbType.String, _BE_Ejecucion.entidadFinanciera);

                objDE.AddInParameter(objComando, "@nroCarta", DbType.String, _BE_Ejecucion.nroCarta);
                objDE.AddInParameter(objComando, "@tipoCarta", DbType.Int32, _BE_Ejecucion.tipoCarta);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaRenovacion", DbType.DateTime, _BE_Ejecucion.Date_fechaRenovacion);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@porcentaje", DbType.String, _BE_Ejecucion.porcentaje);

                objDE.AddInParameter(objComando, "@verificado", DbType.String, _BE_Ejecucion.flagVerificado);
                //objDE.AddInParameter(objComando, "@renovado", DbType.String, _BE_Ejecucion.flagRenovado);

                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@urlDocAccion", DbType.String, _BE_Ejecucion.urlDocAccion);
                objDE.AddInParameter(objComando, "@idTipoAccion", DbType.String, _BE_Ejecucion.idTipoAccion);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_CartaFianzaSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_CartaFianzaSupervision]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@entidadFinanciera", DbType.String, _BE_Ejecucion.entidadFinanciera);

                objDE.AddInParameter(objComando, "@nroCarta", DbType.String, _BE_Ejecucion.nroCarta);
                objDE.AddInParameter(objComando, "@tipoCarta", DbType.Int32, _BE_Ejecucion.tipoCarta);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaRenovacion", DbType.DateTime, _BE_Ejecucion.Date_fechaRenovacion);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@porcentaje", DbType.String, _BE_Ejecucion.porcentaje);

                objDE.AddInParameter(objComando, "@verificado", DbType.String, _BE_Ejecucion.flagVerificado);
                //objDE.AddInParameter(objComando, "@renovado", DbType.String, _BE_Ejecucion.flagRenovado);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@urlDocAccion", DbType.String, _BE_Ejecucion.urlDocAccion);
                objDE.AddInParameter(objComando, "@idTipoAccion", DbType.String, _BE_Ejecucion.idTipoAccion);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_CartaFianza(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_CartaFianza]");

                objDE.AddInParameter(objComando, "@id_tabla", DbType.Int32, _BE_Ejecucion.id_tabla);
                objDE.AddInParameter(objComando, "@entidadFinanciera", DbType.String, _BE_Ejecucion.entidadFinanciera);

                objDE.AddInParameter(objComando, "@nroCarta", DbType.String, _BE_Ejecucion.nroCarta);
                objDE.AddInParameter(objComando, "@tipoCarta", DbType.Int32, _BE_Ejecucion.tipoCarta);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaRenovacion", DbType.DateTime, _BE_Ejecucion.Date_fechaRenovacion);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@porcentaje", DbType.String, _BE_Ejecucion.porcentaje);

                objDE.AddInParameter(objComando, "@verificado", DbType.String, _BE_Ejecucion.flagVerificado);
                objDE.AddInParameter(objComando, "@renovado", DbType.String, _BE_Ejecucion.flagRenovado);

                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_CartaFianza_FlagVerifica(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_CartaFianza_FlagVerifica]");

                objDE.AddInParameter(objComando, "@id_tabla", DbType.Int32, _BE_Ejecucion.id_tabla);
                objDE.AddInParameter(objComando, "@verificado", DbType.String, _BE_Ejecucion.flagVerificado);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ObtenerVariablesPNSU(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ObtenerVariablesPNSU]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Ejecucion.snip);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_ObtenerVariablesPMIB(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ObtenerVariablesPMIB]");
                objDE.AddInParameter(objComando, "@snip", DbType.String, _BE_Ejecucion.snip);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_ComponentesPNSU(BE_MON_VarPNSU _BE_VarPNSU)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_ComponentesPNSU]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_VarPNSU.Id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_VarPNSU.TipoFinanciamiento);
                objDE.AddInParameter(objComando, "@a_captacion", DbType.Int32, _BE_VarPNSU.A_captacion);
                objDE.AddInParameter(objComando, "@a_trata", DbType.Int32, _BE_VarPNSU.A_trata);
                objDE.AddInParameter(objComando, "@a_conduccion", DbType.Int32, _BE_VarPNSU.A_conduccion);
                objDE.AddInParameter(objComando, "@a_reservorio", DbType.Int32, _BE_VarPNSU.A_reservorio);
                objDE.AddInParameter(objComando, "@a_Aduccion", DbType.Int32, _BE_VarPNSU.A_Aduccion);
                objDE.AddInParameter(objComando, "@a_redes", DbType.Int32, _BE_VarPNSU.A_redes);
                objDE.AddInParameter(objComando, "@a_conexion_nueva", DbType.Int32, _BE_VarPNSU.A_conexion_nueva);
                objDE.AddInParameter(objComando, "@a_conexion_rehab", DbType.Int32, _BE_VarPNSU.A_conexion_rehab);
                objDE.AddInParameter(objComando, "@a_conexion_piletas", DbType.Int32, _BE_VarPNSU.A_conexion_piletas);
                objDE.AddInParameter(objComando, "@a_impulsion", DbType.Int32, _BE_VarPNSU.A_impulsion);
                objDE.AddInParameter(objComando, "@a_estacion", DbType.Int32, _BE_VarPNSU.A_estacion);

                objDE.AddInParameter(objComando, "@s_efluente", DbType.Int32, _BE_VarPNSU.S_efluente);
                objDE.AddInParameter(objComando, "@s_camara", DbType.Int32, _BE_VarPNSU.S_camara);
                objDE.AddInParameter(objComando, "@s_emisor", DbType.Int32, _BE_VarPNSU.S_emisor);
                objDE.AddInParameter(objComando, "@s_planta", DbType.Int32, _BE_VarPNSU.S_planta);
                objDE.AddInParameter(objComando, "@s_conexion_nueva", DbType.Int32, _BE_VarPNSU.S_conexion_nueva);
                objDE.AddInParameter(objComando, "@s_conexion_rehab", DbType.Int32, _BE_VarPNSU.S_conexion_rehab);
                objDE.AddInParameter(objComando, "@s_conexion_unid", DbType.Int32, _BE_VarPNSU.S_conexion_unid);
                objDE.AddInParameter(objComando, "@s_redes", DbType.Int32, _BE_VarPNSU.S_redes);
                objDE.AddInParameter(objComando, "@s_impulsion", DbType.Int32, _BE_VarPNSU.S_impulsion);

                objDE.AddInParameter(objComando, "@d_tuberia", DbType.Int32, _BE_VarPNSU.D_tuberia);
                objDE.AddInParameter(objComando, "@d_cuneta", DbType.Int32, _BE_VarPNSU.D_cuneta);
                objDE.AddInParameter(objComando, "@d_tormenta", DbType.Int32, _BE_VarPNSU.D_tormenta);
                objDE.AddInParameter(objComando, "@d_conexion", DbType.Int32, _BE_VarPNSU.D_conexion);
                objDE.AddInParameter(objComando, "@d_inspeccion", DbType.Int32, _BE_VarPNSU.D_inspeccion);
                objDE.AddInParameter(objComando, "@d_secundarios", DbType.Int32, _BE_VarPNSU.D_secundario);
                objDE.AddInParameter(objComando, "@d_principal", DbType.Int32, _BE_VarPNSU.D_principal);

                objDE.AddInParameter(objComando, "@a_captacion_p", DbType.Double, _BE_VarPNSU.A_captacion_p);
                objDE.AddInParameter(objComando, "@a_planta_p", DbType.Double, _BE_VarPNSU.A_trata_p);
                objDE.AddInParameter(objComando, "@a_conduccion_p", DbType.Double, _BE_VarPNSU.A_conduccion_p);
                objDE.AddInParameter(objComando, "@a_reservorio_p", DbType.Double, _BE_VarPNSU.A_reservorio_p);
                objDE.AddInParameter(objComando, "@a_aduccion_p", DbType.Double, _BE_VarPNSU.A_Aduccion_p);
                objDE.AddInParameter(objComando, "@a_redes_p", DbType.Double, _BE_VarPNSU.A_redes_p);
                objDE.AddInParameter(objComando, "@a_conexion_nueva_p", DbType.Double, _BE_VarPNSU.A_conexion_nueva_p);
                objDE.AddInParameter(objComando, "@a_conexion_rehab_p", DbType.Double, _BE_VarPNSU.A_conexion_rehab_p);
                objDE.AddInParameter(objComando, "@a_conexion_piletas_p", DbType.Int32, _BE_VarPNSU.A_conexion_piletas_p);
                objDE.AddInParameter(objComando, "@a_impulsion_p", DbType.Int32, _BE_VarPNSU.A_impulsion_p);
                objDE.AddInParameter(objComando, "@a_estacion_p", DbType.Int32, _BE_VarPNSU.A_estacion_p);

                objDE.AddInParameter(objComando, "@s_linea_p", DbType.Int32, _BE_VarPNSU.S_efluente_p);
                objDE.AddInParameter(objComando, "@s_camara_p", DbType.Int32, _BE_VarPNSU.S_camara_p);
                objDE.AddInParameter(objComando, "@s_emisor_p", DbType.Int32, _BE_VarPNSU.S_emisor_p);
                objDE.AddInParameter(objComando, "@s_planta_p", DbType.Int32, _BE_VarPNSU.S_planta_p);
                objDE.AddInParameter(objComando, "@s_conexion_nueva_p", DbType.Int32, _BE_VarPNSU.S_conexion_nueva_p);
                objDE.AddInParameter(objComando, "@s_conexion_rehab_p", DbType.Int32, _BE_VarPNSU.S_conexion_rehab_p);
                objDE.AddInParameter(objComando, "@s_conexion_unid_p", DbType.Int32, _BE_VarPNSU.S_conexion_unid_p);
                objDE.AddInParameter(objComando, "@s_redes_p", DbType.Int32, _BE_VarPNSU.S_redes_p);
                objDE.AddInParameter(objComando, "@s_impulsion_p", DbType.Int32, _BE_VarPNSU.S_impulsion_p);

                objDE.AddInParameter(objComando, "@d_tuberia_p", DbType.Int32, _BE_VarPNSU.D_tuberia_p);
                objDE.AddInParameter(objComando, "@d_cuneta_p", DbType.Int32, _BE_VarPNSU.D_cuneta_p);
                objDE.AddInParameter(objComando, "@d_boca_p", DbType.Int32, _BE_VarPNSU.D_tormenta_p);
                objDE.AddInParameter(objComando, "@d_conexion_p", DbType.Int32, _BE_VarPNSU.D_conexion_p);
                objDE.AddInParameter(objComando, "@d_inpeccion_p", DbType.Int32, _BE_VarPNSU.D_inspeccion_p);
                objDE.AddInParameter(objComando, "@d_secundarios_p", DbType.Int32, _BE_VarPNSU.D_secundario_p);
                objDE.AddInParameter(objComando, "@d_principal_p", DbType.Int32, _BE_VarPNSU.D_principal_p);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_VarPNSU.id_usuario);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_ComponentesPNSU(BE_MON_VarPNSU _BE_VarPNSU)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ComponentesPNSU]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_VarPNSU.Id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_VarPNSU.TipoFinanciamiento);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spud_MON_AvanceFisico_Eliminar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_AvanceFisico_Eliminar]");

                objDE.AddInParameter(objComando, "@id_avanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spud_MON_AvanceFisico_Editar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_AvanceFisico_Editar]");

                objDE.AddInParameter(objComando, "@id_avanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                objDE.AddInParameter(objComando, "@tipoValorizacion", DbType.String, _BE_Ejecucion.id_tipoValorizacion);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@fisicoReal", DbType.String, _BE_Ejecucion.fisicoReal);
                objDE.AddInParameter(objComando, "@fisicoProgramado", DbType.String, _BE_Ejecucion.fisicoProgramado);
                objDE.AddInParameter(objComando, "@financieroReal", DbType.String, _BE_Ejecucion.financieroReal);
                objDE.AddInParameter(objComando, "@financieroProgramado", DbType.String, _BE_Ejecucion.financieroProgramado);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ejecucion.observacion);
                objDE.AddInParameter(objComando, "@estadoSituacion", DbType.Int32, _BE_Ejecucion.estadoSituacional);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);
                objDE.AddInParameter(objComando, "@NroValorizacion", DbType.String, _BE_Ejecucion.NroAdicional);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spud_MON_Seguimiento_Carta_Fianza_Eliminar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Carta_Fianza_Eliminar]");

                objDE.AddInParameter(objComando, "@id_carta_fianza", DbType.Int32, _BE_Ejecucion.id_carta_fianza);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spud_MON_Seguimiento_Carta_Fianza_Editar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Carta_Fianza_Editar]");

                objDE.AddInParameter(objComando, "@id_carta_fianza", DbType.Int32, _BE_Ejecucion.id_carta_fianza);
                objDE.AddInParameter(objComando, "@entidadFinanciera", DbType.String, _BE_Ejecucion.entidadFinanciera);
                objDE.AddInParameter(objComando, "@nroCarta", DbType.String, _BE_Ejecucion.nroCarta);
                objDE.AddInParameter(objComando, "@tipoCarta", DbType.Int32, _BE_Ejecucion.tipoCarta);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@porcentaje", DbType.String, _BE_Ejecucion.porcentaje);
                objDE.AddInParameter(objComando, "@verificado", DbType.String, _BE_Ejecucion.flagVerificado);
                //objDE.AddInParameter(objComando, "@renovado", DbType.String, _BE_Ejecucion.flagRenovado);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@urlDocAccion", DbType.String, _BE_Ejecucion.urlDocAccion);
                objDE.AddInParameter(objComando, "@fechaRenovacion", DbType.DateTime, _BE_Ejecucion.Date_fechaRenovacion);
                objDE.AddInParameter(objComando, "@idTipoAccion", DbType.String, _BE_Ejecucion.idTipoAccion);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        //public int spud_MON_AvanceFisico_Estudio_Eliminar(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_AvanceFisico_Estudio_Eliminar]");

        //        objDE.AddInParameter(objComando, "@id_avanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
        //        objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

        //        return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        public int spud_MON_AvanceFisico_Estudio_Editar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_AvanceFisico_Estudio_Editar]");

                objDE.AddInParameter(objComando, "@id_avanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@informe", DbType.String, _BE_Ejecucion.informe);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@avance", DbType.String, _BE_Ejecucion.avance);
                objDE.AddInParameter(objComando, "@finacieroReal", DbType.String, _BE_Ejecucion.financieroReal);
                objDE.AddInParameter(objComando, "@situacion", DbType.String, _BE_Ejecucion.situacion);
                objDE.AddInParameter(objComando, "@accionesPrevistas", DbType.String, _BE_Ejecucion.accionesPorRealizar);
                objDE.AddInParameter(objComando, "@accionesRealizadas", DbType.String, _BE_Ejecucion.accionesRealizadas);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);
                //objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, _BE_Ejecucion.flagAyuda);

                objDE.AddInParameter(objComando, "@id_tipomonitoreo", DbType.Int32, _BE_Ejecucion.id_tipomonitoreo);
                objDE.AddInParameter(objComando, "@id_tipoSubMonitoreo", DbType.Int32, _BE_Ejecucion.id_tipoSubMonitoreo);
                objDE.AddInParameter(objComando, "@idEstado", DbType.Int32, _BE_Ejecucion.id_tipoEstadoEjecuccion);
                objDE.AddInParameter(objComando, "@idSubEstado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoEjecucion);
                //objDE.AddInParameter(objComando, "@idSubEstado2", DbType.String, _BE_Ejecucion.id_tipoEstadoParalizado);
                objDE.AddInParameter(objComando, "@urlActa", DbType.String, _BE_Ejecucion.UrlActa);
                objDE.AddInParameter(objComando, "@urlInforme", DbType.String, _BE_Ejecucion.UrlInforme);
                objDE.AddInParameter(objComando, "@urlOficio", DbType.String, _BE_Ejecucion.UrlOficio);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public DataSet spMON_Supervisor(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Supervisor]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_Supervisor(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Supervisor]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Ejecucion.empleadoObra);

                objDE.AddInParameter(objComando, "@id_tipoEmpleado", DbType.Int32, _BE_Ejecucion.tipoempleado);


                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@telefono", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE_Ejecucion.correo);




                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_CertificacionAutorizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_CertificacionAutorizacion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@EIA", DbType.String, _BE_Ejecucion.Eia);

                objDE.AddInParameter(objComando, "@legal", DbType.String, _BE_Ejecucion.Legal);

                objDE.AddInParameter(objComando, "@fuente", DbType.String, _BE_Ejecucion.Fuente);
                objDE.AddInParameter(objComando, "@residuales", DbType.String, _BE_Ejecucion.Residuales);
                objDE.AddInParameter(objComando, "@infobras", DbType.String, _BE_Ejecucion.Infobras);
                objDE.AddInParameter(objComando, "@codigo_infoobras", DbType.String, _BE_Ejecucion.CodigoInfobra);
                objDE.AddInParameter(objComando, "@pruebas", DbType.String, _BE_Ejecucion.pruebas);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_CertificacionAutorizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_CertificacionAutorizacion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EvaluacionRecomendacion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_Ejecucion> F_spMON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_EvaluacionRecomendacion";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.Id_ejecucionRecomendacion = Convert.ToInt32(Reader["id_evaluacionRecomendacion"].ToString());

                        //objENT.fecha = Reader["fechaVisita"].ToString();
                        if ((Reader["fechaVisita"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaVisita = Convert.ToDateTime(Reader["fechaVisita"].ToString());
                            objENT.dtFecha = Convert.ToDateTime(Reader["fechaVisita"].ToString());
                        }
                        objENT.Evaluacion = Reader["evaluacion"].ToString();
                        objENT.Recomendacion = Reader["recomendacion"].ToString();
                        objENT.flagAyuda = Reader["flagAyuda"].ToString();
                        objENT.FisicoEjec = Reader["fisicoEjecutado"].ToString();
                        objENT.financieroReal = Reader["financieroEjecutado"].ToString();

                        if (Reader["Concluidas"] != DBNull.Value)
                            objENT.Concluidas = Convert.ToInt32(Reader["Concluidas"]);
                        if (Reader["EnEjecucion"] != DBNull.Value)
                            objENT.EnEjecucion = Convert.ToInt32(Reader["EnEjecucion"]);
                        if (Reader["PorIniciar"] != DBNull.Value)
                            objENT.PorIniciar = Convert.ToInt32(Reader["PorIniciar"]);
                        if (Reader["Paralizadas"] != DBNull.Value)
                            objENT.Paralizadas = Convert.ToInt32(Reader["Paralizadas"]);

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Ejecucion> F_MON_DetalleSituacional(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_DetalleSituacional";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        //objENT.Id_ejecucionRecomendacion = Convert.ToInt32(Reader["id_evaluacionRecomendacion"].ToString());

                        //objENT.fecha = Reader["fechaVisita"].ToString();
                        if ((Reader["fechaVisita"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaVisita = Convert.ToDateTime(Reader["fechaVisita"].ToString());
                        }
                        objENT.Evaluacion = Reader["detalleSituacional"].ToString();
                        objENT.Recomendacion = Reader["acciones"].ToString();


                        //objENT.flagAyuda = Reader["flagAyuda"].ToString();

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int spi_MON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EvaluacionRecomendacion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@fechaVisita", DbType.DateTime, _BE_Ejecucion.Date_fechaVisita);

                objDE.AddInParameter(objComando, "@evaluacion", DbType.String, _BE_Ejecucion.Evaluacion);

                objDE.AddInParameter(objComando, "@recomendacion", DbType.String, _BE_Ejecucion.Recomendacion);
                objDE.AddInParameter(objComando, "@fisicoEjec", DbType.String, _BE_Ejecucion.FisicoEjec);
                objDE.AddInParameter(objComando, "@fisicoProg", DbType.String, _BE_Ejecucion.FisicoProg);
                // objDE.AddInParameter(objComando, "@CIP", DbType.String, _BE_Ejecucion.CIP);
                objDE.AddInParameter(objComando, "@urlActa", DbType.String, _BE_Ejecucion.UrlActa);
                objDE.AddInParameter(objComando, "@urlInforme", DbType.String, _BE_Ejecucion.UrlInforme);
                objDE.AddInParameter(objComando, "@urlOficio", DbType.String, _BE_Ejecucion.UrlOficio);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@id_tipomonitoreo", DbType.Int32, _BE_Ejecucion.id_tipomonitoreo);
                objDE.AddInParameter(objComando, "@id_tipoSubMonitoreo", DbType.Int32, _BE_Ejecucion.id_tipoSubMonitoreo);

                objDE.AddInParameter(objComando, "@nrotramite", DbType.String, _BE_Ejecucion.nrotramite);
                //objDE.AddInParameter(objComando, "@tipodocumento", DbType.String, _BE_Ejecucion.tipodocumento);
                objDE.AddInParameter(objComando, "@id_tipodocumento", DbType.String, _BE_Ejecucion.id_tipodocumento);
                objDE.AddInParameter(objComando, "@asunto", DbType.String, _BE_Ejecucion.asunto);

                objDE.AddInParameter(objComando, "@financieroEjec", DbType.String, _BE_Ejecucion.financieroReal);
                objDE.AddInParameter(objComando, "@financieroProg", DbType.String, _BE_Ejecucion.financieroProgramado);

                objDE.AddInParameter(objComando, "@idEstado", DbType.Int32, _BE_Ejecucion.id_tipoEstadoEjecuccion);
                objDE.AddInParameter(objComando, "@idSubEstado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoEjecucion);
                objDE.AddInParameter(objComando, "@idSubEstado2", DbType.String, _BE_Ejecucion.id_tipoEstadoParalizado);
                //objDE.AddInParameter(objComando, "@idSubEstadoParalizado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoParalizado);
                objDE.AddInParameter(objComando, "@detalleProblematica", DbType.String, _BE_Ejecucion.detalleProblematica);

                objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, _BE_Ejecucion.flagAyuda);

                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, (_BE_Ejecucion.Date_fechaInicio < Convert.ToDateTime("01/01/1990") ? Convert.ToDateTime("09/09/9999") : _BE_Ejecucion.Date_fechaInicio));
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, (_BE_Ejecucion.Date_fechaFin < Convert.ToDateTime("01/01/1990") ? Convert.ToDateTime("09/09/9999") : _BE_Ejecucion.Date_fechaFin));


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_EvaluacionRecomendacion]");

                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);
                objDE.AddInParameter(objComando, "@fechaVisita", DbType.DateTime, _BE_Ejecucion.Date_fechaVisita);

                objDE.AddInParameter(objComando, "@evaluacion", DbType.String, _BE_Ejecucion.Evaluacion);

                objDE.AddInParameter(objComando, "@recomendacion", DbType.String, _BE_Ejecucion.Recomendacion);
                objDE.AddInParameter(objComando, "@fisicoEjec", DbType.String, _BE_Ejecucion.FisicoEjec);
                objDE.AddInParameter(objComando, "@fisicoProg", DbType.String, _BE_Ejecucion.FisicoProg);
                objDE.AddInParameter(objComando, "@tipo", DbType.String, _BE_Ejecucion.Tipo);
                objDE.AddInParameter(objComando, "@urlActa", DbType.String, _BE_Ejecucion.UrlActa);
                objDE.AddInParameter(objComando, "@urlInforme", DbType.String, _BE_Ejecucion.UrlInforme);
                objDE.AddInParameter(objComando, "@urlOficio", DbType.String, _BE_Ejecucion.UrlOficio);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@id_tipomonitoreo", DbType.Int32, _BE_Ejecucion.id_tipomonitoreo);
                objDE.AddInParameter(objComando, "@id_tipoSubMonitoreo", DbType.Int32, _BE_Ejecucion.id_tipoSubMonitoreo);

                objDE.AddInParameter(objComando, "@nrotramite", DbType.String, _BE_Ejecucion.nrotramite);
                objDE.AddInParameter(objComando, "@id_tipodocumento", DbType.String, _BE_Ejecucion.id_tipodocumento);
                objDE.AddInParameter(objComando, "@asunto", DbType.String, _BE_Ejecucion.asunto);
                objDE.AddInParameter(objComando, "@financieroEjec", DbType.String, _BE_Ejecucion.financieroReal);
                objDE.AddInParameter(objComando, "@financieroProg", DbType.String, _BE_Ejecucion.financieroProgramado);
                objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, _BE_Ejecucion.flagAyuda);
                objDE.AddInParameter(objComando, "@idEstado", DbType.Int32, _BE_Ejecucion.id_tipoEstadoEjecuccion);
                objDE.AddInParameter(objComando, "@idSubEstado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoEjecucion);
                objDE.AddInParameter(objComando, "@idSubEstado2", DbType.String, _BE_Ejecucion.id_tipoEstadoParalizado);
                //objDE.AddInParameter(objComando, "@idSubEstadoParalizado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoParalizado);
                objDE.AddInParameter(objComando, "@detalleProblematica", DbType.String, _BE_Ejecucion.detalleProblematica);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, (_BE_Ejecucion.Date_fechaInicio < Convert.ToDateTime("01/01/1990") ? Convert.ToDateTime("09/09/9999") : _BE_Ejecucion.Date_fechaInicio));
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, (_BE_Ejecucion.Date_fechaFin < Convert.ToDateTime("01/01/1990") ? Convert.ToDateTime("09/09/9999") : _BE_Ejecucion.Date_fechaFin));
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_AvanceFisicoByFlagAyudaMemoria(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_AvanceFisicoByFlagAyudaMemoria]");

                objDE.AddInParameter(objComando, "@id_avanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, _BE_Ejecucion.flagAyuda);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_EvaluacionRecomendacionByFlag(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_EvaluacionRecomendacionByFlag]");

                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                objDE.AddInParameter(objComando, "@flagAyuda", DbType.String, _BE_Ejecucion.flagAyuda);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public int spi_MON_EstadoEjecuccion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EstadoEjecuccion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@coordinadorMVCS", DbType.String, _BE_Ejecucion.coordinadorMVCS);

                objDE.AddInParameter(objComando, "@id_estadoEjecucion", DbType.Int32, _BE_Ejecucion.id_tipoEstadoEjecuccion);
                objDE.AddInParameter(objComando, "@id_subEstado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoEjecucion);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);
                objDE.AddInParameter(objComando, "@fechaInicioContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaInicioContractual);
                objDE.AddInParameter(objComando, "@fechaInicioReal", DbType.DateTime, _BE_Ejecucion.Date_fechaInicioReal);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaFinContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaFinContractual);
                objDE.AddInParameter(objComando, "@fechaFinReal", DbType.DateTime, _BE_Ejecucion.Date_fechaFinReal);
                objDE.AddInParameter(objComando, "@urlMetas", DbType.String, _BE_Ejecucion.urlMetasFisicas);
                objDE.AddInParameter(objComando, "@urlMemorias", DbType.String, _BE_Ejecucion.urlMemoriasDescriptivas);

                objDE.AddInParameter(objComando, "@beneficiariosdirectos", DbType.Int32, _BE_Ejecucion.poblacion);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@fechaEntregaTerreno", DbType.DateTime, _BE_Ejecucion.Date_fechaEntregaTerreno);
                objDE.AddInParameter(objComando, "@fechaRecepcion", DbType.DateTime, _BE_Ejecucion.Date_fechaRecepcion);
                objDE.AddInParameter(objComando, "@supervisor", DbType.String, _BE_Ejecucion.supervisorDesignado);
                objDE.AddInParameter(objComando, "@residente", DbType.String, _BE_Ejecucion.residenteObra);
                objDE.AddInParameter(objComando, "@inspector", DbType.String, _BE_Ejecucion.inspector);
                objDE.AddInParameter(objComando, "@coordinadorUE", DbType.String, _BE_Ejecucion.coordinadorUE);
                objDE.AddInParameter(objComando, "@telefonoUE", DbType.String, _BE_Ejecucion.telefonoUE);
                objDE.AddInParameter(objComando, "@correoUE", DbType.String, _BE_Ejecucion.correoUE);
                objDE.AddInParameter(objComando, "@telefonocoordinador", DbType.String, _BE_Ejecucion.telefonocoordinador);
                objDE.AddInParameter(objComando, "@correocoordinador", DbType.String, _BE_Ejecucion.correocoordinador);

                objDE.AddInParameter(objComando, "@montoAdelantoDirecto", DbType.String, _BE_Ejecucion.montoAdelantoDirecto);
                objDE.AddInParameter(objComando, "@montoAdelantoMaterial", DbType.String, _BE_Ejecucion.montoAdelantoMaterial);
                objDE.AddInParameter(objComando, "@fechaAdelantoDirecto", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoDirecto);
                objDE.AddInParameter(objComando, "@fechaAdelantoMAterial", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoMaterial);
                objDE.AddInParameter(objComando, "@urlFichaRecepcion", DbType.String, _BE_Ejecucion.urlFichaRecepcion);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_EstadoEjecuccionTransferencia(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EstadoEjecuccionTransferencia]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@coordinadorMVCS", DbType.String, _BE_Ejecucion.coordinadorMVCS);

                //objDE.AddInParameter(objComando, "@id_estadoEjecucion", DbType.Int32, _BE_Ejecucion.id_tipoEstadoEjecuccion);
                //objDE.AddInParameter(objComando, "@id_subEstado", DbType.String, _BE_Ejecucion.id_tipoSubEstadoEjecucion);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);
                objDE.AddInParameter(objComando, "@fechaInicioContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaInicioContractual);
                objDE.AddInParameter(objComando, "@fechaInicioReal", DbType.DateTime, _BE_Ejecucion.Date_fechaInicioReal);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaFinContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaFinContractual);
                objDE.AddInParameter(objComando, "@fechaFinReal", DbType.DateTime, _BE_Ejecucion.Date_fechaFinReal);
                objDE.AddInParameter(objComando, "@fechaFinProbable", DbType.DateTime, _BE_Ejecucion.Date_fechaFinProbable);

                objDE.AddInParameter(objComando, "@urlMetas", DbType.String, _BE_Ejecucion.urlMetasFisicas);
                objDE.AddInParameter(objComando, "@urlMemorias", DbType.String, _BE_Ejecucion.urlMemoriasDescriptivas);

                objDE.AddInParameter(objComando, "@beneficiariosdirectos", DbType.Int32, _BE_Ejecucion.poblacion);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@fechaEntregaTerreno", DbType.DateTime, _BE_Ejecucion.Date_fechaEntregaTerreno);
                objDE.AddInParameter(objComando, "@fechaRecepcion", DbType.DateTime, _BE_Ejecucion.Date_fechaRecepcion);
                objDE.AddInParameter(objComando, "@supervisor", DbType.String, _BE_Ejecucion.supervisorDesignado);
                objDE.AddInParameter(objComando, "@residente", DbType.String, _BE_Ejecucion.residenteObra);
                objDE.AddInParameter(objComando, "@inspector", DbType.String, _BE_Ejecucion.inspector);
                objDE.AddInParameter(objComando, "@coordinadorUE", DbType.String, _BE_Ejecucion.coordinadorUE);
                objDE.AddInParameter(objComando, "@telefonoUE", DbType.String, _BE_Ejecucion.telefonoUE);
                objDE.AddInParameter(objComando, "@correoUE", DbType.String, _BE_Ejecucion.correoUE);
                objDE.AddInParameter(objComando, "@telefonocoordinador", DbType.String, _BE_Ejecucion.telefonocoordinador);
                objDE.AddInParameter(objComando, "@correocoordinador", DbType.String, _BE_Ejecucion.correocoordinador);

                //objDE.AddInParameter(objComando, "@montoAdelantoDirecto", DbType.String, _BE_Ejecucion.montoAdelantoDirecto);
                //objDE.AddInParameter(objComando, "@montoAdelantoMaterial", DbType.String, _BE_Ejecucion.montoAdelantoMaterial);
                //objDE.AddInParameter(objComando, "@fechaAdelantoDirecto", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoDirecto);
                //objDE.AddInParameter(objComando, "@fechaAdelantoMAterial", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoMaterial);
                objDE.AddInParameter(objComando, "@urlFichaRecepcion", DbType.String, _BE_Ejecucion.urlFichaRecepcion);

                //objDE.AddInParameter(objComando, "@iTipoPrioridad", DbType.Int32, (_BE_Ejecucion.tipoPrioridad is null ? "0" : _BE_Ejecucion.tipoPrioridad));


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int I_spi_MON_EstadoEjecucionNE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_EstadoEjecucionNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    //cmd.Parameters.Add("@id_estadoEjecucion", SqlDbType.Int).Value = _BE.id_tipoEstadoEjecuccion;
                    //cmd.Parameters.Add("@id_subEstado", SqlDbType.VarChar, 5).Value = _BE.id_tipoSubEstadoEjecucion;
                    cmd.Parameters.Add("@fechaInforme", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@fechaInicio", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    cmd.Parameters.Add("@fechaInicioContractual", SqlDbType.DateTime).Value = _BE.Date_fechaInicioContractual;
                    cmd.Parameters.Add("@plazo", SqlDbType.Int).Value = _BE.plazoEjecuccion;
                    cmd.Parameters.Add("@plazoProyecto", SqlDbType.Int).Value = _BE.plazoProyecto;
                    //cmd.Parameters.Add("@fechaFin", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@fechaFinContractual", SqlDbType.DateTime).Value = _BE.Date_fechaFinContractual;
                    cmd.Parameters.Add("@fechaFinReal", SqlDbType.DateTime).Value = _BE.Date_fechaFinReal;
                    cmd.Parameters.Add("@fechaFinProbable", SqlDbType.DateTime).Value = _BE.Date_fechaFinProbable;

                    cmd.Parameters.Add("@fechaEntregaTerreno", SqlDbType.DateTime).Value = _BE.Date_fechaEntregaTerreno;
                    cmd.Parameters.Add("@urlEntregaTerreno", SqlDbType.VarChar, 200).Value = _BE.UrlActaTerreno;
                    cmd.Parameters.Add("@urlInformeInicial", SqlDbType.VarChar, 200).Value = _BE.UrlInformeInicial;
                    cmd.Parameters.Add("@fechaAutorizacionRetiro", SqlDbType.DateTime).Value = _BE.Date_fechaRetiro;
                    cmd.Parameters.Add("@fechaAprobacionPMA", SqlDbType.DateTime).Value = _BE.Date_fechaAprobacionPMA;
                    //cmd.Parameters.Add("@fechaRecepcion", SqlDbType.DateTime).Value = _BE.Date_fechaRecepcion;
                    //cmd.Parameters.Add("@urlFichaRecepcion", SqlDbType.VarChar, 200).Value = _BE.urlFichaRecepcion;

                    cmd.Parameters.Add("@fechaInicioProyecto", SqlDbType.DateTime).Value = _BE.Date_fechaInicioProyecto;
                    cmd.Parameters.Add("@fechaProbableInicioObra", SqlDbType.DateTime).Value = _BE.Date_fechaProbableInicio;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_spiu_MON_EstadoSituacionalNE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_EstadoSituacionalNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@id_estadoEjecucion", SqlDbType.Int).Value = _BE.id_tipoEstadoEjecuccion;
                    cmd.Parameters.Add("@id_subEstado", SqlDbType.VarChar, 5).Value = _BE.id_tipoSubEstadoEjecucion;
                    cmd.Parameters.Add("@id_subEstado2", SqlDbType.VarChar, 5).Value = _BE.id_tipoEstadoParalizado;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_AsignarAgente(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_AsignarAgente", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@id_tecnico", SqlDbType.Int).Value = _BE.id_tecnico;
                    cmd.Parameters.Add("@id_tipoAgente", SqlDbType.Int).Value = _BE.id_tipoAgente;
                    cmd.Parameters.Add("@fechaActaAsignacion", SqlDbType.DateTime).Value = _BE.Date_fechaActaAsignacion;
                    //cmd.Parameters.Add("@nroContrato", SqlDbType.VarChar, 15).Value = _BE.nroContrato;
                    cmd.Parameters.Add("@montoContrato", SqlDbType.Float).Value = _BE.monto;

                    //cmd.Parameters.Add("@fechaInicioContrato", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    //cmd.Parameters.Add("@plazoContrato", SqlDbType.Int).Value = _BE.plazoEjecuccion;
                    //cmd.Parameters.Add("@fechaEstimadaFin", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@idEstadoAsignacion", SqlDbType.Int).Value = _BE.id_estado;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_Agentes(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_Agentes";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_proyecto = Convert.ToInt32(Reader["id_proyecto"].ToString());
                        objENT.id_tecnico = Convert.ToInt32(Reader["id_tecnico"].ToString());
                        objENT.id_proyectoTecnico = Convert.ToInt32(Reader["id_proyecto_tecnico"].ToString());
                        objENT.agente = Reader["Agente"].ToString();
                        objENT.nombre = Reader["nombre"].ToString();

                        objENT.id_tipoAgente = Convert.ToInt32(Reader["id_tipoAgente"].ToString());
                        objENT.tipoAgente = Reader["tipoAgente"].ToString();
                        objENT.tipoProfesion = Reader["profesion"].ToString();



                        objENT.monto = Reader["montoContrato"].ToString();

                        if ((Reader["fechaActaAsignacion"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaActaAsignacion = Convert.ToDateTime(Reader["fechaActaAsignacion"].ToString());
                        }
                        objENT.id_tipoEstadoEjecuccion = Convert.ToInt32(Reader["id_estadoAsignacionAgente"].ToString() == "" ? "0" : Reader["id_estadoAsignacionAgente"].ToString());
                        objENT.tipoEstado = Reader["estado"].ToString();
                        //objENT.nroContrato = Reader["nroContrato"].ToString();
                        //if ((Reader["fechaInicioContrato"].ToString()).Length > 0)
                        //{
                        //    objENT.Date_fechaInicio = Convert.ToDateTime(Reader["fechaInicioContrato"].ToString());
                        //}
                        //if ((Reader["plazoContrato"].ToString()).Length > 0)
                        //{
                        //    objENT.plazoEjecuccion = Convert.ToInt32(Reader["plazoContrato"].ToString());
                        //}
                        //if ((Reader["fechaEstimadaFin"].ToString()).Length > 0)
                        //{
                        //    objENT.Date_fechaFin = Convert.ToDateTime(Reader["fechaEstimadaFin"].ToString());
                        //}

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }
                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int UD_spud_MON_AsignarAgente(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_AsignarAgente", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@id_tecnico", SqlDbType.Int).Value = _BE.id_tecnico;
                    cmd.Parameters.Add("@id_tipoAgente", SqlDbType.Int).Value = _BE.id_tipoAgente;
                    cmd.Parameters.Add("@fechaActaAsignacion", SqlDbType.DateTime).Value = _BE.Date_fechaActaAsignacion;
                    //cmd.Parameters.Add("@nroContrato", SqlDbType.VarChar, 15).Value = _BE.nroContrato;
                    cmd.Parameters.Add("@montoContrato", SqlDbType.Float).Value = _BE.monto;

                    //cmd.Parameters.Add("@fechaInicioContrato", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    //cmd.Parameters.Add("@plazoContrato", SqlDbType.Int).Value = _BE.plazoEjecuccion;
                    //cmd.Parameters.Add("@fechaEstimadaFin", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@idEstadoAsignacion", SqlDbType.Int).Value = _BE.id_estado;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_ProgramacionInforme(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ProgramacionInforme";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_avanceFisico"].ToString());
                        //objENT.id_proyectoTecnico = Convert.ToInt32(Reader["id_proyectoTecnico"].ToString());

                        if ((Reader["fechaProgramada"].ToString()).Length > 0)
                        {
                            objENT.dtFechaProgramada = Convert.ToDateTime(Reader["fechaProgramada"].ToString());
                            objENT.strFechaProgramada = Reader["fechaProgramada"].ToString();
                        }
                        else
                        {
                            objENT.strFechaProgramada = "";
                        }

                        if ((Reader["fechaInicioInforme"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaInicio = Convert.ToDateTime(Reader["fechaInicioInforme"].ToString());
                        }
                        if ((Reader["fechaFinInforme"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaFin = Convert.ToDateTime(Reader["fechaFinInforme"].ToString());
                        }
                        //objENT.diasProgramado = Convert.ToInt32(Reader["diasProgramado"].ToString());
                        //objENT.diasReal = Convert.ToInt32(Reader["diasReal"].ToString());
                        objENT.monto = Reader["montoValorizacionProgramado"].ToString();
                        //objENT.id_tipoAgente = Convert.ToInt32(Reader["id_tipoAgente"].ToString());
                        objENT.nombre = Reader["nombre"].ToString();
                        objENT.fisicoProgramado = Reader["fisicoProgramado"].ToString();
                        objENT.fisicoProgramadoAcumulado = Reader["Acumulado"].ToString();
                        objENT.financieroProgramado = Reader["financieroProgramado"].ToString();

                        objENT.flagVerificado = Reader["flagInforme"].ToString();

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }
                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public BE_MON_Ejecucion F_MON_MaxValoresProgramacion(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            //List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_MaxValoresProgramacion";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                cmd.Parameters.Add("@tipoModalidad", SqlDbType.Int).Value = _BE.id_tipoModalidad;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    if (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        //objENT.id_proyectoTecnico = Convert.ToInt32(Reader["id_proyectoTecnico"].ToString());
                        if ((Reader["fechaInicio"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaInicioReal = Convert.ToDateTime(Reader["fechaInicio"].ToString());
                        }

                        objENT.plazoEjecuccion = Convert.ToInt32(Reader["plazoEjecucion"].ToString());
                        objENT.diasReal = Convert.ToInt32(Reader["diasAmpliados"].ToString());

                        if ((Reader["fechaInicioInforme"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaInicio = Convert.ToDateTime(Reader["fechaInicioInforme"].ToString());
                        }

                        //objCollection.Add(objENT);
                        //objENT = null;
                    }

                }
                //   Reader.Close();
                return objENT;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public List<BE_MON_Ejecucion> F_spMON_MesesValorizacionSupervision(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_MesesValorizacionSupervision";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.mes = Reader["mes"].ToString();

                        objENT.Date_fechaFin = Convert.ToDateTime(Reader["fechaFinInforme"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public int I_spi_MON_GeneracionProgramacion(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_GeneracionProgramacion", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    //cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_CulminacionEjecucionNE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_CulminacionEjecucionNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@fechaFin", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@fechaRendicionFinal", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@fechaCulminacionProyecto", SqlDbType.DateTime).Value = _BE.Date_fechaCulminacionProyecto;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@urlActatermino", SqlDbType.VarChar).Value = _BE.urlActatermino;
                    cmd.Parameters.Add("@fechaRecepcion", SqlDbType.DateTime).Value = _BE.Date_fechaRecepcion;
                    cmd.Parameters.Add("@urlFichaRecepcion", SqlDbType.VarChar, 200).Value = _BE.urlFichaRecepcion;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int I_spi_MON_ProgramacionInforme(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_ProgramacionInforme", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    //cmd.Parameters.Add("@id_proyectoTecnico", SqlDbType.Int).Value = _BE.id_proyectoTecnico;
                    cmd.Parameters.Add("@fechaProgramada", SqlDbType.DateTime).Value = _BE.dtFechaProgramada;

                    //cmd.Parameters.Add("@diasProgramado", SqlDbType.Int).Value = _BE.diasProgramado;

                    cmd.Parameters.Add("@fechaInicioInformar", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    cmd.Parameters.Add("@fechaFinInformar", SqlDbType.DateTime).Value = _BE.Date_fechaFin;

                    cmd.Parameters.Add("@fisicoProgramado", SqlDbType.VarChar, 5).Value = _BE.fisicoProgramado;
                    cmd.Parameters.Add("@financieroProgramado", SqlDbType.VarChar, 5).Value = _BE.financieroProgramado;

                    //cmd.Parameters.Add("@id_tipoAgente", SqlDbType.Int).Value = _BE.id_tipoAgente;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int D_spd_MON_CronogramaProgramacion(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spd_MON_CronogramaProgramacion", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@NewfechaInicioInformar", SqlDbType.DateTime).Value = _BE.dtFechaProgramada;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_ProgramacionInforme(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_ProgramacionInforme", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_avanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;
                    //cmd.Parameters.Add("@id_proyectoTecnico", SqlDbType.Int).Value = _BE.id_proyectoTecnico;
                    cmd.Parameters.Add("@fechaProgramada", SqlDbType.DateTime).Value = _BE.dtFechaProgramada;
                    cmd.Parameters.Add("@fisicoProgramado", SqlDbType.VarChar, 5).Value = _BE.fisicoProgramado;
                    cmd.Parameters.Add("@financieroProgramado", SqlDbType.VarChar, 5).Value = _BE.financieroProgramado;
                    //cmd.Parameters.Add("@diasProgramado", SqlDbType.Int).Value = _BE.diasProgramado;

                    //cmd.Parameters.Add("@fechaInicioInformar", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    //cmd.Parameters.Add("@fechaFinInformar", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@tipoUD", SqlDbType.Int).Value = _BE.Tipo;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_ProgramacionInformePorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_ProgramacionInformePorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_avanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;
                    //cmd.Parameters.Add("@id_proyectoTecnico", SqlDbType.Int).Value = _BE.id_proyectoTecnico;
                    //cmd.Parameters.Add("@fechaProgramada", SqlDbType.DateTime).Value = _BE.dtFechaProgramada;
                    //cmd.Parameters.Add("@fisicoProgramado", SqlDbType.VarChar, 5).Value = _BE.fisicoProgramado;
                    cmd.Parameters.Add("@montoValorizacionProgramado", SqlDbType.VarChar, 18).Value = _BE.monto;
                    cmd.Parameters.Add("@fisicoProgramado", SqlDbType.VarChar, 5).Value = _BE.fisicoProgramado;
                    //cmd.Parameters.Add("@diasProgramado", SqlDbType.Int).Value = _BE.diasProgramado;

                    //cmd.Parameters.Add("@fechaInicioInformar", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    //cmd.Parameters.Add("@fechaFinInformar", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@tipoUD", SqlDbType.Int).Value = _BE.Tipo;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvanceNE(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_InformeAvanceNE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        //objENT.numero = Convert.ToInt32(Reader["numero"].ToString());
                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_avanceFisico"].ToString());
                        //objENT.id_proyectoTecnico = Convert.ToInt32(Reader["id_proyectoTecnico"].ToString());
                        //objENT.tipoAgente = Reader["tipoAgente"].ToString();
                        //if ((Reader["fechaProgramada"].ToString()).Length > 0)
                        //{
                        //    objENT.dtFechaProgramada = Convert.ToDateTime(Reader["fechaProgramada"].ToString());
                        //    objENT.strFechaProgramada = Reader["fechaProgramada"].ToString();
                        //}
                        //objENT.Date_fechaFin = Convert.ToDateTime(Reader["fechaLimitePreLiquidacion"].ToString());
                        if ((Reader["fechaLimitePreLiquidacion"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaFin = Convert.ToDateTime(Reader["fechaLimitePreLiquidacion"].ToString());
                        }
                        if ((Reader["fechaPreLiquidacion"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaPreLiquidacion = Convert.ToDateTime(Reader["fechaPreLiquidacion"].ToString());
                        }
                        if ((Reader["fechaAprobacionPreLiquidacion"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaAprobacion = Convert.ToDateTime(Reader["fechaAprobacionPreLiquidacion"].ToString());
                        }
                        if ((Reader["fechaObsPreLiquidacion"].ToString()).Length > 0)
                        {
                            objENT.Date_fechaObsPreLiquidacion = Convert.ToDateTime(Reader["fechaObsPreLiquidacion"].ToString());
                        }

                        //objENT.tipoValorizacion = Reader["tipoValorizacion"].ToString();
                        //objENT.NroAdicional = Reader["NroAdicional"].ToString();
                        if ((Reader["fecha"].ToString()).Length > 0)
                        {
                            objENT.fecha = Reader["fecha"].ToString();
                        }
                        //string monto= Reader["monto"].ToString();
                        objENT.monto = Convert.ToDouble(Reader["monto"].ToString()).ToString("N2");
                        objENT.montoPreLiquidacion = Convert.ToDouble(Reader["montoPreLiquidacion"].ToString()).ToString("N2");
                        objENT.montoEjecutado = Convert.ToDouble(Reader["montoCanchaNE"].ToString()).ToString("N2");
                        objENT.fisicoReal = Reader["fisicoReal"].ToString();
                        objENT.fisicoProgramado = Reader["fisicoProgramado"].ToString();
                        objENT.financieroReal = Reader["financieroReal"].ToString();
                        objENT.financieroProgramado = Reader["financieroProgramado"].ToString();

                        //objENT.materialReal = Reader["valorizacionMaterialReal"].ToString();
                        //objENT.materialProgramado = Reader["valorizacionMaterialProgramado"].ToString();
                        string strIdEstadoPreLiquidacion = Reader["id_tipoEstadoPreLiquidacion"].ToString();
                        objENT.idEstadoPreLiquidacion = strIdEstadoPreLiquidacion.Length == 0 ? -1 : Convert.ToInt32(strIdEstadoPreLiquidacion);
                        objENT.idEstadoSituacional = Convert.ToInt32(Reader["id_tipoEstadoSituacional"].ToString());
                        objENT.estadoSituacional = Reader["estadoSituacional"].ToString();

                        //objENT.diasProgramado = Convert.ToInt32(Reader["diasProgramado"].ToString());
                        objENT.nombre = Reader["mes"].ToString();

                        objENT.urlDoc = Reader["urlDoc"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }
                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int I_InformeAvanceNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_InformeAvancePNSR]");

                objDE.AddInParameter(objComando, "@idAvanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                //objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                //objDE.AddInParameter(objComando, "@idProyectoTecnico", DbType.Int32, _BE_Ejecucion.id_proyectoTecnico);
                //objDE.AddInParameter(objComando, "@fechaProgramada", DbType.DateTime, _BE_Ejecucion.dtFechaProgramada);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Ejecucion.Date_fecha);
                objDE.AddInParameter(objComando, "@fechaPreLiquidacion", DbType.DateTime, _BE_Ejecucion.Date_fechaPreLiquidacion);
                objDE.AddInParameter(objComando, "@fechaAprobacion", DbType.DateTime, _BE_Ejecucion.Date_fechaAprobacion);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@montoPreLiquidacion", DbType.String, _BE_Ejecucion.montoPreLiquidacion);
                objDE.AddInParameter(objComando, "@montoCancha", DbType.String, _BE_Ejecucion.montoEjecutado);
                objDE.AddInParameter(objComando, "@fisicoReal", DbType.String, _BE_Ejecucion.fisicoReal);
                //objDE.AddInParameter(objComando, "@diaAsistidos", DbType.Int32, _BE_Ejecucion.diasReal);
                objDE.AddInParameter(objComando, "@financieroReal", DbType.String, _BE_Ejecucion.financieroReal);
                //objDE.AddInParameter(objComando, "@financieroProgramado", DbType.String, _BE_Ejecucion.financieroProgramado);
                objDE.AddInParameter(objComando, "@estadoSituacion", DbType.Int32, _BE_Ejecucion.estadoSituacional);
                objDE.AddInParameter(objComando, "@estadoPreLiquidacion", DbType.Int32, _BE_Ejecucion.estadoPreLiquidacion);
                objDE.AddInParameter(objComando, "@fechaObsPreLiquidacion", DbType.DateTime, _BE_Ejecucion.Date_fechaObsPreLiquidacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Ejecucion.urlDoc);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ejecucion.observacion);

                objDE.AddInParameter(objComando, "@tipoValorizacion", DbType.String, _BE_Ejecucion.id_tipoValorizacion);
                //objDE.AddInParameter(objComando, "@NroValorizacion", DbType.String, _BE_Ejecucion.NroAdicional);

                //objDE.AddInParameter(objComando, "@materialReal", DbType.String, _BE_Ejecucion.materialReal);
                //objDE.AddInParameter(objComando, "@materialProgramado", DbType.String, _BE_Ejecucion.materialProgramado);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int D_InformeAvancePNSR(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spd_MON_InformeAvancePNSR");

                objDE.AddInParameter(objComando, "@idAvanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvancePorContrata(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_InformeAvancePorContrata";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_avanceFisico"].ToString());
                        objENT.mes = Reader["mes"].ToString();
                        objENT.Date_fechaInforme = Convert.ToDateTime(Reader["fechaPresentacion"].ToString());
                        objENT.monto = Convert.ToDouble(Reader["monto"].ToString()).ToString("N2");
                        objENT.montoReajuste = Convert.ToDouble(Reader["montoReajuste"].ToString()).ToString("N2");
                        objENT.amortizacionDirecto = Convert.ToDouble(Reader["amortizacionDirecto"].ToString()).ToString("N2");
                        objENT.amortizacionMateriales = Convert.ToDouble(Reader["amortizacionMateriales"].ToString()).ToString("N2");
                        objENT.porcentaje = Reader["IGV"].ToString();
                        objENT.montoEjecutado = Reader["montoValorizacionEjecutado"].ToString();
                        objENT.fisicoReal = Reader["fisicoReal"].ToString();
                        objENT.fisicoProgramado = Reader["fisicoProgramado"].ToString();
                        objENT.financieroReal = Reader["financieroReal"].ToString();
                        objENT.financieroProgramado = Reader["financieroProgramado"].ToString();
                        objENT.idEstadoSituacional = Convert.ToInt32(Reader["id_tipoEstadoSituacional"].ToString());
                        objENT.estadoSituacional = Reader["estadoSituacional"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();
                        objENT.Date_fechaPago = Convert.ToDateTime(Reader["fechaPago"].ToString());
                        objENT.vFechaPago = (Reader["vfechaPago"].ToString());
                        objENT.urlDoc = Reader["urlFactura"].ToString();
                        objENT.id_tipoValorizacion = (Reader["id_tipoValorizacion"].ToString());

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public List<BE_MON_Ejecucion> F_spMON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_InformeAvanceAdicionalPorContrata";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_avanceFisico"].ToString());
                        //objENT.mes = Reader["mes"].ToString();
                        objENT.NroAdicional = Reader["NroAdicional"].ToString();
                        objENT.nroValorizacionAdicional = Reader["nroValorizacionAdicional"].ToString();
                        //objENT.Date_fechaInforme = Convert.ToDateTime(Reader["fechaPresentacion"].ToString());

                        if (Reader["fechaPresentacion"].ToString() == "")
                        {

                        }
                        else
                        {
                            objENT.Date_fechaInforme = Convert.ToDateTime(Reader["fechaPresentacion"].ToString());
                        }

                        objENT.monto = Convert.ToDouble(Reader["monto"].ToString()).ToString("N2");
                        objENT.montoReajuste = Convert.ToDouble(Reader["montoReajuste"].ToString()).ToString("N2");
                        //objENT.amortizacionDirecto = Convert.ToDouble(Reader["amortizacionDirecto"].ToString()).ToString("N2");
                        //objENT.amortizacionMateriales = Convert.ToDouble(Reader["amortizacionMateriales"].ToString()).ToString("N2");
                        objENT.porcentaje = Reader["IGV"].ToString();
                        objENT.montoEjecutado = Reader["montoValorizacionEjecutado"].ToString();
                        objENT.fisicoReal = Reader["fisicoReal"].ToString();
                        objENT.fisicoProgramado = Reader["fisicoProgramado"].ToString();
                        objENT.financieroReal = Reader["financieroReal"].ToString();
                        objENT.financieroProgramado = Reader["financieroProgramado"].ToString();
                        objENT.idEstadoSituacional = Convert.ToInt32(Reader["id_tipoEstadoSituacional"].ToString());
                        objENT.estadoSituacional = Reader["estadoSituacional"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();
                        objENT.amortizacionDirecto = Convert.ToDouble(Reader["amortizacionDirecto"].ToString()).ToString("N2");
                        objENT.amortizacionMateriales = Convert.ToDouble(Reader["amortizacionMateriales"].ToString()).ToString("N2");
                        objENT.strFechaPago = (Reader["fechaPago"].ToString());

                        if (Reader["fechaPago"].ToString() == "")
                        {

                        }
                        else
                        {
                            objENT.Date_fechaPago = Convert.ToDateTime(Reader["fechaPago"].ToString());
                        }

                        objENT.urlDoc = Reader["urlFactura"].ToString();
                        objENT.id_tipoValorizacion = (Reader["id_tipoValorizacion"].ToString());

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public List<BE_MON_Ejecucion> F_spMON_AvanceFinancieroNE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
                List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_AvanceFinancieroNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@id_avanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_Ejecucion();

                            objENT.numero = Convert.ToInt32(Reader["id_itemFinanciero"].ToString());
                            objENT.nombre = Reader["nombre"].ToString();
                            objENT.monto = Convert.ToDouble(Reader["valorFinanciado"].ToString()).ToString("N2");
                            objENT.montoTotal = Reader["acumulado"].ToString();
                            objENT.montoEjecutado = Reader["gasto"].ToString();

                            objENT.usuario = Reader["usuario"].ToString();
                            if (Reader["fecha_update"].ToString() == "")
                            {
                                objENT.fecha_update = null;
                            }
                            else
                            {
                                objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                            }
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvanceSupervisionPorContrata(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_InformeAvanceSupervisionPorContrata";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_avanceFisico"].ToString());
                        objENT.mes = Reader["mes"].ToString();
                        objENT.Date_fechaInforme = Convert.ToDateTime(Reader["fechaPresentacion"].ToString());
                        objENT.Date_fechaPago = Convert.ToDateTime(Reader["fechaPago"].ToString());

                        objENT.monto = Convert.ToDouble(Reader["monto"].ToString()).ToString("N2");


                        objENT.amortizacionDirecto = Convert.ToDouble(Reader["amortizacionDirecto"].ToString()).ToString("N2");

                        objENT.fisicoReal = Reader["fisicoReal"].ToString();

                        objENT.idEstadoSituacional = Convert.ToInt32(Reader["id_tipoEstadoSituacional"].ToString());
                        objENT.estadoSituacional = Reader["estadoSituacional"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();

                        objENT.urlDoc = Reader["urlDoc"].ToString();
                        objENT.id_tipoValorizacion = (Reader["id_tipoValorizacion"].ToString());

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public int I_FinancieroTotalNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FinancieroTotal]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@id_itemFinanciero", DbType.Int32, _BE_Ejecucion.id_item);
                objDE.AddInParameter(objComando, "@valor", DbType.String, _BE_Ejecucion.montoTotal);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                //objDE.AddInParameter(objComando, "@materialReal", DbType.String, _BE_Ejecucion.materialReal);
                //objDE.AddInParameter(objComando, "@materialProgramado", DbType.String, _BE_Ejecucion.materialProgramado);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_FinancieroMensualNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_FinancieroMensualNE]");

                objDE.AddInParameter(objComando, "@id_avanceFisico", DbType.Int32, _BE_Ejecucion.id_avanceFisico);
                objDE.AddInParameter(objComando, "@id_itemFinanciero", DbType.Int32, _BE_Ejecucion.id_item);
                objDE.AddInParameter(objComando, "@valor", DbType.String, _BE_Ejecucion.monto);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                objDE.AddInParameter(objComando, "@idProyectoTecnico", DbType.Int32, _BE_Ejecucion.id_proyectoTecnico);
                objDE.AddInParameter(objComando, "@fechaProgramada", DbType.DateTime, _BE_Ejecucion.dtFechaProgramada);
                //objDE.AddInParameter(objComando, "@materialReal", DbType.String, _BE_Ejecucion.materialReal);
                //objDE.AddInParameter(objComando, "@materialProgramado", DbType.String, _BE_Ejecucion.materialProgramado);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public List<BE_MON_Ejecucion> F_spMON_GastosNE(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_GastosNE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_item = Convert.ToInt32(Reader["id_gastoNE"].ToString());
                        objENT.numero = Convert.ToInt32(Reader["autorizacion"].ToString());
                        objENT.monto = Convert.ToDouble(Reader["desembolso"].ToString()).ToString("N2");
                        objENT.Date_fecha = Convert.ToDateTime(Reader["fechaDesembolso"].ToString());
                        objENT.Tipo = Convert.ToInt32(Reader["id_tipoGastoNE"].ToString());
                        objENT.tipoEstado = (Reader["tipoGastoNE"].ToString());
                        objENT.urlDoc = (Reader["urlDoc"].ToString());
                        objENT.observacion = (Reader["observacion"].ToString());

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }
                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int I_spi_MON_GastosNE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_GastosNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@autorizacion", SqlDbType.Int).Value = _BE.numero;
                    cmd.Parameters.Add("@fechaDesembolso", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@monto", SqlDbType.VarChar, 50).Value = _BE.monto;
                    cmd.Parameters.Add("@id_tipoGasto", SqlDbType.Int).Value = _BE.id_tabla;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 200).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_GastosNE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_GastosNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_gastosNE", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@autorizacion", SqlDbType.Int).Value = _BE.numero;
                    cmd.Parameters.Add("@fechaDesembolso", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@monto", SqlDbType.VarChar, 50).Value = _BE.monto;
                    cmd.Parameters.Add("@id_tipoGasto", SqlDbType.Int).Value = _BE.id_tabla;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 200).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_MON_PersonalNE(BE_MON_Ejecucion _BE)
        {

            using (SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
                List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_PersonalNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@dni", SqlDbType.VarChar, 8000).Value = _BE.DNI;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_Ejecucion();

                            objENT.DNI = Reader["dni"].ToString();
                            objENT.nombre = Reader["nombre"].ToString();
                            objENT.tipoProfesion = Reader["profesion"].ToString();
                            objENT.NroAdicional = Reader["nroColegiatura"].ToString();
                            objENT.tipoAgente = Reader["cargoActual"].ToString();
                            objENT.pruebas = Reader["programa"].ToString();
                            objENT.snip = Reader["cod_snip"].ToString();
                            objENT.usuario = Reader["nom_proyecto"].ToString();
                            objENT.cargo = Reader["cargo"].ToString();
                            objENT.estadoResidente = Reader["estadoTecnico"].ToString();
                            objENT.estadoSituacional = Reader["estadoProyecto"].ToString();
                            objENT.Recomendacion = Reader["ProgramaNE"].ToString();

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public int spi_MON_EstadoEjecuccionPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EstadoEjecuccionPorContrata]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@Administrador", DbType.String, _BE_Ejecucion.administrador);
                objDE.AddInParameter(objComando, "@telefonoAdm", DbType.String, _BE_Ejecucion.telefono);
                objDE.AddInParameter(objComando, "@correoAdm", DbType.String, _BE_Ejecucion.correo);

                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);

                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaFinContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaFinContractual);
                objDE.AddInParameter(objComando, "@fechaFinReal", DbType.DateTime, _BE_Ejecucion.Date_fechaFinReal);
                objDE.AddInParameter(objComando, "@fechaFinProbable", DbType.DateTime, _BE_Ejecucion.Date_fechaFinProbable);

                objDE.AddInParameter(objComando, "@fechaEntregaTerreno", DbType.DateTime, _BE_Ejecucion.Date_fechaEntregaTerreno);

                objDE.AddInParameter(objComando, "@supervisor", DbType.String, _BE_Ejecucion.supervisorDesignado);
                objDE.AddInParameter(objComando, "@residente", DbType.String, _BE_Ejecucion.residenteObra);
                objDE.AddInParameter(objComando, "@inspector", DbType.String, _BE_Ejecucion.inspector);

                //objDE.AddInParameter(objComando, "@montoAdelantoDirecto", DbType.String, _BE_Ejecucion.montoAdelantoDirecto);
                //objDE.AddInParameter(objComando, "@montoAdelantoMaterial", DbType.String, _BE_Ejecucion.montoAdelantoMaterial);
                //objDE.AddInParameter(objComando, "@fechaAdelantoDirecto", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoDirecto);
                //objDE.AddInParameter(objComando, "@fechaAdelantoMAterial", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoMaterial);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_EstadoEjecuccionPE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EstadoEjecuccionPE]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@coordinadorMVCS", DbType.String, _BE_Ejecucion.coordinadorMVCS);
                objDE.AddInParameter(objComando, "@coordinadorUE", DbType.String, _BE_Ejecucion.coordinadorUE);
                objDE.AddInParameter(objComando, "@telefonoUE", DbType.String, _BE_Ejecucion.telefonoUE);
                objDE.AddInParameter(objComando, "@correoUE", DbType.String, _BE_Ejecucion.correoUE);
                objDE.AddInParameter(objComando, "@jefeUnidadImplementacion", DbType.String, _BE_Ejecucion.jefeUnidadImplementacion);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);
                objDE.AddInParameter(objComando, "@fechaInicioContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaInicioContractual);
                objDE.AddInParameter(objComando, "@fechaInicioReal", DbType.DateTime, _BE_Ejecucion.Date_fechaInicioReal);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaFinContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaFinContractual);
                objDE.AddInParameter(objComando, "@fechaFinReal", DbType.DateTime, _BE_Ejecucion.Date_fechaFinReal);

                objDE.AddInParameter(objComando, "@fechaEntregaTerreno", DbType.DateTime, _BE_Ejecucion.Date_fechaEntregaTerreno);

                objDE.AddInParameter(objComando, "@residente", DbType.String, _BE_Ejecucion.residenteObra);
                objDE.AddInParameter(objComando, "@inspector", DbType.String, _BE_Ejecucion.inspector);
                objDE.AddInParameter(objComando, "@gerenteObra", DbType.String, _BE_Ejecucion.gerenteObra);

                objDE.AddInParameter(objComando, "@id_tipoMonedaAdelanto", DbType.Int32, _BE_Ejecucion.id_tipoMoneda);
                objDE.AddInParameter(objComando, "@montoAdelantoDirecto", DbType.String, _BE_Ejecucion.montoAdelantoDirecto);
                objDE.AddInParameter(objComando, "@montoAdelantoMaterial", DbType.String, _BE_Ejecucion.montoAdelantoMaterial);
                objDE.AddInParameter(objComando, "@fechaAdelantoDirecto", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoDirecto);
                objDE.AddInParameter(objComando, "@fechaAdelantoMAterial", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoMaterial);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_EstadoEjecuccionPE32(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EstadoEjecuccionPE32]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);
                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaFinContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaFinContractual);
                objDE.AddInParameter(objComando, "@fechaFinReal", DbType.DateTime, _BE_Ejecucion.Date_fechaFinReal);

                objDE.AddInParameter(objComando, "@fechaRecepcion", DbType.DateTime, _BE_Ejecucion.Date_fechaRecepcion);

                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ejecucion.observacion);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_EstadoEjecuccionPE35(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_EstadoEjecuccionPE35]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ejecucion.observacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_DetalleEstadoEjecuccionPE35(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spi_DetalleEstadoEjecuccionPE35");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);
                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);

                objDE.AddInParameter(objComando, "@fechaFin", DbType.DateTime, _BE_Ejecucion.Date_fechaFin);
                objDE.AddInParameter(objComando, "@fechaFinContractual", DbType.DateTime, _BE_Ejecucion.Date_fechaFinContractual);
                objDE.AddInParameter(objComando, "@fechaFinReal", DbType.DateTime, _BE_Ejecucion.Date_fechaFinReal);

                objDE.AddInParameter(objComando, "@ampliacion", DbType.Int32, _BE_Ejecucion.ampliacion);

                objDE.AddInParameter(objComando, "@ciudad", DbType.String, _BE_Ejecucion.observacion);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spiu_MON_EjecucionSupervisionPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiu_MON_EjecucionSupervisionPorContrata]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Ejecucion.tipoFinanciamiento);

                objDE.AddInParameter(objComando, "@fechaInicio", DbType.DateTime, _BE_Ejecucion.Date_fechaInicio);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Ejecucion.plazoEjecuccion);

                objDE.AddInParameter(objComando, "@supervisor", DbType.String, _BE_Ejecucion.supervisorDesignado);

                objDE.AddInParameter(objComando, "@flagAdelanto", DbType.String, _BE_Ejecucion.flag);
                objDE.AddInParameter(objComando, "@montoAdelantoDirecto", DbType.String, _BE_Ejecucion.montoAdelantoDirecto);
                objDE.AddInParameter(objComando, "@fechaAdelantoDirecto", DbType.DateTime, _BE_Ejecucion.Date_fechaAdelantoDirecto);

                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Ejecucion.observacion);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Ejecucion.id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int U_spu_MON_InformeAvancePorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_InformeAvancePorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idAvanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;
                    cmd.Parameters.Add("@tipoValorizacion", SqlDbType.VarChar, 2).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@montoReajuste", SqlDbType.Money).Value = _BE.montoReajuste;
                    cmd.Parameters.Add("@montoDirecto", SqlDbType.Money).Value = _BE.montoAdelantoDirecto;
                    cmd.Parameters.Add("@montoMateriales", SqlDbType.Money).Value = _BE.montoAdelantoMaterial;

                    cmd.Parameters.Add("@montoEjecutado", SqlDbType.Money).Value = _BE.montoEjecutado;

                    cmd.Parameters.Add("@fisicoReal", SqlDbType.VarChar, 6).Value = _BE.FisicoEjec;
                    cmd.Parameters.Add("@financieroReal", SqlDbType.VarChar, 6).Value = _BE.financieroReal;
                    cmd.Parameters.Add("@financieroProgramado", SqlDbType.VarChar, 6).Value = _BE.financieroProgramado;

                    cmd.Parameters.Add("@IGV", SqlDbType.Money).Value = _BE.porcentaje;
                    cmd.Parameters.Add("@id_tipoEstadoSituacion", SqlDbType.Int).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@fechaPago", SqlDbType.DateTime).Value = _BE.Date_fechaPago;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_InformeAvanceAdicionalPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@tipoValorizacion", SqlDbType.VarChar, 2).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@montoReajuste", SqlDbType.Money).Value = _BE.montoReajuste;
                    cmd.Parameters.Add("@fisicoReal", SqlDbType.VarChar, 6).Value = _BE.fisicoReal;
                    cmd.Parameters.Add("@fisicoProgramado", SqlDbType.VarChar, 6).Value = _BE.fisicoProgramado;
                    cmd.Parameters.Add("@financieroReal", SqlDbType.VarChar, 6).Value = _BE.financieroReal;
                    cmd.Parameters.Add("@financieroProgramado", SqlDbType.VarChar, 6).Value = _BE.financieroProgramado;

                    cmd.Parameters.Add("@IGV", SqlDbType.Money).Value = _BE.porcentaje;
                    cmd.Parameters.Add("@id_tipoEstadoSituacion", SqlDbType.Int).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@fechaPago", SqlDbType.DateTime).Value = _BE.Date_fechaPago;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@montoDirecto", SqlDbType.Money).Value = _BE.montoAdelantoDirecto;
                    cmd.Parameters.Add("@montoMateriales", SqlDbType.Money).Value = _BE.montoAdelantoMaterial;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int U_spi_MON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_InformeAvanceAdicionalPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idAvanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;
                    cmd.Parameters.Add("@tipoValorizacion", SqlDbType.VarChar, 2).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@montoReajuste", SqlDbType.Money).Value = _BE.montoReajuste;
                    cmd.Parameters.Add("@fisicoReal", SqlDbType.VarChar, 6).Value = _BE.fisicoReal;
                    cmd.Parameters.Add("@fisicoProgramado", SqlDbType.VarChar, 6).Value = _BE.fisicoProgramado;
                    cmd.Parameters.Add("@financieroReal", SqlDbType.VarChar, 6).Value = _BE.financieroReal;
                    cmd.Parameters.Add("@financieroProgramado", SqlDbType.VarChar, 6).Value = _BE.financieroProgramado;

                    cmd.Parameters.Add("@IGV", SqlDbType.Money).Value = _BE.porcentaje;
                    cmd.Parameters.Add("@id_tipoEstadoSituacion", SqlDbType.Int).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@fechaPago", SqlDbType.DateTime).Value = _BE.Date_fechaPago;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@montoDirecto", SqlDbType.Money).Value = _BE.amortizacionDirecto;
                    cmd.Parameters.Add("@montoMateriales", SqlDbType.Money).Value = _BE.amortizacionMateriales;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int D_spd_MON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spd_MON_InformeAvanceAdicionalPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idAvanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_InformeAvanceSupervisionPorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_InformeAvanceSupervisionPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@tipoValorizacion", SqlDbType.VarChar, 2).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@fechaValorizacion", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@fechaPago", SqlDbType.DateTime).Value = _BE.Date_fechaPago;

                    cmd.Parameters.Add("@fisicoReal", SqlDbType.VarChar, 6).Value = _BE.fisicoReal;
                    cmd.Parameters.Add("@montoDirecto", SqlDbType.Money).Value = _BE.montoAdelantoDirecto;

                    cmd.Parameters.Add("@id_tipoEstadoSituacion", SqlDbType.Int).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;

                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_InformeAvanceSupervisionPorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_InformeAvanceSupervisionPorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idAvanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;

                    cmd.Parameters.Add("@tipoValorizacion", SqlDbType.VarChar, 2).Value = _BE.id_tipoValorizacion;

                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@fechaPago", SqlDbType.DateTime).Value = _BE.Date_fechaPago;

                    cmd.Parameters.Add("@fisicoReal", SqlDbType.VarChar, 6).Value = _BE.fisicoReal;
                    cmd.Parameters.Add("@montoDirecto", SqlDbType.Money).Value = _BE.montoAdelantoDirecto;

                    cmd.Parameters.Add("@id_tipoEstadoSituacion", SqlDbType.Int).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;

                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@tipoUD", SqlDbType.Int).Value = _BE.Tipo;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_Ejecucion> F_spMON_ProfesionalTecnicoSupervision(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_ProfesionalTecnicoSupervision";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_item = Convert.ToInt32(Reader["id_profesionalTecnico"].ToString());
                        objENT.nombre = Reader["nombre"].ToString();
                        objENT.cargo = Reader["cargo"].ToString();
                        objENT.Date_fecha = Convert.ToDateTime(Reader["fecha_asignacion"].ToString());
                        objENT.telefono = Reader["telefono"].ToString();
                        objENT.correo = Reader["correo"].ToString();
                        objENT.porcentaje = Reader["participacion"].ToString();

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }
                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }
        public int I_spi_MON_ProfesionalTecnicoSupervision(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_ProfesionalTecnicoSupervision", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 200).Value = _BE.nombre;
                    cmd.Parameters.Add("@cargo", SqlDbType.VarChar, 200).Value = _BE.cargo;
                    cmd.Parameters.Add("@fechaAsignacion", SqlDbType.DateTime).Value = _BE.Date_fecha;

                    cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 20).Value = _BE.telefono;
                    cmd.Parameters.Add("@correo", SqlDbType.VarChar, 50).Value = _BE.correo;
                    cmd.Parameters.Add("@participacion", SqlDbType.VarChar, 5).Value = _BE.porcentaje;
                    //cmd.Parameters.Add("@diasProgramado", SqlDbType.Int).Value = _BE.diasProgramado;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_ProfesionalTecnicoSupervision(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_ProfesionalTecnicoSupervision", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@idProfesional", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 200).Value = _BE.nombre;
                    cmd.Parameters.Add("@cargo", SqlDbType.VarChar, 200).Value = _BE.cargo;
                    cmd.Parameters.Add("@fechaAsignacion", SqlDbType.DateTime).Value = _BE.Date_fecha;

                    cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 20).Value = _BE.telefono;
                    cmd.Parameters.Add("@correo", SqlDbType.VarChar, 50).Value = _BE.correo;
                    cmd.Parameters.Add("@participacion", SqlDbType.VarChar, 5).Value = _BE.porcentaje;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_MON_AvanceFisicoPE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_AvanceFisicoPE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@id_tipoValorizacion", SqlDbType.Int).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@id_tipoMonedaMonto", SqlDbType.Int).Value = _BE.id_tipoMoneda;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@id_tipoMonedaContrapartida", SqlDbType.Int).Value = _BE.id_tipoMonedaContrapartida;
                    cmd.Parameters.Add("@montoContrapartida", SqlDbType.Money).Value = _BE.montoContrapartida;
                    cmd.Parameters.Add("@id_tipoMonedaRE", SqlDbType.Int).Value = _BE.id_tipoMonedaRE;
                    cmd.Parameters.Add("@montoRE", SqlDbType.Money).Value = _BE.montoRE;
                    cmd.Parameters.Add("@avance", SqlDbType.VarChar, 6).Value = _BE.avance;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int U_MON_AvanceFisicoPE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_MON_AvanceFisicoPE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_valorizacionPE", SqlDbType.Int).Value = _BE.id_tabla;

                    //cmd.Parameters.Add("@id_tipoValorizacion", SqlDbType.Int).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@id_tipoMonedaMonto", SqlDbType.Int).Value = _BE.id_tipoMoneda;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@id_tipoMonedaContrapartida", SqlDbType.Int).Value = _BE.id_tipoMonedaContrapartida;
                    cmd.Parameters.Add("@montoContrapartida", SqlDbType.Money).Value = _BE.montoContrapartida;
                    cmd.Parameters.Add("@id_tipoMonedaRE", SqlDbType.Int).Value = _BE.id_tipoMonedaRE;
                    cmd.Parameters.Add("@montoRE", SqlDbType.Money).Value = _BE.montoRE;
                    cmd.Parameters.Add("@avance", SqlDbType.VarChar, 6).Value = _BE.avance;
                    //cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_Ejecucion> F_MON_AvanceFisicoPE(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_AvanceFisicoPE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_valorizacionPE"].ToString());
                        objENT.id_tipoValorizacion = (Reader["id_tipoValorizacion"].ToString());

                        objENT.numero = Convert.ToInt32(Reader["numero"].ToString());

                        objENT.NroAdicional = Reader["NroAdicional"].ToString();
                        objENT.nroValorizacionAdicional = Reader["nroValorizacionAdicional"].ToString();

                        objENT.mes = Reader["mes"].ToString();
                        objENT.anio = Reader["anio"].ToString();

                        objENT.tipoValorizacion = Reader["tipoMoneda"].ToString();
                        objENT.monto = Convert.ToDouble(Reader["monto"].ToString()).ToString("N2");

                        objENT.id_tipoMonedaContrapartida = Convert.ToInt32(Reader["id_tipoMonedaContrapartida"].ToString());
                        objENT.tipoProfesion = Reader["tipoMonedaContrapartida"].ToString();
                        objENT.montoContrapartida = Convert.ToDouble(Reader["montoContrapartida"].ToString()).ToString("N2");

                        objENT.id_tipoMonedaRE = Convert.ToInt32(Reader["id_tipoMonedaRE"].ToString());
                        objENT.tipodocumento = Reader["tipoMonedaRE"].ToString();
                        objENT.montoRE = Convert.ToDouble(Reader["montoRE"].ToString()).ToString("N2");

                        objENT.avance = Reader["avanceFisico"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();

                        objENT.usuario = Reader["nombre"].ToString();
                        objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int I_MON_AvanceFisicoAdicionalPE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_AvanceFisicoAdicionalPE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@id_tipoValorizacion", SqlDbType.Int).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@id_tipoMonedaMonto", SqlDbType.Int).Value = _BE.id_tipoMoneda;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@id_tipoMonedaContrapartida", SqlDbType.Int).Value = _BE.id_tipoMonedaContrapartida;
                    cmd.Parameters.Add("@montoContrapartida", SqlDbType.Money).Value = _BE.montoContrapartida;
                    cmd.Parameters.Add("@id_tipoMonedaRE", SqlDbType.Int).Value = _BE.id_tipoMonedaRE;
                    cmd.Parameters.Add("@montoRE", SqlDbType.Money).Value = _BE.montoRE;
                    cmd.Parameters.Add("@avance", SqlDbType.VarChar, 6).Value = _BE.avance;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_MON_AvanceFisicoSupervisionPE(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_AvanceFisicoSupervisionPE]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@id_tipoValorizacion", SqlDbType.Int).Value = _BE.id_tipoValorizacion;
                    cmd.Parameters.Add("@id_tipoMonedaMonto", SqlDbType.Int).Value = _BE.id_tipoMoneda;
                    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = _BE.monto;
                    cmd.Parameters.Add("@id_tipoMonedaContrapartida", SqlDbType.Int).Value = _BE.id_tipoMonedaContrapartida;
                    cmd.Parameters.Add("@montoContrapartida", SqlDbType.Money).Value = _BE.montoContrapartida;
                    cmd.Parameters.Add("@id_tipoMonedaContrapartida2", SqlDbType.Int).Value = _BE.tipoProfesion;
                    cmd.Parameters.Add("@montoContrapartida2", SqlDbType.Money).Value = _BE.montoAdelantoMaterial;
                    cmd.Parameters.Add("@id_tipoMonedaRE", SqlDbType.Int).Value = _BE.id_tipoMonedaRE;
                    cmd.Parameters.Add("@montoRE", SqlDbType.Money).Value = _BE.montoRE;
                    cmd.Parameters.Add("@id_tipoMonedaRE2", SqlDbType.Int).Value = _BE.tipoAgente;
                    cmd.Parameters.Add("@montoRE2", SqlDbType.Money).Value = _BE.montoTotal;
                    cmd.Parameters.Add("@avance", SqlDbType.VarChar, 6).Value = _BE.avance;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = _BE.observacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_MON_AvanceFisicoSupervisionPE(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_AvanceFisicoSupervisionPE";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_avanceFisico = Convert.ToInt32(Reader["id_valorizacionPE"].ToString());
                        objENT.id_tipoValorizacion = (Reader["id_tipoValorizacion"].ToString());

                        objENT.NroAdicional = Reader["NroAdicional"].ToString();
                        objENT.nroValorizacionAdicional = Reader["nroValorizacionAdicional"].ToString();

                        objENT.mes = Reader["mes"].ToString();
                        objENT.anio = Reader["anio"].ToString();

                        objENT.tipoValorizacion = Reader["tipoMoneda"].ToString();
                        objENT.monto = Convert.ToDouble(Reader["monto"].ToString()).ToString("N2");

                        objENT.tipoProfesion = Reader["tipoMonedaContrapartida"].ToString();
                        objENT.montoContrapartida = Convert.ToDouble(Reader["montoContrapartida"].ToString()).ToString("N2");
                        objENT.id_tipodocumento = Reader["tipoMonedaContrapartida2"].ToString();
                        objENT.montoAdelantoMaterial = Convert.ToDouble(Reader["montoContrapartida2"].ToString()).ToString("N2");

                        objENT.tipodocumento = Reader["tipoMonedaRE"].ToString();
                        objENT.montoRE = Convert.ToDouble(Reader["montoRE"].ToString()).ToString("N2");
                        objENT.tipoAgente = Reader["tipoMonedaRe2"].ToString();
                        objENT.montoTotal = Convert.ToDouble(Reader["montoRE2"].ToString()).ToString("N2");

                        objENT.avance = Reader["avanceFisico"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();

                        objENT.usuario = Reader["nombre"].ToString();
                        objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Ejecucion> F_MON_InformeSupervision(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_InformeSupervision";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                cmd.Parameters.Add("@id_tipoInforme", SqlDbType.Int).Value = _BE.id_tipodocumento;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_item = Convert.ToInt32(Reader["id_informe"].ToString());
                        //objENT.id_tipodocumento = (Reader["id_tipoInforme"].ToString());
                        //objENT.tipodocumento = Reader["tipoInforme"].ToString();
                        if (Reader["fechaInforme"].ToString().Length > 0)
                        {
                            objENT.dtFechaProgramada = Convert.ToDateTime(Reader["fechaInforme"].ToString());
                        }

                        objENT.mes = Reader["mes"].ToString();
                        objENT.Date_fechaInforme = Convert.ToDateTime(Reader["fechaPresentacion"].ToString());
                        objENT.estadoSituacional = Reader["detalleEstadoSituacional"].ToString();
                        objENT.observacion = Reader["observacion"].ToString();

                        objENT.urlDoc = Reader["urlDoc"].ToString();
                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fecha_update"].ToString() == "")
                        {
                            objENT.fecha_update = null;
                        }
                        else
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int I_MON_InformeSupervision(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_InformeSupervision]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@id_tipoInforme", SqlDbType.Int).Value = _BE.id_tipodocumento;

                    cmd.Parameters.Add("@fechaInforme", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@DetalleEstado", SqlDbType.VarChar, 800).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 800).Value = _BE.observacion;

                    //cmd.Parameters.Add("@NroValorizacion", SqlDbType.Int).Value = _BE.NroAdicional;
                    //cmd.Parameters.Add("@NroValorizacionAdicional", SqlDbType.Int).Value = _BE.nroValorizacionAdicional;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 100).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_MON_InformeSupervision(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spud_MON_InformeSupervision]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_informe", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;

                    cmd.Parameters.Add("@fechaPresentacion", SqlDbType.DateTime).Value = _BE.Date_fechaInforme;
                    cmd.Parameters.Add("@DetalleEstado", SqlDbType.VarChar, 800).Value = _BE.estadoSituacional;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 800).Value = _BE.observacion;

                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 100).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        //PersonalNE==================================================================================================

        public IEnumerable<BE_MON_Ejecucion> ValidarTecnicoPersonalNE(List<BE_MON_Ejecucion> PersonalNE, int pid_usuario)
        {
            TecnicoPersonalNEType listaPersonalNE = new TecnicoPersonalNEType();
            listaPersonalNE.AddRange(PersonalNE);

            using (SqlConnection Cnx = new SqlConnection(strCadenaConexion))
            {
                List<BE_MON_Ejecucion> lista = new List<BE_MON_Ejecucion>();
                SqlCommand cmd = default(SqlCommand);

                string detalleError;

                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[dbo].[sps_MON_TecnicoPersonalNE_ValidarCarga]";
                    cmd.CommandTimeout = 36000;

                    // Add the input parameter and set its properties.
                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = "@PersonalNE";
                    parameter.SqlDbType = SqlDbType.Structured;
                    parameter.Direction = ParameterDirection.Input;
                    parameter.TypeName = "[dbo].[T_TecnicoPersonalNE_Type]";
                    parameter.Value = listaPersonalNE;
                    //// Add the parameter to the Parameters collection. 
                    cmd.Parameters.Add(parameter);

                    parameter = new SqlParameter();
                    parameter.ParameterName = "@id_usuario";
                    parameter.SqlDbType = SqlDbType.Int;
                    parameter.Value = pid_usuario;
                    cmd.Parameters.Add(parameter);

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        //int id_tecnico = Reader.GetOrdinal("id_tecnico");
                        //int nivel = Reader.GetOrdinal("nivel");
                        int nombre = Reader.GetOrdinal("nombre");
                        //int activo = Reader.GetOrdinal("activo");
                        int id_tipoPrograma = Reader.GetOrdinal("id_tipoPrograma");
                        int tipoPrograma = Reader.GetOrdinal("tipoPrograma");
                        int correo = Reader.GetOrdinal("correo");
                        int id_tipoAgente = Reader.GetOrdinal("id_tipoAgente");
                        int tipoAgente = Reader.GetOrdinal("tipoAgente");
                        int dni = Reader.GetOrdinal("dni");
                        int id_tipoProfesion = Reader.GetOrdinal("id_tipoProfesion");
                        int tipoProfesion = Reader.GetOrdinal("tipoProfesion");
                        int nroColegiatura = Reader.GetOrdinal("nroColegiatura");

                        int cantidadDNI = Reader.GetOrdinal("cantidadDNI");
                        //int Numero = Reader.GetOrdinal("Numero");

                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                detalleError = "";

                                var obj = new BE_MON_Ejecucion();
                                //if (!Reader.IsDBNull(id_tecnico))
                                //{
                                //    obj.id_tecnico = Reader.GetInt32(id_tecnico);

                                //}
                                //if (!Reader.IsDBNull(nivel))
                                //    obj.nivel = Reader.GetString(nivel);

                                if (!Reader.IsDBNull(nombre))
                                    obj.nombre = Reader.GetString(nombre);

                                //if (!Reader.IsDBNull(activo))
                                //    obj.activo = Convert.ToBoolean(activo);
                                //    //obj.activoS = obj.activo.ToString();

                                if (!Reader.IsDBNull(tipoPrograma))
                                    obj.tipoPrograma = Reader.GetString(tipoPrograma);

                                if (!Reader.IsDBNull(correo))
                                    obj.correo = Reader.GetString(correo);

                                if (!Reader.IsDBNull(tipoAgente))
                                    obj.tipoAgente = Reader.GetString(tipoAgente);

                                if (!Reader.IsDBNull(dni))
                                    obj.DNI = Reader.GetString(dni);

                                if (!Reader.IsDBNull(tipoProfesion))
                                    obj.tipoProfesion = Reader.GetString(tipoProfesion);

                                if (!Reader.IsDBNull(nroColegiatura))
                                    obj.nroColegiatura = Reader.GetString(nroColegiatura);

                                if (Reader.IsDBNull(id_tipoPrograma))
                                    detalleError = detalleError + " Tipo programa no existe | ";

                                if (Reader.IsDBNull(id_tipoAgente))
                                    detalleError = detalleError + " Tipo agente no existe | ";

                                if (obj.tipoProfesion != null)
                                {
                                    if (Reader.IsDBNull(id_tipoProfesion))
                                        detalleError = detalleError + " Tipo profesion no existe | ";
                                }

                                if (Reader.GetInt32(cantidadDNI) > 0)
                                    detalleError = detalleError + " DNI ya se encuentra registrado | ";


                                obj.Detalle_Error = detalleError;

                                //
                                //obj.Numero = Reader.GetInt64(Numero);
                                lista.Add(obj);
                            }
                        }

                    }

                    return lista;
                }
                catch (Exception ex)
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_ReProgramacionInformePorContrata(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_ReProgramacionInformePorContrata", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_avanceFisico", SqlDbType.Int).Value = _BE.id_avanceFisico;
                    cmd.Parameters.Add("@fechaInicioInformar", SqlDbType.DateTime).Value = _BE.Date_fechaInicio;
                    cmd.Parameters.Add("@fechaFinInformar", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@tipoUD", SqlDbType.Int).Value = _BE.Tipo;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        //public DataSet spMON_MetasPNSR(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("spMON_MetasPNSR");
        //        objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
        //        return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        //public int U_MON_MetasPNSR(BE_MON_Ejecucion _BE)
        //{
        //    using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        //    {
        //        SqlCommand cmd;
        //        try
        //        {
        //            cmd = new SqlCommand("[spu_MON_MetasPNSR]", strCnx);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
        //            cmd.Parameters.Add("@nroAgua", SqlDbType.Int).Value = _BE.nroAgua;
        //            cmd.Parameters.Add("@nroAlcantarillado", SqlDbType.Int).Value = _BE.nroAlcantarillado;
        //            cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

        //            cmd.Connection.Open();
        //            return Convert.ToInt32(cmd.ExecuteScalar());
        //        }
        //        catch (Exception ex)
        //        {
        //            throw;
        //        }
        //        finally
        //        {
        //            cmd = null;
        //            if (strCnx.State == ConnectionState.Open)
        //            {
        //                strCnx.Close();
        //            }
        //        }
        //    }
        //}

        public List<BE_MON_BANDEJA> F_MON_TipoSistemaDrenaje(int idSector)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoSistemaDrenaje";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@idSector", SqlDbType.Int).Value = idSector;
                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public List<BE_MON_Ejecucion> F_MON_SistemaDrenajeDetalle(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_SistemaDrenajeDetalle";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();

                        objENT.id_item = Convert.ToInt32(Reader["id_sistemaDrenajeDetalle"].ToString());

                        objENT.id_tabla = Convert.ToInt32(Reader["id_tipoSistemaDrenaje"].ToString());
                        objENT.tipoEstado = Reader["TipoComponente"].ToString();
                        objENT.nombre = Reader["TipoSistemaDrenaje"].ToString();
                        objENT.observacion = Reader["comentario"].ToString();

                        objENT.usuario = Reader["usuario"].ToString();

                        if (Reader["fecha_update"].ToString() != null)
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fecha_update"].ToString());
                        }



                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int I_MON_SistemaDrenajeDetalle(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_SistemaDrenajeDetalle]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;
                    cmd.Parameters.Add("@id_TipoSistemaDrenaje", SqlDbType.Int).Value = _BE.id_item;

                    cmd.Parameters.Add("@comentario", SqlDbType.VarChar, 3000).Value = _BE.observacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int U_MON_EvaluacionRecomendacionActaVisita(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spu_MON_EvaluacionRecomendacionActaVisita]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;
                    cmd.Parameters.Add("@idTipoFuncionamiento", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@comentarioFuncionamiento", SqlDbType.VarChar, 3000).Value = _BE.Evaluacion;
                    cmd.Parameters.Add("@flagInaugurado", SqlDbType.Int).Value = _BE.flag;
                    cmd.Parameters.Add("@flagCumplimientoInicio", SqlDbType.VarChar, 1).Value = _BE.flagVerificado;
                    cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 3000).Value = _BE.observacion;
                    cmd.Parameters.Add("@recomendacion", SqlDbType.VarChar, 3000).Value = _BE.Recomendacion;
                    cmd.Parameters.Add("@entregaDocumento", SqlDbType.VarChar, 3000).Value = _BE.comentario;
                    cmd.Parameters.Add("@fechaActa", SqlDbType.DateTime).Value = _BE.Date_fechaVisita;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_Ejecucion> F_MON_EvaluacionRecomendacionActaVisita(BE_MON_Ejecucion _BE)
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_Ejecucion objENT = new BE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = new List<BE_MON_Ejecucion>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_EvaluacionRecomendacionActaVisita";
                cmd.CommandTimeout = 36000;

                cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_Ejecucion();


                        objENT.observacion = Reader["observacionActaVisita"].ToString();
                        objENT.Recomendacion = Reader["recomendacionActaVisita"].ToString();

                        objENT.usuario = Reader["usuario"].ToString();
                        if (Reader["fechaUpdataActaVisita"].ToString() != "")
                        {
                            objENT.fecha_update = Convert.ToDateTime(Reader["fechaUpdataActaVisita"].ToString());
                        }

                        objCollection.Add(objENT);

                        objENT = null;
                    }

                }
                //   Reader.Close();
                return objCollection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int U_MON_EvaluacionRecomendacionInformeVisita(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spu_MON_EvaluacionRecomendacionInformeVisita]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;

                    cmd.Parameters.Add("@NroInformeVisita", SqlDbType.VarChar, 300).Value = _BE.informe;
                    cmd.Parameters.Add("@DirigidoAInforme", SqlDbType.VarChar, 300).Value = _BE.dirigidoA;
                    cmd.Parameters.Add("@DirigidoCargoAInforme", SqlDbType.VarChar, 300).Value = _BE.cargo;
                    cmd.Parameters.Add("@AsuntoInforme", SqlDbType.VarChar, 300).Value = _BE.asunto;
                    cmd.Parameters.Add("@ReferenciaInforme", SqlDbType.VarChar, 3000).Value = _BE.referencia;
                    cmd.Parameters.Add("@CabeceraInforme", SqlDbType.VarChar, 3000).Value = _BE.cabecera;
                    cmd.Parameters.Add("@fechaInforme", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@metasInforme", SqlDbType.VarChar, 8000).Value = _BE.metas;
                    cmd.Parameters.Add("@antecedenteInforme", SqlDbType.VarChar, 8000).Value = _BE.antecedentes;
                    cmd.Parameters.Add("@comentarioFinanciamientoInforme", SqlDbType.VarChar, 8000).Value = _BE.comentario; //comentarioFinanciamientoInforme
                    cmd.Parameters.Add("@analisisInforme", SqlDbType.VarChar, 8000).Value = _BE.analisis;
                    cmd.Parameters.Add("@conclusionInforme", SqlDbType.VarChar, 8000).Value = _BE.observacion;
                    cmd.Parameters.Add("@recomendacionInforme", SqlDbType.VarChar, 8000).Value = _BE.Recomendacion;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_EvaluacionRecomendacionActaVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EvaluacionRecomendacionActaVisita]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_Adelanto(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Adelanto]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);
                objDE.AddInParameter(objComando, "@flagObra", DbType.Int32, _BE_Ejecucion.flag);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int UD_MON_SistemaDrenajeDetalle(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spud_MON_SistemaDrenajeDetalle]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_sistemaDrenajeDetalle", SqlDbType.Int).Value = _BE.id_tabla;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;
                    cmd.Parameters.Add("@id_TipoSistemaDrenaje", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@comentario", SqlDbType.VarChar, 3000).Value = _BE.observacion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_MON_RevisionItemAdministrativoVisita(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spiu_MON_RevisionItemAdministrativoVisita]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;
                    cmd.Parameters.Add("@id_itemAdministrativoVisita", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@result", SqlDbType.Int).Value = _BE.flag;
                    cmd.Parameters.Add("@comentario", SqlDbType.VarChar, 800).Value = _BE.comentario;
                    //cmd.Parameters.Add("@entregaDocumento", SqlDbType.VarChar, 3000).Value = _BE.comentario;
                    cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_RevisionItemAdministrativoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_RevisionItemAdministrativoVisita]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_DetalleFotoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_DetalleFotoVisita]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_DetalleFotoVisitaMovil(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_DetalleFotoVisitaMovil]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet paSSP_MON_rFotoCuaderoObra(Int32 pSNIP)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_MON_rFotoCuaderoObra]");
                objDE.AddInParameter(objComando, "@SNIP", DbType.Int32, pSNIP);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_MON_DetalleFotoVisita(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_DetalleFotoVisita]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;
                    cmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 2000).Value = _BE.comentario;
                    cmd.Parameters.Add("@urlFoto", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_MON_DetalleFotoVisita(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spud_MON_DetalleFotoVisita]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 2000).Value = _BE.comentario;
                    cmd.Parameters.Add("@urlFoto", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public int I_MON_VisitaFotoMovil(int pIdEvaluacion, string pJsonFotos, int pIdUsuario)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_VisitaFotoMovil]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = pIdEvaluacion;
                    cmd.Parameters.Add("@vJsonFotos", SqlDbType.VarChar, 2000).Value = pJsonFotos;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = pIdUsuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int U_MON_EvaluacionRecomendacionParalizacion(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spu_MON_EvaluacionRecomendacionParalizacion]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;

                    cmd.Parameters.Add("@cordinadorMVCS", SqlDbType.VarChar, 300).Value = _BE.coordinadorMVCS;
                    cmd.Parameters.Add("@fechaParalizacion", SqlDbType.DateTime).Value = _BE.Date_fechaParalizacion;
                    cmd.Parameters.Add("@fechaDiagnostico", SqlDbType.DateTime).Value = _BE.Date_fechaDiagnostico;
                    cmd.Parameters.Add("@fechaReinicio", SqlDbType.DateTime).Value = _BE.Date_fechaReinicio;
                    cmd.Parameters.Add("@problematica", SqlDbType.VarChar, 3000).Value = _BE.Evaluacion;
                    cmd.Parameters.Add("@diagnostico", SqlDbType.VarChar, 3000).Value = _BE.diagnostico;
                    cmd.Parameters.Add("@fechaFinanciamientoExpTecnico", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    Int32 valor = Convert.ToInt32(cmd.ExecuteScalar());
                    return valor;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_SeguimientoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_SeguimientoParalizacion]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, _BE_Ejecucion.Id_ejecucionRecomendacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_MON_SeguimientoParalizacion(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_SeguimientoParalizacion]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_evaluacionRecomendacion", SqlDbType.Int).Value = _BE.Id_ejecucionRecomendacion;
                    cmd.Parameters.Add("@id_tipoMonitoreo", SqlDbType.Int).Value = _BE.id_tipomonitoreo;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@situacionActual", SqlDbType.VarChar, 3000).Value = _BE.situacion;
                    cmd.Parameters.Add("@accionesRealizadas", SqlDbType.VarChar, 3000).Value = _BE.accionesRealizadas;
                    cmd.Parameters.Add("@accionesPorRealizar", SqlDbType.VarChar, 3000).Value = _BE.accionesPorRealizar;
                    cmd.Parameters.Add("@urlActa", SqlDbType.VarChar, 200).Value = _BE.UrlActa;
                    cmd.Parameters.Add("@urlInforme", SqlDbType.VarChar, 200).Value = _BE.UrlInforme;
                    cmd.Parameters.Add("@urlOficio", SqlDbType.VarChar, 200).Value = _BE.UrlOficio;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_MON_SeguimientoParalizacion(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spud_MON_SeguimientoParalizacion]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_seguimientoParalizacion", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@id_tipoMonitoreo", SqlDbType.Int).Value = _BE.id_tipomonitoreo;
                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@situacionActual", SqlDbType.VarChar, 3000).Value = _BE.situacion;
                    cmd.Parameters.Add("@accionesRealizadas", SqlDbType.VarChar, 3000).Value = _BE.accionesRealizadas;
                    cmd.Parameters.Add("@accionesPorRealizar", SqlDbType.VarChar, 3000).Value = _BE.accionesPorRealizar;
                    cmd.Parameters.Add("@urlActa", SqlDbType.VarChar, 200).Value = _BE.UrlActa;
                    cmd.Parameters.Add("@urlInforme", SqlDbType.VarChar, 200).Value = _BE.UrlInforme;
                    cmd.Parameters.Add("@urlOficio", SqlDbType.VarChar, 200).Value = _BE.UrlOficio;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;
                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_EvaluacionRecomendacionAccionesSeleccionar(int Id_ejecucionRecomendacion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_EvaluacionRecomendacionAccionesSeleccionar]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, Id_ejecucionRecomendacion);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet U_MON_GuardarRecomendacionAcciones(DataTable DtTableEvaluacionRecomendacionAcciones)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    //guardar Acciones de las paralizaciones               
                    cmd = new SqlCommand("spMON_GuardarRecomendacionAcciones", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = cmd.Parameters.AddWithValue("@VT_TableEvaluacionRecomendacionAcciones", DtTableEvaluacionRecomendacionAcciones);
                    tvpParam.SqlDbType = SqlDbType.Structured;
                    tvpParam.TypeName = "dbo.TableEvaluacionRecomendacionAcciones";

                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();

                    cmd.Connection.Open();
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int paSSP_MON_EvaluacionRecomendacionViviendas(int id_evaluacionRecomendacion, int? Concluidas, int? EnEjecucion,
            int? PorIniciar, int? Paralizadas)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[paSSP_MON_EvaluacionRecomendacionViviendas]");
                objDE.AddInParameter(objComando, "@id_evaluacionRecomendacion", DbType.Int32, id_evaluacionRecomendacion);
                objDE.AddInParameter(objComando, "@Concluidas", DbType.Int32, Concluidas);
                objDE.AddInParameter(objComando, "@EnEjecucion", DbType.Int32, EnEjecucion);
                objDE.AddInParameter(objComando, "@PorIniciar", DbType.Int32, PorIniciar);
                objDE.AddInParameter(objComando, "@Paralizadas", DbType.Int32, Paralizadas);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet spMON_CombosSeguimientoPCM()
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_CombosSeguimientoPCM]");

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_SeguimientoPCM(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_SeguimientoPCM]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Ejecucion.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_MON_SeguimientoPCM(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_MON_SeguimientoPCM]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@idEtapaCompromiso", SqlDbType.Int).Value = _BE.id_etapaCompromiso;
                    cmd.Parameters.Add("@mes", SqlDbType.Int).Value = _BE.mes;
                    cmd.Parameters.Add("@anio", SqlDbType.Int).Value = _BE.anio;
                    cmd.Parameters.Add("@idCompromiso", SqlDbType.Int).Value = _BE.id_gradoCumplimiento;
                    cmd.Parameters.Add("@idBrecha", SqlDbType.Int).Value = _BE.id_cierreBrecha;
                    cmd.Parameters.Add("@fechaFin", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_MON_SeguimientoPCM(BE_MON_Ejecucion _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spud_MON_SeguimientoPCM]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_seguimientoPCM", SqlDbType.Int).Value = _BE.id_item;
                    cmd.Parameters.Add("@idEtapaCompromiso", SqlDbType.Int).Value = _BE.id_etapaCompromiso;
                    cmd.Parameters.Add("@mes", SqlDbType.Int).Value = _BE.mes;
                    cmd.Parameters.Add("@anio", SqlDbType.Int).Value = _BE.anio;
                    cmd.Parameters.Add("@idCompromiso", SqlDbType.Int).Value = _BE.id_gradoCumplimiento;
                    cmd.Parameters.Add("@idBrecha", SqlDbType.Int).Value = _BE.id_cierreBrecha;
                    cmd.Parameters.Add("@fechaFin", SqlDbType.DateTime).Value = _BE.Date_fechaFin;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;
                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.Tipo;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_MON_CompromisoGEI(int _idProyecto, string _flagEnergia, float _undEnergia, string _flagNuevoPtar, float _undNuevoPtarCarga, string _flagDigestores, float _undDigestoresCapacidad, int _idUsuario)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spiu_MON_CompromisoGEI]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _idProyecto;
                    cmd.Parameters.Add("@flagEnergia", SqlDbType.VarChar, 1).Value = _flagEnergia;
                    cmd.Parameters.Add("@undEnergia", SqlDbType.Float).Value = _undEnergia;
                    cmd.Parameters.Add("@flagNuevoPtar", SqlDbType.VarChar, 1).Value = _flagNuevoPtar;
                    cmd.Parameters.Add("@undNuevoPtarCarga", SqlDbType.Float).Value = _undNuevoPtarCarga;
                    cmd.Parameters.Add("@flagDigestores", SqlDbType.VarChar, 1).Value = _flagDigestores;
                    cmd.Parameters.Add("@undDigestoresCapacidad", SqlDbType.Float).Value = _undDigestoresCapacidad;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _idUsuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_CompromisoGEI(int _idProyecto)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_CompromisoGEI]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _idProyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }



        public DataSet spi_MON_Adelanto(BE_MON_Ejecucion _BE)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Adelanto]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE.id_proyecto);
                objDE.AddInParameter(objComando, "@fechaAdelanto", DbType.DateTime, _BE.Date_fecha);
                objDE.AddInParameter(objComando, "@nroAdelanto", DbType.String, _BE.nroAdelanto);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE.monto);
                objDE.AddInParameter(objComando, "@idTipoAdelanto", DbType.Int32, _BE.idTipoAdelanto);
                objDE.AddInParameter(objComando, "@flagObra", DbType.Int32, _BE.flag);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spud_MON_Adelanto(BE_MON_Ejecucion _BE)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Adelanto]");
                objDE.AddInParameter(objComando, "@id_adelanto", DbType.Int32, _BE.id_item);
                objDE.AddInParameter(objComando, "@fechaAdelanto", DbType.DateTime, _BE.Date_fecha);
                objDE.AddInParameter(objComando, "@nroAdelanto", DbType.String, _BE.nroAdelanto);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE.monto);
                objDE.AddInParameter(objComando, "@idTipoAdelanto", DbType.Int32, _BE.idTipoAdelanto);
                objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE.Tipo);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);


                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public IEnumerable<DataTable> CargaSiaf22(List<BE_MON_SIAF_PNSR> PersonalNE, int pIdUsuario, string pAnio, string pMes)
        {
            DataSet ds = new DataSet();
            SiafPnsrType listaPersonalNE = new SiafPnsrType();
            listaPersonalNE.AddRange(PersonalNE);
            List<DataTable> lista = new List<DataTable>();

            using (SqlConnection Cnx = new SqlConnection(strCadenaConexion))
            {
                SqlCommand cmd;
                cmd = new SqlCommand("spi_MON_SiafPnsr", Cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id_usuario", SqlDbType.VarChar, 100).Value = pIdUsuario;
                cmd.Parameters.Add("@pMes", SqlDbType.VarChar, 2).Value = pMes;
                cmd.Parameters.Add("@pAnio", SqlDbType.VarChar, 4).Value = pAnio;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@SiafPnsr";
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.Direction = ParameterDirection.Input;
                parameter.TypeName = "[dbo].[T_SiafPnsr_Type]";
                parameter.Value = listaPersonalNE;
                cmd.Parameters.Add(parameter);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                Cnx.Open();
                da.Fill(ds);
                Cnx.Close();

                if (ds.Tables.Count > 0)
                {
                    lista.Add(ds.Tables[0]);
                }
                return lista;
            }

        }
    }
}
