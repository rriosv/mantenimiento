﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DE_MON_Calidad
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public int spi_MON_PruebaVerificada(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_PruebaVerificada]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Calidad.id_proyecto);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Calidad.Date_Fecha);
                objDE.AddInParameter(objComando, "@tipoPrueba", DbType.String, _BE_Calidad.tipoPrueba);
                objDE.AddInParameter(objComando, "@Estado", DbType.String, _BE_Calidad.estado);
                objDE.AddInParameter(objComando, "@ResultadoFinal", DbType.String, _BE_Calidad.resultadoFinal);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Calidad.urlDoc);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Calidad.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_PruebaVerificada(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_PruebaVerificada]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Calidad.id_proyecto);
          
                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_PruebaCalidad(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_PruebaCalidad]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Calidad.id_proyecto);
                objDE.AddInParameter(objComando, "@evaluacionFinal", DbType.String, _BE_Calidad.evaluacionFinal);
                objDE.AddInParameter(objComando, "@deficiencias", DbType.String, _BE_Calidad.deficiencias);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Calidad.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet spMON_PruebaCalidad(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_PruebaCalidad]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Calidad.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        //public DataSet spMON_TipoResultadoFinal()
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_TipoResultadoFinal]");
        //         return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}
        public List<BE_MON_BANDEJA> F_spMON_TipoResultadoFinal()
        {
            SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

            BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_TipoResultadoFinal";
                cmd.CommandTimeout = 36000;

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    int valor = Reader.GetOrdinal("valor");
                    int nombre = Reader.GetOrdinal("nombre");

                    while (Reader.Read())
                    {
                        objENT = new BE_MON_BANDEJA();

                        objENT.valor = Reader.GetInt32(valor);
                        objENT.nombre = Reader.GetString(nombre);

                        objCollection.Add(objENT);

                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }            
        //public DataSet spMON_TipoEstadoPrueba()
        //{
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("[spMON_TipoEstadoPrueba]");
        //         return objDE.ExecuteDataSet(objComando);

        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        //public List<BE_MON_BANDEJA> F_spMON_TipoEstadoPrueba()
        //{
        //    SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        //    BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
        //    List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
        //    SqlCommand cmd = default(SqlCommand);
        //    try
        //    {
        //        cmd = new SqlCommand();
        //        cmd.Connection = Cnx;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "spMON_TipoEstadoPrueba";
        //        cmd.CommandTimeout = 36000;

        //        cmd.Connection.Open();
        //        using (SqlDataReader Reader = cmd.ExecuteReader())
        //        {
        //            int valor = Reader.GetOrdinal("valor");
        //            int nombre = Reader.GetOrdinal("nombre");

        //            while (Reader.Read())
        //            {
        //                objENT = new BE_MON_BANDEJA();

        //                objENT.valor = Reader.GetInt32(valor);
        //                objENT.nombre = Reader.GetString(nombre);

        //                objCollection.Add(objENT);

        //                objENT = null;
        //            }
        //        }
        //        return objCollection;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        cmd = null;
        //        if (Cnx.State == ConnectionState.Open)
        //        {
        //            Cnx.Close();
        //        }
        //    }

        //}            

        public int spud_MON_PruebaVerificada_Eliminar(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_PruebaVerificada_Eliminar]");

                objDE.AddInParameter(objComando, "@id_prueba_verificada", DbType.Int32, _BE_Calidad.id_prueba_verificada);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Calidad.id_usuario);
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public int spud_MON_PruebaVerificada_Editar(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_PruebaVerificada_Editar]");

                objDE.AddInParameter(objComando, "@id_prueba_verificada", DbType.Int32, _BE_Calidad.id_prueba_verificada);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Calidad.id_usuario);
                objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_Calidad.Date_Fecha);
                objDE.AddInParameter(objComando, "@tipoPrueba", DbType.String, _BE_Calidad.tipoPrueba);
                objDE.AddInParameter(objComando, "@Estado", DbType.String, _BE_Calidad.estado);
                objDE.AddInParameter(objComando, "@ResultadoFinal", DbType.String, _BE_Calidad.resultadoFinal);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Calidad.urlDoc);
             

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }






    }
}
