﻿using DataAccess.MappingType;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DE_PadronBeneficiario
    {
        private string strCadenaConexion = ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public List<BE_MON_PadronBeneficiario> Listar_PadronBeneficiario(int idProyecto)
        {
            using (SqlConnection Cnx = new SqlConnection(strCadenaConexion))
            {
                List<BE_MON_PadronBeneficiario> lista = new List<BE_MON_PadronBeneficiario>();
                SqlCommand cmd = default(SqlCommand);

                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sps_MON_PadronBeneficiario";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = idProyecto;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int id_padronBeneficiario = Reader.GetOrdinal("id_padronBeneficiario");
                        int Apellido_Paterno = Reader.GetOrdinal("Apellido_Paterno");
                        int Apellido_Materno = Reader.GetOrdinal("Apellido_Materno");
                        int Nombres = Reader.GetOrdinal("Nombres");
                        int id_sexo = Reader.GetOrdinal("id_sexo");
                        int Sexo = Reader.GetOrdinal("Sexo");
                        int Fecha_Nacimiento = Reader.GetOrdinal("Fecha_Nacimiento");
                        int id_tipoDocumentoMiembroNE = Reader.GetOrdinal("id_tipoDocumentoMiembroNE");
                        int Tipo_DocMiembroNE = Reader.GetOrdinal("Tipo_DocMiembroNE");
                        int DNI = Reader.GetOrdinal("DNI");
                        int id_tipoDomicilio = Reader.GetOrdinal("id_tipoDomicilio");
                        int Tipo_Domicilio = Reader.GetOrdinal("Tipo_Domicilio");
                        int Direccion_Domicilio = Reader.GetOrdinal("Direccion_Domicilio");
                        int Nro_Domicilio = Reader.GetOrdinal("Nro_Domicilio");
                        int id_tipoBeneficio = Reader.GetOrdinal("id_tipoBeneficio");
                        int Tipo_Beneficio = Reader.GetOrdinal("Tipo_Beneficio");
                        int id_estadoBeneficiario = Reader.GetOrdinal("id_estadoBeneficiario");
                        int Estado_Beneficiario = Reader.GetOrdinal("Estado_Beneficiario");
                        int Nro_MiembrosH = Reader.GetOrdinal("Nro_MiembrosH");
                        int Nro_MiembrosM = Reader.GetOrdinal("Nro_MiembrosM");
                        int id_conexioAgua = Reader.GetOrdinal("id_conexioAgua");
                        int Conexion_Agua = Reader.GetOrdinal("Conexion_Agua");
                        int id_ubs = Reader.GetOrdinal("id_ubs");
                        int Ubs = Reader.GetOrdinal("Ubs");
                        int id_tipoUbs = Reader.GetOrdinal("id_tipoUbs");
                        int Tipo_Ubs = Reader.GetOrdinal("Tipo_Ubs");
                        int id_estadoPredio = Reader.GetOrdinal("id_estadoPredio");
                        int Estado_Predio = Reader.GetOrdinal("Estado_Predio");
                        int Observaciones = Reader.GetOrdinal("Observaciones");
                        int Fecha_Registro = Reader.GetOrdinal("Fecha_Registro");
                        int Fecha_Modificación = Reader.GetOrdinal("Fecha_Modificación");

                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                var obj = new BE_MON_PadronBeneficiario();
                                if (!Reader.IsDBNull(id_padronBeneficiario))
                                {
                                    obj.id_padronBeneficiario = Reader.GetInt32(id_padronBeneficiario);
                                }

                                if (!Reader.IsDBNull(Apellido_Paterno))
                                {
                                    obj.apellidoPaterno = Reader.GetString(Apellido_Paterno);
                                }


                                if (!Reader.IsDBNull(Apellido_Materno))
                                {
                                    obj.apellidoMaterno = Reader.GetString(Apellido_Materno);
                                }
                                   

                                if (!Reader.IsDBNull(Nombres))
                                {
                                    obj.nombres = Reader.GetString(Nombres);
                                }

                                if (!Reader.IsDBNull(id_sexo))
                                {                               
                                    obj.id_sexo= Reader.GetInt32(id_sexo);
                                }
                                if (!Reader.IsDBNull(Sexo))
                                {
                                    obj.Sexo = Reader.GetString(Sexo);
                                }

                                if (!Reader.IsDBNull(Fecha_Nacimiento))
                                {
                                      obj.fechaNacimiento = Reader.GetDateTime(Fecha_Nacimiento);
                                    obj.vfechaNacimiento = obj.fechaNacimiento.ToString("dd/MM/yyyy");
                                }

                                if (!Reader.IsDBNull(id_tipoDocumentoMiembroNE))
                                {
                                    obj.id_tipoDocumentoMiembroNE = Reader.GetInt32(id_tipoDocumentoMiembroNE);
                                   
                                }
                                if (!Reader.IsDBNull(Tipo_DocMiembroNE))
                                {
                                    obj.Tipo_DocMiembroNE = Reader.GetString(Tipo_DocMiembroNE);
                                }
 
                                if (!Reader.IsDBNull(DNI))
                                {
                                    obj.dni = Reader.GetString(DNI);
                                }


                                if (!Reader.IsDBNull(id_tipoDomicilio))
                                {
                                    obj.id_tipoDomicilio = Reader.GetInt32(id_tipoDomicilio);
                                }
                                if (!Reader.IsDBNull(Tipo_Domicilio))
                                {
                                    obj.Tipo_Domicilio = Reader.GetString(Tipo_Domicilio);
                                }
                                

                                if (!Reader.IsDBNull(Direccion_Domicilio))
                                {
                                    obj.direccionDomicilio = Reader.GetString(Direccion_Domicilio);
                                }
   
                                if (!Reader.IsDBNull(Nro_Domicilio))
                                {
                                    obj.nroDomicilio = Reader.GetString(Nro_Domicilio);
                                }

                                if (!Reader.IsDBNull(id_tipoBeneficio))
                                {
                                    obj.id_tipoBeneficio = Reader.GetInt32(id_tipoBeneficio);
                                }
                                if (!Reader.IsDBNull(Tipo_Beneficio))
                                {
                                    obj.Tipo_Beneficio = Reader.GetString(Tipo_Beneficio);
                                }

                                if (!Reader.IsDBNull(id_estadoBeneficiario))
                                {
                                    obj.id_estadoBeneficiario = Reader.GetInt32(id_estadoBeneficiario);
                                }
                                if (!Reader.IsDBNull(Estado_Beneficiario))
                                {
                                    obj.Estado_Beneficiario = Reader.GetString(Estado_Beneficiario);
                                }
                                
                                if (!Reader.IsDBNull(Nro_MiembrosH))
                                {
                                    obj.nroMiembrosHombres = Reader.GetInt32(Nro_MiembrosH);
                                }
                               
                                if (!Reader.IsDBNull(Nro_MiembrosM))
                                {
                                    obj.nroMiembrosMujeres = Reader.GetInt32(Nro_MiembrosM);
                                }

                                if (!Reader.IsDBNull(id_conexioAgua))
                                {
                                    obj.id_conexioAgua = Reader.GetInt32(id_conexioAgua);
                                }
                                if (!Reader.IsDBNull(Conexion_Agua))
                                {
                                    obj.Conexion_Agua = Reader.GetString(Conexion_Agua);
                                }

                                if (!Reader.IsDBNull(id_ubs))
                                {
                                    obj.id_ubs = Reader.GetInt32(id_ubs);
                                }
                                if (!Reader.IsDBNull(Ubs))
                                {
                                    obj.Ubs = Reader.GetString(Ubs);
                                }

                                if (!Reader.IsDBNull(id_tipoUbs))
                                {
                                    obj.id_tipoUbs = Reader.GetInt32(id_tipoUbs);
                                }
                                if (!Reader.IsDBNull(Tipo_Ubs))
                                {
                                    obj.Tipo_Ubs = Reader.GetString(Tipo_Ubs);
                                }

                                if (!Reader.IsDBNull(id_estadoPredio))
                                {
                                    obj.id_estadoPredio = Reader.GetInt32(id_estadoPredio);
                                }
                                if (!Reader.IsDBNull(Estado_Predio))
                                {
                                    obj.Estado_Predio = Reader.GetString(Estado_Predio);
                                }                         

                                if (!Reader.IsDBNull(Observaciones))
                                {
                                    obj.observaciones = Reader.GetString(Observaciones);
                                }
                                
                                lista.Add(obj);
                            }
                        }

                    }

                    return lista;
                }
                catch (Exception ex)
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public int Insertar_PadronBeneficiario(BE_MON_PadronBeneficiario padron)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spi_MON_PadronBeneficiario");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, padron.id_proyecto);
                objDE.AddInParameter(objComando, "@apellidoPaterno", DbType.String, padron.apellidoPaterno);
                objDE.AddInParameter(objComando, "@apellidoMaterno", DbType.String, padron.apellidoMaterno);
                objDE.AddInParameter(objComando, "@nombres", DbType.String, padron.nombres);
                objDE.AddInParameter(objComando, "@id_sexo", DbType.Int32, padron.id_sexo);
                objDE.AddInParameter(objComando, "@fechaNacimiento", DbType.Date, padron.fechaNacimiento);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoMiembroNE", DbType.Int32, padron.id_tipoDocumentoMiembroNE);
                objDE.AddInParameter(objComando, "@dni", DbType.String, padron.dni);
                objDE.AddInParameter(objComando, "@id_tipoDomicilio", DbType.Int32, padron.id_tipoDomicilio);
                objDE.AddInParameter(objComando, "@direccionDomicilio", DbType.String, padron.direccionDomicilio);
                objDE.AddInParameter(objComando, "@nroDomicilio", DbType.String, padron.nroDomicilio);
                objDE.AddInParameter(objComando, "@id_tipoBeneficio", DbType.Int32, padron.id_tipoBeneficio);
                objDE.AddInParameter(objComando, "@id_estadoBeneficiario", DbType.Int32, padron.id_estadoBeneficiario);
                objDE.AddInParameter(objComando, "@nroMiembrosHombres", DbType.Int32, padron.nroMiembrosHombres);
                objDE.AddInParameter(objComando, "@nroMiembrosMujeres", DbType.Int32, padron.nroMiembrosMujeres);
                objDE.AddInParameter(objComando, "@id_conexioAgua", DbType.Int32, padron.id_conexioAgua);
                objDE.AddInParameter(objComando, "@id_ubs", DbType.Int32, padron.id_ubs);
                objDE.AddInParameter(objComando, "@id_tipoUbs", DbType.Int32, padron.id_tipoUbs);
                objDE.AddInParameter(objComando, "@id_estadoPredio", DbType.Int32, padron.id_estadoPredio);
                objDE.AddInParameter(objComando, "@observaciones", DbType.String, padron.observaciones);
                objDE.AddInParameter(objComando, "@usr_registro", DbType.Int32, padron.usr_registro);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int Actualizar_PadronBeneficiario(BE_MON_PadronBeneficiario padron)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spu_MON_PadronBeneficiario");

                objDE.AddInParameter(objComando, "@id_padronBeneficiario", DbType.Int32, padron.id_padronBeneficiario);

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, padron.id_proyecto);
                objDE.AddInParameter(objComando, "@apellidoPaterno", DbType.String, padron.apellidoPaterno);
                objDE.AddInParameter(objComando, "@apellidoMaterno", DbType.String, padron.apellidoMaterno);
                objDE.AddInParameter(objComando, "@nombres", DbType.String, padron.nombres);
                objDE.AddInParameter(objComando, "@id_sexo", DbType.Int32, padron.id_sexo);
                objDE.AddInParameter(objComando, "@fechaNacimiento", DbType.Date, padron.vfechaNacimiento);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoMiembroNE", DbType.Int32, padron.id_tipoDocumentoMiembroNE);
                objDE.AddInParameter(objComando, "@dni", DbType.String, padron.dni);
                objDE.AddInParameter(objComando, "@id_tipoDomicilio", DbType.Int32, padron.id_tipoDomicilio);
                objDE.AddInParameter(objComando, "@direccionDomicilio", DbType.String, padron.direccionDomicilio);
                objDE.AddInParameter(objComando, "@nroDomicilio", DbType.String, padron.nroDomicilio);
                objDE.AddInParameter(objComando, "@id_tipoBeneficio", DbType.Int32, padron.id_tipoBeneficio);
                objDE.AddInParameter(objComando, "@id_estadoBeneficiario", DbType.Int32, padron.id_estadoBeneficiario);
                objDE.AddInParameter(objComando, "@nroMiembrosHombres", DbType.Int32, padron.nroMiembrosHombres);
                objDE.AddInParameter(objComando, "@nroMiembrosMujeres", DbType.Int32, padron.nroMiembrosMujeres);
                objDE.AddInParameter(objComando, "@id_conexioAgua", DbType.Int32, padron.id_conexioAgua);
                objDE.AddInParameter(objComando, "@id_ubs", DbType.Int32, padron.id_ubs);
                objDE.AddInParameter(objComando, "@id_tipoUbs", DbType.Int32, padron.id_tipoUbs);
                objDE.AddInParameter(objComando, "@id_estadoPredio", DbType.Int32, padron.id_estadoPredio);
                objDE.AddInParameter(objComando, "@observaciones", DbType.String, padron.observaciones);
                objDE.AddInParameter(objComando, "@usr_update", DbType.Int32, padron.usr_update);

                //return Convert.ToBoolean(objDE.ExecuteScalar(objComando));
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        //public int Eliminar_PadronBeneficiario(int idPadronBeneficiario)
        //{   
        //    try
        //    {
        //        SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
        //        DbCommand objComando = objDE.GetStoredProcCommand("spd_MON_PadronBeneficiario");

        //        objDE.AddInParameter(objComando, "@id_padronBeneficiario", DbType.Int32, idPadronBeneficiario);

        //        return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        //    }
        //    catch (DbException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //}

        public int Eliminar_PadronBeneficiario(BE_MON_PadronBeneficiario padron)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spd_MON_PadronBeneficiario");

                objDE.AddInParameter(objComando, "@id_padronBeneficiario", DbType.Int32, padron.id_padronBeneficiario);

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, padron.id_proyecto);
                objDE.AddInParameter(objComando, "@apellidoPaterno", DbType.String, padron.apellidoPaterno);
                objDE.AddInParameter(objComando, "@apellidoMaterno", DbType.String, padron.apellidoMaterno);
                objDE.AddInParameter(objComando, "@nombres", DbType.String, padron.nombres);
                objDE.AddInParameter(objComando, "@id_sexo", DbType.Int32, padron.id_sexo);
                objDE.AddInParameter(objComando, "@fechaNacimiento", DbType.Date, padron.vfechaNacimiento);
                objDE.AddInParameter(objComando, "@id_tipoDocumentoMiembroNE", DbType.Int32, padron.id_tipoDocumentoMiembroNE);
                objDE.AddInParameter(objComando, "@dni", DbType.String, padron.dni);
                objDE.AddInParameter(objComando, "@id_tipoDomicilio", DbType.Int32, padron.id_tipoDomicilio);
                objDE.AddInParameter(objComando, "@direccionDomicilio", DbType.String, padron.direccionDomicilio);
                objDE.AddInParameter(objComando, "@nroDomicilio", DbType.String, padron.nroDomicilio);
                objDE.AddInParameter(objComando, "@id_tipoBeneficio", DbType.Int32, padron.id_tipoBeneficio);
                objDE.AddInParameter(objComando, "@id_estadoBeneficiario", DbType.Int32, padron.id_estadoBeneficiario);
                objDE.AddInParameter(objComando, "@nroMiembrosHombres", DbType.Int32, padron.nroMiembrosHombres);
                objDE.AddInParameter(objComando, "@nroMiembrosMujeres", DbType.Int32, padron.nroMiembrosMujeres);
                objDE.AddInParameter(objComando, "@id_conexioAgua", DbType.Int32, padron.id_conexioAgua);
                objDE.AddInParameter(objComando, "@id_ubs", DbType.Int32, padron.id_ubs);
                objDE.AddInParameter(objComando, "@id_tipoUbs", DbType.Int32, padron.id_tipoUbs);
                objDE.AddInParameter(objComando, "@id_estadoPredio", DbType.Int32, padron.id_estadoPredio);
                objDE.AddInParameter(objComando, "@observaciones", DbType.String, padron.observaciones);
                objDE.AddInParameter(objComando, "@usr_update", DbType.Int32, padron.usr_update);

                //return Convert.ToBoolean(objDE.ExecuteScalar(objComando));
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public void InsercionMasiva(DataTable dtExcelData)
        {  
          
            using (SqlConnection con = new SqlConnection(strCadenaConexion))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    try
                    {
                        //Set the database table name
                        sqlBulkCopy.DestinationTableName = "dbo.HojaExcel";

                        //[OPTIONAL]: Map the Excel columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("id_proyecto", "id_proyecto");

                        sqlBulkCopy.ColumnMappings.Add("apellidoPaterno", "apellidoPaterno");
                        sqlBulkCopy.ColumnMappings.Add("apellidoMaterno", "apellidoMaterno");
                        sqlBulkCopy.ColumnMappings.Add("nombres", "nombres");
                        sqlBulkCopy.ColumnMappings.Add("id_sexo", "id_sexo");
                        sqlBulkCopy.ColumnMappings.Add("fechaNacimiento", "fechaNacimiento");
                        sqlBulkCopy.ColumnMappings.Add("id_tipoDocumentoMiembroNE", "id_tipoDocumentoMiembroNE");
                        sqlBulkCopy.ColumnMappings.Add("dni", "dni");
                        sqlBulkCopy.ColumnMappings.Add("id_tipoDomicilio", "id_tipoDomicilio");
                        sqlBulkCopy.ColumnMappings.Add("direccionDomicilio", "direccionDomicilio");
                        sqlBulkCopy.ColumnMappings.Add("nroDomicilio", "nroDomicilio");
                        sqlBulkCopy.ColumnMappings.Add("id_tipoBeneficio", "id_tipoBeneficio");
                        sqlBulkCopy.ColumnMappings.Add("id_estadoBeneficiario", "id_estadoBeneficiario");
                        sqlBulkCopy.ColumnMappings.Add("nroMiembrosHombres", "nroMiembrosHombres");
                        sqlBulkCopy.ColumnMappings.Add("nroMiembrosMujeres", "nroMiembrosMujeres");
                        sqlBulkCopy.ColumnMappings.Add("id_conexioAgua", "id_conexioAgua");
                        sqlBulkCopy.ColumnMappings.Add("id_ubs", "id_ubs");
                        sqlBulkCopy.ColumnMappings.Add("id_tipoUbs", "id_tipoUbs");
                        sqlBulkCopy.ColumnMappings.Add("id_estadoPredio", "id_estadoPredio");
                        sqlBulkCopy.ColumnMappings.Add("observaciones", "observaciones");

                        sqlBulkCopy.ColumnMappings.Add("usr_registro", "usr_registro");

                        con.Open();
                        sqlBulkCopy.WriteToServer(dtExcelData);
                        con.Close();
                        

                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    
                }
            }
        }

        public IEnumerable<BE_MON_PadronBeneficiario> ValidarPadronBeneficiario(List<BE_MON_PadronBeneficiario> padron, int pid_proyecto, int pusr_registro)
        {
            PadronBeneficiarioType listapadron = new PadronBeneficiarioType();
            listapadron.AddRange(padron);

            using (SqlConnection Cnx = new SqlConnection(strCadenaConexion))
            {
                List<BE_MON_PadronBeneficiario> lista = new List<BE_MON_PadronBeneficiario>();
                SqlCommand cmd = default(SqlCommand);

                string detalleError;

                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[dbo].[sps_MON_PadronBeneficiario_ValidarCarga]";
                    cmd.CommandTimeout = 36000;

                    // Add the input parameter and set its properties.
                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = "@Padron";
                    parameter.SqlDbType = SqlDbType.Structured;
                    parameter.Direction = ParameterDirection.Input;
                    parameter.TypeName = "[dbo].[T_PadronBeneficiario_Type]";
                    parameter.Value = listapadron;
                    // Add the parameter to the Parameters collection. 
                    cmd.Parameters.Add(parameter);

                    parameter = new SqlParameter();
                    parameter.ParameterName = "@id_proyecto";
                    parameter.SqlDbType = SqlDbType.Int;
                    parameter.Value = pid_proyecto;
                    cmd.Parameters.Add(parameter);

                    parameter = new SqlParameter();
                    parameter.ParameterName = "@usr_registro";
                    parameter.SqlDbType = SqlDbType.Int;
                    parameter.Value = pusr_registro;
                    cmd.Parameters.Add(parameter);

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        //int id_proyecto = Reader.GetOrdinal("id_proyecto");
                        int apellidoPaterno = Reader.GetOrdinal("apellidoPaterno");
                        int apellidoMaterno = Reader.GetOrdinal("apellidoMaterno");
                        int nombres = Reader.GetOrdinal("nombres");
                        int id_sexo = Reader.GetOrdinal("id_sexo");
                        int sexo = Reader.GetOrdinal("sexo");
                        int fechaNacimiento = Reader.GetOrdinal("fechaNacimiento");
                        int id_tipoDocumentoMiembroNE = Reader.GetOrdinal("id_tipoDocumentoMiembroNE");
                        int tipoDocumentoMiembroNE = Reader.GetOrdinal("tipoDocumentoMiembroNE");
                        int dni = Reader.GetOrdinal("dni");
                        int id_tipoDomicilio = Reader.GetOrdinal("id_tipoDomicilio");
                        int tipoDomicilio = Reader.GetOrdinal("tipoDomicilio");
                        int direccionDomicilio = Reader.GetOrdinal("direccionDomicilio");
                        int nroDomicilio = Reader.GetOrdinal("nroDomicilio");
                        int id_tipoBeneficio = Reader.GetOrdinal("id_tipoBeneficio");
                        int tipoBeneficio = Reader.GetOrdinal("tipoBeneficio");
                        int id_estadoBeneficiario = Reader.GetOrdinal("id_estadoBeneficiario");
                        int estadoBeneficiario = Reader.GetOrdinal("estadoBeneficiario");
                        int nroMiembrosHombres = Reader.GetOrdinal("nroMiembrosHombres");
                        int nroMiembrosMujeres = Reader.GetOrdinal("nroMiembrosMujeres");
                        int id_conexioAgua = Reader.GetOrdinal("id_conexioAgua");
                        int conexionAgua = Reader.GetOrdinal("conexionAgua");
                        int id_ubs = Reader.GetOrdinal("id_ubs");
                        int ubs = Reader.GetOrdinal("ubs");
                        int id_tipoUbs = Reader.GetOrdinal("id_tipoUbs");
                        int tipoUbs = Reader.GetOrdinal("tipoUbs");
                        int id_estadoPredio = Reader.GetOrdinal("id_estadoPredio");
                        int estadoPredio = Reader.GetOrdinal("estadoPredio");
                        int observaciones = Reader.GetOrdinal("observaciones");

                        int cantidadDNI = Reader.GetOrdinal("cantidadDNI");
                        int Numero = Reader.GetOrdinal("Numero");
                        

                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                detalleError = "";

                                var obj = new BE_MON_PadronBeneficiario();
                                //if (!Reader.IsDBNull(id_proyecto))
                                //    obj.id_padronBeneficiario = Reader.GetInt32(id_proyecto);
                                
                                if (!Reader.IsDBNull(apellidoPaterno))
                                    obj.apellidoPaterno = Reader.GetString(apellidoPaterno);

                                if (!Reader.IsDBNull(apellidoMaterno))
                                    obj.apellidoMaterno = Reader.GetString(apellidoMaterno);

                                if (!Reader.IsDBNull(nombres))
                                    obj.nombres = Reader.GetString(nombres);

                                if (!Reader.IsDBNull(sexo))
                                    obj.Sexo = Reader.GetString(sexo);

                                if (!Reader.IsDBNull(fechaNacimiento))
                                    obj.fechaNacimiento = Reader.GetDateTime(fechaNacimiento);
                                    obj.vfechaNacimiento = obj.fechaNacimiento.ToString("dd/MM/yyyy");

                                if (!Reader.IsDBNull(tipoDocumentoMiembroNE))
                                    obj.Tipo_DocMiembroNE = Reader.GetString(tipoDocumentoMiembroNE);

                                if (!Reader.IsDBNull(dni))
                                    obj.dni = Reader.GetString(dni);

                                if (!Reader.IsDBNull(tipoDomicilio))
                                    obj.Tipo_Domicilio = Reader.GetString(tipoDomicilio);

                                if (!Reader.IsDBNull(direccionDomicilio))
                                    obj.direccionDomicilio = Reader.GetString(direccionDomicilio);

                                if (!Reader.IsDBNull(nroDomicilio))
                                    obj.nroDomicilio = Reader.GetString(nroDomicilio);

                                if (!Reader.IsDBNull(tipoBeneficio))
                                    obj.Tipo_Beneficio = Reader.GetString(tipoBeneficio);

                                if (!Reader.IsDBNull(estadoBeneficiario))
                                    obj.Estado_Beneficiario = Reader.GetString(estadoBeneficiario);

                                if (!Reader.IsDBNull(nroMiembrosHombres))
                                    obj.nroMiembrosHombres = Reader.GetInt32(nroMiembrosHombres);

                                if (!Reader.IsDBNull(nroMiembrosMujeres))
                                    obj.nroMiembrosMujeres = Reader.GetInt32(nroMiembrosMujeres);

                                if (!Reader.IsDBNull(conexionAgua))
                                    obj.Conexion_Agua = Reader.GetString(conexionAgua);

                                if (!Reader.IsDBNull(ubs))
                                    obj.Ubs = Reader.GetString(ubs);

                                if (!Reader.IsDBNull(tipoUbs))
                                    obj.Tipo_Ubs = Reader.GetString(tipoUbs);

                                if (!Reader.IsDBNull(estadoPredio))
                                    obj.Estado_Predio = Reader.GetString(estadoPredio);

                                if (!Reader.IsDBNull(observaciones))
                                    obj.observaciones = Reader.GetString(observaciones);

                                if (Reader.IsDBNull(id_sexo))
                                    detalleError = detalleError + " Tipo genero no existe | ";

                                if (Reader.IsDBNull(id_tipoDocumentoMiembroNE))
                                    detalleError = detalleError + " Tipo Documento MiembroNE no existe | ";
                            
                                if (Reader.IsDBNull(id_tipoDomicilio))
                                    detalleError = detalleError + " Tipo Domicilio no existe | ";

                                if (Reader.IsDBNull(id_tipoBeneficio))
                                    detalleError = detalleError + " Tipo Beneficio no existe | ";

                                if (Reader.IsDBNull(id_estadoBeneficiario))
                                    detalleError = detalleError + " Estado Beneficiario no existe | ";

                                if (Reader.IsDBNull(id_conexioAgua))
                                    detalleError = detalleError + " Conexion Agua no existe | ";

                                if (Reader.IsDBNull(id_ubs))
                                    detalleError = detalleError + " UBS no existe | ";

                                if (Reader.IsDBNull(id_tipoUbs))
                                    detalleError = detalleError + " Tipo USB no existe | ";

                                if (Reader.IsDBNull(id_estadoPredio))
                                    detalleError = detalleError + " Estado Predio no existe | ";

                                if (Reader.GetInt32(cantidadDNI) > 0)
                                    detalleError = detalleError + " DNI ya se encuentra registrado | ";

                                obj.Detalle_Error = detalleError;

                                //
                                obj.Numero = Reader.GetInt64(Numero);
                                lista.Add(obj);
                               
                            }
                        }
                    }

                    return lista;
                }
                catch (Exception ex)
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }              
        }
    }
}
