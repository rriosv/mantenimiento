﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Entity;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace DataAccess
{
    public class DE_Administrador
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();
        public SqlConnection Cnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        public List<BE_Administrador> F_spAdm_ListarUsuarios()
         {
             BE_Administrador objENT = new BE_Administrador();
             List<BE_Administrador> objCollection = new List<BE_Administrador>();
             SqlCommand cmd = default(SqlCommand);
             try
             {
                 cmd = new SqlCommand();
                 cmd.Connection = Cnx;
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.CommandText = "spAdm_ListarUsuarios";
                 cmd.CommandTimeout = 36000;
                 //cmd.Parameters.AddWithValue("@id_proyecto", _BE.id_proyecto);

                 cmd.Connection.Open();
                 using (SqlDataReader Reader = cmd.ExecuteReader())
                 {
                     while (Reader.Read())
                     {
                         objENT = new BE_Administrador();
                         objENT.id_usuario = Convert.ToInt32(Reader["id_usuario"].ToString());
                         objENT.id_perfil_usuario = Convert.ToInt32(Reader["id_perfil_usuario"].ToString());
                         objENT.login = Reader["login"].ToString();
                         objENT.passw = Reader["passw"].ToString();
                         objENT.nombre = Reader["nombre"].ToString(); 
                         objENT.correo = Reader["correo"].ToString();
                         objENT.vDNI = Reader["vDNI"].ToString();
                         objENT.iCodOFicina = Reader["iCodOficina"].ToString();
                         objENT.oficina = Reader["oficina"].ToString();
                         objENT.nivelEstudio = Reader["NivelEstudio"].ToString();
                         objENT.nivelMonitoreo = Reader["NivelMonitoreo"].ToString();
                         objENT.nivelEmblematico = Reader["NivelEmblematico"].ToString();
                         objENT.activo = Convert.ToBoolean(Reader["activo"].ToString());
                        
                         objCollection.Add(objENT);
                         objENT = null;
                     }
                 }
                 return objCollection;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 cmd = null;
                 if (Cnx.State == ConnectionState.Open)
                 {
                     Cnx.Close();
                 }
             }

         }

        public List<BE_Administrador> F_spAdm_ListaOficina()
        {
            BE_Administrador objENT = new BE_Administrador();
            List<BE_Administrador> objCollection = new List<BE_Administrador>();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spAdm_ListaOficina";
                cmd.CommandTimeout = 36000;
                //cmd.Parameters.AddWithValue("@id_proyecto", _BE.id_proyecto);

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        objENT = new BE_Administrador();
                        objENT.valor = Convert.ToInt32(Reader["valor"].ToString());
                        objENT.nombre = Reader["nombre"].ToString();

                        objCollection.Add(objENT);
                        objENT = null;
                    }
                }
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public BE_Administrador F_spAdm_ValidarUsuario(BE_Administrador _BE)
        {
            BE_Administrador objENT = new BE_Administrador();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spAdm_ValidarUsuario";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.AddWithValue("@correo", _BE.correo);
                cmd.Parameters.AddWithValue("@usuario", _BE.login);

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    if (Reader.Read())
                    {
                        objENT = new BE_Administrador();
                        objENT.id_usuario = Convert.ToInt32(Reader["id_usuario"].ToString());
                        objENT.nombre = Reader["nombre"].ToString();
                        objENT.correo = Reader["correo"].ToString();
                        //objCollection.Add(objENT);
                        //objENT = null;
                    }
                }
                return objENT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public BE_Administrador F_sp_GetByIdUsuario(int idUsuario)
        {
            BE_Administrador objENT = new BE_Administrador();
            BE_Administrador objCollection = new BE_Administrador();
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_GetByIdUsuario";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.AddWithValue("@id_usuario", idUsuario);

                cmd.Connection.Open();
                using (SqlDataReader Reader = cmd.ExecuteReader())
                {
                    if (Reader.Read())
                    {
                        objENT = new BE_Administrador();
                        objENT.id_usuario = Convert.ToInt32(Reader["id_usuario"].ToString());
                        objENT.id_perfil_usuario = Convert.ToInt32(Reader["id_perfil_usuario"].ToString());
                        objENT.login = Reader["login"].ToString();
                        objENT.passw = Reader["passw"].ToString();
                        objENT.nombre = Reader["nombre"].ToString();
                        objENT.correo = Reader["correo"].ToString();
                        objENT.activo = Convert.ToBoolean(Reader["activo"].ToString());
                                             
                    }
                                      
                }
                return objENT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int spiuAdm_Usuario(BE_Administrador _BE)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiuAdm_Usuario]");

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE.id_usuario);
                objDE.AddInParameter(objComando, "@id_perfil_usuario", DbType.Int32, _BE.id_perfil_usuario);
                objDE.AddInParameter(objComando, "@login", DbType.String, _BE.login);
                objDE.AddInParameter(objComando, "@passw", DbType.String, _BE.passw);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE.nombre);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE.correo);
                objDE.AddInParameter(objComando, "@activo", DbType.Boolean, _BE.activo);
                objDE.AddInParameter(objComando, "@DNI", DbType.String, _BE.vDNI);
                objDE.AddInParameter(objComando, "@iCodOficina", DbType.String, _BE.iCodOFicina);
                objDE.AddInParameter(objComando, "@nivelEstudio", DbType.String, _BE.nivelEstudio);
                objDE.AddInParameter(objComando, "@nivelMonitoreo", DbType.String, _BE.nivelMonitoreo);
                objDE.AddInParameter(objComando, "@nivelEmblematico", DbType.String, _BE.nivelEmblematico);
                
                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        
        public int spiAdm_AccesoPublico(BE_Administrador _BE)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spiAdm_AccesoPublico]");
                              
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE.nombre);
                objDE.AddInParameter(objComando, "@correo", DbType.String, _BE.correo);
                objDE.AddInParameter(objComando, "@DNI", DbType.String, _BE.vDNI);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
