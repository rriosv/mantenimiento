﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess
{
    public class DE_MON_Conclusion
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

    public DataSet spMON_Conclusion(BE_MON_Conclusion _BE_Conclusion)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Conclusion]");
            objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Conclusion.id_proyecto);
            objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Conclusion.tipoFinanciamiento);
           
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }

    public int spi_MON_Conclusion(BE_MON_Conclusion _BE_Conclusion)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_Conclusion]");

            objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Conclusion.id_proyecto);
            objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Conclusion.tipoFinanciamiento);

            objDE.AddInParameter(objComando, "@conclusion", DbType.String, _BE_Conclusion.conclusion);
            objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Conclusion.observacion);
            objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Conclusion.urlDoc);
 
            objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Conclusion.id_usuario);

            return Convert.ToInt32(objDE.ExecuteScalar(objComando));
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
    }





    }
}
