﻿using System;
using System.Collections.Generic;
using Entity;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccess
{
    public class DE_GrupoDetalle
    {
        //private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public List<BE_GrupoDetalle> ListarGrupo(int idGrupo)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                List<BE_GrupoDetalle> lista = new List<BE_GrupoDetalle>();
                SqlCommand cmd = default(SqlCommand);

                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_ListarGrupos";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@idGrupo", SqlDbType.Int).Value = idGrupo;
                    
                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int IdGrupoDetalle = Reader.GetOrdinal("IdGrupoDetalle");
                        int IdGrupo = Reader.GetOrdinal("IdGrupo");
                        int Nombre = Reader.GetOrdinal("Nombre");

                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                var obj = new BE_GrupoDetalle();
                                obj.IdGrupoDetalle = Reader.GetInt32(IdGrupoDetalle);
                                obj.IdGrupo = Reader.GetInt32(IdGrupo);
                                obj.Nombre = Reader.GetString(Nombre);
                                lista.Add(obj);                             
                            }
                        }
                        
                    }

                    return lista;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
      }
}
