using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;

public class DELogin
{
    private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

    #region Consulta

    public DataSet sp_validarUsuario(string strLogin, string strPassword)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_validarUsuario]");
            objDE.AddInParameter(objComando, "login", DbType.String, strLogin);
            objDE.AddInParameter(objComando, "passw", DbType.String, strPassword);
            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet sp_validarUsuarioLogin(string strLogin)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_validarUsuarioLogin]");
            objDE.AddInParameter(objComando, "login", DbType.String, strLogin);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet sp_validarUsuarioDNI(string strDNI)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("[sp_validarUsuarioDNI]");
            objDE.AddInParameter(objComando, "DNI", DbType.String, strDNI);
            return objDE.ExecuteDataSet(objComando);

        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }

    }

    public DataSet paSSP_rObtenerSession(string clave)
    {
        try
        {
            SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
            DbCommand objComando = objDE.GetStoredProcCommand("paSSP_ADM_rObtenerSession");
            objDE.AddInParameter(objComando, "@clave", DbType.String, clave);

            return objDE.ExecuteDataSet(objComando);
        }
        catch (DbException ex)
        {
            throw ex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }

    }

    public int I_iSession(string clave, int id_usuario)
    {
        using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
        {
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("paSSP_ADM_iSession", strCnx);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@clave", SqlDbType.VarChar,200).Value = clave;
                cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = id_usuario;

                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (strCnx.State == ConnectionState.Open)
                {
                    strCnx.Close();
                }
            }
        }
    }

    #endregion

    #region Transaccion

    #endregion
}
