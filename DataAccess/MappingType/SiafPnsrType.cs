﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Entity;

namespace DataAccess.MappingType
{
    public class SiafPnsrType : List<BE_MON_SIAF_PNSR>, IEnumerable<SqlDataRecord>
    {
        #region IEnumerable<SqlDataRecord> GetEnumerator
        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
                        new SqlMetaData[] {
                            new SqlMetaData("ANO_EJE", SqlDbType.VarChar,255),
                            new SqlMetaData("EXPEDIENTE", SqlDbType.VarChar,255),
                            new SqlMetaData("SEC_AREA", SqlDbType.VarChar,255),
                            new SqlMetaData("FASE", SqlDbType.VarChar,255),
                            new SqlMetaData("COD_DOC", SqlDbType.VarChar,255),
                            new SqlMetaData("COD_DOC_NOMBRE", SqlDbType.VarChar,255),
                            new SqlMetaData("NUM_DOC", SqlDbType.VarChar,255),
                            new SqlMetaData("NOTAS", SqlDbType.VarChar,255),
                            new SqlMetaData("FECHA_DOC", SqlDbType.VarChar,255),
                            new SqlMetaData("SEC_FUNC", SqlDbType.VarChar,255),
                            new SqlMetaData("SEC_FUNC_NOMBRE", SqlDbType.VarChar,255),
                            new SqlMetaData("PROD_PRY", SqlDbType.VarChar,255),
                            new SqlMetaData("META", SqlDbType.VarChar,255),
                            new SqlMetaData("FINALIDAD", SqlDbType.VarChar,255),
                            new SqlMetaData("MONTO", SqlDbType.VarChar,255),
                            new SqlMetaData("MONTO_NACIONAL", SqlDbType.VarChar,255),
                            new SqlMetaData("FECHA_AUTORIZACION", SqlDbType.VarChar,255),
                            new SqlMetaData("NUM_DOC_B", SqlDbType.VarChar,255),
                            new SqlMetaData("ANO_PROCESO", SqlDbType.VarChar,255),
                            new SqlMetaData("MES_PROCESO", SqlDbType.VarChar,255),
                            new SqlMetaData("DIA_PROCESO", SqlDbType.VarChar,255),
                            new SqlMetaData("CERTIFICADO_GLOSA", SqlDbType.VarChar,255),
                            new SqlMetaData("USUARIO_CREACION_CLT", SqlDbType.VarChar,255),
                            new SqlMetaData("FECHA_FIN_CONTRATO", SqlDbType.VarChar,255),

                            }
                        );

            foreach (BE_MON_SIAF_PNSR item in this)
            {
                //sdr.SetInt32(0, item.id_tecnico);
                //sdr.SetString(0, item.nivel);
                sdr.SetString(0, item.ANO_EJE);
                sdr.SetString(1, item.EXPEDIENTE);
                sdr.SetString(2, item.SEC_AREA);
                sdr.SetString(3, item.FASE);
                sdr.SetString(4, item.COD_DOC);
                sdr.SetString(5, item.COD_DOC_NOMBRE);
                sdr.SetString(6, item.NUM_DOC);
                sdr.SetString(7, item.NOTAS);
                sdr.SetString(8, item.FECHA_DOC);
                sdr.SetString(9, item.SEC_FUNC);
                sdr.SetString(10, item.SEC_FUNC_NOMBRE);
                sdr.SetString(11, item.PROD_PRY);
                sdr.SetString(12, item.META);
                sdr.SetString(13, item.FINALIDAD);
                sdr.SetString(14, item.MONTO);
                sdr.SetString(15, item.MONTO_NACIONAL);
                sdr.SetString(16, item.FECHA_AUTORIZACION);
                sdr.SetString(17, item.NUM_DOC_B);
                sdr.SetString(18, item.ANO_PROCESO);
                sdr.SetString(19, item.MES_PROCESO);
                sdr.SetString(20, item.DIA_PROCESO);
                sdr.SetString(21, item.CERTIFICADO_GLOSA);
                sdr.SetString(22, item.USUARIO_CREACION_CLT);
                sdr.SetString(23, item.FECHA_FIN_CONTRATO);

                //if (item.correo == null) sdr.SetDBNull(2); else sdr.SetString(2, item.correo);
                ////sdr.SetString(2, item.correo);
                //sdr.SetString(3, item.tipoAgente);
                //sdr.SetString(4, item.DNI);
                //if (item.tipoProfesion == null) sdr.SetDBNull(5); else sdr.SetString(5, item.tipoProfesion);
                //if (item.nroColegiatura == null) sdr.SetDBNull(6); else sdr.SetString(6, item.nroColegiatura);


                yield return sdr;
            }
        }

        #endregion
    }
}
