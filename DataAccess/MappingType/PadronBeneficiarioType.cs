﻿using Microsoft.SqlServer.Server;

using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DataAccess.MappingType
{
    public class PadronBeneficiarioType : List<BE_MON_PadronBeneficiario>, IEnumerable<SqlDataRecord>
    {
        #region IEnumerable<SqlDataRecord> GetEnumerator
        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
                        new SqlMetaData[] {
                            new SqlMetaData("id_padronBeneficiario", SqlDbType.Int),
                            //new SqlMetaData("id_proyecto", SqlDbType.Int),
                            new SqlMetaData("apellidoPaterno", SqlDbType.VarChar,50),
                            new SqlMetaData("apellidoMaterno", SqlDbType.VarChar,50),
                            new SqlMetaData("nombres", SqlDbType.VarChar,50),
                            new SqlMetaData("sexo", SqlDbType.VarChar,2),
                            new SqlMetaData("fechaNacimiento", SqlDbType.Date),
                            new SqlMetaData("tipoDocumentoMiembroNE", SqlDbType.VarChar,50),
                            new SqlMetaData("dni", SqlDbType.VarChar,8),
                            new SqlMetaData("tipoDomicilio", SqlDbType.VarChar,50),
                            new SqlMetaData("direccionDomicilio", SqlDbType.VarChar,100),
                            new SqlMetaData("nroDomicilio", SqlDbType.Char,4),
                            new SqlMetaData("tipoBeneficio", SqlDbType.VarChar,50),
                            new SqlMetaData("estadoBeneficiario", SqlDbType.VarChar,50),
                            new SqlMetaData("nroMiembrosHombres", SqlDbType.VarChar,50),
                            new SqlMetaData("nroMiembrosMujeres", SqlDbType.VarChar,50),
                            new SqlMetaData("conexioAgua", SqlDbType.VarChar,50),
                            new SqlMetaData("ubs", SqlDbType.VarChar,50),
                            new SqlMetaData("tipoUbs", SqlDbType.VarChar,50),
                            new SqlMetaData("estadoPredio", SqlDbType.VarChar,50),
                            new SqlMetaData("observaciones", SqlDbType.VarChar,50),
                            }
                        );

            foreach (BE_MON_PadronBeneficiario item in this)
            {
                
                sdr.SetInt32(0, item.id_padronBeneficiario);
                sdr.SetString(1, item.apellidoPaterno);
                sdr.SetString(2, item.apellidoMaterno);
                sdr.SetString(3, item.nombres);
                sdr.SetString(4, item.Sexo);
                //sdr.SetString(5, Convert.ToString(item.fechaNacimiento));
                //sdr.SetString(5, item.fechaNacimiento.ToString("dd/mm/yyyy"));
                sdr.SetDateTime(5, item.fechaNacimiento);
                //sdr.SetString(5, item.vfechaNacimiento);
                sdr.SetString(6, item.Tipo_DocMiembroNE);
                if (item.dni == null) sdr.SetDBNull(7); else sdr.SetString(7,item.dni);
                sdr.SetString(8, item.Tipo_Domicilio);
                if (item.direccionDomicilio == null) sdr.SetDBNull(9); else sdr.SetString(9, item.direccionDomicilio);
                if (item.nroDomicilio == null) sdr.SetDBNull(10); else sdr.SetString(10, item.nroDomicilio);
                sdr.SetString(11, item.Tipo_Beneficio);
                sdr.SetString(12, item.Estado_Beneficiario);
                if (Convert.ToString(item.nroMiembrosHombres) == null) sdr.SetDBNull(13); else sdr.SetString(13, Convert.ToString(item.nroMiembrosHombres));
                if (Convert.ToString(item.nroMiembrosMujeres) == null) sdr.SetDBNull(14); else sdr.SetString(14, Convert.ToString(item.nroMiembrosMujeres));
                sdr.SetString(15, item.Conexion_Agua);
                sdr.SetString(16, item.Ubs);
                sdr.SetString(17, item.Tipo_Ubs);
                sdr.SetString(18, item.Estado_Predio);
                if (item.observaciones == null) sdr.SetDBNull(19); else sdr.SetString(19, item.observaciones);
                yield return sdr;
            }
        }
        #endregion
    }    
}

