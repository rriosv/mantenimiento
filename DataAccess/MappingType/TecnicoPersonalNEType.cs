﻿using Microsoft.SqlServer.Server;
using Entity;
using System.Collections.Generic;
using System.Data;

namespace DataAccess.MappingType
{
    public class TecnicoPersonalNEType : List<BE_MON_Ejecucion>, IEnumerable<SqlDataRecord>
    {
        # region IEnumerable<SqlDataRecord> GetEnumerator
        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
                        new SqlMetaData[] {
                            //new SqlMetaData("id_tecnico", SqlDbType.Int),
                            //new SqlMetaData("nivel", SqlDbType.VarChar,1),
                            new SqlMetaData("nombre", SqlDbType.VarChar,200),
                            //new SqlMetaData("activo", SqlDbType.Bit),
                            new SqlMetaData("tipoPrograma", SqlDbType.VarChar,50),
                            new SqlMetaData("correo", SqlDbType.VarChar,50),
                            new SqlMetaData("tipoAgente", SqlDbType.VarChar,50),
                            new SqlMetaData("dni", SqlDbType.VarChar,8),
                            new SqlMetaData("tipoProfesion", SqlDbType.VarChar,50),
                            new SqlMetaData("nroColegiatura", SqlDbType.VarChar,50),
                            }
                        );

            foreach (BE_MON_Ejecucion item in this)
            {
                //sdr.SetInt32(0, item.id_tecnico);
                //sdr.SetString(0, item.nivel);
                sdr.SetString(0, item.nombre);
                //sdr.SetBoolean(1, item.activo);
                sdr.SetString(1, item.tipoPrograma);
                if (item.correo == null) sdr.SetDBNull(2); else sdr.SetString(2, item.correo);
                //sdr.SetString(2, item.correo);
                sdr.SetString(3, item.tipoAgente);
                sdr.SetString(4, item.DNI);
                if (item.tipoProfesion == null) sdr.SetDBNull(5); else sdr.SetString(5, item.tipoProfesion);
                if (item.nroColegiatura == null) sdr.SetDBNull(6); else sdr.SetString(6, item.nroColegiatura);
                

                yield return sdr;
            }
        }

        #endregion
    }
}
