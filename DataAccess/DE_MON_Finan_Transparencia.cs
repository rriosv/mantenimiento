﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess
{
    public class DE_MON_Finan_Transparencia
    {
       private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        //SqlConnection cnn = null;
        //SqlCommand cmd = null;
             

        public int spud_MON_FinanTransferencia(BE_MON_Finan_Transparencia _BE_MON_Finan_Transparencia)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_FinanTransferencia]");

                objDE.AddInParameter(objComando, "@id_finan_transferencia", DbType.Int32, _BE_MON_Finan_Transparencia.id_finan_transferencia);
                //objDE.AddInParameter(objComando, "@id_tipo_transferencia", DbType.Int32, _BE_MON_Finan_Transparencia.id_tipo_transferencia);
                //objDE.AddInParameter(objComando, "@nroconvenio", DbType.String, _BE_MON_Finan_Transparencia.nroconvenio);
                //objDE.AddInParameter(objComando, "@fecha", DbType.DateTime, _BE_MON_Finan_Transparencia.fecha);
                //objDE.AddInParameter(objComando, "@montoconvenio", DbType.String, _BE_MON_Finan_Transparencia.montoconvenio);
                //objDE.AddInParameter(objComando, "@dispositivo_aprobacion", DbType.String, _BE_MON_Finan_Transparencia.dispositivo_aprobacion);
                //objDE.AddInParameter(objComando, "@fecha_aprobacion", DbType.DateTime, _BE_MON_Finan_Transparencia.fecha_aprobacion);
                //objDE.AddInParameter(objComando, "@monto_aprobacion", DbType.String, _BE_MON_Finan_Transparencia.monto_aprobacion);
                //objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_MON_Finan_Transparencia.observacion);
                //objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_MON_Finan_Transparencia.urlDoc);
                //objDE.AddInParameter(objComando, "@tipo", DbType.Int32, _BE_MON_Finan_Transparencia.tipo);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_MON_Finan_Transparencia.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
