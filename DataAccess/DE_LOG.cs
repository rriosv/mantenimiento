﻿using System;
using System.Data;
using Entity;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DE_LOG
    {


        public int spi_Log(BE_LOG _BE_Log)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("[spi_Log]", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE_Log.Id_usuario;
                    cmd.Parameters.Add("@ip", SqlDbType.VarChar, 2000).Value = _BE_Log.Ip;
                    cmd.Parameters.Add("@hostName", SqlDbType.VarChar, 2000).Value = _BE_Log.HostName;
                    cmd.Parameters.Add("@pagina", SqlDbType.VarChar,2000).Value = _BE_Log.Pagina;
                    cmd.Parameters.Add("@tipoDispositivo", SqlDbType.VarChar, 2000).Value = _BE_Log.TipoDispositivo;
                    cmd.Parameters.Add("@agente", SqlDbType.VarChar, 2000).Value = _BE_Log.Agente;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

    }
}
