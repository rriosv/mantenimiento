﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class DE_MON_PROCESO
    {
        private string strCadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString();

        public List<BE_MON_BANDEJA> F_spMON_TipoAdjudicacion()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoAdjudicacion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_SeguimientoProcesoSeleccion(BE_MON_PROCESO _BE)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_SeguimientoProcesoSeleccion";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.VarChar, 1).Value = _BE.flagObra;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.numero = Convert.ToInt32(Reader["numero"].ToString());
                            objENT.id_seguimiento = Convert.ToInt32(Reader["id_seguimiento"].ToString());
                            objENT.id_proceso_seleccion = Convert.ToInt32(Reader["id_proceso_seleccion"].ToString());
                            objENT.convocatoria = (Reader["convocatoria"].ToString());
                            objENT.fechaPublicacion = (Reader["fechaPublicacion"].ToString());
                            objENT.id_tipoAdjudicacion = Convert.ToInt32(Reader["id_TipoAdjudicacion"].ToString());
                            objENT.tipo_adjudicacion = (Reader["tipoAdjudicacion"].ToString());
                            objENT.resultado = Reader["resultado"].ToString();
                            objENT.idResultado = Convert.ToInt32(Reader["idResultado"].ToString());
                            objENT.urlDoc = Reader["urlDocumento"].ToString();
                            objENT.observacion = Reader["observacion"].ToString();
                            objENT.usuario = Reader["usuario"].ToString();
                            objENT.strFecha_update = Reader["fecha_update"].ToString();

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_RendicionCuentaNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_RendicionCuentaNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.VarChar, 1).Value = _BE.flagObra;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.id = Convert.ToInt32(Reader["id_rendicionNE"].ToString());
                            objENT.Date_fechaPublicacion = Convert.ToDateTime(Reader["fecha"].ToString());
                            objENT.monto = (Reader["montoRendicion"].ToString());
                            objENT.hombres = Convert.ToInt32(Reader["asistentesHombres"].ToString());
                            objENT.mujeres = Convert.ToInt32(Reader["asistentesMujeres"].ToString());
                            objENT.urlDoc = (Reader["urlActa"].ToString());
                            objENT.usuario = Reader["usuario"].ToString();
                            objENT.strFecha_update = Reader["fecha_update"].ToString();

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_GrupoConsorcio(BE_MON_PROCESO _BE)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_GrupoConsorcio";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.VarChar, 1).Value = _BE.flagObra;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.id_grupo_consorcio = Convert.ToInt32(Reader["id_grupo_consorcio"].ToString());
                            objENT.nombreContratista = Reader["nombreContratista"].ToString();
                            objENT.rucContratista = Reader["ruc"].ToString();
                            objENT.representante = Reader["Representante"].ToString();


                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoModalidad()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoModalidad";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoAlcanceconvenio()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoAlcanceconvenio";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoContratacion()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoContratacion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_SubContrato(BE_MON_PROCESO _BE)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_SubContrato";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@flagObra", SqlDbType.VarChar, 1).Value = _BE.flagObra;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.numero = Convert.ToInt32(Reader["numero"].ToString());
                            objENT.id_proceso_seleccion = Convert.ToInt32(Reader["id_proceso_seleccion"].ToString());
                            objENT.id_subContratos = Convert.ToInt32(Reader["id_subContratos"].ToString());
                            objENT.id_rubro = Convert.ToInt32(Reader["id_rubro"].ToString());
                            objENT.representante = Reader["rubro"].ToString();
                            objENT.nombre = Reader["nombre"].ToString();
                            objENT.monto = Reader["monto"].ToString();
                            objENT.plazo = Convert.ToInt32(Reader["plazo"].ToString());
                            objENT.id_tipoModalidad = Convert.ToInt32(Reader["id_tipoModalidad_contrato"].ToString());
                            objENT.modalidad = Reader["modalidad"].ToString();
                            objENT.urlDoc = Reader["urlDoc"].ToString();

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoModalidad_Contrato()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoModalidad_Contrato";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListaRubro()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ListaRubro";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDocumentoIdentificacion()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoDocumentoIdentificacion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoGradoInstruccion()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoGradoInstruccion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoOcupacion()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoOcupacion";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_ProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_ProcesoSeleccion]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Proceso.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_ResponsableObra(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_ResponsableObra]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Proceso.nombre);
                objDE.AddInParameter(objComando, "@NroResolucion", DbType.String, _BE_Proceso.nroResolucion);
                objDE.AddInParameter(objComando, "@cargo", DbType.String, _BE_Proceso.cargo);
                objDE.AddInParameter(objComando, "@cip", DbType.String, _BE_Proceso.cip);
                objDE.AddInParameter(objComando, "@id_tipoModalida", DbType.String, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Proceso.urlDoc);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spMON_Acceso(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_Acceso]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_ProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_ProcesoSeleccion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Proceso.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);

                objDE.AddInParameter(objComando, "@id_tipoModalidad", DbType.Int32, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@id_tipoAdjudicacion", DbType.Int32, _BE_Proceso.id_tipoAdjudicacion);
                objDE.AddInParameter(objComando, "@nroLic", DbType.String, _BE_Proceso.norLic);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Proceso.anio);
                objDE.AddInParameter(objComando, "@detalle", DbType.String, _BE_Proceso.detalle);
                objDE.AddInParameter(objComando, "@buenaPro", DbType.DateTime, _BE_Proceso.Date_buenapro);
                objDE.AddInParameter(objComando, "@buenaProConsentida", DbType.DateTime, _BE_Proceso.Date_buenaProConsentida);
                objDE.AddInParameter(objComando, "@valorReferencial", DbType.String, _BE_Proceso.valorReferencial);
                objDE.AddInParameter(objComando, "@id_tipoEmpresa", DbType.Int32, _BE_Proceso.id_tipoEmpresa);
                objDE.AddInParameter(objComando, "@nombreContratista", DbType.String, _BE_Proceso.nombreContratista);
                objDE.AddInParameter(objComando, "@rucContratista", DbType.String, _BE_Proceso.rucContratista);
                objDE.AddInParameter(objComando, "@montoConsorcio", DbType.String, _BE_Proceso.montoConsorcio);
                objDE.AddInParameter(objComando, "@NroContratoConsorcio", DbType.String, _BE_Proceso.NroContratoConsorcio);
                objDE.AddInParameter(objComando, "@fechaFirmaContrato", DbType.DateTime, _BE_Proceso.Date_fechaContrato);
                objDE.AddInParameter(objComando, "@representanteContratista", DbType.String, _BE_Proceso.representanteContratista);

                objDE.AddInParameter(objComando, "@urlCartaConsorcio", DbType.String, _BE_Proceso.urlCartaConsorcio);
                objDE.AddInParameter(objComando, "@urlContrato", DbType.String, _BE_Proceso.urlContrato);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);
                objDE.AddInParameter(objComando, "@id_tipoContratacion", DbType.String, _BE_Proceso.TipoContratacion);

                objDE.AddInParameter(objComando, "@nombreConsorcio", DbType.String, _BE_Proceso.nombreConsorcio);
                objDE.AddInParameter(objComando, "@rucConsorcio", DbType.String, _BE_Proceso.rucConsorcio);
                objDE.AddInParameter(objComando, "@representanteConsorcio", DbType.String, _BE_Proceso.representanteConsorcio);
                objDE.AddInParameter(objComando, "@telefonoConsorcio", DbType.String, _BE_Proceso.telefonoConsorcio);

                objDE.AddInParameter(objComando, "@entidadEjecutora", DbType.String, _BE_Proceso.EntidadEjecutora);
                objDE.AddInParameter(objComando, "@coordinadorEJ", DbType.String, _BE_Proceso.CoordinadorEJ);
                objDE.AddInParameter(objComando, "@telefonoCoordEJ", DbType.String, _BE_Proceso.TelefonoCoordEJ);
                objDE.AddInParameter(objComando, "@correoCoordEJ", DbType.String, _BE_Proceso.CorreoCoordEJ);
                objDE.AddInParameter(objComando, "@fechaConvenio", DbType.DateTime, _BE_Proceso.Date_fechaConvenio);
                objDE.AddInParameter(objComando, "@convenioContrato", DbType.String, _BE_Proceso.ConvenioContrato);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_ProcesoSeleccionPE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_ProcesoSeleccionPE]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Proceso.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);

                objDE.AddInParameter(objComando, "@id_tipoModalidad", DbType.Int32, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@id_tipoAdjudicacion", DbType.Int32, _BE_Proceso.id_tipoAdjudicacion);
                objDE.AddInParameter(objComando, "@nroLic", DbType.String, _BE_Proceso.norLic);
                objDE.AddInParameter(objComando, "@anio", DbType.String, _BE_Proceso.anio);
                objDE.AddInParameter(objComando, "@detalle", DbType.String, _BE_Proceso.detalle);
                objDE.AddInParameter(objComando, "@buenaPro", DbType.DateTime, _BE_Proceso.Date_buenapro);
                objDE.AddInParameter(objComando, "@buenaProConsentida", DbType.DateTime, _BE_Proceso.Date_buenaProConsentida);
                objDE.AddInParameter(objComando, "@valorReferencial", DbType.String, _BE_Proceso.valorReferencial);
                objDE.AddInParameter(objComando, "@id_tipoEmpresa", DbType.Int32, _BE_Proceso.id_tipoEmpresa);
                objDE.AddInParameter(objComando, "@nombreContratista", DbType.String, _BE_Proceso.nombreContratista);
                objDE.AddInParameter(objComando, "@rucContratista", DbType.String, _BE_Proceso.rucContratista);
                objDE.AddInParameter(objComando, "@id_tipoMoneda", DbType.Int32, _BE_Proceso.id_tipoMoneda);
                objDE.AddInParameter(objComando, "@montoConsorcio", DbType.String, _BE_Proceso.montoConsorcio);
                objDE.AddInParameter(objComando, "@NroContratoConsorcio", DbType.String, _BE_Proceso.NroContratoConsorcio);
                objDE.AddInParameter(objComando, "@fechaFirmaContrato", DbType.DateTime, _BE_Proceso.Date_fechaContrato);
                objDE.AddInParameter(objComando, "@representanteContratista", DbType.String, _BE_Proceso.representanteContratista);

                objDE.AddInParameter(objComando, "@urlCartaConsorcio", DbType.String, _BE_Proceso.urlCartaConsorcio);
                objDE.AddInParameter(objComando, "@urlContrato", DbType.String, _BE_Proceso.urlContrato);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);
                objDE.AddInParameter(objComando, "@id_tipoContratacion", DbType.String, _BE_Proceso.TipoContratacion);

                objDE.AddInParameter(objComando, "@nombreConsorcio", DbType.String, _BE_Proceso.nombreConsorcio);
                objDE.AddInParameter(objComando, "@rucConsorcio", DbType.String, _BE_Proceso.rucConsorcio);
                objDE.AddInParameter(objComando, "@representanteConsorcio", DbType.String, _BE_Proceso.representanteConsorcio);
                objDE.AddInParameter(objComando, "@telefonoConsorcio", DbType.String, _BE_Proceso.telefonoConsorcio);

                objDE.AddInParameter(objComando, "@entidadContratante", DbType.String, _BE_Proceso.entidadContratante);
                objDE.AddInParameter(objComando, "@comentario", DbType.String, _BE_Proceso.comentario);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_SubContrato(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_SubContrato]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);

                objDE.AddInParameter(objComando, "@id_rubro", DbType.Int32, _BE_Proceso.id_rubro);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Proceso.nombre);
                objDE.AddInParameter(objComando, "@NroRUC", DbType.String, _BE_Proceso.NroRuc);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Proceso.monto);
                objDE.AddInParameter(objComando, "@id_tipoModalida", DbType.String, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Proceso.plazo);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Proceso.urlDoc);

                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Procesos_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Procesos_Eliminar]");

                objDE.AddInParameter(objComando, "@id_seguimiento", DbType.Int32, _BE_Proceso.id_seguimiento);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Procesos_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Procesos_Editar]");

                objDE.AddInParameter(objComando, "@id_seguimiento", DbType.Int32, _BE_Proceso.id_seguimiento);
                objDE.AddInParameter(objComando, "@convocatoria", DbType.String, _BE_Proceso.convocatoria);
                objDE.AddInParameter(objComando, "@fechaPublicacion", DbType.DateTime, _BE_Proceso.Date_fechaPublicacion);
                objDE.AddInParameter(objComando, "@id_tipoAdjudicacion", DbType.Int32, _BE_Proceso.id_tipoAdjudicacion);
                objDE.AddInParameter(objComando, "@resultado", DbType.String, _BE_Proceso.resultado);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Proceso.urlDoc);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Proceso.observacion);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_SubContrato_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_SubContrato_Eliminar]");

                objDE.AddInParameter(objComando, "@id_subContratos", DbType.Int32, _BE_Proceso.id_subContratos);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_SubContrato_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_SubContrato_Editar]");

                objDE.AddInParameter(objComando, "@id_subContratos", DbType.Int32, _BE_Proceso.id_subContratos);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);
                objDE.AddInParameter(objComando, "@id_rubro", DbType.Int32, _BE_Proceso.id_rubro);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Proceso.nombre);
                objDE.AddInParameter(objComando, "@monto", DbType.String, _BE_Proceso.monto);
                objDE.AddInParameter(objComando, "@plazo", DbType.Int32, _BE_Proceso.plazo);
                objDE.AddInParameter(objComando, "@id_tipoModalida", DbType.String, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Proceso.urlDoc);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Responsable_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Responsable_Eliminar]");

                objDE.AddInParameter(objComando, "@id_responsable", DbType.Int32, _BE_Proceso.id_responsable);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Responsable_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Responsable_Editar]");

                objDE.AddInParameter(objComando, "@id_responsable", DbType.Int32, _BE_Proceso.id_responsable);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);
                objDE.AddInParameter(objComando, "@cargo", DbType.String, _BE_Proceso.cargo);
                objDE.AddInParameter(objComando, "@NroResolucion", DbType.String, _BE_Proceso.nroResolucion);
                objDE.AddInParameter(objComando, "@nombre", DbType.String, _BE_Proceso.nombre);
                objDE.AddInParameter(objComando, "@cip", DbType.String, _BE_Proceso.cip);
                objDE.AddInParameter(objComando, "@id_tipoModalida", DbType.String, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Proceso.urlDoc);



                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Consorcio_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Consorcio_Eliminar]");

                objDE.AddInParameter(objComando, "@id_grupo_consorcio", DbType.Int32, _BE_Proceso.id_grupo_consorcio);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spud_MON_Seguimiento_Consorcio_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spud_MON_Seguimiento_Consorcio_Editar]");

                objDE.AddInParameter(objComando, "@id_grupo_consorcio", DbType.Int32, _BE_Proceso.id_grupo_consorcio);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);
                objDE.AddInParameter(objComando, "@nombreContratista", DbType.String, _BE_Proceso.nombreContratista);
                objDE.AddInParameter(objComando, "@ruc", DbType.String, _BE_Proceso.rucContratista);
                objDE.AddInParameter(objComando, "@representante", DbType.String, _BE_Proceso.representante);


                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spu_MON_ModalidadEjecucion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spu_MON_ModalidadEjecucion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Proceso.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);
                objDE.AddInParameter(objComando, "@id_tipoModalidad", DbType.Int32, _BE_Proceso.id_tipoModalidad);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int spi_MON_SeguimientoProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_SeguimientoProcesoSeleccion]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Proceso.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@convocatoria", DbType.String, _BE_Proceso.convocatoria);
                objDE.AddInParameter(objComando, "@id_tipoAdjudicacion", DbType.Int32, _BE_Proceso.id_tipoAdjudicacion);
                objDE.AddInParameter(objComando, "@fechaPublicacion", DbType.DateTime, _BE_Proceso.Date_fechaPublicacion);
                objDE.AddInParameter(objComando, "@resultado", DbType.String, _BE_Proceso.resultado);
                objDE.AddInParameter(objComando, "@observacion", DbType.String, _BE_Proceso.observacion);
                objDE.AddInParameter(objComando, "@urlDoc", DbType.String, _BE_Proceso.urlDoc);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_spi_MON_ProcesoSeleccionIndirectaNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_ProcesoSeleccionIndirectaNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.Int).Value = _BE.flagObra;
                    cmd.Parameters.Add("@id_tipoModalidad", SqlDbType.Decimal).Value = _BE.id_tipoModalidad;
                    //cmd.Parameters.Add("@fechaConvenio", SqlDbType.DateTime).Value = _BE.Date_fechaConvenio;
                    //cmd.Parameters.Add("@convenioContrato", SqlDbType.VarChar, 50).Value = _BE.ConvenioContrato;
                    //cmd.Parameters.Add("@idTipoAlcanceConvenio", SqlDbType.Int).Value = _BE.idTipoAlcance;
                    cmd.Parameters.Add("@fechaConstitucionNE", SqlDbType.DateTime).Value = _BE.Date_fechActaConstitucionNE;
                    cmd.Parameters.Add("@asistentesHombres", SqlDbType.Int).Value = _BE.hombres;
                    cmd.Parameters.Add("@asistentesMujeres", SqlDbType.Int).Value = _BE.mujeres;
                    cmd.Parameters.Add("@urlActaAsamblea", SqlDbType.VarChar, 200).Value = _BE.urlActaAsamblea;
                    //cmd.Parameters.Add("@urlConvenioNE", SqlDbType.VarChar, 200).Value = _BE.urlConvenio;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_ProcesoSeleccionIndirectaNEConvenio(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_ProcesoSeleccionIndirectaNEConvenio", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.Int).Value = _BE.flagObra;
                    cmd.Parameters.Add("@id_tipoModalidad", SqlDbType.Decimal).Value = _BE.id_tipoModalidad;
                    cmd.Parameters.Add("@fechaConvenio", SqlDbType.DateTime).Value = _BE.Date_fechaConvenio;
                    cmd.Parameters.Add("@convenioContrato", SqlDbType.VarChar, 50).Value = _BE.ConvenioContrato;
                    //cmd.Parameters.Add("@idTipoAlcanceConvenio", SqlDbType.Int).Value = _BE.idTipoAlcance;
                    //cmd.Parameters.Add("@fechaConstitucionNE", SqlDbType.DateTime).Value = _BE.Date_fechActaConstitucionNE;
                    //cmd.Parameters.Add("@asistentesHombres", SqlDbType.Int).Value = _BE.hombres;
                    //cmd.Parameters.Add("@asistentesMujeres", SqlDbType.Int).Value = _BE.mujeres;
                    //cmd.Parameters.Add("@urlActaAsamblea", SqlDbType.VarChar, 200).Value = _BE.urlActaAsamblea;
                    cmd.Parameters.Add("@urlConvenioNE", SqlDbType.VarChar, 200).Value = _BE.urlConvenio;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_ProcesoSeleccionIndirectaNESolicitud(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_ProcesoSeleccionIndirectaNESolicitud", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.Int).Value = _BE.flagObra;
                    cmd.Parameters.Add("@id_tipoModalidad", SqlDbType.Decimal).Value = _BE.id_tipoModalidad;

                    cmd.Parameters.Add("@fechaSolicitud", SqlDbType.DateTime).Value = _BE.Date_fechaSolicitud;
                    cmd.Parameters.Add("@fechaAprobacionSolicitud", SqlDbType.DateTime).Value = _BE.Date_fechaAprobacionSolicitud;
                    cmd.Parameters.Add("@urlSolicitud", SqlDbType.VarChar, 200).Value = _BE.urlSolicitud;
                    cmd.Parameters.Add("@flagResultado", SqlDbType.Int).Value = _BE.flagResultado;
                    cmd.Parameters.Add("@motivoRechazo", SqlDbType.VarChar, 3000).Value = _BE.observacion;
                    cmd.Parameters.Add("@urlMotivoRechazo", SqlDbType.VarChar, 200).Value = _BE.urlDoc;


                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_RendicionCuentaNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_RendicionCuentaNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.Int).Value = _BE.flagObra;
                    cmd.Parameters.Add("@id_tipoModalidad", SqlDbType.Int).Value = _BE.id_tipoModalidad;

                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fechaPublicacion;
                    cmd.Parameters.Add("@monto", SqlDbType.VarChar, 18).Value = _BE.monto;
                    cmd.Parameters.Add("@nroHombre", SqlDbType.Int).Value = _BE.hombres;
                    cmd.Parameters.Add("@nroMujeres", SqlDbType.Int).Value = _BE.mujeres;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int UD_spud_MON_RendicionCuentaNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_RendicionCuentaNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_rendicionCuenta", SqlDbType.Int).Value = _BE.id;

                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.id_tipo;

                    cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = _BE.Date_fechaPublicacion;
                    cmd.Parameters.Add("@monto", SqlDbType.VarChar, 18).Value = _BE.monto;
                    cmd.Parameters.Add("@nroHombre", SqlDbType.Int).Value = _BE.hombres;
                    cmd.Parameters.Add("@nroMujeres", SqlDbType.Int).Value = _BE.mujeres;
                    cmd.Parameters.Add("@urlDoc", SqlDbType.VarChar, 200).Value = _BE.urlDoc;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int I_spi_MON_ProcesoMiembroNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_ProcesoMiembroNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@flagObra", SqlDbType.Int).Value = _BE.flagObra;
                    cmd.Parameters.Add("@id_tipoModalidad", SqlDbType.Int).Value = _BE.id_tipoModalidad;

                    cmd.Parameters.Add("@idTipoDocumento", SqlDbType.Int).Value = _BE.id_tipoDocumentoidentificacion;
                    cmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 8).Value = _BE.nroDocumento;

                    cmd.Parameters.Add("@idTipoCargo", SqlDbType.Int).Value = _BE.id_TipoCargo;
                    cmd.Parameters.Add("@idTipoEstadoAsignacion", SqlDbType.Int).Value = _BE.id_TipoEstadoAsignacion;

                    cmd.Parameters.Add("@fechaAsignacion", SqlDbType.DateTime).Value = _BE.Date_fechaAsignacion;
                    cmd.Parameters.Add("@fechaDesignacion", SqlDbType.DateTime).Value = _BE.Date_fechaDesignacion;

                    cmd.Parameters.Add("@urlActaDesignacion", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int spi_MON_GrupoConsorcio(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spi_MON_GrupoConsorcio]");

                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@tipoFinanciamiento", DbType.Int32, _BE_Proceso.tipoFinanciamiento);
                objDE.AddInParameter(objComando, "@flagObra", DbType.String, _BE_Proceso.flagObra);

                objDE.AddInParameter(objComando, "@nombreContratista", DbType.String, _BE_Proceso.nombreContratista);
                objDE.AddInParameter(objComando, "@ruc", DbType.String, _BE_Proceso.rucContratista);

                objDE.AddInParameter(objComando, "@representante", DbType.String, _BE_Proceso.representante);
                objDE.AddInParameter(objComando, "@id_usuario", DbType.Int32, _BE_Proceso.id_usuario);

                return Convert.ToInt32(objDE.ExecuteScalar(objComando));
            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_spi_MON_PadronMiembrosNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_PadronMiembrosNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@tipoDocumento", SqlDbType.Int).Value = _BE.tipoDocumento;
                    cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar, 8).Value = _BE.nroDocumento;
                    cmd.Parameters.Add("@ApellidoPaterno", SqlDbType.VarChar, 50).Value = _BE.apellidoPaterno;
                    cmd.Parameters.Add("@ApellidoMaterno", SqlDbType.VarChar, 50).Value = _BE.apellidoMaterno;
                    cmd.Parameters.Add("@nombres", SqlDbType.VarChar, 50).Value = _BE.nombre;
                    cmd.Parameters.Add("@tipoSexo", SqlDbType.Char, 1).Value = _BE.sexo;
                    cmd.Parameters.Add("@fechaNacimiento", SqlDbType.DateTime).Value = _BE.Date_fechaNacimiento;
                    cmd.Parameters.Add("@domicilio", SqlDbType.VarChar, 200).Value = _BE.domicilio;
                    cmd.Parameters.Add("@cod_depa", SqlDbType.Char, 2).Value = _BE.cod_depa;
                    cmd.Parameters.Add("@cod_prov", SqlDbType.Char, 2).Value = _BE.cod_prov;
                    cmd.Parameters.Add("@cod_dist", SqlDbType.Char, 2).Value = _BE.cod_dist;
                    cmd.Parameters.Add("@cod_CCPP", SqlDbType.Char, 10).Value = _BE.cod_CCPP;

                    cmd.Parameters.Add("@tipoGrado", SqlDbType.Int).Value = _BE.tipoGrado;
                    cmd.Parameters.Add("@tipoOcupacion", SqlDbType.Int).Value = _BE.tipoOcupacion;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_PadronMiembrosNE()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_PadronMiembrosNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int id_padronMiembroNE = Reader.GetOrdinal("id_padronMiembroNE");
                        int nroDocumento = Reader.GetOrdinal("nroDocumento");
                        int apellidoPaterno = Reader.GetOrdinal("apellidoPaterno");
                        int apellidoMaterno = Reader.GetOrdinal("apellidoMaterno");
                        int nombres = Reader.GetOrdinal("nombres");
                        int fechaNacimiento = Reader.GetOrdinal("fechaNacimiento");
                        int domicilio = Reader.GetOrdinal("domicilio");
                        int id_tipoDocumentoidentificacion = Reader.GetOrdinal("id_tipoDocumentoidentificacion");


                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.id = Reader.GetInt32(id_padronMiembroNE);
                            objENT.nroDocumento = Reader.GetString(nroDocumento);
                            objENT.apellidoPaterno = Reader.GetString(apellidoPaterno);
                            objENT.apellidoMaterno = Reader.GetString(apellidoMaterno);
                            objENT.nombre = Reader.GetString(nombres);
                            objENT.fechaNacimiento = Reader.GetString(fechaNacimiento);
                            objENT.domicilio = Reader.GetString(domicilio);
                            objENT.id_tipoDocumentoidentificacion = Reader.GetInt32(id_tipoDocumentoidentificacion);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoCargoMiembrosNE()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoCargoMiembrosNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoAsignacionNE()
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_BANDEJA objENT = new BE_MON_BANDEJA();
                List<BE_MON_BANDEJA> objCollection = new List<BE_MON_BANDEJA>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_TipoEstadoAsignacionNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {
                        int valor = Reader.GetOrdinal("valor");
                        int nombre = Reader.GetOrdinal("nombre");

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_BANDEJA();

                            objENT.valor = Reader.GetInt32(valor);
                            objENT.nombre = Reader.GetString(nombre);

                            objCollection.Add(objENT);

                            objENT = null;
                        }
                    }
                    return objCollection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_ProcesoMiembroNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_ProcesoMiembroNE";
                    cmd.CommandTimeout = 36000;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@flagObra", SqlDbType.VarChar, 1).Value = _BE.flagObra;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Connection.Open();
                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.id = Convert.ToInt32(Reader["id_miembroNE"].ToString());
                            objENT.id_padronMiembroNE = Convert.ToInt32(Reader["id_padronMiembroNE"].ToString());
                            objENT.nombre = Reader["nombre"].ToString();
                            objENT.id_TipoCargo = Convert.ToInt32(Reader["id_tipoCargoMiembroNE"].ToString());
                            objENT.tipoCargo = Reader["CargoMiembroNE"].ToString();
                            objENT.id_TipoEstadoAsignacion = Convert.ToInt32(Reader["id_estadoAsignacion"].ToString());
                            objENT.tipoEstadoAsignacion = Reader["EstadoAsignacion"].ToString();
                            //objENT.fechaAsignacion = Reader["fechaAsignacion"].ToString();

                            //if (Reader["fechaAsignacion"].ToString() == "")
                            //{
                            //    objENT.dtFechaAsignacion = "01/01/0001";
                            //}
                            //else
                            //{
                            objENT.dtFechaAsignacion = Convert.ToDateTime(Reader["fechaAsignacion"].ToString());
                            //}

                            objENT.urlDoc = Reader["urlActaDesignacion"].ToString();

                            if (Reader["fechaActaDesignacion"].ToString() == "")
                            {
                                objENT.dtFechaDesignacion = null;
                            }
                            else
                            {
                                objENT.dtFechaDesignacion = Convert.ToDateTime(Reader["fechaActaDesignacion"].ToString());
                            }

                            objENT.usuario = Reader["usuario"].ToString();
                            objENT.strFecha_update = Reader["fecha_update"].ToString();
                            objENT.id_tipoDocumentoidentificacion = Convert.ToInt32(Reader["id_tipoDocumentoidentificacion"].ToString());
                            objENT.nroDocumento = Reader["nroDocumento"].ToString(); ;
                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public List<BE_MON_PROCESO> F_spMON_AprobacionExpediente(int id_proyecto)
        {
            using (SqlConnection Cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["CnSTD"].ConnectionString))
            {
                BE_MON_PROCESO objENT = new BE_MON_PROCESO();
                List<BE_MON_PROCESO> objCollection = new List<BE_MON_PROCESO>();
                SqlCommand cmd = default(SqlCommand);
                try
                {
                    cmd = new SqlCommand();
                    cmd.Connection = Cnx;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spMON_AprobacionExpediente";
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;
                    cmd.Connection.Open();

                    using (SqlDataReader Reader = cmd.ExecuteReader())
                    {

                        while (Reader.Read())
                        {
                            objENT = new BE_MON_PROCESO();

                            objENT.strFechaAprobacionExpediente = Reader["fecha_aprobacionExpediente"].ToString();
                            objENT.nroResolucion = Reader["aprobacionExpedienteRD"].ToString();
                            objENT.montoFinanciadoMVCS = Reader["monto_financiamientoPrograma"].ToString();
                            objENT.montoAprobadoExpediente = Reader["monto_AprobadoExpediente"].ToString();
                            objENT.montoTransferencia = Reader["monto_TransferirNE"].ToString();
                            objENT.urlDoc = Reader["urlResolucionDirectorial"].ToString();

                            objENT.usuario = Reader["usuario"].ToString();
                            objENT.strFecha_update = Reader["fechaUpdateExpediente"].ToString();

                            objCollection.Add(objENT);

                            objENT = null;
                        }

                    }
                    //   Reader.Close();
                    return objCollection;

                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    cmd = null;
                    if (Cnx.State == ConnectionState.Open)
                    {
                        Cnx.Close();
                    }
                }
            }
        }

        public BE_MON_PROCESO F_spMON_SaldoBancoNE(int id_proyecto)
        {
            BE_MON_PROCESO objENT = new BE_MON_PROCESO();
            SqlConnection Cnx = new SqlConnection(strCadenaConexion);
            SqlCommand cmd = default(SqlCommand);
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = Cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spMON_SaldoBancoNE";
                cmd.CommandTimeout = 36000;
                cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = id_proyecto;

                cmd.Connection.Open();
                SqlDataReader Reader = cmd.ExecuteReader();

                int saldo = Reader.GetOrdinal("saldo");
                int usuario = Reader.GetOrdinal("usuario");
                int fecha_update = Reader.GetOrdinal("fecha_update");

                if (Reader.Read())
                {
                    objENT = new BE_MON_PROCESO();

                    objENT.monto = Reader.GetString(saldo);
                    objENT.usuario = Reader.GetString(usuario);
                    objENT.strFecha_update = Reader.GetDateTime(fecha_update).ToString();

                }

                return objENT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd = null;
                if (Cnx.State == ConnectionState.Open)
                {
                    Cnx.Close();
                }
            }

        }

        public int U_AprobacionExpedienteNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spu_AprobacionExpediente", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@fecha_aprobacionExpediente", SqlDbType.DateTime).Value = _BE.Date_fechaPublicacion;
                    cmd.Parameters.Add("@monto_financiamientoPrograma", SqlDbType.Float).Value = _BE.montoFinanciadoMVCS;
                    cmd.Parameters.Add("@monto_AprobadoExpediente", SqlDbType.Float).Value = _BE.montoAprobadoExpediente;
                    cmd.Parameters.Add("@monto_TransferirNE", SqlDbType.Float).Value = _BE.montoTransferencia;
                    cmd.Parameters.Add("@urlResolucionDirectorial", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@aprobacionExpedienteRD", SqlDbType.VarChar, 50).Value = _BE.nroResolucion;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }
        public int UD_spud_MON_ProcesoMiembroNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_ProcesoMiembroNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = _BE.id;

                    cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = _BE.id_tipo;

                    cmd.Parameters.Add("@idTipoDocumento", SqlDbType.Int).Value = _BE.id_tipoDocumentoidentificacion;
                    cmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 8).Value = _BE.nroDocumento;

                    cmd.Parameters.Add("@idTipoCargo", SqlDbType.Int).Value = _BE.id_TipoCargo;
                    cmd.Parameters.Add("@idTipoEstadoAsignacion", SqlDbType.Int).Value = _BE.id_TipoEstadoAsignacion;

                    cmd.Parameters.Add("@fechaAsignacion", SqlDbType.DateTime).Value = _BE.Date_fechaAsignacion;
                    cmd.Parameters.Add("@fechaDesignacion", SqlDbType.DateTime).Value = _BE.Date_fechaDesignacion;

                    cmd.Parameters.Add("@urlActaDesignacion", SqlDbType.VarChar, 200).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_spiu_MON_SaldoBancoNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_SaldoBancoNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@saldo", SqlDbType.VarChar, 18).Value = _BE.monto;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public DataSet spMON_DatosPreoperativosNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_DatosPreoperativosNE]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int IU_spiu_MON_CuentaBancariaNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_CuentaBancariaNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@id_tipoBanco", SqlDbType.Int).Value = _BE.id_tipo;
                    cmd.Parameters.Add("@nroCuenta", SqlDbType.VarChar, 50).Value = _BE.numeroCuenta;
                    cmd.Parameters.Add("@fechaApertura", SqlDbType.DateTime).Value = _BE.Date_fechaApertura;
                    cmd.Parameters.Add("@nombreAgencia", SqlDbType.VarChar, 100).Value = _BE.nombre;
                    cmd.Parameters.Add("@ubigeoAgencia", SqlDbType.Char, 6).Value = _BE.cod_dist;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int IU_spiu_MON_SesionesNE(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spiu_MON_SesionesNE", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@fechaSesion", SqlDbType.DateTime).Value = _BE.Date_fecha;
                    cmd.Parameters.Add("@nroAsistentesHombres", SqlDbType.Int).Value = _BE.hombres;
                    cmd.Parameters.Add("@nroAsistentesMujeres", SqlDbType.Int).Value = _BE.mujeres;
                    cmd.Parameters.Add("@actaSesion", SqlDbType.VarChar, 100).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int I_spi_MON_Modalidad(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_Modalidad", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;
                    cmd.Parameters.Add("@id_tipoModalidad", SqlDbType.Int).Value = _BE.id_tipoModalidad;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_PersonalClave(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("[spMON_PersonalClave]");
                objDE.AddInParameter(objComando, "@id_proyecto", DbType.Int32, _BE_Proceso.id_proyecto);
                objDE.AddInParameter(objComando, "@flagObra", DbType.Int32, _BE_Proceso.flagObra);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public int I_MON_PersonalClave(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spi_MON_PersonalClave", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_proyecto", SqlDbType.Int).Value = _BE.id_proyecto;
                    cmd.Parameters.Add("@flagObra", SqlDbType.Int).Value = _BE.flagObra;
                    cmd.Parameters.Add("@tipoFinanciamiento", SqlDbType.Int).Value = _BE.tipoFinanciamiento;

                    cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 500).Value = _BE.nombre;
                    cmd.Parameters.Add("@dni", SqlDbType.VarChar, 500).Value = _BE.dni;
                    cmd.Parameters.Add("@cip", SqlDbType.VarChar, 500).Value = _BE.cip;
                    cmd.Parameters.Add("@especialidad", SqlDbType.VarChar, 500).Value = _BE.especialidad;
                    cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 500).Value = _BE.telefono;
                    cmd.Parameters.Add("@correo", SqlDbType.VarChar, 500).Value = _BE.correo;
                    cmd.Parameters.Add("@urlDocCV", SqlDbType.VarChar, 500).Value = _BE.urlDoc;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }


        public int UD_MON_PersonalClave(BE_MON_PROCESO _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("spud_MON_PersonalClave", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@id_personalClave", SqlDbType.Int).Value = _BE.id;

                    cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 500).Value = _BE.nombre;
                    cmd.Parameters.Add("@dni", SqlDbType.VarChar, 500).Value = _BE.dni;
                    cmd.Parameters.Add("@cip", SqlDbType.VarChar, 500).Value = _BE.cip;
                    cmd.Parameters.Add("@especialidad", SqlDbType.VarChar, 500).Value = _BE.especialidad;
                    cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 500).Value = _BE.telefono;
                    cmd.Parameters.Add("@correo", SqlDbType.VarChar, 500).Value = _BE.correo;
                    cmd.Parameters.Add("@urlDocCV", SqlDbType.VarChar, 500).Value = _BE.urlDoc;
                    cmd.Parameters.Add("@tipo", SqlDbType.VarChar, 500).Value = _BE.id_tipo;

                    cmd.Parameters.Add("@id_usuario", SqlDbType.Int).Value = _BE.id_usuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public int paSSP_HitosProyecto(BE_HitosProyecto _BE)
        {
            using (SqlConnection strCnx = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString()))
            {
                SqlCommand cmd;
                try
                {
                    cmd = new SqlCommand("paSSP_HitosProyecto", strCnx);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@iId", SqlDbType.Int).Value = _BE.iId;
                    cmd.Parameters.Add("@EtapaRegistro", SqlDbType.Int).Value = _BE.EtapaRegistro;
                    if (_BE.dFechaInicioProyecto != null)
                        cmd.Parameters.Add("@dFechaInicioProyecto", SqlDbType.Date).Value = _BE.dFechaInicioProyecto;
                    else
                        cmd.Parameters.Add("@dFechaInicioProyecto", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaFinProyecto != null)
                        cmd.Parameters.Add("@dFechaFinProyecto", SqlDbType.Date).Value = _BE.dFechaFinProyecto;
                    else
                        cmd.Parameters.Add("@dFechaFinProyecto", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaConvocatoria != null)
                        cmd.Parameters.Add("@dFechaConvocatoria", SqlDbType.Date).Value = _BE.dFechaConvocatoria;
                    else
                        cmd.Parameters.Add("@dFechaConvocatoria", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaRegistroParticipantes != null)
                        cmd.Parameters.Add("@dFechaRegistroParticipantes", SqlDbType.Date).Value = _BE.dFechaRegistroParticipantes;
                    else
                        cmd.Parameters.Add("@dFechaRegistroParticipantes", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaFormulacion != null)
                        cmd.Parameters.Add("@dFechaFormulacion", SqlDbType.Date).Value = _BE.dFechaFormulacion;
                    else
                        cmd.Parameters.Add("@dFechaFormulacion", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaIntegracionBases != null)
                        cmd.Parameters.Add("@dFechaIntegracionBases", SqlDbType.Date).Value = _BE.dFechaIntegracionBases;
                    else
                        cmd.Parameters.Add("@dFechaIntegracionBases", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaPreparacionOfertas != null)
                        cmd.Parameters.Add("@dFechaPreparacionOfertas", SqlDbType.Date).Value = _BE.dFechaPreparacionOfertas;
                    else
                        cmd.Parameters.Add("@dFechaPreparacionOfertas", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaEvaluacionOferta != null)
                        cmd.Parameters.Add("@dFechaEvaluacionOferta", SqlDbType.Date).Value = _BE.dFechaEvaluacionOferta;
                    else
                        cmd.Parameters.Add("@dFechaEvaluacionOferta", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaOtorgamientoBuenaPro != null)
                        cmd.Parameters.Add("@dFechaOtorgamientoBuenaPro", SqlDbType.Date).Value = _BE.dFechaOtorgamientoBuenaPro;
                    else
                        cmd.Parameters.Add("@dFechaOtorgamientoBuenaPro", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaConsentimientoBuenaPro != null)
                        cmd.Parameters.Add("@dFechaConsentimientoBuenaPro", SqlDbType.Date).Value = _BE.dFechaConsentimientoBuenaPro;
                    else
                        cmd.Parameters.Add("@dFechaConsentimientoBuenaPro", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaPresentacionDocumentoContrato != null)
                        cmd.Parameters.Add("@dFechaPresentacionDocumentoContrato", SqlDbType.Date).Value = _BE.dFechaPresentacionDocumentoContrato;
                    else
                        cmd.Parameters.Add("@dFechaPresentacionDocumentoContrato", SqlDbType.Date).Value = DBNull.Value;

                    if (_BE.dFechaFirmaContrato != null)
                        cmd.Parameters.Add("@dFechaFirmaContrato", SqlDbType.Date).Value = _BE.dFechaFirmaContrato;
                    else
                        cmd.Parameters.Add("@dFechaFirmaContrato", SqlDbType.Date).Value = DBNull.Value;


                    if (_BE.dFechaActoPosteriorFirmaContrato != null)
                        cmd.Parameters.Add("@dFechaActoPosteriorFirmaContrato", SqlDbType.Date).Value = _BE.dFechaActoPosteriorFirmaContrato;
                    else
                        cmd.Parameters.Add("@dFechaActoPosteriorFirmaContrato", SqlDbType.Date).Value = DBNull.Value;
                     
                    cmd.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = _BE.IdUsuario;

                    cmd.Connection.Open();
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    cmd = null;
                    if (strCnx.State == ConnectionState.Open)
                    {
                        strCnx.Close();
                    }
                }
            }
        }

        public DataSet spMON_HitosProyecto(int iId, int EtapaRegistro)
        {
            try
            {
                SqlDatabase objDE = new SqlDatabase(strCadenaConexion);
                DbCommand objComando = objDE.GetStoredProcCommand("spMON_HitosProyecto");
                objDE.AddInParameter(objComando, "@iId", DbType.Int32, iId);
                objDE.AddInParameter(objComando, "@EtapaRegistro", DbType.Int32, EtapaRegistro);

                return objDE.ExecuteDataSet(objComando);

            }
            catch (DbException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }

}


