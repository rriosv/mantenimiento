﻿using System;
using System.Collections.Generic;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Menu
    {
        /// <summary>
        /// Devuelve una lista de etapas del proyecto segun el tipo de pantalla a procesar
        /// </summary>
        /// <param name="snip"></param>
        /// <param name="pPantalla">
        /// 1- Obra
        /// 2- Preinversion
        /// 3- Expediente tecnico
        /// </param>
        /// <returns></returns>
        public List<BE_MON_Menu> paSSP_MON_rEtapas(int snip, int pPantalla)
        {
            DE_MON_Menu objDet = new DE_MON_Menu();
            List<BE_MON_Menu> objCollection = new List<BE_MON_Menu>();
            try
            {
                objCollection = objDet.paSSP_MON_rEtapas(snip, pPantalla);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
    }
}
