﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class BL_Notificacion
    {

        public DataTable spMON_Notificacion(string pSnip, string pUnificado, Int32 pIdUsuario)
        {
            try
            {
                DataSet ds;
                DE_Notificacion objProy = new DE_Notificacion();
                ds = objProy.spMON_Notificacion(pSnip, pUnificado, pIdUsuario);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
