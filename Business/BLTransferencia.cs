using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using Entity;
using System.Data;
using System.Data.Common;
namespace Business
{
    public class BLTransferencia
    {
      
      
        #region Consulta
        public DataTable spSOL_Seguimiento_Det_Transferenica(BETransferencia _BETransferencia)
        {
            try
            {
                DataSet dstProyecto;
                DETransferencia objProy = new DETransferencia();
                dstProyecto = objProy.spSOL_Seguimiento_Det_Transferenica(_BETransferencia);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spSOL_Seguimiento_Transferencia(BETransferencia _BETransferencia)
        {
            try
            {
                DataSet dstProyecto;
                DETransferencia objProy = new DETransferencia();
                dstProyecto = objProy.spSOL_Seguimiento_Transferencia(_BETransferencia);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Reporte_Transferencia(BETransferencia _BETransferencia)
        {
            try
            {
                DataSet dstProyecto;
                DETransferencia objProy = new DETransferencia();
                dstProyecto = objProy.spSOL_Reporte_Transferencia(_BETransferencia);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Seguimiento_Presupuesto(BEPresupuesto _BEPresupuesto)
        {
            try
            {
                DataSet dstProyecto;
                DETransferencia objProy = new DETransferencia();
                dstProyecto = objProy.spSOL_Seguimiento_Presupuesto(_BEPresupuesto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #endregion
        #region "Transaccion"

        public void spiuSOL_Ingresar_Presupuesto(BEPresupuesto _BEPresupuesto)
        {
            try
            {
                DETransferencia objProy = new DETransferencia();
                objProy.spiuSOL_Ingresar_Presupuesto(_BEPresupuesto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public void spu_Actualizar_Montos_Tranf_MEF(Int64 id_transf, string ds, string oficio, int tipo, string OficioNro, string OficioAnio, string DocNro, string DocAnio, string fechaOficio,string fechaDoc, int idEntidadFinanciera)
        {
            try
            {
                DETransferencia objProy = new DETransferencia();
                objProy.spu_Actualizar_Montos_Tranf_MEF(id_transf, ds, oficio, tipo,OficioNro,OficioAnio,DocNro,DocAnio,fechaOficio,fechaDoc, idEntidadFinanciera);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public void spuSOL_Transferencia_MEF(BETransferencia _BETransferencia)
        {
            try
            {
                DETransferencia objProy = new DETransferencia();
                objProy.spuSOL_Transferencia_MEF(_BETransferencia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public Int64 spiuSOL_Transferencia(BETransferencia _BETransferencia)
        {
            try
            {
                DETransferencia objProy = new DETransferencia();
                return objProy.spiuSOL_Transferencia(_BETransferencia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public Int64 spiuSOL_DetTransferencia(BETransferencia _BETransferencia)
        {
            try
            {
                DETransferencia objProy = new DETransferencia();
                return objProy.spiuSOL_DetTransferencia(_BETransferencia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public Int32 spuSOL_Convenio_DetTransferencia(BETransferencia _BETransferencia)
        {
            try
            {
                DETransferencia objProy = new DETransferencia();
                return objProy.spuSOL_Convenio_DetTransferencia(_BETransferencia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #endregion
    }
}
