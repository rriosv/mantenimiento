﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using DataAccess;

namespace Business
{
    public class BL_Resumen
    {

        public BE_FichaResumen fnObtenerUltimaFichaResumen(string CodSnip)
        {
            BE_FichaResumen resultado = new BE_FichaResumen();
            try
            {
                resultado = new DE_Resumen().fnObtenerUltimaFichaResumen(CodSnip);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public bool fn_Insert_ResFicha(BE_FichaResumen oFichaResumen)
        {
            bool bRpta = false;
            try
            {
                bRpta = new DE_Resumen().fn_Insert_ResFicha(oFichaResumen);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bRpta;

        }
    }
}