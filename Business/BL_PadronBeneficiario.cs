﻿using Entity;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Business
{
    public class BL_PadronBeneficiario
    {
        public List<BE_MON_PadronBeneficiario> Listar_PadronBeneficiario(int idProyecto)
        {
            return new DE_PadronBeneficiario().Listar_PadronBeneficiario(idProyecto);
        }

        public int Insertar_PadronBeneficiario(BE_MON_PadronBeneficiario padron)
        {
            
            return new DE_PadronBeneficiario().Insertar_PadronBeneficiario(padron); 
        }

        public int Actualizar_PadronBeneficiario(BE_MON_PadronBeneficiario padron)
        {
            return new DE_PadronBeneficiario().Actualizar_PadronBeneficiario(padron);
        }

        public int Eliminar_PadronBeneficiario(BE_MON_PadronBeneficiario padron)
        {
            return new DE_PadronBeneficiario().Eliminar_PadronBeneficiario(padron);
        }

        public void InsercionMasiva(DataTable dtExcelData)
        {
            new DE_PadronBeneficiario().InsercionMasiva(dtExcelData);
        }

        public IEnumerable<BE_MON_PadronBeneficiario> ValidarPadronBeneficiario(List<BE_MON_PadronBeneficiario> padron, int id_proyecto, int usr_registro)
        {
            return new DE_PadronBeneficiario().ValidarPadronBeneficiario(padron, id_proyecto, usr_registro);            
        }
    }
}
