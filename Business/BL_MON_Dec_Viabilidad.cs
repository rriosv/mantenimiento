﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Dec_Viabilidad
    {
      
        public DataTable spMON_Aprobacion(int id_proyecto, int id_tipoAprobacion)
        {
            try
            {
                DataSet ds;
                DE_MON_Dec_Viabilidad objProy = new DE_MON_Dec_Viabilidad();
                ds = objProy.spMON_Aprobacion(id_proyecto,id_tipoAprobacion);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_TipoDocumentoAprobacion(int id_tipoAprobacion)
        {
            try
            {
                DataSet ds;
                DE_MON_Dec_Viabilidad objProy = new DE_MON_Dec_Viabilidad();
                ds = objProy.spMON_TipoDocumentoAprobacion(id_tipoAprobacion);
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spi_MON_Aprobacion(int id_proyecto, int id_tipoAprobacion, int id_tipoDocumentoAprobacion, string detalleDocumento, DateTime fecha, string urlDoc, int id_usuario)
        {
            try
            {
                DE_MON_Dec_Viabilidad objProy = new DE_MON_Dec_Viabilidad();
                return objProy.spi_MON_Aprobacion(id_proyecto,id_tipoAprobacion,id_tipoDocumentoAprobacion,detalleDocumento,fecha,urlDoc,id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Aprobacion(int id_aprobacion, int id_tipoDocumentoAprobacion, string detalleDocumento, DateTime fecha, string urlDoc, int tipo, int id_usuario)
        {
            try
            {
                DE_MON_Dec_Viabilidad objProy = new DE_MON_Dec_Viabilidad();
                return objProy.spud_MON_Aprobacion(id_aprobacion,id_tipoDocumentoAprobacion,detalleDocumento,fecha,urlDoc,tipo,id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
