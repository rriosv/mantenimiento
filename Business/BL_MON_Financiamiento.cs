﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Financiamiento
    {
        public DataTable F_spMON_FinanciamientoDirecta(int id_proyecto)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                dstFinanciamiento = objProy.F_spMON_FinanciamientoDirecta(id_proyecto);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int F__spu_MON_FinanciamientoDirecta(int id_proyecto, decimal costoEtapa, decimal costoSupervision, string urlCostoEtapa, string urlCostoSupervision, string urlElaboracionEtapa, string urlElaboracionSupervision, string urlPresupuesto, int id_usuario)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spu_MON_FinanciamientoDirecta(id_proyecto,costoEtapa,costoSupervision,urlCostoEtapa,urlCostoSupervision,urlElaboracionEtapa,urlElaboracionSupervision,urlPresupuesto,id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int F_spu_MON_SistemaInversion(int id_proyecto, int iTipoSistema, int id_usuario)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spu_MON_SistemaInversion(id_proyecto,iTipoSistema,id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int F_spu_MON_PMIServicio(int id_proyecto, int bInfraestructuraAguaPotable, int bInfraestructuraAlcantarillado, int bInfraestructuraAguaResidual, int id_usuario)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spu_MON_PMIServicio(id_proyecto,bInfraestructuraAguaPotable,bInfraestructuraAlcantarillado,bInfraestructuraAguaResidual,id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Financiamiento> F_spMON_FinanContrapartida(BE_MON_Financiamiento _BEFinanciamiento)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);

            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_spMON_FinanContrapartida(_BEFinanciamiento);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_Financiamiento> F_spMON_TransferenciaFinanciera(BE_MON_Financiamiento _BEFinanciamiento)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);

            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_spMON_TransferenciaFinanciera(_BEFinanciamiento);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable spMON_ProcesoFinanciero(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {

            try
            {
                DataSet dts;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                dts = objProy.spMON_ProcesoFinanciero(_BE_MON_Financiamiento);

                return dts.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_TransferenciaPresupuestal(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {

            try
            {
                DataSet dts;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                dts = objProy.spMON_TransferenciaPresupuestal(_BE_MON_Financiamiento);

                return dts.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public String F_spMON_MontoTotalGrd(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            String objCollection = "0";

            try
            {

                objCollection = objDet.F_spMON_MontoTotalGrd(_BE_MON_Financiamiento);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Financiamiento> F_spMON_FinanTransferencia(BE_MON_Financiamiento _BEFinanciamiento)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);

            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_spMON_FinanTransferencia(_BEFinanciamiento);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_SOSEM> F_spMON_SeguimientoSOSEM(BE_MON_Financiamiento _BE)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_SOSEM> objCollection = default(List<BE_MON_SOSEM>);

            try
            {
                objCollection = new List<BE_MON_SOSEM>();

                objCollection = objDet.F_spMON_SeguimientoSOSEM(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_CONVENIO_DIRECTA> F_spMON_ConvenioDirecta(int id_proyecto)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_CONVENIO_DIRECTA> objCollection = default(List<BE_MON_CONVENIO_DIRECTA>);

            try
            {
                objCollection = new List<BE_MON_CONVENIO_DIRECTA>();

                objCollection = objDet.F_spMON_ConvenioDirecta(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataSet spMON_rParametro(int idParametroGeneral)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.spMON_rParametro(idParametroGeneral);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_SOSEM> F_spMON_SosemEjecutora(int snip)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_SOSEM> objCollection = default(List<BE_MON_SOSEM>);

            try
            {
                objCollection = new List<BE_MON_SOSEM>();

                objCollection = objDet.F_spMON_SosemEjecutora(snip);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_SOSEM> F_spMON_SosemDevengadoMensualizado(int snip, int idEjecutora)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_SOSEM> objCollection = default(List<BE_MON_SOSEM>);

            try
            {
                objCollection = new List<BE_MON_SOSEM>();

                objCollection = objDet.F_spMON_SosemDevengadoMensualizado(snip, idEjecutora);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_SOSEM> F_spMON_SosemFuenteFinanciamiento(int snip, int idEjecutora)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_SOSEM> objCollection = default(List<BE_MON_SOSEM>);

            try
            {
                objCollection = new List<BE_MON_SOSEM>();

                objCollection = objDet.F_spMON_SosemFuenteFinanciamiento(snip, idEjecutora);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }


        public List<BE_MON_BANDEJA> F_spMON_TipoAporte()
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoAporte();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoProcesoFinanciero()
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoProcesoFinanciero();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDocumentoSustento()
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoDocumentoSustento();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoFInanTransferencia()
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoFInanTransferencia();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Financiamiento> F_spSOL_TransferenciaByProyecto(int IdSolicitud)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);

            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_spSOL_TransferenciaByProyecto(IdSolicitud);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Financiamiento> F_spMON_PresupuestoPNVR(int IdProyecto)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);

            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_spMON_PresupuestoPNVR(IdProyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable spMON_GastosSAV_NE(int id_proyecto)
        {

            try
            {
                DataSet dts;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                dts = objProy.spMON_GastosSAV_NE(id_proyecto);

                return dts.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_ConvenioDirecta(BE_MON_CONVENIO_DIRECTA _BE_MON_CONVENIO_DIRECTA)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spi_MON_ConvenioDirecta(_BE_MON_CONVENIO_DIRECTA);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_ConvenioDirecta(BE_MON_CONVENIO_DIRECTA _BE_MON_CONVENIO_DIRECTA, int tipo)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spu_MON_ConvenioDirecta(_BE_MON_CONVENIO_DIRECTA, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }



        public int spi_MON_FinanContrapartida(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spi_MON_FinanContrapartida(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_ProcesoFinanciero(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spi_MON_ProcesoFinanciero(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_ProcesoFinanciero(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spud_MON_ProcesoFinanciero(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_TransferenciaFinanciero(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spu_MON_TransferenciaFinanciero(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spu_MON_TransferenciaPresupuestal(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spu_MON_TransferenciaPresupuestal(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_FinanContrapartida(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spud_MON_FinanContrapartida(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_FinanTransferencia(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spi_MON_FinanTransferencia(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_FinanTransferencia_Editar(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spud_MON_FinanTransferencia_Editar(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_CartaOrden(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {

            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.spMON_CartaOrden(_BE_MON_Financiamiento);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_CartaObra(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spi_MON_CartaObra(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_CartaOrden(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spud_MON_CartaOrden(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_spiu_MON_PresupuestoPNVR(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spiu_MON_PresupuestoPNVR(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_spu_MON_FlagPresupuestoPNVR(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.U_spu_MON_FlagPresupuestoPNVR(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_FinanContrapartidaPE(BE_MON_Financiamiento _BE_MON_Financiamiento)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spi_MON_FinanContrapartidaPE(_BE_MON_Financiamiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spMON_CodigoSeace(int idProyecto, int idEtapa)
        {

            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spMON_CodigoSeace(idProyecto, idEtapa).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet spMON_SEACE_Cronograma(int idCodigoConvocatoria)
        {
            try
            {
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                return objProy.spMON_SEACE_Cronograma(idCodigoConvocatoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_SEACE_Obra(int snip, int idProyecto)
        {

            DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();

            try
            {
                return objDet.spMON_SEACE_Obra(snip, idProyecto).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }



        //public List<BE_MON_SEACE> spMON_SEACE_Consultoria(int snip)
        //{

        //    DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
        //    List<BE_MON_SEACE> objCollection = default(List<BE_MON_SEACE>);

        //    try
        //    {
        //        objCollection = new List<BE_MON_SEACE>();

        //        objCollection = objDet.spMON_SEACE_Consultoria(snip);
        //        return objCollection;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        objCollection = null;
        //    }
        //}


        //public List<BE_MON_SEACE> spMON_SEACE_BienesServicios(int snip)
        //{

        //    DE_MON_Financiamiento objDet = new DE_MON_Financiamiento();
        //    List<BE_MON_SEACE> objCollection = default(List<BE_MON_SEACE>);

        //    try
        //    {
        //        objCollection = new List<BE_MON_SEACE>();

        //        objCollection = objDet.spMON_SEACE_BienesServicios(snip);
        //        return objCollection;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        objCollection = null;
        //    }
        //}


        public DataSet spMON_SEACE_BienesServicio_Detalle(int identificador)
        {

            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.spMON_SEACE_BienesServicio_Detalle(identificador);

                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Obtiene información de estructura de financiamiento y montos transferidos segun el id proporcionado 
        /// (https://files.slack.com/files-pri/T5DJD80GH-F5XBM5A1Y/screencapture-localhost-54085-monitor-registro_obra-aspx-1498064504563.png)
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <returns></returns>
        public DataSet paSSP_MON_rFinanciamiento(int idProyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.paSSP_MON_rFinanciamiento(idProyecto);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Obtiene los tipos de financiamiento para ser usados en el form de nueva etapa
        /// (https://trello-attachments.s3.amazonaws.com/58eba3ab0c937aa30d911773/59419673674176b5f2030b9e/aeb697025dcb59a10a8ef4dceb5c7ded/2017-06-21_18_03_06-Obra.png)
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <returns></returns>
        public DataSet paSSP_MON_rTipoFinanciamientos(int idTipoFinanciamiento)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.paSSP_MON_rTipoFinanciamientos(idTipoFinanciamiento);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        /// <summary>
        /// Invoca a un SP para guardar una etapa de proyecto (obra, exp tecnico, etc)
        /// https://trello.com/c/uqVY2SZM/41-obras
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idTipoFinanciamiento"></param>
        /// <param name="vEtapa"></param>
        /// <param name="idEtapa"></param>
        /// <returns></returns>
        public DataSet paSSP_MON_gPROYECTO(int idProyecto, int idUsuario, int idTipoFinanciamiento, int vEtapa, int idEtapa)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.paSSP_MON_gPROYECTO(idProyecto, idUsuario, idTipoFinanciamiento, vEtapa, idEtapa);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        /// <summary>
        /// Obtiene las metas de un proyecto como tal
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <returns>Dataset</returns>
        public DataSet paSSP_MON_rMetas(int idProyecto,  int etapaRegistro , int tipoMeta)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.paSSP_MON_rMetas(idProyecto, etapaRegistro, tipoMeta);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_VigenciaVencida(int idProyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.spMON_VigenciaVencida(idProyecto);
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataSet spMEF_Financiamiento(int pSnip)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.spMEF_Financiamiento(pSnip);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet spMON_SeguimientoFinancieroPriorizados()
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.spMON_SeguimientoFinancieroPriorizados();
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet paSSP_spiu_MON_VigenciaVencida(int idProyecto, int idUsuario, int flagVigenciaVencida, int idTipoAccion)
        {
            try
            {
                DataSet ds;
                DE_MON_Financiamiento objProy = new DE_MON_Financiamiento();
                ds = objProy.paSSP_spiu_MON_VigenciaVencida(idProyecto, idUsuario, flagVigenciaVencida, idTipoAccion);
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet spd_MON_IncorporacionRegistro(int id_proyecto, int Incorporado, DateTime? FechaIncorporacion, decimal? MontoIncorporado, string ActaResolucionAlcaldia, int UsuarioRegistro,
            int RequiereActualizacionETCOVID19, int ETCuentaPartidaCOVID19, int TieneResolucionAprobacionET)
        {
            try
            { 
                DataSet ds = new DE_MON_Financiamiento().spd_MON_IncorporacionRegistro(id_proyecto, Incorporado, FechaIncorporacion, MontoIncorporado, ActaResolucionAlcaldia, UsuarioRegistro,
                    RequiereActualizacionETCOVID19, ETCuentaPartidaCOVID19, TieneResolucionAprobacionET);
                return ds;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            } 
        }

        public DataSet spd_MON_ObtenerIncorporacionRegistro(int id_proyecto)
        {
            try
            {
                DataSet ds = new DE_MON_Financiamiento().spd_MON_ObtenerIncorporacionRegistro(id_proyecto);
                return ds;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

 
}
