﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Finan_Transparencia
    {
        
         public int spud_MON_FinanTransferencia(BE_MON_Finan_Transparencia _BE_MON_Finan_Transparencia)
         {
             try
             {
                 DE_MON_Finan_Transparencia objProy = new DE_MON_Finan_Transparencia();
                 return objProy.spud_MON_FinanTransferencia(_BE_MON_Finan_Transparencia);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }
    }
}
