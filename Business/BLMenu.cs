using System;
using System.Collections.Generic;
using Entity;
using System.Data;


namespace Business
{
    public class BLMenu
    {
     
     
        public List<BEMenu> F_sp_ListarOpcionesMenu(int id_usuario)
        {

            DEMenu objDet = new DEMenu();
            List<BEMenu> objCollection = default(List<BEMenu>);

            try
            {
                objCollection = new List<BEMenu>();
                objCollection = objDet.F_sp_ListarOpcionesMenu(id_usuario);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataSet sp_ListarMenusxUsuario(int id_usuario)
        {

            DEMenu objDet = new DEMenu();
            DataSet ds = new DataSet();
            try
            {
                ds = new DataSet();
                ds = objDet.sp_ListarMenusxUsuario(id_usuario);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds = null;
            }
        }

        public DataSet sp_ListarMenusxUsuarioNew(int id_usuario)
        {

            DEMenu objDet = new DEMenu();
            DataSet ds = new DataSet();
            try
            {
                ds = new DataSet();
                ds = objDet.sp_ListarMenusxUsuarioNew(id_usuario);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds = null;
            }
        }

    }
}
