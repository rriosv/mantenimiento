﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
   public class BL_MON_BANDEJA
    {
         
        #region SELECT

        #endregion

       public DataTable spMON_ReporteMonitoreoOEEE()
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_ReporteMonitoreoOEEE();

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }
       public DataTable spMON_ReporteSedapal()
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_ReporteSedapal();

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

       public DataTable spMON_ReporteNE(BE_MON_BANDEJA _BE_Bandeja)
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_ReporteNE(_BE_Bandeja);

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

       public DataTable spMON_Reporte_PorContrata()
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_Reporte_PorContrata();

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

       public DataTable spMON_ReporteValorFinanciadoNE(BE_MON_BANDEJA _BE_Bandeja)
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_ReporteValorFinanciadoNE(_BE_Bandeja);

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

       public DataTable spMON_ReporteSeguimientoNE(BE_MON_BANDEJA _BE_Bandeja)
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_ReporteSeguimientoNE(_BE_Bandeja);

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

       public DataTable spMON_ReporteDetalleDevengadoNE(BE_MON_BANDEJA _BE_Bandeja)
       {
           try
           {
               DataSet dstFinanciamiento;
               DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
               dstFinanciamiento = objProy.spMON_ReporteDetalleDevengadoNE(_BE_Bandeja);

               return dstFinanciamiento.Tables[0];

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

        public DataTable spMON_BandejaCoordinador(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_BandejaCoordinador(_BE_Bandeja);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
       
        public List<BE_MON_BANDEJA> F_spMON_ListarUbigeo(string tipo, string depa, string prov, string dist)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();
                objCollection = objDet.F_spMON_ListarUbigeo(tipo,depa,prov,dist);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoPrograma()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoPrograma();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoSubPrograma(int id_tipoPrograma)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoSubPrograma(id_tipoPrograma);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        
        public List<BE_MON_BANDEJA> F_spMON_ListarEstado()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarEstado();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTecnico(string sector)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarTecnico(sector);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTecnicoxTipo(string strSector, string strTipoAgente)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarTecnicoxTipo(strSector, strTipoAgente);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTipoEjecucion()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarTipoEjecucion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spi_MON_AsignarProyecto(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spi_MON_AsignarProyecto(_BE_Bandeja);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoFinanciamiento()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoFinanciamiento();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
       
        public List<BE_MON_BANDEJA> F_spMON_FiltroEstadoEjecucion()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_FiltroEstadoEjecucion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        //public DataTable spMON_ObtenerCorreo(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
        //        ds = objProy.F_spMON_ObtenerCorreo(_BE_Bandeja);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_ObtenerCorreo(BE_MON_BANDEJA _BE)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objENT = default(List<BE_MON_BANDEJA>);

            try
            {
                objENT = new List<BE_MON_BANDEJA>();

                objENT = objDet.F_spMON_ObtenerCorreo(_BE);
                return objENT;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objENT = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_BandejaMonitor(BE_MON_BANDEJA _BE)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_BandejaMonitor(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_BancoMonitoreo()
        {
            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);
            try
            {
                objCollection = new List<BE_MON_BANDEJA>();
                objCollection = objDet.F_spMON_BancoMonitoreo();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        //public DataTable spMON_BandejaConcluidos(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
        //        ds = objProy.spMON_BandejaConcluidos(_BE_Bandeja);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_BandejaConcluidos(BE_MON_BANDEJA _BE)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_BandejaConcluidos(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        //public DataTable spMON_Reporte_BandejaConcluidos(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
        //        ds = objProy.spMON_Reporte_BandejaConcluidos(_BE_Bandeja);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_Reporte_BandejaConcluidos(BE_MON_BANDEJA _BE)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_Reporte_BandejaConcluidos(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ReporteObrasDesactualizadas(BE_MON_BANDEJA _BE)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ReporteObrasDesactualizadas(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoAgente(int tipoAgente)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoAgente(tipoAgente);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public DataTable spProc_Rep_ShockInversion(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spProc_Rep_ShockInversion(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteDetalladoPersonalizable(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteDetalladoPersonalizable(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spMON_ObtenerIDProyectosFinalizado(string snip, string tipoFinanciamiento)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ObtenerIDProyectosFinalizado(snip,tipoFinanciamiento);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spMON_ObtieneInfo(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
        //        ds = objProy.spMON_ObtieneInfo(_BE_Bandeja);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public BE_MON_BANDEJA F_spMON_ObtieneInfo(BE_MON_BANDEJA _BEBandeja)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            BE_MON_BANDEJA objCollection = default(BE_MON_BANDEJA);

            try
            {
                objCollection = objDet.F_spMON_ObtieneInfo(_BEBandeja);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public DataTable spMON_ReporteGeneralPMIB(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteGeneralPMIB(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteDetallePMIB(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteDetallePMIB(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteParalizados(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteParalizados(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteMetasPMIB(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteMetasPMIB(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_ReporteSuperGeneralPNSU(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteSuperGeneralPNSU(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_ReporteMonitoreoPNSUparaPNSR()
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteMonitoreoPNSUparaPNSR();

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_ReporteGeneral(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_ReporteGeneral(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_RegistroPreliminar(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spSOL_RegistroPreliminar(_BE_Bandeja);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spuSOL_PreliminarSNIPSolicitudes(BE_MON_BANDEJA _BE_Ejecucion)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spuSOL_PreliminarSNIPSolicitudes(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spMON_DocumentoMonitor(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
        //        ds = objProy.spMON_DocumentoMonitor(_BE_Bandeja);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_DocumentoMonitor(BE_MON_BANDEJA _BEBandeja)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_DocumentoMonitor(_BEBandeja);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        //public DataTable spMON_FotoDocumentoMonitor(BE_MON_BANDEJA _BE_Bandeja)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
        //        ds = objProy.F_spMON_FotoDocumentoMonitor(_BE_Bandeja);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_FotoDocumentoMonitor(BE_MON_BANDEJA _BEBandeja)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_FotoDocumentoMonitor(_BEBandeja);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public int spi_MON_DocumentoMonitor(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spi_MON_DocumentoMonitor(_BE_Bandeja);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDocumentoMonitor()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoDocumentoMonitor();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spud_MON_DocumentoMonitor(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spud_MON_DocumentoMonitor(_BE_Bandeja);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_Revisor(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spMON_Revisor(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_InauguracionProyecto(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_InauguracionProyecto(_BE_Bandeja);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_InauguracionCalendario(string fecha)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_InauguracionCalendario(fecha);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_InauguracionAll()
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_InauguracionAll();

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoInaugura()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoInaugura();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }


        public List<BE_MON_BANDEJA> F_spSOL_MotivoDevolucion()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spSOL_MotivoDevolucion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spSOL_TipoDevolucion()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spSOL_TipoDevolucion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoTipologia()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoTipologia();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTambos()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarTambos();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDenominacionFuente()
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoDenominacionFuente();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public int spi_MON_InauguracionProyecto(int id_proyecto, int id_tipoInaugura, DateTime fechaInagura, string HoraInicio, string HoraFin, string representanteMVCS, string id_usuario)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spi_MON_InauguracionProyecto(id_proyecto, id_tipoInaugura, fechaInagura, HoraInicio, HoraFin, representanteMVCS, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_BandejaEntregaExpedientes(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spSOL_BandejaEntregaExpedientes(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_BandejaCambiarEstado(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet ds;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                ds = objProy.spSOL_BandejaCambiarEstado(_BE_Bandeja);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public List<BE_MON_BANDEJA> F_spSOL_BandejaDevolucion(BE_MON_BANDEJA _BE)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spSOL_BandejaDevolucion(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spiuSOL_EntregaExpedientes(int id_solicitudes, DateTime fechaEntrega, string observacion, int id_usuario)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spiuSOL_EntregaExpedientes(id_solicitudes, fechaEntrega, observacion, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spiuSOL_CambioEstado(int id_solicitudes, string id_usuario, int estado)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spiuSOL_CambioEstado(id_solicitudes, id_usuario,estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TecnicoSkype(int id_proyecto)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TecnicoSkype(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_UbigeosMetas(int id_proyecto)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_UbigeosMetas(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_Ubigeo(int id_proyecto)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_Ubigeo(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TamboProyecto(int id_proyecto)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TamboProyecto(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_CorreobyIdProyecto(int id_proyecto)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_CorreobyIdProyecto(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spiuSOL_DevolucionSolicitud(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spiuSOL_DevolucionSolicitud(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_spiu_MON_ProyectoUbigeoCCPP(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.IU_spiu_MON_ProyectoUbigeoCCPP(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int U_MON_ProyectoBanco(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.U_MON_ProyectoBanco(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int I_spi_MON_UbigeoMetas(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.I_spi_MON_UbigeoMetas(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int UD_spud_MON_UbigeoMetas(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.UD_spud_MON_UbigeoMetas(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int I_spi_MON_RegistrarProyecto(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.I_spi_MON_RegistrarProyecto(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int IU_spiu_MON_ProyectoTipologia(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.IU_spiu_MON_ProyectoTipologia(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_spu_MON_DatosComplementariosPNSR(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.U_spu_MON_DatosComplementariosPNSR(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        
        public int I_spi_MON_TamboProyecto(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.I_spi_MON_TamboProyecto(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int D_spd_MON_TamboProyecto(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.D_spd_MON_TamboProyecto(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TecnicosInformeNE(int id_proyecto)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TecnicosInformeNE(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_SOL_validarSNIP(string SNIP)
        {

            DE_MON_BANDEJA objDet = new DE_MON_BANDEJA();

            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_SOL_validarSNIP(SNIP);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int U_MON_InformacionGeneralJICA(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.U_MON_InformacionGeneralJICA(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int U_MON_UbigeoMetasTerminadas(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.U_MON_UbigeoMetasTerminadas(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteSaneamiento2016(BE_MON_BANDEJA _BE)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_ReporteSaneamiento2016(_BE);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteSaneamientoObras(BE_MON_BANDEJA _BE)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_ReporteSaneamientoObras(_BE);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ListaProyectosDetalle(BE_MON_BANDEJA _BE_Bandeja)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_ListaProyectosDetalle(_BE_Bandeja);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int U_MON_AdminitradorReporte(BE_MON_BANDEJA _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.U_MON_AdminitradorReporte(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiu_MON_RegistrarCodigosSeace(CodigosSeace _BE)
        {
            try
            {
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                return objProy.spi_MON_RegistrarCodigosSeace(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteDemandaSG()
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spSOL_ReporteDemandaSG();

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ReporteIntervencionesSG( string programa)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.spMON_ReporteIntervencionesSG(programa);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet pa_rBandejaMonitoreo(ConsultaMonitoreo pConsulta)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_BANDEJA objProy = new DE_MON_BANDEJA();
                dstFinanciamiento = objProy.pa_rBandejaMonitoreo(pConsulta);

                return dstFinanciamiento;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
