using System;
using System.Data;

namespace Business
{
    public class BLLogin
    {

        #region consulta

        public DataTable sp_validarUsuario(string strLogin, string strPassword)
        {
            try
            {
                DataSet dstUsuario;
                DELogin objlogin = new DELogin();
                dstUsuario = objlogin.sp_validarUsuario(strLogin, strPassword);

                return dstUsuario.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_validarUsuarioLogin(string strLogin)
        {
            try
            {
                DataSet dstUsuario;
                DELogin objlogin = new DELogin();
                dstUsuario = objlogin.sp_validarUsuarioLogin(strLogin);

                return dstUsuario.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable sp_validarUsuarioDNI(string strDNI)
        {
            try
            {
                DataSet dstUsuario;
                DELogin objlogin = new DELogin();
                dstUsuario = objlogin.sp_validarUsuarioDNI(strDNI);

                return dstUsuario.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable paSSP_rObtenerSession(string clave)
        {
            try
            {
                DataSet dstUsuario;
                DELogin objlogin = new DELogin();
                dstUsuario = objlogin.paSSP_rObtenerSession(clave);

                return dstUsuario.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int I_iSession(string clave, int id_usuario)
        {
            try
            {
                DELogin objProy = new DELogin();
                return objProy.I_iSession(clave, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion
    }
}