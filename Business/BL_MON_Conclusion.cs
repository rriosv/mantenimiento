﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;


namespace Business
{
    public class BL_MON_Conclusion
    {

     
         public int spi_MON_Conclusion(BE_MON_Conclusion _BE_Conclusion)
       {
           try
           {
               DE_MON_Conclusion objProy = new DE_MON_Conclusion();
               return objProy.spi_MON_Conclusion(_BE_Conclusion);
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }

         public DataTable spMON_Conclusion(BE_MON_Conclusion _BE_Conclusion)
           {
               try
               {
                   DataSet dstFinanciamiento;
                   DE_MON_Conclusion objProy = new DE_MON_Conclusion();
                   dstFinanciamiento = objProy.spMON_Conclusion(_BE_Conclusion);

                   return dstFinanciamiento.Tables[0];

               }
               catch (Exception ex)
               {
                   throw ex;
               }
               finally
               {
               }
           }
    }
}
