﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using Entity;

namespace Business
{
   public class BL_MON_Ejecucion
    {
        public DataTable spMON_EstadoEjecuccion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_EstadoEjecuccion(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_DetalleEstadoEjecuccionPE35(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_DetalleEstadoEjecuccionPE35(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_EjecucionSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_EjecucionSupervision(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_Plazos(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_Plazos(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_Plazos(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_Plazos(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spi_MON_EstadoEjecuccion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EstadoEjecuccion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EstadoEjecuccionTransferencia(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EstadoEjecuccionTransferencia(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EstadoEjecuccionPE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EstadoEjecuccionPE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EstadoEjecuccionPE32(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EstadoEjecuccionPE32(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EstadoEjecuccionPE35(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EstadoEjecuccionPE35(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spi_DetalleEstadoEjecuccionPE35(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_DetalleEstadoEjecuccionPE35(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_EstadoProyecto(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spu_MON_EstadoProyecto(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_EstadoEjecucion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spu_MON_EstadoEjecucion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoEjecucion(int tipoProyecto, int tipoModalidadFinanciamiento)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoEstadoEjecucion(tipoProyecto, tipoModalidadFinanciamiento);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoResultado()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoResultado();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoAsignacionAgente()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoEstadoAsignacionAgente();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoSubEstadoEjecucion(int estado)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoSubEstadoEjecucion(estado);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoSubEstadoParalizado(int pTipoEstadoParalizado)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoSubEstadoParalizado(pTipoEstadoParalizado);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoSubEstado2(int pTipoSubEstado)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoSubEstado2(pTipoSubEstado);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoValorizacion()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoValorizacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable spMON_AvanceFisico(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_AvanceFisico(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_AvanceFisicoAdicional(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_AvanceFisicoAdicional(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_AvanceFisico(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_AvanceFisico(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_EmpleadoObra(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_EmpleadoObra(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_EmpleadoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_EmpleadoParalizacion(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_EmpleadoObraSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_EmpleadoObraSupervision(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_EmpleadoObraPre(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_EmpleadoObraPre(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_EmpleadoObraExp(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_EmpleadoObraExp(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EmpleadoObraExp(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EmpleadoObraExp(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spi_MON_EmpleadoObraPre(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EmpleadoObraPre(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spi_MON_EmpleadoObra(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EmpleadoObra(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EmpleadoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EmpleadoParalizacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EmpleadoObraSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EmpleadoObraSupervision(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_EmpleadoObra(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_EmpleadoObra(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_EmpleadoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_EmpleadoParalizacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_CartaFianza(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_CartaFianza(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_CartaFianzaSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_CartaFianzaSupervision(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoCartaFianza()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoCartaFianza();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoAccionCarta()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoAccionCarta();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spi_MON_CartaFianza(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_CartaFianza(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_CartaFianzaSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_CartaFianzaSupervision(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_CartaFianza(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spu_MON_CartaFianza(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_CartaFianza_FlagVerifica(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spu_MON_CartaFianza_FlagVerifica(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoSituacional()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoEstadoSituacional();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable spMON_ObtenerVariablesPNSU(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_ObtenerVariablesPNSU(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_ComponentesPNSU(BE_MON_VarPNSU _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_ComponentesPNSU(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ComponentesPNSU(BE_MON_VarPNSU _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_ComponentesPNSU(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_AvanceFisico_Eliminar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_AvanceFisico_Eliminar(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_AvanceFisico_Editar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_AvanceFisico_Editar(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Carta_Fianza_Eliminar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_Seguimiento_Carta_Fianza_Eliminar(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Carta_Fianza_Editar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_Seguimiento_Carta_Fianza_Editar(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public int spud_MON_AvanceFisico_Estudio_Eliminar(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    try
        //    {
        //        DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
        //        return objProy.spud_MON_AvanceFisico_Estudio_Eliminar(_BE_Ejecucion);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}
        public int spud_MON_AvanceFisico_Estudio_Editar(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_AvanceFisico_Estudio_Editar(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_Supervisor(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_Supervisor(_BE_Ejecucion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_Supervisor(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_Supervisor(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoCertificacion()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoCertificacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spi_MON_CertificacionAutorizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_CertificacionAutorizacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_CertificacionAutorizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_CertificacionAutorizacion(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_EvaluacionRecomendacion(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EvaluacionRecomendacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_EvaluacionRecomendacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_AvanceFisicoByFlagAyudaMemoria(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spu_MON_AvanceFisicoByFlagAyudaMemoria(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_EvaluacionRecomendacionByFlag(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spu_MON_EvaluacionRecomendacionByFlag(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTipoMonitoreo()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarTipoMonitoreo();
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarSubTipoMonitoreo(int iTipoMonitoreo)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarSubTipoMonitoreo(iTipoMonitoreo);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListarTipoDocumentoEvaluacionRecomendacion()
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListarTipoDocumentoEvaluacionRecomendacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_EvaluacionRecomendacion(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_EvaluacionRecomendacion(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_MON_DetalleSituacional(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_DetalleSituacional(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }


        public int I_spi_MON_EstadoEjecucionNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_EstadoEjecucionNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_spiu_MON_EstadoSituacionalNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.IU_spiu_MON_EstadoSituacionalNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_AsignarAgente(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_AsignarAgente(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_Agentes(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_Agentes(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_MON_PersonalNE(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_PersonalNE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }


        public int UD_spud_MON_AsignarAgente(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_spud_MON_AsignarAgente(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_ProgramacionInforme(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_ProgramacionInforme(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public BE_MON_Ejecucion F_MON_MaxValoresProgramacion(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            BE_MON_Ejecucion objCollection = default(BE_MON_Ejecucion);

            try
            {
                objCollection = new BE_MON_Ejecucion();

                objCollection = objDet.F_MON_MaxValoresProgramacion(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_MesesValorizacionSupervision(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_MesesValorizacionSupervision(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_spi_MON_ProgramacionInforme(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_ProgramacionInforme(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int D_spd_MON_CronogramaProgramacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.D_spd_MON_CronogramaProgramacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int I_spi_MON_GeneracionProgramacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_GeneracionProgramacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int I_spi_MON_CulminacionEjecucionNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_CulminacionEjecucionNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int UD_spud_MON_ProgramacionInforme(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_spud_MON_ProgramacionInforme(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_spud_MON_ProgramacionInformePorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_spud_MON_ProgramacionInformePorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvanceNE(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_InformeAvanceNE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvancePorContrata(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_InformeAvancePorContrata(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_InformeAvanceAdicionalPorContrata(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_InformeAvanceSupervisionPorContrata(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_InformeAvanceSupervisionPorContrata(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_InformeAvanceNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_InformeAvanceNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_spu_MON_InformeAvancePorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_spu_MON_InformeAvancePorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_InformeAvanceAdicionalPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_spi_MON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_spi_MON_InformeAvanceAdicionalPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int D_spd_MON_InformeAvanceAdicionalPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.D_spd_MON_InformeAvanceAdicionalPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_InformeAvanceSupervisionPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_InformeAvanceSupervisionPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public int UD_spud_MON_InformeAvanceSupervisionPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_spud_MON_InformeAvanceSupervisionPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int D_InformeAvancePNSR(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.D_InformeAvancePNSR(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_AvanceFinancieroNE(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_AvanceFinancieroNE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_FinancieroTotalNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_FinancieroTotalNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_FinancieroMensualNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_FinancieroMensualNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_GastosNE(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_GastosNE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_spi_MON_GastosNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_GastosNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_spud_MON_GastosNE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_spud_MON_GastosNE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_EstadoEjecuccionPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_EstadoEjecuccionPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiu_MON_EjecucionSupervisionPorContrata(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spiu_MON_EjecucionSupervisionPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_spMON_ProfesionalTecnicoSupervision(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_spMON_ProfesionalTecnicoSupervision(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_spi_MON_ProfesionalTecnicoSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_spi_MON_ProfesionalTecnicoSupervision(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_spud_MON_ProfesionalTecnicoSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_spud_MON_ProfesionalTecnicoSupervision(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_AvanceFisicoPE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_AvanceFisicoPE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_MON_AvanceFisicoPE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_MON_AvanceFisicoPE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_MON_AvanceFisicoPE(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_AvanceFisicoPE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_MON_AvanceFisicoAdicionalPE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_AvanceFisicoAdicionalPE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_AvanceFisicoSupervisionPE(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_AvanceFisicoSupervisionPE(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_MON_AvanceFisicoSupervisionPE(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_AvanceFisicoSupervisionPE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_Ejecucion> F_MON_InformeSupervision(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_InformeSupervision(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_MON_InformeSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_InformeSupervision(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_MON_InformeSupervision(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_MON_InformeSupervision(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public IEnumerable<BE_MON_Ejecucion> ValidarTecnicoPersonalNE(List<BE_MON_Ejecucion> PersonalNE, int id_usuario)
        {
            return new DE_MON_Ejecucion().ValidarTecnicoPersonalNE(PersonalNE, id_usuario);
        }
        public IEnumerable<DataTable> CargaSiaf(List<BE_MON_SIAF_PNSR> PersonalNE, int id_usuario, string pAnio, string pMes)
        {
            return new DE_MON_Ejecucion().CargaSiaf22(PersonalNE, id_usuario,pAnio,pMes);
        }

        //public DataTable spMON_MetasPNSR(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
        //        ds = objProy.spMON_MetasPNSR(_BE_Ejecucion);

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        //public int U_MON_MetasPNSR(BE_MON_Ejecucion _BE_Ejecucion)
        //{
        //    try
        //    {
        //        DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
        //        return objProy.U_MON_MetasPNSR(_BE_Ejecucion);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_MON_TipoSistemaDrenaje(int idSector)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_MON_TipoSistemaDrenaje(idSector);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Ejecucion> F_MON_SistemaDrenajeDetalle(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_SistemaDrenajeDetalle(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_MON_SistemaDrenajeDetalle(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_SistemaDrenajeDetalle(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_MON_EvaluacionRecomendacionActaVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_MON_EvaluacionRecomendacionActaVisita(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Ejecucion> F_MON_EvaluacionRecomendacionActaVisita(BE_MON_Ejecucion _BE)
        {

            DE_MON_Ejecucion objDet = new DE_MON_Ejecucion();
            List<BE_MON_Ejecucion> objCollection = default(List<BE_MON_Ejecucion>);

            try
            {
                objCollection = new List<BE_MON_Ejecucion>();

                objCollection = objDet.F_MON_EvaluacionRecomendacionActaVisita(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int U_MON_EvaluacionRecomendacionInformeVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_MON_EvaluacionRecomendacionInformeVisita(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_EvaluacionRecomendacionActaVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_EvaluacionRecomendacionActaVisita(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_MON_SistemaDrenajeDetalle(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_MON_SistemaDrenajeDetalle(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_MON_RevisionItemAdministrativoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.IU_MON_RevisionItemAdministrativoVisita(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_RevisionItemAdministrativoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_RevisionItemAdministrativoVisita(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_DetalleFotoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_DetalleFotoVisita(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_DetalleFotoVisitaMovil(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_DetalleFotoVisitaMovil(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable paSSP_MON_rFotoCuaderoObra(Int32 pSNIP)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.paSSP_MON_rFotoCuaderoObra(pSNIP);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_DetalleFotoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_DetalleFotoVisita(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_MON_DetalleFotoVisita(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_MON_DetalleFotoVisita(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_VisitaFotoMovil(int pIdEvaluacion, string pJsonFotos, int pIdUsuario)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_VisitaFotoMovil(pIdEvaluacion, pJsonFotos, pIdUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_MON_EvaluacionRecomendacionParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_MON_EvaluacionRecomendacionParalizacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_MON_SeguimientoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_MON_SeguimientoParalizacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_SeguimientoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_SeguimientoParalizacion(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_SeguimientoParalizacion(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_SeguimientoParalizacion(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet spMON_EvaluacionRecomendacionAccionesSeleccionar(int Id_ejecucionRecomendacion)
        {
            try
            {
                 DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spMON_EvaluacionRecomendacionAccionesSeleccionar(Id_ejecucionRecomendacion);
             }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet U_MON_GuardarRecomendacionAcciones(DataTable DtTableEvaluacionRecomendacionAcciones) {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.U_MON_GuardarRecomendacionAcciones(DtTableEvaluacionRecomendacionAcciones);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int paSSP_MON_EvaluacionRecomendacionViviendas(int id_evaluacionRecomendacion, int? Concluidas, int? EnEjecucion,
            int? PorIniciar, int? Paralizadas)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.paSSP_MON_EvaluacionRecomendacionViviendas(id_evaluacionRecomendacion, Concluidas, EnEjecucion, PorIniciar, Paralizadas);
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        public DataSet spMON_CombosSeguimientoPCM()
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spMON_CombosSeguimientoPCM();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_SeguimientoPCM(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_SeguimientoPCM(_BE_Ejecucion);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_SeguimientoPCM(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.I_MON_SeguimientoPCM(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_MON_SeguimientoPCM(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.UD_MON_SeguimientoPCM(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_MON_CompromisoGEI(int _idProyecto, string _flagEnergia, float _undEnergia, string _flagNuevoPtar, float _undNuevoPtarCarga, string _flagDigestores, float _undDigestoresCapacidad, int _idUsuario)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.IU_MON_CompromisoGEI(_idProyecto, _flagEnergia, _undEnergia, _flagNuevoPtar, _undNuevoPtarCarga, _flagDigestores, _undDigestoresCapacidad, _idUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_CompromisoGEI(int _idProyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                ds = objProy.spMON_CompromisoGEI(_idProyecto);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_Adelanto(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                dstFinanciamiento = objProy.spMON_Adelanto(_BE_Ejecucion);
                return dstFinanciamiento.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }



        public DataTable spi_MON_Adelanto(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spi_MON_Adelanto(_BE_Ejecucion).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spud_MON_Adelanto(BE_MON_Ejecucion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ejecucion objProy = new DE_MON_Ejecucion();
                return objProy.spud_MON_Adelanto(_BE_Ejecucion).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
