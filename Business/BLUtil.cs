using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.IO;
using DataAccess;
using Entity;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic.FileIO;
using System.DirectoryServices;
using System.Net.Mail;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections;
using System.Net.NetworkInformation;
using System.Web.UI.HtmlControls;
using System.Web;

namespace Business
{
    public class BLUtil
    {
       
        #region consulta
     
        public bool GetFlagAmbitoFonie(string ubigeo)
        {
            try
            {
                bool dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_GetFlagAmbitoFonie(ubigeo);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
                //return false;
            }
            finally
            {
            }
        }

        public List<BECentroPoblado> GetCentroPoblado(string ubigeo, int idSolicitud)
        {
            List<BECentroPoblado> dstresult = new List<BECentroPoblado>();
            try
            {

                DEUtil objlog = new DEUtil();

                dstresult = objlog.sp_GetCentroPoblado(ubigeo, idSolicitud);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
                //return new List<BECentroPoblado>();
            }
            finally
            {

            }
        }

        public bool VerificaUbigeopnsr(string ubigeo)
        {
            try
            {
                bool dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_verifica_Ubigeo_pnsr(ubigeo);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
                //return false;
            }
            finally
            {
            }
        }
        public int obtenerCodigo(string strCodigo, int intIdTabla, int intCod_Programa, int intCelda)
        {
            DEUtil de = new DEUtil();
            return de.sp_obtenerCodigoItem(strCodigo, intIdTabla, intCod_Programa, intCelda);
        }

        public DataTable lista_departamentos(int tipo)
        {
            try
            {
                DataSet dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_listarDepartamento(tipo);

                return dstresult.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //Problemas
        public DataTable listado_departamento()
        {
            try
            {
                DEUtil objlog = new DEUtil();
                return objlog.sp_getDepartamento();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable listar_sector()
        {
            try
            {
                DEUtil objlog = new DEUtil();
                return objlog.sp_getSector();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable listar_Estados()
        {
            try
            {
                DEUtil objlog = new DEUtil();
                return objlog.sp_getEstado();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable listar_NivelSolucion()
        {
            try
            {
                DEUtil objlog = new DEUtil();
                return objlog.sp_getNivelSol();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BEProyecto> F_spSOL_obtieneID_estado_snip(string estado_snip)
        {

            DEUtil objDet = new DEUtil();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_obtieneID_estado_snip(estado_snip);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BEProyecto> F_spSOL_Listar_Estado_Snip()
        {

            DEUtil objDet = new DEUtil();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_Listar_Estado_Snip();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable listarCombos(string id_tabla, string param)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.spN_ListarCombos(id_tabla, param);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BEProyecto> F_spSOL_Listar_Estrategia()
        {

            DEUtil objDet = new DEUtil();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_Listar_Estrategia();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        //EMERGENCIA
        public DataTable listarCombosODN(string id_tabla, string param)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.spN_ListarCombosODN(id_tabla, param);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        //SOLICITUD
        public DataTable listarCombosSOL(string id_tabla, string param)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_ListarCombosSOL(id_tabla, param);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable listarCombosSOL_SSP(string id_tabla, string param)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_ListarCombosSOL_SSP(id_tabla, param);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        //ANUNCIOS
        public DataTable listarCombosANUN(string id_tabla, string param)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_ListarCombosANUN(id_tabla, param);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //DS TRANSFERENCIAS
        public DataTable listarCombosDS(string id_tabla, string param, string param1)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.sp_ListarCombosDS(id_tabla, param, param1);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BEProyecto> F_spSOL_Listar_Departamento(int tipo, string cod_depa, string cod_prov, string cod_dist)
        {

            DEUtil objDet = new DEUtil();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_Listar_Departamento(tipo,cod_depa,cod_prov,cod_dist);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable spSOL_Informacion(Int32 tipo, Int64 codigo, string variable, int cod_estado)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.spSOL_Informacion(tipo, codigo, variable, cod_estado);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BEProyecto> F_spSOL_ListaTecnicoRevision(string codigo)
        {

            DEUtil objDet = new DEUtil();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_ListaTecnicoRevision(codigo);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        //TAMBOS

        public DataTable spFOES_Estados(Int32 tipo)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.spFOES_Estados(tipo);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        
        public DataTable spSOL_obtieneID_departamento(int tipo, string cod_depa, string cod_prov, string cod_dist)
        {
            try
            {
                DataTable dstresult;
                DEUtil objlog = new DEUtil();
                dstresult = objlog.spSOL_obtieneID_departamento(tipo, cod_depa, cod_prov, cod_dist);

                return dstresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion

        int LOGON32_LOGON_INTERACTIVE = 2;
        int LOGON32_PROVIDER_DEFAULT = 0;
        WindowsImpersonationContext impersonationContext;


        [DllImport("advapi32.dll")]
        public static extern int LogonUserA(String lpszUserName,
            String lpszDomain,
            String lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken);
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken,
            int impersonationLevel,
            ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);

        //DeclareAutoDeclareAutoDeclareAuto

        private bool impersonateValidUser(String userName, String domain, String password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            if (RevertToSelf())
            {
                if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                {
                    if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                    {
                        tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                        impersonationContext = tempWindowsIdentity.Impersonate();
                        if (impersonationContext != null)
                        {
                            CloseHandle(token);
                            CloseHandle(tokenDuplicate);
                            return true;
                        }
                    }
                }
            }
            if (token != IntPtr.Zero)
                CloseHandle(token);
            if (tokenDuplicate != IntPtr.Zero)
                CloseHandle(tokenDuplicate);
            return false;
        }

        private void undoImpersonation()
        {
            impersonationContext.Undo();
        }

        public string uploadfile(FileUpload upfile)
        {
            string arch="";
            string user = System.Configuration.ConfigurationManager.AppSettings["Usuario_ActiveDirectory"].ToString();
            string clave = System.Configuration.ConfigurationManager.AppSettings["Clave_ActiveDirectory"].ToString();
            string servidor = System.Configuration.ConfigurationManager.AppSettings["Servidor_ActiveDirectory"].ToString();

            if (impersonateValidUser(user, servidor, clave))
            {
                if (upfile.HasFile)
                {
                    arch = Path.GetFileName(upfile.FileName);
                    arch = DateTime.Now.ToString("yyyyMMdd") + "_" + Path.GetFileName(upfile.FileName);
                    arch = arch.Replace(",", "");
                    arch = arch.Replace(" ", "_");
                    arch = arch.Replace("'", "");
                    string pat = "";

                    pat = ConfigurationManager.AppSettings["DocumentosMonitor"];
                    if (FileSystem.FileExists(pat + arch))
                    {
                        int indice = 1;
                        string ext = Path.GetExtension(arch);
                        string nombre = Path.GetFileNameWithoutExtension(arch);
                        while (true)
                        {
                            arch = nombre + indice.ToString() + ext;
                            if (FileSystem.FileExists(pat + arch))
                                indice = indice + 1;
                            else
                                break;
                        }
                    }
                    upfile.SaveAs(pat + arch);
                }

                undoImpersonation();
            }
            return arch;
        }

        
        public bool ValidaFecha(string inValue)
        {
            bool bValid;
            try
            {
                if (inValue.Length != 10)
                {
                    bValid = false;
                }
                else
                {
                    DateTime myDT = Convert.ToDateTime(inValue);
                    bValid = true;

                    if (myDT.Year < 1990 || myDT.Year > 2050)
                    {
                        bValid = false;
                    }
                }
            }
            catch (Exception e)
            {
                bValid = false;
            }


            return bValid;
        }

        public DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\'':
                        sb.Append("\\\'");
                        break;
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");

            return sb.ToString();
        }


        public bool F_LoginUsuarioAD(string pUsuario, string pClave, string pIdOficina, string ServerStart)
        {
            string _pathVIVIENDA = ConfigurationManager.ConnectionStrings["ADPathVIVIENDA"].ConnectionString;
            string _domainVIVIENDA = ConfigurationManager.ConnectionStrings["ADDominioVIVIENDA"].ConnectionString;

            string domainAndUsernameVIVIENDA = _domainVIVIENDA + "\\" + pUsuario;
            try
            {
                DirectoryEntry entry = new DirectoryEntry(_pathVIVIENDA, domainAndUsernameVIVIENDA, pClave);
                dynamic obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + pUsuario + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();
                //if (result == null) { return false;}
                _pathVIVIENDA = result.Path;
                return true;
            }
            catch (Exception ex)
            {
                if (!(ex.Message.ToString().Equals("The user name or password is incorrect.\r\n") || ex.Message.ToString().Equals("El nombre de usuario o la contrase�a no son correctos.\r\n")))
                {
                    //Mandar mail de alerta error webservice PNSU
                    string cuerpo = "";
                    cuerpo = "<b>Error : </b>" + ex.Message.ToString() + "<br><b>Servidor : </b>" + ServerStart + "<br><b>Dominio AD: </b>" + _pathVIVIENDA + "<br> <b>Usuario :</b>" + domainAndUsernameVIVIENDA;
                    cuerpo = cuerpo + "<br><b>Conexi�n : </b>" + DateTime.Now.ToString();
                    MailDeError(cuerpo, "Error al conectarse al Active Directory - VIVIENDA " + DateTime.Now.ToString("dd/MM/yyyy"), true);
                }
                return false;
            }
        }

        public static DataTable ordenarDt(DataTable pDataTable, string pColName, string pDirection)
        {
            if (pDataTable != null && pDataTable.Columns.Contains(pColName) && pDataTable.Rows.Count > 0
               && (pDirection.ToUpper().Equals("ASC") || pDirection.ToUpper().Equals("DESC")))
            {
                pDataTable.DefaultView.Sort = pColName + " " + pDirection;
                pDataTable = pDataTable.DefaultView.ToTable();
            }
            return pDataTable;
        }

        #region PIDE
        // CLASE PARA PIIDE

        public string PrepareJsonParams(object obj)
        {
            string pJson = JsonConvert.SerializeObject(obj);
            pJson = EncodeDecode.Base64Encode(pJson);
            pJson = Regex.Replace(pJson, @"\t|\n|\r", "");
            //string pJson = JsonConvert.SerializeObject(obj);
            //pJson = EncodeDecode.Base64Encode(pJson);
            return pJson;
        }
        private static string jsonError(string xidError, string msjError)
        {
            return "[{\"data\":{ \"error\":{\"xidError\": " + xidError + ",\"xidTipoError\":\"ER\", \"msjError\":\"" + msjError + "\"},\"length\":0, \"data\":{}}}]";
        }

        public string invokePost(string url, string parameters)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            List<string> lKeyVal = new List<string>();
            StringBuilder sbPostData = new StringBuilder();
            lKeyVal.Add("=" + System.Net.WebUtility.HtmlEncode(parameters));
            sbPostData.Append(String.Join("&", lKeyVal));
            string sPostData = sbPostData.ToString();
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(sPostData);
            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;
            try
            {
                // Write data  
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }
            }
            catch (Exception e)
            {
                //return "";
                return jsonError("503", e.Message);
            }

            Response response_last;
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    response_last = new Response(response);
                    // Get the response stream  
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string responseString = reader.ReadToEnd();
                    return responseString;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response == null)
                {
                    response_last = new Response(null);
                    return jsonError("500", wex.Message);
                    //return "";
                }
                using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                {
                    response_last = new Response(errorResponse);
                    StreamReader reader = new StreamReader(errorResponse.GetResponseStream(), Encoding.UTF8);
                    //return "";
                    return jsonError("500", reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                //return "";
                return jsonError("503", e.Message);
            }
        }

        public static string getMacAddress()
        {
            // Contador para un ciclo
            int i = 0;
            // Colecci�n de direcciones MAC
            ArrayList DireccionesMAC = new ArrayList();
            // Informaci�n de las tarjetas de red
            NetworkInterface[] interfaces = null;
            // Obtener todas las interfaces de red de la PC
            interfaces = NetworkInterface.GetAllNetworkInterfaces();
            // Validar la cantidad de tarjetas de red que tiene
            if (interfaces != null && interfaces.Length > 0)
            {
                // Recorrer todas las interfaces de red
                foreach (NetworkInterface adaptador in interfaces)
                {
                    // Obtener la direcci�n fisica
                    PhysicalAddress direccion = adaptador.GetPhysicalAddress();
                    // Obtener en modo de arreglo de bytes la direcci�n
                    byte[] bytes = direccion.GetAddressBytes();
                    // Variable que tendra la direcci�n visible
                    string mac_address = string.Empty;
                    // Recorrer todos los bytes de la direccion
                    for (i = 0; i < bytes.Length; i++)
                    {
                        // Pasar el byte a un formato legible para el usuario
                        mac_address += bytes[i].ToString("X2");
                        if (i != bytes.Length - 1)
                        {
                            // Agregar un separador, por formato
                            mac_address += "-";
                        }
                    }
                    // Agregar la direccion MAC a la lista
                    DireccionesMAC.Add(mac_address);
                    return mac_address;
                }
            }
            // Valor de retorno, la lista de direcciones MAC
            //return DireccionesMAC;

            return "";
        }


        public string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public class EncodeDecode
        {
            public static string Base64Encode(string plainText)
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                return System.Convert.ToBase64String(plainTextBytes);
            }

            public static string Base64Decode(string base64EncodedData)
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData != null ? base64EncodedData : "");
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }

            public static bool TryGetFromBase64String(string input)
            {

                try
                {
                    Convert.FromBase64String(input == null ? "" : input);
                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }

            public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
            {
                MemoryStream ms = new MemoryStream();
                Rijndael alg = Rijndael.Create();
                alg.Key = Key;
                alg.IV = IV;

                CryptoStream cs = new CryptoStream(ms,
                   alg.CreateEncryptor(), CryptoStreamMode.Write);

                cs.Write(clearData, 0, clearData.Length);
                cs.Close();
                byte[] encryptedData = ms.ToArray();

                return encryptedData;
            }

            public static string Encrypt(string clearText, string Password)
            {
                byte[] clearBytes =
                  System.Text.Encoding.Unicode.GetBytes(clearText);

                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                byte[] encryptedData = Encrypt(clearBytes,
                         pdb.GetBytes(32), pdb.GetBytes(16));

                return Convert.ToBase64String(encryptedData);

            }

            public static byte[] Encrypt(byte[] clearData, string Password)
            {
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                return Encrypt(clearData, pdb.GetBytes(32), pdb.GetBytes(16));
            }

            public static void Encrypt(string fileIn,
                        string fileOut, string Password)
            {
                FileStream fsIn = new FileStream(fileIn,
                    FileMode.Open, FileAccess.Read);
                FileStream fsOut = new FileStream(fileOut,
                    FileMode.OpenOrCreate, FileAccess.Write);

                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                Rijndael alg = Rijndael.Create();
                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);

                CryptoStream cs = new CryptoStream(fsOut,
                    alg.CreateEncryptor(), CryptoStreamMode.Write);

                int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int bytesRead;

                do
                {
                    bytesRead = fsIn.Read(buffer, 0, bufferLen);
                    cs.Write(buffer, 0, bytesRead);
                } while (bytesRead != 0);

                cs.Close();
                fsIn.Close();
            }

            public static byte[] Decrypt(byte[] cipherData,
                                        byte[] Key, byte[] IV)
            {
                MemoryStream ms = new MemoryStream();

                Rijndael alg = Rijndael.Create();
                alg.Key = Key;
                alg.IV = IV;

                CryptoStream cs = new CryptoStream(ms,
                    alg.CreateDecryptor(), CryptoStreamMode.Write);

                cs.Write(cipherData, 0, cipherData.Length);

                cs.Close();

                byte[] decryptedData = ms.ToArray();

                return decryptedData;
            }

            public static string Decrypt(string cipherText, string Password)
            {
                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65,
            0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                byte[] decryptedData = Decrypt(cipherBytes,
                    pdb.GetBytes(32), pdb.GetBytes(16));

                return System.Text.Encoding.Unicode.GetString(decryptedData);
            }

            public static byte[] Decrypt(byte[] cipherData, string Password)
            {
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
            }

            public static void Decrypt(string fileIn,
                        string fileOut, string Password)
            {
                FileStream fsIn = new FileStream(fileIn,
                            FileMode.Open, FileAccess.Read);
                FileStream fsOut = new FileStream(fileOut,
                            FileMode.OpenOrCreate, FileAccess.Write);

                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
                Rijndael alg = Rijndael.Create();

                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);

                CryptoStream cs = new CryptoStream(fsOut,
                    alg.CreateDecryptor(), CryptoStreamMode.Write);

                int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int bytesRead;

                do
                {
                    bytesRead = fsIn.Read(buffer, 0, bufferLen);

                    // Decrypt it 
                    cs.Write(buffer, 0, bytesRead);

                } while (bytesRead != 0);

                cs.Close();
                fsIn.Close();
            }
        }

        public class Response
        {
            public Response(HttpWebResponse response)
            {
                if (response == null)
                {
                    this.description = "response == null";
                    this.code = HttpStatusCode.BadRequest;
                }
                else
                {
                    this.description = response.StatusDescription;
                    this.code = response.StatusCode;
                }
            }

            public string description { get; set; }

            public HttpStatusCode code { get; set; }

            public bool unsuccess { get { return code != HttpStatusCode.OK; } }

            public override string ToString()
            {
                return ((int)code).ToString() + " - " + description;
            }
        }

        public class DatosDNI
        {
            public string apPrimer { get; set; }
            public string apSegundo { get; set; }
            public string prenombres { get; set; }
            public string estadoCivil { get; set; }
            public string ubigeo { get; set; }
            public string foto { get; set; }
            public string direccion { get; set; }
            public string restriccion { get; set; }

        }
        #endregion

        public void buscarRucSunat(TextBox txtRuc, TextBox txtRazon, TextBox txtRepresentante, HtmlGenericControl divMsj, string pIdUsuario)
        {
  
            BLUtil util = new BLUtil();

            string pRUC = txtRuc.Text.Trim();

            string ip = GetIPAddress();
            string url = String.Format(ConfigurationManager.AppSettings["UrlPIDEBackend"]);
            string pJson = PrepareJsonParams(new { pAccion = "1", pContrato = "10", numruc = pRUC, pServicio = "1", pIdSistema = "4", pIp = ip, CodArea = "0", codUsuario = pIdUsuario, vNomUsuario = "SSP", vArea = "SSP" });
            string respuesta = invokePost(url, pJson);

            try
            {
                Newtonsoft.Json.Linq.JObject JObject = Newtonsoft.Json.Linq.JObject.Parse(respuesta);

                string ErrorCode = JObject["data"]["error"]["xidError"].ToString();
                string msjError = JObject["data"]["error"]["msjError"].ToString();

                if (ErrorCode == "200")
                {
                    string length = JObject["data"]["objects"]["table1"]["length"].ToString();
                    string rpta = JObject["data"]["objects"]["table1"]["data"].ToString();

                    if (length != "0")
                    {
                        DataTable dtResp1 = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(rpta); // parseJsonToDs("[{" + vResult + "}]", mylist);
                        txtRazon.Text = dtResp1.Rows[0]["ddp_nombre"].ToString();

                        if (txtRazon.Text.Length > 0)
                        {
                            divMsj.InnerHtml = "";
                            pJson = PrepareJsonParams(new { pAccion = "1", pContrato = "10", numruc = pRUC, pServicio = "8", pIdSistema = "4", pIp = ip, CodArea = "0", codUsuario = pIdUsuario, vNomUsuario = "SSP", vArea = "SSP" });
                            respuesta = invokePost(url, pJson);

                            Newtonsoft.Json.Linq.JObject JObject2 = Newtonsoft.Json.Linq.JObject.Parse(respuesta);

                            string ErrorCode2 = JObject2["data"]["error"]["xidError"].ToString();
                            string msjError2 = JObject2["data"]["error"]["msjError"].ToString();

                            if (ErrorCode2 == "200")
                            {
                                string length2 = JObject2["data"]["objects"]["table1"]["length"].ToString();
                                string rpta2 = JObject2["data"]["objects"]["table1"]["data"].ToString();
                                if (length2 != "0")
                                {
                                    DataTable dtResp2 = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(rpta2); // parseJsonToDs("[{" + vResult + "}]", mylist);
                                    txtRepresentante.Text = dtResp2.Rows[0]["rso_nombre"].ToString();
                                }

                            }
                        }
                        else
                        {
                            string phtml = "<p class='bg-danger paddingSM'>" + "RUC no encontrado. Verificar valor ingresado." + "</p>";
                            divMsj.InnerHtml = phtml;
                        }
                    }
                }
                else
                {
                    string phtml = "<p class='bg-danger paddingSM'>" + "RUC no encontrado. Verificar valor ingresado." + "</p>";
                    divMsj.InnerHtml = phtml;

                }
            }
            catch (Exception ex)
            {
                string phtml = "<p class='bg-danger paddingSM'>" + "Error con la SUNAT. Vuelva a intentarlo en unos minutos" + "</p>";
                divMsj.InnerHtml = phtml;

            }
        }

        public string formatoparafecha(string fecha)
        {
            string fechastr = "";
            DateTime fec = Convert.ToDateTime(fecha);
            fechastr = fec.Day.ToString().PadLeft(2, '0') + "/" + fec.Month.ToString().PadLeft(2, '0') + "/" + fec.Year.ToString();
            return fechastr;
        }

        public bool IsDate(Object obj)
        {
            string strDate = obj.ToString();
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public byte[] GeneratePDFFile(string pNameFile)
        {
            byte[] fileContent = null;

            try
            {
                string vUrlWebForm = ConfigurationManager.AppSettings["UrlWebForm"].ToString();
                string url = HttpContext.Current.Request.Url.PathAndQuery;
                url = vUrlWebForm + url;
                url = url.Substring(0, url.Length - 1);
                url = url + "p";
                
                string ruta = HttpContext.Current.Server.MapPath("../executables/wkhtmltopdf.0.12.6/");

                string strnom = pNameFile;
                string filepath = ruta + strnom;

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.FileName = ruta + "/bin/wkhtmltopdf.exe";
                process.StartInfo.Arguments = "-O Portrait " + url + " " + filepath;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;

                process.Start();
                process.WaitForExit();

                int returnCode = process.ExitCode;
                process.Close();

                FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
                fileContent = new byte[(int)fs.Length];

                fs.Read(fileContent, 0, (int)fs.Length);

                fs.Close();

                File.Delete(ruta + strnom);

            }
            catch (Exception e)
            {
                MailDeError("Error: " + e.ToString(),"Error en generaci�n de PDF",false);
            }
            return fileContent;
        }

        public void MailDeError(String pCuerpoHtml, string pAsunto, bool pEnviarRedes)
        {
            string pEnvironment = ConfigurationManager.AppSettings["Environment"].ToString();
            if (!(pEnvironment.Equals("Prod")))
            {
                pAsunto = pAsunto + " - [" + pEnvironment + "]";
            }
            
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            //if (pEnviarRedes)
            //{
            //}
            mail.To.Add("monitorObras21@gmail.com");
            mail.Subject = pAsunto;
            string cuerpo = pCuerpoHtml;
            /*********************************************/
            //mail.From = new MailAddress("noreply.monitor@gmail.com", "SSP", System.Text.Encoding.UTF8);
            mail.Body = cuerpo;
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }
    }
}
