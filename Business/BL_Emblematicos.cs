﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_Emblematicos
    {
         

       //  public DataTable spEMB_BandejaProyectos(BE_Emblematicos _BE)
       //{
       //    try
       //    {
       //        DataSet dstFinanciamiento;
       //        DE_Emblematicos objProy = new DE_Emblematicos();
       //        dstFinanciamiento = objProy.spEMB_BandejaProyectos(_BE);

       //        return dstFinanciamiento.Tables[0];

       //    }
       //    catch (Exception ex)
       //    {
       //        throw ex;
       //    }
       //    finally
       //    {
       //    }
       //}

         public List<BE_Emblematicos> F_spEMB_BandejaProyectos(BE_Emblematicos _BE)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_BandejaProyectos(_BE);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }
        
         //public DataTable spEmb_Seguimiento(int id, int flagEjecutado)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEmb_Seguimiento(id,flagEjecutado);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public List<BE_Emblematicos> F_spEmb_Seguimiento(int id_proyecto, int flagEjecutado)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEmb_Seguimiento(id_proyecto, flagEjecutado);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public int spEMB_getValidaRegistroSeguimiento(int idProyecto, int flagEjecutado, int idSeguimiento, string anio)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spEMB_getValidaRegistroSeguimiento(idProyecto,flagEjecutado,idSeguimiento,anio);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public List<BE_Emblematicos> F_spEMB_Grupo()
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_Grupo();
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_MetasProyectos(BE_Emblematicos _BE)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_MetasProyectos(_BE);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_Actividad(BE_Emblematicos _BE)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_Actividad(_BE);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_getActividad(int idActividad)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_getActividad(idActividad);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_Documento(int id_proyecto)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_Documento(id_proyecto);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_ActividadHistorial(int id_proyecto)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_ActividadHistorial(id_proyecto);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public DataTable spEMB_Proyecto(int id)
         {
             try
             {
                 DataSet dstFinanciamiento;
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 dstFinanciamiento = objProy.spEMB_Proyecto(id);

                 return dstFinanciamiento.Tables[0];

             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         //public DataTable spEMB_Actividad(int id)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEMB_Actividad(id);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public List<BE_Emblematicos> F_spEMB_Reporte_Resumen(int id_proyecto)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_Reporte_Resumen(id_proyecto);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_Reporte_Gantt(int id_proyecto)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_Reporte_Gantt(id_proyecto);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }
         //public DataTable spEMB_Reporte_Resumen(int id)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEMB_Reporte_Resumen(id);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         //public DataTable spEMB_getActividad(int idActividad)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEMB_getActividad(idActividad);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         //public DataTable spEMB_ActividadComponente(int id)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.F_spEMB_ActividadComponente(id);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public List<BE_Emblematicos> F_spEMB_ActividadComponente(int id_proyecto)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_ActividadComponente(id_proyecto);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_ActividadDetalle(int id_proyecto, string componente)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_ActividadDetalle(id_proyecto, componente);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }


         public int spiEMB_Actividad(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_Actividad(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_NombreComponente(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_NombreComponente(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_Actividad(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_Actividad(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_ActividadDescripcion(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_ActividadDescripcion(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }
         public int spdEMB_Actividad(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spdEMB_Actividad(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         //public DataTable spEMB_ActividadHistorial(int id)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.F_spEMB_ActividadHistorial(id);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public int spiEMB_Seguimiento(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_Seguimiento(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_Seguimiento(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_Seguimiento(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spdEMB_Seguimiento(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spdEMB_Seguimiento(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         //public DataTable spEMB_CalendarioActividad(Int64 id, String flag)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEMB_CalendarioActividad(id, flag);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public List<BE_Emblematicos> F_spEMB_CalendarioActividad(BE_Emblematicos _BE)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_CalendarioActividad(_BE);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public List<BE_Emblematicos> F_spEMB_CalendarioProyecto(BE_Emblematicos _BE)
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_Emblematicos> objCollection = default(List<BE_Emblematicos>);

             try
             {
                 objCollection = new List<BE_Emblematicos>();

                 objCollection = objDet.F_spEMB_CalendarioProyecto(_BE);
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }

         public DataTable spEMB_getCalendarioActividades(Int64 id, DateTime fecha,Int32 tipo, Int32 flagAccion)
         {
             try
             {
                 DataSet dstFinanciamiento;
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 dstFinanciamiento = objProy.spEMB_getCalendarioActividades(id, fecha, tipo, flagAccion);

                 return dstFinanciamiento.Tables[0];

             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }
        
         public int spiEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_CalendarioActividad(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_CalendarioActividad(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spdEMB_CalendarioActividad(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spdEMB_CalendarioActividad(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         //public DataTable spEMB_Responsable()
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.F_spEMB_Responsable();

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public List<BE_MON_BANDEJA> F_spEMB_Responsable()
         {

             DE_Emblematicos objDet = new DE_Emblematicos();

             List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

             try
             {
                 objCollection = new List<BE_MON_BANDEJA>();

                 objCollection = objDet.F_spEMB_Responsable();
                 return objCollection;
             }
             catch (Exception ex)
             {
                 return null;
             }
             finally
             {
                 objCollection = null;
             }
         }
        
         public int spiEMB_Proyecto(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_Proyecto(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_Proyecto(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_Proyecto(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spdEMB_Proyecto(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spdEMB_Proyecto(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }
        
         public int spiEMB_DetalleProyectoRegiones(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_DetalleProyectoRegiones(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spiEMB_DetalleResponsableProyecto(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_DetalleResponsableProyecto(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_DetalleProyectoRegiones(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_DetalleProyectoRegiones(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }
        
         //public DataTable spEMB_getDetalleComponente(int id)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEMB_getDetalleComponente(id);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         public int spuEMB_DetalleResponsableProyecto(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_DetalleResponsableProyecto(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         //public DataTable spEMB_ReporteDevengado(int idProyecto, int flagEjecutado, int anio)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.spEMB_ReporteDevengado(idProyecto,flagEjecutado,anio);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

         //public DataTable spEMB_Documento(Int64 id)
         //{
         //    try
         //    {
         //        DataSet dstFinanciamiento;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        dstFinanciamiento = objProy.F_spEMB_Documento(id);

         //        return dstFinanciamiento.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

      
         public int spiEMB_Documento(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_Documento(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spuEMB_Documento(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spuEMB_Documento(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spdEMB_Documento(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spdEMB_Documento(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spEMB_NivelAcceso(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spEMB_NivelAcceso(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spdEMB_MetasProyectos(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spdEMB_MetasProyectos(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spiEMB_MetasProyectos(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_MetasProyectos(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spiEMB_Impresion(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_Impresion(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         public int spiEMB_DetalleImpresion(BE_Emblematicos _BE)
         {
             try
             {
                 DE_Emblematicos objProy = new DE_Emblematicos();
                 return objProy.spiEMB_DetalleImpresion(_BE);
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

         //public DataTable spEMB_Grupo()
         //{
         //    try
         //    {
         //        DataSet ds;
         //        DE_Emblematicos objProy = new DE_Emblematicos();
         //        ds = objProy.F_spEMB_Grupo();

         //        return ds.Tables[0];

         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
         //    finally
         //    {
         //    }
         //}

        

    }
}
