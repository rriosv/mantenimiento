﻿using DataAccess;
using System;
using System.Data;
using Entity;


namespace Business
{
    public class BL_MON_Riesgo
    {
        public DataTable spMON_Riesgos(int pIdProyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Riesgo objProy = new DE_MON_Riesgo();
                ds = objProy.spMON_Riesgos(pIdProyecto);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_RiesgosFicha(int pIdProyecto, int pIdRiesgo)
        {
            try
            {
                DataSet ds;
                DE_MON_Riesgo objProy = new DE_MON_Riesgo();
                ds = objProy.spMON_RiesgosFicha(pIdProyecto, pIdRiesgo);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiu_MON_Riesgos(BE_MON_Riesgo _pEntidad)
        {
            try
            {
                DE_MON_Riesgo objProy = new DE_MON_Riesgo();
                return objProy.spiu_MON_Riesgos(_pEntidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiu_MON_RiesgosFicha(BE_MON_Riesgo _pEntidad)
        {
            try
            {
                DE_MON_Riesgo objProy = new DE_MON_Riesgo();
                return objProy.spiu_MON_RiesgosFicha(_pEntidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spd_MON_Riesgos(BE_MON_Riesgo _pEntidad)
        {
            try
            {
                DE_MON_Riesgo objProy = new DE_MON_Riesgo();
                return objProy.spd_MON_Riesgos(_pEntidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_RiesgosChart(int pIdProyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Riesgo objProy = new DE_MON_Riesgo();
                ds = objProy.spMON_RiesgosChart(pIdProyecto);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
