﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Historial
    {
      
        public DataTable spMON_Proyecto_HistorialMonitoreo(int id_proyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Historial objProy = new DE_MON_Historial();
                ds = objProy.spMON_Proyecto_HistorialMonitoreo(id_proyecto);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spi_MON_PROYECTO_HistorialMonitoreo(BE_MON_Historial _BE_Historial)
        {
            try
            {
                DE_MON_Historial objProy = new DE_MON_Historial();
                return objProy.spi_MON_PROYECTO_HistorialMonitoreo(_BE_Historial);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Proyecto_HistorialMonitoreo(BE_MON_Historial _BE_Historial)
        {
            try
            {
                DE_MON_Historial objProy = new DE_MON_Historial();
                return objProy.spud_MON_Proyecto_HistorialMonitoreo(_BE_Historial);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
