﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
   public class BL_MON_Ampliacion
    {
        public DataTable sp_MON_Penalidad(int id_proyecto)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                dstFinanciamiento = objProy.sp_MON_Penalidad(id_proyecto);
                return dstFinanciamiento.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spi_MON_Penalidad(int id_proyecto, int iTipoPenalidad, string detalleIncumplimiento, DateTime fechaPenalidad, decimal montoPenalidad, int id_usuario)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spi_MON_Penalidad(id_proyecto, iTipoPenalidad, detalleIncumplimiento, fechaPenalidad, montoPenalidad, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spud_MON_Penalidad(int id_penalidad, int tipo, int iTipoPenalidad, string detalleIncumplimiento, DateTime fechaPenalidad, decimal montoPenalidad, int id_usuario)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spud_MON_Penalidad(id_penalidad, tipo, iTipoPenalidad, detalleIncumplimiento, fechaPenalidad, montoPenalidad, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_Ampliacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spi_MON_Ampliacion(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_AmpliacionPE(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spi_MON_AmpliacionPE(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_MON_Ampliacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                dstFinanciamiento = objProy.sp_MON_Ampliacion(_BE_Ampliacion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int D_MON_Ampliacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.D_MON_Ampliacion(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Ampliacion_Editar(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spud_MON_Seguimiento_Ampliacion_Editar(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
           
        public int spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
          

        public int spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_AmpliacionPE(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spu_MON_AmpliacionPE(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int I_MON_AmpliacionPlazoPorContrata(BE_MON_Ampliacion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.I_MON_AmpliacionPlazoPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public int U_MON_AmpliacionPlazoPorContrata(BE_MON_Ampliacion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.U_MON_AmpliacionPlazoPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public int I_MON_Adendas(BE_MON_Ampliacion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.I_MON_Adendas(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public int UD_MON_Adendas(BE_MON_Ampliacion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.UD_MON_Adendas(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public List<BE_MON_Ampliacion> F_MON_CertificacionAutorizacion_PorContrata(BE_MON_Ampliacion _BE)
        {

            DE_MON_Ampliacion objDet = new DE_MON_Ampliacion();
            List<BE_MON_Ampliacion> objCollection = default(List<BE_MON_Ampliacion>);

            try
            {
                objCollection = new List<BE_MON_Ampliacion>();

                objCollection = objDet.F_MON_CertificacionAutorizacionPorContrata(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public int I_MON_DetalleCertificacionAutorizacionPorContrata(BE_MON_Ampliacion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.I_MON_DetalleCertificacionAutorizacionPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public int UD_MON_DetalleCertificacionAutorizacionPorContrata(BE_MON_Ampliacion _BE_Ejecucion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.UD_MON_DetalleCertificacionAutorizacionPorContrata(_BE_Ejecucion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }


        public int spi_Detalle_Paralizacion(BE_MON_Ampliacion _BE_MON_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spi_Detalle_Paralizacion(_BE_MON_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spMON_T_Detalle_Paralizacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DataSet dstDetalleParalizacion;
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                dstDetalleParalizacion = objProy.spMON_T_Detalle_Paralizacion(_BE_Ampliacion);

                return dstDetalleParalizacion.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_Detalle_Paralizacion(BE_MON_Ampliacion _BE_MON_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spu_MON_Detalle_Paralizacion(_BE_MON_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_Elim_Detalle_Paralizacion(BE_MON_Ampliacion _BE_MON_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spu_MON_Elim_Detalle_Paralizacion(_BE_MON_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Detalle_Paralizacion(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spud_MON_Detalle_Paralizacion(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                dstFinanciamiento = objProy.spMON_AvanceFisicoFoto(_BE_Ampliacion);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spiMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spiMON_AvanceFisicoFoto(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spuMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.spuMON_AvanceFisicoFoto(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int speMON_AvanceFisicoFoto(BE_MON_Ampliacion _BE_Ampliacion)
        {
            try
            {
                DE_MON_Ampliacion objProy = new DE_MON_Ampliacion();
                return objProy.speMON_AvanceFisicoFoto(_BE_Ampliacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


    }
}
