﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using Entity;

namespace Business
{
   public class BLAccionesDiarias
    {
             

        public DataTable sp_CalendarioAccionesDiarias(string strSNIP)
        {
            try
            {
                DataSet dstAccionesDiarias;
                DEAccionesDiarias objProy = new DEAccionesDiarias();
                dstAccionesDiarias = objProy.sp_CalendarioAccionesDiarias(strSNIP);

                return dstAccionesDiarias.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable sp_lista_TipoComunicacion()
        {
            try
            {
                DataSet dstAccionesDiarias;
                DEAccionesDiarias objProy = new DEAccionesDiarias();
                dstAccionesDiarias = objProy.sp_lista_TipoComunicacion();

                return dstAccionesDiarias.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable sp_obtiene_InfoBusqueda(string strSNIP, string fecha)
        {
            try
            {
                DataSet dstAccionesDiarias;
                DEAccionesDiarias objProy = new DEAccionesDiarias();
                dstAccionesDiarias = objProy.sp_obtiene_InfoBusqueda(strSNIP, fecha);
                return dstAccionesDiarias.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable sp_obtiene_ComunicacionesRealiazadas(string strSNIP, string fecha)
        {
            try
            {
                DataSet dstAccionesDiarias;
                DEAccionesDiarias objProy = new DEAccionesDiarias();
                dstAccionesDiarias = objProy.sp_obtiene_ComunicacionesRealiazadas(strSNIP, fecha);
                return dstAccionesDiarias.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spi_AccionesDiarias(string snip, string fecha, string observacion, string correo, string telef, string oficio, string reu, string otros, int user,string id_solicitud, string flagAyuda)
        {
            try
            {
                DEAccionesDiarias objProy = new DEAccionesDiarias();
                return objProy.spi_AccionesDiarias(snip, fecha, observacion, correo, telef, oficio, reu, otros, user,id_solicitud, flagAyuda);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable sp_ValidaNombreAcciones(string snip)
         {
             try
             {
                 DataSet dstAccionesDiarias;
                 DEAccionesDiarias objProy = new DEAccionesDiarias();
                 dstAccionesDiarias = objProy.sp_ValidaNombreAcciones(snip);
                 return dstAccionesDiarias.Tables[0];

             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {
             }
         }

        public DataTable spSOL_ReporteListaDepartamento()
        {
            try
            {
                DataSet dstAccionesDiarias;
                DEAccionesDiarias objProy = new DEAccionesDiarias();
                dstAccionesDiarias = objProy.spSOL_ReporteListaDepartamento();
                return dstAccionesDiarias.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spdSOL_Acciones(string SNIP, string fecha, Int32 user)
        {
            try
            {

                DEAccionesDiarias objProy = new DEAccionesDiarias();
                return objProy.spdSOL_Acciones(SNIP, fecha,  user);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        //public DataTable sp_Acciones(string strSNIP)
        //{
        //    try
        //    {
        //        DataSet dstAccionesDiarias;
        //        DEAccionesDiarias objProy = new DEAccionesDiarias();
        //        dstAccionesDiarias = objProy.sp_Acciones(strSNIP);
        //        return dstAccionesDiarias.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_SOL_Acciones(string id_solicitudes)
        {
            DEAccionesDiarias objDet = new DEAccionesDiarias();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_SOL_Acciones(id_solicitudes);
                return objCollection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCollection = null;
            }
        }

    }
}
