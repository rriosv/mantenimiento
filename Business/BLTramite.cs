using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;

namespace Business
{
    public class BLTramite
    {
     

        #region consulta

        public DataTable sp_tramite(string codigo)
        {
            try
            {
                DataSet dstUsuario;
                DETramite objlogin = new DETramite();
                dstUsuario = objlogin.sp_tramite(codigo);

                return dstUsuario.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable sp_tramiteVerifica(string codigo)
        {
            try
            {
                DataSet dstUsuario;
                DETramite objlogin = new DETramite();
                dstUsuario = objlogin.sp_tramiteVerifica(codigo);

                return dstUsuario.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion
    }

}
