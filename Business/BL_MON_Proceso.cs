﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;


namespace Business
{
    public class BL_MON_Proceso
    {

        public int spMON_Acceso(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spMON_Acceso(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_ProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                dstFinanciamiento = objProy.spMON_ProcesoSeleccion(_BE_Proceso);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_ProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spi_MON_ProcesoSeleccion(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_ProcesoSeleccionPE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spi_MON_ProcesoSeleccionPE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoAdjudicacion()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoAdjudicacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_PROCESO> F_spMON_SeguimientoProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_SeguimientoProcesoSeleccion(_BE_Proceso);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_PROCESO> F_spMON_RendicionCuentaNE(BE_MON_PROCESO _BE_Proceso)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_RendicionCuentaNE(_BE_Proceso);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int I_spi_MON_RendicionCuentaNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_RendicionCuentaNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_spud_MON_RendicionCuentaNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.UD_spud_MON_RendicionCuentaNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spi_MON_SeguimientoProcesoSeleccion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spi_MON_SeguimientoProcesoSeleccion(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_PROCESO> F_spMON_GrupoConsorcio(BE_MON_PROCESO _BE_Proceso)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_GrupoConsorcio(_BE_Proceso);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public int spi_MON_GrupoConsorcio(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spi_MON_GrupoConsorcio(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoModalidad()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoModalidad();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public List<BE_MON_BANDEJA> F_spMON_TipoAlcanceconvenio()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoAlcanceconvenio();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoContratacion()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoContratacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_PROCESO> F_spMON_SubContrato(BE_MON_PROCESO _BE_Proceso)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_SubContrato(_BE_Proceso);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spi_MON_ResponsableObra(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spi_MON_ResponsableObra(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoModalidad_Contrato()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoModalidad_Contrato();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_ListaRubro()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_ListaRubro();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoDocumentoIdentificacion()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoDocumentoIdentificacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoGradoInstruccion()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoGradoInstruccion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoOcupacion()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoOcupacion();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public int spi_MON_SubContrato(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spi_MON_SubContrato(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Procesos_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_Procesos_Eliminar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Procesos_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_Procesos_Editar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_SubContrato_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_SubContrato_Eliminar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_SubContrato_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_SubContrato_Editar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Responsable_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_Responsable_Eliminar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Responsable_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_Responsable_Editar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Consorcio_Eliminar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_Consorcio_Eliminar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_Seguimiento_Consorcio_Editar(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spud_MON_Seguimiento_Consorcio_Editar(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spu_MON_ModalidadEjecucion(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.spu_MON_ModalidadEjecucion(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_ProcesoSeleccionIndirectaNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_ProcesoSeleccionIndirectaNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_ProcesoSeleccionIndirectaNEConvenio(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_ProcesoSeleccionIndirectaNEConvenio(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_ProcesoSeleccionIndirectaNESolicitud(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_ProcesoSeleccionIndirectaNESolicitud(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_PadronMiembrosNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_PadronMiembrosNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_ProcesoMiembroNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_ProcesoMiembroNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_PROCESO> F_spMON_PadronMiembrosNE()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_PadronMiembrosNE();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoCargoMiembrosNE()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoCargoMiembrosNE();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoEstadoAsignacionNE()
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoEstadoAsignacionNE();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_PROCESO> F_spMON_ProcesoMiembroNE(BE_MON_PROCESO _BE_Proceso)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_ProcesoMiembroNE(_BE_Proceso);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_PROCESO> F_spMON_AprobacionExpediente(int id_proyecto)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            List<BE_MON_PROCESO> objCollection = default(List<BE_MON_PROCESO>);

            try
            {
                objCollection = new List<BE_MON_PROCESO>();

                objCollection = objDet.F_spMON_AprobacionExpediente(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int U_AprobacionExpedienteNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.U_AprobacionExpedienteNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int UD_spud_MON_ProcesoMiembroNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.UD_spud_MON_ProcesoMiembroNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public BE_MON_PROCESO F_spMON_SaldoBancoNE(int id_proyecto)
        {

            DE_MON_PROCESO objDet = new DE_MON_PROCESO();
            BE_MON_PROCESO objCollection = default(BE_MON_PROCESO);

            try
            {
                objCollection = objDet.F_spMON_SaldoBancoNE(id_proyecto);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int IU_spiu_MON_SaldoBancoNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.IU_spiu_MON_SaldoBancoNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_DatosPreoperativosNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                dstFinanciamiento = objProy.spMON_DatosPreoperativosNE(_BE_Proceso);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_spiu_MON_CuentaBancariaNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.IU_spiu_MON_CuentaBancariaNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_spiu_MON_SesionesNE(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.IU_spiu_MON_SesionesNE(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_spi_MON_Modalidad(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_spi_MON_Modalidad(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_PersonalClave(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                dstFinanciamiento = objProy.spMON_PersonalClave(_BE_Proceso);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_PersonalClave(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.I_MON_PersonalClave(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int UD_MON_PersonalClave(BE_MON_PROCESO _BE_Proceso)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.UD_MON_PersonalClave(_BE_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int paSSP_HitosProyecto(BE_HitosProyecto _BE)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                return objProy.paSSP_HitosProyecto(_BE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
            }
        }



        public DataTable spMON_HitosProyecto(int iId, int EtapaRegistro)
        {
            try
            {
                DE_MON_PROCESO objProy = new DE_MON_PROCESO();
                DataSet ds = objProy.spMON_HitosProyecto(iId, EtapaRegistro);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
