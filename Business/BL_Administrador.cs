﻿using DataAccess;
using Entity;
using System;
using System.Collections.Generic;
namespace Business
{
    public class BL_Administrador
    {
        public List<BE_Administrador> F_spAdm_ListarUsuarios()
        {

            DE_Administrador objDet = new DE_Administrador();

            List<BE_Administrador> objCollection = default(List<BE_Administrador>);

            try
            {
                objCollection = new List<BE_Administrador>();

                objCollection = objDet.F_spAdm_ListarUsuarios();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_Administrador> F_spAdm_ListaOficina()
        {

            DE_Administrador objDet = new DE_Administrador();

            List<BE_Administrador> objCollection = default(List<BE_Administrador>);

            try
            {
                objCollection = new List<BE_Administrador>();

                objCollection = objDet.F_spAdm_ListaOficina();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public BE_Administrador F_spAdm_ValidarUsuario(BE_Administrador _BE)
        {

            DE_Administrador objDet = new DE_Administrador();

            BE_Administrador objCollection = default(BE_Administrador);

            try
            {
                objCollection = new BE_Administrador();

                objCollection = objDet.F_spAdm_ValidarUsuario(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public BE_Administrador F_sp_GetByIdUsuario(int idUsuario)
        {

            DE_Administrador objDet = new DE_Administrador();

            BE_Administrador objCollection = default(BE_Administrador);

            try
            {
                objCollection = new BE_Administrador();

                objCollection = objDet.F_sp_GetByIdUsuario(idUsuario);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int spiuAdm_Usuario(BE_Administrador _BE)
        {
            try
            {
                DE_Administrador objProy = new DE_Administrador();
                return objProy.spiuAdm_Usuario(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public int spiAdm_AccesoPublico(BE_Administrador _BE)
        {
            try
            {
                DE_Administrador objProy = new DE_Administrador();
                return objProy.spiAdm_AccesoPublico(_BE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}
