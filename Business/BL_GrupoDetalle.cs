﻿using Entity;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class BL_GrupoDetalle
    {
        public List<BE_GrupoDetalle> ListarGrupo(int idgrupo)
        {
            //conexion a la base de datos
            return new DE_GrupoDetalle().ListarGrupo(idgrupo);

        }
    }
}
