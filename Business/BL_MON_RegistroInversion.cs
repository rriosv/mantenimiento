﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_RegistroInversion
    {
      
        public DataTable spMON_FaseInversion(int id_proyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_RegistroInversion objProy = new DE_MON_RegistroInversion();
                ds = objProy.spMON_FaseInversion(id_proyecto);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_FaseInversion(int id_proyecto, int iTipoRegistro, decimal montoRegistrado, DateTime fechaRegistro, string enlace, int id_usuario)
        {
            try
            {
                DE_MON_RegistroInversion objProy = new DE_MON_RegistroInversion();
                return objProy.spi_MON_FaseInversion(id_proyecto, iTipoRegistro, montoRegistrado, fechaRegistro, enlace, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_FaseInversion(int id_proyecto, int iTipoRegistro, decimal montoRegistrado, DateTime fechaRegistro, string enlace, int id_usuario, int id_faseInversion, int tipo)
        {
            try
            {
                DE_MON_RegistroInversion objProy = new DE_MON_RegistroInversion();
                return objProy.spud_MON_FaseInversion(id_proyecto, iTipoRegistro, montoRegistrado, fechaRegistro, enlace, id_usuario, id_faseInversion, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }
}
