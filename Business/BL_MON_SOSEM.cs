﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_SOSEM
    {
        
 

       public int spi_MON_FinanSOSEM(BE_MON_SOSEM _BE_SOSEM)
        {
            try
            {
                DE_MON_SOSEM objProy = new DE_MON_SOSEM();
                return objProy.spi_MON_FinanSOSEM(_BE_SOSEM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

       public int spud_MON_SOSEM_ELIMINAR(BE_MON_SOSEM _BE_SOSEM)
       {
           try
           {
               DE_MON_SOSEM objProy = new DE_MON_SOSEM();
               return objProy.spud_MON_SOSEM_ELIMINAR(_BE_SOSEM);
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }
       public int spud_MON_SOSEM_EDITAR(BE_MON_SOSEM _BE_SOSEM)
       {
           try
           {
               DE_MON_SOSEM objProy = new DE_MON_SOSEM();
               return objProy.spud_MON_SOSEM_EDITAR(_BE_SOSEM);
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
           }
       }
    }
}
