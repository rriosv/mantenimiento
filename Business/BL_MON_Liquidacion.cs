﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Liquidacion
    {

        public int spi_MON_Liquidacion(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.spi_MON_Liquidacion(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable sp_MON_Liquidacion(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                dstFinanciamiento = objProy.sp_MON_Liquidacion(_BE_LIQUIDACION);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_Listado_Proyecto(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                dstFinanciamiento = objProy.spMON_Listado_Proyecto(_BE_LIQUIDACION);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_Ayuda_Memoria(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                dstFinanciamiento = objProy.spMON_Ayuda_Memoria(_BE_LIQUIDACION);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable paSSP_rFichaTecnica(string pSNIP)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                dstFinanciamiento = objProy.paSSP_rFichaTecnica(pSNIP);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spMON_Ayuda_MemoriaNE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                dstFinanciamiento = objProy.spMON_Ayuda_MemoriaNE(_BE_LIQUIDACION);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        


        public DataTable spMON_Ayuda_MemoriaPE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DataSet dstFinanciamiento;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                dstFinanciamiento = objProy.spMON_Ayuda_MemoriaPE(_BE_LIQUIDACION);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_spMON_TipoReceptora()
        {

            DE_MON_Liquidacion objDet = new DE_MON_Liquidacion();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoReceptora();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int IU_spiu_MON_CierreProyecto_NE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.IU_spiu_MON_CierreProyecto_NE(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_spiu_MON_LiquidacionNE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.IU_spiu_MON_LiquidacionNE(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int I_MON_LiquidacionPE(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.I_MON_LiquidacionPE(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_MON_RecepcionObra_PorContrata(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.IU_MON_RecepcionObra_PorContrata(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_MON_SostenibilidadPorContrata(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.IU_MON_SostenibilidadPorContrata(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int IU_MON_LiquidacionPorContrata(BE_MON_Liquidacion _BE_LIQUIDACION)
        {
            try
            {
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                return objProy.IU_MON_LiquidacionPorContrata(_BE_LIQUIDACION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_Liquidacion> F_WEB_LiquidadosNE(BE_MON_BANDEJA BE)
        {

            DE_MON_Liquidacion objDet = new DE_MON_Liquidacion();
            List<BE_MON_Liquidacion> objCollection = default(List<BE_MON_Liquidacion>);

            try
            {
                objCollection = new List<BE_MON_Liquidacion>();

                objCollection = objDet.F_WEB_LiquidadosNE(BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Financiamiento> F_WEB_EstadosNE(BE_MON_BANDEJA _BE)
        {
            DE_MON_Liquidacion objDet = new DE_MON_Liquidacion();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);
            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_WEB_EstadosNE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public List<BE_MON_Financiamiento> F_WEB_MontoInversionNE(BE_MON_BANDEJA _BE)
        {
            DE_MON_Liquidacion objDet = new DE_MON_Liquidacion();
            List<BE_MON_Financiamiento> objCollection = default(List<BE_MON_Financiamiento>);
            try
            {
                objCollection = new List<BE_MON_Financiamiento>();

                objCollection = objDet.F_MontoInversionNE(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }


        public DataSet spMON_FichaPriorizados(string pIdProyecto)
        {
            try
            {
                DataSet ds;
                DE_MON_Liquidacion objProy = new DE_MON_Liquidacion();
                ds = objProy.spMON_FichaPriorizados(pIdProyecto);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
    }

}
