using System;
using System.Collections.Generic;
using System.Data;
using Entity;

namespace Business
{
    public class BLProyecto
    {

        #region consulta

        public DataTable sp_listadoProyectos(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoProyectos(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataSet paSSP_SOL_rProyectoIntegral(int pSnip)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.paSSP_SOL_rProyectoIntegral(pSnip);

                return dstProyecto;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #endregion

        #region concesiones
        public DataTable sp_listadoConcesiones(int tipo, int id_conce, int flag_report)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoConcesiones(tipo, id_conce, flag_report);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoAcuerdos(int id_conce, int tipo, int etapa)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoAcuerdos(id_conce, tipo, etapa);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoSituacion(int id_conce)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoSituacion(id_conce);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoProyectosConcesiones(int id_conce)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoProyectosConcesiones(id_conce);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoReProgramacion(int id_acuerdo)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoReprogramacion(id_acuerdo);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoAudiConcesiones(int id_conce, int opcion)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoAudiConcesiones(id_conce, opcion);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiu_SituacionActual(int id_situacion, int id_concesiones, int semana, string acc_ejec, string acc_pen, string comentario, string fecha, int estado, int usuario)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiu_SituacionActual(id_situacion, id_concesiones, semana, acc_ejec, acc_pen, comentario, fecha, estado, usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spiu_Acuerdos(int id_acuerdo, int id_concesiones, int nro, string acuerdo, string fecha_acuerdo, string fecha_fin, int etapa, int estado_vb, int estado, int usuario)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiu_Acuerdos(id_acuerdo, id_concesiones, nro, acuerdo, fecha_acuerdo, fecha_fin, etapa, estado_vb, estado, usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spiu_Reprogramacion(int id_acuerdo, string fecha_antigua, string fecha_nueva, int usuario)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiu_Reprogramacion(id_acuerdo, fecha_antigua, fecha_nueva, usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion

        #region Solicitudes

        public DataTable paAdm_ReporteAdmisibilidad()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.paAdm_ReporteAdmisibilidad();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteMinistro()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteMinistro();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_ReporteMinistroFiltro(string departamento, string snip, string proyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteMinistroFiltro(departamento, snip, proyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spSOL_Documentos(BEDocumento _BEDocumento)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Documentos(_BEDocumento);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ConsultaPry(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ConsultaPry(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_BancaPry(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_BancaPry(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spSOL_ListarTipoFinanciamiento()
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.spSOL_ListarTipoFinanciamiento();

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BEProyecto> F_spSOL_ListarTipoFinanciamiento()
        {

            DEProyecto objDet = new DEProyecto();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_ListarTipoFinanciamiento();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public DataTable spSOL_Seguimiento_Variable_PNSU(BEVarPnsu _BEVarPnsu)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Seguimiento_Variable_PNSU(_BEVarPnsu);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Seguimiento_Variable_PMIB(BEVarPimb _BEVarPimb)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Seguimiento_Variable_PMIB(_BEVarPimb);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataSet paSSP_SOL_rMetasAyudaMemoria(int pId, int pEtapaRegistro, int pTipoMeta)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.paSSP_SOL_rMetasAyudaMemoria(pId, pEtapaRegistro,pTipoMeta);

                return dstProyecto;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Seguimiento_Historial(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Seguimiento_Historial(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Plantilla(BECheckList _BECheckList)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Plantilla(_BECheckList);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Seguimiento_Informe(BECheckList _BECheckList)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Seguimiento_Informe(_BECheckList);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable sp_listadoSolicitudes(BEProyecto _beProyecto)
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto(SOLICITUD);
        //        dstProyecto = objProy.sp_listadoSolicitudes(_beProyecto);

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public DataTable sp_listadoSolicitudes_SSP(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoSolicitudes_SSP(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoSolicitudes_SSP1(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_listadoSolicitudes_SSP1(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Apto_Transferencia(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Apto_Transferencia(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteResumenFONIE()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteResumenFONIE();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteDetalleFONIE(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteDetalleFONIE(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteBandejaRevision(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteBandejaRevision(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteBandejaRevisionHistorialEvaluaciones(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteBandejaRevisionHistorialEvaluaciones(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int F_rNumMetasRegistradas(int pId, int pEtapaRegistro, int pTipoMeta)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.paSSP_SOL_rNumMetasRegistradas(pId, pEtapaRegistro, pTipoMeta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteBandejaRevisionPNSU(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteBandejaRevisionPNSU(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteBandejaRevisionPMIB(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteBandejaRevisionPMIB(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteComponentePNSU(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteComponentePNSU(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spSOL_ReporteBandejaRevisionAccionesDiarias(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteBandejaRevisionAccionesDiarias(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_listadoSolicitudesAntiguos(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_listadoSolicitudesAntiguos(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_TransferenciaContinuidad(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_TransferenciaContinuidad(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spSOL_listadoProyEjecucion(BEProyecto _beProyecto)
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.spSOL_listadoProyEjecucion(_beProyecto);

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public DataTable sp_listadoSolicitudesAsignadas(BEProyecto _beProyecto)
        {
            try
            {
                _beProyecto.Cod_estado = 2;
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_solicitudesAsignadas(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_listadoSolicitudesNoAsignadas(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_solicitudesAsignadas(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_NewAsignacion(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_NewAsignacion(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable sp_BaseNegativa_UE(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_BaseNegativa_UE(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_BaseNegativa_DetalleEmpresas(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_BaseNegativa_DetalleEmpresas(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable sp_Listado_Asignacion_Expediente(BEProyecto _beProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_Listado_Asignacion_Expediente(_beProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spiuSOL_IngresarVariablePnsu(BEVarPnsu _BEVarPnsu)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuSOL_IngresarVariablePnsu(_BEVarPnsu);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiuSOL_IngresarVariablePimb(BEVarPimb _BEVarPimb)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuSOL_IngresarVariablePimb(_BEVarPimb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiu_AsignarSolicitud(BEProyecto _beProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiu_AsignarSolicitud(_beProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spiu_QuitarSolicitud(BEProyecto _beProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiu_QuitarSolicitud(_beProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spuSOL_Actualizar_Estados(BEProyecto _beProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spuSOL_Actualizar_Estados(_beProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiuSOL_CheckList_PMIB(BECheckList _BECheckList)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuSOL_CheckList_PMIB(_BECheckList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Seguimiento_ChecklistPMIB(BECheckList _BECheckList)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Seguimiento_ChecklistPMIB(_BECheckList);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_Seguimiento_ChecklistPMIB_Hist_Observaciones(BECheckList _BECheckList)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_Seguimiento_ChecklistPMIB_Hist_Observaciones(_BECheckList);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_obtieneDirectorioUE(string unidadEjecutora, int id_solicitud)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_obtieneDirectorioUE(unidadEjecutora, id_solicitud);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable spSOL_DirectorioUE()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_DirectorioUE();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public int spiuSOL_CheckList_PNSU(BECheckList _BECheckList)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuSOL_CheckList_PNSU(_BECheckList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiuSOL_Ingresar_Informe(BECheckList _BECheckList)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuSOL_Ingresar_Informe(_BECheckList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spuSOL_ActualizarMontos(BEProyecto _beProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spuSOL_ActualizarMontos(_beProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiSOL_Historial_Solicitudes(Int64 id_solicitudes, int tipo)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiSOL_Historial_Solicitudes(id_solicitudes, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiSOL_IngresarBanco(BEProyecto _beProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiSOL_IngresarBanco(_beProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiSOL_PreliminarIngresarBanco(BEProyecto _beProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiSOL_PreliminarIngresarBanco(_beProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spSOL_TipoDocumento()
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.spSOL_TipoDocumento();

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BEProyecto> F_spSOL_TipoDocumento()
        {

            DEProyecto objDet = new DEProyecto();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_TipoDocumento();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        //public DataTable spSOL_Lista()
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.F_spSOL_Lista();

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BEProyecto> F_spSOL_Lista()
        {

            DEProyecto objDet = new DEProyecto();
            List<BEProyecto> objCollection = default(List<BEProyecto>);

            try
            {
                objCollection = new List<BEProyecto>();

                objCollection = objDet.F_spSOL_Lista();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }


        public int spiuSOL_IngresarDocumento(BEDocumento _BEDocumento)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuSOL_IngresarDocumento(_BEDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spuSOL_Eliminar(Int64 codigo, Int32 tipo)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spuSOL_Eliminar(codigo, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #endregion

        #region reportes
        //GENERAL
        public DataTable DS_rep_seguimiento(string cod_subsector, string cod_depa, string cod_prov)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.DS_rep_seguimiento(cod_subsector, cod_depa, cod_prov);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Programas(int subsector, int periodo)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Programas(subsector, periodo);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteComponentePMIB()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteComponentePMIB();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        //public DataTable sabana_consolidado(string cod_depa, int categ)
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto(CORTE);
        //        dstProyecto = objProy.sabana_consolidado(cod_depa, categ);

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public DataTable rep_semanalProy(int programa)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_semanalProy(programa);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        //PIMBP
        public DataTable rep_Proy_Estados(int subsector, int id_usuario)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_Estados(subsector, id_usuario);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_EstadosDept(int subsector, int id_usuario)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_EstadosDept(subsector, id_usuario);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_EstadosZonales(int subsector, int id_usuario)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_EstadosZonales(subsector, id_usuario);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_FechasRecepcion()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_FechasRecepcion();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_FechasRecepcionZonales(int subsector, int id_usuario)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_FechasRecepcionZonales(subsector, id_usuario);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_EstadosSituacional2009()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_EstadosSituacional2009();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_EstadosSituacionalZonales(int subsector, int id_usuario)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_EstadosSituacionalZonales(subsector, id_usuario);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable rep_Proy_AvanceExpTecnicoNucleos(int subsector, int id_usuario)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_Proy_AvanceExpTecnicoNucleos(subsector, id_usuario);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //FORSUR
        public DataTable rep_ListadoProyFORSUR()
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.rep_ListadoProyFORSUR();

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #endregion

        #region tambos

        public DataTable spFOES_Reporte_Resumen_Ministro(int tipo, int periodo, int mes)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spFOES_Reporte_Resumen_Ministro(tipo, periodo, mes);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spFOES_Reporte_Avance(BETambo _BETambo)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spFOES_Reporte_Avance(_BETambo);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spFOES_Detalle_Porcentaje(BETambo _BETambo)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spFOES_Detalle_Porcentaje(_BETambo);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spFoes_Seguimiento_Tambos(BETambo _BETambo)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spFoes_Seguimiento_Tambos(_BETambo);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiuFOES_Ingresar_ProyectoTambo(BETambo _BETambo)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiuFOES_Ingresar_ProyectoTambo(_BETambo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiFOES_Ingresar_Porcentaje_Foes(BETambo _BETambo)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiFOES_Ingresar_Porcentaje_Foes(_BETambo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #endregion

        #region REPORTE DE ERRORES

        public DataTable spSOL_ReporteErrores(string valor, string id_solicitud, string snip, string flag)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteErrores(valor, id_solicitud, snip, flag);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion

        public int sp_inserta_solicitud_ubigeo(long idsolicitud, string ubigeo, bool estado)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.sp_inserta_solicitud_ubigeo(idsolicitud, ubigeo, estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public DataTable sp_validarSNIP(string valor)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.sp_validarSNIP(valor);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spSOL_listadoProyEliminados()
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.spSOL_listadoProyEliminados();

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public DataTable spSOL_CheckListFONIE(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_CheckListFONIE(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiSOL_CheckListFONIE(int id_solicitudes, int id_usuario, int id_campo, string valor)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiSOL_CheckListFONIE(id_solicitudes, id_usuario, id_campo, valor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #region Proyecto NEW


        public DataTable spMON_ObtieneProyectos(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spMON_ObtieneProyectos(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_SNIP_Relacionados(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_SNIP_Relacionados(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spiSOL_SNIP_Relacionados(BEProyecto _BEProyecto)
        {
            try
            {
                int dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spiSOL_SNIP_Relacionados(_BEProyecto);

                return dstProyecto;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spdSOL_SNIP_Relacionados(BEProyecto _BEProyecto)
        {
            try
            {
                int dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spdSOL_SNIP_Relacionados(_BEProyecto);

                return dstProyecto;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_SNIP_RelacionadosBuscar(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_SNIP_RelacionadosBuscar(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
 
        public DataTable spSOL_ReporteFONIE(int id_solicitudes)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteFONIE(id_solicitudes);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spSOL_SeguimientoSITRAD(BEProyecto _BEProyecto)
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.spSOL_SeguimientoSITRAD(_BEProyecto);

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public DataTable spSOL_RequisitoMinimoET_Info(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_RequisitoMinimoET_Info(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_RequisitoMinimoET_CheckList(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_RequisitoMinimoET_CheckList(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spiSOL_RequisitoMinimoET_Info(BEProyecto _BEProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiSOL_RequisitoMinimoET_Info(_BEProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public int spiSOL_RequisitoMinimoET_CheckList(int id_solicitudes, int id_usuario, int id_campo, string valor)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spiSOL_RequisitoMinimoET_CheckList(id_solicitudes, id_usuario, id_campo, valor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_ReporteRequisitoMinimoET(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_ReporteRequisitoMinimoET(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteDemanda(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteDemanda(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spSOL_ReporteDemandaPMIB(BEProyecto _BEProyecto)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_ReporteDemandaPMIB(_BEProyecto);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spMON_ReporteGeneralMINISTRO(BEProyecto _BEProyecto)
        //{
        //    try
        //    {
        //        DataSet dstProyecto;
        //        DEProyecto objProy = new DEProyecto();
        //        dstProyecto = objProy.spMON_ReporteGeneralMINISTRO(_BEProyecto);

        //        return dstProyecto.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}


        public int spuSOL_FlagBandeja(int id_solicitudes, string flagNuevo, string flagRevision, int id_usuario)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spuSOL_FlagBandeja(id_solicitudes, flagNuevo, flagRevision, id_usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_ObtieneProyecto(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_ObtieneProyecto(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }


        public DataTable spSOL_HistorialSolicitudes(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_HistorialSolicitudes(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        #endregion

        public BEProyecto F_spSOL_AyudaMemoria(BEProyecto _BE)
        {

            DEProyecto objDet = new DEProyecto();
               BEProyecto objCollection = default(BEProyecto);

            try
            {
                objCollection = new BEProyecto();

                objCollection = objDet.F_spSOL_AyudaMemoria(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }
        public BEProyecto F_MON_ObtieneModalidad(BEProyecto _BE)
        {

            DEProyecto objDet = new DEProyecto();
            BEProyecto objCollection = default(BEProyecto);

            try
            {
                objCollection = new BEProyecto();

                objCollection = objDet.F_MON_ObtieneModalidad(_BE);
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int U_SOL_DirectorioUE(BEProyecto _BEProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.U_SOL_DirectorioUE(_BEProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_MON_UnidadEjecutora(BEProyecto _BEProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.U_MON_UnidadEjecutora(_BEProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int U_MON_MontosMVCS(BEProyecto _BEProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.U_MON_MontosMVCS(_BEProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public List<BE_MON_BANDEJA> F_SOL_EquivalenciaArchivos()
        {
            DEProyecto objDet = new DEProyecto();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_SOL_EquivalenciaArchivos();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        public int F_SOL_ACCESO(BEProyecto _BEProyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.F_SOL_ACCESO(_BEProyecto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        #region aprobación de expedientes
        public DataTable spSOL_BandejaAprobacion(string Estados, string snip, string proyecto, string tipoPrograma)
        {
            try
            {
                DataSet dstProyecto;
                DEProyecto objProy = new DEProyecto();
                dstProyecto = objProy.spSOL_BandejaAprobacion(Estados, snip, proyecto, tipoPrograma);

                return dstProyecto.Tables[0];

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


      

        public string spSol_ObtenerValoresDerivacion(string Tipo, Int64 id_solicitudes)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spSol_ObtenerValoresDerivacion(Tipo, id_solicitudes);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string spSol_ObtenerExpediente_Sitrad(Int64 id_solicitudes)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spSol_ObtenerExpediente_Sitrad(id_solicitudes);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void spSol_ActualizarDocumentosAprobaciones(Int64 Id_Solicitudes, string Url, string Tipo, string nroDocumento, Int32 usr_registro)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                objProy.spSol_ActualizarDocumentosAprobaciones(Id_Solicitudes, Url, Tipo, nroDocumento, usr_registro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

     

        public Boolean spSol_VerificarCreacion(Int64 id_solicitudes, string tipoDocumento, int usr_registro)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spSol_VerificarCreacion(id_solicitudes, tipoDocumento, usr_registro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AAAAAA(int id_solicitudes)
        {
            try
            {
                DataSet dstFinanciamiento;
                DEProyecto objProy = new DEProyecto();
                dstFinanciamiento = objProy.spSOL_HistorialSolicitudes(id_solicitudes);

                return dstFinanciamiento.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spu_MON_UltimaActualizacion(Int64 id_proyecto)
        {
            try
            {
                DEProyecto objProy = new DEProyecto();
                return objProy.spu_MON_UltimaActualizacion(id_proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

         
    }


}
