﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.IO;
using DataAccess;
using Entity;

namespace Business
{
    public class BL_MON_Calidad
    {
       
        public int spi_MON_PruebaVerificada(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                DE_MON_Calidad objProy = new DE_MON_Calidad();
                return objProy.spi_MON_PruebaVerificada(_BE_Calidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_PruebaVerificada(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                DataSet ds;
                DE_MON_Calidad objProy = new DE_MON_Calidad();
                ds = objProy.spMON_PruebaVerificada(_BE_Calidad);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spi_MON_PruebaCalidad(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                DE_MON_Calidad objProy = new DE_MON_Calidad();
                return objProy.spi_MON_PruebaCalidad(_BE_Calidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public DataTable spMON_PruebaCalidad(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                DataSet ds;
                DE_MON_Calidad objProy = new DE_MON_Calidad();
                ds = objProy.spMON_PruebaCalidad(_BE_Calidad);

                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        //public DataTable spMON_TipoResultadoFinal()
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_Calidad objProy = new DE_MON_Calidad();
        //        ds = objProy.spMON_TipoResultadoFinal();

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        public List<BE_MON_BANDEJA> F_spMON_TipoResultadoFinal()
        {

            DE_MON_Calidad objDet = new DE_MON_Calidad();
            List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

            try
            {
                objCollection = new List<BE_MON_BANDEJA>();

                objCollection = objDet.F_spMON_TipoResultadoFinal();
                return objCollection;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                objCollection = null;
            }
        }

        //public DataTable spMON_TipoEstadoPrueba()
        //{
        //    try
        //    {
        //        DataSet ds;
        //        DE_MON_Calidad objProy = new DE_MON_Calidad();
        //        ds = objProy.spMON_TipoEstadoPrueba();

        //        return ds.Tables[0];

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        //public List<BE_MON_BANDEJA> F_spMON_TipoEstadoPrueba()
        //{

        //    DE_MON_Calidad objDet = new DE_MON_Calidad();
        //    List<BE_MON_BANDEJA> objCollection = default(List<BE_MON_BANDEJA>);

        //    try
        //    {
        //        objCollection = new List<BE_MON_BANDEJA>();

        //        objCollection = objDet.F_spMON_TipoEstadoPrueba();
        //        return objCollection;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        objCollection = null;
        //    }
        //}

        public int spud_MON_PruebaVerificada_Eliminar(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                DE_MON_Calidad objProy = new DE_MON_Calidad();
                return objProy.spud_MON_PruebaVerificada_Eliminar(_BE_Calidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int spud_MON_PruebaVerificada_Editar(BE_MON_Calidad _BE_Calidad)
        {
            try
            {
                DE_MON_Calidad objProy = new DE_MON_Calidad();
                return objProy.spud_MON_PruebaVerificada_Editar(_BE_Calidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

    }
}
