﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginOld.aspx.cs" Inherits="Web.loginOld" %>

<!DOCTYPE html>
<html lang="es-pe">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="theme-color" content="#343a40" />
    <%--<meta property='og:title' content='Sistema de Seguimiento de Proyectos'>
    <meta property='og:type' content='website'>
    <meta property='og:image' content='http://ssp.vivienda.gob.pe/img/logo_master_page.png'>
    <meta property='og:url' content='http://ssp.vivienda.gob.pe'>
    <meta property='og:description' content='Sistema de Monitoreo de Obras'>--%>
    <meta name="Description" content="Sistema de seguimiento de proyecto del Ministerio de Vivienda, Construcción y Saneamiento." />
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--Import Font and Icons-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
    <link media="screen" rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Compiled and minified CSS -->
    <link media="screen" rel="stylesheet" href="sources/bootstrap-4.3.1/css/bootstrap.min.css">
    <!-- Compiled and minified JavaScript -->
    <link media="screen" rel="stylesheet" href="sources/waves/waves.min.css">

    <style>
      
        .content-wrapper {
            overflow: auto;
        }

        html {
            background: url('images/bg-login.jpg') no-repeat 50% fixed;
            background-size: cover;
            background-position: center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            height: 100%;
            overflow: hidden;
        }

        body {
            background: url('images/malla.png') right bottom repeat;
        }

        .bg-dark-trans {
            background-color: rgba(52, 58, 64, .8) !important;
        }

        .bg-secondary-trans {
            background-color: rgba(108, 117, 125, .9) !important;
        }

        footer p {
            font-size: .88em;
        }

        footer {
            position: absolute;
            z-index: -1;
            bottom: 0px;
            width: 100vw;
        }
    </style>

    <title>Sistema de Monitoreo</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark-trans">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">
                    <img src="img/master/logo_escudo_50.png" alt="Logo" />
                   
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsForm"
                    aria-controls="navbarsForm" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsForm">
                    <div class="mr-auto">
                    </div>
                    <form runat="server" class="form-inline my-2 my-lg-0 d-flex justify-content-center">

                        
                        <div class="input-group mr-1">
                            <asp:TextBox ID="txtUser" data-toggle="tooltip"
                                autofocus
                                data-placement="bottom" title="Usuario incorrecto" runat="server"
                                placeholder="Nombre de usuario"
                                autocomplete="username"
                                CssClass="form-control xxx"></asp:TextBox>
                        </div>
                        <div class="input-group mr-1">
                            <asp:TextBox
                                ID="txtPass" data-toggle="tooltip" data-placement="bottom"
                                title="Password incorrecto" runat="server" placeholder="Contraseña"
                                autocomplete="current-password"
                                TextMode="Password" CssClass="form-control mt-1 mt-sm-0 xxx">
                            </asp:TextBox>
                        </div>

                        <asp:LinkButton
                            data-toggle="tooltip" data-placement="bottom" title="Tiene 5 Intentos"
                            OnClick="btnIngresar"
                            OnClientClick="procesarLogin(this)"
                            CssClass="btn btn-primary my-2 ml-1 mr-2"
                            ID="btnIng" runat="server" Text="ENTRAR">
                            <i class="fas fa-circle-notch"></i>
                            ENTRAR  <span class="badge badge-danger" hidden>
                                <asp:Label ID="lblCount" runat="server" Text="5"></asp:Label>
                            </span>
                        </asp:LinkButton>
                                          
                        <a class="btn text-warning btn-link" href="recuperar_password.aspx">¿Olvidó su contraseña?</a>
                   
                    </form>
                </div>
            </div>
        </nav>
    </header>

    <div class="content-wrapper d-flex flex-column justify-content-end">
        <main class="py-3 d-flex  flex-column align-items-center justify-content-center">
            <div class="card-deck">
                <div class="card">
                <%--    <div class="card-header text-left bg-info text-white" style="background-color: #fff!important;">
                        <h5 class="my-0 font-weight-normal" style="color: #0e6588;">
                                        ¡AVISO!
                        </h5>
                    </div>
                    <div class="card-body">
                        <img class="img-fluid rounded" src="img/avisos/AsistenciaSSP2.jpg" alt="Aviso" />
                    </div>
                    <div class="card-footer"></div>--%>
                </div>
            </div>
        </main>
        <footer>
            <div class="bg-dark-trans">
                <div class="container">
                    <div class="row py-2">
                        <div class="col-12 col-lg-2 d-flex flex-column align-items-center align-items-lg-start">
                            <img src="img/logo_master_page.png" height="46" alt="Logo MVCS" />
                        </div>
                        <div class="col-12 col-lg-8 d-flex flex-column align-items-center mt-2 mt-lg-0">
                            <p class="d-block my-auto text-light text-center">
                                © Todos los derechos reservados 2021<br />
                            </p>
                        </div>
                    <%--    <div class="col-12 col-lg-2  d-flex flex-column align-items-center align-items-lg-end mt-2 mt-lg-0">
                            <a rel="noreferrer" href="https://validator.w3.org/check?uri=http://ssp.vivienda.gob.pe/login" target="_blank">
                                <img src="images/ssp-w3c.png" alt="ssp-w3c">
                            </a>
                        </div>--%>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="sources/jquery/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="sources/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="sources/waves/waves.min.js"></script>
    <script type="text/javascript">

        function procesarLogin(e) {
            var i = $(e).find('.fa-circle-notch');
            i.toggleClass('fa-spin');
            $(e).addClass('disabled')
        }

        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            
            auth2.signOut().then(function () {
                console.log('User signed out.');
                auth2.disconnect();
            });
        }

        window.onbeforeunload = function(e){
            signOut();
        };

        window.onload = function () {

            Waves.attach('.btn');
            Waves.init();

            $('#txtPass').on('keypress', function (e) {
                if (e.keyCode == 13) {
                    $("#btnIng").trigger('click')
                    __doPostBack('btnIng', '')
                }
            })

            $('[data-toggle="tooltip"]').tooltip()

            if ($('#lblCount').text() == "0") {
                $("#btnIng").attr('data-original-title', 'No tiene más intentos. Vuelva a intentarlo mas tarde.')
                $("#btnIng").addClass('disabled')
                $("#btnIng").tooltip('show')
            } else {
                $("#btnIng").attr('data-original-title', 'Tiene ' + $('#lblCount').text() + ' Intentos')
                $("#btnIng").removeClass('disabled')
            }

            var usuario = $('#txtUser')
            if (usuario.hasClass('is-invalid')) {
                usuario.tooltip('enable')
                usuario.tooltip('show')
            } else {
                usuario.tooltip('disable')
            }

            var password = $('#txtPass')
            if (password.hasClass('is-invalid')) {
                password.tooltip('enable')
                password.tooltip('show')
            } else {
                password.tooltip('disable')
            }

            //Habilitar obligatorio https solo en producción
            // if (location.protocol != 'https:') {
            //    location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
            //}

        }
    </script>


</body>
</html>


