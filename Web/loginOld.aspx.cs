﻿using Business;
using Entity;
using pry_san;
using seguridad;
using System;
using System.Data;
using System.Net;
using System.Web;
using System.Web.UI;

namespace Web
{
    public partial class loginOld : System.Web.UI.Page
    {
        mGeneral objMetodoGeneral = new mGeneral();
        mProyectos objMenu = new mProyectos();
        BLMenu _BLMenu = new BLMenu();
        BL_Administrador objAdmin = new BL_Administrador();
        BE_Administrador _BEAdmin = new BE_Administrador();
        BLUtil objUtil = new BLUtil();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt32(lblCount.Text) == 0)
            {
                //lblError.Text = "El número de intento fue mayor a 5, se bloqueó el acceso. Vuelva a intentarlo después.";
                //btnIng.Visible = false;
            }
            else
            {
                //id_proyecto = Convert.ToInt32(Session["id_proyecto"]);
                //objMetodoGeneral.setFlgEdicion("T_PROYECTO", "id_proyecto", id_proyecto, 0);
                Session.RemoveAll();
                //FormsAuthentication.SignOut();

                string fecha = DateTime.Parse(Convert.ToString(DateTime.Today)).ToLongDateString();
                string mensaje = Request.Params.Get("ms");

                //if (mensaje != null && mensaje.Length > 0)
                //{
                //    lbl_logout.Visible = true;
                //}

                //lblHora.Text = fecha.Substring(0, 1).ToUpper() + fecha.Substring(1, fecha.Length - 1);
                //txtUser.Focus();
            }
        }

        protected void btnIngresar(object sender, EventArgs e)
        {

            if (Convert.ToInt32(lblCount.Text) > 0)
            {
                acceso objAcceso = new acceso();
                BLLogin objAcceso1 = new BLLogin();
                BLUtil _Metodo = new BLUtil();

                string passw = objAcceso.Encriptar(txtPass.Text);

                DataTable dt = objAcceso1.sp_validarUsuario(txtUser.Text.Trim(), passw);
                //lblError.Text = "";
                if (dt.Rows.Count > 0)
                {
                    Ingresar(dt);
                }
                else
                {
                    dt = objAcceso1.sp_validarUsuarioLogin(txtUser.Text.Trim());

                    if (dt.Rows.Count > 0)
                    {
                        string IdOficina = dt.Rows[0]["iCodOficina"].ToString();

                        string pServerStar = "";
                        try
                        {
                            pServerStar = Dns.GetHostEntry(Request.ServerVariables["REMOTE_HOST"]).HostName;
                        }
                        catch (Exception ex)
                        {
                            objUtil.MailDeError(ex.ToString(), "Error en login al obtener Remote Host",false);
                        }

                        if (_Metodo.F_LoginUsuarioAD(txtUser.Text.Trim(), txtPass.Text, IdOficina, pServerStar) == true)
                        {
                            Ingresar(dt);
                        }
                        else
                        {
                            String clases = txtPass.CssClass;
                            txtPass.CssClass = clases.Replace("xxx", "is-invalid");
                            txtUser.CssClass = clases.Replace("is-invalid", "xxx");

                            String intentos = (Convert.ToInt32(lblCount.Text) - 1).ToString();
                            lblCount.Text = intentos;
                        }
                    }
                    else
                    {
                        String clases = txtUser.CssClass;
                        txtUser.CssClass = clases.Replace("xxx", "is-invalid");
                        txtPass.CssClass = clases.Replace("is-invalid", "xxx");

                        String intentos = (Convert.ToInt32(lblCount.Text) - 1).ToString();
                        lblCount.Text = intentos;
                    }
                }
            }
        }

        protected void Ingresar(DataTable dt)
        {
            string vUrlWebForm = System.Configuration.ConfigurationManager.AppSettings["UrlWebForm"].ToString();
            string vUrlActual = HttpContext.Current.Request.Url.AbsoluteUri;

            if (vUrlActual.Contains("localhost"))
            {
                vUrlWebForm = "";
            }

            acceso objAcceso = new acceso();
            BLLogin objAcceso1 = new BLLogin();

            DataTableReader dr = dt.CreateDataReader();

            while (dr.Read())
            {
                Session["IdUsuario"] = dr["id_usuario"];
                Session["LoginUsuario"] = dr["login"];
                Session["NombreUsuario"] = dr["nombre"];
                Session["Correo"] = dr["correo"];
                Session["CodTipoUsuario"] = dr["cod_tipo_usuario"];
                Session["CodSector"] = dr["cod_entidad"];
                Session["CodSubsector"] = dr["cod_subsector"];
                Session["NombreOficina"] = dr["cod_programa"];
                Session["Region"] = dr["nom_depa"];
                Session["CodRegion"] = dr["cod_depa"];
                Session["CodProv"] = dr["cod_prov"];
                Session["CodNivelAcceso"] = dr["cod_nivel_acceso"];
                Session["PerfilUsuario"] = dr["id_perfil_usuario"];
                Session["idTecnicoEstudio"] = dr["idTecnico"];
                Session["DNI"] = dr["vDNI"];
                //FormsAuthentication.Initialize();
                //FormsAuthentication.RedirectFromLoginPage(txtUser.Text, false);

                // INICIO SESION TOKEN
                string token = dr["id_usuario"].ToString() + DateTime.Now.ToString("yyyyMMdd");
                token = objAcceso.Encriptar(token);

                int val = objAcceso1.I_iSession(token, Convert.ToInt32(dr["id_usuario"].ToString()));
                Session["clave"] = token;
                // FIN SESION TOKEN

                DataSet ds = _BLMenu.sp_ListarMenusxUsuario(Convert.ToInt32(dr["id_usuario"]));
                Session["OpcionesMenu"] = ds;

                string ruta = "";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        string titulo1 = row["subtitulo"].ToString();
                        if (titulo1.Equals("Consulta de Monitoreo"))
                        {
                            ruta = row["pagina"].ToString();
                            Context.ApplicationInstance.CompleteRequest();
                            Response.Redirect(vUrlWebForm + ruta, true);
                            break;
                        }
                    }
                    ////Supervisor Externo
                    //if (Session["PerfilUsuario"].ToString().Equals("82") && Session["NombreOficina"].ToString().Equals("EXTERNO"))
                    //{
                    //    ruta = "/Monitor/Bandeja_Monitoreo";
                    //}
                    //else
                    //{
                    //    ruta = "Solicitudes/inicio";
                    //}

                    ruta = "Solicitudes/inicio";

                    Response.Redirect(vUrlWebForm + ruta, true);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    //lblError.Text = "No tiene acceso.";
                }
            }
            dr.Close();
            dr.Dispose();
        }
      
        protected void Page_Init(object sender, EventArgs e)
        {

            if (Session.IsNewSession)
            {
                // Force session to be created;
                // otherwise the session ID changes on every request.
                Session["ForceSession"] = DateTime.Now;
            }
            // 'Sign' the viewstate with the current session.
            this.ViewStateUserKey = Session.SessionID;
            if (Page.EnableViewState)
            {
                // Make sure ViewState wasn't passed on the querystring.
                // This helps prevent one-click attacks.
                if (!string.IsNullOrEmpty(Request.Params["__VIEWSTATE"]) &&
                  string.IsNullOrEmpty(Request.Form["__VIEWSTATE"]))
                {
                    throw new Exception("Viewstate existed, but not on the form.");
                }
            }
        }
    }
}