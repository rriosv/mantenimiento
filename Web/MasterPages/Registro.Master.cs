﻿using System;
using System.Web.UI;
using Business;
using System.Data;
using System.Web;
using System.Text.RegularExpressions;
using System.Net;
using Entity;

namespace Web.MasterPages
{
    public partial class Registro : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {
                try
                {
                    string token = Request.QueryString["token"].ToString();

                    BLLogin _obj = new BLLogin();
                    DataTable dt = new DataTable();

                    dt = _obj.paSSP_rObtenerSession(token);

                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];

                        Session["IdUsuario"] = dr["xidIdUsuario"];
                        Session["LoginUsuario"] = dr["vLogin"];
                        Session["NombreUsuario"] = dr["vNombreUsuario"];
                        Session["Correo"] = dr["vCorreo"];
                        Session["CodTipoUsuario"] = dr["xIdCodTipoUsuario"];
                        Session["CodSector"] = dr["xidCodEntidad"];
                        Session["CodSubsector"] = dr["xidTipoPrograma"];
                        Session["NombreOficina"] = dr["vNombreOficina"];
                        Session["Region"] = dr["vNombreDepartamento"];
                        Session["CodRegion"] = dr["xidCodDepa"];
                        Session["CodProv"] = dr["xidCodPRov"];
                        Session["CodNivelAcceso"] = dr["xidCodNivelAcceso"];
                        Session["PerfilUsuario"] = dr["xidIdPerfilUsuario"];
                        Session["idTecnicoEstudio"] = dr["xidIdTecnicoCalidad"];
                        Session["DNI"] = dr["vDNI"];

                        Session["clave"] = token;
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                        Response.Redirect("~/login?ms=1");
                    }

                }
                catch
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                    Response.Redirect("~/login?ms=1",true);
                }


            }

            Session.Timeout = 360;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Grabar_Log();
            }
        }

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip = "";
            string hostName = "";
            string pagina = "";
            string tipoDispositivo = "";
            string agente = "";

            try
            {
                ip = (Request.ServerVariables["REMOTE_ADDR"] == null ? "" : Request.ServerVariables["REMOTE_ADDR"].ToString());
                hostName = (Request.ServerVariables["remote_addr"] == null ? "" : (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName));
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;

                if (Request.ServerVariables["HTTP_USER_AGENT"] != null)
                {
                    string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                    agente = uAg;
                    Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                    bool isMobile = regEx.IsMatch(uAg);
                    if (isMobile)
                    {
                        tipoDispositivo = "Movil";
                    }
                    else if (Request.Browser.IsMobileDevice)
                    {
                        tipoDispositivo = "Movil";
                    }
                    else
                    {
                        tipoDispositivo = "PC";
                    }
                }

                if (Session["IdUsuario"] != null)
                {
                    _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
                }
                else
                {
                    _BELog.Id_usuario = 0;
                }

            }
            catch (Exception ex)
            {
                agente = ex.Message.ToString();
            }
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }
    }
}