﻿using Business;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
namespace Web.MasterPages
{
    public partial class Menu : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {
                Response.Redirect("~/login?ms=1", true);
                //Context.ApplicationInstance.CompleteRequest();
            }
            Session.Timeout = 180;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string nomusu = Session["NombreUsuario"].ToString();
            //string desSubsector = Session["NombreOficina"].ToString();

            lblUser.Text = nomusu;
            GenerarMenu();
        }

        public string limpiarTexto(string txtOriginal)
        {
            string textResultado = Regex.Replace(txtOriginal, @"[^a-zA-z0-9 ]+", "");
            textResultado = textResultado.Replace(" ", "");
            return textResultado;
        }

        public void GenerarMenu()
        {
            int id_usuario = Convert.ToInt32(Session["IdUsuario"]);
            //int id_usuario = 481;

            if (Session["vHtmlMenu"] == null)
            {
                List<BEMenu> ListMenu = new List<BEMenu>();
                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();
                DataTable dt3 = new DataTable();

                DataTable dtAccesos = new DataTable();

                BLMenu obj = new BLMenu();
                ds = obj.sp_ListarMenusxUsuarioNew(id_usuario);

                StringBuilder strHtml = new StringBuilder();

                strHtml.Append("<ul class='nav nav-pills nav-sidebar flex-column' data-widget='treeview' role='menu' data-accordion='false'>");

                dt1 = ds.Tables[0];
                foreach (DataRow row1 in dt1.Rows)
                {
                    string pagina1 = row1["pagina"].ToString();
                    string titulo1 = row1["titulo"].ToString();
                    string target1 = "";
                    string FontIcon1 = row1["vFontIcon"].ToString();
                    if (!(pagina1.Equals("0")))
                    {
                        if (pagina1.Substring(0, 4).Equals("http"))
                        {
                            target1 = "_blank";
                        }
                        else
                        {
                            pagina1 = "../" + pagina1;
                        }

                        // NIVEL 1
                        strHtml.Append("<li class='nav-item'>" +
                            "<a href='" + pagina1.Trim() + "' target='" + target1 + "' class='textMenu nav-link' >" +
                            "<i class='nav-icon " + FontIcon1 + "'></i> " +
                            "<p>" + Convert.ToString(titulo1).Trim() + "</p>" +
                            "</a>" +
                            "</li>");

                    }
                    else // AGREGANDO A NIVEL 2
                    {
                        // Se agregá cabecera sin link 
                        //strHtml.Append("<li><a href = '#" + limpiarTexto(titulo1) + "' data-toggle='collapse' aria-expanded='false' class='textMenu'><i class='" + FontIcon1 + "'></i>&nbsp;" + Convert.ToString(titulo1).Trim() + "</a>");
                        //strHtml.Append("<ul class='collapse list-unstyled nivel1' id='" + limpiarTexto(titulo1) + "'>");

                        strHtml.Append("<li class='nav-item'>" +
                            "<a href = '#" + limpiarTexto(titulo1) + "' class='textMenu nav-link' >" +
                            "<i class='nav-icon " + FontIcon1 + "'></i> " +
                            "<p>" + Convert.ToString(titulo1).Trim() +
                            "<i class='right fas fa-angle-left'></i>" +
                            "</p> " +
                            "</a>");
                        strHtml.Append("<ul class='nav nav-treeview nivel1' id='" + limpiarTexto(titulo1) + "'>");
                        //Se llama a la rutina que Agrega los Hijos
                        dt2 = ds.Tables[1];
                        string expression = "titulo = '" + titulo1 + "'";

                        var dr = dt2.Select(expression);

                        foreach (DataRow row2 in dr)
                        {
                            string pagina2 = row2["pagina"].ToString();
                            string subtitulo2 = row2["subtitulo"].ToString();
                            string target2 = "";
                            string FontIcon2 = row2["vFontIcon"].ToString();
                            if (!(pagina2.Equals("0")))
                            {
                                if (pagina2.Substring(0, 4).Equals("http"))
                                {
                                    target2 = "_blank";

                                }
                                else
                                {
                                    pagina2 = "../" + pagina2;
                                }

                                strHtml.Append("<li class='nav-item'>" +
                                    "<a href = '" + pagina2.Trim() + "' target='" + target2 + "' class='subTextMenu nav-link'>" +
                                    "<i class='nav-icon " + FontIcon2 + "'></i>" +
                                        //"&nbsp;" + Convert.ToString(subtitulo2).Trim() +
                                        "<p>" + Convert.ToString(subtitulo2).Trim() + "</p>" +
                                    "</a></li>");

                            }
                            else // AGREGANDO A NIVEL 3
                            {
                                // Se agregá cabecera sin link
                                //strHtml.Append("<li><a href = '#" + limpiarTexto(subtitulo2 + titulo1) + "' class='textMenu'><i class='" + FontIcon2 + "'></i>&nbsp;" + Convert.ToString(subtitulo2).Trim() + "</a>");
                                //strHtml.Append("<ul class='collapse list-unstyled nivel2' id='" + limpiarTexto(subtitulo2 + titulo1) + "'>");

                                strHtml.Append("<li class='nav-item'>" +
                                    "<a href = '#" + limpiarTexto(subtitulo2 + titulo1) + "' class='textMenu nav-link' >" +
                                    "<i class='nav-icon " + FontIcon2 + "'></i> " +
                                    "<p>" + Convert.ToString(subtitulo2).Trim() +
                                    "<i class='right fas fa-angle-left'></i>" +
                                    "</p> " +
                                    "</a>");
                                strHtml.Append("<ul class='nav nav-treeview nivel1' id='" + limpiarTexto(subtitulo2 + titulo1) + "'>");
                                //strHtml.Append("<li class='treeview'><a href = '#" + limpiarTexto(subtitulo2 + titulo1) + "' class='textMenu' ><i class='" + FontIcon2 + "'></i> <span>" + Convert.ToString(subtitulo2).Trim() + "</span> <span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>");
                                //strHtml.Append("<ul class='treeview-menu' id='" + limpiarTexto(subtitulo2 + titulo1) + "'>");

                                dt3 = ds.Tables[2];
                                var dr3 = dt3.Select("titulo ='" + titulo1 + "' and subtitulo='" + subtitulo2 + "'");
                                foreach (DataRow row3 in dr3)
                                {
                                    string pagina3 = row3["pagina"].ToString();
                                    string subtitulo3 = row3["subtitulo2"].ToString();
                                    string target3 = "";
                                    string FontIcon3 = row3["vFontIcon"].ToString();
                                    if (pagina3.Substring(0, 4).Equals("http"))
                                    {
                                        target3 = "_blank";
                                    }
                                    else
                                    {
                                        pagina3 = "../" + pagina3;
                                    }

                                    //strHtml.Append("<li><a href = '" + pagina3.Trim() + "' target='" + target3 + "' class='subTextMenu'><i class='" + FontIcon2 + "'></i>&nbsp;" + Convert.ToString(subtitulo3).Trim() + "</a></li>");

                                    strHtml.Append("<li class='nav-item'>" +
                                    "<a href = '" + pagina3.Trim() + "' target='" + target3 + "' class='subTextMenu nav-link'>" +
                                    "<i class='nav-icon " + FontIcon2 + "'></i>" +
                                        "<p>" + Convert.ToString(subtitulo3).Trim() + "</p>" +
                                    "</a></li>");

                                }
                                strHtml.Append("</ul></li>");
                            }
                        }
                        strHtml.Append("</ul></li>");
                    }
                }

                strHtml.Append("</ul>");
                Session["vHtmlMenu"] = strHtml.ToString();

                dtAccesos = ds.Tables[3];

                Session["tblAccesos"] = dtAccesos;
            }

            string val = Session["vHtmlMenu"].ToString();
            idMenu.InnerHtml = (val);
        }

    }
}