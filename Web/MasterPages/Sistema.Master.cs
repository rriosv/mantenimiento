﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Web.MasterPages
{
    public partial class Sistema : System.Web.UI.MasterPage
    {
       

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {
                Response.Redirect("~/login?ms=1", true);
                //Context.ApplicationInstance.CompleteRequest();
            }
            Session.Timeout = 180;

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            string nomusu = Session["NombreUsuario"].ToString();
            string desSubsector = Session["NombreOficina"].ToString();

            lblUser.Text = nomusu;
            GenerarMenu();

            //verificarAcceso();
            if (!IsPostBack)
            {
                Grabar_Log();
            }
        }

        public string limpiarTexto(string txtOriginal) {
            string textResultado = Regex.Replace(txtOriginal, @"[^a-zA-z0-9 ]+", "");
            textResultado = textResultado.Replace(" ", "");
            return textResultado;
        }

        public void GenerarMenu()
        {
            int id_usuario = Convert.ToInt32(Session["IdUsuario"]);
           
            if (Session["vHtmlMenu"] == null)
            {
                List<BEMenu> ListMenu = new List<BEMenu>();
                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();
                DataTable dt3 = new DataTable();

                DataTable dtAccesos = new DataTable();

                BLMenu obj = new BLMenu();
                ds = obj.sp_ListarMenusxUsuario(id_usuario);

                StringBuilder strHtml = new StringBuilder();
                
                strHtml.Append("<ul class='sidebar-menu' data-widget='tree'><li class='header'>MENÚ</li>");

                dt1 = ds.Tables[0];
                foreach (DataRow row1 in dt1.Rows)
                {
                    string pagina1 = row1["pagina"].ToString();
                    string titulo1 = row1["titulo"].ToString();
                    string target1 = "";
                    string FontIcon1 = row1["vFontIcon"].ToString();
                    if (!(pagina1.Equals("0")))
                    {
                        if (pagina1.Substring(0, 4).Equals("http"))
                        {
                            target1 = "_blank";
                        }
                        else
                        {
                            pagina1 = "../" + pagina1;
                        }
                        
                        // NIVEL 1
                        strHtml.Append("<li><a href='"+ pagina1.Trim() + "' target='" + target1 + "' class='textMenu' ><i class='" + FontIcon1 + "'></i> <span>" + Convert.ToString(titulo1).Trim()+"</span></a></li>");
                        //strHtml.Append("<li><a href = '" + pagina1.Trim() + "' target='" + target1 + "' class='textMenu'><i class='" + FontIcon1 + "' ></i>&nbsp;" + Convert.ToString(titulo1).Trim() + "</a></li>");

                    }
                    else // AGREGANDO A NIVEL 2
                    {
                        // Se agregá cabecera sin link 
                        //strHtml.Append("<li><a href = '#" + limpiarTexto(titulo1) + "' data-toggle='collapse' aria-expanded='false' class='textMenu'><i class='" + FontIcon1 + "'></i>&nbsp;" + Convert.ToString(titulo1).Trim() + "</a>");
                        //strHtml.Append("<ul class='collapse list-unstyled nivel1' id='" + limpiarTexto(titulo1) + "'>");

                        strHtml.Append("<li class='treeview'><a href = '#" + limpiarTexto(titulo1) + "' class='textMenu' ><i class='" + FontIcon1 + "'></i> <span>" + Convert.ToString(titulo1).Trim() + "</span> <span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>");
                        strHtml.Append("<ul class='treeview-menu nivel1' id='" + limpiarTexto(titulo1) + "'>");
                        //Se llama a la rutina que Agrega los Hijos
                        dt2 = ds.Tables[1];
                        string expression = "titulo = '" + titulo1 + "'";

                        var dr = dt2.Select(expression);

                        foreach (DataRow row2 in dr)
                        {
                            string pagina2 = row2["pagina"].ToString();
                            string subtitulo2 = row2["subtitulo"].ToString();
                            string target2 = "";
                            string FontIcon2 = row2["vFontIcon"].ToString();
                            if (!(pagina2.Equals("0")))
                            {
                                if (pagina2.Substring(0, 4).Equals("http"))
                                {
                                    target2 = "_blank";
                                                                        
                                }
                                else
                                {
                                    pagina2 = "../" + pagina2;
                                }

                                strHtml.Append("<li><a href = '" + pagina2.Trim() + "' target='" + target2 + "' class='subTextMenu'><i class='" + FontIcon2 + "'></i>&nbsp;" + Convert.ToString(subtitulo2).Trim() + " </a></li>");
                               
                            }
                            else // AGREGANDO A NIVEL 3
                            {
                                // Se agregá cabecera sin link
                                //strHtml.Append("<li><a href = '#" + limpiarTexto(subtitulo2 + titulo1) + "' class='textMenu'><i class='" + FontIcon2 + "'></i>&nbsp;" + Convert.ToString(subtitulo2).Trim() + "</a>");
                                //strHtml.Append("<ul class='collapse list-unstyled nivel2' id='" + limpiarTexto(subtitulo2 + titulo1) + "'>");

                                strHtml.Append("<li class='treeview'><a href = '#" + limpiarTexto(subtitulo2 + titulo1) + "' class='textMenu' ><i class='" + FontIcon2 + "'></i> <span>" + Convert.ToString(subtitulo2).Trim() + "</span> <span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>");
                                strHtml.Append("<ul class='treeview-menu' id='" + limpiarTexto(subtitulo2+ titulo1) + "'>");

                                dt3 = ds.Tables[2];
                                var dr3 = dt3.Select("titulo ='" + titulo1 + "' and subtitulo='" + subtitulo2 + "'");
                                foreach (DataRow row3 in dr3)
                                {
                                    string pagina3 = row3["pagina"].ToString();
                                    string subtitulo3 = row3["subtitulo2"].ToString();
                                    string target3 = "";
                                    string FontIcon3 = row3["vFontIcon"].ToString();
                                    if (pagina3.Substring(0, 4).Equals("http"))
                                    {
                                        target3 = "_blank";
                                    }
                                    else
                                    {
                                        pagina3 = "../" + pagina3;
                                    }

                                    strHtml.Append("<li><a href = '" + pagina3.Trim() + "' target='" + target3 + "' class='subTextMenu'><i class='" + FontIcon2 + "'></i>&nbsp;" + Convert.ToString(subtitulo3).Trim() + "</a></li>");

                                }
                                strHtml.Append("</ul></li>");
                            }
                        }
                        strHtml.Append("</ul></li>");
                    }
                }

                strHtml.Append("</ul>");
                Session["vHtmlMenu"] = strHtml.ToString();

                dtAccesos = ds.Tables[3];

                Session["tblAccesos"] = dtAccesos;
            }

            string val = Session["vHtmlMenu"].ToString();
            idMenu.InnerHtml = (val);
        }

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }

        public void verificarAcceso() {

            DataTable dtAccesos = new DataTable();
            dtAccesos = (DataTable)(Session["tblAccesos"]);

            string cadena = HttpContext.Current.Request.Url.AbsoluteUri;
            string[] Separado = cadena.Split('/');
            string pagina = Separado[Separado.Length - 1];

            DataRow[] filtDT = dtAccesos.Select("pagina like  '%" + pagina + "%'");

            if (filtDT.Length == 0)
            {
                Response.Redirect("~/Pagina403?p="+pagina, true);
            }
           
        }
    }
}