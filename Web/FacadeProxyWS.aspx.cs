﻿using Newtonsoft.Json;
using Business;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;

namespace Web
{
    public partial class FacadeProxyWS : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //[System.Web.Services.WebMethod]
        //public static string invoke(string idEvaluacion, string idCriterio)
        //{
        //    ConsultarPNSU _objPNSU = new ConsultarPNSU();

        //    int pidEvaluacion, pidCriterio;
        //    string vResult = "";
        //    int.TryParse(idEvaluacion, out pidEvaluacion);
        //    int.TryParse(idCriterio, out pidCriterio);
        //    return vResult = _objPNSU.FichaEvaluacionDetalle2(pidEvaluacion, pidCriterio);
        //}

        [System.Web.Services.WebMethod]
        public static string invokeRiesgo(string idProyecto)
        {
            BL_MON_Riesgo _objRiesgo = new BL_MON_Riesgo();

            string vResult = "";
            int pidProyecto;
            int.TryParse(idProyecto, out pidProyecto);
            DataTable dt = _objRiesgo.spMON_RiesgosChart(pidProyecto);
            vResult = JsonConvert.SerializeObject(dt);
            return vResult;
        }

        [System.Web.Services.WebMethod]
        public static string invokeGetExterno(string url)
        {
            String responseString = "";
            WebRequest request1;
            try
            {
                request1 = WebRequest.Create(url);
                request1.Method = "GET";
                WebResponse response1 = request1.GetResponse();
                using (Stream stream = response1.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("{0} Exception caught.", ex);
                responseString = "";
            }
            return responseString;
        }
    }
}