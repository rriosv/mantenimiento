function abrirVentana(url,nombre,w,h,toolbar,scrol,menubars,statusbar,status,resizable,location)
{
	if (window.screen.width) {
		izq=(window.screen.availWidth-w)/2;
		arriba=(window.screen.availHeight-h)/2;
		ventana = window.open(url,nombre,"width="+w+",height="+h+",left="+izq+",top="+arriba+",toolbar="+toolbar+",scrollbars="+scrol+",menubars="+menubars+",statusbar="+statusbar+",status="+status+",resizable="+resizable+",location="+location);
	} else {
		ventana =  window.open(url,nombre,"width="+w+",height="+h+",toolbar=no,scrollbars="+scrol);		
	}
 return ventana;
}

function cerrarVentana()
{
  if (confirm ('Desea actualizar la informaci�n contenida?'))
    window.opener.location.reload(true);
  window.close();
}

/****solicitudes***/
function cerrarventanachecklist(){
   if(!confirm('Desea actualizar la informaci�n?'))return false
}

function cerrar()
{

  window.opener.location.reload(true);
  window.close();
}

function abrirVentanaModal(url,nombre,w,h,status,resizable)
{
  if (window.screen.width) {
	izq=(window.screen.availWidth-w)/2;
	arriba=(window.screen.availHeight-h)/2;
	ventanaModal = window.showModalDialog(url,nombre,"dialogWidth:"+w+"px;dialogHeight:"+h+"px;dialogLeft:"+izq+"px; dialogTop:"+arriba+"px;edge: Raised; center: Yes;resizable:"+resizable+"; status:"+status+";");
  } else {
	ventanaModal = window.showModalDialog(url,nombre,"dialogWidth:"+w+"px;dialogHeight:"+h+"px;edge: Raised; center: Yes;resizable:"+resizable+"; status:"+status+";");
  }
}
//****************************************
// Funcion para mostrar im�genes ampliadas
//****************************************
function ampliarImagen(url) {
  img = new Image();
  img.src = url;
  w = img.width + 0;
  h = img.height + 0;
  var ventana = abrirVentana(url,"foto_ampliada",w,h,"no","no","no","no","no","no","no")
  ventana.document.title = "Imagen Ampliada";
}
//******************************
// Funcion para abrir calendario
//******************************
function ShowCalendario(obj1,obj2)
{
  /*  El  primer objeto es obligatorio para que funcione el aplicativo */
  var Fecha   = document.getElementById(obj1).value;
  Fecha = window.showModalDialog("../calendario/Calendario.htm", Fecha, "dialogHeight: 210px; dialogWidth: 317px; edge: Raised; center:yes; help:no; resizable:no; status:no; scroll:no;");
  if (typeof(Fecha) != "undefined") {
	document.getElementById(obj1).value = Fecha;
	if (obj2) document.getElementById(obj2).value = Fecha;
	  return 1;
	}
  else
	return 0;
}

function check_uncheck (obj,chkbAll,chkb) 
{ 
  var ValChecked = obj.checked;
  var ValId =obj.id;
  var frm = document.forms[0];
  // Loop atraves de todos los elementos
  for (i=0; i<frm.length; i++)
  {
    // Look for Header Template's Checkbox
    if (this!=null)
    {
      if (ValId.indexOf(chkbAll) != -1) 
      {
        // Check if main checkbox is checked, then select or deselect datagrid checkboxes 
        if(ValChecked)
        {
          // solo actualiza a los checkbox que contengan el id 'deleteRec'
          if (frm.elements[i].name.indexOf (chkb) != -1)
            frm.elements[i].checked = true;
        }
        else
        {
          // solo actualiza a los checkbox que contengan el id 'deleteRec'
          if (frm.elements[i].name.indexOf (chkb) != -1)
            frm.elements[i].checked = false;
        }
      } 
      else if (ValId.indexOf (chkb) != -1) 
      {
        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
        if(frm.elements[i].checked == false)
          frm.elements[1].checked = false;
      }
    }
  }
}
function confirmMsg (frm) 
{ 
  for (i=0; i<frm.length; i++) 
  {
    if (frm.elements[i].name.indexOf("deleteRec") !=-1) 
    {
      if(frm.elements[i].checked) return confirm ('Esta seguro de querer borrar estos Ubigeos?')
    }
  }
}
function sacaSubtitulo(id)
{
  var subt=document.getElementById(id).innerHTML;
  menuprincipal.setSessionSubTitulo(subt, callback_setSessionSubTitulo);
}
function callback_setSessionSubTitulo(res){
  if (res == null)
  {
	alert(res.error);
	return;
  }
}
function CambiaColorCelda(id,op)
{
  if(op==true) {
   id.style.backgroundImage="url('../img/marca.gif')";
  } else {
    id.style.backgroundImage="none";
  }
}
function imprime()
{
    document.all.item("botones").style.visibility="hidden";
	//factory.printing.printBackground = true;
	factory.printing.portrait = true;
	factory.printing.header = "" 
	factory.printing.footer = "";
	//factory.printing.footer = "MVCS&bP�gina &p de &P";
	factory.printing.topMargin = 10;
	factory.printing.bottomMargin = 10;
	factory.printing.leftMargin = 15;
	factory.printing.rightMargin = 15;
	factory.printing.Print(true);
	//window.print()
	document.all.item("botones").style.visibility="visible";
}

//metodos para mostrar y ocultar mensaje de confirmacion
  function fMostrarConfirmacion()
  {     
//        var intAnchoBody = parseInt(hoja.style.width);
//        var intAltoBody =  parseInt(hoja.style.height);
        
        if(divConfirmacion.style.display != "block")
        {       
//                divConfirmacion.style.width = intAnchoBody;
//                divConfirmacion.style.height = intAltoBody;
                divConfirmacion.style.top = 0;
                divConfirmacion.style.left = 0;
//                divMsj.style.top=((parseInt(intAltoBody)/2)-(parseInt(divMsj.style.height)/2));
//                divMsj.style.left=((parseInt(intAnchoBody)/2)-(parseInt(divMsj.style.width)/2));

                divConfirmacion.style.display = "block";                
        }
        else
        {                
                divConfirmacion.style.display = "none";    
        }        
 }
 
 function ocultarConfirmacion()
{
    divConfirmacion.style.display = "none"; 
}
    function IrLogin()
    {
            alert("PRUEBA");
            window.location="../login.aspx";
    }

///////////////SOLO NUMEROS/////////////
function numero(evt) {
    var keyPressed = (evt.which) ? evt.which : event.keyCode
    return !(keyPressed > 31 && (keyPressed < 48 || keyPressed > 57));
}
///SOLO PERMITE NUMEROS Y PUNTO/////
function numero_punto(evt) {
    var keyPressed = (evt.which) ? evt.which : event.keyCode
    return !(keyPressed > 31 && (keyPressed < 46 || keyPressed == 47 || keyPressed > 57));
}

//////CARLUAR RATIO////
function ratio(m){
    var po = document.getElementById('txtpoblacion').value;
    var ratio;
    if (po=='') po='0';
    
    if (po=='0') ratio=0;
    else 
    ratio = parseFloat(m)/parseInt(po);
    
    document.getElementById('txtratio').value = ratio;
    
}

function setHeight(txtdesc) {
   
   txtdesc.style.height = txtdesc.scrollHeight + "px";
}