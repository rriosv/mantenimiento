﻿$(function () {

    fakewaffle.responsiveTabs(['xs', 'sm']);

    var $inicioHeader = $('#inicioProyectoHeader')
        , $allTabs = $('#tabsEvaluador')
        , $disabledTabs = $allTabs.find('li.disabled')
        , $enabledTabs = $allTabs.find('li:not(.disabled)')
        , cantEnableds = $enabledTabs.length
        , $btnBack = $('#btnBack')
        , $btnNext = $('#btnNext')
        , $actionButton = $('.box-footer').find('.action-button')
        , $btnAdmNoAdm = $('#btnAdmisible, #btnNoAdmisible')
        , cursorTab = 0  // Podria ser variable
        , $firstTab = $('[data-toggle="tab"]:eq(0)')

        , IS_CHROME = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase())

        , getTabContent = function (sHeaders, sRows) {
            return '<div class="table-responsive">\
                        <table class="table table-bordered table-condensed">\
                            <thead>\
                                <tr class="bolder text-center">' + sHeaders + '</tr>\
                            </thead>\
                            <tbody><!--br /-->' + sRows + '</tbody>\
                        </table>\
                    </div>';
        }
        , getParam = function (key) {
            // Grab and unescape the query string - appending an '&' keeps the RegExp simple
            // for the sake of this example.
            var queryStr = unescape(PNSU.queryString) + '&';
            var regex = new RegExp('.*?[&\\?]' + key + '=(.*?)&.*');// Dynamic replacement RegExp
            val = queryStr.replace(regex, "$1");// Apply RegExp to the query string
            // If the string is the same, we didn't find a match - return false
            return val == queryStr ? false : val;
        }
        , fncShowMsg = function (sMsg, fCallbackIsOk, bShowMsgOk) {

            if ($.trim(sMsg).toUpperCase() === 'OK') {
                if (fCallbackIsOk) { fCallbackIsOk(); }
                if (!bShowMsgOk) { return; }
                $("#alert-success").show().find('#paragraph-message').html('Se guardo correctamente !!!');
                $('html, body').animate({ scrollTop: $("#alert-success").offset().top }, 600);
                return;
            }

            // Si no es OK
            var aMsg = sMsg.split(':')
                , sFlag = aMsg[0]
                , $divWarn = $("#warning-error")
                , $divErr = $("#alert-error")
                , $divMsg = $divWarn
                , msgMesaAyuda = ""
                ;

            // Eliminando el primer elemento del array de msgs
            aMsg.splice(0, 1);

            if (sFlag.toUpperCase() === 'ER') {
                msgMesaAyuda = "<p><b>" + PNSU.msgMesaAyuda + "</b></p>";
                $divMsg = $divErr.css('display', 'block');
            }

            // Para RN o como Default (Cualquier cadena o nada)
            //log("win.PNSU.extra->submitModal:", $divMsg.show().find('#paragraph-message'), aMsg.join(':'));
            $divMsg.show().find('#paragraph-message').html(aMsg.join(':') + msgMesaAyuda); // Mostrando el msg

            $('html, body').animate({ scrollTop: $divMsg.offset().top }, 600);
        }

        /*
            formatNumber.new(123456779.18, "$") // retorna "$123.456.779,18"
            formatNumber.new(123456779.18) // retorna "123.456.779,18"
            formatNumber.new(123456779) // retorna "$123.456.779"
        */
        , formatNumber = {
            separador: ",", // separador para los miles
            sepDecimal: '.', // separador para los decimales
            formatear: function (num) {
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                }
                return this.simbol + splitLeft + splitRight;
            },
            new: function (num, simbol) {
                this.simbol = simbol || '';
                return this.formatear(num);
            }
        }

        , IS_ELEGIBILIDAD = (getParam('xidTipoEval') == '484')
        ;

    // FUncionalidad Select Historial
    $("#evalHist").on('change', function () {
        var $opt = $(this).find('option:selected');
        var qStr = $opt.attr('data-qstring');
        location.href = "?" + $opt.attr('data-qstring');
    });

    // Link de cabecera ver mas
    $('#contentShowMore').on('click', function () {
        var $this = $(this), thisText = $.trim($this.text());
        $('#tblHeaderMore').slideToggle('slow', function () {
            $this.html((thisText == 'Ver más...') ? 'Ocultar' : 'Ver más...');
        });
    });

    // Mostrando el tab por defecto (explicito)
    $allTabs.find('li:not(.disabled):eq(' + cursorTab + ')').find('a').tab('show');

    // Habilitando o deshabilitando BackButon (inicializando)
    $btnBack[cursorTab ? 'removeClass' : 'addClass']('disabled');

    // Bloqueando contenido de Tabs disabled
    // $allTabs.on("click", function (e) { e.preventDefault(); return false; });

    // Evento al mostrar un Tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(".list-group li").removeClass('active'); // Bug-fix : Quitando estilo active (se ve mal) 
        var sDataExtra = $.trim($(e.target).attr('data-extra')) //log("shown.bs.tab->sDataExtra:", sDataExtra)
            , oDataExtra = (new Function(' return ' + sDataExtra + ';'))()
            , $contentTab = $($.trim($(e.target).attr('href')))

            , innerTextTab = $.trim($(e.target).text())

            , isCEP = (innerTextTab === 'CEP - Rural' || innerTextTab === 'CEP - EPS' || innerTextTab === 'CEP - Pequeñas Ciudades') // Para agregar columna de puntajes y Row de totales
            ;

        // Estableciendo cursor tab
        cursorTab = parseInt($.trim($(e.target).attr('data-cursor')));
        //log("now-prev", e.target, e.relatedTarget);

        $.ajax({
            url: "../FacadeProxyWS.aspx/invoke", // listaCriteriosEval ListaTipoDocumentoAprobar
            type: "POST",
            contentType: 'application/json',

            data: JSON.stringify({
                idEvaluacion: $('#xidEvaluacion').val(),
                idCriterio: oDataExtra.pkMaeCriterio,
            }),

            success: function (oRes) {
                var sHeaders = ""
                    , sAllTrs = "" // Almacena los TR's
                    , iCountF = 0  // contador de filas
                    , aTrHeader = ["<tr class='bolder text-center'>", [], '</tr>']

                    , oPkMaeCritCss = {
                        9: "width40per", // Componentes del proyecto -> Componentes
                        27: "width40per", // CA- EPS -> &nbsp;
                        33: "width40per", // Requisitos Mínimos -> &nbsp;
                        38: "width40per", // Requisitos Mínimos -> Estudios Basicos
                        50: "width30per", // Conclusiones -> Estudios
                        51: "width30per", // Conclusiones -> Intervencion del MVCS en los ultimos 05 años
                    }

                    , oPkCritParCss = {
                        // Creación/Instalación  |  Mejoramiento         |  Ampliación
                        "1-1": "width20per", "2-2": "width20per", "3-3": "width20per",
                        //Se Adjunta(SI|NO) 	 | Fecha Presupuesto     | Observaciones
                        "5-1": "width10per", "7-3": "width10per", "8-4": "width40per",
                        // | No corresponde
                        "9-3": "width10per",
                        //Se Adjunta(SI|NO) 	 | Observaciones
                        "69-1": "width10per", "8-3": "width60per",
                        // | Cantidad de Proyectos |      Monto          |    Observaciones
                        "10-3": "width10per", "11-4": "width20per", "8-5": "width60per"
                    }
                    ;
                oRes = (new Function(' return ' + oRes.d + ';'))()[0];
                //log("shown.bs.tab->success->oData:", oRes);
                oData = oRes["data"]["objects"]["table1"]["data"];
                //log("shown.bs.tab->success->oData:", oData);

                // Ejemplo data recibida
                // 10	Agua	  1	["1|Creación / Instalación|checkbox|0","2|Mejoramiento / Recuperación|checkbox|0","3|Ampliación|checkbox|0"]
                // 11	Alcant... 2	["1|Creación / Instalación|checkbox|0","2|Mejoramiento / Recuperación|checkbox|0","3|Ampliación|checkbox|0"]
                // Recorriendo x filas
                for (var f in oData) {

                    var // un objeto(row) de la respuesta
                        oRow = oData[f]
                        , pkMaeCriterio = oRow['pkMaeCriterio']
                        //Un array ["1|Text superior1|inpType|0", "2|Text superior2|inpType|1", "3|Text superior3|inpType|0"];
                        , aValsCriterios = (new Function(' return ' + oRow['vCriterios'] + ';'))()
                        , lengthCriterios = aValsCriterios.length
                        // Registro es nombre de padre
                        , bRowIsNameParent = ($.isArray(aValsCriterios) && !aValsCriterios.length)
                        , aTrBody = ["<tr class='text-center'>", [], "</tr>"]
                        , iCritPos = 0 // Posiscion de cada columna
                        , isSI_NO = false // Si tiene los campos "Se adjunta" o "Cumple" Si/No
                        ;

                    //log("TabEvent->ajax->success->bRowIsNameParent:", bRowIsNameParent);
                    // Si hay row con nombre de padre (Capturamos su nombre para crear su <tr />)
                    // !iCountF <> primer registro
                    if (bRowIsNameParent) {
                        aTrHeader[1].push(
                            "\n<td rowspan='2' class='text-left " + oPkMaeCritCss[pkMaeCriterio] + "' "
                            //+ (IS_ELEGIBILIDAD ? " colspan='15'" : "")
                            + (!IS_ELEGIBILIDAD ? ' style="width:30%;"' : ' colspan="3" ')
                            + ">" + oRow['vDescCriterio'] + "</td>"
                        );
                    } else {
                        // Si para este Row anteriormente se registro un NameParent
                        var bExistNameParent = (aTrHeader[1].length > 0)
                            , cantOpts = 0 // Cantidad de options para selects
                            , tplSel = ["<select class='form-control no-scroll' multiple><option class='not' value=''>Seleccionar</option>", "", "</select>"]
                            ;

                        if (IS_ELEGIBILIDAD) { tplSel = ["<div class='text-left" + (isCEP ? ' sel-points' : '') + "' >", "", "</div>"]; }

                        // Si no existe row anterior NameParent y es el primer registro (oRow) completamos primera columna (Nom Cat Padre vacio)
                        if (!bExistNameParent && !iCountF) {
                            aTrHeader[1].push("\n<td rowspan='2' class='" + (!IS_ELEGIBILIDAD ? oPkMaeCritCss[pkMaeCriterio] : '') + "'>&nbsp;</td>");
                        }

                        if (!IS_ELEGIBILIDAD)
                            aTrBody[1].push("\n<td class='text-justify'>" + oRow['vDescCriterio'] + "</td>");
                        else {
                            var vCriterio = oRow['vDescCriterio'];
                            var firstChar = vCriterio.charAt(0);
                            if (firstChar <= '9' && firstChar >= '0') {
                                aTrBody[1].push("\n<td class='text-left bolder'>" + oRow['vDescCriterio'] + "</td>");
                            }
                            else
                                aTrBody[1].push("\n<td class='text-left' style='padding-left: 20px;'>" + oRow['vDescCriterio'] + "</td>");
                        }


                        // Recorriendo x columnas 
                        for (var c in aValsCriterios) {

                            var aVals = aValsCriterios[c].split('|')
                                , pkCritPar = aVals[0]
                                , text = aVals[1]
                                , type = aVals[2]
                                , value = aVals[3]
                                , checked = ((type == 'checkbox' || type == 'radio') && value == '1') ? ' checked' : ' '
                                , isSelect = (type == 'select')
                                , isInpText = (type == 'text' || type == 'textarea')
                                , isValInt = /^(checkbox|radio|select)$/g.test(type)
                                , isChkRdo = /^(checkbox|radio)$/g.test(type)
                                , vValor = isValInt ? parseInt($.trim(value)) : '""' // Para type=text value inicial cadena vacia
                                , dataSend = '{"pkMaeCrit":' + oRow.pkMaeCriterio + ',"pkCritPar":' + pkCritPar + ',"vValor":' + vValor + '}'
                                , inpName = 'inp' + oRow['pkCriterioPadre'] + oRow['pkMaeCriterio']

                                //, sClass = 'inp-' + type + '-' + pkCritPar  // Clase especifica para un input type de un criterio especifico
                                , isObs = (text === 'Observaciones')
                                , isAdjSi = (pkCritPar == 5 || pkCritPar == 69)
                                , isAdjNo = (pkCritPar == 6 || pkCritPar == 70)
                                , isFecPres = (pkCritPar == 7)
                                , isCantProy = (pkCritPar == 10)
                                , sColSpan = isObs ? " colspan='15'" : (isAdjSi ? " colspan='2'" : "")
                                ;

                            if (isAdjSi || isAdjNo) { isSI_NO = true; }

                            isSelect ? (cantOpts++) : 0;
                            // 
                            // Si existe row anterior NameParent o primer registro adicionamos cabeceras
                            if (bExistNameParent || !iCountF) {
                                iCritPos++; // Posicion de la cabecera
                                // Nota: iCritPos==5 en la parte de observaciones de conclusiones
                                /*var classHeadName = isObs ? ( (iCritPos == 5) ? (IS_CHROME ? "width40per" : "width60per") : "width40per") : (isCantProy ? "width10per" : "")
                                    , sRowSpan = " rowspan='2'"
                                ;*/
                                var classHeadName = oPkCritParCss[pkCritPar + "-" + iCritPos], sRowSpan = " rowspan='2'";
                                if (isAdjSi) { /*classHeadName = "width10per";*/ text = "Se Adjunta"; sRowSpan = ""; }
                                // if (isFecPres) { classHeadName = "width10per"; }
                                if (!isAdjNo && !IS_ELEGIBILIDAD) {
                                    aTrHeader[1].push(
                                        "\n<td " + sRowSpan + " " + sColSpan + " class='text-center " + classHeadName + "'" + ">" //+ pkCritPar + "-" + iCritPos + ")"
                                        + (isSelect ? '' : text) +
                                        "</td>"
                                    );
                                }
                            }

                            // Si es select
                            if (isSelect) {
                                if (IS_ELEGIBILIDAD) {
                                    if (!(value == '0' || value == '')) { tplSel[1] += "<div data-send='" + dataSend + "'>" + text + "</div>"; }
                                } else {
                                    tplSel[1] += "<option data-send='" + dataSend + "' value='" + text + "'" + ((value == '0' || value == '') ? '' : ' selected') + ">" + text + "</option>";
                                }
                            } else {
                                if (tplSel[1] !== "") {
                                    // Adicionamos el select (cantOpts puede variar es diferente en cada caso)
                                    //aTrBody[1].push('<td colspan="' + 1 + '">' + tplSel.join('') + '</td>' + (isCEP ? '<td><span class="tr-point">0 pts</span></td>' : ''));
                                    aTrBody[1].push(
                                        '<td colspan="' + (IS_ELEGIBILIDAD ? 1 : 15) + '">' + tplSel.join('') + '</td>'
                                        + ((IS_ELEGIBILIDAD && isCEP) ? '<td colspan="2"><span class="tr-point">0 pts</span></td>' : '')
                                    );
                                    tplSel[1] = ""; // limpiando options
                                    cantOpts = 0;  // Ya no hay options para select
                                }
                                if (isInpText) {
                                    aTrBody[1].push("\n\t<td " + sColSpan + "><div class='text-justify'>" + value + "</div></td>");
                                }
                                else {
                                    aTrBody[1].push("\n\t<td class='text-center'>" + ((value == '1' && isChkRdo) ? "<span class='x-style'>X</span>" : "") + "</td>");
                                }
                            }
                            //Si llegamos al ultimo criterio y hay options para el select
                            if (c == (lengthCriterios - 1) && tplSel[1] !== "") {
                                // Adicionamos el select (cantOpts puede variar es diferente en cada caso)
                                aTrBody[1].push(
                                    '<td colspan="' + (IS_ELEGIBILIDAD ? 1 : 15) + '">' + tplSel.join('') + '</td>'
                                    + ((IS_ELEGIBILIDAD && isCEP) ? '<td colspan="2"><span class="tr-point">0 pts</span></td>' : '')
                                );

                                tplSel[1] = ""; // limpiando options
                                cantOpts = 0;  // Ya no hay options para select
                            }
                        }

                        // Guardando Headers para cuando existe un row con Nombre de padre
                        if (bExistNameParent || !iCountF) {
                            aTrHeader[1] = aTrHeader[1].join(''); // Convirtiendo el item central de aTrHeader en string de <td />'s
                            sAllTrs += aTrHeader.join(''); // almacenando todo el <tr /> para cabecera
                            // Para completar con un row por el tema del SI|NO y el rowspan en todos los td del header
                            sAllTrs += '<tr>' + (isSI_NO ? '<td class="text-center width5per"><b>SI</b></td><td class="text-center"><b>NO</b></td>' : '') + '</tr>';

                            aTrHeader[1] = []; // seteando item central de <td />'s a un array vacio
                        }

                        // Guardando registros comunes
                        aTrBody[1] = aTrBody[1].join(''); // Convirtiendo el item central de aTrHeader en string de <td />'s
                        sAllTrs += aTrBody.join(''); // almacenando todo el <tr /> para cabecera
                        iCountF++;
                    }
                }

                $contentTab.html(getTabContent(sHeaders, sAllTrs)).removeClass('hide');

                var oDataForSave = {}; // Objeto de data que se guardara
                var $tBody = $contentTab.find('table.table-condensed>tbody'); // Variables para el contenido recientemente cargado

                $contentTab.find('input,select').prop('disabled', true);

                if (!IS_ELEGIBILIDAD) {
                    $contentTab.find('select[multiple]').each(function (i, ele) {
                        $(this).find('option:eq(0)').remove(); // quitando option seleccionar
                        $(ele).attr('size', $(this).find('option').length);
                    });
                }

                if (IS_ELEGIBILIDAD && isCEP) {
                    // Creando ultimo row (para calculo de puntajes)
                    var $endTr = $tBody.find('tr:last-child')
                        , lenCS = parseInt($endTr.find('td[colspan]').attr('colspan')) // len ColSpan
                        , lenTr = $endTr.find('td').length  // logitud de td's de un tr de tbody
                        , newCS = lenCS + lenTr - 2  // nuevo colspan para nuevo row
                        , strTr = '<tr class="text-center">\
                                           <td colspan="' + newCS + '" class="text-right"><b class="tr-point">Puntaje total :</b></td>\
                                           <td colspan="2"><span id="totalPoint">0 pts</span></td>\
                                       </tr>'
                        , $btnNoAdmisible = $("#btnNoAdmisible")
                        , $btnAdmisible = $("#btnAdmisible")
                        , $firstTr = $tBody.find('tr:first-child')
                        ;

                    $btnNoAdmisible.prop('disabled', false);
                    $btnAdmisible.prop('disabled', true);

                    $tBody.append(strTr); // Agregando row final para el count de totales
                    if ($.trim($firstTr.text()) == '') { $firstTr.css('display', 'none'); } // Ocultando row cabecera si esta vacia de texto

                    $tBody.find('.sel-points').each(function (i, ele) {
                        var $span = $(ele).parent().parent().find('span')
                            , $allSpans = $tBody.find('span.tr-point')
                            , $spanTotal = $tBody.find('#totalPoint')
                            , iPoint = parseInt($(ele).html().replace(/.*\( ([0-9]*) puntos \).*/g, function (a, b) { return b; }))
                            , iTotal = 0
                            ;
                        // seteando valor
                        iPoint = isNaN(iPoint) ? 0 : iPoint;
                        iPoint = (iPoint >= 100) ? 100 : iPoint;
                        $span.data('point', iPoint).text(((iPoint > 9) ? '' : '0') + iPoint + ' pts');
                        //log("DATA-POINT:", iPoint, $span.data('point'));
                        //log("$allSpans:", $allSpans);
                        // Recalculando todos los span's de puntos
                        $allSpans.each(function (i, ele) {
                            iTotal += $(ele).data('point') ? $(ele).data('point') : 0;
                            //log("iTotal:", iTotal);
                        });
                        $spanTotal.text(((iTotal > 9) ? '' : '0') + (iTotal >= 100 ? 100 : iTotal) + ' pts');
                        $btnNoAdmisible.prop('disabled', (iTotal > 49));
                        $btnAdmisible.prop('disabled', !(iTotal > 49));
                    }); //.trigger('change') Para volver a cargar sumatoria
                }

            }
        });

    });

    // Habriendo el primer tab
    // $firstTab.trigger('shown.bs.tab');
    $(".panel-heading a").trigger('click');
});