﻿$(function () {
   
    var chkGenEtapa= $("#chkGenEtapa");
    var divEtapaNro = $("#divEtapaNro");
    var btnValidEtapa = $("#btnValidEtapa");
    var successAlert = $('<div class="alert alert-success" role="alert"></div>');
    var errorAlert = $('<div class="alert alert-danger" role="alert"></div>');
    var newProjModal = $("#newProjModal");
    var btnNuevoProy = $("#ContentPlaceHolder1_btnNuevoProy");

    $('[data-toggle="popover"]').popover();

    chkGenEtapa.on("change", function () {
        if ($(this).is(":checked")) {
            divEtapaNro.show();
        }
        else {
            divEtapaNro.hide();
        }
    });

    btnValidEtapa.on("click", function () {

        var slTipoFinancVal = $("#ContentPlaceHolder1_slTipoFinanc").val();
        var chkGenEtapaVal = "0";
        //var txtNumEtapaVal = $.trim($("#txtNumEtapa").val());
        var txtNumEtapaVal = "0";

        if (slTipoFinancVal == "") {
            alert("Seleccione un tipo de financiamiento");
            return false;
        }

        //if (chkGenEtapaVal.is(':checked')) {
        //    chkGenEtapaVal = "1";
        //    if (txtNumEtapaVal == "") {
        //        alert("Ingrese una etapa");
        //        return false;
        //    }
        //}
        //else{
        //    chkGenEtapaVal = "0";
        //}


        //Guardando info
        var request = {
            idProyecto: p,
            idUsuario: u,
            idTipoFinanciamiento: slTipoFinancVal ,
            vEtapa: chkGenEtapaVal,
            idEtapa: txtNumEtapaVal
        };    

        var strRequest = JSON.stringify(request);  

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: 'Registro_Obra.aspx/GuardarEtapa',
            data: strRequest,
            success: function (response) {
                var msg = successAlert.clone();
                if (response.d == "200") {
                    newProjModal.find(".modal-body").append(msg);
                    msg.show();
                    msg.html("Datos registrados satisfactoriamente en el sistema");
                    newProjModal.find(":input").prop("disabled", true);

                }
                else {
                    var msg = errorAlert.clone();
                    newProjModal.find(".modal-body").append(msg);
                    msg.show();
                    msg.html("Ha ocurrido un error en el sistema, verifique la data enviada, de persistir el error, por favor sírvase de llamar a mesa de ayuda");
                    newProjModal.find(":input").prop("disabled", false);
                }

                setTimeout(function () {
                    msg.remove();
                    newProjModal.modal("hide");
                    window.location.reload();
                }, 4000);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);

                var msg = errorAlert.clone();
                newProjModal.find(".modal-body").append(msg);
                msg.show();
                msg.html("Ha ocurrido un error en el sistema, verifique la data enviada, de persistir el error, por favor sírvase de llamar a mesa de ayuda");
                newProjModal.find(":input").prop("disabled", false);
            },
        });
        
    });
    
    ////////// fix para habilitacion del boton +
   // var lblEstado = $("#ContentPlaceHolder1_lblIdEstado").html();
   // var lblSubEstado = $("#ContentPlaceHolder1_lblIdSubEstado").html();
    
   

});
