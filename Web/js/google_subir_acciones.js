﻿//=Create object of FilePicker Constructor function function & set Properties===
var picker = {};

/**
 * 
 * @param {string} folder: ID de carpeta obtenido de google drive
 * @param {string} title: Titulo que se mostrara en el picker
 * @param {boolean} multiple: si la carga sera de multiples archivos o no
 * @param {int} process: opcional, para realizar algun procedimiento de negocio si se necesita
 */
function SetPicker(folder, title, multiple, process) {

    //Button
    //console.log("User.buttonEl", User.buttonEl);
    //$.each(User.buttonEl, function (i, val) {
    //    values += "<option value='" + val.Valor + "'>" + val.Descripcion + "</option>"
    //});

    picker = new FilePicker(
        {
            clientId: '21938297471-3hh6h1pl6cik1ltmc91615l8clr9g9f1.apps.googleusercontent.com',
            folder: folder || '',
            multiple: multiple,
            process: process || null,
            title: title || 'Carga tus Archivos',
            buttonEl: $(".SubirDrive"),
            onClick: function (file) {
                PopupCenter('https://drive.google.com/file/d/' + file.id + '/view', "", 1026, 500);
            }
        });
}

/**
 * Función para obtener un access token para usar el componente picker
 * */
function getToken() {

    var res = "";

    $.ajax({
        async: false,
        url: "iframe_RegistrarSeguimientoParalizacion.aspx/getToken",
        type: 'POST',
        contentType: 'application/json',
        error: function () {
            console.log(arguments);
        },
        success: function (result) {
            res = result;
        }
    });

    return res;

}

//====================Create POPUP function==============
function PopupCenter(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

//===============Create Constructor function==============
function FilePicker(User) {

    //Configuration
    this.clientId = User.clientId;
    this.token = '';
    this.title = User.title;
    this.process = User.process;
    this.folder = User.folder;
    this.multiple = User.multiple;



    this.buttonEl = User.buttonEl;
    this.mimeArr = [];

    //Click Events
    this.onClick = User.onClick;
    this.buttonEl.on('click', this.open.bind(this));


    //Disable the button until the API loads, as it won't work properly until then.
    this.buttonEl.disabled = true;

    //Load the drive API
    gapi.client.load('drive', 'v3', this.DriveApiLoaded.bind(this));

    gapi.load('picker', {
        callback: this.PickerApiLoaded.bind(this)
    }
    );

}

FilePicker.prototype = {
    //==========Check Authentication & Call ShowPicker() function=======

    open: function (elem) {

        window.IdAccion = $(elem.target).data("id");
        window.Tipo = $(elem.target).data("tipo");
        window.idboton = $(elem.target).attr("id");


        // Check if the user has already authenticated

        let objtoken = getToken();
        this.token = objtoken.d;

        console.log("token", this.token);

        if (this.token) {
            var mimetypes = "";
            switch (this.process) {
                case 1: //UPLOAD ARCHIVOS EXPEDIENTE
                    mimetypes = archivos.getMimeType();
                    break;

                case 2: // UPLOAD CREDENCIAL
                    mimetypes = acreditaciones.getMimeType();
                    break;
            }

            this.ShowPicker(mimetypes);

        }
        else {
            console.log("nada que hacer");
        }
    },

    //========Show the file picker once authentication has been done.=========
    ShowPicker: function (mimeType) {
        mimeType = "xls"
        var accessToken = this.token;

        //User can upload file in specific folder
        var UploadView = new google.picker.DocsUploadView().setParent(this.folder)
        var mimeTypeFinal = "";

        if (mimeType != "") {

            mimeArr = mimeType.split(",");

            // Now it can be used reliably with $.map()
            mimeArr = $.map(mimeArr, function (val, i) {

                switch ($.trim(val)) {
                    case "xls":
                        return "application/vnd.ms-excel";
                        break;
                    case "xlsx":
                        return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        break;
                    case "dwg":
                        return 'image/vnd.dwg';
                        break;
                    case "pdf":
                        return "application/pdf";
                        break;
                }

            });

            this.mimeArr = mimeArr;
        }
        var formatStr = "";

        if (mimeType != "") {

            var formatStr = " - los formatos permitidos son ";

            formatStr = formatStr + (mimeType.includes("xls") ? "Excel (.xls, .xlsx)" : "");
            formatStr = formatStr + (mimeType.includes("pdf") ? " PDF (.pdf)" : "");
            formatStr = formatStr + (mimeType.includes("dwg") ? " Autocad (.dwg)" : "");

        }

        picker = new google.picker.PickerBuilder().
            addView(UploadView).
            setAppId(this.clientId).
            setLocale('es-419').
            enableFeature(this.multiple ? google.picker.Feature.MULTISELECT_ENABLED : google.picker.Feature.SIMPLE_UPLOAD_ENABLED).
            setSelectableMimeTypes(mimeType != "" ? mimeType : "*").
            setOAuthToken(accessToken).
            setCallback(this.PickerResponse.bind(this)).
            setTitle(this.title + formatStr).
            build().
            setVisible(true);

    },

    //====Called when a file has been selected in the Google Picker Dialog Box======
    PickerResponse: async function (data) {

        console.log("RESPONSE", data);

        switch (data[google.picker.Response.ACTION]) {
            case "loaded":
                //console.log("loaded", data);
                break;
            case google.picker.Action.PICKED:

                var docs = data.docs;

                docs.map(entry => {

                    console.log(entry);
                    //AQUI OBTENDRAS TU OBJETO FILE Y VES QUE HACES CON EL
                    __doPostBack(window.idboton, window.Tipo + '|' + window.IdAccion + '|' + entry.id + '|' + entry.name);
                })

                break;
            case "cancel":
                console.log("cancel");
                //WS PARA GUARDAR EN BD
                console.log("WS PARA GUARDAR EN BD");
                break;
        }

    },

    //====Called when file details have been retrieved from Google Drive========
    GetFileDetails: function (file) {

        if (this.onClick) {
            this.onClick(file);
        }
    },

    //====Called when the Google Drive file picker API has finished loading.=======
    PickerApiLoaded: function () {

    },

    //========Called when the Google Drive API has finished loading.==========
    DriveApiLoaded: function () {
        console.log("PICKER LOADED")
        picker.buttonEl.disabled = false;
    },

};
