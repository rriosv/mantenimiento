﻿

//*************************************************
// Validad registro de avance y monitoreo de obras por Transferencias
//**************************************************
function ValidarVisitaObraTransferencia() {

    var TipoMonitoreo = document.getElementById("ctl00_ContentPlaceHolder1_TabContainer_TabPanelEstadoEjecuccion_ddlTipoMonitoreo").value;
    var FechaVisita = document.getElementById('ctl00_ContentPlaceHolder1_TabContainer_TabPanelEstadoEjecuccion_txtFechaVisitaTab2').value;
    var AvanceFisico = document.getElementById('ctl00_ContentPlaceHolder1_TabContainer_TabPanelEstadoEjecuccion_txtEjecutadoTab2').value;
    var AvanceProgramado = document.getElementById('ctl00_ContentPlaceHolder1_TabContainer_TabPanelEstadoEjecuccion_txtProgramadoTab2').value;

    var TipoEstado = document.getElementById("ctl00_ContentPlaceHolder1_TabContainer_TabPanelEstadoEjecuccion_ddlTipoEstadoInformeTab2").value;
    var TipoSubEstado = document.getElementById("ctl00_ContentPlaceHolder1_TabContainer_TabPanelEstadoEjecuccion_ddlTipoSubEstadoInformeTab2").value;

    if (TipoMonitoreo == '') {
        alert('Seleccionar tipo de monitoreo.');
        return false;
    }
    else {
        if (FechaVisita == '') {
            alert('Ingresar fecha de visita.');
            return false;
        }
        else {
            if (TipoEstado == '') {
                alert('Seleccionar estado de ejecución.');
                return false;
            }
            else {
                if (TipoSubEstado == '') {
                    alert('Seleccionar sub estado de ejecución.');
                    return false;
                }
                else {
                    if (AvanceFisico == '') {
                        alert('Ingresar avance físico.');
                        return false;
                    }
                    else {
                        if (AvanceProgramado == '') {
                            alert('Ingresar avance programado.');
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                }
            }
        }
    }
}