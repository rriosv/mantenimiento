﻿using Business;
using seguridad;
using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace Web
{
    public partial class login : System.Web.UI.Page
    {
        BLMenu _BLMenu = new BLMenu();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
            //FormsAuthentication.SignOut();
        }

        protected void btnIngresar(object sender, EventArgs e)
        {
            acceso objAcceso = new acceso();
            BLLogin objAcceso1 = new BLLogin();
            BLUtil _Metodo = new BLUtil();

            string passw = objAcceso.Encriptar(txtPass.Text);

            DataTable dt = objAcceso1.sp_validarUsuario(txtUser.Text.Trim(), passw);

            if (dt.Rows.Count > 0)
            {
                Ingresar(dt);
            }
            else
            {
                string script = "<script>alert('El usuario o contraseña es incorrecto. Volver a intentar.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        }


        protected void Ingresar(DataTable dt)
        {
            string vUrlWebForm = System.Configuration.ConfigurationManager.AppSettings["UrlWebForm"].ToString();
            string vUrlActual = HttpContext.Current.Request.Url.AbsoluteUri;

            if (vUrlActual.Contains("localhost"))
            {
                vUrlWebForm = "";
            }

            acceso objAcceso = new acceso();
            BLLogin objAcceso1 = new BLLogin();

            DataTableReader dr = dt.CreateDataReader();

            while (dr.Read())
            {
                Session["IdUsuario"] = dr["id_usuario"];
                Session["LoginUsuario"] = dr["login"];
                Session["NombreUsuario"] = dr["nombre"];
                Session["Correo"] = dr["correo"];
                Session["CodTipoUsuario"] = dr["cod_tipo_usuario"];
                Session["CodSector"] = dr["cod_entidad"];
                Session["CodSubsector"] = dr["cod_subsector"];
                Session["NombreOficina"] = dr["cod_programa"];
                Session["Region"] = dr["nom_depa"];
                Session["CodRegion"] = dr["cod_depa"];
                Session["CodProv"] = dr["cod_prov"];
                Session["CodNivelAcceso"] = dr["cod_nivel_acceso"];
                Session["PerfilUsuario"] = dr["id_perfil_usuario"];
                Session["idTecnicoEstudio"] = dr["idTecnico"];
                Session["DNI"] = dr["vDNI"];
                //FormsAuthentication.Initialize();
                //FormsAuthentication.RedirectFromLoginPage(txtUser.Text, false);

                // INICIO SESION TOKEN
                string token = dr["id_usuario"].ToString() + DateTime.Now.ToString("yyyyMMdd");
                token = objAcceso.Encriptar(token);

                int val = objAcceso1.I_iSession(token, Convert.ToInt32(dr["id_usuario"].ToString()));
                Session["clave"] = token;
                // FIN SESION TOKEN

                DataSet ds = _BLMenu.sp_ListarMenusxUsuario(Convert.ToInt32(dr["id_usuario"]));
                Session["OpcionesMenu"] = ds;

                string ruta = "";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        string titulo1 = row["subtitulo"].ToString();
                        if (titulo1.Equals("Consulta de Monitoreo"))
                        {
                            ruta = row["pagina"].ToString();
                            Context.ApplicationInstance.CompleteRequest();
                            Response.Redirect(vUrlWebForm + ruta, true);
                            break;
                        }
                    }
                    ////Supervisor Externo
                    //if (Session["PerfilUsuario"].ToString().Equals("82") && Session["NombreOficina"].ToString().Equals("EXTERNO"))
                    //{
                    //    ruta = "/Monitor/Bandeja_Monitoreo";
                    //}
                    //else
                    //{
                    //    ruta = "Solicitudes/inicio";
                    //}

                    ruta = "Solicitudes/inicio";

                    Response.Redirect(vUrlWebForm + ruta, true);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    //lblError.Text = "No tiene acceso.";
                }
            }
            dr.Close();
            dr.Dispose();
        }
    }
}