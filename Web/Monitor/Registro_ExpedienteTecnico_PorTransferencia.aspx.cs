﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Registro_ExpedienteTecnico_PorTransferencia : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();

        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Conclusion _objBLConclusion = new BL_MON_Conclusion();
        BE_MON_Conclusion _BEConclusion = new BE_MON_Conclusion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        DataTable dt = new DataTable();

        BL_MON_Finan_Transparencia _objBLFinaTra = new BL_MON_Finan_Transparencia();
        BE_MON_Finan_Transparencia _BEFinaTra = new BE_MON_Finan_Transparencia();

        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();

        BLUtil _Metodo = new BLUtil();

        decimal sumFooterValue = 0;
              
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {
                Response.Redirect("~/login.aspx?ms=1");
            }


            Session.Timeout = 120;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            int idProy = Convert.ToInt32(Request.QueryString["id"]);

            if (!IsPostBack)
            {

                BtnRefrescar_Click(null, null);

                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                // Session["id_proyecto"] = Convert.ToInt32(LblID_PROYECTO.Text);

                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();
                lblID_PERFIL_USUARIO.Text = Session["PerfilUsuario"].ToString();
                lblCOD_SUBSECTOR.Text = Session["CodSubsector"].ToString();

                //int idacceso = Convert.ToInt32(Request.QueryString["acceso"].ToString());
                // = Convert.ToInt32(Session["AccesoMonitor"].ToString());

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int idacceso = _objBLProceso.spMON_Acceso(_BEProceso);
                Session["FlagAcceso"] = idacceso;
                lblID_ACCESO.Text = idacceso.ToString();

                if (idacceso == 0)
                {
                    OcultarRegistro();
                }

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);

                if (dt.Rows.Count > 0)
                {
                    lblNombProy.Text = dt.Rows[0]["nom_proyecto"].ToString();
                    lblSNIP.Text = dt.Rows[0]["cod_snip"].ToString();

                    if (lblSNIP.Text == "0")
                    {
                        trVigencia1.Visible = false;
                        trVigencia2.Visible = false;
                        trVigencia3.Visible = false;
                    }

                    ifrGraficoSosem.Attributes["src"] = "iframe_GraficoSOSEM?snip=" + lblSNIP.Text;

                    string depa = dt.Rows[0]["depa"].ToString();
                    string prov = dt.Rows[0]["prov"].ToString();
                    string dist = dt.Rows[0]["dist"].ToString();
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                    txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    lblCOD_SUBSECTOR.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                    lblTipoFinanciamiento.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();
                                      
                    ///
                    lblIdEstadoRegistro.Text = dt.Rows[0]["idEstadoProyecto"].ToString();
                    if (lblIdEstadoRegistro.Text.Equals("3"))
                    {
                        lblEstadoRegistro.Text = "FINALIZADO";
                        btnFinalizarTab3.Text = "Activar";
                    }
                    else
                    {
                        lblEstadoRegistro.Text = "ACTIVADO";
                        btnFinalizarTab3.Text = "Finalizar";
                    }
                }


                CargaTab0(Convert.ToInt32(LblID_PROYECTO.Text));

                CargaFinanciamientoTab0(Convert.ToInt32(LblID_PROYECTO.Text));

                CargaTab1();
                CargaTab2();
                CargaTab3();
                CargaTab4();
                CargaTab6();
                CargaTab7();
                //CargaTab8();
                         

                //if (lblCOD_SUBSECTOR.Text == "2")
                //{
                //    //txtMontoTab0.Visible = true;
                //    txtMontoSupervisionTab0.Visible = true;
                //    lblMontoObra.Visible = true;
                //    lblMontoSupervisión.Visible = true;
                //    grdContrapartidasTab0.Columns[3].Visible = true;
                //    grdContrapartidasTab0.Columns[4].Visible = true;
                //    grd_TransferenciasTab0.Columns[11].Visible = false;
                //}

                CargarMetas(Convert.ToInt32(LblID_PROYECTO.Text));

                // COORDINADORES PMIB , PNSU, PNSR, PNVR, PASLC
                if (lblIdEstadoRegistro.Text.Equals("3") && (lblID_PERFIL_USUARIO.Text.Equals("23") || lblID_PERFIL_USUARIO.Text.Equals("24") || lblID_PERFIL_USUARIO.Text.Equals("71") || lblID_PERFIL_USUARIO.Text.Equals("74") || lblID_PERFIL_USUARIO.Text.Equals("76") || LblID_USUARIO.Text.Equals("481")))
                {
                    btnFinalizarTab3.Visible = true;
                }
                else if (lblIdEstadoRegistro.Text.Equals("3"))
                {
                    btnFinalizarTab3.Visible = false;
                }

            }

            //Agregar menu lateral por etapa - https://trello.com/c/uqVY2SZM/41-obras
            BL_MON_Menu oMenu = new BL_MON_Menu();

            List<BE_MON_Menu> ListMenu = new List<BE_MON_Menu>();
            ListMenu = oMenu.paSSP_MON_rEtapas(idProy, 3);
            int currentIndex = 0;
            btnNuevoProy.Visible = false;

            if (ListMenu != null)
            {

                for (int i = 0; i < ListMenu.Count; i++)
                {
                    BE_MON_Menu menu = ListMenu[i];
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    if (menu.idProyecto == idProy)
                    {
                        li.Attributes.Add("class", "active");

                        //Colocando tipos de financiamiento - https://trello.com/c/uqVY2SZM/41-obras
                        DataSet dsTipo = new DataSet();
                        dsTipo = _objBLFinanciamiento.paSSP_MON_rTipoFinanciamientos(menu.idTipoFinanciamiento);
                        DataTable dtTipoFinanc = new DataTable();
                        dtTipoFinanc = dsTipo.Tables[0];

                        foreach (DataRow row in dtTipoFinanc.Rows)
                        {
                            slTipoFinanc.Items.Add(new ListItem(row["vNombre"].ToString(), row["idTIpoFinanciamiento"].ToString()));
                        }
                        currentIndex = i;
                    }

                    menuObra.Controls.Add(li);

                    HtmlGenericControl anchor = new HtmlGenericControl("a");
                    
                    string aspxPage = "";
                    switch (menu.idTipoFinanciamiento)
                    {
                        case 1:
                            if (menu.idModalidadFinanciamiento == 3)
                            {
                                aspxPage = "Registro_PreInversion.aspx";
                            }
                            else
                            {
                                aspxPage = "Registro_PreInversion_PorTransferencia.aspx";
                            }
                            break;
                        case 2:
                            if (menu.idModalidadFinanciamiento == 3)
                            {
                                aspxPage = "Registro_ExpedienteTecnico.aspx";
                            }
                            else
                            {
                                aspxPage = "Registro_ExpedienteTecnico_PorTransferencia.aspx";
                            }
                            break;
                        case 3:
                            if (menu.idModalidadFinanciamiento == 3)
                            {
                                aspxPage = "Registro_EjecucionContrata.aspx";
                            }
                            else
                            {
                                if (menu.idModalidadFinanciamiento == 4)
                                { aspxPage = "Registro_NE.aspx"; }
                                else
                                {
                                    if (menu.idModalidadFinanciamiento == 2)
                                    {
                                        aspxPage = "Registro_Jica.aspx";
                                    }
                                    else { aspxPage = "Registro_Obra.aspx"; }
                                }
                            }
                            break;
                        default:
                            aspxPage = "Registro_ExpedienteTecnico_PorTransferencia.aspx";
                            break;
                    }

                    //anchor.Attributes.Add("href", "/Monitor/" + aspxPage + "?id=" + menu.idProyecto);
                    anchor.Attributes.Add("href", "" + aspxPage + "?id=" + menu.idProyecto + "&token=" + Request.QueryString["token"]);
                    anchor.InnerText = menu.tipoFinanciamiento;
                    li.Controls.Add(anchor);

                    if (menu.flagUltimo == 1 && menu.idProyecto == idProy)
                    {
                        btnNuevoProy.Disabled = false;
                        btnNuevoProy.Visible = true;
                    }
                }
            }
        }


        protected void OcultarRegistro()
        {
           
            //btnAgregarContrapartidasTab0.Visible = false;
            btnAgregarTransferenciaTab0.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            //imgbtn_agregarSOSEM.Visible = false;
            imgAgregaPlazoTab4.Visible = false;
            imgbtnAvanceFisicoTab2.Visible = false;
            imgbtnPlaTab4.Visible = false;
            imgbtnPreTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = false;
            btnGuardarTab7.Visible = false;
            btnGuardarTab3.Visible = false;
            btnGuardarTab2.Visible = false;
            imgbtn_CoordinadorTab2.Visible = false;
            btnGuardarTab1.Visible = false;

            //btnAgregarContrapartidasTab0.Visible = false;
            btnAgregarTransferenciaTab0.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            //imgbtn_agregarSOSEM.Visible = false;


            //btnGuardarComponentesPNSU.Visible = false;


            //grdContrapartidasTab0.Columns[0].Visible = false;
            //grdContrapartidasTab0.Columns[8].Visible = false;

            grd_TransferenciasTab0.Columns[0].Visible = false;
            grd_TransferenciasTab0.Columns[16].Visible = false;
            //grdSosemTab0.Columns[0].Visible = false;
            //grdSosemTab0.Columns[19].Visible = false;

            grdSeguimientoProcesoTab1.Columns[0].Visible = false;
            grdSeguimientoProcesoTab1.Columns[11].Visible = false;
            grdConsultoresTab1.Columns[0].Visible = false;
            grdConsultoresTab1.Columns[4].Visible = false;

            grdAvanceFisicoTab2.Columns[0].Visible = false;
            grdAvanceFisicoTab2.Columns[15].Visible = false;
            imgbtnAvanceFisicoTab2.Visible = false;

            grdPlazoTab4.Columns[0].Visible = false;
            grdPlazoTab4.Columns[11].Visible = false;
            imgAgregaPlazoTab4.Visible = false;
            grdPresupuestoTab4.Columns[0].Visible = false;
            grdPresupuestoTab4.Columns[12].Visible = false;
            grdDeductivoTab4.Columns[0].Visible = false;
            grdDeductivoTab4.Columns[12].Visible = false;

            grdPanelTab6.Columns[0].Visible = false;
            grdPanelTab6.Columns[8].Visible = false;
            imgbtnAgregarPanelTab6.Visible = false;

            //btnGuardarMetasPMIB.Visible = false;
            //btnGuardarConexionTab2.Visible = false;

            imgbtnEditarUE.Visible = false;
            imgbtnEditarUbigeo.Visible = false;

            trCoordenadaTabMeta.Visible = false;
            lblMsjMetaTabH.Visible = false;

            btnGuardarTransferenciaFinancieraTab01.Visible = false;
            btnGuardarTransferenciaPresupuestalTab01.Visible = false;
            grdFinancieraTab01.Columns[0].Visible = false;
            grdFinancieraTab01.Columns[15].Visible = false;

            lnkbtnBuscarRUCConsorcioTab1.Visible = false;
            lnkbtnBuscarRUCContratistaTab1.Visible = false;
            lnkbtnBuscarRUCMiembroConsorcioTab1.Visible = false;

            btnFinalizarTab3.Visible = false;

            UPTabContainerDetalles.Update();
        }
      
        protected void grd_ResumConvenioTab0_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell cell = new TableHeaderCell();
                cell.ColumnSpan = 7;
                HeaderGridRow.Cells.Add(cell);

                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Monto Transferido";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(HeaderCell);

                //HeaderCell = new TableCell();
                //HeaderCell.Text = "Monto Transferido";
                //HeaderCell.ColumnSpan = 2;
                //HeaderCell.CssClass = "GridHeader2";
                //HeaderGridRow.Cells.Add(HeaderCell);
                HeaderGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);

            }

            //if (e.Row.RowType == DataControlRowType.Footer)
            //{

            //    //e.Row.Cells.RemoveAt(0);
            //    //e.Row.Cells.RemoveAt(0);

            //    //e.Row.Cells.RemoveAt(0);
            //    //e.Row.Cells.RemoveAt(0);
            //    //e.Row.Cells[3].ColumnSpan = 2;
            //    //e.Row.Cells[3].Text = "S/ " + ShowGrandTotalComp();
            //    //e.Row.Cells[4].ColumnSpan = 2;
            //    //e.Row.Cells[4].Text = "S/ " + ShowGrandTotalTransf();

            //}
        }

        protected void CargarMetas(int idProy)
        {
            DataSet dsMet = new DataSet();
            dsMet = _objBLFinanciamiento.paSSP_MON_rMetas(idProy, 2, 0);
            DataTable dtSistema = dsMet.Tables[0];
            DataTable dtProgamado = dsMet.Tables[1];

            string htmlProgTotal = "";

            foreach (DataRow rowSistema in dtSistema.Rows)
            {
                string htmlProg = "";
                foreach (DataRow rowP in dtProgamado.Rows)
                {
                    if (rowP["idSistema"].ToString().Equals(rowSistema["idSistema"].ToString()))
                    {
                        if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
                        {
                            htmlProg = htmlProg +
                                         "<tr>" +
                                              "<td>" + rowP["COMPONENTE"].ToString() + "</td>" +
                                              "<td>" + rowP["Cantidad"].ToString() + "</td>" +
                                              "<td>" + rowP["Costo"].ToString() + "</td>" +
                                         "</tr>";
                        }
                        else //SANEAMIENTO
                        {
                            htmlProg = htmlProg +
                                       "<tr>" +
                                            "<td>" + rowP["COMPONENTE"].ToString() + "</td>" +
                                            "<td>" + rowP["TIPOLOGIA1"].ToString() + "</td>" +
                                            "<td>" + rowP["TIPOLOGIA2"].ToString() + "</td>" +
                                            "<td>" + rowP["NATURALEZA"].ToString() + "</td>" +
                                            "<td>" + rowP["Cantidad"].ToString() + "</td>" +
                                            "<td>" + rowP["Capacidad"].ToString() + "</td>" +
                                            "<td>" + rowP["Costo"].ToString() + "</td>" +
                                       "</tr>";
                        }
                    }
                }

                if (htmlProg.Length > 0)
                {
                    if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
                    {
                        htmlProgTotal = htmlProgTotal +
                                    "<table class='table table-bordered'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        "<tr>" +
                                        "<th>Componente</th>" +
                                        "<th>Cantidad</th>" +
                                        "<th>Costo S/</th>" +
                                        "</tr>" +
                                        htmlProg +
                                    "</table>";
                    }
                    else
                    {
                        htmlProgTotal = htmlProgTotal +
                                    "<table class='table table-bordered'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        "<tr>" +
                                        "<th>Componente</th>" +
                                        "<th>Tipología 1</th>" +
                                        "<th>Tipología 2</th>" +
                                        "<th>Naturaleza de <br> Intervención</th>" +
                                        "<th>Cantidad</th>" +
                                        "<th>Capacidad</th>" +
                                        "<th>Costo S/</th>" +
                                        "</tr>" +
                                        htmlProg +
                                    "</table>";
                    }
                }
            }

            if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
            {
                divMetaProg.Style.Add("width", "85%");
            }

            if (htmlProgTotal.Equals(""))
            {
                htmlProgTotal = "<table class='table table-bordered'> " +
                                       "<tr><td>No hay registro</td></tr>" +
                                   "</table>";
            }
            divMetaProg.InnerHtml = htmlProgTotal;
        }


        /// <summary>
        /// Metodo que se usa para poder armar columnas agrupadas por encabezados posterior a haberse creado
        /// </summary>
        /// <seealso cref="String">
        /// http://www.etechpulse.com/2013/07/c-merging-gridview-header-columnscells.html
        /// </seealso>
        protected void grd_ResumConvenioTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[9].Text = "S/ " + ShowGrandTotalComp();
                //e.Row.Cells[12].Text = "S/ " + ShowGrandTotalTransf();
            }
        }

        /// <summary>
        /// Metodo que retorna los montos con formato (xxx,xxx,xxx.xx)
        /// </summary>
        /// <seealso cref="String">
        /// </seealso>
        public string GetFormatAmount(object num)
        {
            return (Convert.ToDouble(num)).ToString("N");
        }

        /// <summary>
        /// Metodo que suma subtotales para el total del monto comprometido
        /// </summary>
        /// <seealso cref="String">
        /// https://forums.asp.net/t/1538966.aspx?Grand+total+column+in+gridview
        /// </seealso>
        public string ShowSubTotalComp(object totalCost)
        {
            sumFooterValue += Convert.ToInt32(totalCost);
            return (Convert.ToDouble(totalCost)).ToString("N");
        }

        /// <summary>
        /// Metodo que retorna el total del monto comprometido
        /// </summary>
        /// <seealso cref="String">
        /// https://forums.asp.net/t/1538966.aspx?Grand+total+column+in+gridview
        /// </seealso>
        public string ShowGrandTotalComp()
        {
            return (Convert.ToDouble(sumFooterValue)).ToString("N");
        }


        protected void CargaFinanciamientoTab0(int idProy)
        {
            //Colocando resumen de financiamientp etapa - https://trello.com/c/uqVY2SZM/41-obras
            DataSet ds = new DataSet();
            ds = _objBLFinanciamiento.paSSP_MON_rFinanciamiento(idProy);
            // DataTable dtSumatorias = new DataTable();

            if (ds.Tables[0].Rows.Count > 0)
            {
                lblMontoComprometido.Text = (Convert.ToDouble(ds.Tables[0].Rows[0]["SumMontoComprometido"])).ToString("N");
                lblMontoTransferido.Text = (Convert.ToDouble(ds.Tables[0].Rows[0]["SumMontoTransferido"])).ToString("N");
            }
            else
            {
                lblMontoComprometido.Text = "0.00";
                lblMontoTransferido.Text = "0.00";
            }

            DataTable dtCofinanciamiento = new DataTable();
            dtCofinanciamiento = ds.Tables[1];

            if (dtCofinanciamiento.Rows.Count > 0)
            {
                lblMontoConfinFirm.Text = (Convert.ToDouble(dtCofinanciamiento.Rows[0]["SumCofinanciamiento"])).ToString("N");
            }
            else
            {
                lblMontoConfinFirm.Text = "0.00";
            }

            DataTable dtMontosTotalesDisp = new DataTable();
            dtMontosTotalesDisp = ds.Tables[2];
            lblMontoTotalDispDS.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[0]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispDU.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[2]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispRM.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[1]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispLey.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[3]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispDL.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[4]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispRD.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[5]["SumMontoTransferidoXDisp"])).ToString("N");

            DataTable dtMontosTotalesFuente = new DataTable();
            dtMontosTotalesDisp = ds.Tables[3];
            lblMontoTotalFuenteRO.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[0]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalFuenteRD.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[3]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalFuenteROOC.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[2]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalFuenteDYT.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[1]["SumMontoTransferidoXDisp"])).ToString("N");

            //Colocando grilla de financiamiento - https://trello.com/c/uqVY2SZM/41-obras
            DataTable dtFinanciamiento = new DataTable();
            dtFinanciamiento = ds.Tables[4];

            if (dtFinanciamiento.Rows.Count > 0)
                grd_ResumConvenioTab0.DataSource = dtFinanciamiento;
            else
                grd_ResumConvenioTab0.DataSource = new DataTable();

            grd_ResumConvenioTab0.DataBind();
        }
        protected Boolean validaArchivoFotos(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".bmp" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".jpg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar solo fotos en formato: BMP, JPG, JPEG o PNG.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaArchivo(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected void GeneraIcoFile(string lnk, ImageButton imgbtn)
        {
            if (lnk != "")
            {

                string ext = lnk.Substring(lnk.Length - 3, 3);

                ext = ext.ToLower();

                if (ext == "pdf")
                {
                    imgbtn.ImageUrl = "~/img/pdf.gif";
                }

                if (ext == "xls" || ext == "lsx")
                {
                    imgbtn.ImageUrl = "~/img/xls.gif";

                }

                if (ext == "doc" || ext == "ocx")
                {
                    imgbtn.ImageUrl = "~/img/doc.gif";

                }

                if (ext == "png" || ext == "PNG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }
                if (ext == "jpg" || ext == "JPG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                imgbtn.ToolTip = lnk;
            }
            else
            {
                imgbtn.ToolTip = "";
                imgbtn.ImageUrl = "~/img/blanco.png";
            }

        }

        #region Tab0

        protected void CargaTab0(int idproy)
        {


            //CargaTipoAporteTab0();
            CargaTipoFinanTransferenciaTab0();
            CargaTipoTransferenciaTab0();
            CargaTipoDocSustento();
            CargaTipoFaseProceso();



            _BEFinanciamiento.id_proyecto = idproy;
            _BEFinanciamiento.tipoFinanciamiento = 2;

            //dt = _objBLFinanciamiento.spMON_infoFinanciamiento(_BEFinanciamiento);
            //if (dt.Rows.Count > 0)
            //{
            //   txtMontoSNIPTab0.Text = dt.Rows[0]["montoMVCS"].ToString();
            //}

            //CargaContrapartidaTab0();


            CargaFinanTransferencia();
            CargaSOSEMEjecutorasTab0();


        }

        //protected void CargaContrapartidaTab0()
        //{
        //    _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEFinanciamiento.tipoFinanciamiento = 2;

        //    grdContrapartidasTab0.DataSource = _objBLFinanciamiento.F_spMON_FinanContrapartida(_BEFinanciamiento);
        //    grdContrapartidasTab0.DataBind();

        //    totalMontoGrdContrapartida();


        //    double val1 = 0;
        //    //if (txtAcumuladogrdContraTab0.Text != "")
        //    //{ val1 = Convert.ToDouble(txtAcumuladogrdContraTab0.Text); }

        //    double val2 = 0;
        //    if (txtMontoSNIPTab0.Text != "")
        //    { val2 = Convert.ToDouble(txtMontoSNIPTab0.Text); }

        //    //@TODO TOTAL INVERSION COMENTADO
        //    //txtTotalInversionTab0.Text = (val1 + val2).ToString("N");


        //}

        protected void CargaFinanTransferencia()
        {
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEFinanciamiento.tipoFinanciamiento = 2;

            grd_TransferenciasTab0.DataSource = _objBLFinanciamiento.F_spMON_FinanTransferencia(_BEFinanciamiento);
            grd_TransferenciasTab0.DataBind();

            // totalMontoGrdTransferenciaTab0();

        }

        protected void grd_TransferenciasTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            double total;
            total = 0;

            double totalGirado;
            totalGirado = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTransTab0");

                Label lblMonto = (Label)e.Row.FindControl("lblMontoAprob");
                total = Convert.ToDouble(txtAcumuladorTranferenciaTab0.Text) + Convert.ToDouble(lblMonto.Text);
                txtAcumuladorTranferenciaTab0.Text = total.ToString("N");

                Label lblMontoGirado = (Label)e.Row.FindControl("lblMontoGirado");
                totalGirado = Convert.ToDouble(txtTotalGiradoTab0.Text) + Convert.ToDouble(lblMontoGirado.Text);
                txtTotalGiradoTab0.Text = totalGirado.ToString("N");

                //@TODO TOTAL INVERSION COMENTADO
                /*if (Convert.ToDouble(txtTotalInversionTab0.Text) < total)
                {
                    lblAlerta2Tab0.Visible = true;
                }
                else
                {
                    lblAlerta2Tab0.Visible = false;
                }*/

                GeneraIcoFile(imb.ToolTip, imb);


            }
        }
    
        //protected void CargaTipoAporteTab0()
        //{
        //    ddlTipoAporteTab0.DataSource = _objBLFinanciamiento.F_spMON_TipoAporte();
        //    ddlTipoAporteTab0.DataTextField = "nombre";
        //    ddlTipoAporteTab0.DataValueField = "valor";
        //    ddlTipoAporteTab0.DataBind();

        //    ddlTipoAporteTab0.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}

        protected void CargaTipoFinanTransferenciaTab0()
        {
            ddlTipoTab0.DataSource = _objBLFinanciamiento.F_spMON_TipoFInanTransferencia();
            ddlTipoTab0.DataTextField = "nombre";
            ddlTipoTab0.DataValueField = "valor";
            ddlTipoTab0.DataBind();

            ddlTipoTab0.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        //protected void btnAgregarContrapartidasTab0_OnClick(object sender, EventArgs e)
        //{
        //    btnModificarContrapartidasTab0.Visible = false;
        //    btnGuardarContrapartidasTab0.Visible = true;
        //    Panel_ContrapartidaTab0.Visible = true;
        //    btnAgregarContrapartidasTab0.Visible = false;
        //    lblNomUsuarioContrapartida.Text = "";
        //    Up_Tab0.Update();

        //}

        //protected void totalMontoGrdContrapartida()
        //{
        //    _BEFinanciamiento.tipoGrd = 3;
        //    _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    txtAcumuladogrdContraTab0.Text = _objBLFinanciamiento.F_spMON_MontoTotalGrd(_BEFinanciamiento);
        //    if (txtAcumuladogrdContraTab0.Text != "")
        //    {
        //        txtAcumuladogrdContraTab0.Text = Convert.ToDouble(txtAcumuladogrdContraTab0.Text).ToString("N2");
        //    }

        //}
        
        protected void CargaTipoTransferenciaTab0()
        {
            //    ddlTipoTransferenciaTab0.DataSource = _objBLFinanciamiento.spMON_TipoAp();
            //    ddlTipoTransferenciaTab0.DataTextField = "nombre";
            //    ddlTipoTransferenciaTab0.DataValueField = "valor";
            //    ddlTipoTransferenciaTab0.DataBind();


            ddlTipoTransferenciaTab0.Items.Insert(0, new ListItem("Presupuestal", "2"));
            ddlTipoTransferenciaTab0.Items.Insert(0, new ListItem("Financiera", "1"));

            ddlTipoTransferenciaTab0.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoFaseProceso()
        {
            ddlFaseProcesoTab01.DataSource = _objBLFinanciamiento.F_spMON_TipoProcesoFinanciero();
            ddlFaseProcesoTab01.DataTextField = "nombre";
            ddlFaseProcesoTab01.DataValueField = "valor";
            ddlFaseProcesoTab01.DataBind();

            ddlFaseProcesoTab01.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoDocSustento()
        {
            ddlTipoDocSustentoTab01.DataSource = _objBLFinanciamiento.F_spMON_TipoDocumentoSustento();
            ddlTipoDocSustentoTab01.DataTextField = "nombre";
            ddlTipoDocSustentoTab01.DataValueField = "valor";
            ddlTipoDocSustentoTab01.DataBind();

            ddlTipoDocSustentoTab01.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }
     
        //protected void btnGuardarContrapartidasTab0_OnClick(object sender, EventArgs e)
        //{
        //    if (ValidarContrapartidaTab0())
        //    {

        //        _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEFinanciamiento.tipoFinanciamiento = 2;
        //        _BEFinanciamiento.entidad_cooperante = txtEntidadCooperanteTab0.Text;
        //        _BEFinanciamiento.documento = txtDocumentoTab0.Text;
        //        _BEFinanciamiento.monto = txtMontoTab0.Text;
        //        _BEFinanciamiento.id_tipo_aporte = Convert.ToInt32(ddlTipoAporteTab0.SelectedValue.ToString());
        //        _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEFinanciamiento.MontoObra = txtMontoObraTab0.Text;
        //        _BEFinanciamiento.MontoSupervision = txtMontoSupervisionTab0.Text;
        //        int val = _objBLFinanciamiento.spi_MON_FinanContrapartida(_BEFinanciamiento);


        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Registro Correcto.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            CargaContrapartidaTab0();

        //            txtEntidadCooperanteTab0.Text = "";
        //            txtDocumentoTab0.Text = "";
        //            txtMontoTab0.Text = "";
        //            ddlTipoAporteTab0.SelectedValue = "";

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }
        //        Panel_ContrapartidaTab0.Visible = false;
        //        btnAgregarContrapartidasTab0.Visible = true;
        //        Up_Tab0.Update();
        //    }
        //}

        //protected void btnCancelarContrapartidasTab0_OnClick(object sender, EventArgs e)
        //{
        //    Panel_ContrapartidaTab0.Visible = false;
        //    btnAgregarContrapartidasTab0.Visible = true;
        //    Up_Tab0.Update();

        //}


        protected void btnAgregarTransferenciaTab0_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarTranasferenciaTab0.Visible = true;
            btnAgregarTransferenciaTab0.Visible = false;
            btnGuardarTransferenciaTab0.Visible = true;
            btnModificarTransparenciaTab0.Visible = false;
            lblNomUsuarioTransferencia.Text = "";
            Up_Tab0.Update();
        }

        //protected Boolean ValidarContrapartidaTab0()
        //{
        //    Boolean result;
        //    result = true;

        //    if (txtEntidadCooperanteTab0.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese contrapartida.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    //if (txtDocumentoTab0.Text == "")
        //    //{
        //    //    string script = "<script>alert('Ingrese documento.');</script>";
        //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    //    result = false;
        //    //    return result;

        //    //}

        //    if (ddlTipoAporteTab0.SelectedValue == "")
        //    {
        //        string script = "<script>alert('Seleccione tipo de aporte.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }


        //    return result;

        //}
        protected Boolean ValidarTransferenciaTab0()
        {
            Boolean result;
            result = true;

            if (ddlTipoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtNroTransTab0.Text == "")
            {
                string script = "<script>alert('Ingrese número de convenio o adenda');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtFechaNroTransTab0.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de convenio o adenda');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoNroTransTab0.Text == "")
            {
                string script = "<script>alert('Ingrese monto de convenio o adenda');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtDispositivoTransTab0.Text == "")
            {
                string script = "<script>alert('Ingrese dispositivo de aprobación');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaDispTab0.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de dispositivo de aprobación');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoTransTab0.Text == "")
            {
                string script = "<script>alert('Ingrese monto aprobado');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlTipoTransferenciaTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar Tipo de Transferencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;

        }

        protected void CargaSOSEMEjecutorasTab0()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSOSEMEjecutoras.DataSource = _objBLFinanciamiento.F_spMON_SosemEjecutora(snip);
            grdSOSEMEjecutoras.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }

        protected void CargaSOSEMDevengadoMensualizadoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdDetalleSOSEMTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemDevengadoMensualizado(snip, idEjecutora);
            grdDetalleSOSEMTab0.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }

        
        protected void CargaSOSEMFuenteFinanciamientoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdFuenteFinanciamientoTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemFuenteFinanciamiento(snip, idEjecutora);
            grdFuenteFinanciamientoTab0.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }
        //protected Boolean ValidarSOSEMTab0()
        //{
        //    Boolean result;
        //    result = true;

        //    if (ddlanioSOSEMTAb0.SelectedValue == "")
        //    {
        //        string script = "<script>alert('Seleccione Año');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }


        //    return result;

        //}
        protected void btnGuardarTransferenciaTab0_OnClick(object sender, EventArgs e)
        {

            if (ValidarTransferenciaTab0())
            {
                if (validaArchivo(FileUploadTransferenciaTab0))
                {
                    _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEFinanciamiento.tipoFinanciamiento = 2;
                    _BEFinanciamiento.id_tipo_trans = Convert.ToInt32(ddlTipoTab0.SelectedValue);
                    _BEFinanciamiento.nro_convenio = txtNroTransTab0.Text;
                    _BEFinanciamiento.Date_Fecha_Convenio = Convert.ToDateTime(txtFechaNroTransTab0.Text);
                    _BEFinanciamiento.montoConvenio = txtMontoNroTransTab0.Text;
                    _BEFinanciamiento.dispositivo_aprobacion = txtDispositivoTransTab0.Text;
                    _BEFinanciamiento.Date_fecha_aprobacion = Convert.ToDateTime(txtFechaDispTab0.Text);
                    _BEFinanciamiento.montoAprobacion = txtMontoTransTab0.Text;
                    _BEFinanciamiento.Tipo_transferencia = Convert.ToInt32(ddlTipoTransferenciaTab0.SelectedValue);
                    _BEFinanciamiento.observacion = txtObservacionTransTab0.Text;
                    _BEFinanciamiento.docUrl = _Metodo.uploadfile(FileUploadTransferenciaTab0);
                    _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int resul;
                    resul = _objBLFinanciamiento.spi_MON_FinanTransferencia(_BEFinanciamiento);
                    if (resul == 1)
                    {
                        string script = "<script>alert('Registro Correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaFinanTransferencia();

                        ddlTipoTab0.SelectedValue = "";
                        txtNroTransTab0.Text = "";
                        txtFechaNroTransTab0.Text = "";
                        txtMontoNroTransTab0.Text = "";
                        txtDispositivoTransTab0.Text = "";
                        txtFechaDispTab0.Text = "";
                        txtMontoTransTab0.Text = "";
                        txtObservacionTransTab0.Text = "";

                        Panel_AgregarTranasferenciaTab0.Visible = false;
                        btnAgregarTransferenciaTab0.Visible = true;
                        Up_Tab0.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }



                }


            }
        }

        protected void imgDocTransTab0_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grd_TransferenciasTab0.Rows[row.RowIndex].FindControl("imgDocTransTab0");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void btnCancelarTransferenciaTab0_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarTranasferenciaTab0.Visible = false;
            btnAgregarTransferenciaTab0.Visible = true;
            Up_Tab0.Update();

        }

        //protected void imgbtn_agregarSOSEM_OnClick(object sender, EventArgs e)
        //{
        //    Panel_AgregarSOSEM.Visible = true;
        //    imgbtn_agregarSOSEM.Visible = false;
        //    lblNomUsuarioSosem.Text = "";
        //    Up_Tab0.Update();
        //}

        //protected void btnCancelarRegistroSOSEM_OnClick(object sender, EventArgs e)
        //{
        //    Panel_AgregarSOSEM.Visible = false;
        //    imgbtn_agregarSOSEM.Visible = true;
        //    Up_Tab0.Update();

        //}


        //protected void imgbtn_AgregarSeguimientoTab0_OnClick(object sender, EventArgs e)
        //{
        //    Panel_AgregarSOSEM.Visible = true;
        //    imgbtn_agregarSOSEM.Visible = false;
        //    Up_Tab0.Update();
        //}

        //protected void btnGuardarRegistroSOSEMTab0_OnClick(object sender, EventArgs e)
        //{
        //    if (ValidarSOSEMTab0())
        //    {
        //        _BESOSEM.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BESOSEM.tipoFinanciamiento = 2;

        //        _BESOSEM.anio = ddlanioSOSEMTAb0.SelectedValue;
        //        _BESOSEM.pia = txtPiaSOSEMTab0.Text;
        //        _BESOSEM.pimAcumulado = txtPimAcumTab0.Text;
        //        _BESOSEM.devAcumulado = txtDevAcumTab0.Text;
        //        _BESOSEM.ene = txtEneSosTab0.Text;
        //        _BESOSEM.feb = txtFebSosTab0.Text;
        //        _BESOSEM.mar = txtMarSosTab0.Text;
        //        _BESOSEM.abr = txtAbrSosTab0.Text;
        //        _BESOSEM.may = txtMaySosTab0.Text;
        //        _BESOSEM.jun = txtJunSosTab0.Text;
        //        _BESOSEM.jul = txtJulSosTab0.Text;
        //        _BESOSEM.ago = txtAgoSosTab0.Text;
        //        _BESOSEM.sep = txtSepSosTab0.Text;
        //        _BESOSEM.oct = txtOctSosTab0.Text;
        //        _BESOSEM.nov = txtNovSosTab0.Text;
        //        _BESOSEM.dic = txtDicSosTab0.Text;
        //        _BESOSEM.compromisoAnual = txtComproAnualTab0.Text;

        //        _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLSOSEM.spi_MON_FinanSOSEM(_BESOSEM);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Registro Correcto.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            CargaFinanSOSEMTab0();
        //            ddlanioSOSEMTAb0.SelectedValue = "";
        //            txtPiaSOSEMTab0.Text = "0";
        //            txtPimAcumTab0.Text = "0";
        //            txtDevAcumTab0.Text = "0";
        //            txtEneSosTab0.Text = "0";
        //            txtFebSosTab0.Text = "0";
        //            txtMarSosTab0.Text = "0";
        //            txtAbrSosTab0.Text = "0";
        //            txtMaySosTab0.Text = "0";
        //            txtJunSosTab0.Text = "0";
        //            txtJulSosTab0.Text = "0";
        //            txtAgoSosTab0.Text = "0";
        //            txtSepSosTab0.Text = "0";
        //            txtOctSosTab0.Text = "0";
        //            txtNovSosTab0.Text = "0";
        //            txtDicSosTab0.Text = "0";

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //        Panel_AgregarSOSEM.Visible = false;
        //        imgbtn_agregarSOSEM.Visible = true;
        //        Up_Tab0.Update();
        //    }

        //}

        protected void lnkdispo_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grd_TransferenciasTab0.DataKeys[row.RowIndex].Value;


            //  id_cartaOrden.Text = grdCartaOrden.SelectedDataKey.Value.ToString();


            string idTipoTransferencia = ((Label)row.FindControl("lblIdTipoTransferencia")).Text;


            if (idTipoTransferencia == "")
            {
                string script = "<script>alert('Registrar tipo de transferencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


            }
            else
            {
                if (idTipoTransferencia == "1")  // Transferencia Financiera
                {
                    lblNombTituloTranferenciaTab0.Text = "TRANSFERENCIA EN MODALIDAD FINANCIERA";
                    lblNombTituloTranferenciaTab0_2.Text = "Unidad Ejecutora que afecta su presupuesto - Unidad Ejecutora que recibe en Cuenta Bancaria la transferencia";
                    Panel_Financiera.Visible = true;
                    Panel_Presupuestal.Visible = false;
                    string IdFinanciera = ((Label)row.FindControl("lblIdFinanciera")).Text;
                    string fechaAprobacion = ((Label)row.FindControl("lblFechaAprobación")).Text;
                    string montoaprobado = ((Label)row.FindControl("lblMontoAprob")).Text;

                    txtCodigoPIPTab01.Text = lblSNIP.Text;
                    txtProyectoTab01.Text = lblNombProy.Text;
                    txtNormaTab01.Text = ((LinkButton)row.FindControl("lnkdispo")).Text;
                    txtMontoAutorizadoTab01.Text = Convert.ToDouble(montoaprobado).ToString("N");
                    lblID_transferencia_financiera.Text = IdFinanciera;

                    CargaFinancieraTab01(IdFinanciera);

                    if (IdFinanciera == "")
                    { CargaProcesoFinancieraTab01(0); }
                    else
                    {
                        CargaProcesoFinancieraTab01(Convert.ToInt32(IdFinanciera));
                    }


                }
                else
                {
                    if (idTipoTransferencia == "2")  // Transferencia Presupuestal
                    {
                        lblNombTituloTranferenciaTab0.Text = "TRANSFERENCIA EN MODALIDAD PRESUPUESTAL";
                        lblNombTituloTranferenciaTab0_2.Text = "Unidad Ejecutora que habilita - Unidad Ejecutora habilitada, en terminos presupuestales";

                        Panel_Financiera.Visible = false;
                        Panel_Presupuestal.Visible = true;

                        string IdPresupuestal = ((Label)row.FindControl("lblIdPresupuestal")).Text;
                        string fechaAprobacion = ((Label)row.FindControl("lblFechaAprobación")).Text;
                        string montoaprobado = ((Label)row.FindControl("lblMontoAprob")).Text;


                        txtCodigoPIPTab02.Text = lblSNIP.Text;
                        txtProyectoTab02.Text = lblNombProy.Text;
                        txtNormaTab02.Text = ((LinkButton)row.FindControl("lnkdispo")).Text;
                        txtMontoAutorizadoTab02.Text = Convert.ToDouble(montoaprobado).ToString("N");
                        lblID_transferencia_presupuestal.Text = IdPresupuestal;

                        // CargaPresupuestalTab01(lblID_transferencia_presupuestal.Text);
                        CargaPresupuestalTab01(IdPresupuestal);

                    }


                }


                //id_finanTransferencia.Text = id.ToString();
                //_BEFinanciamiento.Id_finaTransferencia = Convert.ToInt32(id.ToString());
                //dt = _objBLFinanciamiento.spMON_CartaOrden(_BEFinanciamiento);

                //grdCartaOrden.DataSource = dt;
                //grdCartaOrden.DataBind();

                // imgbtnAgregarCartaOrden.Visible = true;
                //Up_CartaOrden.Update();
                //MPE_CartaOrden.Show();

                ImgbtnAgregarFinancieraTab01.Visible = true;

                if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) != 68)
                {
                    ImgbtnAgregarFinancieraTab01.Visible = false;
                    //grdCartaOrden.Columns[0].Visible = false;
                    // grdCartaOrden.Columns[12].Visible = false;

                }

                string script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                Panel_RegistroFinancieraTab01.Visible = false;

                Up_CartaOrden.Update();
                Up_Fondo.Update();

            }







        }
        protected void btnCancelarFinancieraTab01_OnClick(object sender, EventArgs e)
        {
            Panel_RegistroFinancieraTab01.Visible = false;
            ImgbtnAgregarFinancieraTab01.Visible = true;
            //Up_CartaOrden.Update();
            if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) != 68)
            {
                ImgbtnAgregarFinancieraTab01.Visible = false;

            }
        }




        protected void btnGuardarFinancieraTab01_OnClick(object sender, EventArgs e)
        {
            if (ddlFaseProcesoTab01.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar fase de proceso.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEFinanciamiento.Id_transferenciaFinanciera = Convert.ToInt32(lblID_transferencia_financiera.Text);
                _BEFinanciamiento.Id_tipoProcesoFinanciero = Convert.ToInt32(ddlFaseProcesoTab01.SelectedValue);
                _BEFinanciamiento.NroRegistro = txtNroRegistro01.Text;
                _BEFinanciamiento.Date_fechaRegistro = VerificaFecha(txtFechaRegistroTab01.Text);
                _BEFinanciamiento.MontoRegistrado = txtmontoRegistradoTab01.Text;

                _BEFinanciamiento.Id_tipoDocumentoSustento = Convert.ToInt32(ddlTipoDocSustentoTab01.SelectedValue);
                _BEFinanciamiento.NroDocumentoSustento = txtNroDocSustentoTab01.Text;
                _BEFinanciamiento.DocumentoPago = txtDocumentoPagoTab01.Text;
                _BEFinanciamiento.NroDocPago = txtNroDocPagoTab01.Text;
                _BEFinanciamiento.Date_fechaDocPago = VerificaFecha(txtfechaDocPagoTab01.Text);
                _BEFinanciamiento.Banco = (txtBancoTab01.Text);
                _BEFinanciamiento.NroCuentaBanco = txtNroCuentaTab01.Text;

                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLFinanciamiento.spi_MON_ProcesoFinanciero(_BEFinanciamiento);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaProcesoFinancieraTab01(Convert.ToInt32(lblID_transferencia_financiera.Text));

                    ImgbtnAgregarFinancieraTab01.Visible = true;

                    Panel_RegistroFinancieraTab01.Visible = false;
                    Up_CartaOrden.Update();
                    // Up_Fondo.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }


            }

        }

        protected void btnGuardarTransferenciaFinancieraTab01_OnClick(object sender, EventArgs e)
        {
            _BEFinanciamiento.Id_transferenciaFinanciera = Convert.ToInt32(lblID_transferencia_financiera.Text);
            _BEFinanciamiento.Meta = txtMetaTab01.Text;
            _BEFinanciamiento.Cadena = txtCadenaTab01.Text;
            _BEFinanciamiento.Fte = txtFteTab01.Text;
            _BEFinanciamiento.UE_destino = txtUEDestinoTab01.Text;
            _BEFinanciamiento.AnioFiscal = txtAnioTab01.Text;

            if (txtGiradoTab01.Text == "")
            {
                txtGiradoTab01.Text = "0";
            }
            _BEFinanciamiento.Girado = txtGiradoTab01.Text;


            _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLFinanciamiento.spu_MON_TransferenciaFinanciero(_BEFinanciamiento);

            if (val == 1)
            {
                string script = "<script>alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                CargaFinancieraTab01(lblID_transferencia_financiera.Text);

                //script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                // Up_CartaOrden.Update();
                // Up_Fondo.Update();


            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void btnGuardarTransferenciaPresupuestalTab01_OnClick(object sender, EventArgs e)
        {
            _BEFinanciamiento.Id_transferencia_presupuesta = Convert.ToInt32(lblID_transferencia_presupuestal.Text);
            _BEFinanciamiento.EjecutoraHabilita = txtEjecutoraHabilitaTab02.Text;
            _BEFinanciamiento.NotaModificatoria = txtNotaModificatoriaTab02.Text;
            _BEFinanciamiento.Date_Fecha = VerificaFecha(txtFechaTab02.Text);
            _BEFinanciamiento.EjecutoraHabilitada = txtEjecutoraHabilitatada02.Text;

            if (txtMontoHabilitadoTab02.Text == "")
            {
                txtMontoHabilitadoTab02.Text = "0";
            }
            _BEFinanciamiento.CodigoPresupuestal = txtCodigoPresupuestalTab02.Text;
            _BEFinanciamiento.MontoHabilitado = txtMontoHabilitadoTab02.Text;
            _BEFinanciamiento.FteFinanciamiento = txtFteTab02.Text;
            _BEFinanciamiento.AnioFiscal = txtAnioTab02.Text;
            if (chkbHabilitoTab02.Checked == true)
            {
                _BEFinanciamiento.FlagHabilito = 1;

            }
            else
            {
                _BEFinanciamiento.FlagHabilito = 0;
            }



            _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLFinanciamiento.spu_MON_TransferenciaPresupuestal(_BEFinanciamiento);

            if (val == 1)
            {
                string script = "<script>alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                CargaPresupuestalTab01(lblID_transferencia_presupuestal.Text);

                //script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                // Up_CartaOrden.Update();
                // Up_Fondo.Update();


            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void btnModificarFinancieraTab01_OnClick(object sender, EventArgs e)
        {
            if (ddlFaseProcesoTab01.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar fase de proceso.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEFinanciamiento.Id_proceso_financiero = Convert.ToInt32(id_proceso_financiero.Text);
                _BEFinanciamiento.tipo = 1;
                _BEFinanciamiento.Id_tipoProcesoFinanciero = Convert.ToInt32(ddlFaseProcesoTab01.SelectedValue);
                _BEFinanciamiento.NroRegistro = txtNroRegistro01.Text;
                _BEFinanciamiento.Date_fechaRegistro = VerificaFecha(txtFechaRegistroTab01.Text);
                _BEFinanciamiento.MontoRegistrado = txtmontoRegistradoTab01.Text;

                _BEFinanciamiento.Id_tipoDocumentoSustento = Convert.ToInt32(ddlTipoDocSustentoTab01.SelectedValue);
                _BEFinanciamiento.NroDocumentoSustento = txtNroDocSustentoTab01.Text;
                _BEFinanciamiento.DocumentoPago = txtDocumentoPagoTab01.Text;
                _BEFinanciamiento.NroDocPago = txtNroDocPagoTab01.Text;
                _BEFinanciamiento.Date_fechaDocPago = VerificaFecha(txtfechaDocPagoTab01.Text);
                _BEFinanciamiento.Banco = (txtBancoTab01.Text);
                _BEFinanciamiento.NroCuentaBanco = txtNroCuentaTab01.Text;

                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLFinanciamiento.spud_MON_ProcesoFinanciero(_BEFinanciamiento);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //_BEFinanciamiento.Id_finaTransferencia = Convert.ToInt32(id_finanTransferencia.Text);
                    //dt = _objBLFinanciamiento.spMON_CartaOrden(_BEFinanciamiento);
                    //grdCartaOrden.DataSource = dt;
                    //grdCartaOrden.DataBind();

                    CargaProcesoFinancieraTab01(Convert.ToInt32(lblID_transferencia_financiera.Text));

                    //script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroFinancieraTab01.Visible = false;
                    ImgbtnAgregarFinancieraTab01.Visible = true;

                    // Up_CartaOrden.Update();
                    // Up_Fondo.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }

        protected void ImgbtnAgregarFinancieraTab01_OnClick(object sender, EventArgs e)
        {
            Panel_RegistroFinancieraTab01.Visible = true;
            Up_CartaOrden.Update();
            ImgbtnAgregarFinancieraTab01.Visible = false;
            btnModificarFinancieraTab01.Visible = false;
            btnGuardarFinancieraTab01.Visible = true;

            lblNomUsuarioFinancieraTab01.Text = "";
        }


        //protected void imgbtnAgregarCartaOrden_OnClick(object sender, EventArgs e)
        //{
        //    Panel_RegistroCartaOrden.Visible = true;
        //    imgbtnAgregarCartaOrden.Visible = false;
        //    btnModificarCartaOrden.Visible = false;
        //    btnGuardarCartaOrden.Visible = true;
        //    txtSIAF.Text = "";
        //    txtMntCompromiso.Text = "";
        //    txtMntDevengado.Text = "";
        //    txtMntGirado.Text = "";
        //    txtFechaDevengado.Text = "";
        //    txtFechaGirado.Text = "";
        //    txtFechaCompromiso.Text = "";
        //    lblNomUsuarioCartaOrden.Text = "";
        //    txtMntCompromiso.Text = "";
        //    txtNroComprobante.Text = "";
        //    txtFechaComprobante.Text = "";
        //    txtFechaTransferencia.Text = "";
        //    txtFechaEmision.Text = "";
        //    txtCadena.Text = "";
        //    txtMeta.Text = "";
        //    txtComentario.Text = "";
        //    txtNroCarta.Text = "";
        //    txtFechaCarta.Text = "";
        //    //Up_CartaOrden.Update();
        //}

        //protected void grdCartaOrden_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //if (e.CommandName == "eliminar")
        //{
        //    Control ctl = e.CommandSource as Control;
        //    GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //    object objTemp = grdCartaOrden.DataKeys[CurrentRow.RowIndex].Value as object;

        //    _BEFinanciamiento.Id_CartaOrden = Convert.ToInt32(objTemp.ToString());
        //    _BEFinanciamiento.tipo = 2;
        //    _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //    _BEFinanciamiento.Date_fechaCartaOrden = Convert.ToDateTime("09/09/1999");
        //    _BEFinanciamiento.Date_fechaComprobante = Convert.ToDateTime("09/09/1999");
        //    _BEFinanciamiento.Date_FechaCompromiso = Convert.ToDateTime("09/09/1999");
        //    _BEFinanciamiento.Date_fechaDevengado = Convert.ToDateTime("09/09/1999");
        //    _BEFinanciamiento.Date_fechaTransferencia = Convert.ToDateTime("09/09/1999");
        //    _BEFinanciamiento.Date_fechaemision = Convert.ToDateTime("09/09/1999");
        //    _BEFinanciamiento.Date_fechaGirado = Convert.ToDateTime("09/09/1999");

        //    int val = _objBLFinanciamiento.spud_MON_CartaOrden(_BEFinanciamiento);

        //    if (val == 1)
        //    {
        //        string script = "<script>alert('Eliminación Correcta.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        _BEFinanciamiento.Id_finaTransferencia = Convert.ToInt32(id_finanTransferencia.Text);
        //        dt = _objBLFinanciamiento.spMON_CartaOrden(_BEFinanciamiento);
        //        grdCartaOrden.DataSource = dt;
        //        grdCartaOrden.DataBind();

        //        script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        Up_CartaOrden.Update();
        //         Up_Fondo.Update();
        //    }
        //    else
        //    {
        //        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    }

        //}
        //}

        //protected void grdCartaOrden_OnSelectedIndexChanged(object sender, EventArgs e)
        //{

        //    id_cartaOrden.Text = grdCartaOrden.SelectedDataKey.Value.ToString();

        //    GridViewRow row = grdCartaOrden.SelectedRow;

        //    txtNroCarta.Text = ((Label)row.FindControl("lblNroCartaOrden")).Text;
        //    txtFechaCarta.Text = ((Label)row.FindControl("lblFechaCartaOrden")).Text;
        //    //txtMntCompromiso.Text = ((Label)row.FindControl("lblMntCompromiso")).Text;
        //    txtMntCompromiso.Text = Server.HtmlDecode(row.Cells[4].Text);
        //    txtMntDevengado.Text = Server.HtmlDecode(row.Cells[6].Text);
        //    txtMntGirado.Text = Server.HtmlDecode(row.Cells[8].Text);
        //    //txtMntDevengado.Text = ((Label)row.FindControl("lblMntDeveng")).Text;
        //    //txtMntGirado.Text = ((Label)row.FindControl("lblMntGirado")).Text;
        //    txtFechaTransferencia.Text = ((Label)row.FindControl("lblFechaTrans")).Text;

        //    txtSIAF.Text = ((Label)row.FindControl("lblSiaf")).Text;
        //    txtFechaCompromiso.Text = ((Label)row.FindControl("lblfechaCompromiso")).Text;
        //    txtFechaDevengado.Text = ((Label)row.FindControl("lblfechaDevengado")).Text;
        //    txtFechaGirado.Text = ((Label)row.FindControl("lblfechaGirado")).Text;

        //    txtNroComprobante.Text = ((Label)row.FindControl("lblNroComprobante")).Text;
        //    txtFechaComprobante.Text = ((Label)row.FindControl("lblfechaComprobante")).Text;
        //    txtFechaEmision.Text = ((Label)row.FindControl("lblfechaEmision")).Text;
        //    txtCadena.Text = ((Label)row.FindControl("lblcadena")).Text;
        //    txtMeta.Text = ((Label)row.FindControl("lblmeta")).Text;
        //    txtComentario.Text = ((Label)row.FindControl("lblComentario")).Text;
        //    lblNomUsuarioCartaOrden.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    Panel_RegistroCartaOrden.Visible = true;
        //    imgbtnAgregarCartaOrden.Visible = false;
        //    btnModificarCartaOrden.Visible = true;
        //    btnGuardarCartaOrden.Visible = false;

        //    if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) != 68)
        //    {
        //        btnModificarCartaOrden.Visible = false;

        //    }
        //}

        //protected void grdFinancieraTab01_OnSelectedIndexChanged(object sender, EventArgs e)
        //{

        //    id_proceso_financiero.Text = grdFinancieraTab01.SelectedDataKey.Value.ToString();

        //    GridViewRow row = grdFinancieraTab01.SelectedRow;

        //    ddlFaseProcesoTab01.SelectedValue = ((Label)row.FindControl("lblID_FaseProceso")).Text;
        //    txtNroRegistro01.Text = ((Label)row.FindControl("lblNroRegistro")).Text;
        //    txtFechaRegistroTab01.Text = ((Label)row.FindControl("lblFechaRegistro")).Text;
        //    //txtMntDevengado.Text = ((Label)row.FindControl("lblMntDeveng")).Text;
        //    //txtMntGirado.Text = ((Label)row.FindControl("lblMntGirado")).Text;
        //    txtmontoRegistradoTab01.Text = ((Label)row.FindControl("lblMntRegistrado")).Text;

        //    ddlTipoDocSustentoTab01.SelectedItem.Text = ((Label)row.FindControl("lblDocSustento")).Text;
        //    txtNroDocSustentoTab01.Text = ((Label)row.FindControl("lblNroDocSustento")).Text;
        //    txtDocumentoPagoTab01.Text = ((Label)row.FindControl("lblDocPago")).Text;
        //    txtNroDocPagoTab01.Text = ((Label)row.FindControl("lblNroDocPago")).Text;

        //    txtfechaDocPagoTab01.Text = ((Label)row.FindControl("lblFechaPago")).Text;
        //    txtBancoTab01.Text = ((Label)row.FindControl("lblBanco")).Text;
        //    txtNroCuentaTab01.Text = ((Label)row.FindControl("lblCuentaBanco")).Text;

        //    lblNomUsuarioFinancieraTab01.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    Panel_RegistroFinancieraTab01.Visible = true;
        //    ImgbtnAgregarFinancieraTab01.Visible = false;
        //    btnModificarFinancieraTab01.Visible = true;
        //    btnGuardarFinancieraTab01.Visible = false;

        //    if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) != 68)
        //    {
        //        btnModificarFinancieraTab01.Visible = false;

        //    }

        //    Up_Fondo.Update();

        //}

        protected void grdFinancieraTab01_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "editar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdFinancieraTab01.DataKeys[CurrentRow.RowIndex].Value as object;
                id_proceso_financiero.Text = objTemp.ToString();
                GridViewRow row = CurrentRow;

                ddlFaseProcesoTab01.SelectedValue = ((Label)row.FindControl("lblID_FaseProceso")).Text;

                if (ddlFaseProcesoTab01.SelectedValue == "5")
                {
                    txtDocumentoPagoTab01.Visible = true;
                    txtNroDocPagoTab01.Visible = true;
                    txtfechaDocPagoTab01.Visible = true;
                    txtBancoTab01.Visible = true;
                    txtNroCuentaTab01.Visible = true;

                    lblDocumentoPagoTab01.Visible = true;
                    lblNroDocPagoTab01.Visible = true;
                    lblfechaDocPagoTab01.Visible = true;
                    lblBancoTab01.Visible = true;
                    lblNroCuentaTab01.Visible = true;

                }
                else
                {
                    txtDocumentoPagoTab01.Visible = false;
                    txtNroDocPagoTab01.Visible = false;
                    txtfechaDocPagoTab01.Visible = false;
                    txtBancoTab01.Visible = false;
                    txtNroCuentaTab01.Visible = false;

                    lblDocumentoPagoTab01.Visible = false;
                    lblNroDocPagoTab01.Visible = false;
                    lblfechaDocPagoTab01.Visible = false;
                    lblBancoTab01.Visible = false;
                    lblNroCuentaTab01.Visible = false;

                }

                txtNroRegistro01.Text = ((Label)row.FindControl("lblNroRegistro")).Text;
                txtFechaRegistroTab01.Text = ((Label)row.FindControl("lblFechaRegistro")).Text;
                //txtMntDevengado.Text = ((Label)row.FindControl("lblMntDeveng")).Text;
                //txtMntGirado.Text = ((Label)row.FindControl("lblMntGirado")).Text;
                txtmontoRegistradoTab01.Text = ((Label)row.FindControl("lblMntRegistrado")).Text;

                ddlTipoDocSustentoTab01.SelectedValue = ((Label)row.FindControl("lblIDDocSustento")).Text;
                txtNroDocSustentoTab01.Text = ((Label)row.FindControl("lblNroDocSustento")).Text;
                txtDocumentoPagoTab01.Text = ((Label)row.FindControl("lblDocPago")).Text;
                txtNroDocPagoTab01.Text = ((Label)row.FindControl("lblNroDocPago")).Text;

                txtfechaDocPagoTab01.Text = ((Label)row.FindControl("lblFechaPago")).Text;
                txtBancoTab01.Text = ((Label)row.FindControl("lblBanco")).Text;
                txtNroCuentaTab01.Text = ((Label)row.FindControl("lblCuentaBanco")).Text;

                lblNomUsuarioFinancieraTab01.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

                Panel_RegistroFinancieraTab01.Visible = true;
                ImgbtnAgregarFinancieraTab01.Visible = false;
                btnModificarFinancieraTab01.Visible = true;
                btnGuardarFinancieraTab01.Visible = false;

                if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) != 68)
                {
                    btnModificarFinancieraTab01.Visible = false;

                }

                //  Up_Fondo.Update();

            }
            else
            {
                if (e.CommandName == "eliminar")
                {
                    Control ctl = e.CommandSource as Control;
                    GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                    object objTemp = grdFinancieraTab01.DataKeys[CurrentRow.RowIndex].Value as object;
                    id_proceso_financiero.Text = objTemp.ToString();

                    _BEFinanciamiento.Id_proceso_financiero = Convert.ToInt32(id_proceso_financiero.Text);
                    _BEFinanciamiento.tipo = 2;

                    _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLFinanciamiento.spud_MON_ProcesoFinanciero(_BEFinanciamiento);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se eliminó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                        CargaProcesoFinancieraTab01(Convert.ToInt32(lblID_transferencia_financiera.Text));

                        Panel_RegistroFinancieraTab01.Visible = false;
                        ImgbtnAgregarFinancieraTab01.Visible = true;


                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                }

            }




        }

        protected void CargaPresupuestalTab01(string id_transferenciaPresupuestal)
        {
            _BEFinanciamiento.Id_transferencia_presupuesta = Convert.ToInt32(id_transferenciaPresupuestal);

            dt = _objBLFinanciamiento.spMON_TransferenciaPresupuestal(_BEFinanciamiento);

            if (dt.Rows.Count > 0)
            {
                lblID_transferencia_presupuestal.Text = dt.Rows[0]["id_transferencia_presupuestal"].ToString();
                txtEjecutoraHabilitaTab02.Text = dt.Rows[0]["EjecutoraHabilita"].ToString();
                txtNotaModificatoriaTab02.Text = dt.Rows[0]["NotaModificatoria"].ToString();
                txtFechaTab02.Text = dt.Rows[0]["fecha"].ToString();
                txtEjecutoraHabilitatada02.Text = dt.Rows[0]["EjecutoraHabilitada"].ToString();
                txtCodigoPresupuestalTab02.Text = dt.Rows[0]["CodigoPresupuestal"].ToString();
                txtMontoHabilitadoTab02.Text = dt.Rows[0]["MontoHabilitado"].ToString();
                txtFteTab02.Text = dt.Rows[0]["FteFinanciamiento"].ToString();

                txtAnioTab02.Text = dt.Rows[0]["anioFiscal"].ToString();

                string valor = dt.Rows[0]["flagHabilito"].ToString();
                if (valor == "1")
                {
                    chkbHabilitoTab02.Checked = true;

                }
                else
                {
                    chkbHabilitoTab02.Checked = false;

                }


                lblNomUsuarioTransferenciaPresupuestalTab01.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

            }


        }

        protected void CargaFinancieraTab01(string id)
        {
            _BEFinanciamiento.Id = id;

            List<BE_MON_Financiamiento> ListTransFinanciera = _objBLFinanciamiento.F_spMON_TransferenciaFinanciera(_BEFinanciamiento);


            if (ListTransFinanciera.Count > 0)
            {
                txtMetaTab01.Text = ListTransFinanciera.ElementAt(0).Meta.ToString();
                txtCadenaTab01.Text = ListTransFinanciera.ElementAt(0).Cadena.ToString();
                txtFteTab01.Text = ListTransFinanciera.ElementAt(0).Fte.ToString();
                txtUEDestinoTab01.Text = ListTransFinanciera.ElementAt(0).UE_destino.ToString();
                txtGiradoTab01.Text = ListTransFinanciera.ElementAt(0).Girado.ToString();
                txtAnioTab01.Text = ListTransFinanciera.ElementAt(0).AnioFiscal.ToString();

                lblNomUsuarioTransferenciaFinancieraTab01.Text = "Actualizó: " + ListTransFinanciera.ElementAt(0).usuario.ToString() + " - " + ListTransFinanciera.ElementAt(0).strFecha_update.ToString();

            }


        }

        protected void CargaProcesoFinancieraTab01(int Id_transferenciaFinanciera)
        {
            _BEFinanciamiento.Id_transferenciaFinanciera = Id_transferenciaFinanciera;

            grdFinancieraTab01.DataSource = _objBLFinanciamiento.spMON_ProcesoFinanciero(_BEFinanciamiento);
            grdFinancieraTab01.DataBind();


        }

        #endregion

        #region Tab1

        protected void FechaTermino_OnTextChanged(object sender, EventArgs e)
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);
                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }
        }

        //CAMBIOS

        protected void CargaTab1()
        {
            //Up_Tab1.Visible = true;
            CargaddlTipoAdjudicacionTab1(ddlAdjudicacionTab1);
            CargaddlTipoAdjudicacionTab1(ddlTipoAdjudicacionTab1);
            CargaDllTipoResultado();
            cargaSeguimientoProcesoTab1();

            CargaDatosTab1();
            TabActivoProceso();
            //cargaConsultoresTab1();
        }

      

        protected void ddlProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcesoTab1.SelectedValue != "")
            {
                CargaDatosTab1();
                cargaSeguimientoProcesoTab1();
            }

            TabActivoProceso();
            Up_Tab1.Update();
        }
        protected void ActualizaFechasTab1()
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);
                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }
        }
        protected void CargaDatosTab1()
        {
            lblTitSeace.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " (Obligatorio)";
            lnkbtnSeaceAutoPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " POR SEACE";
            lnkbtnSeaceManualPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " DE FORMA MANUAL";

            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 2;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);

            if (dt.Rows.Count > 0)
            {
                ddlAdjudicacionTab1.SelectedValue = dt.Rows[0]["id_tipoAdjudicacion"].ToString();
                txtNroLicTab1.Text = dt.Rows[0]["NroLic"].ToString();
                TxtAnio2Tab1.Text = dt.Rows[0]["anio"].ToString();
                txtDetalleTab1.Text = dt.Rows[0]["detalle"].ToString();
                txtProTab1.Text = dt.Rows[0]["buenaPro"].ToString();
                txtProConsentidaTab1.Text = dt.Rows[0]["buenaProConsentida"].ToString();
                txtValorReferencialTab1.Text = dt.Rows[0]["valorReferencial"].ToString();
                txtMontoContratadoTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                txtNroContrato.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                txtFechaFirmaContratoTab1.Text = dt.Rows[0]["fechaFirmaContrato"].ToString();

                lblNomActualizaProcesoSeleccion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                string valCheck = dt.Rows[0]["id_tipoEmpresa"].ToString();
                if (valCheck != "")
                {
                    if (Convert.ToInt32(valCheck) == 1)
                    {
                        rbConsultorTab1.Checked = true;
                        rbConsorcioTab1.Checked = false;
                        //rbConsorcioTab1.Enabled = false;

                        lblNomConsultorTab1.Font.Bold = true;
                        lblNomConsorcioTab1.Font.Bold = false;

                        Panel_ConsultorTab1.Visible = true;
                        Panel_ConsorcioTab1.Visible = false;
                        Up_rbProcesoTab1.Update();

                        txtNombConsultorTab1.Text = dt.Rows[0]["nombreContratista"].ToString();
                        txtRucConsultorTab1.Text = dt.Rows[0]["rucContratista"].ToString();
                        lnkbtnContratoContratistaTab1.Text = dt.Rows[0]["urlContrato"].ToString();
                        txtRepresentanteLegalConsultorTab1.Text = dt.Rows[0]["representanteContratista"].ToString();
                        GeneraIcoFile(lnkbtnContratoContratistaTab1.Text, imgbtnContratoContratistaTab1);

                    }
                    if (Convert.ToInt32(valCheck) == 2)
                    {
                        rbConsorcioTab1.Checked = true;
                        rbConsultorTab1.Checked = false;
                        //rbConsultorTab1.Enabled = false;

                        lblNomConsorcioTab1.Font.Bold = true;
                        lblNomConsultorTab1.Font.Bold = false;

                        Panel_ConsorcioTab1.Visible = true;
                        Panel_ConsultorTab1.Visible = false;
                        Up_rbProcesoTab1.Update();


                        lnkbtnCartaTab1.Text = dt.Rows[0]["urlCartaFielConsorcio"].ToString();
                        lnkbtnContratoTab1.Text = dt.Rows[0]["urlContrato"].ToString();


                        txtNomconsorcioTab1.Text = dt.Rows[0]["nombreConsorcio"].ToString();
                        txtRucConsorcioTab1.Text = dt.Rows[0]["rucConsorcio"].ToString();
                        txtTelefonoConsorcioTab1.Text = dt.Rows[0]["telefonoConsorcio"].ToString();
                        txtRepresentanteTab1.Text = dt.Rows[0]["representateConsorcio"].ToString();

                        GeneraIcoFile(lnkbtnCartaTab1.Text, imgbtnAdjCartaTab1);
                        GeneraIcoFile(lnkbtnContratoTab1.Text, imgbtnContratoTab1);

                    }
                }
                else
                {
                    rbConsorcioTab1.Checked = false;
                    rbConsultorTab1.Checked = false;
                    rbConsultorTab1.Enabled = true;
                    rbConsorcioTab1.Enabled = true;
                    Panel_ConsultorTab1.Visible = false;
                    Panel_ConsorcioTab1.Visible = false;
                    lblNomConsorcioTab1.Font.Bold = false;
                    lblNomConsultorTab1.Font.Bold = false;

                    txtNombConsultorTab1.Text = "";
                    txtRucConsultorTab1.Text = "";
                    txtNomconsorcioTab1.Text = "";
                    txtRucConsorcioTab1.Text = "";
                    txtTelefonoConsorcioTab1.Text = "";
                    txtRepresentanteTab1.Text = "";

                    cargaConsultoresTab1();
                    Up_rbProcesoTab1.Update();
                }


            }
            else
            {
                ddlAdjudicacionTab1.SelectedValue = "";
                txtNroLicTab1.Text = "";
                TxtAnio2Tab1.Text = "";
                txtDetalleTab1.Text = "";
                txtProTab1.Text = "";
                txtProConsentidaTab1.Text = "";
                txtValorReferencialTab1.Text = "";

                txtMontoContratadoTab1.Text = "";
                txtNroContrato.Text = "";
                txtNomconsorcioTab1.Text = "";
                txtRucConsorcioTab1.Text = "";
                txtTelefonoConsorcioTab1.Text = "";
                txtRepresentanteTab1.Text = "";
                lblNomActualizaProcesoSeleccion.Text = "";
                lblNomConsorcioTab1.Font.Bold = false;
                lblNomConsultorTab1.Font.Bold = false;
                rbConsorcioTab1.Checked = false;
                rbConsultorTab1.Checked = false;
                rbConsultorTab1.Enabled = true;
                rbConsorcioTab1.Enabled = true;
                Panel_ConsultorTab1.Visible = false;
                Panel_ConsorcioTab1.Visible = false;
                imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoContratistaTab1.Text = "";
                imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoTab1.Text = "";

                Up_Tab1.Update();
            }

            // INFORMACION AUTOMATICA SEACE
            CargaCodigoSeace();
            CargaProcesosRelSEACE();
        }

        protected void CargaddlTipoAdjudicacionTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoAdjudicacion();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void rbConsultorTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsultorTab1.Font.Bold = true;
            lblNomConsorcioTab1.Font.Bold = false;
            Up_lblNomConsultorTab1.Update();
            Up_lblNomConsorcioTab1.Update();

            rbConsorcioTab1.Checked = false;
            Up_rbConsorcioTab1.Update();
            Panel_ConsultorTab1.Visible = true;
            Panel_ConsorcioTab1.Visible = false;
            Up_rbProcesoTab1.Update();
        }

        protected void rbConsorcioTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsorcioTab1.Font.Bold = true;
            lblNomConsultorTab1.Font.Bold = false;
            Up_lblNomConsorcioTab1.Update();
            Up_lblNomConsultorTab1.Update();

            rbConsultorTab1.Checked = false;
            Up_rbConsultorTab1.Update();
            Panel_ConsorcioTab1.Visible = true;
            Panel_ConsultorTab1.Visible = false;
            Up_rbProcesoTab1.Update();

        }

        protected void cargaSeguimientoProcesoTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 2;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdSeguimientoProcesoTab1.DataSource = _objBLProceso.F_spMON_SeguimientoProcesoSeleccion(_BEProceso);
            grdSeguimientoProcesoTab1.DataBind();
        }

        protected void CargaDllTipoResultado()
        {
            ddlResultadoTab1.DataSource = _objBLEjecucion.F_spMON_TipoResultado();
            ddlResultadoTab1.DataTextField = "nombre";
            ddlResultadoTab1.DataValueField = "valor";
            ddlResultadoTab1.DataBind();

            ddlResultadoTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }


        protected void grdSeguimientoProcesoTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab1");

                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void imgDocTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdSeguimientoProcesoTab1.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdSeguimientoProcesoTab1.Rows[row.RowIndex].FindControl("imgDocTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtn_AgregarSeguimientoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;

            lblNomUsuarioSeguimientoProceso.Text = "";
            btnGuardarSeguimientoProcesoTab1.Visible = true;
            btnModificarSeguimientoProcesoTab1.Visible = false;

            Up_Tab1.Update();
        }

        protected void btnCancelaSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = true;
            Up_Tab1.Update();
        }


        protected Boolean ValidarSeguimientoTab1()
        {
            Boolean result;
            result = true;

            if (TxtConvocatoriaTab1.Text == "")
            {
                string script = "<script>alert('Ingrese convocatoria.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaPubliTab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de publicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaPubliTab1.Text) == false)
            {
                string script = "<script>alert('Ingrese fecha de publicación valida.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToDateTime(txtFechaPubliTab1.Text) > DateTime.Now.Date)
            {
                string script = "<script>alert('La Fecha de Publicación no puede ser mayor a la actual.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (txtProTab1.Text.Length > 0 && Convert.ToDateTime(txtFechaPubliTab1.Text) > Convert.ToDateTime(txtProTab1.Text))
            {
                string script = "<script>alert('La fecha de publicación debe ser menor o igual a la fecha de buena pro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoAdjudicacionTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de adjudicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlResultadoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione resultado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }

        protected void btnGuardarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSeguimientoTab1())
            {
                if (validaArchivo(FileUploadSeguimientoTAb1))
                {
                    if (validaArchivo(FileUploadContratoContratistaTab1))
                    {

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 2;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                        _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaPubliTab1.Text);
                        _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                        _BEProceso.resultado = ddlResultadoTab1.SelectedValue;
                        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                        _BEProceso.observacion = txtObservacion.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val = _objBLProceso.spi_MON_SeguimientoProcesoSeleccion(_BEProceso);

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            TxtConvocatoriaTab1.Text = "";
                            txtFechaPubliTab1.Text = "";
                            ddlTipoAdjudicacionTab1.SelectedValue = "";
                            ddlResultadoTab1.SelectedValue = "";
                            txtObservacion.Text = "";

                            Panel_AgregarSeguimientoTab1.Visible = false;
                            imgbtn_AgregarSeguimientoTab1.Visible = true;

                            cargaSeguimientoProcesoTab1();
                            Up_Tab1.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }
                }
            }

        }

        protected void cargaConsultoresTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 2;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdConsultoresTab1.DataSource = _objBLProceso.F_spMON_GrupoConsorcio(_BEProceso);
            grdConsultoresTab1.DataBind();


        }

        protected void imgbtn_AgregarConsultorTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = true;
            btn_modificarConsultor.Visible = false;
            Up_Tab1.Update();
        }

        protected void btn_cancelarConsultor_OnClick(object sender, EventArgs e)
        {

            txtContratistaGrupTab1.Text = "";
            txtRucGrupTab1.Text = "";
            txtRepresentanGrupTAb1.Text = "";
            txtRepresentanGrupTAb1.Text = "";
            Panel_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void lnkbtnCartaTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnCartaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnCartaTab1.Text);
                Response.End();
            }

        }

        protected void lnkbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnAdjCartaTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnCartaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnCartaTab1.Text);
                Response.End();
            }

        }

        protected void imgbtnContratoContratistaTab1_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoContratistaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoContratistaTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }


        protected Boolean ValidarGrupoConsorcioTab1()
        {
            Boolean result;
            result = true;

            if (txtContratistaGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese nombre de contratista.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtRucGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese N° de RUC.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtRepresentanGrupTAb1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Representante Legal.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            return result;
        }

        protected void btn_guardarConsultor_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (validaArchivo(FileUploadAdjCartaTab1))
                {
                    if (validaArchivo(FileUploadAdjContratoTab1))
                    {
                        if (ValidarGrupoConsorcioTab1())
                        {
                            //REGISTRAR PROCESO DE SELECCION INDIRECTA
                            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEProceso.tipoFinanciamiento = 2;
                            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                            _BEProceso.id_tipoModalidad = 2; //Expediente por default es Indirecto
                            if (ddlAdjudicacionTab1.SelectedValue == "")
                            {
                                _BEProceso.id_tipoAdjudicacion = 0;
                            }
                            else
                            { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }
                            if (txtNroLicTab1.Text == "")
                            { txtNroLicTab1.Text = "0"; }
                            _BEProceso.norLic = (txtNroLicTab1.Text);
                            _BEProceso.anio = TxtAnio2Tab1.Text;
                            _BEProceso.detalle = txtDetalleTab1.Text;
                            _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                            _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                            if (txtValorReferencialTab1.Text == "")
                            { txtValorReferencialTab1.Text = "0"; }
                            _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                            if (txtMontoContratadoTab1.Text == "")
                            { txtMontoContratadoTab1.Text = "0"; }
                            _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                            _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);

                            if (rbConsultorTab1.Checked == true)
                            {
                                _BEProceso.id_tipoEmpresa = 1;
                                _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                                _BEProceso.rucContratista = txtRucConsultorTab1.Text;


                                if (FileUploadContratoContratistaTab1.HasFile)
                                {
                                    _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                                }
                                else
                                { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }

                            }

                            if (rbConsorcioTab1.Checked == true)
                            {
                                _BEProceso.id_tipoEmpresa = 2;
                                _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                                _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                                _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                                _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                                if (FileUploadAdjCartaTab1.HasFile)
                                {
                                    _BEProceso.urlCartaConsorcio = _Metodo.uploadfile(FileUploadAdjCartaTab1);
                                }
                                else
                                { _BEProceso.urlCartaConsorcio = lnkbtnCartaTab1.Text; }

                                if (FileUploadAdjContratoTab1.HasFile)
                                {
                                    _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                                }
                                else
                                { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                            }

                            _BEProceso.Date_fechaConvenio = VerificaFecha("");
                            _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
                            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int valI = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);


                            //REGISTRAR CONSORCIO
                            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEProceso.tipoFinanciamiento = 2;
                            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                            _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                            _BEProceso.rucContratista = txtRucGrupTab1.Text;
                            _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLProceso.spi_MON_GrupoConsorcio(_BEProceso);

                            if (val == 1 && valI == 1)
                            {
                                string script = "<script>alert('Se registró correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                cargaConsultoresTab1();

                                txtContratistaGrupTab1.Text = "";
                                txtRucGrupTab1.Text = "";
                                txtRepresentanGrupTAb1.Text = "";

                                Panel_AgregarConsultorTab1.Visible = false;
                                imgbtn_AgregarConsultorTab1.Visible = true;

                                Up_Tab1.Update();

                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }

                        }
                    }
                }
            }
        }

        protected Boolean ValidarResultadoTab1()
        {
            Boolean result;
            result = true;

            //if (ddlAdjudicacionTab1.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione tipo de Adjudicación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtNroLicTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese N° de Licitación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (TxtAnio2Tab1.Text == "")
            {
                string script = "<script>alert('Ingrese año.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!txtProTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }
            }

            if (!txtProConsentidaTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProConsentidaTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro Consentida.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }
            }

            if (!txtFechaFirmaContratoTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtFechaFirmaContratoTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de firma de contrato.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de firma de contrato ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }

                if (!txtProConsentidaTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProConsentidaTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato no puede ser menor a la Fecha de Buena Pro Consentida.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }

                if (txtFechaInicioTab2.Text.Length > 0)
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > Convert.ToDateTime(txtFechaInicioTab2.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato debe ser menor a la fecha de inicio.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }
          

            if (txtMontoContratadoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese monto contratado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

        

            if (rbConsultorTab1.Checked == true)
            {
                if (txtNombConsultorTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese nombre del contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (txtRucConsultorTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese RUC de contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }

            if (rbConsorcioTab1.Checked == true)
            {
                if (txtNomconsorcioTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese nombre del consorcio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                //if (txtRucConsorcioTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese RUC de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}
                //if (txtRepresentanteTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese representante legal de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}

            }




            return result;
        }



        protected void btnGuardarTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (validaArchivo(FileUploadAdjCartaTab1))
                {
                    if (validaArchivo(FileUploadAdjContratoTab1))
                    {
                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 2;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                        _BEProceso.id_tipoModalidad = 2; //Expediente por default es Indirecto
                        if (ddlAdjudicacionTab1.SelectedValue == "")
                        {
                            _BEProceso.id_tipoAdjudicacion = 0;
                        }
                        else
                        { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }
                        if (txtNroLicTab1.Text == "")
                        { txtNroLicTab1.Text = "0"; }
                        _BEProceso.norLic = (txtNroLicTab1.Text);
                        _BEProceso.anio = TxtAnio2Tab1.Text;
                        _BEProceso.detalle = txtDetalleTab1.Text;
                        _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                        _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                        if (txtValorReferencialTab1.Text == "")
                        { txtValorReferencialTab1.Text = "0"; }
                        _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                        if (txtMontoContratadoTab1.Text == "")
                        { txtMontoContratadoTab1.Text = "0"; }
                        _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                        _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);

                        if (rbConsultorTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 1;
                            _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                            _BEProceso.rucContratista = txtRucConsultorTab1.Text;


                            if (FileUploadContratoContratistaTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }

                        }

                        if (rbConsorcioTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 2;
                            _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                            _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                            _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                            _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                            if (FileUploadAdjCartaTab1.HasFile)
                            {
                                _BEProceso.urlCartaConsorcio = _Metodo.uploadfile(FileUploadAdjCartaTab1);
                            }
                            else
                            { _BEProceso.urlCartaConsorcio = lnkbtnCartaTab1.Text; }

                            if (FileUploadAdjContratoTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                        }

                        _BEProceso.Date_fechaConvenio = VerificaFecha("");
                        _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);


                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            CargaDatosTab1();

                            Up_Tab1.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }

                    }
                }
            }


        }

        #endregion

        #region Tab2
        protected void CargaTab2()
        {

            CargaDatosTab2();

            dt = CargaEmpleadoObraTab2(4);
            grd_HistorialCoordinador.DataSource = dt;
            grd_HistorialCoordinador.DataBind();

            //if (dt.Rows.Count > 0)
            //{ txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString(); }
            //txtCordinadorTab2.Text = 
            CargaAvanceFisico();

            if (lblCOD_SUBSECTOR.Text == "1")
            {
                trGEI.Visible = true;
            }

            if (lblID_ACCESO.Text.Equals("1"))
            {
                CargaTipoMonitoreo();
            }

        }
        #region Coordinador
        protected DataTable CargaEmpleadoObraTab2(int tipo)
        {
            DataTable dt;
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoObraExp(_BEEjecucion);
            return dt;

            //  grd.DataBind();
        }
        protected void imgbtn_CoordinadorTab2_OnClick(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = true;
            btnGuardar_Coordinador.Visible = true;
            btnModificar_Coordinador.Visible = false;
            Up_Tab2.Update();
        }
        protected void btnCancelarCoordinador_Onclick(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = false;

            Up_Tab2.Update();
        }
        protected void btnGuardarCoordinador_OnClick(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de coordinador MVCS.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 4;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spi_MON_EmpleadoObraExp(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró coordinador MVCS correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //txtCordinadorTab2.Text = txtCoordinador.Text;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoObraTab2(4);
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    Panel_Coordinador.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grd_HistorialCoordinador_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialCoordinador.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;//PARA LA ELIMINACION
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;

                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoObraTab2(4);
                    if (dt.Rows.Count > 0)
                    {
                        txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCordinadorTab2.Text = "";
                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();

                    Panel_Coordinador.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grd_HistorialCoordinador_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialCoordinador.SelectedDataKey.Value.ToString();
            lblIdCoordinadorM.Text = id;
            GridViewRow row = grd_HistorialCoordinador.SelectedRow;

            txtCoordinador.Text = ((Label)row.FindControl("lblnombreS")).Text;
            txtFechaC.Text = ((Label)row.FindControl("lblFechaSS")).Text;

            btnGuardar_Coordinador.Visible = false;
            btnModificar_Coordinador.Visible = true;
            Panel_Coordinador.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }
        protected void btnModificarCoordinador_OnClick(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIdCoordinadorM.Text);
                _BEEjecucion.Tipo = 1;//valor q indica la modificacion
                                      // _BEEjecucion.empleadoObra = txtu.Text;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el coordinador correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(4);
                    if (dt.Rows.Count > 0)
                    {
                        txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCordinadorTab2.Text = "";

                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    Panel_Coordinador.Visible = false;
                    btnModificar_Coordinador.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        #endregion
        //protected void CargaTipoAdjudicacionTab2()
        //{
        //    ddlEstadoTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(2);
        //    ddlEstadoTab2.DataTextField = "nombre";
        //    ddlEstadoTab2.DataValueField = "valor";
        //    ddlEstadoTab2.DataBind();

        //    ddlEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}

        //protected void CargaSubTipoEstadoEjecucionTab2(string tipo)
        //{
        //    ddlSubEstadoTab2.Items.Clear();

        //    if (tipo == "")
        //    {
        //        ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        //    }
        //    else
        //    {
        //        List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

        //        list = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(tipo));

        //        ddlSubEstadoTab2.DataSource = list;
        //        ddlSubEstadoTab2.DataTextField = "nombre";
        //        ddlSubEstadoTab2.DataValueField = "valor";
        //        ddlSubEstadoTab2.DataBind();

        //        ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //        if (list.Count == 0)
        //        {
        //            lblnNomSubEstadoTab2.Visible = false;
        //            ddlSubEstadoTab2.Visible = false;
        //        }
        //        else
        //        {
        //            lblnNomSubEstadoTab2.Visible = true;
        //            ddlSubEstadoTab2.Visible = true;
        //        }
        //    }
        //}

        //protected void ddlEstadoTab2_OnSelecdIndexChanged(object sender, EventArgs e)
        //{
        //    //if (ddlEstadoTab2.SelectedValue == "21" )
        //    //{
        //    //    lblnNomSubEstadoTab2.Visible = true;
        //    //    ddlSubEstadoTab2.Visible = true;
        //    CargaSubTipoEstadoEjecucionTab2(ddlEstadoTab2.SelectedValue);
        //    //}
        //    //else
        //    //{
        //    //    lblnNomSubEstadoTab2.Visible = false;
        //    //    ddlSubEstadoTab2.Visible = false;
        //    //}
        //    //Up_lblNombSubEstadoTab2.Update();
        //    Up_SubEstadoTab2.Update();
        //}

        protected void CargaDatosTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 2;

            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                lblNombreEstadoGeneral.Text = dt.Rows[0]["vEstadoEjecucion"].ToString().ToUpper();

                lblIdEstado.Text = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();
                lblIdSubEstado.Text = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
                lblIdSubEstado2.Text = dt.Rows[0]["id_TipoEstadoParalizado"].ToString();
                
                txtCordinadorTab2.Text = dt.Rows[0]["coordinadorMVCS"].ToString();

                //ddlEstadoTab2.SelectedValue = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();

                //CargaSubTipoEstadoEjecucionTab2(ddlEstadoTab2.SelectedValue);
                //ddlSubEstadoTab2.SelectedValue = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();

                txtFechaInicioTab2.Text = dt.Rows[0]["fechaInicio"].ToString();
                txtPlazoEjecucionTab2.Text = dt.Rows[0]["plazoEjecucion"].ToString();
                txtFechaInicioContractualTab2.Text = dt.Rows[0]["fechaInicioContractual"].ToString();

                txtFechaInicioReal.Text = dt.Rows[0]["fechaInicioReal"].ToString();
                txtFechaFinContractual.Text = dt.Rows[0]["fechaFinContractual"].ToString();
                txtFechaTerminoReal.Text = dt.Rows[0]["fechaFinReal"].ToString();
                txtFechaTermino.Text = dt.Rows[0]["fechaFin"].ToString();

                txtCoordinadorUETab2.Text = dt.Rows[0]["coordinadorUE"].ToString();
                txtTelefonoUETab2.Text = dt.Rows[0]["telefonoUE"].ToString();
                txtCorreoUETab2.Text = dt.Rows[0]["correoUE"].ToString();
                txtTelefonoCoordinadorUETab2.Text = dt.Rows[0]["telefonocoordinador"].ToString();
                txtCorreoCoordinadorUETab2.Text = dt.Rows[0]["correocoordinador"].ToString();

                //lnkbtnMetasTab2.Text = dt.Rows[0]["urlMetasFisicas"].ToString();
                //lnkbtnMemoriaTab2.Text = dt.Rows[0]["urlMemorariaDescriptivas"].ToString();
                lblNombActuaTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                if (dt.Rows[0]["usuarioUpdateEstado"].ToString().Length > 0)
                {
                    lblFechaEstado.Text = "(Actualizó: " + dt.Rows[0]["usuarioUpdateEstado"].ToString() + " - " + dt.Rows[0]["fechaUpdateEstado"].ToString() + ")";
                }
                TxtNotasAdicionales.Text = dt.Rows[0]["notasAdicionales"].ToString();

                if (TxtNotasAdicionales.Text.Length > 0)
                {
                    PanelNotasAdicionales.Visible = true;

                }

                //GeneraIcoFile(lnkbtnMetasTab2.Text, imgbtnMetasTab2);
                //GeneraIcoFile(lnkbtnMemoriaTab2.Text, imgbtnMemoriaTab2);
                                
            }

        }

        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected Boolean ValidarEjecucionTab2()
        {
            Boolean result;
            result = true;

            if (txtFechaInicioTab2.Text != "")
            {
                if (_Metodo.ValidaFecha(txtFechaInicioTab2.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Inicio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (Convert.ToDateTime(txtFechaInicioTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La Fecha de Inicio no puede ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }
            }

            if (txtFechaTermino.Text != "")
            {
                if (_Metodo.ValidaFecha(txtFechaTermino.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Termino.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaTermino.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La Fecha de Termino Real no debe ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (Convert.ToDateTime(txtFechaTermino.Text) < Convert.ToDateTime(txtFechaInicioTab2.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser mayor  a la fecha de inicio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                }

                if (txtFechaTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaTerminoReal.Text) > Convert.ToDateTime(txtFechaTab3.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser menor o igual a la fecha de liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtPlazoEjecucionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese Plazo de ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }

        protected void btnGuardarTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarEjecucionTab2())
            {
                if (validaArchivo(FileUploadMetasTab1))
                {
                    if (validaArchivo(FileUploadMemoriasTab1))
                    {


                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 2;

                        _BEEjecucion.coordinadorMVCS = txtCordinadorTab2.Text;
                        //_BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                        //_BEEjecucion.id_tipoSubEstadoEjecucion = (ddlSubEstadoTab2.SelectedValue);


                        _BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaInicioTab2.Text);
                        if (txtPlazoEjecucionTab2.Text == "")
                        {
                            txtPlazoEjecucionTab2.Text = "0";
                        }
                        _BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                        _BEEjecucion.Date_fechaInicioContractual = VerificaFecha(txtFechaInicioContractualTab2.Text);
                        _BEEjecucion.Date_fechaInicioReal = VerificaFecha(txtFechaInicioReal.Text);
                        _BEEjecucion.Date_fechaFinContractual = VerificaFecha(txtFechaFinContractual.Text);
                        _BEEjecucion.Date_fechaFinReal = VerificaFecha(txtFechaTerminoReal.Text);
                        _BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaTermino.Text);
                        _BEEjecucion.Date_fechaEntregaTerreno = VerificaFecha("");
                        _BEEjecucion.Date_fechaAdelantoMaterial = VerificaFecha("");
                        _BEEjecucion.Date_fechaAdelantoDirecto = VerificaFecha("");
                        _BEEjecucion.Date_fechaRecepcion = VerificaFecha("");
                        _BEEjecucion.Date_fechaFinProbable = VerificaFecha("");

                        _BEEjecucion.coordinadorUE = txtCoordinadorUETab2.Text;
                        _BEEjecucion.telefonoUE = txtTelefonoUETab2.Text;
                        _BEEjecucion.correoUE = txtCorreoUETab2.Text;
                        _BEEjecucion.telefonocoordinador = txtTelefonoCoordinadorUETab2.Text;
                        _BEEjecucion.correocoordinador = txtCorreoCoordinadorUETab2.Text; ;

                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        _BEEjecucion.urlMetasFisicas = FileUploadMetasTab1.HasFile ? _Metodo.uploadfile(FileUploadMetasTab1) : lnkbtnMetasTab2.Text;

                        _BEEjecucion.urlMemoriasDescriptivas = FileUploadMemoriasTab1.HasFile ? _Metodo.uploadfile(FileUploadMemoriasTab1) : lnkbtnMemoriaTab2.Text;
                        _BEEjecucion.urlFichaRecepcion = "";


                        int val = _objBLEjecucion.spi_MON_EstadoEjecuccionTransferencia(_BEEjecucion);

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            CargaDatosTab2();

                            Up_Tab2.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }

                    }
                }
            }

        }
        protected void CargaTipoMonitoreo()
        {
            ddlTipoMonitoreo.DataSource = _objBLEjecucion.F_spMON_ListarTipoMonitoreo();
            ddlTipoMonitoreo.DataTextField = "nombre";
            ddlTipoMonitoreo.DataValueField = "valor";
            ddlTipoMonitoreo.DataBind();

            ddlTipoMonitoreo.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaAvanceFisico()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 2;
            DataTable dt = _objBLEjecucion.spMON_AvanceFisico(_BEEjecucion);
            if (dt.Rows.Count > 0)
            {
                lblAvanceFisicoTab2.Text = (dt.Rows[dt.Rows.Count - 1]["avance"]).ToString();
            }
            grdAvanceFisicoTab2.DataSource = dt;
            grdAvanceFisicoTab2.DataBind();
        }
        
        protected void imgbtnAgregarAvanceFisicoTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;

            lblNomUsuarioAvance.Text = "";
            btnModificarAvanceTab2.Visible = false;
            btnGuardarAvanceTab2.Visible = true;

            imgbtnActaTab2.ImageUrl = "~/img/blanco.png";
            imgbtnInformeTab2.ImageUrl = "~/img/blanco.png";
            imgbtnOficioTab2.ImageUrl = "~/img/blanco.png";

            lnkbtnInformeTab2.Text = "";
            lnkbtnOficioTab2.Text = "";
            lnkbtnActaTab2.Text = "";

            ddlTipoMonitoreo.SelectedValue = "";

            chbAyudaMemoriaTab2.Checked = true;
            chbAyudaMemoriaTab2.Enabled = true;
            lblNomUsuarioFlagAyudaTab2.Text = "";

            CargaTipoEstadosDetalle();
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);

            Up_Tab2.Update();
        }


        protected void CargaTipoEstadosDetalle()
        {
            ddlTipoEstadoInformeTab2.ClearSelection();

            ddlTipoEstadoInformeTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(2,0); //EXP. TECNICO
            ddlTipoEstadoInformeTab2.DataTextField = "nombre";
            ddlTipoEstadoInformeTab2.DataValueField = "valor";
            ddlTipoEstadoInformeTab2.DataBind();

            ddlTipoEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void CargaTipoSubEstadosDetalle(string TipoEstadoEjecucion)
        {
            ddlTipoSubEstadoInformeTab2.ClearSelection();

            if (!TipoEstadoEjecucion.Equals(""))
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                list = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(TipoEstadoEjecucion));

                ddlTipoSubEstadoInformeTab2.DataSource = list;
                ddlTipoSubEstadoInformeTab2.DataTextField = "nombre";
                ddlTipoSubEstadoInformeTab2.DataValueField = "valor";
                ddlTipoSubEstadoInformeTab2.DataBind();

                //ddlTipoSubEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

                if (list.Count == 0)
                {
                    lblNombreSubEstado.Visible = false;
                    ddlTipoSubEstadoInformeTab2.Visible = false;
                }
                else
                {
                    lblNombreSubEstado.Visible = true;
                    ddlTipoSubEstadoInformeTab2.Visible = true;
                }
            }

            ddlTipoSubEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void ddlTipoEstadoInformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);

            //Up_EstadoDetalleTab2.Update();

            Up_SubEstadoNombreTab2.Update();
            Up_SubEstadoDetalleTab2.Update();
        }

        protected void btnCancelarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = false;
            imgbtnAvanceFisicoTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void btnGuardarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUpload_ActaTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 2;

                    _BEEjecucion.informe = txtInformeTab2.Text;
                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtFechaTab2.Text);
                    _BEEjecucion.avance = txtAvanceTab2.Text;
                    _BEEjecucion.financieroReal = txtAvanceFinancieroTab2.Text.Equals("") ? "0" : txtAvanceFinancieroTab2.Text;
                    _BEEjecucion.situacion = txtSituacionTab2.Text;
                    //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);

                    _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                    _BEEjecucion.id_tipoSubMonitoreo = ddlTipoSubMonitoreo.SelectedValue.Equals("") ? "0" : ddlTipoSubMonitoreo.SelectedValue;
                    _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue);
                    _BEEjecucion.id_tipoSubEstadoEjecucion = ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") ? "0" : ddlTipoSubEstadoInformeTab2.SelectedValue;
                    _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab2);
                    _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab2);
                    _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab2);

                    if (chbAyudaMemoriaTab2.Checked == true)
                    {
                        _BEEjecucion.flagAyuda = "1";
                    }
                    else
                    {
                        _BEEjecucion.flagAyuda = "0";
                    }

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.spi_MON_AvanceFisico(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtInformeTab2.Text = "";
                        txtFechaTab2.Text = "";
                        txtAvanceTab2.Text = "";
                        txtSituacionTab2.Text = "";

                        Panel_AgregarAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }

        }
        protected void lnkbtnMetasTab2_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnMetasTab2.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnMetasTab2.Text);
                Response.End();
            }

        }

        protected void imgbtnMetasTab2_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnMetasTab2.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnMetasTab2.Text);
                Response.End();
            }

        }

        protected void lnkbtnMemoriaTab2_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnMemoriaTab2.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnMemoriaTab2.Text);
                Response.End();
            }

        }

        protected void imgbtnMemoriaTab2_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnMemoriaTab2.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnMemoriaTab2.Text);
                Response.End();
            }

        }

        //protected void btnComponentesPNSU_OnClick(object sender, EventArgs e)
        //{
        //    VarPnsu();
        //    cargaComponentesPNSU();
        //    Up_Componente.Update();
        //    MPE_PNSU.Show();

        //}

        //private void VarPnsu()
        //{

        //    DataTable _obj;
        //    _BEEjecucion.snip = lblSNIP.Text;
        //    _BEEjecucion.tipoFinanciamiento = 2;

        //    _obj = _objBLEjecucion.spMON_ObtenerVariablesPNSU(_BEEjecucion);

        //    if (_obj.Rows.Count > 0)
        //    {
        //        txtA_captacion_nuc.Text = _obj.Rows[0]["a_captacion_nuc"].ToString();//
        //        txtA_captacion_cacl.Text = _obj.Rows[0]["a_captacion_cacl"].ToString();//

        //        txtA_trataPotable_num.Text = _obj.Rows[0]["a_trataPotable_num"].ToString();//
        //        txtA_trataPotable_cap.Text = _obj.Rows[0]["a_trataPotable_cap"].ToString();//

        //        txtA_conduccion_nuc.Text = _obj.Rows[0]["a_conduccion_nuc"].ToString();
        //        txtA_conduccion_cacd.Text = _obj.Rows[0]["a_conduccion_cacd"].ToString();

        //        txtA_impulsion_num.Text = _obj.Rows[0]["a_impulsion_num"].ToString();
        //        txtA_impulsion_cap.Text = _obj.Rows[0]["a_impulsion_cap"].ToString();

        //        txtA_estacion_num.Text = _obj.Rows[0]["a_estacion_num"].ToString();

        //        txtA_reservorio_nur.Text = _obj.Rows[0]["a_reservorio_nur"].ToString();
        //        txtA_reservorio_carm.Text = _obj.Rows[0]["a_reservorio_carm"].ToString();

        //        txtA_aduccion_nua.Text = _obj.Rows[0]["a_aduccion_nua"].ToString();
        //        txtA_aduccion_caad.Text = _obj.Rows[0]["a_aduccion_caad"].ToString();

        //        txtA_redes_nur.Text = _obj.Rows[0]["a_redes_nur"].ToString();
        //        txtA_redes_card.Text = _obj.Rows[0]["a_redes_card"].ToString();

        //        txtA_conexion_nucn.Text = _obj.Rows[0]["a_conexion_nucn"].ToString();
        //        txtA_conexion_nucr.Text = _obj.Rows[0]["a_conexion_nucr"].ToString();
        //        txtA_conexion_nucp.Text = _obj.Rows[0]["a_conexion_nucp"].ToString();

        //        txtS_colector_num.Text = _obj.Rows[0]["s_colectores_num"].ToString();
        //        txtS_colector_cap.Text = _obj.Rows[0]["s_colectores_cap"].ToString();


        //        txtS_camara_num.Text = _obj.Rows[0]["s_camara_num"].ToString();

        //        txtS_planta_nup.Text = _obj.Rows[0]["s_planta_nup"].ToString();
        //        txtS_planta_capl.Text = _obj.Rows[0]["s_planta_capl"].ToString();

        //        txtS_agua_nua.Text = _obj.Rows[0]["s_agua_nua"].ToString();
        //        txtS_agua_caad.Text = _obj.Rows[0]["s_agua_caad"].ToString();

        //        txtS_conexion_nucn.Text = _obj.Rows[0]["s_conexion_nucn"].ToString();
        //        txtS_conexion_nucr.Text = _obj.Rows[0]["s_conexion_nucr"].ToString();
        //        txtS_conexion_nuclo.Text = _obj.Rows[0]["s_conexion_nuclo"].ToString();

        //        txtS_efluente_num.Text = _obj.Rows[0]["s_efluente_num"].ToString();
        //        txtS_efluente_cap.Text = _obj.Rows[0]["s_efluente_cap"].ToString();

        //        txtS_impulsion_num.Text = _obj.Rows[0]["s_impulsion_num"].ToString();
        //        txtS_impulsion_cap.Text = _obj.Rows[0]["s_impulsion_cap"].ToString();

        //    }

        //}

        //protected void LlenarPNSU()
        //{
        //    txtA_captacion.Text = "0";
        //    txtA_conduccion.Text = "0";
        //    txtA_estacion.Text = "0";
        //    txtA_impulsion.Text = "0";
        //    txtA_trata.Text = "0";
        //    txtA_reservorio.Text = "0";
        //    txtA_aduccion.Text = "0";
        //    txtA_redes.Text = "0";
        //    txtA_conexion_nueva.Text = "0";
        //    txtA_conexion_piletas.Text = "0";
        //    txtA_conexion_rehab.Text = "0";

        //    txtS_efluente.Text = "0";
        //    txtS_camara.Text = "0";
        //    txtS_emisor.Text = "0";
        //    txtS_Impulsion.Text = "0";
        //    txtS_planta.Text = "0";
        //    txtS_redes.Text = "0";
        //    txtS_conexion_nueva.Text = "0";
        //    txtS_conexion_rehab.Text = "0";
        //    txtS_conexion_unid.Text = "0";
        //    txtS_Impulsion.Text = "0";
        //    txtS_redes.Text = "0";

        //    txtD_tuberia.Text = "0";
        //    txtD_cuneta.Text = "0";
        //    txtD_tormenta.Text = "0";
        //    txtD_conexion.Text = "0";
        //    txtD_inspeccion.Text = "0";
        //    txtD_secundario.Text = "0";
        //    txtD_principal.Text = "0";

        //    txtA_captacion_p.Text = "0";
        //    txtA_conduccion_p.Text = "0";
        //    txtA_estacion_p.Text = "0";
        //    txtA_impulsion_p.Text = "0";
        //    txtA_trata_p.Text = "0";
        //    txtA_reservorio_p.Text = "0";
        //    txtA_aduccion_p.Text = "0";
        //    txtA_redes_p.Text = "0";
        //    txtA_conexion_nueva_p.Text = "0";
        //    txtA_conexion_piletas_p.Text = "0";
        //    txtA_conexion_rehab_p.Text = "0";

        //    txtS_efluente_p.Text = "0";
        //    txtS_camara_p.Text = "0";
        //    txtS_emisor_p.Text = "0";
        //    txtS_Impulsion_p.Text = "0";
        //    txtS_planta_p.Text = "0";
        //    txtS_redes_p.Text = "0";
        //    txtS_conexion_nueva_p.Text = "0";
        //    txtS_conexion_rehab_p.Text = "0";
        //    txtS_conexion_unid_p.Text = "0";
        //    txtS_Impulsion_p.Text = "0";
        //    txtS_redes_p.Text = "0";

        //    txtD_tuberia_p.Text = "0";
        //    txtD_cuneta_p.Text = "0";
        //    txtD_tormenta_p.Text = "0";
        //    txtD_conexion_p.Text = "0";
        //    txtD_inspeccion_p.Text = "0";
        //    txtD_secundario_p.Text = "0";
        //    txtD_principal_p.Text = "0";
        //}
        //protected void cargaComponentesPNSU()
        //{
        //    _BEVarPNSU.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEVarPNSU.TipoFinanciamiento = 2;

        //    dt = _objBLEjecucion.spMON_ComponentesPNSU(_BEVarPNSU);


        //    if (dt.Rows.Count > 0)
        //    {
        //        txtA_captacion.Text = dt.Rows[0]["a_captacion"].ToString();//
        //        txtA_conduccion.Text = dt.Rows[0]["a_conduccion"].ToString();//
        //        txtA_trata.Text = dt.Rows[0]["a_trata"].ToString();//
        //        txtA_reservorio.Text = dt.Rows[0]["a_reservorio"].ToString();//
        //        txtA_aduccion.Text = dt.Rows[0]["a_Aduccion"].ToString();
        //        txtA_redes.Text = dt.Rows[0]["a_redes"].ToString();
        //        txtA_conexion_nueva.Text = dt.Rows[0]["a_conexion_nueva"].ToString();
        //        txtA_conexion_piletas.Text = dt.Rows[0]["a_conexion_piletas"].ToString();
        //        txtA_conexion_rehab.Text = dt.Rows[0]["a_conexion_rehab"].ToString();
        //        txtA_impulsion.Text = dt.Rows[0]["a_impulsion"].ToString();
        //        txtA_estacion.Text = dt.Rows[0]["a_estacion"].ToString();

        //        txtS_efluente.Text = dt.Rows[0]["s_efluente"].ToString();
        //        txtS_camara.Text = dt.Rows[0]["s_camara"].ToString();
        //        txtS_emisor.Text = dt.Rows[0]["s_emisor"].ToString();
        //        txtS_planta.Text = dt.Rows[0]["s_planta"].ToString();
        //        txtS_conexion_nueva.Text = dt.Rows[0]["s_conexion_nueva"].ToString();
        //        txtS_conexion_rehab.Text = dt.Rows[0]["s_conexion_rehab"].ToString();
        //        txtS_conexion_unid.Text = dt.Rows[0]["s_conexion_unid"].ToString();
        //        txtS_Impulsion.Text = dt.Rows[0]["s_impulsion"].ToString();
        //        txtS_redes.Text = dt.Rows[0]["s_redes"].ToString();

        //        txtD_tuberia.Text = dt.Rows[0]["d_tuberia"].ToString();
        //        txtD_cuneta.Text = dt.Rows[0]["d_cuneta"].ToString();
        //        txtD_tormenta.Text = dt.Rows[0]["d_tormenta"].ToString();
        //        txtD_conexion.Text = dt.Rows[0]["d_conexion"].ToString();
        //        txtD_inspeccion.Text = dt.Rows[0]["d_inspeccion"].ToString();
        //        txtD_secundario.Text = dt.Rows[0]["d_secundarios"].ToString();
        //        txtD_principal.Text = dt.Rows[0]["d_principal"].ToString();

        //        txtA_captacion_p.Text = dt.Rows[0]["a_captacion_p"].ToString();//
        //        txtA_conduccion_p.Text = dt.Rows[0]["a_conduccion_p"].ToString();//
        //        txtA_trata_p.Text = dt.Rows[0]["a_planta_p"].ToString();//
        //        txtA_reservorio_p.Text = dt.Rows[0]["a_reservorio_p"].ToString();//
        //        txtA_aduccion_p.Text = dt.Rows[0]["a_aduccion_p"].ToString();
        //        txtA_redes_p.Text = dt.Rows[0]["a_redes_p"].ToString();
        //        txtA_conexion_nueva_p.Text = dt.Rows[0]["a_conexion_nueva_p"].ToString();
        //        txtA_conexion_piletas_p.Text = dt.Rows[0]["a_conexion_piletas_p"].ToString();
        //        txtA_conexion_rehab_p.Text = dt.Rows[0]["a_conexion_rehab_p"].ToString();
        //        txtA_impulsion_p.Text = dt.Rows[0]["a_impulsion_p"].ToString();
        //        txtA_estacion_p.Text = dt.Rows[0]["a_estacion_p"].ToString();

        //        txtS_efluente_p.Text = dt.Rows[0]["s_linea_p"].ToString();
        //        txtS_camara_p.Text = dt.Rows[0]["s_camara_p"].ToString();
        //        txtS_emisor_p.Text = dt.Rows[0]["s_emisor_p"].ToString();
        //        txtS_planta_p.Text = dt.Rows[0]["s_planta_p"].ToString();
        //        txtS_conexion_nueva_p.Text = dt.Rows[0]["s_conexion_nueva_p"].ToString();
        //        txtS_conexion_rehab_p.Text = dt.Rows[0]["s_conexion_rehab_p"].ToString();
        //        txtS_conexion_unid_p.Text = dt.Rows[0]["s_conexion_unid_p"].ToString();
        //        txtS_Impulsion_p.Text = dt.Rows[0]["s_impulsion_p"].ToString();
        //        txtS_redes_p.Text = dt.Rows[0]["s_redes_p"].ToString();

        //        txtD_tuberia_p.Text = dt.Rows[0]["d_tuberia_p"].ToString();
        //        txtD_cuneta_p.Text = dt.Rows[0]["d_cuneta_p"].ToString();
        //        txtD_tormenta_p.Text = dt.Rows[0]["d_boca_p"].ToString();
        //        txtD_conexion_p.Text = dt.Rows[0]["d_conexion_p"].ToString();
        //        txtD_inspeccion_p.Text = dt.Rows[0]["d_inpeccion_p"].ToString();
        //        txtD_secundario_p.Text = dt.Rows[0]["d_secundarios_p"].ToString();
        //        txtD_principal_p.Text = dt.Rows[0]["d_principal_p"].ToString();
        //    }
        //    else
        //    { LlenarPNSU(); }


        //}

        //protected void btnGuardarComponentesPNSU_OnClick(object sender, EventArgs e)
        //{
        //    _BEVarPNSU.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEVarPNSU.TipoFinanciamiento = 2;
        //    _BEVarPNSU.A_captacion = Convert.ToInt32(txtA_captacion.Text);
        //    _BEVarPNSU.A_conduccion = Convert.ToInt32(txtA_conduccion.Text);
        //    _BEVarPNSU.A_trata = Convert.ToInt32(txtA_trata.Text);
        //    _BEVarPNSU.A_reservorio = Convert.ToInt32(txtA_reservorio.Text);
        //    _BEVarPNSU.A_Aduccion = Convert.ToInt32(txtA_aduccion.Text);
        //    _BEVarPNSU.A_redes = Convert.ToInt32(txtA_redes.Text);
        //    _BEVarPNSU.A_conexion_nueva = Convert.ToInt32(txtA_conexion_nueva.Text);
        //    _BEVarPNSU.A_conexion_piletas = Convert.ToInt32(txtA_conexion_piletas.Text);
        //    _BEVarPNSU.A_conexion_rehab = Convert.ToInt32(txtA_conexion_rehab.Text);
        //    _BEVarPNSU.A_impulsion = Convert.ToInt32(txtA_impulsion.Text);
        //    _BEVarPNSU.A_estacion = Convert.ToInt32(txtA_estacion.Text);

        //    _BEVarPNSU.S_efluente = Convert.ToInt32(txtS_efluente.Text);
        //    _BEVarPNSU.S_camara = Convert.ToInt32(txtS_camara.Text);
        //    _BEVarPNSU.S_emisor = Convert.ToInt32(txtS_emisor.Text);
        //    _BEVarPNSU.S_planta = Convert.ToInt32(txtS_planta.Text);
        //    _BEVarPNSU.S_conexion_nueva = Convert.ToInt32(txtS_conexion_nueva.Text);
        //    _BEVarPNSU.S_conexion_rehab = Convert.ToInt32(txtS_conexion_rehab.Text);
        //    _BEVarPNSU.S_conexion_unid = Convert.ToInt32(txtS_conexion_unid.Text);
        //    _BEVarPNSU.S_redes = Convert.ToInt32(txtS_redes.Text);
        //    _BEVarPNSU.S_impulsion = Convert.ToInt32(txtS_Impulsion.Text);

        //    _BEVarPNSU.D_tuberia = Convert.ToInt32(txtD_tuberia.Text);
        //    _BEVarPNSU.D_cuneta = Convert.ToInt32(txtD_cuneta.Text);
        //    _BEVarPNSU.D_tormenta = Convert.ToInt32(txtD_tormenta.Text);
        //    _BEVarPNSU.D_conexion = Convert.ToInt32(txtD_conexion.Text);
        //    _BEVarPNSU.D_inspeccion = Convert.ToInt32(txtD_inspeccion.Text);
        //    _BEVarPNSU.D_secundario = Convert.ToInt32(txtD_secundario.Text);
        //    _BEVarPNSU.D_principal = Convert.ToInt32(txtD_principal.Text);

        //    _BEVarPNSU.A_captacion_p = Convert.ToInt32(txtA_captacion_p.Text);
        //    _BEVarPNSU.A_conduccion_p = Convert.ToInt32(txtA_conduccion_p.Text);
        //    _BEVarPNSU.A_trata_p = Convert.ToInt32(txtA_trata_p.Text);
        //    _BEVarPNSU.A_reservorio_p = Convert.ToInt32(txtA_reservorio_p.Text);
        //    _BEVarPNSU.A_Aduccion_p = Convert.ToInt32(txtA_aduccion_p.Text);
        //    _BEVarPNSU.A_redes_p = Convert.ToInt32(txtA_redes_p.Text);
        //    _BEVarPNSU.A_conexion_nueva_p = Convert.ToInt32(txtA_conexion_nueva_p.Text);
        //    _BEVarPNSU.A_conexion_piletas_p = Convert.ToInt32(txtA_conexion_piletas_p.Text);
        //    _BEVarPNSU.A_conexion_rehab_p = Convert.ToInt32(txtA_conexion_rehab_p.Text);
        //    _BEVarPNSU.A_impulsion_p = Convert.ToInt32(txtA_impulsion_p.Text);
        //    _BEVarPNSU.A_estacion_p = Convert.ToInt32(txtA_estacion_p.Text);

        //    _BEVarPNSU.S_efluente_p = Convert.ToInt32(txtS_efluente_p.Text);
        //    _BEVarPNSU.S_camara_p = Convert.ToInt32(txtS_camara_p.Text);
        //    _BEVarPNSU.S_emisor_p = Convert.ToInt32(txtS_emisor_p.Text);
        //    _BEVarPNSU.S_planta_p = Convert.ToInt32(txtS_planta_p.Text);
        //    _BEVarPNSU.S_conexion_nueva_p = Convert.ToInt32(txtS_conexion_nueva_p.Text);
        //    _BEVarPNSU.S_conexion_rehab_p = Convert.ToInt32(txtS_conexion_rehab_p.Text);
        //    _BEVarPNSU.S_conexion_unid_p = Convert.ToInt32(txtS_conexion_unid_p.Text);
        //    _BEVarPNSU.S_redes_p = Convert.ToInt32(txtS_redes_p.Text);
        //    _BEVarPNSU.S_impulsion_p = Convert.ToInt32(txtS_Impulsion_p.Text);

        //    _BEVarPNSU.D_tuberia_p = Convert.ToInt32(txtD_tuberia_p.Text);
        //    _BEVarPNSU.D_cuneta_p = Convert.ToInt32(txtD_cuneta_p.Text);
        //    _BEVarPNSU.D_tormenta_p = Convert.ToInt32(txtD_tormenta_p.Text);
        //    _BEVarPNSU.D_conexion_p = Convert.ToInt32(txtD_conexion_p.Text);
        //    _BEVarPNSU.D_inspeccion_p = Convert.ToInt32(txtD_inspeccion_p.Text);
        //    _BEVarPNSU.D_secundario_p = Convert.ToInt32(txtD_secundario_p.Text);
        //    _BEVarPNSU.D_principal_p = Convert.ToInt32(txtD_principal_p.Text);

        //    _BEVarPNSU.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //    int val = _objBLEjecucion.spi_MON_ComponentesPNSU(_BEVarPNSU);

        //    if (val == 1)
        //    {
        //        string script = "<script>alert('Se registró Correctamente.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        VarPnsu();
        //        Up_Componente.Update();

        //    }
        //    else
        //    {
        //        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    }

        //}

        //protected void btnComponentesPMIB_Click(object sender, EventArgs e)
        //{
        //    VarPmib();
        //    Up_VarPMIB.Update();
        //    MPE_PMIB.Show();
        //}

        //private void VarPmib()
        //{
        //    DataTable _obj;
        //    BEVarPimb _objVarPimb = new BEVarPimb();
        //    _objVarPimb.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
        //    _obj = objBLProyecto.spSOL_Seguimiento_Variable_PMIB(_objVarPimb);

        //    if (_obj.Rows.Count > 0)
        //    {
        //        txtP_concreto_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_concreto_m2"].ToString()).ToString("N");
        //        txtP_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["P_concreto_co"].ToString()).ToString("N");
        //        txtP_concreto_cu.Text = (Convert.ToDouble(txtP_concreto_co.Text) / Convert.ToDouble(txtP_concreto_m2.Text)).ToString("N");

        //        txtP_asfalto_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_asfalto_m2"].ToString()).ToString("N");
        //        txtP_asfalto_co.Text = Convert.ToDouble(_obj.Rows[0]["P_asfalto_co"].ToString()).ToString("N");
        //        txtP_asfalto_cu.Text = (Convert.ToDouble(txtP_asfalto_co.Text) / Convert.ToDouble(txtP_asfalto_m2.Text)).ToString("N");

        //        txtP_emboquillado_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_emboquillado_m2"].ToString()).ToString("N");//
        //        txtP_emboquillado_co.Text = Convert.ToDouble(_obj.Rows[0]["P_emboquillado_co"].ToString()).ToString("N");//
        //        txtP_emboquillado_cu.Text = (Convert.ToDouble(txtP_emboquillado_co.Text) / Convert.ToDouble(txtP_emboquillado_m2.Text)).ToString("N");

        //        txtP_adoquinado_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_adoquinado_m2"].ToString()).ToString("N");//
        //        txtP_adoquinado_co.Text = Convert.ToDouble(_obj.Rows[0]["P_adoquinado_co"].ToString()).ToString("N");//
        //        txtP_adoquinado_cu.Text = (Convert.ToDouble(txtP_adoquinado_co.Text) / Convert.ToDouble(txtP_adoquinado_m2.Text)).ToString("N");

        //        divAutomatica(txtP_concreto_m2, txtP_concreto_cu, txtP_concreto_co);
        //        divAutomatica(txtP_asfalto_m2, txtP_asfalto_cu, txtP_asfalto_co);
        //        divAutomatica(txtP_emboquillado_m2, txtP_emboquillado_cu, txtP_emboquillado_co);
        //        divAutomatica(txtP_adoquinado_m2, txtP_adoquinado_cu, txtP_adoquinado_co);

        //        lblCostoP.Text = (Convert.ToDouble(txtP_concreto_co.Text) + Convert.ToDouble(txtP_asfalto_co.Text) + Convert.ToDouble(txtP_emboquillado_co.Text) + Convert.ToDouble(txtP_adoquinado_co.Text)).ToString("N");


        //        txtV_concreto_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_concreto_m2"].ToString()).ToString("N");//
        //        txtV_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["V_concreto_co"].ToString()).ToString("N");//
        //        txtV_concreto_cu.Text = (Convert.ToDouble(txtV_concreto_co.Text) / Convert.ToDouble(txtV_concreto_m2.Text)).ToString("N");

        //        txtV_adoquinado_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_adoquinado_m2"].ToString()).ToString("N");//
        //        txtV_adoquinado_co.Text = Convert.ToDouble(_obj.Rows[0]["V_adoquinado_co"].ToString()).ToString("N");//
        //        txtV_adoquinado_cu.Text = (Convert.ToDouble(txtV_adoquinado_co.Text) / Convert.ToDouble(txtV_adoquinado_m2.Text)).ToString("N");

        //        txtV_piedra_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_piedra_m2"].ToString()).ToString("N");
        //        txtV_piedra_co.Text = Convert.ToDouble(_obj.Rows[0]["V_piedra_co"].ToString()).ToString("N");
        //        txtV_piedra_cu.Text = (Convert.ToDouble(txtV_piedra_co.Text) / Convert.ToDouble(txtV_piedra_m2.Text)).ToString("N");

        //        txtV_emboquillado_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_emboquillado_m2"].ToString()).ToString("N");//
        //        txtV_emboquillado_co.Text = Convert.ToDouble(_obj.Rows[0]["V_emboquillado_co"].ToString()).ToString("N");//
        //        txtV_emboquillado_cu.Text = (Convert.ToDouble(txtV_emboquillado_co.Text) / Convert.ToDouble(txtV_emboquillado_m2.Text)).ToString("N");

        //        divAutomatica(txtV_concreto_m2, txtV_concreto_cu, txtV_concreto_co);
        //        divAutomatica(txtV_adoquinado_m2, txtV_adoquinado_cu, txtV_adoquinado_co);
        //        divAutomatica(txtV_piedra_m2, txtV_piedra_cu, txtV_piedra_co);
        //        divAutomatica(txtV_emboquillado_m2, txtV_emboquillado_cu, txtV_emboquillado_co);

        //        lblCostoV.Text = (Convert.ToDouble(txtV_concreto_co.Text) + Convert.ToDouble(txtV_adoquinado_co.Text) + Convert.ToDouble(txtV_piedra_co.Text) + Convert.ToDouble(txtV_emboquillado_co.Text)).ToString("N");


        //        txtD_concreto_ml.Text = Convert.ToDouble(_obj.Rows[0]["D_concreto_ml"].ToString()).ToString("N");
        //        txtD_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["D_concreto_co"].ToString()).ToString("N");
        //        txtD_concreto_cu.Text = (Convert.ToDouble(txtD_concreto_co.Text) / Convert.ToDouble(txtD_concreto_ml.Text)).ToString("N");

        //        txtD_piedra_ml.Text = _obj.Rows[0]["D_piedra_ml"].ToString();
        //        txtD_piedra_co.Text = _obj.Rows[0]["D_piedra_co"].ToString();
        //        txtD_piedra_cu.Text = (Convert.ToDouble(txtD_piedra_co.Text) / Convert.ToDouble(txtD_piedra_ml.Text)).ToString("N");

        //        divAutomatica(txtD_concreto_ml, txtD_concreto_cu, txtD_concreto_co);
        //        divAutomatica(txtD_piedra_ml, txtD_piedra_cu, txtD_piedra_co);

        //        lblCostoD.Text = (Convert.ToDouble(txtD_concreto_co.Text) + Convert.ToDouble(txtD_piedra_co.Text)).ToString("N");


        //        txtO_muro_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_muro_m2"].ToString()).ToString("N");
        //        txtO_muro_co.Text = Convert.ToDouble(_obj.Rows[0]["O_muro_co"].ToString()).ToString("N");
        //        txtO_muro_cu.Text = (Convert.ToDouble(txtO_muro_co.Text) / Convert.ToDouble(txtO_muro_m2.Text)).ToString("N");

        //        txtO_ponton_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_ponton_m2"].ToString()).ToString("N");
        //        txtO_ponton_co.Text = Convert.ToDouble(_obj.Rows[0]["O_ponton_co"].ToString()).ToString("N");
        //        txtO_ponton_cu.Text = (Convert.ToDouble(txtO_ponton_co.Text) / Convert.ToDouble(txtO_ponton_m2.Text)).ToString("N");

        //        txtO_bermas_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_bermas_m2"].ToString()).ToString("N");
        //        txtO_bermas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_bermas_co"].ToString()).ToString("N");
        //        txtO_bermas_cu.Text = (Convert.ToDouble(txtO_bermas_co.Text) / Convert.ToDouble(txtO_bermas_m2.Text)).ToString("N");

        //        txtO_verdes_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_verdes_m2"].ToString()).ToString("N");
        //        txtO_verdes_co.Text = Convert.ToDouble(_obj.Rows[0]["O_verdes_co"].ToString()).ToString("N");
        //        txtO_verdes_cu.Text = (Convert.ToDouble(txtO_verdes_co.Text) / Convert.ToDouble(txtO_verdes_m2.Text)).ToString("N");

        //        txtO_jardineras_u.Text = _obj.Rows[0]["O_jardineras_u"].ToString();
        //        txtO_jardineras_co.Text = Convert.ToDouble(_obj.Rows[0]["O_jardineras_co"].ToString()).ToString("N");
        //        txtO_jardineras_cu.Text = (Convert.ToDouble(txtO_jardineras_co.Text) / Convert.ToDouble(txtO_jardineras_u.Text)).ToString("N");

        //        txtO_plantones_u.Text = _obj.Rows[0]["O_plantones_u"].ToString();
        //        txtO_plantones_co.Text = Convert.ToDouble(_obj.Rows[0]["O_plantones_co"].ToString()).ToString("N");
        //        txtO_plantones_cu.Text = (Convert.ToDouble(txtO_plantones_co.Text) / Convert.ToDouble(txtO_plantones_u.Text)).ToString("N");

        //        txtO_sanitarias_u.Text = _obj.Rows[0]["O_sanitarias_u"].ToString();
        //        txtO_sanitarias_co.Text = Convert.ToDouble(_obj.Rows[0]["O_sanitarias_co"].ToString()).ToString("N");
        //        txtO_sanitarias_cu.Text = (Convert.ToDouble(txtO_sanitarias_co.Text) / Convert.ToDouble(txtO_sanitarias_u.Text)).ToString("N");

        //        txtO_electricas_u.Text = _obj.Rows[0]["O_electricas_u"].ToString();
        //        txtO_electricas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_electricas_co"].ToString()).ToString("N");
        //        txtO_electricas_cu.Text = (Convert.ToDouble(txtO_electricas_co.Text) / Convert.ToDouble(txtO_electricas_u.Text)).ToString("N");

        //        txtO_bancas_u.Text = _obj.Rows[0]["O_bancas_u"].ToString();
        //        txtO_bancas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_bancas_co"].ToString()).ToString("N");
        //        txtO_bancas_cu.Text = (Convert.ToDouble(txtO_bancas_co.Text) / Convert.ToDouble(txtO_bancas_u.Text)).ToString("N");

        //        txtO_basureros_u.Text = _obj.Rows[0]["O_basureros_u"].ToString();
        //        txtO_basureros_co.Text = Convert.ToDouble(_obj.Rows[0]["O_basureros_co"].ToString()).ToString("N");
        //        txtO_basureros_cu.Text = (Convert.ToDouble(txtO_basureros_co.Text) / Convert.ToDouble(txtO_basureros_u.Text)).ToString("N");

        //        txtO_pergolas_u.Text = _obj.Rows[0]["O_pergolas_u"].ToString();
        //        txtO_pergolas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_pergolas_co"].ToString()).ToString("N");
        //        txtO_pergolas_cu.Text = (Convert.ToDouble(txtO_pergolas_co.Text) / Convert.ToDouble(txtO_pergolas_u.Text)).ToString("N");

        //        txtO_glorietas_u.Text = _obj.Rows[0]["O_glorietas_u"].ToString();
        //        txtO_glorietas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_glorietas_co"].ToString()).ToString("N");
        //        txtO_glorietas_cu.Text = (Convert.ToDouble(txtO_glorietas_co.Text) / Convert.ToDouble(txtO_glorietas_u.Text)).ToString("N");

        //        txtO_otros_u.Text = _obj.Rows[0]["O_otros_u"].ToString();
        //        txtO_otros_co.Text = Convert.ToDouble(_obj.Rows[0]["O_otros_co"].ToString()).ToString("N");
        //        txtO_otros_cu.Text = (Convert.ToDouble(txtO_otros_co.Text) / Convert.ToDouble(txtO_otros_u.Text)).ToString("N");
        //        txtObservacionesPMIB.Text = _obj.Rows[0]["observaciones"].ToString();

        //        lblUsuarioMetasPMIB.Text = "Actualizó: " + _obj.Rows[0]["usuario"].ToString() + " - " + _obj.Rows[0]["fecha_update"].ToString();

        //        divAutomatica(txtO_muro_m2, txtO_muro_cu, txtO_muro_co);
        //        divAutomatica(txtO_ponton_m2, txtO_ponton_cu, txtO_ponton_co);
        //        divAutomatica(txtO_bermas_m2, txtO_bermas_cu, txtO_bermas_co);
        //        divAutomatica(txtO_verdes_m2, txtO_verdes_cu, txtO_verdes_co);

        //        divAutomatica2(txtO_jardineras_u, txtO_jardineras_cu, txtO_jardineras_co);
        //        divAutomatica2(txtO_plantones_u, txtO_plantones_cu, txtO_plantones_co);
        //        divAutomatica2(txtO_sanitarias_u, txtO_sanitarias_cu, txtO_sanitarias_co);
        //        divAutomatica2(txtO_electricas_u, txtO_electricas_cu, txtO_electricas_co);

        //        divAutomatica2(txtO_bancas_u, txtO_bancas_cu, txtO_bancas_co);
        //        divAutomatica2(txtO_basureros_u, txtO_basureros_cu, txtO_basureros_co);
        //        divAutomatica2(txtO_pergolas_u, txtO_pergolas_cu, txtO_pergolas_co);
        //        divAutomatica2(txtO_glorietas_u, txtO_glorietas_cu, txtO_glorietas_co);
        //        divAutomatica2(txtO_otros_u, txtO_otros_cu, txtO_otros_co);

        //        lblCostoO.Text = (Convert.ToDouble(txtO_muro_co.Text) + Convert.ToDouble(txtO_ponton_co.Text) + Convert.ToDouble(txtO_bermas_co.Text) + Convert.ToDouble(txtO_verdes_co.Text) + Convert.ToDouble(txtO_jardineras_co.Text) + Convert.ToDouble(txtO_plantones_co.Text) + Convert.ToDouble(txtO_sanitarias_co.Text) + Convert.ToDouble(txtO_electricas_co.Text) + Convert.ToDouble(txtO_bancas_co.Text) + Convert.ToDouble(txtO_basureros_co.Text) + Convert.ToDouble(txtO_pergolas_co.Text) + Convert.ToDouble(txtO_glorietas_co.Text) + Convert.ToDouble(txtO_otros_co.Text)).ToString("N");

        //        lblCostoTotalObra.Text = (Convert.ToDouble(lblCostoP.Text) + Convert.ToDouble(lblCostoV.Text) + Convert.ToDouble(lblCostoD.Text) + Convert.ToDouble(lblCostoO.Text)).ToString("N");

        //    }
        //    else
        //    {
        //        //llenarcampos();
        //    }

        //}

        //protected void divAutomatica(TextBox uni, TextBox cu, TextBox co)
        //{
        //    if (uni.Text == "")
        //    { uni.Text = "0.00"; }
        //    else
        //    { uni.Text = Convert.ToDouble(uni.Text).ToString("N"); }

        //    if (co.Text == "")
        //    { co.Text = "0.00"; }
        //    else
        //    { co.Text = Convert.ToDouble(co.Text).ToString("N"); }

        //    if (uni.Text == "0.00" || uni.Text == "0")
        //    { cu.Text = "0.00"; }
        //    else { cu.Text = (Convert.ToDouble(co.Text) / Convert.ToDouble(uni.Text)).ToString("N"); }

        //}

        //protected void divAutomatica2(TextBox uni, TextBox cu, TextBox co)
        //{
        //    if (uni.Text == "")
        //    { uni.Text = "0"; }
        //    else
        //    { uni.Text = Convert.ToDouble(uni.Text).ToString(); }
        //    if (co.Text == "")
        //    { co.Text = "0.00"; }
        //    else
        //    { co.Text = Convert.ToDouble(co.Text).ToString("N"); }

        //    if (uni.Text == "0.00" || uni.Text == "0")
        //    { cu.Text = "0.00"; }
        //    else { cu.Text = (Convert.ToDouble(co.Text) / Convert.ToDouble(uni.Text)).ToString("N"); }

        //}

        //protected void Pavimento_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtP_concreto_m2, txtP_concreto_cu, txtP_concreto_co);
        //    divAutomatica(txtP_asfalto_m2, txtP_asfalto_cu, txtP_asfalto_co);
        //    divAutomatica(txtP_emboquillado_m2, txtP_emboquillado_cu, txtP_emboquillado_co);
        //    divAutomatica(txtP_adoquinado_m2, txtP_adoquinado_cu, txtP_adoquinado_co);
        //    CostosTotales();
        //}

        //protected void Veredas_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtV_concreto_m2, txtV_concreto_cu, txtV_concreto_co);
        //    divAutomatica(txtV_adoquinado_m2, txtV_adoquinado_cu, txtV_adoquinado_co);
        //    divAutomatica(txtV_piedra_m2, txtV_piedra_cu, txtV_piedra_co);
        //    divAutomatica(txtV_emboquillado_m2, txtV_emboquillado_cu, txtV_emboquillado_co);
        //    CostosTotales();
        //}

        //protected void Drenaje_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtD_concreto_ml, txtD_concreto_cu, txtD_concreto_co);
        //    divAutomatica(txtD_piedra_ml, txtD_piedra_cu, txtD_piedra_co);
        //    CostosTotales();
        //}

        //protected void Otros_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtO_muro_m2, txtO_muro_cu, txtO_muro_co);
        //    divAutomatica(txtO_ponton_m2, txtO_ponton_cu, txtO_ponton_co);
        //    divAutomatica(txtO_bermas_m2, txtO_bermas_cu, txtO_bermas_co);
        //    divAutomatica(txtO_verdes_m2, txtO_verdes_cu, txtO_verdes_co);

        //    divAutomatica2(txtO_jardineras_u, txtO_jardineras_cu, txtO_jardineras_co);
        //    divAutomatica2(txtO_plantones_u, txtO_plantones_cu, txtO_plantones_co);
        //    divAutomatica2(txtO_sanitarias_u, txtO_sanitarias_cu, txtO_sanitarias_co);
        //    divAutomatica2(txtO_electricas_u, txtO_electricas_cu, txtO_electricas_co);

        //    divAutomatica2(txtO_bancas_u, txtO_bancas_cu, txtO_bancas_co);
        //    divAutomatica2(txtO_basureros_u, txtO_basureros_cu, txtO_basureros_co);
        //    divAutomatica2(txtO_pergolas_u, txtO_pergolas_cu, txtO_pergolas_co);
        //    divAutomatica2(txtO_glorietas_u, txtO_glorietas_cu, txtO_glorietas_co);
        //    divAutomatica(txtO_otros_u, txtO_otros_cu, txtO_otros_co);
        //    CostosTotales();
        //}

        //protected void CostosTotales()
        //{
        //    lblCostoP.Text = (Convert.ToDouble(txtP_concreto_co.Text) + Convert.ToDouble(txtP_asfalto_co.Text) + Convert.ToDouble(txtP_emboquillado_co.Text) + Convert.ToDouble(txtP_adoquinado_co.Text)).ToString("N");

        //    lblCostoV.Text = (Convert.ToDouble(txtV_concreto_co.Text) + Convert.ToDouble(txtV_adoquinado_co.Text) + Convert.ToDouble(txtV_piedra_co.Text) + Convert.ToDouble(txtV_emboquillado_co.Text)).ToString("N");
        //    lblCostoD.Text = (Convert.ToDouble(txtD_concreto_co.Text) + Convert.ToDouble(txtD_piedra_co.Text)).ToString("N");

        //    lblCostoO.Text = (Convert.ToDouble(txtO_muro_co.Text) + Convert.ToDouble(txtO_ponton_co.Text) + Convert.ToDouble(txtO_bermas_co.Text) + Convert.ToDouble(txtO_verdes_co.Text) + Convert.ToDouble(txtO_jardineras_co.Text) + Convert.ToDouble(txtO_plantones_co.Text) + Convert.ToDouble(txtO_sanitarias_co.Text) + Convert.ToDouble(txtO_electricas_co.Text) + Convert.ToDouble(txtO_bancas_co.Text) + Convert.ToDouble(txtO_basureros_co.Text) + Convert.ToDouble(txtO_pergolas_co.Text) + Convert.ToDouble(txtO_glorietas_co.Text) + Convert.ToDouble(txtO_otros_co.Text)).ToString("N");

        //    lblCostoTotalObra.Text = (Convert.ToDouble(lblCostoP.Text) + Convert.ToDouble(lblCostoV.Text) + Convert.ToDouble(lblCostoD.Text) + Convert.ToDouble(lblCostoO.Text)).ToString("N");

        //}

        //protected void btnGuardarMetasPMIB_Click(object sender, EventArgs e)
        //{
        //    BEVarPimb _BEVarPimb = new BEVarPimb();

        //    _BEVarPimb.Id_Solicitudes = Convert.ToInt64(LblID_SOLICITUD.Text);

        //    _BEVarPimb.P_concreto_m2 = Convert.ToDouble(txtP_concreto_m2.Text);
        //    _BEVarPimb.P_concreto_co = Convert.ToDouble(txtP_concreto_co.Text);
        //    _BEVarPimb.P_asfalto_m2 = Convert.ToDouble(txtP_asfalto_m2.Text);
        //    _BEVarPimb.P_asfalto_co = Convert.ToDouble(txtP_asfalto_co.Text);
        //    _BEVarPimb.P_emboquillado_m2 = Convert.ToDouble(txtP_emboquillado_m2.Text);
        //    _BEVarPimb.P_emboquillado_co = Convert.ToDouble(txtP_emboquillado_co.Text);
        //    _BEVarPimb.P_adoquinado_m2 = Convert.ToDouble(txtP_adoquinado_m2.Text);
        //    _BEVarPimb.P_adoquinado_co = Convert.ToDouble(txtP_adoquinado_co.Text);

        //    _BEVarPimb.V_concreto_m2 = Convert.ToDouble(txtV_concreto_m2.Text);
        //    _BEVarPimb.V_concreto_co = Convert.ToDouble(txtV_concreto_co.Text);
        //    _BEVarPimb.V_adoquinado_m2 = Convert.ToDouble(txtV_adoquinado_m2.Text);
        //    _BEVarPimb.V_adoquinado_co = Convert.ToDouble(txtV_adoquinado_co.Text);
        //    _BEVarPimb.V_piedra_m2 = Convert.ToDouble(txtV_piedra_m2.Text);
        //    _BEVarPimb.V_piedra_co = Convert.ToDouble(txtV_piedra_co.Text);
        //    _BEVarPimb.V_emboquillado_m2 = Convert.ToDouble(txtV_emboquillado_m2.Text);
        //    _BEVarPimb.V_emboquillado_co = Convert.ToDouble(txtV_emboquillado_co.Text);

        //    _BEVarPimb.D_concreto_ml = Convert.ToDouble(txtD_concreto_ml.Text);
        //    _BEVarPimb.D_concreto_co = Convert.ToDouble(txtD_concreto_co.Text);
        //    _BEVarPimb.D_piedra_ml = Convert.ToDouble(txtD_piedra_ml.Text);
        //    _BEVarPimb.D_piedra_co = Convert.ToDouble(txtD_piedra_co.Text);

        //    _BEVarPimb.O_muro_m2 = Convert.ToDouble(txtO_muro_m2.Text);
        //    _BEVarPimb.O_muro_co = Convert.ToDouble(txtO_muro_co.Text);
        //    _BEVarPimb.O_ponton_m2 = Convert.ToDouble(txtO_ponton_m2.Text);
        //    _BEVarPimb.O_ponton_co = Convert.ToDouble(txtO_ponton_co.Text);
        //    _BEVarPimb.O_bermas_m2 = Convert.ToDouble(txtO_bermas_m2.Text);
        //    _BEVarPimb.O_bermas_co = Convert.ToDouble(txtO_bermas_co.Text);
        //    _BEVarPimb.O_verdes_m2 = Convert.ToDouble(txtO_verdes_m2.Text);
        //    _BEVarPimb.O_verdes_co = Convert.ToDouble(txtO_verdes_co.Text);

        //    _BEVarPimb.O_jardineras_u = Convert.ToInt32(txtO_jardineras_u.Text);
        //    _BEVarPimb.O_jardineras_co = Convert.ToDouble(txtO_jardineras_co.Text);
        //    _BEVarPimb.O_plantones_u = Convert.ToInt32(txtO_plantones_u.Text);
        //    _BEVarPimb.O_plantones_co = Convert.ToDouble(txtO_plantones_co.Text);
        //    _BEVarPimb.O_sanitarias_u = Convert.ToInt32(txtO_sanitarias_u.Text);
        //    _BEVarPimb.O_sanitarias_co = Convert.ToDouble(txtO_sanitarias_co.Text);
        //    _BEVarPimb.O_electricas_u = Convert.ToInt32(txtO_electricas_u.Text);
        //    _BEVarPimb.O_electricas_co = Convert.ToDouble(txtO_electricas_co.Text);

        //    _BEVarPimb.O_bancas_u = Convert.ToInt32(txtO_bancas_u.Text);
        //    _BEVarPimb.O_bancas_co = Convert.ToDouble(txtO_bancas_co.Text);
        //    _BEVarPimb.O_basureros_u = Convert.ToInt32(txtO_basureros_u.Text);
        //    _BEVarPimb.O_basureros_co = Convert.ToDouble(txtO_basureros_co.Text);
        //    _BEVarPimb.O_pergolas_u = Convert.ToInt32(txtO_pergolas_u.Text);
        //    _BEVarPimb.O_pergolas_co = Convert.ToDouble(txtO_pergolas_co.Text);
        //    _BEVarPimb.O_glorietas_u = Convert.ToInt32(txtO_glorietas_u.Text);
        //    _BEVarPimb.O_glorietas_co = Convert.ToDouble(txtO_glorietas_co.Text);
        //    _BEVarPimb.O_otros_u = Convert.ToDouble(txtO_otros_u.Text);
        //    _BEVarPimb.O_otros_co = Convert.ToDouble(txtO_otros_co.Text);
        //    _BEVarPimb.Observaciones = txtObservacionesPMIB.Text;

        //    _BEVarPimb.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //    objBLProyecto.spiuSOL_IngresarVariablePimb(_BEVarPimb);

        //    //string script = "<script>alert('Se actualizó las metas.'); window.opener.location.reload(); window.close();</script>";
        //    string script = "<script>alert('Se actualizó las metas.');</script>";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        //    VarPmib();
        //    Up_VarPMIB.Update();

        //}

        #endregion

        #region Tab3

        protected void CargaTab3()
        {

            CargaLiquidacion();

            txtTotalTransferencia.Text = txtAcumuladorTranferenciaTab0.Text;
        }

        protected void CargaLiquidacion()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BELiquidacion.tipoFinanciamiento = 2;

            dt = _objBLLiquidacion.sp_MON_Liquidacion(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                txtLiquidacionTab3.Text = dt.Rows[0]["resolucion"].ToString();
                txtFechaTab3.Text = dt.Rows[0]["fecha"].ToString();
                txtMontoTab3.Text = dt.Rows[0]["montoLiquidacion"].ToString();
                txtSaldoTab3.Text = dt.Rows[0]["saldoDevolver"].ToString();

                txtResolucionLiqSupervision.Text = dt.Rows[0]["resolucionSupervision"].ToString();
                txtFechaLiqSupervision.Text = dt.Rows[0]["fechaLiqSupervision"].ToString();
                txtMontoLiqSupervision.Text = dt.Rows[0]["montoLiqSupervision"].ToString();

                LnkbtnLiquidacionTab3.Text = dt.Rows[0]["urlLiquidacion"].ToString();
                LnkbtnActaTab3.Text = dt.Rows[0]["urlActaCierre"].ToString();
                LnkbtnDevolucionTab3.Text = dt.Rows[0]["urlDocDevolucion"].ToString();
                lnkbtnLiqSupervisionTab3.Text = dt.Rows[0]["urlLiquidacionSupervision"].ToString();

                lnkbtnAprobacionExpedienteTab3.Text = dt.Rows[0]["urlAprobacionExpedienteTecnico"].ToString();
                

                double val1 = 0;
                if (txtMontoTab3.Text != "")
                { val1 = Convert.ToDouble(txtMontoTab3.Text); }

                double val2 = 0;
                if (txtMontoLiqSupervision.Text != "")
                { val2 = Convert.ToDouble(txtMontoLiqSupervision.Text); }

                double val3 = 0;
                if (txtSaldoTab3.Text != "")
                { val3 = Convert.ToDouble(txtSaldoTab3.Text); }

                txtTotalTab3.Text = (val1 + val2 + val3).ToString("N");

                lblNomLiquidacion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                GeneraIcoFile(LnkbtnLiquidacionTab3.Text, imgbtnLiquidacionTab3);
                GeneraIcoFile(lnkbtnLiqSupervisionTab3.Text, imgbtnLiqSupervisionTab3);
                GeneraIcoFile(LnkbtnActaTab3.Text, imgbtnActaTab3);
                GeneraIcoFile(LnkbtnDevolucionTab3.Text, imgbtnDevolucionTab3);
                GeneraIcoFile(lnkbtnAprobacionExpedienteTab3.Text, imgbtnAprobacionExpedienteTab3);


            }

        }

        protected Boolean validaLiquidacionTab3()
        {
            if (txtFechaTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    return false;
                }

                if (Convert.ToDateTime(txtFechaTab3.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La Fecha de Liquidación no debe ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (txtFechaTermino.Text.Length > 0 && Convert.ToDateTime(txtFechaTab3.Text) < Convert.ToDateTime(txtFechaTermino.Text))
                {
                    string script = "<script>alert('La fecha de liquidación debe ser mayor a la fecha de termino real.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

            }
            return true;
        }
        protected void btnGuardarTab3_OnClick(object sender, EventArgs e)
        {
            if (validaLiquidacionTab3())
            {
                if (validaArchivo(FileUploadDevolucionTab3))
                {
                    if (validaArchivo(FileUploadLiquidacionTab3))
                    {
                        if (validaArchivo(FileUploadActaTab3))
                        {

                            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BELiquidacion.tipoFinanciamiento = 2;

                            _BELiquidacion.resolucion = txtLiquidacionTab3.Text;
                            _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaTab3.Text);
                            _BELiquidacion.monto = txtMontoTab3.Text;
                            _BELiquidacion.saldoDevolver = txtSaldoTab3.Text;

                            _BELiquidacion.ResolucionSupervision = txtResolucionLiqSupervision.Text;
                            _BELiquidacion.Date_fechaLiqSupervision = VerificaFecha(txtFechaLiqSupervision.Text);
                            _BELiquidacion.MontoLiqSupervision = txtMontoLiqSupervision.Text;

                            _BELiquidacion.Date_fechaLiqObraComplem = VerificaFecha("");

                            _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                            if (FileUploadLiqSupervisionTab3.HasFile)
                            {
                                _BELiquidacion.UrlLiquidacionSupervision = _Metodo.uploadfile(FileUploadLiqSupervisionTab3);
                            }
                            else
                            { _BELiquidacion.UrlLiquidacionSupervision = lnkbtnLiqSupervisionTab3.Text; }

                            if (FileUploadLiquidacionTab3.HasFile)
                            {
                                _BELiquidacion.urlLiquidacion = _Metodo.uploadfile(FileUploadLiquidacionTab3);
                            }
                            else
                            { _BELiquidacion.urlLiquidacion = LnkbtnLiquidacionTab3.Text; }

                            if (FileUploadActaTab3.HasFile)
                            {
                                _BELiquidacion.urlActaCierre = _Metodo.uploadfile(FileUploadActaTab3);
                            }
                            else
                            { _BELiquidacion.urlActaCierre = LnkbtnActaTab3.Text; }

                            if (FileUploadDevolucionTab3.HasFile)
                            {
                                _BELiquidacion.urlDocDevolucion = _Metodo.uploadfile(FileUploadDevolucionTab3);
                            }
                            else
                            { _BELiquidacion.urlDocDevolucion = LnkbtnDevolucionTab3.Text; }

                            if (FileUploadAprobacionExpedienteTab3.HasFile)
                            {
                                _BELiquidacion.urlAprobacionExpediente = _Metodo.uploadfile(FileUploadAprobacionExpedienteTab3);
                            }
                            else
                            { _BELiquidacion.urlAprobacionExpediente = lnkbtnAprobacionExpedienteTab3.Text; }

                            _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLLiquidacion.spi_MON_Liquidacion(_BELiquidacion);


                            if (val == 1)
                            {
                                string script = "<script>alert('Se guardo correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                CargaLiquidacion();

                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }


                            Up_Tab3.Update();

                        }

                    }

                }

            }
        }

        protected void LnkbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        {

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        {
            ImageButton boton;
            boton = (ImageButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnLiquidacion_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnLiquidacionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void txtMontoTab3_OnClick(object sender, EventArgs e)
        {
            double val1 = 0;
            if (txtMontoTab3.Text != "")
            { val1 = Convert.ToDouble(txtMontoTab3.Text); }
            txtMontoTab3.Text = val1.ToString("N");

            double val2 = 0;
            if (txtMontoLiqSupervision.Text != "")
            { val2 = Convert.ToDouble(txtMontoLiqSupervision.Text); }

            double val3 = 0;
            if (txtSaldoTab3.Text != "")
            { val3 = Convert.ToDouble(txtSaldoTab3.Text); }
            txtSaldoTab3.Text = val3.ToString("N");

            txtTotalTab3.Text = (val1 + val2 + val3).ToString("N");

            Up_Tab3.Update();

        }




        #endregion

        #region Tab4
        protected void CargaTab4()
        {
            cargaPlazoTAb4();
            cargaPersupuestoTab4();
            cargaDeductivoTab4();
        }

        protected void cargaPlazoTAb4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 1;

            grdPlazoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPlazoTab4.DataBind();

        }

        protected void grdPlazoTab4_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPlaTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(lblTotalDias.Text) + Convert.ToInt32(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);



                lblTotalDias.Text = TotalDias.ToString();
            }

        }

        protected void cargaPersupuestoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 2;

            grdPresupuestoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPresupuestoTab4.DataBind();

        }

        protected void grdPresupuestoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPreTab4");
                Label dias = (Label)e.Row.FindControl("lblPresupuesto");

                TotalDias = Convert.ToDouble(lblPresupuestoAdicional.Text) + Convert.ToDouble(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                lblPresupuestoAdicional.Text = TotalDias.ToString("N");
            }
        }

        protected void cargaDeductivoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 3;

            grdDeductivoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdDeductivoTab4.DataBind();

        }

        protected void grdDeductivoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double TotalDias;
            TotalDias = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocDedTab4");
                Label dias = (Label)e.Row.FindControl("lbldeductivo");

                TotalDias = Convert.ToDouble(lblDeductivoTotal.Text) + Convert.ToDouble(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                lblDeductivoTotal.Text = TotalDias.ToString("N");
            }

        }

        protected void imgAgregaPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = false;
            btnGuardarPlazoTab4.Visible = true;
            lblNomUsuarioAmpDias.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = false;
            imgAgregaPlazoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPlazoTab4()
        {
            Boolean result;
            result = true;

            if (txtAmpPlazoPlaTab4.Text == "")
            {
                string script = "<script>alert('Ingrese dias de Ampliación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtResolAproPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaResolPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPlazoTab4())
            {

                if (validaArchivo(FileUploadPlaTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 2;
                    _BEAmpliacion.id_tipo_ampliacion = 1;

                    _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        lblTotalDias.Text = "0";
                        cargaPlazoTAb4();
                        ActualizaFechasTab1();
                        txtAmpPlazoPlaTab4.Text = "";
                        txtResolAproPlaTab4.Text = "";
                        txtFechaResolPlaTab4.Text = "";
                        txtConceptoPlaTab4.Text = "";
                        txtObservacionPlaTab4.Text = "";
                        txtFechaInicio.Text = "";
                        txtFechaFin.Text = "";

                        Panel_PlazoTab4.Visible = false;
                        imgAgregaPlazoTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPlaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPlazoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPlazoTab4.Rows[row.RowIndex].FindControl("imgDocPlaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imbtnAgregarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            lblNomUsuarioPresupuesto.Text = "";
            btnGuardarPresupuestoTab4.Visible = true;
            btnModificarPresupuestoTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void btnCancelarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPresupuestoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPresupuestoTab4())
            {

                if (validaArchivo(FileUploadPreTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 2;
                    _BEAmpliacion.id_tipo_ampliacion = 2;

                    _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha("");
                    _BEAmpliacion.Date_FechaFin = VerificaFecha("");


                    _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                    _BEAmpliacion.observacion = txtObsPreTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaPersupuestoTab4();

                        txtMontoPreTab4.Text = "";
                        txtVincuPreTab4.Text = "";
                        txtResolAproPreTab4.Text = "";
                        txtFechaPreTab4.Text = "";
                        txtIncidenciaPreTab4.Text = "";

                        txtConceptoPreTab4.Text = "";
                        txtObsPreTab4.Text = "";

                        Panel_PresupuestoTab4.Visible = false;
                        imbtnAgregarPresupuestoTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPreTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPresupuestoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPresupuestoTab4.Rows[row.RowIndex].FindControl("imgDocPreTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = false;
            btnGuardarDeductivoTab4.Visible = true;
            lblNomUsuarioDeductivo.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarDeductivoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarDeductivoTab4())
            {

                if (validaArchivo(FileUploadDedTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 2;
                    _BEAmpliacion.id_tipo_ampliacion = 3;

                    _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha("");
                    _BEAmpliacion.Date_FechaFin = VerificaFecha("");

                    _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                    _BEAmpliacion.observacion = txtObsDedTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaDeductivoTab4();

                        txtMontoDedTab4.Text = "";
                        txtVincuDedTab4.Text = "";
                        txtResolAproDedTab4.Text = "";
                        txtFechaDedTab4.Text = "";
                        txtIncidenciaDedTab4.Text = "";

                        txtConceptoDedTab4.Text = "";
                        txtObsDedTab4.Text = "";

                        Panel_deductivoTab4.Visible = false;
                        imgbtnDeductivoTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocDedTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdDeductivoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdDeductivoTab4.Rows[row.RowIndex].FindControl("imgDocDedTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        #endregion

        #region Tab6

        protected void CargaTab6()
        {
            CargaPanelTab6();
            CargaTipoDocumentoMonitorTab6();
        }

        protected void CargaTipoDocumentoMonitorTab6()
        {
            ddlTipoArchivoTab6.DataSource = _objBLBandeja.F_spMON_TipoDocumentoMonitor();
            ddlTipoArchivoTab6.DataValueField = "valor";
            ddlTipoArchivoTab6.DataTextField = "nombre";
            ddlTipoArchivoTab6.DataBind();

            ddlTipoArchivoTab6.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void CargaPanelTab6()
        {
            _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdPanelTab6.DataSource = _objBLBandeja.F_spMON_DocumentoMonitor(_BEBandeja);
            grdPanelTab6.DataBind();
        }

        protected void imgbtnAgregarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnGuardarPanelTab6.Visible = true;
            btnModificarPanelTab6.Visible = false;
            lblNomUsuarioDocumento.Text = "";

            txtFechaTab6.Text = "";
            txtDescripcionTab6.Text = "";
            txtNombreArchivoTab6.Text = "";
            ddlTipoArchivoTab6.SelectedValue = "";
            imgbtnDocTab6.ImageUrl = "~/img/blanco.png";
            LnkbtnDocTab6.Text = "";
            Up_Tab6.Update();
        }

        protected void btnCancelarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = false;
            imgbtnAgregarPanelTab6.Visible = true;
            Up_Tab6.Update();
        }

        protected void imgDocTab6_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdPanelTab6.Rows[row.RowIndex].FindControl("imgDocTab6");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void btnGuardarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;
                    _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLBandeja.spi_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Registro correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;
                        Up_Tab6.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }
        }
        protected void grdPanelTab6_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab6");

                GeneraIcoFile(imb.ToolTip, imb);



            }
        }


        protected void grdPanelTab6_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPanelTab6.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.Id_documentoMonitor = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.tipo = "2";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);

                int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaPanelTab6();
                    Up_Tab6.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPanelTab6_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_documento_monitor.Text = grdPanelTab6.SelectedDataKey.Value.ToString();

            GridViewRow row = grdPanelTab6.SelectedRow;

            txtNombreArchivoTab6.Text = ((Label)row.FindControl("lblNombre")).Text;
            txtDescripcionTab6.Text = ((Label)row.FindControl("lblDescripcion")).Text;

            ddlTipoArchivoTab6.SelectedValue = ((Label)row.FindControl("lblIDTipo")).Text;

            txtFechaTab6.Text = ((Label)row.FindControl("lblFecha")).Text;
            LnkbtnDocTab6.Text = ((ImageButton)row.FindControl("imgDocTab6")).ToolTip;

            lblNomUsuarioDocumento.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnDocTab6.Text, imgbtnDocTab6);



            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnModificarPanelTab6.Visible = true;
            btnGuardarPanelTab6.Visible = false;
            Up_Tab6.Update();
        }

        protected Boolean validarPanelTab6()
        {
            Boolean result;
            result = true;

            if (txtNombreArchivoTab6.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaTab6.Text == "")
            {
                string script = "<script>alert('Ingresar fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (FileUpload_Tab6.HasFile == false && LnkbtnDocTab6.Text == "")
            {
                string script = "<script>alert('Adjuntar archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (ddlTipoArchivoTab6.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (ddlTipoArchivoTab6.SelectedValue.Equals("1"))
                {
                    if (validaArchivoFotos(FileUpload_Tab6) == false)
                    {
                        result = false;
                        return result;
                    }
                }
            }


            return result;


        }

        protected void btnModificarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    _BEBandeja.Id_documentoMonitor = Convert.ToInt32(lblId_documento_monitor.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;

                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEBandeja.tipo = "1";

                    if (FileUpload_Tab6.HasFile)
                    {
                        _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    }
                    else
                    {
                        _BEBandeja.UrlDoc = LnkbtnDocTab6.Text;
                    }


                    int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;
                        Up_Tab6.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }



        }

        protected void LnkbtnDocTab6_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkbtnDocTab6.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        #endregion

        #region Tab7

        protected void CargaTab7()
        {
            cargaConclusionTab7();

        }

        protected void cargaConclusionTab7()
        {
            _BEConclusion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEConclusion.tipoFinanciamiento = 2;

            dt = _objBLConclusion.spMON_Conclusion(_BEConclusion);

            if (dt.Rows.Count > 0)
            {
                txtConclusionTab7.Text = dt.Rows[0]["conclusiones"].ToString();
                txtRecomendacionTab7.Text = dt.Rows[0]["observaciones"].ToString();
                //txtFechaREgistroTab7.Text = dt.Rows[0]["fecha_registro"].ToString();
                lnkbtnFileTab7.Text = dt.Rows[0]["urlDoc"].ToString();
                lblNomUsuarioConclusion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                GeneraIcoFile(lnkbtnFileTab7.Text, imgbtnFileTab7);



            }

        }

        protected void lnkbtnFileTab7_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnFileTab7.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnFileTab7.Text);
                Response.End();
            }

        }

        protected void imgbtnFileTab7_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnFileTab7.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnFileTab7.Text);
                Response.End();
            }

        }
        protected void btnGuardarTab7_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadTab7))
            {
                _BEConclusion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEConclusion.tipoFinanciamiento = 2;

                _BEConclusion.conclusion = txtConclusionTab7.Text;
                _BEConclusion.observacion = txtRecomendacionTab7.Text;
                _BEConclusion.urlDoc = _Metodo.uploadfile(FileUploadTab7);
                _BEConclusion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLConclusion.spi_MON_Conclusion(_BEConclusion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaConclusionTab7();

                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        //protected void btnModificarContrapartidasTab0_OnClick(object sender, EventArgs e)
        //{
        //    if (ValidarContrapartidaTab0())
        //    {

        //        _BEFinanciamiento.id_financiamientoContrapartida = Convert.ToInt32(lblIDTipoContrapartida.Text);
        //        _BEFinanciamiento.tipo = 1;
        //        _BEFinanciamiento.entidad_cooperante = txtEntidadCooperanteTab0.Text;
        //        _BEFinanciamiento.documento = txtDocumentoTab0.Text;
        //        _BEFinanciamiento.monto = txtMontoTab0.Text;
        //        _BEFinanciamiento.id_tipo_aporte = Convert.ToInt32(ddlTipoAporteTab0.SelectedValue.ToString());
        //        _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEFinanciamiento.MontoObra = txtMontoObraTab0.Text;
        //        _BEFinanciamiento.MontoSupervision = txtMontoSupervisionTab0.Text;

        //        int val = _objBLFinanciamiento.spud_MON_FinanContrapartida(_BEFinanciamiento);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Actualización Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            CargaContrapartidaTab0();

        //            txtEntidadCooperanteTab0.Text = "";
        //            txtDocumentoTab0.Text = "";
        //            txtMontoTab0.Text = "";
        //            ddlTipoAporteTab0.SelectedValue = "";

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }
        //        Panel_ContrapartidaTab0.Visible = false;
        //        btnAgregarContrapartidasTab0.Visible = true;
        //        Up_Tab0.Update();
        //    }
        //}

        //protected void grdContrapartidasTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grdContrapartidasTab0.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEFinanciamiento.id_financiamientoContrapartida = Convert.ToInt32(objTemp.ToString());
        //        _BEFinanciamiento.tipo = 2;
        //        _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLFinanciamiento.spud_MON_FinanContrapartida(_BEFinanciamiento);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            CargaContrapartidaTab0();
        //            Up_Tab0.Update();


        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grdContrapartidasTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id_financiamiento_contrapartida;
        //    id_financiamiento_contrapartida = grdContrapartidasTab0.SelectedDataKey.Value.ToString();
        //    lblIDTipoContrapartida.Text = id_financiamiento_contrapartida.ToString();
        //    GridViewRow row = grdContrapartidasTab0.SelectedRow;
        //    txtEntidadCooperanteTab0.Text = ((Label)row.FindControl("lblEntidad")).Text;
        //    txtDocumentoTab0.Text = ((Label)row.FindControl("lblDocumento")).Text;
        //    txtMontoTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
        //    ddlTipoAporteTab0.SelectedValue = ((Label)row.FindControl("lblId_TipoAporte")).Text;
        //    txtMontoObraTab0.Text = Server.HtmlDecode(row.Cells[3].Text);
        //    txtMontoSupervisionTab0.Text = Server.HtmlDecode(row.Cells[4].Text);
        //    lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    btnModificarContrapartidasTab0.Visible = true;
        //    btnGuardarContrapartidasTab0.Visible = false;
        //    Panel_ContrapartidaTab0.Visible = true;
        //    btnAgregarContrapartidasTab0.Visible = false;
        //    Up_Tab0.Update();


        //}

        protected void btnModificarTransparenciaTab0_Click(object sender, EventArgs e)
        {

            if (ValidarTransferenciaTab0())
            {


                _BEFinanciamiento.id_financiamiento = Convert.ToInt32(lblIdTipoTransferencia.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEFinanciamiento.id_tipo_trans = Convert.ToInt32(ddlTipoTab0.SelectedValue);
                _BEFinanciamiento.nro_convenio = txtNroTransTab0.Text;
                _BEFinanciamiento.Date_Fecha_Convenio = Convert.ToDateTime(txtFechaNroTransTab0.Text);
                _BEFinanciamiento.montoConvenio = txtMontoNroTransTab0.Text;
                _BEFinanciamiento.dispositivo_aprobacion = txtDispositivoTransTab0.Text;
                _BEFinanciamiento.Date_fecha_aprobacion = Convert.ToDateTime(txtFechaDispTab0.Text);
                _BEFinanciamiento.montoAprobacion = txtMontoTransTab0.Text;
                _BEFinanciamiento.Tipo_transferencia = Convert.ToInt32(ddlTipoTransferenciaTab0.SelectedValue);
                _BEFinanciamiento.observacion = txtObservacionTransTab0.Text;
                if (FileUploadTransferenciaTab0.HasFile)
                {
                    _BEFinanciamiento.docUrl = _Metodo.uploadfile(FileUploadTransferenciaTab0);
                }
                else
                {
                    _BEFinanciamiento.docUrl = LnkbtnTransferenciamvc.Text;
                }


                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);



                //_BEFinanciamiento.tipoFinanciamiento = 3;



                int resul;
                resul = _objBLFinanciamiento.spud_MON_FinanTransferencia_Editar(_BEFinanciamiento);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaFinanTransferencia();

                    ddlTipoTab0.SelectedValue = "";
                    txtNroTransTab0.Text = "";
                    txtFechaNroTransTab0.Text = "";
                    txtMontoNroTransTab0.Text = "";
                    txtDispositivoTransTab0.Text = "";
                    txtFechaDispTab0.Text = "";
                    txtMontoTransTab0.Text = "";
                    txtObservacionTransTab0.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarTranasferenciaTab0.Visible = false;
                btnAgregarTransferenciaTab0.Visible = true;

                Up_Tab0.Update();



            }



        }

        protected void imgDocTransTab01_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnTransferenciamvc.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grd_TransferenciasTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_TransferenciasTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEFinaTra.id_finan_transferencia = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEFinaTra.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLFinaTra.spud_MON_FinanTransferencia(_BEFinaTra);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaFinanTransferencia();
                    Up_Tab0.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grd_TransferenciasTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_finan_transferencia;
            id_finan_transferencia = grd_TransferenciasTab0.SelectedDataKey.Value.ToString();
            lblIdTipoTransferencia.Text = id_finan_transferencia.ToString();
            GridViewRow row = grd_TransferenciasTab0.SelectedRow;
            ddlTipoTab0.SelectedValue = ((Label)row.FindControl("lblIdTipoFinanciamiento")).Text;
            txtMontoNroTransTab0.Text = Server.HtmlDecode(row.Cells[6].Text);
            txtMontoTransTab0.Text = Server.HtmlDecode(row.Cells[9].Text);
            txtNroTransTab0.Text = ((Label)row.FindControl("lblDetalle")).Text;
            txtDispositivoTransTab0.Text = ((Label)row.FindControl("lbldispo")).Text;
            txtObservacionTransTab0.Text = ((Label)row.FindControl("lblobservacionesTab0")).Text;
            txtFechaNroTransTab0.Text = ((Label)row.FindControl("lblFechaConvenio")).Text;
            txtFechaDispTab0.Text = ((Label)row.FindControl("lblFechaAprobación")).Text;
            LnkbtnTransferenciamvc.Text = ((ImageButton)row.FindControl("imgDocTransTab0")).ToolTip;

            ddlTipoTransferenciaTab0.SelectedValue = ((Label)row.FindControl("lblIdTipoTransferencia")).Text;

            lblNomUsuarioTransferencia.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnTransferenciamvc.Text, imgbtnTransferenciamvc);



            btnModificarTransparenciaTab0.Visible = true;
            btnGuardarTransferenciaTab0.Visible = false;
            Panel_AgregarTranasferenciaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            Up_Tab0.Update();

        }


        //protected void btnActualizarRegistroSOSEMTab0_OnClick(object sender, EventArgs e)
        //{

        //    if (ValidarSOSEMTab0())
        //    {

        //        _BESOSEM.id_seguimiento_sosem = Convert.ToInt32(lblRegistroObras.Text);
        //        //_BEFinanciamiento.tipo = 1;

        //        _BESOSEM.anio = ddlanioSOSEMTAb0.SelectedValue;
        //        _BESOSEM.pia = txtPiaSOSEMTab0.Text;
        //        _BESOSEM.pimAcumulado = txtPimAcumTab0.Text;
        //        _BESOSEM.devAcumulado = txtDevAcumTab0.Text;
        //        _BESOSEM.ene = txtEneSosTab0.Text;
        //        _BESOSEM.feb = txtFebSosTab0.Text;
        //        _BESOSEM.mar = txtMarSosTab0.Text;
        //        _BESOSEM.abr = txtAbrSosTab0.Text;
        //        _BESOSEM.may = txtMaySosTab0.Text;
        //        _BESOSEM.jun = txtJunSosTab0.Text;
        //        _BESOSEM.jul = txtJulSosTab0.Text;
        //        _BESOSEM.ago = txtAgoSosTab0.Text;
        //        _BESOSEM.sep = txtSepSosTab0.Text;
        //        _BESOSEM.oct = txtOctSosTab0.Text;
        //        _BESOSEM.nov = txtNovSosTab0.Text;
        //        _BESOSEM.dic = txtDicSosTab0.Text;
        //        _BESOSEM.compromisoAnual = txtComproAnualTab0.Text;
        //        _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


        //        int val = _objBLSOSEM.spud_MON_SOSEM_EDITAR(_BESOSEM);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Actualización Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            CargaFinanSOSEMTab0();
        //            ddlanioSOSEMTAb0.SelectedValue = "";
        //            txtPiaSOSEMTab0.Text = "";
        //            txtPimAcumTab0.Text = "";
        //            txtDevAcumTab0.Text = "";
        //            txtEneSosTab0.Text = "";
        //            txtFebSosTab0.Text = "";
        //            txtMarSosTab0.Text = "";
        //            txtAbrSosTab0.Text = "";
        //            txtMaySosTab0.Text = "";
        //            txtJunSosTab0.Text = "";
        //            txtJulSosTab0.Text = "";
        //            txtAgoSosTab0.Text = "";
        //            txtSepSosTab0.Text = "";
        //            txtOctSosTab0.Text = "";
        //            txtNovSosTab0.Text = "";
        //            txtDicSosTab0.Text = "";

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //        Panel_AgregarSOSEM.Visible = false;
        //        imgbtn_agregarSOSEM.Visible = true;
        //        Up_Tab0.Update();
        //    }



        //}

        //protected void grdSosemTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grdSosemTab0.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BESOSEM.id_seguimiento_sosem = Convert.ToInt32(objTemp.ToString());
        //        //_BEFinaTra.tipo = 2;
        //        _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLSOSEM.spud_MON_SOSEM_ELIMINAR(_BESOSEM);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        //            CargaFinanSOSEMTab0();
        //            Up_Tab0.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grdSosemTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id_seguimiento_sosem;
        //    id_seguimiento_sosem = grdSosemTab0.SelectedDataKey.Value.ToString();
        //    lblRegistroObras.Text = id_seguimiento_sosem.ToString();
        //    GridViewRow row = grdSosemTab0.SelectedRow;
        //    ddlanioSOSEMTAb0.SelectedValue = ((Label)row.FindControl("lblEntidad")).Text;
        //    txtPiaSOSEMTab0.Text = Server.HtmlDecode(row.Cells[3].Text);
        //    txtPimAcumTab0.Text = Server.HtmlDecode(row.Cells[4].Text);
        //    txtDevAcumTab0.Text = Server.HtmlDecode(row.Cells[5].Text);
        //    txtEneSosTab0.Text = Server.HtmlDecode(row.Cells[6].Text);
        //    txtFebSosTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
        //    txtMarSosTab0.Text = Server.HtmlDecode(row.Cells[8].Text);
        //    txtAbrSosTab0.Text = Server.HtmlDecode(row.Cells[9].Text);
        //    txtMaySosTab0.Text = Server.HtmlDecode(row.Cells[10].Text);
        //    txtJunSosTab0.Text = Server.HtmlDecode(row.Cells[11].Text);
        //    txtJulSosTab0.Text = Server.HtmlDecode(row.Cells[12].Text);
        //    txtAgoSosTab0.Text = Server.HtmlDecode(row.Cells[13].Text);
        //    txtSepSosTab0.Text = Server.HtmlDecode(row.Cells[14].Text);
        //    txtOctSosTab0.Text = Server.HtmlDecode(row.Cells[15].Text);
        //    txtNovSosTab0.Text = Server.HtmlDecode(row.Cells[16].Text);
        //    txtDicSosTab0.Text = Server.HtmlDecode(row.Cells[17].Text);
        //    txtComproAnualTab0.Text = Server.HtmlDecode(row.Cells[18].Text);
        //    lblNomUsuarioSosem.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    btnGuardarRegistroSOSEMTab0.Visible = false;
        //    btnActualizarRegistroSOSEMTab0.Visible = true;
        //    imgbtn_agregarSOSEM.Visible = false;

        //    Panel_AgregarSOSEM.Visible = true;
        //    Up_Tab0.Update();


        //}

        //protected void grdSosemTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    double total;
        //    int anio;
        //    total = 0;

        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {

        //        Label lblMonto = (Label)e.Row.FindControl("lblMontoAprob");
        //        anio = Convert.ToInt32(((Label)e.Row.FindControl("lblEntidad")).Text);


        //        if (DateTime.Now.Year == anio)
        //        {
        //            total = Convert.ToDouble(lblValorAvance.Text) + Convert.ToDouble(Server.HtmlDecode(e.Row.Cells[4].Text));

        //        }
        //        else
        //        {
        //            total = Convert.ToDouble(lblValorAvance.Text) + Convert.ToDouble(Server.HtmlDecode(e.Row.Cells[5].Text));

        //        }

        //        lblValorAvance.Text = total.ToString("N");

        //        if (Convert.ToDouble(txtTotalInversionTab0.Text) < total)
        //        {
        //            lblAlertaSosemTab0.Visible = true;
        //        }
        //        else
        //        {
        //            lblAlertaSosemTab0.Visible = false;
        //        }



        //    }

        //}

        protected void btnModificarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSeguimientoTab1())
            {


                _BEProceso.id_seguimiento = Convert.ToInt32(lblRegistroSeguimientoP.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaPubliTab1.Text);
                _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                _BEProceso.resultado = ddlResultadoTab1.SelectedValue;

                if (FileUploadSeguimientoTAb1.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSeguimientoTAb1.Text;
                }


                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);


                _BEProceso.observacion = txtObservacion.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Procesos_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaFinanTransferencia();


                    TxtConvocatoriaTab1.Text = "";
                    txtFechaPubliTab1.Text = "";
                    ddlTipoAdjudicacionTab1.SelectedValue = "";
                    ddlResultadoTab1.SelectedValue = "";
                    txtObservacion.Text = "";
                    LnkbtnSeguimientoTAb1.Text = "";
                    imgbtnSeguimientoTAb1.Visible = false;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSeguimientoTab1.Visible = false;
                btnModificarSeguimientoProcesoTab1.Visible = true;
                cargaSeguimientoProcesoTab1();
                Up_Tab1.Update();
                imgbtn_AgregarSeguimientoTab1.Visible = true;

            }



        }

        protected void LnkbtnSeguimientoTAb1_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSeguimientoTAb1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdSeguimientoProcesoTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoProcesoTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_seguimiento = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Procesos_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSeguimientoProcesoTab1();
                    Up_Tab1.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdSeguimientoProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdSeguimientoProcesoTab1.SelectedDataKey.Value.ToString();
            lblRegistroSeguimientoP.Text = id_seguimiento.ToString();
            GridViewRow row = grdSeguimientoProcesoTab1.SelectedRow;
            TxtConvocatoriaTab1.Text = ((Label)row.FindControl("lblConvocatoria")).Text;
            txtFechaPubliTab1.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoAdjudicacionTab1.Text = ((Label)row.FindControl("lblTipoAdjudicacion")).Text;
            ddlResultadoTab1.SelectedValue = ((Label)row.FindControl("lblResultado")).Text;
            txtObservacion.Text = ((Label)row.FindControl("lblobservacion")).Text;
            LnkbtnSeguimientoTAb1.Text = ((ImageButton)row.FindControl("imgDocTab1")).ToolTip;
            lblNomUsuarioSeguimientoProceso.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnSeguimientoTAb1.Text, imgbtnSeguimientoTAb1);



            cargaSeguimientoProcesoTab1();
            Up_Tab1.Update();
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            btnModificarSeguimientoProcesoTab1.Visible = true;
            btnGuardarSeguimientoProcesoTab1.Visible = false;

        }

        protected void btnModificarPlazoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPlazoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Ampliacion_Plazos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;


                _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);


                if (FileUploadPlaTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPlaTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtAmpPlazoPlaTab4.Text = "";
                    txtResolAproPlaTab4.Text = "";
                    txtFechaResolPlaTab4.Text = "";
                    txtConceptoPlaTab4.Text = "";
                    txtObservacionPlaTab4.Text = "";
                    LnkbtnPlaTab4.Text = "";
                    txtFechaInicio.Text = "";
                    txtFechaFin.Text = "";
                    imgbtnPlaTab4.Visible = false;
                    imgAgregaPlazoTab4.Visible = true;
                    lblTotalDias.Text = "0";
                    //cargaPlazoTAb4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PlazoTab4.Visible = false;
                cargaPlazoTAb4();
                ActualizaFechasTab1();
                btnModificarPlazoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void imgbtnPlaTabb4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPlaTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void grdPlazoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPlazoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    lblTotalDias.Text = "0";
                    cargaPlazoTAb4();
                    ActualizaFechasTab1();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPlazoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPlazoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Ampliacion_Plazos.Text = id_ampliacion.ToString();
            GridViewRow row = grdPlazoTab4.SelectedRow;

            txtAmpPlazoPlaTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            txtResolAproPlaTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolPlaTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoPlaTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionPlaTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            LnkbtnPlaTab4.Text = ((ImageButton)row.FindControl("imgDocPlaTab4")).ToolTip;

            GeneraIcoFile(LnkbtnPlaTab4.Text, imgbtnPlaTab4);



            btnGuardarPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            Panel_PlazoTab4.Visible = true;
            Up_Tab4.Update();


        }

        protected void btnModificarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPresupuestoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Adicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;
                _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                _BEAmpliacion.observacion = txtObsPreTab4.Text;

                if (FileUploadPreTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPreTab4.Text;
                }



                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);



                    txtMontoPreTab4.Text = "";
                    txtVincuPreTab4.Text = "";
                    txtResolAproPreTab4.Text = "";
                    txtFechaPreTab4.Text = "";
                    txtIncidenciaPreTab4.Text = "";
                    LnkbtnPreTab4.Text = "";
                    txtConceptoPreTab4.Text = "";
                    txtObsPreTab4.Text = "";
                    imgbtnPreTab4.Visible = false;
                    imbtnAgregarPresupuestoTab4.Visible = true;

                    cargaPersupuestoTab4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PresupuestoTab4.Visible = false;
                cargaPersupuestoTab4();
                btnModificarPresupuestoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void LnkbtnPreTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPreTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void ddlFaseProcesoTab01_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFaseProcesoTab01.SelectedValue == "5")
            {
                txtDocumentoPagoTab01.Visible = true;
                txtNroDocPagoTab01.Visible = true;
                txtfechaDocPagoTab01.Visible = true;
                txtBancoTab01.Visible = true;
                txtNroCuentaTab01.Visible = true;

                lblDocumentoPagoTab01.Visible = true;
                lblNroDocPagoTab01.Visible = true;
                lblfechaDocPagoTab01.Visible = true;
                lblBancoTab01.Visible = true;
                lblNroCuentaTab01.Visible = true;

            }
            else
            {
                txtDocumentoPagoTab01.Visible = false;
                txtNroDocPagoTab01.Visible = false;
                txtfechaDocPagoTab01.Visible = false;
                txtBancoTab01.Visible = false;
                txtNroCuentaTab01.Visible = false;

                lblDocumentoPagoTab01.Visible = false;
                lblNroDocPagoTab01.Visible = false;
                lblfechaDocPagoTab01.Visible = false;
                lblBancoTab01.Visible = false;
                lblNroCuentaTab01.Visible = false;

            }

            Up_CartaOrden.Update();

        }

        protected void grdPresupuestoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPresupuestoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaPersupuestoTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPresupuestoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPresupuestoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Adicional.Text = id_ampliacion.ToString();
            GridViewRow row = grdPresupuestoTab4.SelectedRow;

            txtMontoPreTab4.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtVincuPreTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproPreTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaPreTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaPreTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;
            txtConceptoPreTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsPreTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;


            LnkbtnPreTab4.Text = ((ImageButton)row.FindControl("imgDocPreTab4")).ToolTip;
            lblNomUsuarioPresupuesto.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnPreTab4.Text, imgbtnPreTab4);


            btnGuardarPresupuestoTab4.Visible = false;
            btnModificarPresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            Panel_PresupuestoTab4.Visible = true;

            Up_Tab4.Update();


        }

        protected void btnModificarDeductivoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarDeductivoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Deductivo.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                _BEAmpliacion.observacion = txtObsDedTab4.Text;

                if (FileUploadDedTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnDedTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);




                    txtMontoDedTab4.Text = "";
                    txtVincuDedTab4.Text = "";
                    txtResolAproDedTab4.Text = "";
                    txtFechaDedTab4.Text = "";
                    txtIncidenciaDedTab4.Text = "";

                    txtConceptoDedTab4.Text = "";
                    txtObsDedTab4.Text = "";
                    LnkbtnDedTab4.Text = "";
                    imgbtnDedTab4.Visible = false;
                    imgbtnDeductivoTab4.Visible = true;
                    cargaDeductivoTab4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_deductivoTab4.Visible = false;
                cargaDeductivoTab4();
                btnModificarDeductivoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }





        protected void LnkbtnDedTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnDedTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void grdDeductivoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdDeductivoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaDeductivoTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdDeductivoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdDeductivoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Deductivo.Text = id_ampliacion.ToString();
            GridViewRow row = grdDeductivoTab4.SelectedRow;

            txtMontoDedTab4.Text = Server.HtmlDecode(row.Cells[2].Text);
            txtVincuDedTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproDedTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaDedTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaDedTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;

            txtConceptoDedTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsDedTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioDeductivo.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            LnkbtnDedTab4.Text = ((ImageButton)row.FindControl("imgDocDedTab4")).ToolTip;


            GeneraIcoFile(LnkbtnDedTab4.Text, imgbtnDedTab4);

            btnGuardarDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            Panel_deductivoTab4.Visible = true;

            Up_Tab4.Update();


        }


        protected void grdAvanceFisicoTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoTab2.SelectedDataKey.Value.ToString();
            lblRegistro_Avance.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoTab2.SelectedRow;

            txtInformeTab2.Text = ((Label)row.FindControl("lblInforme")).Text;
            txtFechaTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtAvanceTab2.Text = ((Label)row.FindControl("lblAvance")).Text;
            //txtAvanceFinancieroTab2.Text = ((Label)row.FindControl("lblAvanceFinaciero")).Text;
            txtSituacionTab2.Text = ((Label)row.FindControl("lblSitucion")).Text;
            lnkbtnAvanceTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;
            GeneraIcoFile(lnkbtnAvanceTab2.Text, imgbtnAvanceTab2);


            ddlTipoMonitoreo.SelectedValue = ((Label)row.FindControl("lblId_tipomonitoreo")).Text;
            CargaSubTipoMonitoreo(ddlTipoMonitoreo.SelectedValue);
            ddlTipoSubMonitoreo.SelectedValue = ((Label)row.FindControl("lblId_TipoSubMonitoreo")).Text;

            CargaTipoEstadosDetalle();
            ddlTipoEstadoInformeTab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoEjecuccion")).Text;
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);
            ddlTipoSubEstadoInformeTab2.SelectedValue = ((Label)row.FindControl("lblId_tipoSubEstadoEjecucion")).Text;
            //ddlTipoSubEstadoInformeTab2.SelectedValue = ((Label)row.FindControl("lblId_tipoSubEstadoEjecucion")).Text;

            chbAyudaMemoriaTab2.Checked = (((CheckBox)row.FindControl("chbFlagAyuda")).Checked);
            chbAyudaMemoriaTab2.Enabled = false;

            lblNomUsuarioFlagAyudaTab2.Text = "(Actualizó: " + ((Label)row.FindControl("lblId_UsuarioFlag")).Text + " - " + ((Label)row.FindControl("lblFecha_UpdateFlag")).Text + ")";

            lblNomUsuarioAvance.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            lnkbtnActaTab2.Text = ((ImageButton)row.FindControl("imgDocActaTab2")).ToolTip;
            lnkbtnInformeTab2.Text = ((ImageButton)row.FindControl("imgDocInformeTab2")).ToolTip;
            lnkbtnOficioTab2.Text = ((ImageButton)row.FindControl("imgDocOficioTab2")).ToolTip;
            GeneraIcoFile(lnkbtnActaTab2.Text, imgbtnActaTab2);
            GeneraIcoFile(lnkbtnInformeTab2.Text, imgbtnInformeTab2);
            GeneraIcoFile(lnkbtnOficioTab2.Text, imgbtnOficioTab2);

            btnGuardarAvanceTab2.Visible = false;
            btnModificarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;
            Panel_AgregarAvanceTab2.Visible = true;

            Up_Tab2.Update();


        }

        protected void grdAvanceFisicoTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_AvanceFisico_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();

                    CargaDatosTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        public bool ValidaDecimal(string inValue)
        {
            bool bValid;
            try
            {
                decimal myDT = Convert.ToDecimal(inValue);
                bValid = true;

            }
            catch (Exception e)
            {
                bValid = false;
            }


            return bValid;
        }

        protected bool ValidarAvanceTab2()
        {
            string result = "";

            if (txtFechaTab2.Text == "")
            {
                result = result + "*Ingrese fecha.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha.\\n";
            }
            else if (Convert.ToDateTime(txtFechaTab2.Text) > DateTime.Today)
            {
                result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

            }

            if (ddlTipoMonitoreo.SelectedValue == "")
            {
                result = result + "*Ingrese tipo de monitoreo.\\n";
            }

            if (ddlTipoSubMonitoreo.SelectedValue.Equals("") && ddlTipoSubMonitoreo.Visible == true)
            {
                result = result + "*Seleccionar Sub tipo de monitoreo.\\n";
            }



            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals(""))
            {
                result = result + "*Seleccionar estado.\\n";
            }
            else if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") && ddlTipoSubEstadoInformeTab2.Visible == true)
            {
                result = result + "*Seleccionar Sub Estado.\\n";
            }


            if (txtAvanceTab2.Text == "")
            {
                result = result + "*Ingresar avance físico .\\n";
            }
            else if (ValidaDecimal(txtAvanceTab2.Text) == false)
            {
                result = result + "*Ingrese avance fisico valido, por ejemplo: 92.2 .\\n";
            }
            else if (Convert.ToDecimal(txtAvanceTab2.Text) > 100)
            {
                result = result + "*Ingrese avance fisico válido. No debe ser mayor a 100%.\\n";
            }


            //if (txtAvanceFinancieroTab2.Text == "")
            //{
            //    result = result + "*Ingresar avance financiero .\\n";
            //}
            //else if (ValidaDecimal(txtAvanceFinancieroTab2.Text) == false)
            //{
            //    result = result + "*Ingrese avance financiero valido, por ejemplo: 92.2 .\\n";
            //}
            //else if (Convert.ToDecimal(txtAvanceFinancieroTab2.Text) > 100)
            //{
            //    result = result + "*Ingrese avance financiero válido. No debe ser mayor a 100%.\\n";
            //}

          
          

            // Concluido - En Elaboración
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("22") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("35"))
            {
                if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlTipoEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.\\n";
                }
            }


            // Concluido
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("22"))
            {
                if (txtFechaTermino.Text.Length == 0)
                {
                    result = result + "*Para registrar el estado Concluido debe registrar primero la fecha de termino real.\\n";
                }

            }

            if (lblIdEstado.Text.Trim() != "")
            {
                if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue), 2)))
                {
                    result = result + "*No puede volver el proyecto a Actos Previos.\\n";

                }
            }

            if (!validaArchivo(FileUploadAvanceTab2))
            { return false; }

            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                return false;
            }
            else
            {
                return true;
            }


        }

        protected void btnModificarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUpload_ActaTab2))
                {

                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblRegistro_Avance.Text);
                    //_BEFinanciamiento.tipoFinanciamiento = 3;
                    _BEEjecucion.informe = txtInformeTab2.Text;
                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtFechaTab2.Text);
                    _BEEjecucion.avance = txtAvanceTab2.Text;
                    _BEEjecucion.financieroReal = txtAvanceFinancieroTab2.Text.Equals("") ? "0" : txtAvanceFinancieroTab2.Text;
                    _BEEjecucion.situacion = txtSituacionTab2.Text;

                    //if (FileUploadAvanceTab2.PostedFile.ContentLength > 0)
                    //{
                    //    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    //}
                    //else
                    //{
                    //    _BEEjecucion.urlDoc = lnkbtnAvanceTab2.Text;
                    //}

                    if (chbAyudaMemoriaTab2.Checked == true)
                    {
                        _BEEjecucion.flagAyuda = "1";
                    }
                    else
                    {
                        _BEEjecucion.flagAyuda = "0";
                    }

                    _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                    _BEEjecucion.id_tipoSubMonitoreo = ddlTipoSubMonitoreo.SelectedValue.Equals("") ? "0" : ddlTipoSubMonitoreo.SelectedValue;
                    _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue);
                    _BEEjecucion.id_tipoSubEstadoEjecucion = ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") ? "0" : ddlTipoSubEstadoInformeTab2.SelectedValue;

                    _BEEjecucion.UrlActa = FileUpload_ActaTab2.HasFile ? _Metodo.uploadfile(FileUpload_ActaTab2) : lnkbtnActaTab2.Text;
                    _BEEjecucion.UrlInforme = FileUpload_InformeTab2.HasFile ? _Metodo.uploadfile(FileUpload_InformeTab2) : lnkbtnInformeTab2.Text;
                    _BEEjecucion.UrlOficio = FileUpload_OficioTab2.HasFile ? _Metodo.uploadfile(FileUpload_OficioTab2) : lnkbtnOficioTab2.Text;

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int resul;
                    resul = _objBLEjecucion.spud_MON_AvanceFisico_Estudio_Editar(_BEEjecucion);
                    if (resul == 1)
                    {
                        string script = "<script>alert('Registro Correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        txtInformeTab2.Text = "";
                        txtFechaTab2.Text = "";
                        txtAvanceTab2.Text = "";
                        txtSituacionTab2.Text = "";

                        imgbtnAvanceFisicoTab2.Visible = true;
                        CargaAvanceFisico();
                        CargaDatosTab2();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                    Panel_AgregarAvanceTab2.Visible = false;
                    CargaAvanceFisico();
                    btnModificarAvanceTab2.Visible = true;
                    Up_Tab2.Update();

                }
            }
        }
        protected void grdConsultoresTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdConsultoresTab1.SelectedDataKey.Value.ToString();
            lblRegistroContratista.Text = id_seguimiento.ToString();
            GridViewRow row = grdConsultoresTab1.SelectedRow;

            txtContratistaGrupTab1.Text = ((Label)row.FindControl("lblNombConsultor")).Text;
            txtRucGrupTab1.Text = ((Label)row.FindControl("lblRuc")).Text;
            txtRepresentanGrupTAb1.Text = ((Label)row.FindControl("lblRepresentante")).Text;

            cargaConsultoresTab1();
            Up_Tab1.Update();
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = false;
            btn_modificarConsultor.Visible = true;

        }

        protected void btn_modificarConsultor_OnClick(object sender, EventArgs e)
        {

            if (ValidarGrupoConsorcioTab1())
            {

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(lblRegistroContratista.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                _BEProceso.rucContratista = txtRucGrupTab1.Text;
                _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Consorcio_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaFinanTransferencia();

                    txtContratistaGrupTab1.Text = "";
                    txtRucGrupTab1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarConsultorTab1.Visible = false;
                cargaConsultoresTab1();
                btn_modificarConsultor.Visible = true;
                imgbtn_AgregarConsultorTab1.Visible = true;
                Up_Tab1.Update();


            }
        }


        protected void grdConsultoresTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdConsultoresTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Consorcio_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaConsultoresTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }



        #endregion

   

        protected void imgbtnLiqSupervisionTab3_Click(object sender, ImageClickEventArgs e)
        {

        }
        protected void btnDetalleDevengadoTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMDevengadoMensualizadoTab0(Convert.ToInt32(id));
            UpdatePanel3.Update();
            MPE_DetalleSOSEMTab0.Show();
        }
        protected void btnFuenteTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMFuenteFinanciamientoTab0(Convert.ToInt32(id));
            Up_FuenteFinanciamientoTAb0.Update();
            MPE_FuenteFinanciamientoTab0.Show();
        }

        protected void imgDocAvanceTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocAvanceTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnAvanceTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = lnkbtnAvanceTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdAvanceFisicoTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);

                ImageButton imbActa = (ImageButton)e.Row.FindControl("imgDocActaTab2");
                GeneraIcoFile(imbActa.ToolTip, imbActa);

                ImageButton imbInforme = (ImageButton)e.Row.FindControl("imgDocInformeTab2");
                GeneraIcoFile(imbInforme.ToolTip, imbInforme);

                ImageButton imbOficio = (ImageButton)e.Row.FindControl("imgDocOficioTab2");
                GeneraIcoFile(imbOficio.ToolTip, imbOficio);

                CheckBox chbFlagAyuda = (CheckBox)e.Row.FindControl("chbFlagAyuda");

                if (lblID_ACCESO.Text == "0")
                {
                    chbFlagAyuda.Enabled = false;

                }

                if (chbFlagAyuda.Text == "1")
                {
                    chbFlagAyuda.Checked = true;
                    chbFlagAyuda.Text = "";
                }
                else
                {
                    chbFlagAyuda.Checked = false;
                    chbFlagAyuda.Text = "";
                }
            }
        }

        protected void chbFlagAyuda_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var valor = grdAvanceFisicoTab2.DataKeys[row.RowIndex].Value;
            //_BEEjecucion.id_tabla = Convert.ToInt32(valor.ToString());
            string FlagAyudaLectura = ((Label)row.FindControl("lblAyudaObtiene")).Text;
            Boolean FlagAyuda = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;
            Boolean result;
            if (FlagAyudaLectura == "1")
            {
                result = true;
            }
            else
            {
                result = false;
            }

            if (FlagAyuda == result)
            {
            }
            else
            {
                _BEEjecucion.id_avanceFisico = Convert.ToInt32(valor.ToString());
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                if (FlagAyuda == true)
                {
                    _BEEjecucion.flagAyuda = "1";
                }
                else
                {
                    _BEEjecucion.flagAyuda = "0";
                }

                int val;
                try
                {
                    val = _objBLEjecucion.spu_MON_AvanceFisicoByFlagAyudaMemoria(_BEEjecucion);
                }
                catch (Exception ex)
                {
                    val = 0;
                }

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el flag de Ayuda Memoria correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_AgregarAvanceTab2.Visible = false;
                    imgbtnAvanceFisicoTab2.Visible = true;
                    CargaAvanceFisico();

                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        

        protected void imgseaceConsultoria_Click(object sender, ImageClickEventArgs e)
        {

        }

   
        protected void TabContainerDetalles_ActiveTabChanged(object sender, EventArgs e)
        {
            ViewState["ActiveTabIndex"] = TabContainerDetalles.ActiveTabIndex;

            //if (TabContainerDetalles.ActiveTabIndex == 1)
            //{
            //    TabActivoProceso();
            //}
        }

        protected void lnkbtnAprobacionExpedienteTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnAprobacionExpedienteTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnAprobacionExpedienteTab3.Text);
                Response.End();
            }
        }

        protected void lnkbtnActaTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string NameArchivo = lnkbtnActaTab2.Text;
            FileInfo toDownload = new FileInfo(pat + NameArchivo);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + NameArchivo);
                Response.End();
            }
        }

        protected void lnkbtnInformeTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string NameArchivo = lnkbtnInformeTab2.Text;
            FileInfo toDownload = new FileInfo(pat + NameArchivo);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + NameArchivo);
                Response.End();
            }
        }

        protected void lnkbtnOficioTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string NameArchivo = lnkbtnOficioTab2.Text;
            FileInfo toDownload = new FileInfo(pat + NameArchivo);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + NameArchivo);
                Response.End();
            }
        }


        protected void imgDocActaTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocActaTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocInformeTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocInformeTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocOficioTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocOficioTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void ddlTipoMonitoreo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaSubTipoMonitoreo(ddlTipoMonitoreo.SelectedValue);

            Up_SubMonitoreoNombreTab2.Update();
            Up_SubMonitoreoDetalleTab2.Update();
        }

        protected void CargaSubTipoMonitoreo(string vTipoMonitoreo)
        {
            ddlTipoSubMonitoreo.ClearSelection();

            if (!vTipoMonitoreo.Equals(""))
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                list = _objBLEjecucion.F_spMON_ListarSubTipoMonitoreo(Convert.ToInt32(vTipoMonitoreo));

                ddlTipoSubMonitoreo.DataSource = list;
                ddlTipoSubMonitoreo.DataTextField = "nombre";
                ddlTipoSubMonitoreo.DataValueField = "valor";
                ddlTipoSubMonitoreo.DataBind();

                if (list.Count == 0)
                {
                    lblTipoSubNombre.Visible = false;
                    ddlTipoSubMonitoreo.Visible = false;
                }
                else
                {
                    lblTipoSubNombre.Visible = true;
                    ddlTipoSubMonitoreo.Visible = true;
                }
            }

            ddlTipoSubMonitoreo.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        //protected void btnVerRegistroProcesoSeleccion_Click(object sender, EventArgs e)
        //{
        //    Up_Tab1_SEACE.Visible = false;
        //    Up_Tab1.Visible = true;
        //    //TabPanelProcesoSeleccionSEACE.Enabled = false;
        //    //TabPanelProcesoSeleccion.Enabled = true;

        //    CargaddlTipoAdjudicacionTab1(ddlAdjudicacionTab1);
        //    CargaddlTipoAdjudicacionTab1(ddlTipoAdjudicacionTab1);
        //    CargaDllTipoResultado();
        //    cargaSeguimientoProcesoTab1();
        //    CargaDatosTab1();
        //    cargaConsultoresTab1();

        //    btnVerRegistroSEACE.Visible = true;

        //    UPTabContainerDetalles.Update();
        //}

        protected void imgbtnEditarUE_Click(object sender, ImageClickEventArgs e)
        {
            string script = "<script>$('#modalEditarUE').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void btnModificarUE_Click(object sender, EventArgs e)
        {
            ObjBEProyecto.id = Convert.ToInt32(LblID_PROYECTO.Text);
            if (LblID_SOLICITUD.Text == "")
            {
                ObjBEProyecto.Id_Solicitudes = 0;
            }
            else
            {
                ObjBEProyecto.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
            }
            ObjBEProyecto.Entidad_ejec = txtUnidadEjecutora.Text;

            int val = objBLProyecto.U_MON_UnidadEjecutora(ObjBEProyecto);

            if (val == 1)
            {
                string script = "<script>$('#modalEditarUE').modal('hide');alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                }
                UPTabContainerDetalles.Update();



            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void grdUbicacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdUbicacion.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.flagActivo = 0;
                _BEBandeja.CCPP = ((Label)CurrentRow.FindControl("lblUbigeo")).Text + ((Label)CurrentRow.FindControl("lblUbigeoCCPP")).Text;
                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarUbicacion_Click(object sender, ImageClickEventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = true;
            //btnModificarUbicacionMetasTab0.Visible = false;
            Up_UbigeoTab0.Update();
        }

        protected Boolean ValidarUbigeoTab0()
        {
            Boolean result;
            result = true;

            if (ddlDepartamentoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Departamento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlProvinciaTab0.SelectedValue == "")
            {

                string script = "<script>alert('Ingrese Provincia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;


            }

            if (ddlDistritoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Distrito.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;
        }

        protected void btnGuardarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            if (ValidarUbigeoTab0())
            {
                _BEBandeja.flagActivo = 1;
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                if (ddlCCPPTab0.SelectedValue.Equals(""))
                {
                    _BEBandeja.CCPP = ddlDepartamentoTab0.SelectedValue + ddlProvinciaTab0.SelectedValue + ddlDistritoTab0.SelectedValue;
                }
                else
                {
                    _BEBandeja.CCPP = ddlCCPPTab0.SelectedValue;
                }

                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }


        protected void btnCancelarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = false;
        }

        protected void ddlDepartamentoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlProvinciaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlDistritoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = _objBLBandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("- SELECCIONAR -", ""));

        }

        protected void CargaUbigeosProyectos(int idProyecto)
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = _objBLBandeja.F_spMON_Ubigeo(idProyecto);
            grdUbicacion.DataSource = ListUbigeos;
            grdUbicacion.DataBind();



        }

        protected void imgbtnEditarUbigeo_Click(object sender, ImageClickEventArgs e)
        {
            CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));

            cargaUbigeo(ddlDepartamentoTab0, "1", null, null, null);
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);

            Up_UbigeoTab0.Update();
            //string script = "<script>ShowPopupRegistroInformeVisita();</script>";
            string script = "<script>$('#modalUbigeo').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void BtnRefrescar_Click(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);
            DataTable dt = (new BLProyecto().spu_MON_UltimaActualizacion(idProy));
            if (dt.Rows.Count > 0)
            {
                LblUltimaModificacion.Text = Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortTimeString();
                LblUsuario.Text = dt.Rows[0]["Usuario"].ToString();
            }
            else
            {
                LblUltimaModificacion.Text = "";
                LblUsuario.Text = "";
            }

        }

        protected void btnFinalizarTab3_Click(object sender, EventArgs e)
        {
            if (lblIdEstadoRegistro.Text.Equals("2")) //ACTIVO
            {
                _BEEjecucion.id_estadoProyecto = 3;
            }
            else
            {
                _BEEjecucion.id_estadoProyecto = 2;
            }
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_EstadoProyecto(_BEEjecucion);

            if (val == 1)
            {
                //CargaLiquidacion();

                string script = "<script>alert('Se Actualizó Correctamente'); window.location.reload()</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void lnkbtnBuscarRUCContratistaTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsultorTab1, txtNombConsultorTab1, txtRepresentanteLegalConsultorTab1, divMsjBusquedaRucContratistaTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }


        protected void lnkbtnBuscarRUCConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsorcioTab1, txtNomconsorcioTab1, txtRepresentanteTab1, divMsjBusquedaRucConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void lnkbtnBuscarRUCMiembroConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucGrupTab1, txtContratistaGrupTab1, txtRepresentanGrupTAb1, divMsjBusquedaRucMiembroConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void btnVigenciaVencida_Click(object sender, EventArgs e)
        {
            string script = "<script>$('#modalVigencia').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            dt = _objBLFinanciamiento.spMON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text));

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["flagActivoReporte"].ToString() == "1")
                {
                    btnRegistrarVencimiento.Enabled = true;
                }

                if (dt.Rows[0]["fechaUpdateVencimiento"].ToString().Length > 0)
                {
                    ddlFlagVigencia.SelectedValue = dt.Rows[0]["flagVigenciaVencida"].ToString();
                    ddlTipoAccionVigencia.SelectedValue = dt.Rows[0]["idTipoAccionVencimiento"].ToString();

                    if (ddlFlagVigencia.SelectedValue.Equals("1"))
                    {
                        divAccionVigencia.Visible = true;
                    }
                    else
                    {
                        divAccionVigencia.Visible = false;
                        ddlTipoAccionVigencia.SelectedValue = "";
                    }

                    if (ddlTipoAccionVigencia.SelectedValue == "")
                    {
                        divAccionVigencia.Visible = false;
                    }

                    lblNomActualizaVencimiento.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdateVencimiento"].ToString();
                }
            }

            Up_PanelVigencia.Update();
        }

        protected String ValidarVigenciaVencia()
        {
            string msj = "";


            if (ddlFlagVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar etapa con vigencia vencida.\\n";
            }

            if (ddlFlagVigencia.SelectedValue.Equals("1") && ddlTipoAccionVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de acción.\\n";
            }


            return msj;

        }
        protected void btnRegistrarVencimiento_Click(object sender, EventArgs e)
        {
            string msjVal = ValidarVigenciaVencia();

            if (msjVal.Length > 0)
            {
                string script = "<script>alert('" + msjVal + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                int pTipoAccion = 0;
                if (ddlTipoAccionVigencia.SelectedValue != "")
                {
                    pTipoAccion = Convert.ToInt32(ddlTipoAccionVigencia.SelectedValue);
                }

                DataSet ds = new DataSet();
                ds = _objBLFinanciamiento.paSSP_spiu_MON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(LblID_USUARIO.Text), Convert.ToInt32(ddlFlagVigencia.SelectedValue), pTipoAccion);

                if (ds.Tables[0].Rows[0]["vCod"].ToString() == "200")
                {
                    string script = "<script>$('#modalVigencia').modal('hide');alert('Se registró la vigencia de vencimiento. Se actualizará la página');window.location.reload()</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Hubo un error, volver a intentar.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void ddlFlagVigencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFlagVigencia.SelectedValue.Equals("1"))
            {
                divAccionVigencia.Visible = true;
            }
            else
            {
                divAccionVigencia.Visible = false;
                ddlTipoAccionVigencia.SelectedValue = "";
            }

            Up_PanelVigencia.Update();
        }

        protected void TabActivoProceso()
        {
            if (grSeguimientoSeace.Rows.Count > 0)
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }
            else if (grdSeguimientoProcesoTab1.Rows.Count > 0 || txtValorReferencialTab1.Text.Length > 0)
            {
                PanelSeaceAuto.Visible = false;
                PanelSeaceManual.Visible = true;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";
            }
            else
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }

            //Por ahora se desactiva todo 2020.11.13
            lnkbtnSeaceAutoPanel.Visible = false;
            lnkbtnSeaceManualPanel.Visible = false;
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
        }

        protected void grdSEACEObra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSEACEObra.PageIndex = e.NewPageIndex;
            CargaProcesosRelSEACE();
        }

        protected void CargaProcesosRelSEACE()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSEACEObra.DataSource = _objBLFinanciamiento.spMON_SEACE_Obra(snip, Convert.ToInt32(LblID_PROYECTO.Text));
            grdSEACEObra.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void CargaCodigoSeace()
        {
            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            grSeguimientoSeace.DataSource = _objBLFinanciamiento.spMON_CodigoSeace(pIdProyecto, Convert.ToInt32(ddlProcesoTab1.SelectedValue));
            grSeguimientoSeace.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void BtnRegistrarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            if (TxtCodigoSeace.Text.Length == 0)
            {
                string script = "<script>alert('Inresar código de SEACE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script3", script, false);
            }
            else
            {
                CodigosSeace _BE = new CodigosSeace();
                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(TxtCodigoSeace.Text);
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se registro correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnCancelarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            Panel_RegistroSeace.Visible = false;
            imgbtnAgregarSeguimientoSeace.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtnAgregarSeguimientoSeace_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RegistroSeace.Visible = true;
            imgbtnAgregarSeguimientoSeace.Visible = false;
            Up_Tab1.Update();
        }

        protected void grSeguimientoSeace_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grSeguimientoSeace.DataKeys[CurrentRow.RowIndex].Value as object;

                CodigosSeace _BE = new CodigosSeace();

                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(objTemp.ToString());
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grSeguimientoSeace_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblEstadoItem = (Label)e.Row.FindControl("lblEstadoItem");
                Label lblEstado = (Label)e.Row.FindControl("lblEstado");
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");

                string caseSwitch = lblEstadoItem.Text;

                lblEstado.Text = lblEstadoItem.Text;
                lblEstado.ToolTip = lblEstadoItem.Text;

                switch (caseSwitch)
                {
                    case "APELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO POR RECORTE PRESUPUESTAL":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "DESIERTO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUBSCRIPCION DEL CONTRATO POR DECISION DE LA ENTIDAD":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUSCRIBIO EL CONTRATO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NULO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "SUSPENDIDO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "ADJUDICADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "ADJUDICADO A PRORRATA":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONSENTIDO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONTRATADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    default:
                        lblEstado.CssClass = "label label-primary";
                        break;
                }

                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");

                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;

                }
            }
        }

        protected void grdSEACEObra_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");
                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");
                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;
                }
            }
        }
        protected void lnkbtnSeaceAutoPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = true;
            PanelSeaceManual.Visible = false;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";

            Up_Tab1.Update();
        }

        protected void lnkbtnSeaceManualPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";

            Up_Tab1.Update();
        }
    }

}