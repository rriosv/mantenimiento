﻿using OfficeOpenXml;
using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class PersonalNE_CargaMasiva : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            subirArchivos(FileUpload1);
        }

        private void subirArchivos(FileUpload file)
        {
            //divError.Visible = false;
            divErrorCarga.Visible = false;
            try
            {
                string rutaGuardar = Server.MapPath(ConfigurationManager.AppSettings["RutaCargaExcelTMP"]);
                if (file.HasFile == true)
                {
                    var fileExcel = rutaGuardar + file.FileName;
                    file.SaveAs(fileExcel);

                    var dataExcel = ExcelToTecnicoPersonalNE(fileExcel);
                    if (dataExcel == null || dataExcel.Count == 0)
                    {
                        MostrarMensajeClient("El excel no contiene registros para procesar");
                        return;
                    }

                    //var group = (from a in dataExcel group a by a.DNI into result select result).ToList();

                    //if (group.Count != dataExcel.Count)
                    //{
                    //    MostrarMensajeClient("El excel contiene dni duplicados");
                    //    divErrorCarga.Visible = true;
                    //    grillaErrorCarga.DataSource = dataExcel;
                    //    grillaErrorCarga.DataBind();
                    //    return;
                    //}                                             

                    var lista_error = dataExcel.FindAll(c => c.Detalle_Error != null);
                    if (lista_error != null && lista_error.Count > 0)
                    {
                        MostrarMensajeClient("El excel contiene " + lista_error.Count + " registros que son requeridos, completar y volver a cargar el archivo!");
                        divErrorCarga.Visible = true;
                        grillaErrorCarga.DataSource = lista_error;
                        grillaErrorCarga.DataBind();
                        return;
                    }
                    else
                    {
                        int id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                        var resultado = new BL_MON_Ejecucion().ValidarTecnicoPersonalNE(dataExcel, id_usuario);
                        if (resultado != null)
                        {
                            var listaErrorBD = resultado.ToList();

                            //valida si los registros ingresados son los correcto y/o si DNI ya existe en la BD
                            if (listaErrorBD.Count > 0)
                            {
                                //mostrar en grilla
                                divErrorCarga.Visible = true;
                                grillaErrorCarga.DataSource = listaErrorBD;
                                grillaErrorCarga.DataBind();

                                if (dataExcel.Count == listaErrorBD.Count)
                                {
                                    MostrarMensajeClient(listaErrorBD.Count + " registros no se cargaron. Verifique su archivo excel");
                                }
                                else
                                {
                                    MostrarMensajeClient("De " + dataExcel.Count + " registros, sólo se cargaron " + (dataExcel.Count - listaErrorBD.Count) + ". Verifique el detalle del error y su archivo de excel");
                                }

                            }
                            else
                            {
                                MostrarMensajeClient("Se cargaron " + dataExcel.Count + " registros correctamente");
                            }

                        }
                        else
                        {
                            MostrarMensajeClient("Se cargó correctamente todo los " + dataExcel.Count + " registros");

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //mosrrar mmenaje error
                Console.Write(ex.Message);

            }
        }


        public List<BE_MON_Ejecucion> ExcelToTecnicoPersonalNE(string fileName)
        {
            //object dataList = null;
            List<BE_MON_Ejecucion> dataList = new List<BE_MON_Ejecucion>();

            if (File.Exists(fileName))
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    using (var stream = File.OpenRead(fileName))
                    {
                        package.Load(stream);
                    }
                    ExcelWorksheet ws = package.Workbook.Worksheets.First();
                    //dataList = WorksheetToDataTable(ws, true);
                    dataList = WorksheetToTecnicoPersonalNE(ws, true);
                }
            }
            return dataList;
        }

        private List<BE_MON_Ejecucion> WorksheetToTecnicoPersonalNE(ExcelWorksheet ws, bool hasHeader = true)
        {

            //Instanciar lista a retornar
            List<BE_MON_Ejecucion> lista = new List<BE_MON_Ejecucion>();
            //
            DataTable dt = new DataTable(ws.Name);
            int totalCols = ws.Dimension.End.Column;
            int totalRows = ws.Dimension.End.Row;
            int startRow = hasHeader ? 2 : 1;
            ExcelRange wsRow;
            DataRow dr;
            foreach (var firstRowCell in ws.Cells[1, 1, 1, totalCols])
            {
                dt.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            }

            for (int rowNum = startRow; rowNum <= totalRows; rowNum++)
            {
                wsRow = ws.Cells[rowNum, 1, rowNum, totalCols];
                dr = dt.NewRow();
                foreach (var cell in wsRow)
                {
                    dr[cell.Start.Column - 1] = cell.Text;
                }

                dt.Rows.Add(dr);
                var item = new BE_MON_Ejecucion();

                if (!string.IsNullOrWhiteSpace(dr.ItemArray[0].ToString()))
                    item.nombre = Convert.ToString(dr.ItemArray[0]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Nombre es requerido");

                //if (!string.IsNullOrWhiteSpace(dr.ItemArray[1].ToString()))
                //{
                //    ////item.activo = Convert.ToBoolean(dr.ItemArray[1]);
                //    //item.activoS = Convert.ToString(dr.ItemArray[1]);

                //    if (dr.ItemArray[1].ToString() == "si")
                //    {
                //        item.activo = true;
                //    }
                //    else
                //    {
                //        if (dr.ItemArray[1].ToString() == "no")
                //        {
                //            item.activo = false;

                //        }
                //        else
                //        {
                //            item.Detalle_Error = ValidarCampo(item.Detalle_Error, "En Activo, no es un valor válido");
                //        }

                //    }
                //}
                //else
                //    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Activo es requerido");

                if (!string.IsNullOrWhiteSpace(dr.ItemArray[1].ToString()))
                    item.tipoPrograma = Convert.ToString(dr.ItemArray[1]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Tipo Programa es requerido");

                if (!string.IsNullOrWhiteSpace(dr.ItemArray[2].ToString()))
                    item.correo = Convert.ToString(dr.ItemArray[2]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Correo es requerido");

                if (!string.IsNullOrWhiteSpace(dr.ItemArray[3].ToString()))
                    item.tipoAgente = Convert.ToString(dr.ItemArray[3]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Tipo Agente es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[4].ToString()))
                    item.DNI = Convert.ToString(dr.ItemArray[4]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "DNI es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[5].ToString()))
                    item.tipoProfesion = Convert.ToString(dr.ItemArray[5]);

                if (!string.IsNullOrEmpty(dr.ItemArray[6].ToString()))
                    item.nroColegiatura = Convert.ToString(dr.ItemArray[6]);

                lista.Add(item);
            }

            return lista;
        }

        private string ValidarCampo(string Detalle_Error, string mensaje)
        {
            return Detalle_Error == null ? mensaje : Detalle_Error + " | " + mensaje;
        }

        private void MostrarMensajeClient(string mensaje)
        {
            string script = "<script>alert('" + mensaje + "');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }
    }
}