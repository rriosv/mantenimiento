﻿<%@ Page Title="Asignación de Monitoreo" Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="Asignacion.aspx.cs" Inherits="Web.Monitor.Asignacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input[type="checkbox"] {
            height: 20px;
            width: 1.6em;
        }

        #ContentPlaceHolder1_TxtSnipFiltro + .tag-editor .tag-editor-delete {
            height: 25px;
        }

        #ContentPlaceHolder1_TxtSnipFiltro + .tag-editor {
            line-height: 25px;
            border-color: rgb(169, 169, 169)
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" lang="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.gif" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; font-weight: bold; font-size: 12px">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

    <div class="Grid">

        <br />
        <div style="text-align: center">
            <p>
                <span class="titlePage">ASIGNACIÓN DE PROYECTO</span>
            </p>
        </div>
        <br />

        <asp:UpdatePanel runat="server" ID="Up_Filtro" UpdateMode="Conditional">
            <ContentTemplate>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Criterios de Búsqueda</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i>
                                </button>--%>
                        </div>
                    </div>
                    <div class="card-body p-0" style="display: block;">
                        <br />
                        <div class="text-center">

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <asp:RadioButtonList ID="RbFiltro" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="RbFiltro_SelectedIndexChanged" AutoPostBack="true" CssClass="formHeader">
                                            <asp:ListItem Text="SNIP O CUI" Value="COD" Selected="True" />
                                            <asp:ListItem Text="Proyecto" Value="PRY" />
                                        </asp:RadioButtonList>
                                         <div style="display: inline-block; width:100%" >
                                            <asp:TextBox ID="TxtNombreProyecto" runat="server" placeholder="Ingrese nombre del proyecto" Visible="false" CssClass="form-control"></asp:TextBox>
                                            <textarea id="TxtSnipFiltro" runat="server" data-role="tagsinput" rows="1" placeholder="Para buscar varios snip o CUI, separelos por coma" CssClass="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                          <label class="formHeader">DISTRITO</label>
                                          <asp:DropDownList ID="ddldist" runat="server" CssClass="custom-select">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                         <label class="formHeader">TIPO</label>
                                           <asp:DropDownList ID="ddlTipo" runat="server" CssClass="custom-select">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="formHeader">UNIDAD</label>
                                        <asp:DropDownList ID="ddlSubsector" runat="server" AutoPostBack="true" CssClass="custom-select" OnSelectedIndexChanged="ddlSubsector_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="formHeader">RESPONSABLE</label>
                                          <asp:DropDownList ID="ddlTecnico" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <table runat="server" visible="false">
                                <tr>
                                    <td align="center" runat="server" visible="false">
                                        <b>AÑO</b>
                                    </td>
                                    <td align="center">

                                        
                                    </td>
                                    <td align="center" runat="server" visible="false">
                                        <b>DEPARTAMENTO</b>
                                    </td>
                                    <td align="center" runat="server" visible="false">
                                        <b>PROVINCIA</b>
                                    </td>
                                    <td align="center" class="formHeader">DISTRITO
                                    </td>
                                    <td align="center" class="formHeader">UNIDAD
                                    </td>
                                    <td align="center" runat="server" visible="false">
                                        <b>SUB UNIDAD</b></td>
                                    <td align="center" class="formHeader">TIPO</td>
                                    <td align="center" class="formHeader"><b>TÉCNICO</b></td>
                                </tr>
                                <tr>
                                    <td align="center" runat="server" visible="false">
                                        <asp:TextBox ID="txtanno" runat="server" MaxLength="4" Width="54px" placeholder="YYYY" onkeypress="return esInteger(event)"></asp:TextBox>
                                    </td>
                                    <td align="center">
                                       
                                    </td>

                                    <td align="center" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlDepa" Width="150" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepa_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlprov" runat="server" Width="150" AutoPostBack="true" OnSelectedIndexChanged="ddlprov_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center">
                                      
                                    </td>
                                    <td align="center">
                                        
                                    </td>
                                    <td align="center" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlSubPrograma" runat="server" Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center">
                                     
                                    </td>
                                   <td align="center">
                                      
                                    </td>
                                </tr>
                                <tr runat="server" visible="false">
                                    
                                    <td align="center" runat="server" visible="false"><b>MODALIDAD FINANCIAMIENTO</b>&nbsp;</td>
                                </tr>
                                <tr runat="server" visible="false">
                                    
                                    <td align="center" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlModalidadFinanciamiento" runat="server" Width="150px">
                                            <asp:ListItem Text="-Seleccione-" Value=""></asp:ListItem>
                                            <asp:ListItem Text="TRANSFERENCIA" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="POR CONTRATA" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="NÚCLEO EJECUTOR" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="COOPERACIÓN INTERNACIONAL" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>

                            <div class="row">
                                <div class="col-sm-1 offset-sm-5">
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnBuscar" runat="server" OnClick="btnBuscar_OnClick" CssClass="btn btn-primary">
                            <i class="fa fa-filter"></i> BUSCAR
                                        </asp:LinkButton>

                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnLimpiar" runat="server" OnClick="btnLimpiar_OnClick" CssClass="btn btn-default">
                            <i class="fa fa-eraser" id="BtnLimpiar"></i> LIMPIAR
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>

                <br />

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div style="margin: auto">
        <asp:UpdatePanel ID="Up_grdNoAsignado" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="text-align: center">
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="SIN ASIGNAR" CssClass="titulo2" Style="font-size: 13pt;"></asp:Label>
                    </p>
                </div>

                <img src="linea.png" width="98%" height="13px" alt="" />
                <br />

                <table style="width: 95%; margin: auto">

                    <tr>
                        <td align="center">
                            <asp:GridView
                                ID="gvPryNoAsignado"
                                runat="server"
                                Width="100%"
                                AllowPaging="True"
                                DataKeyNames="id_proyecto"
                                AutoGenerateColumns="False"
                                BorderWidth="1px"
                                CssClass="Grid"
                                EmptyDataText="No se han registrado proyectos."
                                EditRowStyle-BackColor="#ffffb7"
                                OnPageIndexChanging="gvPryNoAsignado_PageIndexChanging"
                                OnDataBound="gvPryNoAsignado_DataBound"
                                PageSize="200">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkbNoAsignadosAll" onclick="return check_uncheck(this,'chkbNoAsignadosAll','chkbNoAsignados');" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="id_pryNoasignados" runat="server" Visible="False" Text='<%# DataBinder.Eval (Container.DataItem, "id_proyecto") %>' />
                                            <asp:CheckBox ID="chkbNoAsignados" onclick="return check_uncheck(this);" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIDProyectos" Text='<%# Eval("id_proyecto") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAnio" Text='<%# Eval("anio") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Unidad">
                                        <ItemTemplate>
                                            <asp:Label ID="des_subsector" Text='<%# Eval("subsector") %>' runat="Server" />
                                            <asp:Label ID="id_sector" runat="server" Text='<%# (Eval("subPrograma") != DBNull.Value ? " - " + Eval("subPrograma") : "") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MODALIDAD DE FINANCIAMIENTO" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="ModalidadFinanciamiento" Text='<%# Eval("ModalidadFinanciamiento") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SNIP">
                                        <ItemTemplate>
                                            <asp:Label ID="cod_snip" Text='<%# Convert.ToInt32(Eval("cod_snip"))==0 ? "NO": Eval("cod_snip") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CUI">
                                        <ItemTemplate>
                                            <asp:Label ID="iCodUnificado" Text='<%# Eval("iCodUnificado") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Departamento" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="nom_depa" Text='<%# Eval("nom_depa") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Provincia" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblprov" runat="server" Text='<%# Eval("nom_prov") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Distrito">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldist" runat="server" Text='<%# Eval("nom_dist") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre de Inversión">
                                        <ItemTemplate>
                                            <asp:Label ID="nom_proyecto" Text='<%# Eval("nom_proyecto") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tipo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFinanciamiento" Text='<%# Eval("tipoFinanciamiento") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Monto Viable (S/.)">
                                        <ItemTemplate>
                                            <asp:Label ID="num_pips" Text='<%# Convert.ToDouble(Eval("monto_snip")).ToString("N") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                                <SelectedRowStyle CssClass="GridRowSelected" />
                                <HeaderStyle Height="15px" />
                                <AlternatingRowStyle CssClass="GridAlternating" />
                                <EditRowStyle BackColor="#FFFFB7" />
                                <RowStyle CssClass="GridRowNormal" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 33.33%" align="center">
                            <asp:Label ID="LabelTotalNoAsignado" runat="server" CssClass="small" />
                            <asp:Label ID="lblPaginado" runat="server" CssClass="small" /></td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnAsignar" CssClass="btn btn-primary" runat="server" Text="Asignar" OnClick="btnAsignar_OnClick" Width="120px" /></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <br />
        <div style="text-align: center">
            <p>
                <asp:Label ID="Label1" runat="server" Text="ASIGNADOS" CssClass="titulo2" Style="font-size: 13pt;"></asp:Label>
            </p>
        </div>
        <img src="linea.png" width="98%" height="13px" alt="" />
        <br />

        <asp:UpdatePanel ID="Up_grdAsignado" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 95%; margin: auto">

                    <tr>
                        <td>
                            <asp:GridView
                                ID="grdAsignados"
                                runat="server"
                                Width="100%"
                                AllowPaging="True"
                                DataKeyNames="id_proyecto"
                                AutoGenerateColumns="False"
                                BorderWidth="1px"
                                CssClass="Grid"
                                EmptyDataText="No hay proyectos asignados."
                                EditRowStyle-BackColor="#ffffb7"
                                OnPageIndexChanging="grdAsignados_PageIndexChanging"
                                OnDataBound="grdAsignados_OnDataBound"
                                PageSize="200">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkbAsignadosAll" onclick="return check_uncheck(this,'chkbAsignadosAll','chkbAsignados');" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="id_pryasignados" runat="server" Visible="False" Text='<%# DataBinder.Eval (Container.DataItem, "id_proyecto") %>' />
                                            <asp:CheckBox ID="chkbAsignados" onclick="return check_uncheck(this);" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIDProyectos" Text='<%# Eval("id_proyecto") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="anio" Text='<%# Eval("anio") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Programa">
                                        <ItemTemplate>
                                            <asp:Label ID="des_subsector" Text='<%# Eval("subsector") %>' runat="Server" />
                                            <asp:Label ID="id_sector" runat="server" Text='<%# (Eval("subPrograma") != DBNull.Value ? " - " + Eval("subPrograma") : "") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <%--<asp:TemplateField HeaderText="Sub Prog." Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="subPrograma" Text='<%# Eval("subPrograma") %>' runat="Server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="MODALIDAD DE FINANCIAMIENTO" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="ModalidadFinanciamiento" Text='<%# Eval("ModalidadFinanciamiento") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SNIP">
                                        <ItemTemplate>
                                            <asp:Label ID="cod_snip" Text='<%# Convert.ToInt32(Eval("cod_snip"))==0 ? "NO": Eval("cod_snip") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CUI">
                                        <ItemTemplate>
                                            <asp:Label ID="iCodUnificado" Text='<%# Eval("iCodUnificado") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Departamento" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="nom_depa" Text='<%# Eval("nom_depa") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Provincia" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblprov" runat="server" Text='<%# Eval("nom_prov") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Distrito">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldist" runat="server" Text='<%# Eval("nom_dist") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre de Inversión">
                                        <ItemTemplate>
                                            <asp:Label ID="nom_proyecto" Text='<%# Eval("nom_proyecto") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Fecha Asignación">
                                        <ItemTemplate>
                                            <asp:Label ID="ini_periodo" Text='<%# Eval("fecha_asignacion") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Técnico(s)">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltecnico" Text='<%# Eval("tecnico") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tipo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFinanciamiento" Text='<%# Eval("tipoFinanciamiento") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Monto Viable (S/.)">
                                        <ItemTemplate>
                                            <asp:Label ID="num_pips" Text='<%# Convert.ToDouble(Eval("monto_snip")).ToString("N") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader2" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                                <SelectedRowStyle CssClass="GridRowSelected" />
                                <HeaderStyle Height="15px" />
                                <AlternatingRowStyle CssClass="GridAlternating" />
                                <EditRowStyle BackColor="#FFFFB7" />
                                <RowStyle CssClass="GridRowNormal" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 33.33%" align="center">
                            <asp:Label ID="LabeltotalAsignado" runat="server" CssClass="small" />
                            <asp:Label ID="lblPaginadoAsignado" runat="server" CssClass="small" /></td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnNoAsignar" CssClass="btn btn-primary" runat="server" Text="Quitar Proyecto" OnClick="btnNoAsignar_OnClick" Width="150px" /></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="modal fade" id="modalTecnico" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ASIGNAR PROYECTO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="width: 98%;">
                        <asp:UpdatePanel runat="server" ID="Up_Agente" UpdateMode="Conditional">
                <ContentTemplate>
                    
                    <div class="alert alert-success">
                    <asp:Label ID="lblCantAgente" runat="server" Width="98%" Text="Usted marco 2 proyectos para asignación."></asp:Label>
                        </div>
                
                    <div class="content">
                    <div class="form-row p-2">
                        <div class="col-2">*Unidad :</div>
                        <div class="col-4"> <asp:DropDownList ID="ddlPrograma1" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma1_SelectedIndexChanged"></asp:DropDownList></div>
                    </div>
                     <div class="form-row p-2">
                        <div class="col-2">*Responsable:</div>
                        <div class="col-5"> 
                            <asp:DropDownList ID="ddlNombreAgente1" runat="server" Width="350px" CssClass="ddl-filter"></asp:DropDownList>

                        </div>
                    </div>
                        </div>
                    <table style="border-collapse: separate !important; border-spacing: 4px; margin: auto; width: 98%">
                        <tr>
                           <%-- <td style="text-align: right">*Función:
                            </td>--%>
                            <td style="text-align: left" runat="server" visible="false">
                                <asp:DropDownList ID="ddlTipoAgente1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAgente1_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <td>
                               
                            </td>
                            <td style="text-align: right">
                            </td>
                            <td style="text-align: left">
                                
                            </td>
                            <td></td>
                        </tr>
                        <tr runat="server" visible="false">
                            <td style="text-align: right">Función:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlTipoAgente2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAgente2_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>Programa :</td>
                            <td>
                                <asp:DropDownList ID="ddlPrograma2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma2_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                            <td style="text-align: right">Nombre:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlNombreAgente2" runat="server" CssClass="ddl-filter"></asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr runat="server" visible="false">
                            <td style="text-align: right">Función:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlTipoAgente3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAgente3_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>Programa :</td>
                            <td>
                                <asp:DropDownList ID="ddlPrograma3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma3_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                            <td style="text-align: right">Nombre:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlNombreAgente3" runat="server" CssClass="ddl-filter"></asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr runat="server" visible="false">
                            <td style="text-align: right">Función:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlTipoAgente4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAgente4_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>Programa :</td>
                            <td>
                                <asp:DropDownList ID="ddlPrograma4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma4_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                            <td style="text-align: right">Nombre:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlNombreAgente4" runat="server" CssClass="ddl-filter"></asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr runat="server" visible="false">
                            <td style="text-align: right">Función:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlTipoAgente5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAgente5_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>Programa :</td>
                            <td>
                                <asp:DropDownList ID="ddlPrograma5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma5_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                            <td style="text-align: right">Nombre:
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlNombreAgente5" runat="server" CssClass="ddl-filter"></asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="height: 10px; text-align: left" colspan="4">

                                <asp:Label ID="Label5" runat="server" Text="* Registro Obligatorio." Font-Size="12px" Font-Bold="true"></asp:Label>
                                <br />
                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Button ID="btnRegistrarAgente" runat="server" Text="REGISTRAR" CssClass="btn btn-primary" OnClick="btnRegistrarAgente_Click" />
                            </td>
                        </tr>
                    </table>
                   
                </ContentTemplate>
            </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceFooter" runat="server">
    <script language="javascript" type="text/javascript">
      
        function pageLoad() {
            $('#<%=TxtSnipFiltro.ClientID %>').tagEditor({
                delimiter: ', ', /* space and comma */
                placeholder: 'Ingrese snip o CUI separados por coma',
                maxTags: 20,
                maxLength: 7,
                removeDuplicates: true,
                beforeTagSave: function (field, editor, tags, tag, val) {
                    console.log("field", field);
                    console.log("editor", editor);
                    console.log("tags", tags);
                    console.log("tag", tag);
                    console.log("val", val);

                    if (!$.isNumeric(val))
                        return false;

                    //else if(tags.includes(val))
                }
            });

        }
    </script>
</asp:Content>