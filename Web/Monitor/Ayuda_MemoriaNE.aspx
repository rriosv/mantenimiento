﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ayuda_MemoriaNE.aspx.cs" Inherits="Web.Monitor.Ayuda_MemoriaNE" Culture="es-PE" UICulture="es-PE" Title="AYUDA MEMORIA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
   
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        BODY {
            font-family: HelveticaNeue;
        }

        @font-face {
            font-family: HelveticaNeue;
            src: local('HelveticaNeue'), url("../css/fuente/HelveticaNeue-Medium.otf") format("opentype");
        }
        .auto-style1 {
            text-align: left;
            border: 1px dotted gray;
            height: 67px;
        }

        @media print {
            #imgbtnImprimir, #imgbtnExportar, #imgbtnExportarMobile {
                display: none;
            }
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <center>
            <div id="div_Ficha" style="width: 800px; height: auto;">
                <center>
                    <%--  <div id="div_imagen">
                    <img src="../img/LogoMVCS1-estado.png" height="55"  alt="" />
                </div>--%>                    <%--<div id="title" style="display: none">
                    <center><span style="font-family: 'Trebuchet MS'; font-size: 9pt"><b>MINISTERIO DE VIVIENDA, CONSTRUCCI&Oacute;N Y SANEAMIENTO</b></span></center>
                </div>--%>
                    <table style="margin: auto; margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td rowspan="2">
                                <img src="../img/logo_ministerio.jpg" height="50"  alt="" style="padding-right:15px;padding-left:15px;" />
                            </td>

                        </tr>

                    </table>

                     <span style="font-family: Arial; font-size: 16pt">
                        <asp:Label runat="server" ID="lblTitle" Font-Bold="true" Text="AYUDA MEMORIA">
                        </asp:Label>
                    </span>
                    <br />
                    <asp:Label runat="server" Font-Size="9pt" Text="" ID="lblFechaAyuda"></asp:Label>
                    <div id="icon_word">
                        <center>
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" Width="25px" Height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                                        <asp:ImageButton ID="imgbtnExportarMobile" runat="server" ImageUrl="~/img/download_48.png" width="90%" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" Width="25px" Height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />

                                    </td>
                                </tr>
                            </table>
                        </center>

                    </div>
                    <%--<span style="font-family:'Calibri';font-size:11pt"><b><asp:Label ID="lblComponente1" runat="server" Text=""></asp:Label>, <asp:Label ID="lblDistrito1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblProvincia1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblDepartamento1" runat="server" Text=""></asp:Label></b></span>--%>
                </center>

                <%-- <div style="width:100%;text-align:left">
            <span style="font-family:'Calibri';font-size:11pt"><b>DATOS GENERALES:</b></span>
        </div>--%>

                <div style="width: 100%" id="idDivInformacion">
                    <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">

                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray; width: 148px;"><asp:Label runat="server" ID="LblTextoCodigoSnip" Text="CÓDIGO SNIP"></asp:Label> </td>
                            <td class="celdaTabla" style="border: 1px dotted gray;"><b>
                                <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label></b></td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">NOMBRE PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr style="visibility:collapse" runat="server" visible="false">
                            <td class="celdaTabla" style="border: 1px dotted gray;">MONTO DE INVERSI&Oacute;N TOTAL</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">

                                <asp:Label ID="lblMoneda_PMIB" runat="server" Text="S/."></asp:Label><asp:Label ID="lblMonto1_PMIB" runat="server" Text=""></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">BENEFICIARIO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblBeneficiario" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblhabitantes" runat="server" Text="Habitantes (Fuente Formato SNIP 03)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="border: 1px dotted gray;">FINANCIAMIENTO</td>
                            <td class="auto-style1" style="border: 1px dotted gray">
                                <div>
                                    <table cellpadding="2" cellspacing="0" align="left">
                                        <tr>
                                            <td align="left">MONTO APROBADO :</td>
                                            <td align="right" style="width: 80px;">
                                                <asp:Label ID="lblPorcentajeMontoMVCS_PMIB" Font-Bold="true" runat="server" Text=""></asp:Label>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">MONTO A FINANCIAR : </td>
                                            <td align="right">
                                                <asp:Label ID="lblPorcentajeMontoMUNIC_PMIB" Font-Bold="true" runat="server" Text=""></asp:Label>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"><asp:Label runat="server" ID="LblTextoMontoTransferirNE" Text="MONTO A TRANSFERIR al NE.:"></asp:Label></td>
                                            <td align="right">
                                                <asp:Label ID="lblPorcentajeMontoBENEF_PMIB" Font-Bold="true" runat="server" Text=""></asp:Label>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray"><asp:Label runat="server" ID="LblTextUnidadEjecutora" Text=" UNIDAD EJECUTORA"></asp:Label></td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblEjecutora" runat="server" Text=""></asp:Label></td>
                        </tr>
                                            
                        <tr runat="server" id="TrCostoProyecto">
                            <td class="celdaTabla" style="border: 1px dotted gray">COSTO PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <div>
                                    <table align="left">
                                        <tr>
                                            <td align="left"><b>Monto Viable: </b></td>
                                            <td align="left">
                                                <asp:Label ID="lblMoneda" runat="server" Text="S/."></asp:Label><asp:Label ID="lblMontoViable" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><b>Monto de Fase de Inversi&oacute;n: </b></td>
                                            <td>
                                                <asp:Label ID="Label9" runat="server" Text="S/."></asp:Label><asp:Label ID="lblMontoFaseInversion" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        
                        <tr runat="server" id="TrDesembolsos">
                            <td class="celdaTabla" style="border: 1px dotted gray">DESEMBOLSOS</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                                                                          
                                    <asp:GridView runat="server" ID="grdDevengado" EmptyDataText="No hay registro." 
                                    ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                    CellPadding="2" CellSpacing="2" Width="70%" Font-Size="14px" CssClass="center">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Periodo" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEntidad" Text='<%# Eval("Periodo") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle  HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="FechaCompromiso" HeaderText="Fecha Compromiso" >
                                            <HeaderStyle  HorizontalAlign="Center" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="MontoCompromiso" HeaderText="Monto Compromiso (S/)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FechaDevengado" HeaderText="Fecha Devengado" >
                                            <HeaderStyle  HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MontoDevengado" HeaderText="Monto Devengado (S/)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>                                       

                                    </Columns>
                                    <HeaderStyle Height="13px" Font-Names="Calibri" Font-Size="13px" />
                                    <RowStyle Font-Size="12px" Font-Names="Calibri" />
                                    <EditRowStyle BackColor="#FFFFB7" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">METAS</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label runat="server" ID="lblMetas"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">DATOS DE SEGUIMIENTO A LA EJECUCI&Oacute;N DE OBRA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <div>
                                    <table>
                                        <%--                                    <tr>
                                        <td><span style="font-size:14px"><b>S/. 1506,083.00</b></span></td>
                                    </tr>--%>
                                        <tr>
                                            <td><u>Fecha Inicio</u></td>
                                            <td>
                                                <asp:Label ID="lblFechaInicio1" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Plazo de Ejecución</u></td>
                                            <td>
                                                <asp:Label ID="lblPlazoEjecucion1" runat="server" Text=""></asp:Label><asp:Label ID="Label1" runat="server" Text=" (Días Calendario)"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>F. Termino + Ampl. + Paraliz.</u></td>
                                            <td>
                                                <asp:Label ID="lblFechaTerminoContractual" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                       <%-- <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblFechaTerminoReal" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                     --%>

                                        <tr runat="server" id="trd1">
                                            <td><u>Supervisor</u></td>
                                            <td>
                                                <asp:Label ID="lblSupervisor" runat="server"></asp:Label></td>
                                        </tr>
                                                                              
                                        <tr runat="server" id="trd2">
                                            <td><u>Residente</u></td>
                                            <td>
                                                <asp:Label ID="lblResdente" runat="server"></asp:Label></td>
                                        </tr>
                                       
                                       
                                        <tr>
                                            <td><u><asp:Label runat="server" ID="LblTextoAvanceFisicoAcumulado" Text="Avance Físico Acumulado"></asp:Label> </u></td>
                                            <td>
                                                <asp:Label ID="lblPorcentajeAvance" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Avance Programado Acumulado</u></td>
                                            <td>
                                                <asp:Label ID="lblAvanceProgramado" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u><asp:Label runat="server" ID="LblTextoAvanceFinancieroAcumulado" Text="Avance Financiero Acumulado"></asp:Label></u></td>
                                            <td>
                                                <asp:Label ID="lblAvanceFinanciero" runat="server" Text=""></asp:Label></td>
                                        </tr>


                                    </table>
                                </div>
                            </td>
                        </tr>
                     
                        <tr>
                            <td colspan="2" class="celdaTabla" style="border: 1px dotted gray">
                                <table width="100%" cellspacing="0" cellpadding="3">
                                    <tr>
                                        <td rowspan="3" class="celdaTabla" style="border-right: 1px dotted gray; width: 60px;">SITUACIÓN ACTUAL</td>
                                        <td class="celdaTabla" style="border-bottom: 1px dotted gray; border-right: 1px dotted gray; width: 80px;">ESTADO</td>
                                        <td class="celdaTabla" style="border-bottom: 1px dotted gray; border-left: 1px dotted gray;">
                                            <asp:Label ID="lblEstadoSituacional" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="celdaTabla" style="border-bottom: 1px dotted gray; border-right: 1px dotted gray; width: 80px;">DETALLE DEL ESTADO SITUACIONAL</td>
                                        <td style="border-bottom: 1px dotted gray; border-left: 1px dotted gray;">
                                            <asp:Label ID="lblDetalleSituacional" runat="server"></asp:Label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="celdaTabla" style="border-right: 1px dotted gray; width: 80px;">ACCIONES</td>
                                        <td style="border-left: 1px dotted gray;">
                                            <asp:Label ID="lblAcciones" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>


                        </tr>

                    </table>
                </div>
                <br />
                <br />
            </div>

        </center>

    </form>
</body>
</html>
