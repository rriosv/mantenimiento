﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Seace_Ficha.aspx.cs" Inherits="Web.Monitor.Seace_Ficha" Culture="es-PE" UICulture="es-PE" Title="" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/print.css" type="text/css" media="print" />
    <style type="text/css">
        .celdaTabla{text-align:left;border:1px dotted gray}
        .celdaTabla2{text-align:right;}
        .center {
            margin:auto;
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <center>
        <div id="div_Ficha" style="width: 760px; height: auto;">
            <center>
                <span style="font-family: Arial; font-size: 16pt">
                    <b>
                    <asp:Label runat="server" Font-Size="16pt"  Text="" ID="lblDetalle" Visible="true"></asp:Label>
                    </b>
                </span>
                <br />
                <asp:Label runat="server" Font-Size="9pt"  Text="" ID="lblFechaAyuda" Visible="false"></asp:Label>
                <div id="icon_word">
                    <center>
                        <table>
                            <tr>
                            <td>
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />
                            </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
            </center>
       
            <div style="width: 100%">
                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td colspan="2" style="text-align:left; font-weight:bold">1. INFORMACIÓN GENERAL</td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray;width:148px;">IDENTIFICADOR CONVOCATORIA:</td>
                        <td class="celdaTabla" style="border: 1px dotted gray;"><b>
                            <asp:Label ID="lblIdentificador" runat="server" Text=""></asp:Label></b></td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">ENTIDAD CONVOCANTE</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblEntidadConvocante" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray;" >
                            DESCRIPCION DEL OBJETO
                        </td>
                           <td class="celdaTabla" style="border: 1px dotted gray">
                                        <asp:Label ID="lblDescripcion" runat="server" Text=""></asp:Label>
                        </td>
                   </tr>
                  <%--  <tr runat="server" id="trDescripcionItem" visible="false" >
                        <td class="celdaTabla" style="border: 1px dotted gray">DESCRIPCION DEL ITEM</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblDescripcionItem" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">TIPO DE SELECCIÓN</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblTipoSeleccion" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">MODALIDAD</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                      <asp:Label ID="lblModalidad" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">NORMATICA APLICABLE</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                      <asp:Label ID="lblNormativa" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">SISTEMA DE CONTRATACIÓN</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                      <asp:Label ID="lblSistemaContratacion" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">NOMENCLATURA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                      <asp:Label ID="lblNomenclatura" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">FECHA Y HORA PUBLICACIÓN</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                      <asp:Label ID="lblFechaPublicacion" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">VALOR REFERENCIAL</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                      <asp:Label ID="lblValorReferencial" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="2" style="text-align:left; font-weight:bold">2. RESULTADO DEL PROCESO</td>
                    </tr>
                    </table>
                <table runat="server" id="tblResultadoIndividual"  cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">ESTADO DE ITEM</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblEstadoItem" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">FECHA DE BUENA PRO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblFechaBuenaPRo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray;width:148px;" >FECHA DE BUENA PRO CONSENTIDA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblFechaBuenaProConsentida" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">VALOR ADJUDICADO ITEM</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblValorAdjudicado" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">MONEDA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblMoneda" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">N° CONTRATO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblNroContrato" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">FECHA DE CONTRATO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblFechaContrato" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">VER CONTRATO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblURLContrato" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trEmpresa1" visible="false">
                        <td colspan="2" style="text-align:left; font-weight:bold">3. INFORMACIÓN DEL GANADOR DEL PROCESO</td>
                    </tr>
                    <tr runat="server" id="trEmpresa2" visible="false">
                        <td colspan="2" style="text-align:left; font-weight:bold">
                            <asp:Label ID="lblNommEmpresa" runat="server" Text="3.1. EMPRESA"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trEmpresa3" visible="false">
                        <td class="celdaTabla" style="border: 1px dotted gray">RUC / CODIGO GANADOR</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblRucGanador" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trEmpresa4" visible="false">
                        <td class="celdaTabla" style="border: 1px dotted gray">NOMBRE</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                        <asp:Label ID="lblEmpresaGanador" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" visible="false" id="trEmpresa5">
                        <td class="celdaTabla" style="border: 1px dotted gray">VER GARANTIAS</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <%--<img src="../img/pdf_48x48.png" title="Ver Garantias" alt="Ver Garantias" />--%>
                              <asp:Label ID="lblUrlGarantias" runat="server" Text=""></asp:Label>
                         </td>
                    </tr>
                    <tr runat="server" visible="false" id="trConsorcio1">
                        <td colspan="2" style="text-align: left; font-weight:bold">3.2. CONSORCIADOS</td>
                    </tr>
                    <tr runat="server" visible="false" id="trConsorcio2">
                        <td class="celdaTabla" style="border: 1px dotted gray" colspan="2">
                            <asp:GridView runat="server" ID="grdConsorcio" EmptyDataText="No hay registros."
                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnRowDataBound="grdConsorcio_RowDataBound" 
                                RowStyle-HorizontalAlign="Center"
                                CellPadding="2" CellSpacing="2" Width="80%" >
                                <Columns>
                                 

                                    <asp:TemplateField HeaderText="Nombre Miembro" Visible="true">
                                        <ItemTemplate>
                                            <asp:Label ID="NOMBRE_MIEMBRO_CONSORCIO" Text='<%# Eval("NOMBRE_MIEMBRO_CONSORCIO") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2"  HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="RUC de Empresas Consorciadas" Visible="true">
                                        <ItemTemplate>
                                            <asp:Label ID="RUC_MIEMBRO_CONSORCIO" Text='<%# Eval("RUC_MIEMBRO_CONSORCIO") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Porcentaje Participacion" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="PorcentajeParticipacion" Text='<%# Eval("PorcentajeParticipacion") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ver Garantias" Visible="false">
                                        <ItemTemplate>
                                           <%-- <asp:ImageButton ID="imgDocInformeTab4" runat="server" OnClick="imgDocInformeTab4_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                ToolTip='<%# Eval("UrlGarantia") %>' />--%>

                                              <%--<a style="text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000" href="<%# Eval("UrlGarantia") %>" target="_blank" title="Ver contrato">
                                                    <img src="../img/pdf_48x48.png" title="Ver Contrato" alt="Ver Contrato" width="32px" />
                                                </a>--%>
                                            <asp:Label ID="lblUrlGarantias" runat="server" Text=""></asp:Label>

                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader2" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle Height="15px" />
                                <EditRowStyle BackColor="#FFFFB7" />
                            </asp:GridView>
                        </td>

                    </tr>
                </table>

                <asp:Label runat="server" ID="lblTableMultiple"></asp:Label>
            </div>
            <br />
            <br />
        </div>
    
     </center>
    
    </form>
</body>
</html>
