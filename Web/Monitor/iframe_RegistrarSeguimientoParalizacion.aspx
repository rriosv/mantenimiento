﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iframe_RegistrarSeguimientoParalizacion.aspx.cs" Inherits="Web.Monitor.iframe_RegistrarSeguimientoParalizacion" Culture="es-PE" UICulture="es-PE" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <asp:PlaceHolder runat="server">
        <%:System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>

    <link href="../sources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/Monitoreo.css" rel="stylesheet" type="text/css" />
    <link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body {
            font-family: Calibri;
        }

        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .CajaDialogo {
            background-color: #CEE3F6;
            border-width: 4px;
            padding: 0px;
            width: 200px;
            font-weight: bold;
        }

            .CajaDialogo div {
                margin: 5px;
                text-align: center;
            }

        /* Separate css element for error modals to specify 
z-index to get the popup on top of all existing popups*/

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            font-family: Calibri;
        }

        input[type=text] {
            /*	background-image: url(../img/pdf.gif); 
	        background-repeat: repeat-x;*/
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-family: Tahoma;
            font-size: 11px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }

        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            margin-top: -50px;
            margin-left: 100px;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }

        .Div_Fondo {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: fixed;
            z-index: 1004;
            top: 0;
            left: 0;
            right: 0;
            bottom: -1px;
            height: auto;
        }

        .container {
            max-width: 1300px;
        }



        #cssmenu {
            background: #299386;
            list-style: none;
            margin: 0;
            padding: 0;
            width: 16em;
            display: inline-block;
            margin-top: 3em;
            font-family: Calibri,arial,helvetica,clean,sans-serif !important;
        }

            #cssmenu li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            #cssmenu a {
                background: #299386;
                color: white;
                display: block;
                margin: 0;
                padding: 8px 12px;
                text-decoration: none;
                font-weight: normal;
            }

                #cssmenu a:hover, #cssmenu li.active {
                    background: #2383C6 url("images/hover.gif") left center no-repeat;
                    color: #fff;
                    border-bottom: 4px solid #A9D8F9;
                }
        /* FIN MENU OBRA*/


        .TablaMetasPMIB {
            font-size: 10px;
        }


            .TablaMetasPMIB input[type="text"] {
                font-family: Tahoma;
                font-size: 10px;
            }

        .tooltip {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
        }

            /* Tooltip text */
            .tooltip .tooltiptext {
                visibility: hidden;
                width: 120px;
                background-color: #555;
                color: #fff;
                text-align: center;
                padding: 5px 0;
                border-radius: 6px;
                /* Position the tooltip text */
                position: absolute;
                z-index: 1;
                bottom: 125%;
                left: 50%;
                margin-left: -60px;
                /* Fade in tooltip */
                opacity: 0;
                transition: opacity 1s;
            }

                /* Tooltip arrow */
                .tooltip .tooltiptext::after {
                    content: "";
                    position: absolute;
                    top: 100%;
                    left: 50%;
                    margin-left: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: #555 transparent transparent transparent;
                }

            /* Show the tooltip text when you mouse over the tooltip container */
            .tooltip:hover .tooltiptext {
                visibility: visible;
                opacity: 1;
            }

        .tooltips {
            position: relative;
            display: inline-block;
            font-weight: 300;
        }

            .tooltips > span, a.tooltips > span {
                visibility: hidden;
                /*width: 120px;*/
                background-color: #555;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 8px 10px;
                position: absolute;
                z-index: 1;
                bottom: 125%;
                left: 50%;
                transform: translate(-50%, 0%);
                opacity: 0;
                transition: opacity 1s;
                min-width: 140px;
                font-size: 1.2em;
                text-shadow: none !important;
            }

                .tooltips > span::after {
                    content: "";
                    position: absolute;
                    top: 100%;
                    left: 50%;
                    margin-left: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: #555 transparent transparent transparent;
                }

            .tooltips:hover > span {
                visibility: visible;
                opacity: 1;
            }


            .tooltips > span {
                text-align: center;
                cursor: default;
            }

                .tooltips > span > div {
                    display: inline-block;
                }

                    .tooltips > span > div div {
                        display: block;
                    }

                        .tooltips > span > div div:first-child {
                            text-align: center;
                        }

                .tooltips > span > a {
                    color: #ffffff;
                    text-decoration: none;
                }

                    .tooltips > span > a i {
                        background-color: #ffffff;
                        height: 36px;
                        width: 36px;
                        font-size: 20px;
                        line-height: 36px;
                        vertical-align: middle;
                        border-radius: 50%;
                    }

                    .tooltips > span > a:hover i {
                        background-color: #ededed;
                    }

                .tooltips > span > div {
                    padding: 0 15px;
                }

                    .tooltips > span > div:first-child {
                        padding-left: 15px;
                    }

                    .tooltips > span > div:last-child {
                        padding-right: 15px;
                    }

                .tooltips > span > span {
                    width: 100%;
                    background-color: transparent;
                    display: block;
                    height: 10px;
                    position: absolute;
                    bottom: -5PX;
                    left: 0PX;
                }

        /*BOTONES*/
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px !important;
        }

        .btn-group-xs > .btn, .btn-xs {
            padding: 1px 5px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 4px;
        }

        .btn-group-sm > .btn, .btn-sm {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px;
        }

        .btn-group-lg > .btn, .btn-lg {
            padding: 10px 16px;
            font-size: 18px;
            line-height: 1.3333333;
            border-radius: 6px;
        }

        .btn-outline:active, .btn-outline:focus, .btn-outline:hover {
            color: #fff;
            background-color: #563d7c;
            border-color: #FFFFFF;
        }

        .btn-outline {
            color: #563d7c;
            background-color: transparent;
            border-color: #FFFFFF;
        }

        .btn-azul {
            color: #fff !important;
            background-color: #19a1e0 !important;
            border-color: #FFFFFF !important;
        }

            .btn-azul:active, .btn-azul:focus, .btn-azul:hover {
                background-color: #47B4E6 !important;
                border-color: #FFFFFF !important;
            }

        .btn-verde {
            color: #fff;
            background-color: #6EBD71;
            border-color: #FFFFFF;
        }

            .btn-verde:active, .btn-verde:focus, .btn-verde:hover {
                background-color: #8AC98C;
                border-color: #FFFFFF;
            }

        .btn-amarillo {
            color: #fff;
            background-color: #FFC107;
            border-color: #FFFFFF;
        }

            .btn-amarillo:active, .btn-amarillo:focus, .btn-amarillo:hover {
                background-color: #FFCD39;
                border-color: #FFFFFF;
            }

        .btn-amarillo2 {
            color: #000000;
            background-color: #FFC107;
            border-color: #FFFFFF;
        }

            .btn-amarillo2:active, .btn-amarillo2:focus, .btn-amarillo2:hover {
                color: #000000;
                background-color: #FECC38;
                border-color: #FFFFFF;
            }

        .btn-rojo {
            color: #fff;
            background-color: #F44336;
            border-color: #FFFFFF;
        }

            .btn-rojo:active, .btn-rojo:focus, .btn-rojo:hover {
                background-color: #F6695E;
                border-color: #FFFFFF;
            }

        .btn-rojo2 {
            color: #ffffff;
            background-color: #F44336;
            border-color: #FFFFFF;
        }

            .btn-rojo2:active, .btn-rojo2:focus, .btn-rojo2:hover {
                color: #ffffff;
                background-color: #F5685D;
                border-color: #FFFFFF;
            }

        .btn-plomo {
            color: #fff;
            background-color: #58595B;
            border-color: #FFFFFF;
        }

            .btn-plomo:active, .btn-plomo:focus, .btn-plomo:hover {
                background-color: #797A7C;
                border-color: #FFFFFF;
            }

        .btn-blanco {
            color: #000000;
            background-color: #FAFAFA;
            border-color: #FFFFFF;
        }

            .btn-blanco:active, .btn-blanco:focus, .btn-blanco:hover {
                color: #000000;
                background-color: #ededed;
                border-color: #FFFFFF;
            }

        .btn-tag1, .btn-tag2 {
            color: #ffffff !important;
            border-color: #FFFFFF !important;
        }

        .btn-tag1 {
            background-color: #5C6BC0;
        }

            .btn-tag1:active, .btn-tag1:focus, .btn-tag1:hover {
                background-color: #7D89CD;
            }

        .btn-tag2 {
            background-color: #19a1e0;
        }

            .btn-tag2:active, .btn-tag2:focus, .btn-tag2:hover {
                background-color: #47B4E6;
            }

        .btn-tag3 {
            color: #ffffff;
            background-color: #F20202;
        }

            .btn-tag3:active, .btn-tag3:focus, .btn-tag3:hover {
                background-color: #F44336;
            }

        .GridHeader2 {
            font-weight: initial;
        }
    </style>

    <script src="../js/pace.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>

        <div>
            <asp:UpdatePanel runat="server" ID="Up_SeguimientoParalizacion" UpdateMode="Conditional">
                <ContentTemplate>

                    <asp:Panel ID="Panel13" Visible="true" runat="server" HorizontalAlign="Center">


                        <asp:Label ID="LblID_PROYECTO" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Label ID="lblSNIP" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Label ID="LblID_USUARIO" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblIdEvaluacionRecomendacionParalizacion" runat="server" Visible="false"></asp:Label>

                        <table style="width: 100%; margin: auto; border-collapse: separate; border-spacing: 4px">

                            <tr>
                                <td style="text-align: right">Coordinador MVCS (Paralización) :</td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtCoordinadorMVCSParalizado" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    <asp:ImageButton CssClass="imgActualizar" ID="imgbtnAgregarCoodinador" runat="server"
                                        ImageUrl="~/img/edit.png" onmouseover="this.src='../img/edit2.png';" onmouseout="this.src='../img/edit.png';"
                                        Height="25px" OnClientClick="" OnClick="imgbtnAgregarCoodinador_Click" AlternateText="Agregar"
                                        ToolTip="Registrar Coordinador." />

                                </td>
                                <td style="text-align: right">Fecha Paralización :</td>
                                <td style="text-align: left; padding-right: 20px">
                                    <asp:TextBox ID="txtFechaParalizacion" runat="server" placeholder="dd/mm/yyyy" Width="70px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFechaParalizacion"
                                        FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                    <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtFechaParalizacion"
                                        Format="dd/MM/yyyy"></asp:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel runat="server" ID="Panel_Coordinador" Visible="false" Width="450px">
                                        <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                            <table style="width: 100%; border-collapse: separate; border-spacing: 4px">
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:GridView runat="server" ID="grd_HistorialCoordinador" EmptyDataText="No hay data"
                                                            ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" Width="100%"
                                                            CellPadding="2" CellSpacing="2" DataKeyNames="id_empleadoParalizacion"
                                                            OnRowCommand="grd_HistorialCoordinador_RowCommand"
                                                            OnSelectedIndexChanged="grd_HistorialCoordinador_SelectedIndexChanged">
                                                            <Columns>

                                                                <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit." HeaderStyle-Width="18px"></asp:CommandField>

                                                                <asp:TemplateField HeaderText="Coordinador MVCS" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblnombreS" Text='<%# Eval("nombre") %>' CssClass="tablaGrilla" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Fecha Designación" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFechaSS" CssClass="tablaGrilla" Text='<%# Eval("fecha") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" Width="100px" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminarCoordinador" Width="18px" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <HeaderStyle Height="15px" />
                                                            <EditRowStyle BackColor="#FFFFB7" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center" colspan="2">
                                                        <b>REGISTRAR COORDINADOR</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right">Coordinador MVCS:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:TextBox ID="txtCoordinador" runat="server" Width="200px"></asp:TextBox>
                                                        <asp:Label ID="lblIdCoordinadorM" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right">Fecha Designación:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <asp:TextBox ID="txtFechaC" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="txtFechaC"
                                                            FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                        <asp:CalendarExtender ID="CalendarExtender40" runat="server" TargetControlID="txtFechaC"
                                                            Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 10px"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: center">
                                                        <asp:Button ID="btnGuardar_Coordinador" runat="server" Text="Guardar" OnClick="btnGuardar_Coordinador_Click" />
                                                        <asp:Button ID="btnModificar_Coordinador" runat="server" Text="Modificar" OnClick="btnModificar_Coordinador_Click" Visible="false" />
                                                        <asp:Button ID="btnCancelar_Coordinador" runat="server" OnClick="btnCancelar_Coordinador_Click" Text="Cancelar" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">Fecha Visita Diagnostico :</td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtFechaVisitaDiagnosticoParalizacion" runat="server" placeholder="dd/mm/yyyy" Width="70px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtFechaVisitaDiagnosticoParalizacion"
                                        FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                    <asp:CalendarExtender ID="CalendarExtender28" runat="server" TargetControlID="txtFechaVisitaDiagnosticoParalizacion"
                                        Format="dd/MM/yyyy"></asp:CalendarExtender>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align: right">Problematica :</td>
                                <td colspan="3" style="text-align: left">
                                    <asp:TextBox ID="txtProblematica" runat="server" Width="60%" Enabled="false"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: right">Detalle de la Problematica :</td>
                                <td colspan="3" style="text-align: left">
                                    <asp:TextBox ID="txtDetalleProblematicaParalizacion" runat="server" TextMode="MultiLine" Width="90%" Height="100px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: right">Diagnostico de la Obra Paralizada :</td>
                                <td colspan="3" style="text-align: left">
                                    <asp:TextBox ID="txtDiagnostico" runat="server" TextMode="MultiLine" Width="90%" Height="170px"></asp:TextBox></td>
                            </tr>

                            <tr>
                                <td style="text-align: right">Fecha Reinicio :</td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtFechaReinicio" runat="server" placeholder="dd/mm/yyyy" Width="70px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtFechaReinicio"
                                        FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                    <asp:CalendarExtender ID="CalendarExtender30" runat="server" TargetControlID="txtFechaReinicio"
                                        Format="dd/MM/yyyy"></asp:CalendarExtender>
                                    (<b>Aplica:</b> Cuando se mantiene contrato de ejecución de obra y su modalidad. En caso se elabore el Exp. Técnico de Saldo, no llenar el campo.)
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: right; width: 200px">
                                    <asp:Label runat="server" ID="lblFechaFinanciamientoExp" Text="Fecha de financiamiento del Exp. Técnico de Saldo :" Visible="false"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtFechaFinanciamientoExp" runat="server" placeholder="dd/mm/yyyy" Width="70px" Visible="false"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFechaFinanciamientoExp" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaFinanciamientoExp" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: right; width: 200px">Acciones para reinicio<br>
                                    de obra paralizada
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="up1" runat="server">
                                        <ContentTemplate>
                                          
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                  
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" style="text-align: left; padding-left: 30px;">
                                    <asp:Label ID="lblNombreActualizacionParalizacion" runat="server" Font-Size="10px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" colspan="4">
                                    <asp:Button ID="btnGuardarParalizacion" runat="server" OnClientClick="GuardarArchivos()" Text="Guardar" OnClick="btnGuardarParalizacion_Click" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table style="margin: auto; width: 95%">
                                        <tr>
                                            <td>
                                                <asp:GridView ID="grdSeguimientoParalizacion" runat="server"
                                                    EmptyDataText="No hay información registrada"
                                                    ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                    DataKeyNames="id_seguimientoParalizacion"
                                                    OnRowCommand="grdSeguimientoParalizacion_RowCommand"
                                                    OnRowDataBound="grdSeguimientoParalizacion_RowDataBound"
                                                    OnSelectedIndexChanged="grdSeguimientoParalizacion_SelectedIndexChanged"
                                                    RowStyle-HorizontalAlign="Center" Width="100%"
                                                    CellPadding="2" CellSpacing="2">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit." HeaderStyle-Width="20px"></asp:CommandField>

                                                        <asp:TemplateField HeaderText="Tipo Monitoreo" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTipoMonitoreo" Text='<%# Eval("TipoMonitoreo") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" Height="30px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="Fecha" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFecha" Text='<%# Eval("fecha") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Situación Actual" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSituacionActual" Text='<%# Eval("situacionActual") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Acciones Realizadas" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAccionesRealizadas" Text='<%# Eval("accionesRealizadas") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Acciones Por Realizar" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAccionesPorRealizar" Text='<%# Eval("accionesPorRealizar") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Acta y/o Asistencia" Visible="true">
                                                            <ItemTemplate>
                                                                <div class="container-vertical">

                                                                    <asp:LinkButton ID="lnkbtnVerActa" runat="server" Visible="true" OnClick="lnkbtnVerActa_Click" title="" CssClass="btn btn-tag1 btn-xs" ToolTip="Clic para ver la información registrada.">
                                                                        <asp:Label ID="lblNombreActa" runat="server" Visible="true"></asp:Label>
                                                                    </asp:LinkButton>
                                                                    <br />
                                                                    <asp:ImageButton ID="imgDocActaInformeSeguimiento" runat="server" OnClick="imgDocActaInformeSeguimiento_Click" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                        ToolTip='<%# Eval("urlActa") %>' />
                                                                </div>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Informes" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgDocInformeSeguimiento" runat="server" OnClick="imgDocInformeSeguimiento_Click" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                    ToolTip='<%# Eval("urlInforme") %>' />

                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Oficio" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgDocOficioSeguimiento" runat="server" OnClick="imgDocOficioSeguimiento_Click" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                    ToolTip='<%# Eval("urlOficio") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Elim." Visible="true">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgAvanceFotoEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFechaUpdate" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                <asp:Label ID="lblIdFicha" Text='<%# Eval("id_ficha") %>' runat="server"></asp:Label>
                                                                <asp:Label ID="lblUsuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                <asp:Label ID="lblIdTipoMonitoreo" Text='<%# Eval("id_tipoMonitoreoParalizacion") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                            <td style="vertical-align: top">
                                                <asp:ImageButton ID="imgbtnAgregarSeguimientoParalizacion" runat="server" ImageUrl="~/img/add.png"
                                                    onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                    Height="30px" AlternateText="Agregar" ToolTip="Agregar" OnClick="imgbtnAgregarSeguimientoParalizacion_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />

                    <asp:Panel ID="Panel_RegistroSeguimientoParalizacion" Visible="false" runat="server">
                        <div style="width: 80%; padding: 5px 10px 5px 10px; text-align: center; margin: auto" class="CuadrosEmergentes">
                            <table style="width: 95%; margin: auto; border-collapse: separate; border-spacing: 4px;">
                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <b>SEGUIMIENTO DE PARALIZACION</b>
                                        <asp:Label ID="lblIdSeguimientoParalizacion" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lblIdFicha" runat="server" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 10px"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; width: 20%">Tipo de Monitoreo :
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="ddlTipoMonitoreoSeguimiento" runat="server">
                                            <asp:ListItem Text=" Seleccionar " Value=""></asp:ListItem>
                                            <asp:ListItem Text="Remision UE" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Visita de Obra" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Asistencia Técnica" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="text-align: right">Fecha :
                                    </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtFechaSeguimiento" runat="server" placeholder="dd/mm/yyyy" Width="70px"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtFechaSeguimiento"
                                            FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                        <asp:CalendarExtender ID="CalendarExtender43" runat="server" TargetControlID="txtFechaSeguimiento"
                                            Format="dd/MM/yyyy"></asp:CalendarExtender>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Situación Actual :</td>
                                    <td colspan="3" style="text-align: left">
                                        <asp:TextBox ID="txtSituacionActualSeguimiento" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">Acciones Realizadas :</td>
                                    <td colspan="3" style="text-align: left">
                                        <asp:TextBox ID="txtAccionesRealizadasSeguimiento" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">Acciones Por Realizar :</td>
                                    <td colspan="3" style="text-align: left">
                                        <asp:TextBox ID="txtAccionesPorRealizar" runat="server" TextMode="MultiLine" Width="90%"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Acta y/o Asistencia :
                                    </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:FileUpload ID="FileUploadActaSeguimiento" runat="server" />
                                        <asp:ImageButton ID="imgbtnActaSeguimiento" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnActaSeguimiento_Click" />
                                        <asp:LinkButton ID="lnkbtnActaSeguimiento" runat="server" OnClick="lnkbtnActaSeguimiento_Click"></asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Informe :
                                    </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:FileUpload ID="FileUploadInformeSeguimiento" runat="server" />
                                        <asp:ImageButton ID="imgbtnInformeSeguimiento" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnInformeSeguimiento_Click" />
                                        <asp:LinkButton ID="lnkbtnInformeSeguimiento" runat="server" OnClick="lnkbtnInformeSeguimiento_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">Oficio :
                                    </td>
                                    <td style="text-align: left" colspan="3">
                                        <asp:FileUpload ID="FileUploadOficioSeguimiento" runat="server" />
                                        <asp:ImageButton ID="imgbtnOficioSeguimiento" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnOficioSeguimiento_Click" />
                                        <asp:LinkButton ID="lnkbtnOficioSeguimiento" runat="server" OnClick="lnkbtnOficioSeguimiento_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left; padding-left: 30px;">
                                        <asp:Label ID="lblNombreActualizaSeguimientoParalizada" runat="server" Font-Size="10px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left">
                                        <asp:Label runat="server" ID="lbl" Font-Size="11px" Font-Bold="true" Text="(NOTA: Luego de registrar el Seguimiento de la Paralización se habilitará la opción para el registro del Acta de Visita y/o Seguimiento de Obra Paralizadad.)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center"></td>
                                </tr>

                                <tr>
                                    <td colspan="4" style="text-align: left">

                                        <asp:Panel ID="Panel_InformacionComplementariaParalizada" Visible="false" runat="server" Width="85%">
                                            <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentesFuerte">

                                                <table>
                                                    <tr>
                                                        <td><b><u>INFORMACIÓN COMPLEMENTARIA : </u></b>
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNombreActaParalizada" runat="server" Text="" Font-Bold="true"></asp:Label>:
                                                            <asp:LinkButton ID="lnkbtnRegistrarActaParalizada" runat="server" Visible="true" OnClick="lnkbtnRegistrarActaParalizada_Click">
                                                                  <i class="fa fa-list-ol"></i>
                                                                <div class="botonMonitor-morado">REGISTRAR Y/O MODIFICAR</div>
                                                            </asp:LinkButton>

                                                            <button type="button" style="display: none;" id="btnShowPopupActaParalizada" class="btn btn-primary btn-lg"
                                                                data-toggle="modal" data-target="#modalActaVisita">
                                                                Launch demo modal
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" colspan="4">
                                        <asp:Button ID="btnGuardarSeguimientoParalizacion" runat="server" Text="Guardar" OnClick="btnGuardarSeguimientoParalizacion_Click" />
                                        <asp:Button ID="btnModificarSeguimientoParalizacion" runat="server" Text="Modificar" OnClick="btnModificarSeguimientoParalizacion_Click" Visible="false" />
                                        <asp:Button ID="btnCancelarSeguimientoParalizacion" runat="server" Text="Cancelar" OnClick="btnCancelarSeguimientoParalizacion_Click" />

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>

                </ContentTemplate>

                <Triggers>
                    <asp:PostBackTrigger ControlID="btnGuardarSeguimientoParalizacion" />
                    <asp:PostBackTrigger ControlID="btnModificarSeguimientoParalizacion" />
                    <asp:PostBackTrigger ControlID="grdSeguimientoParalizacion" />

                    <asp:PostBackTrigger ControlID="imgbtnOficioSeguimiento" />
                    <asp:PostBackTrigger ControlID="lnkbtnInformeSeguimiento" />
                    <asp:PostBackTrigger ControlID="imgbtnOficioSeguimiento" />
                    <asp:PostBackTrigger ControlID="lnkbtnOficioSeguimiento" />
                    <asp:PostBackTrigger ControlID="imgbtnActaSeguimiento" />
                    <asp:PostBackTrigger ControlID="lnkbtnActaSeguimiento" />
                </Triggers>


            </asp:UpdatePanel>

        </div>

        <!-- Modal Informe de Visita-->
        <div class="modal fade" id="modalActaVisita" role="dialog">
            <div class="modal-dialog modal-lg" role="document">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;
                        <asp:UpdatePanel runat="server" ID="Up_tituloRegistroActa" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label runat="server" ID="lblTituloActa" Text="REGISTRO PARA LA GENERACIÓN DEL INFORME DE MONITOREO"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <%--<iframe src="Mantenimiento/EnviarClave.aspx" ></iframe>--%>
                        <asp:UpdatePanel runat="server" ID="Up_RegistroActa" UpdateMode="Conditional">
                            <ContentTemplate>
                                <iframe id="ifrmRegistrarActa" runat="server" width="100%" height="650px"></iframe>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <%--REGISTRO DE ACTA--%>

        <asp:HiddenField ID="hfDetalleParalizacion" runat="server"></asp:HiddenField>
        <asp:ModalPopupExtender ID="MPE_ACTA" runat="server" TargetControlID="hfDetalleParalizacion"
            BackgroundCssClass="modalBackground" OkControlID="ImageButton4" DropShadow="true"
            PopupControlID="Panel_Acta">
        </asp:ModalPopupExtender>

        <asp:Panel ID="Panel_Acta" runat="server" Visible="true" Width="1400px" CssClass="modalPopup">

            <table width="100%" style="background-color: #E2ECF3">
                <tr>
                    <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="label27" CssClass="titulo2" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td align="right" style="width: 26px">
                        <asp:ImageButton ID="ImageButton4" runat="server" AlternateText="Cerrar ventana"
                            ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                            Height="25px" Width="26px" />
                    </td>
                </tr>
            </table>

            <br />
            <asp:Panel ID="Panel12" runat="server" ScrollBars="Vertical" Height="860px" Font-Size="10pt">
                <asp:UpdatePanel runat="server" ID="Up_RegistroActaMPE" UpdateMode="Conditional">
                    <ContentTemplate>
                        <iframe id="ifrmRegistroActa" runat="server" width="100%" height="810px"></iframe>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </asp:Panel>

        <%--MENSAJE DE CONFIRMACIÓN PARA EL REGITRO DEL ACTA DE VISITA--%>

        <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
        <asp:ModalPopupExtender ID="MPE_PREGUNTA" runat="server" TargetControlID="HiddenField1"
            BackgroundCssClass="modalBackground" OkControlID="ImageButton1" DropShadow="true"
            PopupControlID="Panel_Pregunta">
        </asp:ModalPopupExtender>

        <asp:Panel ID="Panel_Pregunta" runat="server" Visible="true" Width="500px" CssClass="modalPopup">

            <table width="100%" style="background-color: #E2ECF3">
                <tr>
                    <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="label2" CssClass="titulo2" runat="server" Font-Bold="true">MENSAJE</asp:Label>
                    </td>
                    <td align="right" style="width: 26px">
                        <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="Cerrar ventana"
                            ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                            Height="25px" Width="26px" />
                    </td>
                </tr>
            </table>

            <br />
            <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical" Height="120px" Font-Size="10pt">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2">Se registró correctamente el seguimiento de la paralización de obra.
                                    <br />
                                    <asp:Label runat="server" Text="" ID="lblMsjRegistroActa"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <asp:Button runat="server" ID="btnSIContinuar" OnClick="btnSIContinuar_Click" Text=" SI " Width="70px" />
                                </td>
                                <td style="text-align: left">
                                    <asp:Button runat="server" ID="btnNOContinuar" OnClick="btnNOContinuar_Click" Text=" NO " Width="70px" />
                                </td>
                            </tr>

                        </table>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </asp:Panel>

    </form>

    <script src="<%= ResolveClientUrl("~/scripts/jquery-1.10.2.min.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/scripts/jquery.jaTextCounter-1.0.js") %>" type="text/javascript"></script>

    <%--<script src="../sources/jquery/jquery.min.js" type="text/javascript"></script>--%>

    <script type="text/javascript" charset="UTF-8">

        function pageLoad() {

            $(function () {
                $("#<% = txtDetalleProblematicaParalizacion.ClientID %>").TextCounter(2000);
                $("#<% = txtDiagnostico.ClientID %>").TextCounter(3000);
            })
        }

        function ShowPopupRegistroActaVisita() {
            $("#btnShowPopupActaParalizada").click();
        }

        /**
        Funcion para abrir una nueva ventana con captura de evento close y efectuar un trigger a su ventana padre
        */
        function windowOpener(urlFicha) {
            var url = urlFicha;
            newWindow = window.open(url, "Popup", "width=1500,height=900,scrollbars=yes");
            var timer = setInterval(function () {
                if (newWindow.closed) {
                    $('.modal-btn', window.parent.document).trigger("click");
                    clearInterval(timer);
                }
            }, 250)
        }

        function GuardarArchivos() {
            alert("si paso papu")
        }

    </script>
    <script src="../sources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>


