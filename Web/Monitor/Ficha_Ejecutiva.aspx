﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ficha_Ejecutiva.aspx.cs" Inherits="Web.Monitor.Ficha_Ejecutiva" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>FICHA EJECUTIVA</title>
    <link href="../sources/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="div_Ficha" style="width: 960px; height: auto; margin:auto">
                <center>

                    <table style="margin: auto; margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td rowspan="2">
                                <img src="../img/logo_ministerio.jpg" height="50" alt="" style="padding-right: 15px; padding-left: 15px;" />
                            </td>

                        </tr>

                    </table>

         
                    <br />
                   

                </center>

                <div style="width: 100%" id="idDivInformacion">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2" class="table-dark text-center">
                                    <asp:Label runat="server" ID="lblTitle" Font-Bold="true" Text="FICHA EJECUTIVA">
                                    </asp:Label>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-center">
                                    <asp:Label runat="server" Font-Size="11pt" Text="" ID="lblFechaAyuda"></asp:Label>
                                    <div id="icon_word">
                                        <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" Width="25px" Height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                                        <asp:ImageButton ID="imgbtnExportarMobile" runat="server" ImageUrl="~/img/download_48.png" Width="90%" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />
                                        <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" Width="25px" Height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr runat="server" visible="false">
                                <th>C&Oacute;DIGO SNIP</th>
                                <td><b>
                                    <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label></b></td>
                            </tr>
                            <tr>
                                <th style="width: 26%;">C&Oacute;DIGO &Uacute;NICO</th>
                                <td class="celdaTabla" style="border: 1px dotted gray;">
                                    <asp:Label ID="lblUnico" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <th>NOMBRE PROYECTO</th>
                                <td>
                                    <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr runat="server" id="trMonTotalAyudaMemoria">
                                <th class="celdaTabla" style="border: 1px dotted gray;">MONTO DE INVERSI&Oacute;N TOTAL</th>
                                <td>

                                    <asp:Label ID="lblMoneda_PMIB" runat="server" Text="S/."></asp:Label>
                                    <asp:Label ID="lblMontoInversionTotalMEF" runat="server" Text=""></asp:Label>
                                    <asp:Label runat="server" ID="lblMsjMontoInversion" Text=""></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <th>BENEFICIARIOS</th>
                                <td>
                                    <asp:Label ID="lblBeneficiario" runat="server" Text=""></asp:Label>
                                    <%--<asp:Label ID="lblhabitantes" runat="server" Text="Habitantes (Fuente Formato SNIP 03)"></asp:Label>--%>

                                </td>
                            </tr>
                            <tr runat="server" id="trAportesAyudaMemoria">
                                <td class="celdaTabla" style="border: 1px dotted gray;">ESTRUCTURA DE FINANCIAMIENTO</td>
                                <td>
                                    <div>
                                        <table cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td align="center">MVCS :</td>
                                                <td style="width: 80px;">
                                                    <asp:Label ID="lblPorcentajeMontoMVCS" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                </td>
                                                <td>Monto : S/.</td>
                                                <td>
                                                    <asp:Label ID="lblMontoMVCSTransferido" runat="server" Text="0" Width="90px"></asp:Label>
                                                    <asp:TextBox ID="txtMontoMVCSTransferido" Visible="false" Style="text-align: center" Width="80px" runat="server" Text="0"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">Cof. Firmantes : </td>
                                                <td>
                                                    <asp:Label ID="lblPorcentajeMontoFirmantes" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                </td>
                                                <td>Monto : S/.</td>
                                                <td>
                                                    <asp:Label ID="lblMontoFirmantes" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">TOTAL<br />
                                                    APORTES :</td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="(100%) "></asp:Label>

                                                </td>
                                                <td>Monto : S/.</td>
                                                <td>
                                                    <asp:Label ID="lblTotalInversionTab0" runat="server" Text="0"></asp:Label>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>UNIDAD EJECUTORA INVERSIONES</th>
                                <td>
                                    <asp:Label ID="lblEjecutora" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr runat="server" id="trEtapaFinanciamiento" visible="false">
                                <td>ETAPA</td>
                                <td>
                                    <asp:Label ID="lblNomEtapa" runat="server" Text=""></asp:Label></td>
                            </tr>

                            <tr>
                                <th>MONTO VIABLE</th>
                                <td>
                                    <asp:Label ID="lblMoneda" runat="server" Text="S/."></asp:Label>
                                    <asp:Label ID="lblMontoSnip" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblMontoConvenio" runat="server" Text="" Visible="false"></asp:Label>
                                </td>
                            </tr>

                            <tr runat="server" id="trTransferenciasAyudaMemoria" visible="false">
                                <td>TRANSFERENCIAS</td>
                                <td>
                                    <asp:Label ID="lblMsjTransferencia" runat="server" Text="No hubo transferencia hasta el momento."></asp:Label><br />
                                </td>
                            </tr>

                            <tr runat="server" id="tr3" visible="false">
                                <td>ASIGNACIÓN A TRAVÉS DE PIA</td>
                                <td>
                                    <asp:Label ID="LblAsignacionPIA" runat="server" Text="No hubo asignación."></asp:Label><br />
                                </td>
                            </tr>

                            <tr runat="server" visible="false">
                                <td colspan="2" class="celdaTabla" style="border: 1px dotted gray;">
                                    <asp:Panel runat="server" ID="PanelTransferencia">
                                        <label style="font-weight: bold;">Detalle del financiamiento:</label>
                                        <br />
                                        <br />

                                        <asp:GridView runat="server" ID="xgrdTransferencias" EmptyDataText="No hay registro."
                                            ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                            CellPadding="2" CellSpacing="2" Width="70%" Font-Size="14px" CssClass="center">
                                            <Columns>
                                                <asp:TemplateField HeaderText="DISPOSITIVO DE TRANSFERENCIA Y/O ASIGNACIÓN MEDIANTE PIA" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEntidad" Text='<%# Eval("dispositivo_aprobacion") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="strfecha_aprobacion" HeaderText="FECHA">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="montoAprobacion" HeaderText="MONTO TRANSFERIDO (S/.) (Según DS)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="MontoReservaContingencia" HeaderText="MONTO RESERVA CONTINGENCIA (S/.)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>

                                            </Columns>
                                            <HeaderStyle Height="13px" Font-Names="Calibri" Font-Size="13px" />
                                            <RowStyle Font-Size="12px" Font-Names="Calibri" />
                                            <EditRowStyle BackColor="#FFFFB7" />
                                        </asp:GridView>
                                        <br />
                                    </asp:Panel>

                                    <div style="text-align: left; font-family: Calibri; font-size: 13px">

                                        <asp:Label runat="server" ID="lblMsjSosem" Text=""></asp:Label>

                                    </div>
                                    <asp:GridView runat="server" ID="grdDetalleSOSEMTab0" EmptyDataText="No hay registro."
                                        ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                        CellPadding="2" CellSpacing="2" Width="100%" Font-Size="12px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Año" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEntidad" Text='<%# Eval("anio") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="pia" HeaderText="Pia" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="pimAcumulado" HeaderText="Pim. Acumulado" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="devAcumulado" HeaderText="Dev. Acum." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="enero" HeaderText="ENE" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="febrero" HeaderText="FEB" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="marzo" HeaderText="MAR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="abril" HeaderText="ABR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="mayo" HeaderText="MAY" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="junio" HeaderText="JUN" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="julio" HeaderText="JUL" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="agosto" HeaderText="AGO" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="setiembre" HeaderText="SET" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="octubre" HeaderText="OCT" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="noviembre" HeaderText="NOV" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="diciembre" HeaderText="DIC" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle CssClass="GridHeader2" />
                                            </asp:BoundField>

                                        </Columns>
                                        <HeaderStyle Height="15px" Font-Names="Calibri" Font-Size="11px" />
                                        <RowStyle Font-Size="10px" Font-Names="Calibri" />
                                        <EditRowStyle BackColor="#FFFFB7" />
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                            <tr runat="server" visible="false">
                                <td>METAS DEL PROYECTO</td>
                                <td>
                                    <asp:Label runat="server" ID="lblMetas"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th>PROCESO DE SELECI&Oacute;N</th>
                                <td>
                                    <div>
                                        <table class="table table-borderless">
                                            <tr runat="server" id="trd1">
                                                <td style="width:34%">
                                                    <asp:Label runat="server" ID="lblNomContratista" Text="Contratista/Consorcio Obra"></asp:Label>
                                                 
                                                </td>
                                                <td style="width:3%">:</td>
                                                <td class="celdaTablaLeft">
                                                    <asp:Label ID="lblContratista" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="trd2">
                                                <td>
                                                    <asp:Label runat="server" ID="lblNomMontoContrato" Text="Monto Contratado Obra"></asp:Label>
                                                </td>
                                                <td>:</td>
                                                <td>S/.
                                                <asp:Label ID="lblMontoContratado" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr runat="server" id="tr1">
                                                <td>
                                                    <asp:Label runat="server" ID="lblNomNroContrato" Text="Nro Contrato Obra"></asp:Label></td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblNroContratoObra" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr runat="server" id="trd3">
                                                <td>Supervisor de Ejecución</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblEmpresaSupervisora" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblCargoAyudaMemoria" runat="server" Text=""></asp:Label></td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblSupervisor" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr runat="server" id="trd4">
                                                <td>Monto Contratado Supervisión</td>
                                                <td>:</td>
                                                <td>S/.
                                                <asp:Label ID="lblMontoSupervision" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr runat="server" id="tr2">
                                                <td>Nro Contrato Supervisión</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblNroContratoSupervision" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>EJECUCI&Oacute;N</th>
                                <td>
                                    <div>
                                        <table class="table table-borderless">
                                            <tr>
                                                <td style="width:34%">Fecha Inicio Plazo Contractual</td>
                                                <td style="width:3%">:</td>
                                                <td>
                                                    <asp:Label ID="lblFechaInicio1" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Plazo de Ejecución Contractual</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblPlazoEjecucion1" runat="server" Text=""></asp:Label><asp:Label ID="Label1" runat="server" Text=" (Días Calendario)"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Fecha Termino Plazo Contractual</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblFechaTerminoContractual" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>F. Termino + Ampl. + Paraliz.</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblFechaTerminoReal" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Modalidad de Ejecución Presupuestal</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblModalidad1" runat="server" Text=""></asp:Label></td>
                                            </tr>


                                            <tr runat="server" id="trResidente">
                                                <td>Residente Obra</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblResidente" runat="server" Text=""></asp:Label></td>
                                            </tr>

                                            <tr runat="server" id="trInspector">
                                                <td>Inspector Obra</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblInspector" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Avance Físico Acumulado</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblPorcentajeAvance" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Avance Programado Acumulado</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblAvanceProgramado" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Avance Financiero Acumulado</td>
                                                <td>:</td>
                                                <td>
                                                    <asp:Label ID="lblAvanceFinanciero" runat="server" Text=""></asp:Label></td>
                                            </tr>

                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <asp:Label ID="lblNomEstado" runat="server" Text="ESTADO DE LA OBRA"></asp:Label>
                                </th>
                                <td>
                                     <asp:Label ID="lblEstadoSituacional" runat="server" Text=""></asp:Label>
                                </td>
                                </tr>
                               <tr>
                                <th>
                                    SEGUIMIENTO AL ESTADO SITUACIONAL
                                </th>
                                <td>
                                     <div runat="server" id="divDetalle"  >
                                                </div>
                                </td>
                                </tr>

                             <tr>
                                <th>
                                    ÚLTIMAS ACCIONES
                                </th>
                                <td>
                                       <div runat="server" id="divAcciones" >
                                                </div>
                                </td>
                                </tr>

                        </tbody>
                    </table>
                </div>
                 
            </div>
        </div>
    </form>
</body>
</html>
