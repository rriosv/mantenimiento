﻿using System;

namespace Web.Monitor
{
    public partial class Metas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Session["clave"].ToString();

            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();

            ifrm.Attributes["src"] = UrlFrontEnd + "Meta/index/" + token+"?tipo=2";  // TIPO=2 Monitoreo
            ifrm.Attributes["width"] = "100%";
            //ifrm.Attributes["height"] = "1400px";
            ifrm.Attributes["scrolling"] = "yes";
            ifrm.Attributes["frameborder"] = "0";
        }
    }
}