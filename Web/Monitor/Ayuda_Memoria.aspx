﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ayuda_Memoria.aspx.cs" Inherits="Web.Monitor.Ayuda_Memoria"  Culture="es-PE" UICulture="es-PE" Title="AYUDA MEMORIA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    
    <title></title>

    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTablaLeft {
            text-align: left;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        .detalle ul {
            margin-left: -20px;
        }

            .detalle ul li {
                padding-bottom: 10px;
            }

        @media print {
            #imgbtnImprimir, #imgbtnExportar, #imgbtnExportarMobile {
                display: none;
            }
        }
    </style>

    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <center>
            <div id="div_Ficha" style="width: 800px; height: auto;">
                <center>

                    <table style="margin: auto; margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td rowspan="2">
                                <img src="../img/logo_ministerio.jpg" height="50" alt="" style="padding-right: 15px; padding-left: 15px;" />
                            </td>

                        </tr>

                    </table>

                    <span style="font-family: Arial; font-size: 16pt">
                        <asp:Label runat="server" ID="lblTitle" Font-Bold="true" Text="FICHA EJECUTIVA">
                        </asp:Label>
                    </span>
                    <br />
                    <asp:Label runat="server" Font-Size="9pt" Text="" ID="lblFechaAyuda"></asp:Label>
                    <div id="icon_word">
                        <center>
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" Width="25px" Height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                                        <asp:ImageButton ID="imgbtnExportarMobile" runat="server" ImageUrl="~/img/download_48.png" Width="90%" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" Width="25px" Height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />

                                    </td>
                                </tr>
                            </table>
                        </center>

                    </div>

                </center>

                <div style="width: 100%" id="idDivInformacion">
                    <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">

                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray; width: 148px;">C&Oacute;DIGO SNIP</td>
                            <td class="celdaTabla" style="border: 1px dotted gray;"><b>
                                <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label></b></td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray; width: 148px;">C&Oacute;DIGO &Uacute;NICO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray;"><b>
                                <asp:Label ID="lblUnico" runat="server" Text=""></asp:Label></b></td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">NOMBRE PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr runat="server" id="trMonTotalAyudaMemoria">
                            <td class="celdaTabla" style="border: 1px dotted gray;">MONTO DE INVERSI&Oacute;N TOTAL</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">

                                <asp:Label ID="lblMoneda_PMIB" runat="server" Text="S/."></asp:Label>
                                <asp:Label ID="lblMontoInversionTotalMEF" runat="server" Text=""></asp:Label>
                                <asp:Label runat="server" ID="lblMsjMontoInversion" Text=""></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">BENEFICIARIOS</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblBeneficiario" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblhabitantes" runat="server" Text="Habitantes (Fuente Formato SNIP 03)"></asp:Label></td>
                        </tr>
                        <tr runat="server" id="trAportesAyudaMemoria">
                            <td class="celdaTabla" style="border: 1px dotted gray;">ESTRUCTURA DE FINANCIAMIENTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <div>
                                    <table cellpadding="2" cellspacing="0">
                                        <tr>
                                            <td align="center">MVCS :</td>
                                            <td style="width: 80px;">
                                                <asp:Label ID="lblPorcentajeMontoMVCS" Font-Bold="true" runat="server" Text=""></asp:Label>

                                            </td>
                                            <td>Monto : S/.</td>
                                            <td>
                                                <asp:Label ID="lblMontoMVCSTransferido" runat="server" Text="0" Width="90px"></asp:Label>
                                                <asp:TextBox ID="txtMontoMVCSTransferido" Visible="false" Style="text-align: center" Width="80px" runat="server" Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">Cof. Firmantes : </td>
                                            <td>
                                                <asp:Label ID="lblPorcentajeMontoFirmantes" Font-Bold="true" runat="server" Text=""></asp:Label>

                                            </td>
                                            <td>Monto : S/.</td>
                                            <td>
                                                <asp:Label ID="lblMontoFirmantes" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">TOTAL<br />
                                                APORTES :</td>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="(100%) "></asp:Label>

                                            </td>
                                            <td>Monto : S/.</td>
                                            <td>
                                                <asp:Label ID="lblTotalInversionTab0" runat="server" Text="0"></asp:Label>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">UNIDAD EJECUTORA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblEjecutora" runat="server" Text=""></asp:Label></td>
                        </tr>
                           <tr runat="server" id="trEtapaFinanciamiento" visible="false">
                            <td class="celdaTabla" style="border: 1px dotted gray">ETAPA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblNomEtapa" runat="server" Text=""></asp:Label></td>
                        </tr>

                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">MONTO VIABLE DEL PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblMoneda" runat="server" Text="S/."></asp:Label>
                                <asp:Label ID="lblMontoSnip" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblMontoConvenio" runat="server" Text="" Visible="false"></asp:Label>
                            </td>
                        </tr>

                        <tr runat="server" id="trTransferenciasAyudaMemoria">
                            <td class="celdaTabla" style="border: 1px dotted gray">TRANSFERENCIAS</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblMsjTransferencia" runat="server" Text="No hubo transferencia hasta el momento."></asp:Label><br />
                            </td>
                        </tr>

                         <tr runat="server" id="tr3">
                            <td class="celdaTabla" style="border: 1px dotted gray">ASIGNACIÓN A TRAVÉS DE PIA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="LblAsignacionPIA" runat="server" Text="No hubo asignación."></asp:Label><br />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" class="celdaTabla" style="border: 1px dotted gray;" >
                                <asp:Panel runat="server" ID="PanelTransferencia">
                                    <label style="font-weight:bold;">Detalle del financiamiento:</label>
                                    <br /><br />

                                    <asp:GridView runat="server" ID="xgrdTransferencias" EmptyDataText="No hay registro." 
                                    ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                    CellPadding="2" CellSpacing="2" Width="70%" Font-Size="14px" CssClass="center">
                                    <Columns>
                                        <asp:TemplateField HeaderText="DISPOSITIVO DE TRANSFERENCIA Y/O ASIGNACIÓN MEDIANTE PIA" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEntidad" Text='<%# Eval("dispositivo_aprobacion") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle  HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="strfecha_aprobacion" HeaderText="FECHA" >
                                            <HeaderStyle  HorizontalAlign="Center" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="montoAprobacion" HeaderText="MONTO TRANSFERIDO (S/.) (Según DS)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                         
                                           <asp:BoundField DataField="MontoReservaContingencia" HeaderText="MONTO RESERVA CONTINGENCIA (S/.)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>

                                    </Columns>
                                    <HeaderStyle Height="13px" Font-Names="Calibri" Font-Size="13px" />
                                    <RowStyle Font-Size="12px" Font-Names="Calibri" />
                                    <EditRowStyle BackColor="#FFFFB7" />
                                </asp:GridView>
                                    <br />
                                </asp:Panel>

                                <div style="text-align: left; font-family: Calibri; font-size: 13px">

                                    <asp:Label runat="server" ID="lblMsjSosem" Text=""></asp:Label>

                                </div>
                                <asp:GridView runat="server" ID="grdDetalleSOSEMTab0" EmptyDataText="No hay registro."
                                    ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                    CellPadding="2" CellSpacing="2" Width="100%" Font-Size="12px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Año" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEntidad" Text='<%# Eval("anio") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="pia" HeaderText="Pia" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="pimAcumulado" HeaderText="Pim. Acumulado" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="devAcumulado" HeaderText="Dev. Acum." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="enero" HeaderText="ENE" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="febrero" HeaderText="FEB" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="marzo" HeaderText="MAR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="abril" HeaderText="ABR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="mayo" HeaderText="MAY" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="junio" HeaderText="JUN" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="julio" HeaderText="JUL" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="agosto" HeaderText="AGO" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="setiembre" HeaderText="SET" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="octubre" HeaderText="OCT" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="noviembre" HeaderText="NOV" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="diciembre" HeaderText="DIC" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle CssClass="GridHeader2" />
                                        </asp:BoundField>

                                    </Columns>
                                    <HeaderStyle Height="15px" Font-Names="Calibri" Font-Size="11px" />
                                    <RowStyle Font-Size="10px" Font-Names="Calibri" />
                                    <EditRowStyle BackColor="#FFFFB7" />
                                </asp:GridView>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">METAS DEL PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label runat="server" ID="lblMetas"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">PROCESO DE SELECI&Oacute;N</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <div>
                                    <table style="font-family: Calibri; font-size: 13px; width: 100%">
                                        <tr runat="server" id="trd1">
                                            <td><u>
                                                <asp:Label runat="server" ID="lblNomContratista" Text="Contratista/Consorcio Obra"></asp:Label>
                                            </u></td>
                                            <td>:</td>
                                            <td class="celdaTablaLeft">
                                                <asp:Label ID="lblContratista" runat="server" Text=""></asp:Label>
                                                </td>
                                        </tr>
                                        <tr runat="server" id="trd2">
                                            <td><u>
                                                <asp:Label runat="server" ID="lblNomMontoContrato" Text="Monto Contratado Obra"></asp:Label>
                                            </u></td>
                                            <td>:</td>
                                            <td>S/.
                                                <asp:Label ID="lblMontoContratado" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr runat="server" id="tr1">
                                            <td><u>
                                                <asp:Label runat="server" ID="lblNomNroContrato" Text="Nro Contrato Obra"></asp:Label></u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblNroContratoObra" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr runat="server" id="trd3">
                                            <td><u>Supervisor de Ejecución</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblEmpresaSupervisora" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>
                                                <asp:Label ID="lblCargoAyudaMemoria" runat="server" Text=""></asp:Label></u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblSupervisor" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr runat="server" id="trd4">
                                            <td><u>Monto Contratado Supervisión</u></td>
                                            <td>:</td>
                                            <td>S/.
                                                <asp:Label ID="lblMontoSupervision" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr runat="server" id="tr2">
                                            <td><u>Nro Contrato Supervisión</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblNroContratoSupervision" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">DATOS DE SEGUIMIENTO A LA EJECUCI&Oacute;N DE OBRA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <div>
                                    <table style="font-family: Calibri; font-size: 13px; width: 100%">
                                        <tr>
                                            <td><u>Fecha Inicio Plazo Contractual</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblFechaInicio1" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Plazo de Ejecución Contractual</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblPlazoEjecucion1" runat="server" Text=""></asp:Label><asp:Label ID="Label1" runat="server" Text=" (Días Calendario)"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Fecha Termino Plazo Contractual</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblFechaTerminoContractual" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>F. Termino + Ampl. + Paraliz.</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblFechaTerminoReal" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Modalidad de Ejecución Presupuestal</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblModalidad1" runat="server" Text=""></asp:Label></td>
                                        </tr>


                                        <tr runat="server" id="trResidente">
                                            <td><u>Residente Obra</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblResidente" runat="server" Text=""></asp:Label></td>
                                        </tr>

                                        <tr runat="server" id="trInspector">
                                            <td><u>Inspector Obra</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblInspector" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Avance Físico Acumulado</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblPorcentajeAvance" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Avance Programado Acumulado</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblAvanceProgramado" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><u>Avance Financiero Acumulado</u></td>
                                            <td>:</td>
                                            <td>
                                                <asp:Label ID="lblAvanceFinanciero" runat="server" Text=""></asp:Label></td>
                                        </tr>

                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" class="celdaTabla" style="border: 1px dotted gray">
                                <table cellspacing="0" cellpadding="3" style="font-family: Calibri; font-size: 13px; text-size-adjust: none;">
                                    <tr>
                                        <td rowspan="3" class="celdaTabla" style="border-right: 1px dotted gray; width: 60px;">SITUACIÓN ACTUAL</td>
                                        <td class="celdaTabla" style="border-bottom: 1px dotted gray; border-right: 1px dotted gray; width: 80px;">
                                            <asp:Label ID="lblNomEstado" runat="server" Text="ESTADO DE LA OBRA"></asp:Label>
                                        </td>
                                        <td class="celdaTabla" style="border-bottom: 1px dotted gray; border-left: 1px dotted gray;">
                                            <asp:Label ID="lblEstadoSituacional" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="celdaTabla" style="border-bottom: 1px dotted gray; border-right: 1px dotted gray; width: 80px;">DETALLE DEL ESTADO SITUACIONAL</td>
                                        <td style="border-bottom: 1px dotted gray; border-left: 1px dotted gray; font-family: Calibri; font-size: 13px;">
                                            <div runat="server" id="divDetalle" class="detalle" style="font-family: Calibri; font-size: 13px; width: 100%">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="celdaTabla" style="border-right: 1px dotted gray; width: 80px;">ACCIONES</td>
                                        <td style="border-left: 1px dotted gray;">
                                            <div runat="server" id="divAcciones" class="detalle" style="font-family: Calibri !important; font-size: 13px !important;">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <br />
            </div>

        </center>
              
    </form>
</body>
</html>

