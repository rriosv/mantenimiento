﻿using DevExpress.Web;
using Newtonsoft.Json;
using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Banco_Monitoreo : System.Web.UI.Page
    {
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();

        BLUtil _Metodo = new BLUtil();
        BLUtil _objBLUtil = new BLUtil();


        protected void Page_Load(object sender, EventArgs e)
        {

            CargaBanco(0);

            if (!IsPostBack)
            {
                cargaPrograma();
                cargaSubPrograma(1);
                cargaUbigeo(ddlDepa, "1", null, null, null);
                cargaUbigeo(ddl_prov, "2", ddlDepa.SelectedValue, null, null);
                cargaUbigeo(ddl_dist, "3", ddlDepa.SelectedValue, ddl_prov.SelectedValue, null);
                cargaUbigeo(ddl_ccpp, "4", ddlDepa.SelectedValue, ddl_prov.SelectedValue, ddl_dist.SelectedValue);

                cargaUbigeo(ddlDepartamentoTab0, "1", null, null, null);
                cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
                cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
                cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);

            }

            string scriptSelect = "<script>if(typeof(applySelect2)=='function'){applySelect2('.ddl-filter');}</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script5", scriptSelect, false);
        }

        protected void CargaBanco(int tipoCarga)
        {
            int codSubsector = 0;
            codSubsector = Convert.ToInt32(Session["CodSubsector"].ToString());

            if (tipoCarga == 1) //RECARGAR
            {
                List<BE_MON_BANDEJA> ListBandejaMonitor = new List<BE_MON_BANDEJA>();
                ListBandejaMonitor = objBLbandeja.F_spMON_BancoMonitoreo();

                if (codSubsector > 0)
                {
                    ListBandejaMonitor = (from cust in ListBandejaMonitor
                                          where cust.id_sector == codSubsector
                                          select cust).ToList();

                }

                Session["Banco"] = ListBandejaMonitor;

            }
            else
            {
                if (Session["Banco"] == null)
                {
                    List<BE_MON_BANDEJA> ListBandejaMonitor = new List<BE_MON_BANDEJA>();
                    ListBandejaMonitor = objBLbandeja.F_spMON_BancoMonitoreo();
                    if (codSubsector > 0)
                    {
                        ListBandejaMonitor = (from cust in ListBandejaMonitor
                                              where cust.id_sector == codSubsector
                                              select cust).ToList();

                    }
                    Session["Banco"] = ListBandejaMonitor;
                }

            }


            xgrdBanco.DataSource = (List<BE_MON_BANDEJA>)Session["Banco"];
            xgrdBanco.DataBind();
        }

        protected void cargaPrograma()
        {
            List<BE_MON_BANDEJA> ListTP = new List<BE_MON_BANDEJA>();
            ListTP = objBLbandeja.F_spMON_TipoPrograma();

            ddlPrograma.DataSource = ListTP;
            ddlPrograma.DataTextField = "nombre";
            ddlPrograma.DataValueField = "valor";
            ddlPrograma.DataBind();

            int codSubsector = 0;
            codSubsector = Convert.ToInt32(Session["CodSubsector"].ToString());
            if (codSubsector > 0)
            {
                ddlPrograma.SelectedValue = codSubsector.ToString();
                ddlPrograma.Enabled = false;

                //ddlPrograma_SelectedIndexChanged(null, null);
                //if (ddlPrograma.SelectedValue == "3")
                //{
                //    tdSub1.Visible = true;
                //    tdSub2.Visible = true;

                //    //ddlSubPrograma.SelectedValue = "";
                //}
            }

            ddlPrograma.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlProgramaEdit.DataSource = ListTP;
            ddlProgramaEdit.DataTextField = "nombre";
            ddlProgramaEdit.DataValueField = "valor";
            ddlProgramaEdit.DataBind();

        }

        protected void cargaSubPrograma(int pTipo)
        {
            int iPrograma = 0;

            if (pTipo == 1) //Registro
            {
                if (ddlPrograma.SelectedValue != "")
                {
                    iPrograma = Convert.ToInt32(ddlPrograma.SelectedValue);
                }
                ddlSubPrograma.DataSource = objBLbandeja.F_spMON_TipoSubPrograma(iPrograma); //SOLO PNSR Y PNSU TIENE SUB PROGRAMAS 
                ddlSubPrograma.DataTextField = "nombre";
                ddlSubPrograma.DataValueField = "valor";
                ddlSubPrograma.DataBind();

                ddlSubPrograma.Items.Insert(0, new ListItem("-Seleccione-", "0"));
            }
            else if (pTipo == 2) //Update
            {
                if (ddlProgramaEdit.SelectedValue != "")
                {
                    iPrograma = Convert.ToInt32(ddlProgramaEdit.SelectedValue);
                }
                ddlSubProgramaEdit.DataSource = objBLbandeja.F_spMON_TipoSubPrograma(iPrograma); //SOLO PNSR Y PNSU TIENE SUB PROGRAMAS 
                ddlSubProgramaEdit.DataTextField = "nombre";
                ddlSubProgramaEdit.DataValueField = "valor";
                ddlSubProgramaEdit.DataBind();
                ddlSubProgramaEdit.Items.Insert(0, new ListItem("-Seleccione-", "0"));
            }
        }
        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("- SELECCIONAR -", ""));

        }
        protected void btnNuevoRegistro_Click(object sender, EventArgs e)
        {
            btnNuevoValidar.Visible = false;
            btnmef.Visible = true;
            txtBuscarCodigo.Enabled = true;

            TxtSnipConsultado.Enabled = false;
            txtUnificado.Enabled = false;
            txtproyecto.Enabled = false;

            TxtSnipConsultado.Text = "";
            txtUnificado.Text = "";

            //trLista.Visible = false;
            //rbFlagReconstruccion.ClearSelection();

            //btnNuevoRegistro.Visible = false;
            //btnRegresar.Visible = true;


            //CargarDS();
            cargarGrillasvacias();
            limpiar();
            //MpNuevoProyecto.Show();

            lnkbtnFichaMef.Visible = false;

            string script = "<script>$('#modalNuevoProyecto').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script99", script, false);

        }
             
        protected void btnValidarMef_Click(object sender, EventArgs e)
        {
            try
            {
                VerificarSNIP();

                string script;
                if (txtBuscarCodigo.Text != "")
                {
                    if (txtBuscarCodigo.Text.Equals("0"))
                    {
                        TxtSnipConsultado.Text = "0";
                        txtUnificado.Text = "0";
                        txtproyecto.Enabled = true;

                        lblbtnMef.Text = "El proyecto aún no tiene código Invierte.pe. Cuando el proyecto tenga código de proyecto debe actualizarlo.";
                        lblbtnMef.Visible = true;

                        lnkbtnFichaMef.Visible = false;
                    }
                    else
                    {
                        string pJsonProy;

                        if (txtBuscarCodigo.Text.Length == 7)
                        {
                            pJsonProy = getHtmlPage(ConfigurationManager.AppSettings["RootSosemRest"], "{\"id\":\"" + txtBuscarCodigo.Text + "\",\"tipo\":\"SIAF\"}");
                        }
                        else
                        {
                            pJsonProy = getHtmlPage(ConfigurationManager.AppSettings["RootSosemRest"], "{\"id\":\"" + txtBuscarCodigo.Text + "\",\"tipo\":\"SNIP\"}");
                        }
                        
                        dynamic vDynData = JsonConvert.DeserializeObject(pJsonProy);
                   
                        if (vDynData.Count >0)
                        {
                            TxtSnipConsultado.Enabled = false;
                            txtUnificado.Enabled = false;
                            txtproyecto.Enabled = false;
                            lblbtnMef.Visible = false;
                            btnNuevoValidar.Visible = true;
                            btnmef.Visible = false;
                            txtBuscarCodigo.Enabled = false;

                            TxtSnipConsultado.Text = vDynData[0]["COD_SNIP"];
                            txtUnificado.Text = vDynData[0]["CODIGO_UNICO"];
                            //txtBuscarCodigo.Text = proyecto.CodigoSNIP.ToString();
                            txtejecutora.Text = vDynData[0]["DES_UNIDAD_UEI"];
                            txtPoblacion.Text = vDynData[0]["BENEFICIARIO"];
                            txtmonto_snip.Text = vDynData[0]["MTO_VIABLE"];
                            txtproyecto.Text = vDynData[0]["NOMBRE_INVERSION"];

                            if (vDynData[0]["TIPO_FORMATO"] == "PROYECTO DE INVERSION")
                            {
                                ddlTipoInversion.SelectedValue = "1";
                            }
                            else
                            {
                                ddlTipoInversion.SelectedValue = "";
                            }

                            lnkbtnFichaMef.Visible = true;
                        }
                        else
                        {
                            //lblbtnMef.Visible = true;
                            //btnNuevoValidar.Visible = false;

                            lblbtnMef.Text = "El código de proyecto no fue encontrado en el Banco del MEF. Verificar.";
                            lblbtnMef.Visible = true;
                            lnkbtnFichaMef.Visible = false;
                            //TxtSnipConsultado.Text = txtsnip.Text;
                            //txtUnificado.Text = "0";

                            //TxtSnipConsultado.Enabled = true;
                            //txtUnificado.Enabled = true;
                            //txtproyecto.Enabled = true;
                        }

                        //if (DatosProyectos.Length > 0)
                        //{
                        //    TxtSnipConsultado.Enabled = false;
                        //    txtUnificado.Enabled = false;
                        //    txtproyecto.Enabled = false;

                        //    oProyecto proyecto = new oProyecto();
                        //    oUbigeo ubigeo = new oUbigeo();
                        //    // oMeta meta = new oMeta();

                        //    int ubigeos;

                        //    btnmef.Visible = false;
                        //    txtBuscarCodigo.Enabled = false;
                        //    btnNuevoValidar.Visible = true;

                        //    proyecto = (oProyecto)DatosProyectos.GetValue(0);

                        //    TxtSnipConsultado.Text = proyecto.CodigoSNIP.ToString();
                        //    txtUnificado.Text = proyecto.CodigoUnico.ToString();

                        //    TxtSnipConsultado.Enabled = false;
                        //    txtUnificado.Enabled = false;

                        //    ubigeos = proyecto.Ubigeos.Length;

                        //    txtBuscarCodigo.Text = proyecto.CodigoSNIP.ToString();
                        //    txtejecutora.Text = proyecto.UltEjecutoraPresupuestal;
                        //    txtPoblacion.Text = proyecto.Beneficiarios.ToString("N0");

                        //    txtmonto_snip.Text = proyecto.MontoViable.ToString();
                        //    txtproyecto.Text = proyecto.NombreInversion.ToString();
                        //    txtproyecto.Enabled = false;

                        //    //SE ASIGNA LOS VALORES DEL UBIGEO ASOCIADO AL PROYECTO INGRESADO
                        //    //ESTO SE REPITE TANTAS VECES SE TENGA UBIGEO POR PROYECTO
                        //    ubigeo = (oUbigeo)proyecto.Ubigeos.GetValue(0);

                        //    /* PONER AQUI CODIGO PARA INSERTAR LAS LOCACIONES (DATOS UBIGEO) DEL PROYECTO QUE SE HA REALIZADO LA BUSQUEDA */
                        //    string val;
                        //    DataTable dtdep = _objBLUtil.spSOL_obtieneID_departamento(1, ubigeo.Departamento.ToString(), "", "");
                        //    val = dtdep.Rows[0]["codigo"].ToString();
                        //    dtdep.Clear();

                        //    ddlDepa.SelectedValue = val;

                        //    //ddl_dist.Items.Clear();
                        //    if (ddlDepa.SelectedValue.ToString() != "00")
                        //    {
                        //        ddl_prov.DataSource = _objBLUtil.F_spSOL_Listar_Departamento(5, ddlDepa.SelectedValue.ToString(), null, null);
                        //        ddl_prov.DataValueField = "codigo";
                        //        ddl_prov.DataTextField = "nombre";
                        //        ddl_prov.DataBind();
                        //    }

                        //    dtdep = _objBLUtil.spSOL_obtieneID_departamento(2, ddlDepa.SelectedValue, ubigeo.Provincia.ToString(), "");
                        //    string prov = dtdep.Rows[0]["codigo"].ToString();
                        //    dtdep.Clear();

                        //    ddl_prov.SelectedValue = prov;

                        //    if (ddl_prov.SelectedValue.ToString() != "00")
                        //    {
                        //        ddl_dist.DataSource = _objBLUtil.F_spSOL_Listar_Departamento(6, ddlDepa.SelectedValue.ToString(), ddl_prov.SelectedValue.ToString(), null);
                        //        ddl_dist.DataValueField = "codigo";
                        //        ddl_dist.DataTextField = "nombre";
                        //        ddl_dist.DataBind();
                        //    }

                        //    dtdep = _objBLUtil.spSOL_obtieneID_departamento(3, ddlDepa.SelectedValue, ddl_prov.SelectedValue, ubigeo.Distrito.ToString());
                        //    string dist = dtdep.Rows[0]["codigo"].ToString();
                        //    ddl_dist.SelectedValue = dist;

                        //    cargaUbigeo(ddl_ccpp, "4", ddlDepa.SelectedValue, ddl_prov.SelectedValue, ddl_dist.SelectedValue);

                        //    lblbtnMef.Visible = false;
                        //    btnNuevoValidar.Visible = true;
                        //}
                        //else
                        //{
                        //    //lblbtnMef.Visible = true;
                        //    //btnNuevoValidar.Visible = false;

                        //    lblbtnMef.Text = "El código de proyecto no fue encontrado en el Banco del MEF. Verificar.";
                        //    lblbtnMef.Visible = true;

                        //    //TxtSnipConsultado.Text = txtsnip.Text;
                        //    //txtUnificado.Text = "0";

                        //    //TxtSnipConsultado.Enabled = true;
                        //    //txtUnificado.Enabled = true;
                        //    //txtproyecto.Enabled = true;
                        //}
                    }
                }
                else
                {
                    script = "<script>alert('Ingresar Snip');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
            catch (Exception ex)
            {
                string script = "<script>alert('No se puede obtener información del MEF, intentelo luego. Error : ' " + ex.Message + ");</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        }
           
        public string getHtmlPage(string pRuta, string pData)
        {
            string vRespuesta = "";
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pRuta);
            request.Proxy = null;
            request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                request.Method = "POST";
                request.ContentType = "application/json;charset=\"utf-8\"";
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.WriteLine(pData);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                vRespuesta = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                vRespuesta = "{\"id\":\"\",\"state\":0,\"msg\":\"" + ex.Message + "\"}"; // stringUtils.armarJsonError(0, "ER", ex.Message);
            }
            return vRespuesta;
        }
        protected void ddlDepa_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_prov, "2", ddlDepa.SelectedValue, null, null);
            cargaUbigeo(ddl_dist, "3", ddlDepa.SelectedValue, ddl_prov.SelectedValue, null);
            cargaUbigeo(ddl_ccpp, "4", ddlDepa.SelectedValue, ddl_prov.SelectedValue, ddl_dist.SelectedValue);
        }
        protected void ddl_prov_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_dist, "3", ddlDepa.SelectedValue, ddl_prov.SelectedValue, null);
            cargaUbigeo(ddl_ccpp, "4", ddlDepa.SelectedValue, ddl_prov.SelectedValue, ddl_dist.SelectedValue);
        }

        protected void ddl_dist_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_ccpp, "4", ddlDepa.SelectedValue, ddl_prov.SelectedValue, ddl_dist.SelectedValue);
        }

        protected Boolean ValidadRegistroBanco()
        {
            Boolean result;
            result = true;
            string Mensaje = "";
            if (txtproyecto.Text.Trim() == "")
            {
                Mensaje = Mensaje + "*Ingrese una descripción del proyexto.\\n";
                result = false;
            }

            if (ddlTipoInversion.SelectedValue.ToString() == "")
            {
                Mensaje = Mensaje + "*Seleccionar tipo de inversión y/o intervención.\\n";
                result = false;
            }

            if (ddlFinanciamiento.SelectedIndex == 0)
            {
                Mensaje = Mensaje + "*Seleccione el tipo de proyecto.\\n";
                result = false;
            }
            else if (grdSnipHistorial.Rows.Count > 0)
            {
               
                if (ddlFinanciamiento.SelectedValue.Equals("2"))
                {
                    int countError = 0;
                    foreach (GridViewRow row in grdSnipHistorial.Rows)
                    {
                        Label lblIdFinanciamiento = (Label)row.FindControl("lblIdFinanciamiento");
                        Label lblIdEstado = (Label)row.FindControl("lblIdEstado");

                        //PREINVERSION SIN CONCLUIR
                        if (lblIdFinanciamiento.Text.Equals("1") && !lblIdEstado.Text.Equals("16"))
                        {
                            countError = countError + 1;
                        }
                    }
                    if (countError > 0)
                    {
                        Mensaje = Mensaje + "*La Etapa anterior (PREINVERSION) debe estar CONCLUIDO antes de registrar la etapa actual (EXPEDIENTE TÉCNICO).\\n";
                        result = false;
                    }
                }
                else if (ddlFinanciamiento.SelectedValue.Equals("3"))
                {
                    int countError = 0;
                    foreach (GridViewRow row in grdSnipHistorial.Rows)
                    {
                        Label lblIdFinanciamiento = (Label)row.FindControl("lblIdFinanciamiento");
                        Label lblIdEstado = (Label)row.FindControl("lblIdEstado");

                        //EXPEDIENTE SIN CONCLUIR
                        if (lblIdFinanciamiento.Text.Equals("2") && !lblIdEstado.Text.Equals("22"))
                        {
                            countError = countError + 1;
                        }
                    }
                    if (countError > 0)
                    {
                        Mensaje = Mensaje + "*La Etapa anterior (EXPEDIENTE TÉCNICO) debe estar CONCLUIDO antes de registrar la etapa actual (OBRA).\\n";
                        result = false;
                    }
                }

                //Validamos que no permita registro de etapas de financiamiento menores a los registrados
                int flagMenor = 0;
                foreach (GridViewRow row in grdSnipHistorial.Rows)
                {
                    Label lblIdFinanciamiento = (Label)row.FindControl("lblIdFinanciamiento");
                    Label lblIdEstado = (Label)row.FindControl("lblIdEstado");

                    if ( Convert.ToInt32(lblIdFinanciamiento.Text)>Convert.ToInt32(ddlFinanciamiento.SelectedValue))
                    {
                        flagMenor = flagMenor + 1;
                    }
                }
                if (flagMenor > 0)
                {
                    Mensaje = Mensaje + "*No puede registrar una etapa de financiamiento ("+ddlFinanciamiento.SelectedItem.Text+") menor a la que ya existe registrado en el Sistema.\\n";
                    result = false;
                }
            }


            if (ddlPrograma.SelectedIndex == 0)
            {
                Mensaje = Mensaje + "*Seleccione el tipo de programa.\\n";
                result = false;
            }


            if (rbFlagReconstruccion.SelectedItem == null)
            {
                Mensaje = Mensaje + "*Seleccione si pertenece o no a la reconstrucción.\\n";
                result = false;
            }

            if (rbFlagServicio.SelectedItem == null)
            {
                Mensaje = Mensaje + "*Seleccione si es o no un servicio.\\n";
                result = false;
            }

            if (rbFlagEmergencia.SelectedItem == null)
            {
                Mensaje = Mensaje + "*Seleccione si es o no un proyecto de emergencia.\\n";
                result = false;
            }
            //else if (rbFlagEmergencia.SelectedValue == "1" & CboDSEmergencia.SelectedValue == "")
            //{
            //    Mensaje = Mensaje + "*Si es proyecto de emergencia debe seleccionar un DS.\\n";
            //    result = false;
            //}


            if (ddlPeriodo.SelectedIndex == 0)
            {
                Mensaje = Mensaje + "*Seleccione el periodo.\\n";
                result = false;
            }

            if (ddlModalidadEjecucion.SelectedValue == "")
            {
                Mensaje = Mensaje + "*Seleccione la modalidad de financiamiento.\\n";
                result = false;
            }

            if (GvUbigeoCrear.Rows.Count == 0)
            {
                Mensaje = Mensaje + "*Ingrese al menos una ubicación.\\n";
                result = false;
            }

            if (ddlambito.SelectedIndex == 0)
            {
                Mensaje = Mensaje + "*Seleccione el ambito.\\n";
                result = false;
            }

            if (txtejecutora.Text.Trim() == "")
            {
                Mensaje = Mensaje + "*Ingrese la unidad ejecutora.\\n";
                result = false;
            }

            if (txtPoblacion.Text.Trim() == "")
            {
                Mensaje = Mensaje + "*Ingrese la población.\\n";
                result = false;
            }

            if (txtmonto_snip.Text.Trim() == "")
            {
                Mensaje = Mensaje + "*Ingrese el monto SNIP.\\n";
                result = false;
            }

            if (txtFechaAprobacion.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaAprobacion.Text) == false)
                {
                    Mensaje = Mensaje + "Formato invalido de Fecha de aprobación de Expediente Técnico. El formato debe ser dd/mm/yyyy.\\n";
                    result = false;
                }

                if (Convert.ToDateTime(txtFechaAprobacion.Text) > DateTime.Now)
                {
                    Mensaje = Mensaje + "La fecha de aprobación debe ser menor a la fecha de hoy.\\n";
                    result = false;
                }


            }

            if (TxtCodigoIdea.Text.Trim() != "") {
                if (IsNumeric(TxtCodigoIdea.Text.Trim()) == false) {
                    Mensaje = Mensaje + "Ingrese un  código de idea válido.\\n";
                    result = false;
                }
                else if (IsNumeric(TxtCodigoIdea.Text.Trim()) == true & Convert.ToInt64(TxtCodigoIdea.Text) == 0)
                {
                    Mensaje = Mensaje + "Codigo de Idea debe ser diferente de cero.\\n";
                    result = false;
                }
            }

            if (Mensaje.Trim() != "")
            {
                string script = "<script>alert('" + Mensaje + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script777", script, false);
            }


            return result;

        }

        public static Boolean IsNumeric(string valor)
        {
            int result;
            return int.TryParse(valor, out result);
        }

        protected void btnGrabarBanco_Click(object sender, EventArgs e)
        {
            if (ValidadRegistroBanco())
            {
                tdSub1.Visible = true;
                tdSub2.Visible = true;
                tdSubPrograma1.Visible = true;
                tdSubPrograma2.Visible = true;
                TrAprobacionExpediente.Visible = true;

                _BE_Bandeja.snip = TxtSnipConsultado.Text;
                _BE_Bandeja.sector = ddlPrograma.SelectedValue;
                _BE_Bandeja.Id_tipo = Convert.ToInt32(ddlSubPrograma.SelectedValue);
                //_BE_Bandeja.depa = ddlDepa.SelectedValue;
                //_BE_Bandeja.prov = ddl_prov.SelectedValue;
                //_BE_Bandeja.dist = ddl_dist.SelectedValue;
                //_BE_Bandeja.CCPP = ddl_ccpp.SelectedValue.Substring(6, 4);
                _BE_Bandeja.depa = "";
                _BE_Bandeja.prov = "";
                _BE_Bandeja.dist = "";
                _BE_Bandeja.CCPP = "";
                _BE_Bandeja.unidadEjecutora = txtejecutora.Text;
                _BE_Bandeja.nombProyecto = txtproyecto.Text;
                _BE_Bandeja.monto_SNIP = txtmonto_snip.Text;
                _BE_Bandeja.Date_fecha = VerificaFecha(txtFechaAprobacion.Text);
                _BE_Bandeja.ambito = Convert.ToInt32(ddlambito.SelectedValue);
                _BE_Bandeja.poblacionSnip = Convert.ToInt32(txtPoblacion.Text);
                _BE_Bandeja.tipoFinanciamiento = Convert.ToInt32(ddlFinanciamiento.SelectedValue);
                _BE_Bandeja.id_tipoModalidad = Convert.ToInt32(ddlModalidadEjecucion.SelectedValue);
                _BE_Bandeja.Anio = ddlPeriodo.SelectedValue;
                _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                _BE_Bandeja.flagReconstruccion = (rbFlagReconstruccion.SelectedValue.Equals("1") ? true : false);
                _BE_Bandeja.flagServicio = (rbFlagServicio.SelectedValue.Equals("1") ? true : false);
                _BE_Bandeja.flagEmergencia = Convert.ToInt32(rbFlagEmergencia.SelectedValue);
                //if (rbFlagEmergencia.SelectedValue == "1")
                //{
                //    _BE_Bandeja.id_declaratoria = Convert.ToInt32(CboDSEmergencia.SelectedValue);
                //    _BE_Bandeja.Declaratoria = CboDSEmergencia.SelectedItem.Text;
                //}
                //else {
                    _BE_Bandeja.id_declaratoria = null;
                    _BE_Bandeja.Declaratoria = "";
                //}


                if (txtUnificado.Text.Trim() != "")
                    _BE_Bandeja.CodigoUnificado = Convert.ToInt32(txtUnificado.Text);

                if (TxtCodigoIdea.Text.Trim() != "")
                    _BE_Bandeja.CodigoIdea = Convert.ToInt32(TxtCodigoIdea.Text);

                //llenar los ubigeos
                List<Ubigeos> ListaUbigeos = new List<Ubigeos>();
                foreach (GridViewRow GVR in GvUbigeoCrear.Rows)
                {
                    Ubigeos UBI = new Ubigeos();
                    string codigo = GvUbigeoCrear.DataKeys[GVR.RowIndex]["codigo"].ToString();
                    string ubigeoCCPP = GvUbigeoCrear.DataKeys[GVR.RowIndex]["ubigeoCCPP"].ToString();

                    UBI.id_proyecto = 0;
                    UBI.CCPP = (ubigeoCCPP.Length > 6 ? ubigeoCCPP : codigo);
                    UBI.id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                    UBI.flagActivo = 1;
                    ListaUbigeos.Add(UBI);
                }
                _BE_Bandeja.ListaUbigeos = ListaUbigeos;

                //llenar los codigos de seace
                List<CodigosSeace> ListaCodigosSeace = new List<CodigosSeace>();
                foreach (GridViewRow GVR in GvCodigoSeace.Rows)
                {
                    CodigosSeace CSE = new CodigosSeace();
                    CSE.id_proyecto = 0;
                    CSE.Codigo = Convert.ToInt32(HttpUtility.HtmlDecode(GVR.Cells[0].Text));
                    CSE.Activo = true;
                    ListaCodigosSeace.Add(CSE);
                }
                _BE_Bandeja.ListaCodigosSeace = ListaCodigosSeace;

                //llenar los codigos de seace
                List<CodigosARCC> ListaCodigosArcc = new List<CodigosARCC>();
                foreach (GridViewRow GVR in grdCodigoARCC.Rows)
                {
                    CodigosARCC CSE = new CodigosARCC();
                    CSE.id_proyecto = 0;
                    CSE.Codigo = HttpUtility.HtmlDecode(GVR.Cells[0].Text);
                    CSE.Activo = true;
                    ListaCodigosArcc.Add(CSE);
                }
                _BE_Bandeja.ListaCodigosARCC = ListaCodigosArcc;

                _BE_Bandeja.idTipoInversion = Convert.ToInt32(ddlTipoInversion.SelectedValue);

                int val = objBLbandeja.I_spi_MON_RegistrarProyecto(_BE_Bandeja);

                if (val == 1)
                {
                    string script = "<script>$('#modalNuevoProyecto').modal('hide');alert('Se registro correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //trLista.Visible = true;

                    CargaBanco(1);
                    btnNuevoRegistro.Visible = true;
                    //btnRegresar.Visible = false;
                    limpiar();
                    //MpNuevoProyecto.Hide();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        public void limpiar()
        {
            txtBuscarCodigo.Text = "";
            if (ddlPrograma.Enabled == true)
            {
                ddlPrograma.SelectedValue = "";
            }
            //tdSub1.Visible = false;
            ddlDepa.SelectedValue = "";
            //ddl_prov.SelectedValue = "";
            //ddl_dist.SelectedValue = "";
            txtejecutora.Text = "";
            txtproyecto.Text = "";
            txtmonto_snip.Text = "";
            txtFechaAprobacion.Text = "";
            txtPoblacion.Text = "";
            ddlPeriodo.SelectedValue = "";
            ddlModalidadEjecucion.SelectedValue = "";
            ddlambito.SelectedValue = "0";
            lblAlerta.Text = "";
        }
      
        protected void VerificarSNIP()
        {
            if (txtBuscarCodigo.Text != "0")
            {
                List<BE_MON_BANDEJA> ListBandejaMonitor = new List<BE_MON_BANDEJA>();

                //List<BE_MON_BANDEJA> ListBandejaMonitor = new List<BE_MON_BANDEJA>();
                ListBandejaMonitor = objBLbandeja.F_spMON_BancoMonitoreo();

                //ListBandejaMonitor = (List<BE_MON_BANDEJA>)Session["Banco"];

                ListBandejaMonitor = (from cust in ListBandejaMonitor
                                      where (cust.snip == txtBuscarCodigo.Text || cust.CodigoUnificado==Convert.ToInt32(txtBuscarCodigo.Text))
                                      select cust
                                       ).ToList();
                grdSnipHistorial.DataSource = ListBandejaMonitor;
                grdSnipHistorial.DataBind();

                if (ListBandejaMonitor.Count > 0)
                {
                    //lblAlerta.ForeColor = System.Drawing.Color.Red;
                    lblAlerta.Text = "<div class='alert alert-warning'><h5><i class='fas fa-exclamation-triangle'></i><b>&nbsp;ADVERTENCIA!</b></h5> Ya existe " + ListBandejaMonitor.Count.ToString() + " proyecto(s) registrado en el banco de monitoreo, verificar. </br></br>";

                    lblAlerta.Text = lblAlerta.Text + "<table class='tablaMsj'><tr><th>PERIODO</th><th>SNIP</th><th>UNIFICADO</th><th>NOMBRE DE PROYECTO</th><th>TIPO</th><th>ESTADO</th></tr>";

                    foreach (BE_MON_BANDEJA item in ListBandejaMonitor)
                    {
                        lblAlerta.Text = lblAlerta.Text + "<tr><td style='text-align:center'>" + item.Anio + "</td><td>" + item.snip + "</td><td>" + item.CodigoUnificado + "</td><td>" + item.nombProyecto + "</td><td style='text-align:center'>" + item.tipo + "</td><td style='text-align:center'>" + item.Estado + "</td></tr>";
                    }

                    lblAlerta.Text = lblAlerta.Text + "</table></br>";

                    lblAlerta.Text = lblAlerta.Text + " En caso de ser una nueva etapa agregar al final del nombre del proyecto, si todo es correcto puede continuar con el registro.</div>";
                }
                else
                {
                    lblAlerta.Text = "<div class='alert alert-success'><h5><i class='fas fa-check'></i><b>&nbsp;INFORMACIÓN!</b></h5> El proyecto no se encuentra registrado, puede continuar con el registro.</br></div>";
                    //lblAlerta.ForeColor = System.Drawing.Color.Blue;
                }

            }
            else
            {
                lblAlerta.Text = "";
            }
        }
      
        protected void btnActualizarBanco_Click(object sender, EventArgs e)
        {
            string mensaje = "";
            Boolean validado = true;

            if (txtNombreEdit.Text.Trim() == "")
            {
                mensaje = mensaje + "-Ingrese un nombre de proyecto.\\n";
                validado = false;
            }

            if (ddlSubProgramaEdit.Visible == true && (ddlSubProgramaEdit.SelectedIndex == -1 | ddlSubProgramaEdit.SelectedIndex == 0))
            {
                mensaje = mensaje + "-Ingrese sub-programa.\\n";
                validado = false;
            }

            if (ddlTipoInversionEdit.SelectedItem == null)
            {
                mensaje = mensaje + "-Seleccionar tipo de inversión y/o intervención.\\n\\n";
                validado = false;
            }

            if (rbFlagReconstruccionEdit.SelectedItem == null)
            {
                mensaje = mensaje + "-Ingrese si pertenece o no a la reconstrucción.\\n";
                validado = false;
            }

            if (rbFlagServicioEdit.SelectedItem == null)
            {
                mensaje = mensaje + "-Ingrese si es o no un servicio.\\n";
                validado = false;
            }

            if (rbFlagEmergenciaEdit.SelectedItem == null)
            {
                mensaje = mensaje + "-Ingrese si es o no una emergencia.\\n";
                validado = false;
            }

            if (txtUnidadEjecutoraEdit.Text.Trim() == "")
            {
                mensaje = mensaje + "-Ingrese la unidad ejecutora.\\n";
                validado = false;
            }

            if (txtPoblacionEdit.Text.Trim() == "")
            {
                mensaje = mensaje + "-Ingrese la población.\\n";
                validado = false;
            }

            if (ddlAmbitoEdit.SelectedIndex == 0 | ddlAmbitoEdit.SelectedIndex == -1)
            {
                mensaje = mensaje + "-Seleccione el ámbito.\\n";
                validado = false;
            }

            if (ddlPeriodoEdit.SelectedIndex == 0 | ddlPeriodoEdit.SelectedIndex == -1)
            {
                mensaje = mensaje + "-Seleccione el periodo.\\n";
                validado = false;
            }


            if (mensaje.Trim() != "")
            {
                string script = "<script>alert('" + mensaje + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script778", script, false);
            }

            if (validado)
            {
                _BE_Bandeja.id_proyecto = Convert.ToInt32(lblIdProyecto.Text);
                _BE_Bandeja.nombProyecto = txtNombreEdit.Text;
                _BE_Bandeja.unidadEjecutora = txtUnidadEjecutoraEdit.Text;
                _BE_Bandeja.poblacionSnip = Convert.ToInt32(txtPoblacionEdit.Text);
                _BE_Bandeja.ambito = Convert.ToInt32(ddlAmbitoEdit.SelectedValue);
                _BE_Bandeja.Anio = ddlPeriodoEdit.SelectedValue;
                //_BE_Bandeja.flagReconstruccion = ChkProyectoReconstruccion2.Checked;
                _BE_Bandeja.flagReconstruccion = (rbFlagReconstruccionEdit.SelectedValue.Equals("1") ? true : false);
                _BE_Bandeja.flagServicio = (rbFlagServicioEdit.SelectedValue.Equals("1") ? true : false);
                _BE_Bandeja.flagEmergencia = Convert.ToInt32(rbFlagEmergenciaEdit.SelectedValue);

                int pIdSubPRogram = 0;

                if (tdSubPrograma2.Visible == false)
                {
                    pIdSubPRogram = 0;
                }
                else
                {
                    pIdSubPRogram = Convert.ToInt32(ddlSubProgramaEdit.SelectedValue);
                }

                _BE_Bandeja.id_subPrograma = pIdSubPRogram;
                _BE_Bandeja.idTipoInversion = Convert.ToInt32(ddlTipoInversionEdit.SelectedValue);
                _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = objBLbandeja.U_MON_ProyectoBanco(_BE_Bandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaBanco(1);

                    Up_ModalidaEjecucion.Update();
                    //MPE_InformacionGeneral.Hide();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }

        }

        protected void grdUbicacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdUbicacion.DataKeys[CurrentRow.RowIndex].Value as object;

                _BE_Bandeja.id_proyecto = Convert.ToInt32(lblIdProyecto.Text);
                _BE_Bandeja.flagActivo = 0;
                _BE_Bandeja.CCPP = ((Label)CurrentRow.FindControl("lblUbigeo")).Text + ((Label)CurrentRow.FindControl("lblUbigeoCCPP")).Text;
                _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = objBLbandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BE_Bandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaUbigeosProyectos(Convert.ToInt32(lblIdProyecto.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_ModalidaEjecucion.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarUbicacion_Click(object sender, ImageClickEventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = true;
            //btnModificarUbicacionMetasTab0.Visible = false;
            lblNomUsuarioUbigeoTab0.Text = "";
            Up_ModalidaEjecucion.Update();
        }

        protected void ddlDepartamentoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_ModalidaEjecucion.Update();
        }

        protected void ddlProvinciaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_ModalidaEjecucion.Update();
        }

        protected void ddlDistritoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_ModalidaEjecucion.Update();
        }

        protected void btnGuardarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            _BE_Bandeja.flagActivo = 1;
            _BE_Bandeja.id_proyecto = Convert.ToInt32(lblIdProyecto.Text);

            if (ddlCCPPTab0.SelectedValue.Equals(""))
            {
                _BE_Bandeja.CCPP = ddlDepartamentoTab0.SelectedValue + ddlProvinciaTab0.SelectedValue + ddlDistritoTab0.SelectedValue;
            }
            else
            {
                _BE_Bandeja.CCPP = ddlCCPPTab0.SelectedValue;
            }

            _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

            int val = objBLbandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BE_Bandeja);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                CargaUbigeosProyectos(Convert.ToInt32(lblIdProyecto.Text));
                Panel_UbicacionMetasTab0.Visible = false;
                Up_ModalidaEjecucion.Update();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        }

        protected void btnCancelarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = false;

        }

        protected void CargaUbigeosProyectos(int idProyecto)
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = objBLbandeja.F_spMON_Ubigeo(idProyecto);
            grdUbicacion.DataSource = ListUbigeos;
            grdUbicacion.DataBind();

        }
        protected void GvCodigoSeace_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = GvCodigoSeace.DataKeys[CurrentRow.RowIndex].Value as object;

                DataTable dtG = new DataTable();
                dtG.Columns.Add("codigo", typeof(string));

                foreach (GridViewRow GVR in GvCodigoSeace.Rows)
                {
                    if (HttpUtility.HtmlDecode(GVR.Cells[0].Text) != objTemp.ToString())
                    {
                        DataRow DR = dtG.NewRow();
                        DR["codigo"] = HttpUtility.HtmlDecode(GVR.Cells[0].Text);
                        dtG.Rows.Add(DR);
                    }
                }

                trSeace.Visible = false;
                GvCodigoSeace.DataSource = dtG;
                GvCodigoSeace.DataBind();

            }

        }

        protected void BtnAgregarUbigeo_Click(object sender, EventArgs e)
        {
            if (ddlDepa.SelectedValue == "" | ddl_prov.SelectedValue == "" | ddl_dist.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione mínimo departamente, provincia y distrito');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script11", script, false);
                return;
            }

            DataTable dtubigeos = new DataTable();
            dtubigeos.Columns.Add("codigo", typeof(string));
            dtubigeos.Columns.Add("depa", typeof(string));
            dtubigeos.Columns.Add("prov", typeof(string));
            dtubigeos.Columns.Add("dist", typeof(string));
            dtubigeos.Columns.Add("ubigeoCCPP", typeof(string));
            dtubigeos.Columns.Add("CCPP", typeof(string));
                        
            bool existe = false;
            foreach (GridViewRow GVR in GvUbigeoCrear.Rows)
            {
                string codigo = GvUbigeoCrear.DataKeys[GVR.RowIndex]["codigo"].ToString();
                string ubigeoCCPP = GvUbigeoCrear.DataKeys[GVR.RowIndex]["ubigeoCCPP"].ToString();

                DataRow DR = dtubigeos.NewRow();
                DR["depa"] = HttpUtility.HtmlDecode(GVR.Cells[1].Text);
                DR["prov"] = HttpUtility.HtmlDecode(GVR.Cells[2].Text);
                DR["dist"] = HttpUtility.HtmlDecode(GVR.Cells[3].Text);
                DR["CCPP"] = HttpUtility.HtmlDecode(GVR.Cells[5].Text);
                DR["codigo"] = codigo;
                DR["ubigeoCCPP"] = ubigeoCCPP;
                dtubigeos.Rows.Add(DR);

                if (codigo + ubigeoCCPP == ddlDepa.SelectedValue + ddl_prov.SelectedValue + ddl_dist.SelectedValue + ddl_ccpp.SelectedValue)
                    existe = true;
            }
            if (existe == false)
            {
                DataRow DR2 = dtubigeos.NewRow();
                DR2["depa"] = HttpUtility.HtmlDecode(ddlDepa.SelectedItem.Text);
                DR2["prov"] = HttpUtility.HtmlDecode(ddl_prov.SelectedItem.Text);
                DR2["dist"] = HttpUtility.HtmlDecode(ddl_dist.SelectedItem.Text);

                if (ddl_ccpp.SelectedValue != "")
                    DR2["CCPP"] = HttpUtility.HtmlDecode(ddl_ccpp.SelectedItem.Text);

                else
                    DR2["CCPP"] = "";

                DR2["codigo"] = ddlDepa.SelectedValue + ddl_prov.SelectedValue + ddl_dist.SelectedValue;
                DR2["ubigeoCCPP"] = ddl_ccpp.SelectedValue;
                dtubigeos.Rows.Add(DR2);
            }

            trUbigeo.Visible = false;
            GvUbigeoCrear.DataSource = dtubigeos;
            GvUbigeoCrear.DataBind();
        }

        void cargarGrillasvacias()
        {

            DataTable dtubigeos = new DataTable();
            dtubigeos.Columns.Add("depa", typeof(string));
            dtubigeos.Columns.Add("prov", typeof(string));
            dtubigeos.Columns.Add("dist", typeof(string));
            dtubigeos.Columns.Add("CCPP", typeof(string));
            dtubigeos.Columns.Add("codigo", typeof(string));
            dtubigeos.Columns.Add("ubigeoCCPP", typeof(string));

            DataTable dtcodigosceace = new DataTable();
            dtcodigosceace.Columns.Add("codigo", typeof(string));

            GvCodigoSeace.DataSource = dtcodigosceace;
            GvCodigoSeace.DataBind();
            GvUbigeoCrear.DataSource = dtubigeos;
            GvUbigeoCrear.DataBind();

            DataTable dtcodigoarcc = new DataTable();
            dtcodigoarcc.Columns.Add("codigo", typeof(string));

            grdCodigoARCC.DataSource = dtcodigoarcc;
            grdCodigoARCC.DataBind();
        }

        protected void BtnAgregarCodigoSeace_Click(object sender, EventArgs e)
        {
            if (TxtCodigoSeace.Text.Trim() == "")
            {
                string script = "<script>alert('Ingrese añ menos un código');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script11", script, false);
                return;
            }

            DataTable dtcodigosceace = new DataTable();
            dtcodigosceace.Columns.Add("codigo", typeof(string));

            bool existe = false;
            foreach (GridViewRow GVR in GvCodigoSeace.Rows)
            {
                DataRow DR = dtcodigosceace.NewRow();
                DR["codigo"] = HttpUtility.HtmlDecode(GVR.Cells[0].Text);
                dtcodigosceace.Rows.Add(DR);

                if (HttpUtility.HtmlDecode(GVR.Cells[0].Text) == TxtCodigoSeace.Text)
                    existe = true;
            }
            if (existe == false)
            {
                DataRow DR2 = dtcodigosceace.NewRow();
                DR2["codigo"] = TxtCodigoSeace.Text;
                dtcodigosceace.Rows.Add(DR2);
            }

            trSeace.Visible = false;

            GvCodigoSeace.DataSource = dtcodigosceace;
            GvCodigoSeace.DataBind();
        }

        protected void ddlFinanciamiento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinanciamiento.SelectedIndex != -1)
            {
                if (ddlFinanciamiento.SelectedValue == "3")
                    TrAprobacionExpediente.Visible = true;
                else
                {
                    txtFechaAprobacion.Text = "";
                    TrAprobacionExpediente.Visible = false;
                }
            }
            //CargaModalidadxPrograma(ddlPrograma.SelectedValue, ddlFinanciamiento.SelectedValue);
        }

        protected void btnNuevoValidar_Click(object sender, EventArgs e)
        {
            btnNuevoValidar.Visible = false;
            btnmef.Visible = true;
            TxtSnipConsultado.Enabled = false;
            txtUnificado.Enabled = false;
            txtproyecto.Enabled = false;
            TxtSnipConsultado.Text = "";
            txtUnificado.Text = "";

            txtBuscarCodigo.Enabled = true;
            limpiar();
        }

        protected void lnkbtnFichaMef_Click(object sender, EventArgs e)
        {
            string script;
            if (TxtSnipConsultado.Text != "")
            {
                if (TxtSnipConsultado.Text.Length == 7)
                {
                    script = "<script>window.open('http://ofi5.mef.gob.pe/invierte/formato/verProyectoCU/" + TxtSnipConsultado.Text + "','_blank','left=220,top=80,height=600,width=800,menubar=yes,location=no,status=yes,toolbar=no,resizable=yes,address=yes,scrollbars=yes');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script3", script, false);
                }
                else
                {
                    //script = "<script>window.open('http://ofi5.mef.gob.pe/invierte/formato/verFichaSNIP/" + TxtSnipConsultado.Text + "/0/0','_blank','left=220,top=80,height=600,width=800,menubar=yes,location=no,status=yes,toolbar=no,resizable=yes,address=yes,scrollbars=yes');</script>";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    string script2 = "<script>window.open('http://ofi5.mef.gob.pe/invierte/formato/verFichaSNIP/" + TxtSnipConsultado.Text + "/0/0','_blank') </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script2", script2, false);
                }
            }
            else
            {
                script = "<script>alert('Ingresar Snip');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script4", script, false);
            }
        }
                    
        protected void btnMostrarUbigeo_Click(object sender, EventArgs e)
        {
            if (trUbigeo.Visible == true)
            {
                trUbigeo.Visible = false;
            }
            else
            {
                trUbigeo.Visible = true;
            }
        
        }

        protected void btnMostrarSeace_Click(object sender, EventArgs e)
        {
            if (trSeace.Visible == true)
            {
                trSeace.Visible = false;
            }
            else
            {
                trSeace.Visible = true;
            }
        }

        protected void grdCodigoARCC_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdCodigoARCC.DataKeys[CurrentRow.RowIndex].Value as object;

                DataTable dtG = new DataTable();
                dtG.Columns.Add("codigo", typeof(string));
                
                foreach (GridViewRow GVR in grdCodigoARCC.Rows)
                {
                    if (HttpUtility.HtmlDecode(GVR.Cells[0].Text) != objTemp.ToString())
                    {
                        DataRow DR = dtG.NewRow();
                        DR["codigo"] = HttpUtility.HtmlDecode(GVR.Cells[0].Text);
                        dtG.Rows.Add(DR);
                    }
                }

                grdCodigoARCC.DataSource = dtG;
                grdCodigoARCC.DataBind();

            }

        }

        protected void btnMostrarARCC_Click(object sender, EventArgs e)
        {
            if (trARCC.Visible == true)
            {
                trARCC.Visible = false;
            }
            else
            {
                trARCC.Visible = true;
            }

        }

        protected void btnAgregarCodigoARCC_Click(object sender, EventArgs e)
        {
            if (txtCodigoArcc.Text.Trim() == "")
            {
                string script = "<script>alert('Ingrese al menos un código');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script11", script, false);
                return;
            }

            DataTable dtcodigoArcc = new DataTable();
            dtcodigoArcc.Columns.Add("codigo", typeof(string));

            bool existe = false;
            foreach (GridViewRow GVR in grdCodigoARCC.Rows)
            {
                DataRow DR = dtcodigoArcc.NewRow();
                DR["codigo"] = HttpUtility.HtmlDecode(GVR.Cells[0].Text);
                dtcodigoArcc.Rows.Add(DR);

                if (HttpUtility.HtmlDecode(GVR.Cells[0].Text) == txtCodigoArcc.Text)
                    existe = true;
            }
            if (existe == false)
            {
                DataRow DR2 = dtcodigoArcc.NewRow();
                DR2["codigo"] = txtCodigoArcc.Text;
                dtcodigoArcc.Rows.Add(DR2);
            }
            trARCC.Visible = false;
            grdCodigoARCC.DataSource = dtcodigoArcc;
            grdCodigoARCC.DataBind();
        }

        protected void GvUbigeoCrear_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                //object objTemp = GvUbigeoCrear.DataKeys[CurrentRow.RowIndex].Value as object;
                string codigo = GvUbigeoCrear.DataKeys[CurrentRow.RowIndex]["codigo"].ToString();
                string ubigeoCCPP = GvUbigeoCrear.DataKeys[CurrentRow.RowIndex]["ubigeoCCPP"].ToString();

                DataTable dtubigeos = new DataTable();
                dtubigeos.Columns.Add("depa", typeof(string));
                dtubigeos.Columns.Add("prov", typeof(string));
                dtubigeos.Columns.Add("dist", typeof(string));
                dtubigeos.Columns.Add("CCPP", typeof(string));
                dtubigeos.Columns.Add("codigo", typeof(string));
                dtubigeos.Columns.Add("ubigeoCCPP", typeof(string));


                foreach (GridViewRow GVR in GvUbigeoCrear.Rows)
                {
                    if (!(HttpUtility.HtmlDecode(GVR.Cells[0].Text) == codigo.ToString() && HttpUtility.HtmlDecode(GVR.Cells[4].Text) == ubigeoCCPP.ToString()))
                    {
                        DataRow DR = dtubigeos.NewRow();
                        DR["codigo"] = HttpUtility.HtmlDecode(GVR.Cells[0].Text);
                        DR["depa"] = HttpUtility.HtmlDecode(GVR.Cells[1].Text);
                        DR["prov"] = HttpUtility.HtmlDecode(GVR.Cells[2].Text);
                        DR["dist"] = HttpUtility.HtmlDecode(GVR.Cells[3].Text);
                        DR["ubigeoCCPP"] = HttpUtility.HtmlDecode(GVR.Cells[4].Text);
                        DR["CCPP"] = HttpUtility.HtmlDecode(GVR.Cells[5].Text);

                        dtubigeos.Rows.Add(DR);
                    }
                }

                GvUbigeoCrear.DataSource = dtubigeos;
                GvUbigeoCrear.DataBind();

            }

        }

        protected void lnkbtnEdit_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewDataItemTemplateContainer row;
            boton = (LinkButton)sender;
            row = (GridViewDataItemTemplateContainer)boton.NamingContainer;

            string id = row.KeyValue.ToString();

            lblIdProyecto.Text = id;

            _BE_Bandeja = (BE_MON_BANDEJA)xgrdBanco.GetRow(row.ItemIndex);

            txtSNIPEdit.Text = _BE_Bandeja.snip;
            txtNombreEdit.Text = _BE_Bandeja.nombProyecto;
            txtUnidadEjecutoraEdit.Text = _BE_Bandeja.unidadEjecutora;
            txtPoblacionEdit.Text = _BE_Bandeja.poblacionSnip.ToString();
            ddlPeriodoEdit.SelectedValue = _BE_Bandeja.Anio;
            ddlProgramaEdit.SelectedValue = _BE_Bandeja.id_sector.ToString();
            ddlProgramaEdit.Enabled = false; //No puede editar

            ddlAmbitoEdit.SelectedValue = _BE_Bandeja.ambito.ToString();
            //ChkProyectoReconstruccion2.Checked = _BE_Bandeja.flagReconstruccion;

            if (_BE_Bandeja.idTipoInversion == 0)
            {
                ddlTipoInversionEdit.SelectedValue = "";
            }
            else
            {
                ddlTipoInversionEdit.SelectedValue = _BE_Bandeja.idTipoInversion.ToString();
            }


            if (_BE_Bandeja.flagReconstruccion == true)
            {
                rbFlagReconstruccionEdit.SelectedValue = "1";
            }
            else
            {
                rbFlagReconstruccionEdit.SelectedValue = "0";
            }

            if (_BE_Bandeja.flagServicio == true)
            {
                rbFlagServicioEdit.SelectedValue = "1";
            }
            else
            {
                rbFlagServicioEdit.SelectedValue = "0";
            }

            if (_BE_Bandeja.flagEmergencia == 1)
            {
                rbFlagEmergenciaEdit.SelectedValue = "1";
            }
            else
            {
                rbFlagEmergenciaEdit.SelectedValue = "0";
            }
            //rbFlagReconstruccionEdit.SelectedValue= _BE_Bandeja.flagReconstruccion.ToString();
            //rbFlagServicioEdit.SelectedValue = _BE_Bandeja.flagServicio.ToString();

            CargaUbigeosProyectos(_BE_Bandeja.id_proyecto);

            string pIdPrograma = _BE_Bandeja.id_sector.ToString();

            string pIdSubPrograma = _BE_Bandeja.id_subPrograma.ToString();


            if (pIdPrograma == "1" || pIdPrograma == "3" || pIdPrograma == "10" || pIdPrograma == "7")
            {
                tdSubPrograma1.Visible = true;
                tdSubPrograma2.Visible = true;

                cargaSubPrograma(2);

                try
                {
                    ddlSubProgramaEdit.SelectedValue = pIdSubPrograma;
                }
                catch
                {
                    ddlSubProgramaEdit.SelectedValue = "0";
                }
            }
            else
            {
                tdSubPrograma1.Visible = false;
                tdSubPrograma2.Visible = false;

                try
                {
                    ddlSubProgramaEdit.SelectedValue = "0";
                }
                catch { }
            }

            Up_ModalidaEjecucion.Update();
            //MPE_InformacionGeneral.Show();

            string script = "<script>$('#modalUpdateProyecto').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script98", script, false);
        }
    }
}