﻿using System;

namespace Web.Monitor
{
    public partial class Mapa_Riesgo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Session["clave"].ToString();

            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();

            ifrm.Attributes["src"] = UrlFrontEnd + "mapa/mriesgo/Index/" + token;
            ifrm.Attributes["width"] = "100%";
            //ifrm.Attributes["height"] = "100%";
            //ifrm.Attributes["scrolling"] = "no";
            ifrm.Attributes["scrolling"] = "yes";
            ifrm.Attributes["frameborder"] = "0";
        }
    }
}