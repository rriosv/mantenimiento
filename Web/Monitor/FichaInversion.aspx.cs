﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace Web.Monitor
{
    public partial class FichaInversion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            BL_MON_Liquidacion obj = new BL_MON_Liquidacion();
            DataSet ds = new DataSet();

            string idProyecto = (Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;

            ds = obj.spMON_FichaPriorizados(idProyecto);


            if (ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                lblCodUnificado.Text = dt.Rows[0]["iCodUnificado"].ToString();
                lblNombreInversion.Text = dt.Rows[0]["nom_proyecto"].ToString();
                lblBeneficiarios.Text = dt.Rows[0]["num_hab_snip"].ToString();
                lblUnidadEjecutora.Text = dt.Rows[0]["entidad_ejec"].ToString();
                lblModalidadEjecucion.Text = dt.Rows[0]["modalidad"].ToString();
                lblEstado.Text = dt.Rows[0]["Estado"].ToString();
                lblMontoRDR.Text = dt.Rows[0]["Pim_2019"].ToString();
                if (lblMontoRDR.Text.Length > 0)
                {
                    lblMontoRDR.Text = (Convert.ToDouble(lblMontoRDR.Text)).ToString("N2");
                }
                lblCostoInversion.Text = dt.Rows[0]["CostoInversion"].ToString();
                if (lblCostoInversion.Text.Length > 0)
                {
                    lblCostoInversion.Text = (Convert.ToDouble(lblCostoInversion.Text)).ToString("N2");
                }
                lblFinanAcumulada2018.Text = dt.Rows[0]["Dev_2018"].ToString();
                if (lblFinanAcumulada2018.Text.Length > 0)
                {
                    lblFinanAcumulada2018.Text = (Convert.ToDouble(lblFinanAcumulada2018.Text)).ToString("N2");
                }
                lblPIM2019.Text = dt.Rows[0]["Pim_2019"].ToString();
                if (lblPIM2019.Text.Length > 0)
                {
                    lblPIM2019.Text = (Convert.ToDouble(lblPIM2019.Text)).ToString("N2");
                }
                lblDevengado2019.Text = dt.Rows[0]["Dev_2019"].ToString();
                if (lblDevengado2019.Text.Length > 0)
                {
                    lblDevengado2019.Text = (Convert.ToDouble(lblDevengado2019.Text)).ToString("N2");
                }
                lblAvancePMI2019.Text = dt.Rows[0]["AvancePim2019"].ToString();
                lblAvanceTotalFinan.Text = dt.Rows[0]["AvanceTotal"].ToString();
                lblProyeccionCierre2019.Text = dt.Rows[0]["Proyeccion2019"].ToString();
                if (lblProyeccionCierre2019.Text.Length > 0)
                {
                    lblProyeccionCierre2019.Text = (Convert.ToDouble(lblProyeccionCierre2019.Text)).ToString("N2");
                }
                lblSaldoInversion.Text = dt.Rows[0]["Saldo2019"].ToString();
                if (lblSaldoInversion.Text.Length > 0)
                {
                    lblSaldoInversion.Text = (Convert.ToDouble(lblSaldoInversion.Text)).ToString("N2");
                }
                lblFechaInicio.Text = dt.Rows[0]["fechaInicio"].ToString();
                lblFechaCulminacion.Text = dt.Rows[0]["fechaFinReal"].ToString();
                lblEjecAcum2018.Text = dt.Rows[0]["AcumuladoAl2018"].ToString();
                lblEjec2019.Text = dt.Rows[0]["Ejecutado2019"].ToString();
                lblEjecTotal.Text = dt.Rows[0]["AcumuladoAl2019"].ToString();
                lblEjecProg2019.Text = dt.Rows[0]["ProgramadoAl2019"].ToString();
                lblEstadoAvance.Text = dt.Rows[0]["SubEstado"].ToString();
                lblDescripcion.Text = dt.Rows[0]["Situacion"].ToString();
                lblAcciones.Text = dt.Rows[0]["Acciones"].ToString();
            }
        }
    }
}