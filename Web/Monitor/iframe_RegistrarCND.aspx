﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iframe_RegistrarCND.aspx.cs" Inherits="Web.Monitor.iframe_RegistrarCND" %>

<!DOCTYPE html>

<html lang="es">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="../sources/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <div class="container">
        <form id="form1" runat="server">
            <div>
                <div class="py-3  text-center">
                    <h3>Compromisos de Gases Efecto Invernadero
                    </h3>
                </div><br />

                <div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm">
                            1.

                        </label>

                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">
                            ¿El proyecto considera el uso de energias renovables o generación de energía?

                        </label>
                        <div class="col-sm-2">
                            <asp:RadioButtonList runat="server" ID="rbEnergia" RepeatDirection="Horizontal"  OnSelectedIndexChanged="rbEnergia_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox runat="server" ID="txtEnergiasRenovables" Width="80px" class="form-control" Text="0"></asp:TextBox>
                            <label class="col-form-label col-form-label-sm">Energias Renovables (kWH)</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm">
                            2.
                        </label>

                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">¿El proyecto considera la construcción de una nueva PTAR?</label>
                        <div class="col-sm-2">
                            <asp:RadioButtonList runat="server" ID="rbCarga" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbCarga_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>

                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox runat="server" ID="txtCarga" Width="80px" class="form-control" Text="0" ></asp:TextBox>
                            <label class="col-form-label col-form-label-sm">Carga (kg DBO/día)</label>
                        </div>
                    </div>
                    <div class="form-group row">
                         <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm">
                            3.
                        </label>
                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">¿El proyecto considera digestores de lodos en la PTAR con quema de metano?</label>
                        <div class="col-sm-2">
                            <asp:RadioButtonList runat="server" ID="rbCapacidad" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbCapacidad_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox runat="server" ID="txtCapacidad" Width="80px" class="form-control" Text="0" ></asp:TextBox>
                            <label class="col-form-label col-form-label-sm">Capacidad (m3 de lodo a tratar/día)</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <asp:Label runat="server" ID="lblNomActualizo" class="col-form-label col-form-label-sm"></asp:Label>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <asp:Button runat="server" ID="btnRegistrar" Text="Registrar" CssClass="btn btn-primary btn-block" OnClick="btnRegistrar_Click" />
                        </div>
                    </div>
                </div>

            </div>

            <script src="../sources/jquery/jquery-3.3.1.min.js"></script>
            <script src="../sources/bootstrap-4.3.1/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="../js/pace.min.js"></script>
    
        </form >
                                                                    
    </div>
</body>
</html>
