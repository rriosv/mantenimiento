﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Seace_Cronograma.aspx.cs" Inherits="Web.Monitor.Seace_Cronograma" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SEACE Cronograma</title>
    <link href="../sources/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        body {
            font-family:Calibri;
            font-size:12pt;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin:auto; text-align:center">
            <asp:Label runat="server" Text="ÚLTIMO CRONOGRAMA" Font-Bold="true"></asp:Label>
              <asp:GridView runat="server" ID="grdCronograma" EmptyDataText="No hay registro."
                ShowHeaderWhenEmpty="True"
                AutoGenerateColumns="False"
                CellPadding="2" CellSpacing="2" CssClass="table table-bordered" Width="98%"
              >
                <Columns>
                    <asp:BoundField Visible="true" DataField="item" HeaderText="Nro">
                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField Visible="true" DataField="ETAPA" HeaderText="Etapa">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>

                    <asp:BoundField Visible="true" DataField="FECHA_INICIO" HeaderText="Fecha Inicio">
                        <ItemStyle Width="110px" HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField Visible="true" DataField="FECHA_FIN" HeaderText="Fecha Fin">
                        <ItemStyle Width="110px" HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                  <HeaderStyle BackColor="#337ab7" ForeColor="White" />
            </asp:GridView>

            <asp:Label runat="server" Text="CRONOGRAMAS HISTÓRICOS" Font-Bold="true" ID="lbltitHistorico"></asp:Label>

            <asp:Label runat="server" ID="lblTabla"></asp:Label>
                 <asp:GridView runat="server" ID="grvHistorial" EmptyDataText="No hay registro."
                ShowHeaderWhenEmpty="True"
                AutoGenerateColumns="False"
                CellPadding="2" CellSpacing="2" CssClass="table table-bordered" Width="98%"
              >
                <Columns>
                   <%-- <asp:BoundField Visible="true" DataField="item" HeaderText="Nro">
                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                    </asp:BoundField>--%>

                      <asp:BoundField Visible="true" DataField="VERSION_CRONOGRAMA" HeaderText="Versión">
                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField Visible="true" DataField="ETAPA" HeaderText="Etapa">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>

                    <asp:BoundField Visible="true" DataField="FECHA_INICIO" HeaderText="Fecha Inicio">
                        <ItemStyle Width="115px" HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField Visible="true" DataField="FECHA_FIN" HeaderText="Fecha Fin">
                        <ItemStyle Width="110px" HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                  <HeaderStyle BackColor="#337ab7" ForeColor="White" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
