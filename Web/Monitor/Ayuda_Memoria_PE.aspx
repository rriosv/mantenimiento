﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ayuda_Memoria_PE.aspx.cs" Inherits="Web.Monitor.Ayuda_Memoria_PE" Culture="es-PE" UICulture="es-PE" Title="AYUDA MEMORIA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
   
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        @media print {
            #imgbtnImprimir, #imgbtnExportar, #imgbtnExportarMobile {
                display: none;
            }
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <center>
        <div id="div_Ficha" style="width: 800px; height: auto;">
            <center>
              <%--  <div id="div_imagen">
                    <img src="../img/LogoMVCS1-estado.png" height="55"  alt="" />
                </div>--%>
                            
                <%--<div id="title" style="display: none">
                    <center><span style="font-family: 'Trebuchet MS'; font-size: 9pt"><b>MINISTERIO DE VIVIENDA, CONSTRUCCI&Oacute;N Y SANEAMIENTO</b></span></center>
                </div>--%>
             <table style="margin:auto;margin-top:5px;margin-bottom:6px;" cellpadding="0" cellspacing="0">
                 <tr>
                     <td rowspan="2" >
                         <img src="../img/logo_ministerio.jpg" height="50"  alt="" style="padding-right:15px;padding-left:15px;" />
                     </td>
                  
                 </tr>
                
             </table>
            
                 <span style="font-family: Arial; font-size: 16pt">
                        <asp:Label runat="server" ID="lblTitle" Font-Bold="true" Text="AYUDA MEMORIA">
                        </asp:Label>
                 </span>
                <br />
                <asp:Label runat="server" Font-Size="9pt"  Text="" ID="lblFechaAyuda"></asp:Label>
                <div id="icon_word">
                    <center>
                        <table>
                            <tr>
                            <td>
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                                <asp:ImageButton ID="imgbtnExportarMobile" runat="server" ImageUrl="~/img/download_48.png" width="90%" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />
                            </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                        </table>
                    </center>

                </div>
                <%--<span style="font-family:'Calibri';font-size:11pt"><b><asp:Label ID="lblComponente1" runat="server" Text=""></asp:Label>, <asp:Label ID="lblDistrito1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblProvincia1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblDepartamento1" runat="server" Text=""></asp:Label></b></span>--%>
            </center>
       
            <%-- <div style="width:100%;text-align:left">
            <span style="font-family:'Calibri';font-size:11pt"><b>DATOS GENERALES:</b></span>
        </div>--%>

            <div style="width: 100%">
                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">

                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">NOMBRE PROYECTO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray;">MONTO DE INVERSI&Oacute;N TOTAL</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">

                            <asp:Label ID="lblMontoInversionTotal" runat="server" Text="S/."></asp:Label><asp:Label ID="lblMonto1_PMIB" runat="server" Text=""></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray;">FINANCIAMIENTO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">

                            <asp:Label ID="Label3" runat="server" Text="JAPAN INTERNATIONAL COOPERATION AGENCIA (JICA / EX - JBIC)"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">BENEFICIARIO</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblBeneficiario" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblhabitantes" runat="server" Text="Habitantes"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray;">APORTES</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <div>
                                <table cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="center">MVCS :</td>
                                        <td style="width: 180px;">
                                            <asp:Label ID="lblMontoMVCS" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">JICA : </td>
                                        <td>
                                            <asp:Label ID="lblMontoJica" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">UNIDAD EJECUTORA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblEjecutora" runat="server" Text=""></asp:Label></td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray; width: 250px">UBICACI&Oacute;N</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <div>
                                <table>
                                    <tr>
                                        <td>Departamento :</td>
                                        <td>
                                            <asp:Label ID="lblDepartamento" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Provincia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                                        <td>
                                            <asp:Label ID="lblProvincia" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Distrito&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                                        <td>
                                            <asp:Label ID="lblDistrito" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Localidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                                        <td>
                                            <asp:Label ID="lblLocalidad" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">CONTRATISTA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label runat="server" ID="lblContratista"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">CONSULTOR (Supervisión)</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label runat="server" ID="lblEmpresaSupervisora"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">DATOS DE SEGUIMIENTO A LA EJECUCI&Oacute;N DE OBRA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <div>
                                <table>
                                    <tr runat="server" id="trd2">
                                        <td><u>Monto del Contrato de obra</u></td>
                                        <td>
                                            <asp:Label ID="lblMontoContratado" runat="server" Text=""></asp:Label></td>
                                    </tr>

                                    <tr runat="server" id="trd4" visible="false">
                                        <td><u>Monto Contratado Supervisor</u></td>
                                        <td>
                                            <asp:Label ID="lblMontoSupervision" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><u>Plazo de Ejecución Contractual Final</u></td>
                                        <td>
                                            <asp:Label ID="lblPlazoEjecucion" runat="server" Text=""></asp:Label><asp:Label ID="Label1" runat="server" Text=" (Días Calendario)"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><u>Fecha Entrega de Terreno</u></td>
                                        <td>
                                            <asp:Label ID="lblFechaEntregaTerreno" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><u>Fecha de Inicio</u></td>
                                        <td>
                                            <asp:Label ID="lblFechaInicio1" runat="server" Text=""></asp:Label></td>
                                    </tr>

                                    <tr>
                                        <td><u>Fecha Termino Plazo Contractual</u></td>
                                        <td>
                                            <asp:Label ID="lblFechaTerminoContractual" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><u>Fecha Termino Real</u></td>
                                        <td>
                                            <asp:Label ID="lblFechaTerminoReal" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">RECEPCI&Oacute;N Y LIQUIDACI&Oacute;N DE OBRA</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <div>
                                <table>
                                    <tr>
                                        <td>Certificado de Recepción de Obra</td><td>:</td>
                                        <td>
                                            <asp:Label ID="lblFechaRecepcionObra" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Certificado de Cumplimiento de Obra</td><td>:</td>
                                        <td>
                                            <asp:Label ID="lblFechaCumplimientoObra" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Acta o Certificado de Entrega de Obra para Custoria, Operación y Mantenimiento por la</td><td>:</td>
                                        <td>
                                            EPS: <asp:Label ID="lblUnidadReceptora" runat="server" Text=""></asp:Label><br />
                                            FECHA: <asp:Label ID="lblFechaActaFinal" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Liquidación del Contrato de Obra</td><td>:</td>
                                        <td>
                                            <asp:Label ID="lblLiquidacionContratoObra" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Liquidación del Contrato de Supervisión</td><td>:</td>
                                        <td>
                                            <asp:Label ID="lblLiquidacionContratatoSupervision" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr runat="server" id="trComentarioSupervisor" visible="false">
                                        <td>Nota Supervisión: </td>
                                        <td colspan="2"><asp:Label ID="lblComentarioSupervisor" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                     <tr>
                        <td class="celdaTabla" style="border: 1px dotted gray">SITUACI&Oacute;N ACTUAL</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label runat="server" ID="lblEstado"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
             <br /><div style="vertical-align:top">

                    <asp:Image ID="imgProyecto" BorderStyle="Solid" BorderWidth="4px" Width="350px" BorderColor="#c8130b" runat="server" Visible="false" />
                    <asp:Image ID="imgProyecto2" BorderStyle="Solid" BorderWidth="4px" Width="350px" BorderColor="#c8130b" runat="server" Visible="false" />
                   </div>
                    <br />
            <br />
            
        </div>
    
     </center>
    
    </form>
</body>
</html>
