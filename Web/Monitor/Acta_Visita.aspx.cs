﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class Acta_Visita : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_SOSEM _objBLSOSEM = new BL_MON_SOSEM();
        BE_MON_SOSEM _BESOSEM = new BE_MON_SOSEM();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Conclusion _objBLConclusion = new BL_MON_Conclusion();
        BE_MON_Conclusion _BEConclusion = new BE_MON_Conclusion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();


        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BE_MON_VarPNSU _BEVarPNSU = new BE_MON_VarPNSU();

        BL_MON_Calidad _objBLCalidad = new BL_MON_Calidad();
        BE_MON_Calidad _BECalidad = new BE_MON_Calidad();

        BL_MON_Finan_Transparencia _objBLFinaTra = new BL_MON_Finan_Transparencia();
        BE_MON_Finan_Transparencia _BEFinaTra = new BE_MON_Finan_Transparencia();

        DataTable dt = new DataTable();

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Grabar_Log();
                }
                catch (Exception)
                {
                    //throw;
                }

                // lblFechaAyuda.Text = "("+DateTime.Now.ToString("dd.MM.yyyy") + ")";
            }

            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }

            CargaInformacionGeneral();


        }


        //modificando
        protected void CargaInformacionGeneral()
        {
            DataTable dtVis = new DataTable();
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(Request.QueryString["idE"].ToString());
            dtVis = _objBLEjecucion.spMON_EvaluacionRecomendacionActaVisita(_BEEjecucion);

            //lblObservacionComponentes.Text = dt.Rows[0]["observacionActaVisita"].ToString().ToUpper();
            //lblRecomendacionComponenetes.Text = dt.Rows[0]["recomendacionActaVisita"].ToString();
            lblFechaAyuda.Text = dtVis.Rows[0]["fechaActaVisita"].ToString();

            string iTipoFuncionamiento = dtVis.Rows[0]["id_tipoFuncionamiento"].ToString();
            if (iTipoFuncionamiento.Length > 0)
            {
                chblTipoFuncionamientoActaVisita.SelectedValue = iTipoFuncionamiento;
            }
            
            StringBuilder strEntregaDocumento = new StringBuilder().Append("<div>" + dtVis.Rows[0]["cumplimientoDocumentoActaVisita"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblEntregaDocumentosPMIB.Text = strEntregaDocumento.ToString();

            StringBuilder strComentarioFuncionamiento = new StringBuilder().Append("<div>" + dtVis.Rows[0]["comentarioFuncionamiento"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblComentarioFuncionamiento.Text = strComentarioFuncionamiento.ToString();

            string flagInaugurado = dtVis.Rows[0]["flagInaugurado"].ToString();
            if (flagInaugurado.Length > 0)
            {
                chblFlagInaugurado.SelectedValue = flagInaugurado;
            }

            lblConvenio.Text = dtVis.Rows[0]["Convenio"].ToString();

            StringBuilder strObservacionActa = new StringBuilder().Append("<div>" + dtVis.Rows[0]["observacionActaVisita"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblObservacionComponentes.Text = strObservacionActa.ToString();
            StringBuilder strRecomendacionActa = new StringBuilder().Append("<div>" + dtVis.Rows[0]["recomendacionActaVisita"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblRecomendacionComponenetes.Text = strRecomendacionActa.ToString();

            string caseSwitch = dtVis.Rows[0]["Estado"].ToString();

            switch (caseSwitch)
            {
                case "41"://ACTOS PREVIOS
                    lblEstadoSituacional.Text = dtVis.Rows[0]["estadoGeneral"].ToString();
                    break;
                case "4"://EN EJECUCION
                    if (dtVis.Rows[0]["idSubEstado"].ToString().Equals("87"))
                    {
                        lblEstadoSituacional.Text = "Suspensión       " + "Fecha de Suspensión: " + dtVis.Rows[0]["fechaParalizacion"].ToString()  +  "         Fecha de Reinicio: " + dtVis.Rows[0]["fechaReinicio"].ToString();
                    }
                    else
                    {
                        lblEstadoSituacional.Text = dtVis.Rows[0]["estadoGeneral"].ToString();
                    }
                    break;
                case "5":// PARALIZADA
                    lblEstadoSituacional.Text = "Paralizada       " + "Fecha de Suspensión: " + dtVis.Rows[0]["fechaParalizacion"].ToString() + "         Fecha de Reinicio: " + dtVis.Rows[0]["fechaReinicio"].ToString();
                    break;
                case "6":// CONCLUIDO
                    if (dtVis.Rows[0]["idSubEstado"].ToString().Equals("77")) //Liquidada
                    {
                        lblEstadoSituacional.Text = "Liquidada";
                    }
                    else
                    {
                        lblEstadoSituacional.Text = dtVis.Rows[0]["estadoGeneral"].ToString() + "          Fecha de Culminación de Obra: " + dtVis.Rows[0]["FechaTerminoReal"].ToString() + "         Fecha de Recepción: " + dtVis.Rows[0]["FechaRecepcion"].ToString();
                    }
                    
                    break;
                default:
                    lblEstadoSituacional.Text = dtVis.Rows[0]["estadoGeneral"].ToString();
                    break;
            }

            lblDetalleSituacional.Text ="Comentario: " + dtVis.Rows[0]["detalleSituacional"].ToString();

            //CargarAsuntosTecnicos(Request.QueryString["idE"].ToString());

            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["idPo"].ToString());
            _BELiquidacion.tipoFinanciamiento = 3;
            //_BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());

            dt = _objBLLiquidacion.spMON_Ayuda_Memoria(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {

                lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblMontoConvenio.Text = dt.Rows[0]["MONTO_CONVENIO"].ToString();
                lblEjecutora.Text = dt.Rows[0]["UNIDAD_EJECUTARORA"].ToString();

                lblRegion.Text = dt.Rows[0]["Departamento"].ToString();
                lblProvincia.Text = dt.Rows[0]["Provincia"].ToString();
                lblDistrito.Text = dt.Rows[0]["Distrito"].ToString();
                lblCCPP.Text = dt.Rows[0]["CentroPoblado"].ToString();

                BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
                List<BE_MON_Financiamiento> ListTransferencia = new List<BE_MON_Financiamiento>();
                ListTransferencia = _objBLTransferencia.F_spSOL_TransferenciaByProyecto(Convert.ToInt32(Request.QueryString["idPo"].ToString()));
                xgrdTransferencias.DataSource = ListTransferencia;
                xgrdTransferencias.DataBind();

                lblFechaInicio1.Text = dt.Rows[0]["FECHAINICIO"].ToString();
                lblPlazoEjecucion1.Text = dt.Rows[0]["PLAZOEJECUCION"].ToString();

                
                lblPorcentajeAvance.Text = dt.Rows[0]["FISICOREAL"].ToString();
                lblAvanceProgramado.Text = dt.Rows[0]["FisicoProgramado"].ToString();
                lblAvanceFinanciero.Text = dt.Rows[0]["AvanceFinanciero"].ToString();
                lblAvanceFinanciero.Text = Convert.ToDouble(lblAvanceFinanciero.Text).ToString("N") + "% (Fuente - SOSEM)";

                lblModalidad1.Text = dt.Rows[0]["MODALIDAD"].ToString();

                lblNombreUE.Text = dt.Rows[0]["coordinadorUE"].ToString();
                lblTelefonoUE.Text = dt.Rows[0]["telefonocoordinador"].ToString();
                lblCorreoUE.Text = dt.Rows[0]["correocoordinador"].ToString();

                //Validar si es por contrata

                if (lblModalidad1.Text == "Directa")
                {
                    lblCargoAyudaMemoria.Text = "Ing. Supervisor";
                }
                else if (lblModalidad1.Text == "Por Contrata")
                {
                    lblCargoAyudaMemoria.Text = "Jefe Supervisor";
                }
                else
                {
                    lblCargoAyudaMemoria.Text = "Ing. Supervisor";
                }
                lblContratista.Text = dt.Rows[0]["CONTRATISTA"].ToString();
                lblConsorcio.Text = dt.Rows[0]["CONSORCIO"].ToString();
                lblMontoContratista.Text = dt.Rows[0]["MONTOCONSORCIO"].ToString();
                lblMontoContratista.Text = Convert.ToDouble(lblMontoContratista.Text).ToString("N");

                string idContratacion = dt.Rows[0]["id_tipoContratacion"].ToString();
                if (idContratacion.Length > 0)
                {
                    switch (idContratacion)
                    {
                        case "1":
                            chblistModalidadContratacion.SelectedValue = "1";
                            break;
                        case "2":
                            chblistModalidadContratacion.SelectedValue = "2";
                            break;
                        default:
                            chblistModalidadContratacion.SelectedValue = "3";
                            break;


                    }
                }


                lblEmpresaSupervisor.Text = dt.Rows[0]["EmpresaSupervisor"].ToString();
                lblMontoSupervisor.Text = dt.Rows[0]["MontoSupervisor"].ToString();
                lblMontoSupervisor.Text = Convert.ToDouble(lblMontoSupervisor.Text).ToString("N");
                lblTotalMontoContratado.Text = (Convert.ToDouble(dt.Rows[0]["MontoSupervisor"].ToString()) + Convert.ToDouble(dt.Rows[0]["MONTOCONSORCIO"].ToString())).ToString("N");


                lblSupervisor.Text = dt.Rows[0]["SUPERVISORDESIGNADO"].ToString();
                lblSupervisorTelef.Text = dt.Rows[0]["telefonoSupervisor"].ToString();
                lblSupervisorCorreo.Text = dt.Rows[0]["CorreoSupervisor"].ToString();


                lblResidente.Text = dt.Rows[0]["RESIDENTEOBRA"].ToString();
                lblResidenteTelef.Text = dt.Rows[0]["telefonoResidente"].ToString();
                lblResidenteCorreo.Text = dt.Rows[0]["CorreoResidente"].ToString();
                lblFechaEntregaTerreno.Text = dt.Rows[0]["FechaEntregaTerreno"].ToString();
                lblAmpliacionesPlazo.Text = dt.Rows[0]["TotalAmpliacionPlazo"].ToString();
                lblFechaFinalAmpliacion.Text = dt.Rows[0]["fechaFinAmplParalizacion"].ToString();

                lblFechaAvanceFisico.Text = dt.Rows[0]["FechaFisicoREAL"].ToString();
                lblFechaAvanceProgramado.Text = dt.Rows[0]["FechaFisicoREAL"].ToString();

                lblSosemMontoDevengado.Text = " S/ " + Convert.ToDouble(dt.Rows[0]["DevengadoAcumulado"].ToString()).ToString("N");
                lblSosemFecha.Text = dt.Rows[0]["FechaUpdateMEF"].ToString();

                int tipoReceptora = Convert.ToInt32(dt.Rows[0]["id_tipoReceptora"].ToString());
                switch (tipoReceptora)
                {
                    case 4:
                        chblUnidadReceptora.SelectedValue = "4";
                        break;
                    case 3:
                        chblUnidadReceptora.SelectedValue = "3";
                        break;
                    case 5:
                        chblUnidadReceptora.SelectedValue = "5";
                        break;
                    case 1:
                        chblUnidadReceptora.SelectedValue = "1";
                        break;
                    case 2:
                        chblUnidadReceptora.SelectedValue = "1";
                        break;
                }

                lblComentarioReceptora.Text = dt.Rows[0]["comentarioSostenibilidad"].ToString();

                // CARGA DE COMPONENTES
                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(Request.QueryString["idE"].ToString());
                List<BE_MON_Ejecucion> listComponentes = _objBLEjecucion.F_MON_SistemaDrenajeDetalle(_BEEjecucion);

                string iTipoPrograma = dt.Rows[0]["id_tipoPrograma"].ToString();
                if (iTipoPrograma.Equals("2"))
                {
                    pastillaActa.ImageUrl = "../img/pastillaPMIB.jpg";
                    //trMetasPNSU1.Visible = false;
                    //trMetasPNSU2.Visible = false;

                    //trAsuntosPMIB1.Visible = true;
                    //trAsuntosPMIB2.Visible = true;
                    //trAsuntosPMIB3.Visible = true;
                    //trAsuntosPMIB4.Visible = true;

                    trFuncioamientoPNSU1.Visible = false;
                    trFuncioamientoPNSU2.Visible = false;
                    trFuncioamientoPNSU3.Visible = false;
                    trFuncioamientoPNSU4.Visible = false;
                    trFuncioamientoPNSU5.Visible = false;

                    lblTitAgua.Visible = false;
                    xgrdSistemaAgua.DataSource = listComponentes;
                    xgrdSistemaAgua.DataBind();
                    xgrdSistemaAgua.Visible = true;

                    lblTitConvenio.Visible = true;
                    lblConvenio.Visible = true;
                    lblDetalleSituacional.Visible = true;

                }
                else
                {
                    //PNSU
                    List<BE_MON_Ejecucion> listAgua = (from cust in listComponentes
                                                       where cust.tipoEstado.Equals("Agua Potable")
                                                       select cust).ToList();

                    if (listAgua.Count > 0)
                    {
                        lblTitAgua.Visible = true;
                        xgrdSistemaAgua.DataSource = listAgua;
                        xgrdSistemaAgua.DataBind();
                        xgrdSistemaAgua.Visible = true;
                    }

                    List<BE_MON_Ejecucion> listAlcantarillado = (from cust in listComponentes
                                                                 where cust.tipoEstado.Equals("Alcantarillado, UBS y PTAR")
                                                                 select cust).ToList();

                    if (listAlcantarillado.Count > 0)
                    {
                        lblTitAlcantarillado.Visible = true;
                        xgrdSistemaAlcantarillado.DataSource = listAlcantarillado;
                        xgrdSistemaAlcantarillado.DataBind();
                        xgrdSistemaAlcantarillado.Visible = true;
                    }

                }
            }
        }

        protected void CargarAsuntosTecnicos(string idEvaluacionRecomendacion)
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(idEvaluacionRecomendacion);
            dt = _objBLEjecucion.spMON_RevisionItemAdministrativoVisita(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    TextBox txtCome = (TextBox)FindControl("txtComentario" + row["id_itemAdministrativoVisita"].ToString());
                    RadioButtonList result = (RadioButtonList)FindControl("RadioButtonList" + row["id_itemAdministrativoVisita"].ToString());

                    txtCome.Text = row["comentario"].ToString();
                    if (row["result"].ToString().Equals("0"))
                    {
                        result.ClearSelection();
                    }
                    else
                    {
                        result.SelectedValue = row["result"].ToString();
                    }

                    //else
                    //{
                    //    TextBox txtCome = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtComentario" + i.ToString());
                    //    RadioButtonList result = (RadioButtonList)FindControl("ctl00$ContentPlaceHolder1$RadioButtonList" + i.ToString());
                    //    txtCome.Text = "";
                    //    result.ClearSelection();
                    //}

                }
            }

        }


        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "ActaVisita_SNIP_" + lblSnip.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }
    }
}