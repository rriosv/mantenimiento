﻿<%@ Page Title="Bandeja de Concluidos y Proximos a Terminar" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="Bandeja_Concluidos.aspx.cs" Inherits="Web.Monitor.Bandeja_Concluidos" Culture="es-PE" UICulture="es-PE" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }
                
        .tablaRegistro {
            line-height: 15px;
        }

        .tdFiltro {
            font-weight: bold;
            font-size: 12px;
        }


        .titulo {
            font-size: 14pt;
            font-family: 'DINB';
            color: #0094D4;
        }

        .tituloTipo {
            font-size: 13pt;
            font-family: 'DINB';
            color: #000000;
        }



        .tituloContador {
            font-family: Calibri;
            font-size: 10pt;
        }


        .botonMonitor {
            filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#8C99B9', EndColorStr='#28375B');
            background-color: #28375B;
            color: white;
            font-weight: bold;
            border: 0px;
            border-style: solid;
            /*border-color: #2c5869;*/
            border-color: #4864A4;
            cursor: pointer;
            font-size: 9px;
            padding: 3px;
            border-radius: 8px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: absolute;
        }

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            font-size: 11px;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }

        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }

        .divBody {
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border: 2px solid #006fa7;
            font-family: Calibri;
            padding: 0px 5px 5px 5px;
        }

        .Div_Fondo {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: fixed;
            z-index: 1004;
            top: 0;
            left: 0;
            right: 0;
            bottom: -1px;
            height: auto;
        }

        elemento {
            background-color: black;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            position: absolute;
            left: 5px;
            top: 5px;
            width: 936px;
            height: 736px;
            visibility: visible;
            z-index: 1;
        }

        .user66 {
            position: absolute;
            bottom: 0;
            left: 30px;
        }

        .GridHeader66 {
            filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#8C99B9', EndColorStr='#28375B');
            background-color: #58678B;
            font-weight: bold;
            border: 1px solid #000000;
            color: white;
            padding-left: 3px;
            padding-right: 3px;
            text-align: center;
            line-height: 14px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <center>
        <div class="Grid">
            <br />
            <p>
               
                <span class="titlePage">PROYECTOS CONCLUIDOS Y PRÓXIMOS A TERMINAR</span>
            </p>
            <br />
            <br />

            <asp:UpdatePanel runat="server" ID="Up_Filtro" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 75%; margin:auto" >
                                    <table class="tablaRegistro" style="border-collapse:separate !important; border-spacing: 4px;">
                                        <tr>
                                            <td align="center" class="formHeader">
                                               GESTIÓN
                                            </td>
                                            <td align="center" class="formHeader">
                                               SNIP
                                            </td>
                                            <td align="center" class="formHeader">
                                               PROYECTO
                                            </td>
                                            <td align="center" class="formHeader">
                                                DEPARTAMENTO
                                            </td>
                                            <td align="center" class="formHeader">
                                                PROVINCIA
                                            </td>
                                            <td align="center" class="formHeader">
                                                DISTRITO
                                            </td>
                                            <td align="center" class="formHeader">
                                                PROGRAMA
                                            </td>
                                            <td align="center" class="formHeader">
                                                INAUGURACIÓN
                                            </td>

                                            <td align="center" class="formHeader">
                                                <b></b></td>


                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:DropDownList ID="ddlGestion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGestion_OnSelectedIndexChanged">
                                                    <asp:ListItem Value="" Text="Seleccionar"></asp:ListItem>
                                                    <asp:ListItem Value="2007" Text="2007"></asp:ListItem>
                                                    <asp:ListItem Value="2008" Text="2008"></asp:ListItem>
                                                    <asp:ListItem Value="2009" Text="2009"></asp:ListItem>
                                                    <asp:ListItem Value="2010" Text="2010"></asp:ListItem>
                                                    <asp:ListItem Value="2011" Text="2011"></asp:ListItem>
                                                    <asp:ListItem Value="2012" Text="2012"></asp:ListItem>
                                                    <asp:ListItem Value="2013" Text="2013"></asp:ListItem>
                                                    <asp:ListItem Value="2014" Text="2014"></asp:ListItem>
                                                    <asp:ListItem Value="2015" Text="2015"></asp:ListItem>
                                                    <asp:ListItem Value="2016" Text="2016"></asp:ListItem>
                                                    <asp:ListItem Value="2017" Text="2017"></asp:ListItem>
                                                    <asp:ListItem Value="2018" Text="2018"></asp:ListItem>

                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtSnipFiltro" runat="server" Width="60px"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtProyectoFiltro" runat="server" Width="160px"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:DropDownList ID="ddlDepa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepa_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                <asp:DropDownList ID="ddlprov" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlprov_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                <asp:DropDownList ID="ddldist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddldist_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                <asp:DropDownList ID="ddlSubsector" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubsector_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                <asp:DropDownList ID="ddlInauguración" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlInauguración_SelectedIndexChanged">
                                                    <asp:ListItem Text="-Seleccione-" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="INAUGURADOS" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="POR INAUGURAR" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                <asp:CheckBox ID="ChkboxConcluido" runat="server" Text="Estado Concluido" Font-Bold="false" AutoPostBack="true" OnCheckedChanged="ChkboxConcluido_OnCheckedChanged" />

                                            </td>

                                        </tr>
                                    </table>
                        <br />
                                    <div style="text-align:center">
                                         <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-primary" OnClick="btnBuscar_OnClick" >
                                             <i class="glyphicon glyphicon-search"></i> BUSCAR</asp:LinkButton>
                                        <asp:Button ID="btnLimpiar" runat="server" Width="80px" Text="LIMPIAR" CssClass="btn btn-default" OnClick="btnLimpiar_OnClick" />
                                    </div>
                    </div>
                    <br />

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <br />

        <table width="85%">
            <tr>
                <td align="right">
                    <%--<asp:ImageButton ID="btnReporteResumen" runat="server" OnClick="btnReporteResumen_OnClick" ImageUrl="~/img/EXPORTAR.png" />--%>
                    <asp:LinkButton ID="btnReporte" runat="server" OnClick="btnReporteResumen_OnClick" CssClass="btn btn-verde"><i class="fa fa-file-excel-o"></i> Exportar</asp:LinkButton>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="Up_grdBandeja" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="90%">
                    <tr>
                        <td align="center">
                            <asp:GridView
                                ID="grdBandeja"
                                runat="server"
                                Width="100%"
                                AllowPaging="True"
                                DataKeyNames="id_proyecto"
                                AutoGenerateColumns="False"
                                EmptyDataText="No hay proyectos asignados."
                                EditRowStyle-BackColor="#ffffb7"
                                OnPageIndexChanging="grdBandeja_PageIndexChanging"
                                OnDataBound="grdBandeja_OnDataBound"
                                PageSize="20"
                                CssClass="Grid">
                                <Columns>

                                    <asp:TemplateField HeaderText="PROGRAMA">
                                        <ItemTemplate>
                                            <asp:Label ID="des_subsector" Text='<%# Eval("sector") %>' runat="Server" />
                                            <%-- <asp:Label ID="id_sector" runat="server" Text='<%#Eval("sector") %>' Visible="False" />--%>
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="GESTIÓN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIni_periodo" Text='<%# Eval("Anio") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SNIP" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="cod_snip" Text='<%# Convert.ToInt32(Eval("snip"))==0 ? "NO": Eval("snip") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SNIP">
                                        <ItemTemplate>
                                            <a style="text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000"
                                                href="<%# Eval("snip", "http://ofi2.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo={0}") %>"
                                                target="_blank" title="Hacer clic para ver el proyecto en el MEF">
                                                <%# (Convert.ToInt32(Eval("snip")) == 0 ? "NO" : Eval("snip"))%>
                                            </a>
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DEPARTAMENTO">
                                        <ItemTemplate>
                                            <asp:Label ID="nom_depa" Text='<%# Eval("depa") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PROVINCIA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblprov" runat="server" Text='<%# Eval("prov") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader66" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DISTRITO">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldist" runat="server" Text='<%# Eval("dist") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader66" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="NOMBRE DE PROYECTO">
                                        <ItemTemplate>
                                            <asp:Label ID="nom_proyecto" Text='<%# Eval("nombProyecto") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="UNIDAD EJECUTORA" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="unidad_ejecutora" Text='<%# Eval("unidadEjecutora") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TIPO DE PROYECTO">
                                        <ItemTemplate>
                                            <asp:Label ID="tipoProyecto" Text='<%# Eval("tipoFinanciamientoFiltro") %>' runat="Server" />
                                            <asp:Label ID="lblIdTipoFinanciamiento" Text='<%# Eval("tipoFinanciamiento") %>' runat="Server" Visible="false" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader66" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MONTO SNIP (S/.) ">
                                        <ItemTemplate>
                                            <asp:Label ID="monto_snip" Text='<%# Convert.ToDouble(Eval("monto_SNIP")).ToString("N") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MONTO COMPROMETIDO MVCS (S/.)">
                                        <ItemTemplate>
                                            <asp:Label ID="monto_MVCS" Text='<%# Convert.ToDouble(Eval("costo_MVCS")).ToString("N") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ESTADO EJECUCIÓN">
                                        <ItemTemplate>
                                            <asp:Label ID="tipoEstadoEjecucion" Text='<%# Eval("tipoEstadoEjecucion") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="% AVANCE REAL">
                                        <ItemTemplate>
                                            <asp:Label ID="fisicoEjecutado" Text='<%# Eval("fisicoEjecutado") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FECHA TERMINO CONTRACTUAL">
                                        <ItemTemplate>
                                            <asp:Label ID="fechafin" Text='<%# Eval("strFechaFinContractual") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FECHA TERMINO REAL">
                                        <ItemTemplate>
                                            <asp:Label ID="fechaFinReal" Text='<%# Eval("strFechaFinReal") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="ACTA RECEPCIÓN">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnRecepcion" runat="server" ImageUrl='<%# Eval("imgExtension") %>' ToolTip='<%# Eval("UrlDoc") %>' OnClick="imgbtnRecepcion_OnClick" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="FICHA DE PROYECTO">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnFichaProyecto" runat="server" ImageUrl="~/img/ficha.png" Width="40px" OnClick="imgbtnFichaProyecto_OnClick" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FICHA INAUGURACIÓN">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnRequisito" runat="server" ImageUrl='<%# Eval("imgFicha") %>' Width="42px"  OnClick="imgbtnRequisito_OnClick" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridHeader66" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MAIL" Visible="true">
                                        <ItemTemplate>
                                          <a class="enlace" title="Hacer Clic para Enviar Correo" target="_blank" onclick="abrirVentana('CorreoTecnico.aspx?id=<%#Eval("id_proyecto") %>','_blank',600,500,'no','yes','no','no','no','yes','no')">
                                                   <img src="../img/mail_32x32.png" alt="Enviar correo a responsables." title="Enviar Correo a responsables."  width="25px"/>
                                                </a>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" />

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ID_SOLICITUD">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID_Solicitud" Text='<%# Eval("id") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="GridHeader66" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    

                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                                <SelectedRowStyle CssClass="GridRowSelected" />
                                <HeaderStyle Height="15px" />
                                <AlternatingRowStyle CssClass="GridAlternating" />
                                <EditRowStyle BackColor="#FFFFB7" />
                                <RowStyle CssClass="GridRowNormal" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 33.33%" align="center">
                            <asp:Label ID="LabeltotalAsignado" runat="server" Font-Bold="true" CssClass="tituloContador" /><asp:Label ID="lblPaginadoAsignado" runat="server" Font-Bold="true" CssClass="tituloContador" /></td>

                    </tr>

                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="grdBandeja" />
            </Triggers>
        </asp:UpdatePanel>
    </center>

    <br />
    <br />

    <%-- Requisitos Minimos--%>
    <asp:LinkButton ID="LinkButtonRequisito" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="MPE_Requisitos" runat="server" TargetControlID="LinkButtonRequisito"
        BackgroundCssClass="modalBackground" OkControlID="ImageButton2" DropShadow="true"
        PopupControlID="Panel_Requisitos">
    </asp:ModalPopupExtender>

    <asp:Panel ID="Panel_Requisitos" runat="server" Visible="true" Height="810px" Width="950px"
        CssClass="modalPopup">
        <div class="divBody">
            <table width="100%">
                <tr>
                    <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label8" CssClass="titulo2" runat="server" Font-Bold="true" Font-Underline="true">DATOS PARA INAUGURACIÓN</asp:Label><br />
                        <br />

                    </td>
                    <td align="right" style="width: 5%">
                        <asp:ImageButton ID="ImageButton2" runat="server" AlternateText="Cerrar ventana"
                            ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                            />
                    </td>
                </tr>
            </table>

            <center>
                <asp:UpdatePanel runat="server" ID="Up_bodyRequirimiento" UpdateMode="Conditional">
                    <ContentTemplate>

                        <br />

                        <table width="700px" style="border-collapse:separate !important; border-spacing: 4px;">

                            <tr>
                                <td align="right" style="width: 18%">
                                    <strong>NOMBRE PROYECTO:</strong>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblNombreProyecto" runat="server"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td align="right">
                                    <strong>SNIP:</strong>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblsnip" runat="server"></asp:Label>
                                    <asp:Label ID="lblIdProyecto" Visible="false" runat="server"></asp:Label>

                                </td>
                            </tr>

                            <tr>
                                <td align="right">
                                    <strong>UNIDAD EJECUTORA:</strong>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblejecutora" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>

                        <asp:Panel ID="Panel_UE_Informacion" runat="server" Width="700px" Visible="false">
                            <h5>DIRECTORIO UNIDAD EJECUTORA</h5>
                            <table width="100%" style="border-collapse:separate !important; border-spacing: 4px;">
                                <tr>
                                    <td align="right">
                                        <b>Cargo:</b>
                                    </td>
                                    <td style="width: 12%">
                                        <asp:Label ID="lblCargoUE" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <b>Nombre:</b>
                                    </td>
                                    <td style="width: 17%">
                                        <asp:Label ID="lblEncargadoUE" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>
                                    <td align="right">Dirección:</td>
                                    <td colspan="3">
                                        <asp:Label ID="lblDireccionUE" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="right">Código Postal:</td>
                                    <td>
                                        <asp:Label ID="lblCodigoPostal" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>

                                    <td align="right">Teléfono:</td>
                                    <td>
                                        <asp:Label ID="lbltelefonoUE" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>

                                    <td align="right">Fax:</td>
                                    <td>
                                        <asp:Label ID="lblFaxUE" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>

                                    <td align="right">Correo:</td>
                                    <td>
                                        <asp:Label ID="lblCorreoUE" runat="server" Text="" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <br />
                        <table width="700px" style="border-collapse:separate !important; border-spacing: 4px;">
                            <tr>
                                <td align="right">INAUGURADO POR:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlTipoInauguracion" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">REPRESENTANTE DEL MVCS:</td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtRepresentanteMVCS" runat="server" Width="300px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td align="right">FECHA DE INAUGURACIÓN:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtFechaInauguracion" runat="server" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                    <img style="border-right: 0px; border-top: 0px; border-left: 0px; border-bottom: 0px" class="urlTexto" onclick="ShowCalendario('<%=txtFechaInauguracion.ClientID %>');"
                                        alt="Calendario" src="../img/calendario.jpg" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender162" runat="server" TargetControlID="txtFechaInauguracion"
                                        FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                </td>
                                <td align="right">HORA INICIO:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtHoraInicio" runat="server" MaxLength="5" placeholder="hh:mm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtHoraInicio"
                                        FilterType="Custom, Numbers" ValidChars=":" Enabled="True" />
                                </td>
                                <td align="right">HORA TERMINO:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtHoraTermino" runat="server" MaxLength="5" placeholder="hh:mm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtHoraTermino"
                                        FilterType="Custom, Numbers" ValidChars=":" Enabled="True" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                        </table>



                        <asp:Button ID="btnGuardar" runat="server" Text="GUARDAR" Width="90px" CssClass="btn btn-primary" OnClick="btnGuardar_OnClick" />
                        <br />
                        <br />
                        <br />

                    </ContentTemplate>
                </asp:UpdatePanel>

                <div>
                    <asp:UpdatePanel ID="UP_calendar" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Calendar ID="Calendar1" runat="server" OnDayRender="DayRender" OnSelectionChanged="cCalChanged"></asp:Calendar>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </div>

                <%--ayuda para manejar el dt--%>
                <asp:ListBox ID="lb_Acciones" runat="server" Visible="false"></asp:ListBox>


            </center>

            <div id="user66" class="user66">
                <asp:Label runat="server" ID="lbl20" Text="Verificado por:" Font-Bold="true"></asp:Label>
                <asp:Label runat="server" ID="lblUser" Text=""></asp:Label>

            </div>

        </div>
    </asp:Panel>
</asp:Content>


