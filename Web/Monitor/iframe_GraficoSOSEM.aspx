﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iframe_GraficoSOSEM.aspx.cs" Inherits="Web.Monitor.iframe_GraficoSOSEM" Culture="es-PE" UICulture="es-PE" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" Height="301px" Width="501px">
                <diagramserializable>
                    <cc1:XYDiagram>
                        <axisx visibleinpanesserializable="-1">
                        </axisx>
                        <axisy visibleinpanesserializable="-1">
                            <label textpattern="{V:N2}"></label>
                        </axisy>
                    </cc1:XYDiagram>
                </diagramserializable>
                <legend alignmenthorizontal="Right" equallyspaceditems="False"></legend>
                <seriesserializable>
                    <cc1:Series Name="Devengado">
                    </cc1:Series>
                    <cc1:Series Name="PIM">
                    </cc1:Series>
                </seriesserializable>
            </dxchartsui:WebChartControl>

           <%-- <dxchartsui:WebChartControl ID="WebChartControl2" runat="server" CrosshairEnabled="True" Height="301px" Width="501px">
                <diagramserializable>
                    <cc1:XYDiagram>
                        <axisx visibleinpanesserializable="-1" alignment="Zero">
                              <label>
                              <resolveoverlappingoptions minindent="4" />
                              </label>
                              <gridlines visible="True">
                              </gridlines>
                              <numericscaleoptions aggregatefunction="Maximum" autogrid="False" scalemode="Manual" />
                        </axisx>
                        <axisy visibleinpanesserializable="-1">
                             <label textpattern="{V:N2}">
                             </label>
                        </axisy>
                    </cc1:XYDiagram>
                </diagramserializable>
                <legend alignmenthorizontal="Right" equallyspaceditems="False"></legend>
                <seriesserializable>
                     <cc1:Series Name="Devengado">
                     </cc1:Series>
                     <cc1:Series Name="PIM">
                     </cc1:Series>
                     </seriesserializable>
            </dxchartsui:WebChartControl>--%>

        </div>
    </form>
</body>
</html>
