﻿using System;
using System.Data;
using System.Web.UI;
using Business;
using Entity;


namespace Web.Monitor
{
    public partial class iframe_RegistrarInformeVisita : System.Web.UI.Page
    {
        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BLUtil _Metodo = new BLUtil();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = 0;

                string idProyecto = "";
                try
                {
                    id = _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(Request.QueryString["idE"].ToString());
                    lblIdRecomendacionInformeMonitoreoPOPU.Text = id.ToString();
                    idProyecto = Request.QueryString["idPo"].ToString();
                }
                catch
                {

                }
                cargaDatosInformeVisita(id);
                LblID_PROYECTO.Text = idProyecto;

                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();


            }
        }

  
        protected void cargaDatosInformeVisita(int idEvaluacionRecomendacion)
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(idEvaluacionRecomendacion);
            dt = _objBLEjecucion.spMON_EvaluacionRecomendacionActaVisita(_BEEjecucion);

            txtNumeroInforme.Text = dt.Rows[0]["NroInformeVisita"].ToString();
            txtDestinatario.Text = dt.Rows[0]["DirigidoAInforme"].ToString();
            txtCargoDestinatario.Text = dt.Rows[0]["DirigidoCargoAInforme"].ToString();
            txtAsuntoInforme.Text = dt.Rows[0]["AsuntoInforme"].ToString();
            txtReferenciaInforme.Text = dt.Rows[0]["ReferenciaInforme"].ToString();
            txtFechaInforme.Text = dt.Rows[0]["FechaInformeMonitoreo"].ToString();

            txtAntecedentesInforme.Text = dt.Rows[0]["antecedenteInforme"].ToString();
            txtCabeceraInforme.Text = dt.Rows[0]["CabeceraInforme"].ToString();
            txtMetasInforme.Text = dt.Rows[0]["metasInforme"].ToString();
            txtAspectoFinancieroInforme.Text = dt.Rows[0]["comentarioFinanciamientoInforme"].ToString();
            txtAnalisisInforme.Text = dt.Rows[0]["analisisInforme"].ToString();

            txtConclusionesInforme.Text = dt.Rows[0]["conclusionInforme"].ToString();
            txtRecomendacionesInforme.Text = dt.Rows[0]["recomendacionInforme"].ToString();

            lblNomUsuarioInforme.Text = "Actualizó: " + dt.Rows[0]["usuarioInforme"].ToString() + " - " + dt.Rows[0]["fechaUpdateInforme"].ToString();
        }

        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected void btnGuardarInformeVisita_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroInforme())
            {
                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdRecomendacionInformeMonitoreoPOPU.Text);

                _BEEjecucion.informe = txtNumeroInforme.Text;
                _BEEjecucion.dirigidoA = txtDestinatario.Text;
                _BEEjecucion.cargo = txtCargoDestinatario.Text;
                _BEEjecucion.asunto = txtAsuntoInforme.Text;
                _BEEjecucion.referencia = txtReferenciaInforme.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaInforme.Text);
                _BEEjecucion.cabecera = txtCabeceraInforme.Text;
                _BEEjecucion.antecedentes = txtAntecedentesInforme.Text;
                _BEEjecucion.metas = txtMetasInforme.Text;
                _BEEjecucion.comentario = txtAspectoFinancieroInforme.Text;
                _BEEjecucion.analisis = txtAnalisisInforme.Text;
                _BEEjecucion.observacion = txtConclusionesInforme.Text;
                _BEEjecucion.Recomendacion = txtRecomendacionesInforme.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.U_MON_EvaluacionRecomendacionInformeVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDatosInformeVisita(_BEEjecucion.Id_ejecucionRecomendacion);
                    Up_RegistroInforme.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void btnGenerarInformePOPUP_Click(object sender, EventArgs e)
        {
            string id = lblIdRecomendacionInformeMonitoreoPOPU.Text;
            string script = "<script>window.open('Informe_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected Boolean ValidarRegistroInforme()
        {
            Boolean result;
            result = true;

            if (txtNumeroInforme.Text == "")
            {
                string script = "<script>alert('Ingresar número de informe.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtDestinatario.Text == "")
            {
                string script = "<script>alert('Ingresar dirigido a.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtAsuntoInforme.Text == "")
            {
                string script = "<script>alert('Ingresar asunto.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaInforme.Text == "")
            {
                string script = "<script>alert('Ingresar fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaInforme.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtCabeceraInforme.Text == "")
            {
                string script = "<script>alert('Ingresar cabecera de informe.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtAntecedentesInforme.Text == "")
            {
                string script = "<script>alert('Ingresar antecedentes.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtAspectoFinancieroInforme.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar aspectos financieros.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtAnalisisInforme.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar Analisis.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtAnalisisInforme.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar Analisis.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtConclusionesInforme.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar observaciones.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtRecomendacionesInforme.Text.ToString().Trim() == "")
            {
                string script = "<script>alert('Ingresar recomendaciones.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;

        }
    }
}