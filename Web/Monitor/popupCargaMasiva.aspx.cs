﻿using OfficeOpenXml;
using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class popupCargaMasiva : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            subirArchivos(FileUpload);
        }

        protected void Upload(object sender, EventArgs e)
        {
            string fileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" + fileName + "');", true);
        }


        #region Carga Masiva

        private string validarArchivo(FileUpload file)
        {
            try
            {
                FileInfo fInfo = new FileInfo(file.PostedFile.FileName);
                string ext = fInfo.Extension.ToLower();
                if (ext == ".pdf" | ext == ".doc" | ext == ".docx" | ext == ".xls" | ext == ".xlsx" | ext == ".jpg" | ext == ".jpeg" | ext == ".png" | ext == ".bmp")
                {
                    if (file.PostedFile.ContentLength <= 10485760)
                    {
                        return "OK";
                    }
                    else
                    {
                        return "El Archivo sobrepasa los 10 MB";
                    }
                }
                else
                {
                    return "Extensión no válida";
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return "Ocurrió un error al validar el Archivo a cargar";
            }
        }

        private void subirArchivos(FileUpload file)
        {
            divError.Visible = false;
            try
            {
                string rutaGuardar = Server.MapPath(ConfigurationManager.AppSettings["RutaCargaExcelTMP"]);
                if (file.HasFile == true)
                {
                    var fileExcel = rutaGuardar + file.FileName;
                    file.SaveAs(fileExcel);

                    //var dataExcel = ExcelToPadronBeneficiario(fileExcel);
                    //var lista_error = dataExcel.FindAll(c => c.Detalle_Error != null);
                    //if (lista_error != null && lista_error.Count > 0)
                    //{

                    //    return;
                    //}
                    var dataExcel = ExcelToPadronBeneficiario(fileExcel);
                    if (dataExcel == null || dataExcel.Count == 0)
                    {
                        MostrarMensajeClient("El excel no contiene registros para procesar");
                        return;
                    }


                    var lista_error = dataExcel.FindAll(c => c.Detalle_Error != null);
                    if (lista_error != null && lista_error.Count > 0)
                    {
                        MostrarMensajeClient("El excel contiene " + lista_error.Count + " registros que son requeridos, completar y volver a cargar el archivo!");
                        divError.Visible = true;
                        grillaError.DataSource = lista_error;
                        grillaError.DataBind();
                        return;
                    }
                    else
                    {
                        int id_proyecto = Convert.ToInt32(Session["id_proyecto_carga"]);
                        int usr_registro = Convert.ToInt32(Session["usr_registro_carga"]);

                        var resultado = new BL_PadronBeneficiario().ValidarPadronBeneficiario(dataExcel, id_proyecto, usr_registro);
                        //var mostrar = new BL_PadronBeneficiario().Listar_PadronBeneficiario(id_proyecto);
                        if (resultado != null)
                        {
                            var listaErrorBD = resultado.ToList();

                            if (listaErrorBD.Count > 0)
                            {
                                //mostrar en grilla
                                divError.Visible = true;
                                grillaError.DataSource = listaErrorBD;
                                grillaError.DataBind();
                                if (dataExcel.Count == listaErrorBD.Count)
                                {
                                    MostrarMensajeClient(listaErrorBD.Count + " registros no se cargaron. Verifique su archivo excel");
                                }
                                else
                                {
                                    //MostrarMensajeClient("Sólo se cargaron: "+(dataExcel.Count - listaErrorBD.Count) + " registros. Y " + listaErrorBD.Count + " registros no se cargaron, verifique el detalle del error. Verifique su archivo excel");
                                    MostrarMensajeClient("De " + dataExcel.Count + " registros, sólo se cargaron " + (dataExcel.Count - listaErrorBD.Count) + ". Verifique el detalle del error y su archivo de excel");
                                }
                            }
                            else
                            {
                                MostrarMensajeClient("Se cargaron " + dataExcel.Count + " registros correctamente");
                            }
                        }
                        else
                        {
                            MostrarMensajeClient("Se cargó correctamente todo los " + dataExcel.Count + " registros");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //mosrrar mmenaje error
                Console.Write(ex.Message);
            }
        }

        #region Leer Excel

        public List<BE_MON_PadronBeneficiario> ExcelToPadronBeneficiario(string fileName)
        {
            //object dataList = null;
            List<BE_MON_PadronBeneficiario> dataList = new List<BE_MON_PadronBeneficiario>();

            if (File.Exists(fileName))
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    using (var stream = File.OpenRead(fileName))
                    {
                        package.Load(stream);
                    }
                    ExcelWorksheet ws = package.Workbook.Worksheets.First();
                    //dataList = WorksheetToDataTable(ws, true);
                    dataList = WorksheetToPadronBeneficiario(ws, true);
                }
            }
            return dataList;
        }

        private List<BE_MON_PadronBeneficiario> WorksheetToPadronBeneficiario(ExcelWorksheet ws, bool hasHeader = true)
        {

            //Instanciar lista a retornar
            List<BE_MON_PadronBeneficiario> lista = new List<BE_MON_PadronBeneficiario>();
            //
            DataTable dt = new DataTable(ws.Name);
            int totalCols = ws.Dimension.End.Column;
            int totalRows = ws.Dimension.End.Row;
            int startRow = hasHeader ? 2 : 1;
            ExcelRange wsRow;
            DataRow dr;
            foreach (var firstRowCell in ws.Cells[1, 1, 1, totalCols])
            {
                dt.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            }

            for (int rowNum = startRow; rowNum <= totalRows; rowNum++)
            {
                wsRow = ws.Cells[rowNum, 1, rowNum, totalCols];
                dr = dt.NewRow();
                foreach (var cell in wsRow)
                {
                    dr[cell.Start.Column - 1] = cell.Text;
                }

                dt.Rows.Add(dr);
                var item = new BE_MON_PadronBeneficiario();


                if (!string.IsNullOrWhiteSpace(dr.ItemArray[0].ToString()))
                    item.apellidoPaterno = Convert.ToString(dr.ItemArray[0]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Apellido Paterno es requerido");

                if (!string.IsNullOrWhiteSpace(dr.ItemArray[1].ToString()))
                    item.apellidoMaterno = Convert.ToString(dr.ItemArray[1]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Apellido Materno es requerido");


                if (!string.IsNullOrWhiteSpace(dr.ItemArray[2].ToString()))
                    item.nombres = Convert.ToString(dr.ItemArray[2]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Nombres es requerido");

                if (!string.IsNullOrWhiteSpace(dr.ItemArray[3].ToString()))
                    item.Sexo = Convert.ToString(dr.ItemArray[3]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Sexo es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[4].ToString()))
                {
                    DateTime fecha = DateTime.Now;
                    if (DateTime.TryParse(dr.ItemArray[4].ToString(), out fecha))
                    {
                        item.fechaNacimiento = Convert.ToDateTime(dr.ItemArray[4]);
                        item.vfechaNacimiento = dr.ItemArray[4].ToString();
                    }
                    else
                    {
                        item.vfechaNacimiento = dr.ItemArray[4].ToString();
                        item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Fecha no tiene formato correcto 'dd/mm/yyyy'");
                    }

                }
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Fecha es requerido");


                if (!string.IsNullOrEmpty(dr.ItemArray[5].ToString()))
                    item.Tipo_DocMiembroNE = Convert.ToString(dr.ItemArray[5]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Tipo Documento NE es requerido");


                if ((dr.ItemArray[5].ToString()).ToLower() == "dni")
                    if (!string.IsNullOrEmpty(dr.ItemArray[6].ToString()))
                        item.dni = Convert.ToString(dr.ItemArray[6]);
                    else
                        item.Detalle_Error = ValidarCampo(item.Detalle_Error, "DNI es requerido");
                else
                    if (!string.IsNullOrEmpty(dr.ItemArray[6].ToString()))
                    item.dni = Convert.ToString(dr.ItemArray[6]);


                if (!string.IsNullOrEmpty(dr.ItemArray[7].ToString()))
                    item.Tipo_Domicilio = Convert.ToString(dr.ItemArray[7]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Tipo Domicilio es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[8].ToString()))
                    item.direccionDomicilio = Convert.ToString(dr.ItemArray[8]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Domicilio es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[9].ToString()))
                    item.nroDomicilio = Convert.ToString(dr.ItemArray[9]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Número Domicilio es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[10].ToString()))
                    item.Tipo_Beneficio = Convert.ToString(dr.ItemArray[10]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Tipo Beneficio es requerido");

                if (!string.IsNullOrEmpty(dr.ItemArray[11].ToString()))
                    item.Estado_Beneficiario = Convert.ToString(dr.ItemArray[11]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Estado Beneficiario es requerido");

                if (!string.IsNullOrWhiteSpace((dr.ItemArray[12]).ToString()))
                {
                    int i = 0;
                    if (int.TryParse(dr.ItemArray[12].ToString(), out i))
                    {
                        item.nroMiembrosHombres = Convert.ToInt32(dr.ItemArray[12]);
                    }
                    else
                    {
                        item.Detalle_Error = ValidarCampo(item.Detalle_Error, " En Nro. Miembros H. solo permite valores númericos");
                    }
                }


                if (!string.IsNullOrWhiteSpace((dr.ItemArray[13]).ToString()))
                {
                    int i = 0;
                    if (int.TryParse(dr.ItemArray[13].ToString(), out i))
                    {
                        item.nroMiembrosMujeres = Convert.ToInt32(dr.ItemArray[13]);
                    }
                    else
                    {
                        item.Detalle_Error = ValidarCampo(item.Detalle_Error, " En Nro. Miembros M. solo permite valores númericos");
                    }
                }

                if (!string.IsNullOrWhiteSpace((dr.ItemArray[14]).ToString()))
                    item.Conexion_Agua = Convert.ToString(dr.ItemArray[14]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Conexion agua es requerido");

                if (!string.IsNullOrWhiteSpace((dr.ItemArray[15]).ToString()))
                    item.Ubs = Convert.ToString(dr.ItemArray[15]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "UBS es requerido");

                if (!string.IsNullOrWhiteSpace((dr.ItemArray[16]).ToString()))
                    item.Tipo_Ubs = Convert.ToString(dr.ItemArray[16]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Tipo UBS es requerido");

                if (!string.IsNullOrWhiteSpace((dr.ItemArray[17]).ToString()))
                    item.Estado_Predio = Convert.ToString(dr.ItemArray[17]);
                else
                    item.Detalle_Error = ValidarCampo(item.Detalle_Error, "Estado Predio es requerido");

                if (!string.IsNullOrWhiteSpace((dr.ItemArray[18]).ToString()))
                    item.observaciones = Convert.ToString(dr.ItemArray[18]);

                item.Numero = rowNum;

                lista.Add(item);
            }

            return lista;
        }

        private string ValidarCampo(string Detalle_Error, string mensaje)
        {
            return Detalle_Error == null ? mensaje : Detalle_Error + " | " + mensaje;
        }

        private void MostrarMensajeClient(string mensaje)
        {
            string script = "<script>alert('" + mensaje + "');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        #endregion
        #endregion

    }
}