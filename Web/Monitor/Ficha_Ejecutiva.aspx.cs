﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entity;
using Business;
using System.Data;
using System.Text;

namespace Web.Monitor
{
    public partial class Ficha_Ejecutiva : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        DataTable dt = new DataTable();

        string pUrl = "";
        string pfilepath = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            // GRABAR LOG
            //RegisterAsyncTask(new PageAsyncTask(AsynRegistrarLog));


            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["idtipo"] != null && Request.QueryString["id"].Length > 0 && Request.QueryString["idtipo"].Length > 0)
                {
                    lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";

                    if (Request.Browser.IsMobileDevice && imgbtnExportar.Visible == true)
                    {
                        imgbtnImprimir.Visible = false;
                        imgbtnExportar.Visible = false;
                        imgbtnExportarMobile.Visible = true;
                    }

                    CargaInformacionGeneral();

                    try
                    {
                        string t = Request.QueryString["t"].ToString();

                        if (t == "p")
                        {
                            imgbtnImprimir.Visible = false;
                            imgbtnExportar.Visible = false;
                        }
                    }
                    catch
                    { }

                }
                else
                {
                    lblTitle.Text = "NOTA: PARAMETROS INCORRECTOS.";

                    string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        //modificando
        protected void CargaInformacionGeneral()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;
            _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());


            dt = _objBLLiquidacion.spMON_Ayuda_Memoria(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {

                lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblUnico.Text = dt.Rows[0]["iCodUnificado"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblMontoSnip.Text = dt.Rows[0]["MONTO_SNIP"].ToString();
                lblMontoInversionTotalMEF.Text = dt.Rows[0]["monto_inversion"].ToString();

                if (_BELiquidacion.tipoFinanciamiento == 0)
                {
                    _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(dt.Rows[0]["id_TipoFinanciamiento"].ToString());
                }

                string fechaMEFUpdate = dt.Rows[0]["FechaUpdateMEF"].ToString();
                lblMsjMontoInversion.Text = " (Fuente - SSI al " + fechaMEFUpdate + ")";

                lblMontoConvenio.Text = dt.Rows[0]["MONTO_CONVENIO"].ToString();
                lblEjecutora.Text = dt.Rows[0]["UNIDAD_EJECUTARORA"].ToString();
                lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOS"].ToString()).ToString("N0");

                if (dt.Rows[0]["id_tipoPrograma"].ToString().Equals("2"))
                {
                    if (Convert.ToInt32(dt.Rows[0]["BENEFICIARIOSDIRECTOS"].ToString()) > 0)
                    {
                        lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOSDIRECTOS"].ToString()).ToString("N0") + " directos";
                    }
                    else
                    {
                        lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOS"].ToString()).ToString("N0") + " Habitantes (Fuente Formato SNIP 03)";
                    }
                }
                else
                {
                    lblBeneficiario.Text = lblBeneficiario.Text + " Habitantes (Fuente Formato SNIP 03)";
                }


                BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
                List<BE_MON_Financiamiento> ListTransferencia = new List<BE_MON_Financiamiento>();
                ListTransferencia = _objBLTransferencia.F_spSOL_TransferenciaByProyecto(Convert.ToInt32(Request.QueryString["id"].ToString()));
                xgrdTransferencias.DataSource = ListTransferencia;
                xgrdTransferencias.DataBind();

                if (ListTransferencia.Count > 0)
                {
                    decimal sum = ListTransferencia.Where(s => s.Id == "1" | s.Id == "2").Select(c => Convert.ToDecimal(c.montoAprobacion)).Sum();
                    decimal sumPIA = ListTransferencia.Where(s => s.Id == "3").Select(c => Convert.ToDecimal(c.montoAprobacion)).Sum();
                    decimal conv = Convert.ToDecimal(lblMontoConvenio.Text);
                    string val;
                    if (conv == 0)
                    {
                        val = "0.00";
                        lblMsjTransferencia.Text = "Se ha transferido S/" + sum.ToString("N") + ".";
                        LblAsignacionPIA.Text = "Se ha asignado a través del PIA S/" + sumPIA.ToString("N") + ".";
                    }
                    else
                    {
                        val = ((sum * 100) / conv).ToString("N");
                        lblMsjTransferencia.Text = "Se ha transferido S/" + sum.ToString("N") + " (" + val + "%).";

                        val = ((sumPIA * 100) / conv).ToString("N");
                        LblAsignacionPIA.Text = "Se ha asignado a través del PIA  S/" + sumPIA.ToString("N") + " (" + val + "%).";
                    }
                }

                lblFechaInicio1.Text = dt.Rows[0]["FECHAINICIO"].ToString();
                lblFechaTerminoContractual.Text = dt.Rows[0]["FechaTerminoContractual"].ToString();
                lblFechaTerminoReal.Text = dt.Rows[0]["FECHA_FIN"].ToString();
                lblPlazoEjecucion1.Text = dt.Rows[0]["PLAZOEJECUCION"].ToString();

                lblEstadoSituacional.Text = dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();
                lblPorcentajeAvance.Text = dt.Rows[0]["FISICOREAL"].ToString();
                lblAvanceProgramado.Text = dt.Rows[0]["FisicoProgramado"].ToString();
                lblAvanceFinanciero.Text = dt.Rows[0]["AvanceFinanciero"].ToString();
                lblAvanceFinanciero.Text = Convert.ToDouble(lblAvanceFinanciero.Text).ToString("N") + "% (Fuente - SSI)";

                lblModalidad1.Text = dt.Rows[0]["MODALIDAD"].ToString();

                string vIdTipo = dt.Rows[0]["id_tipoSubModalidadFinanciamiento"].ToString();


                if (vIdTipo.Equals("1")) // TRANSFERENCIA
                {
                    if (lblModalidad1.Text == "Directa")
                    {
                        trd1.Visible = false;
                        trd2.Visible = false;
                        trd3.Visible = false;
                        trd4.Visible = false;
                        //trd5.Visible = false;
                        lblCargoAyudaMemoria.Text = "Jefe Supervisor";
                    }
                }
                else if (vIdTipo.Equals("3")) // POR CONTRATA
                {

                    trMonTotalAyudaMemoria.Visible = false;
                    trAportesAyudaMemoria.Visible = false;
                    //trMontoCompromAyudaMemoria.Visible = false;
                    trTransferenciasAyudaMemoria.Visible = false;
                    lblCargoAyudaMemoria.Text = "Jefe Supervisor";
                    lblMsjTransferencia.Visible = false;
                    xgrdTransferencias.Visible = false;

                    PanelTransferencia.Visible = false;

                    if (xgrdTransferencias.Rows.Count > 0)
                    {
                        PanelTransferencia.Visible = true;
                        xgrdTransferencias.Visible = true;
                        lblMsjTransferencia.Visible = true;
                        trTransferenciasAyudaMemoria.Visible = true;
                    }
                }

                if (dt.Rows[0]["NroContratoSupervision"].ToString().Equals("1"))
                {
                    lblNomContratista.Text = "Contratista/Consorcio Obra";
                }
                else
                {
                    lblNomContratista.Text = "Consorcio Obra";
                }

                lblContratista.Text = dt.Rows[0]["CONTRATISTA"].ToString();

                lblNroContratoSupervision.Text = dt.Rows[0]["NroContratoSupervision"].ToString();
                lblNroContratoObra.Text = dt.Rows[0]["NroContratoObra"].ToString();

                lblMontoContratado.Text = dt.Rows[0]["MONTOCONSORCIO"].ToString();
                lblMontoContratado.Text = Convert.ToDouble(lblMontoContratado.Text).ToString("N");

                lblEmpresaSupervisora.Text = dt.Rows[0]["EmpresaSupervisor"].ToString();

                lblMontoSupervision.Text = dt.Rows[0]["MontoSupervisor"].ToString();
                lblMontoSupervision.Text = Convert.ToDouble(lblMontoSupervision.Text).ToString("N");


                lblSupervisor.Text = dt.Rows[0]["SUPERVISORDESIGNADO"].ToString();
                lblResidente.Text = dt.Rows[0]["RESIDENTEOBRA"].ToString();
                lblInspector.Text = dt.Rows[0]["INSPECTOR"].ToString();

                if (_BELiquidacion.tipoFinanciamiento == 3) //OBRA
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                    List<BE_MON_Ejecucion> _ListAvance = _objBLEjecucion.F_MON_DetalleSituacional(_BEEjecucion);
                    if (_ListAvance.Count > 0)
                    {
                        string htmlDetalleSituacional = "";
                        string htmlAcciones = "";
                        int contador = 0;

                        for (int i = 0; i < _ListAvance.Count && i < 5; i++)
                        {
                            if (_ListAvance.ElementAt(i).Evaluacion.Length > 0)
                            {
                                htmlDetalleSituacional = htmlDetalleSituacional + string.Format("<li>{0}: {1}</li>", _ListAvance.ElementAt(i).Date_fechaVisita.ToString("dd/MM/yyyy"), _ListAvance.ElementAt(i).Evaluacion.ToString());
                                contador = contador + 1;
                            }

                            if (_ListAvance.ElementAt(i).Recomendacion.Length > 0)
                            {
                                htmlAcciones = htmlAcciones + string.Format("<li>{0}: {1}</li>", _ListAvance.ElementAt(i).Date_fechaVisita.ToString("dd/MM/yyyy"), _ListAvance.ElementAt(i).Recomendacion.ToString());
                            }
                        }

                        if (htmlDetalleSituacional.Length > 0)
                        {
                            htmlDetalleSituacional = "<ul>" + htmlDetalleSituacional + "</ul>";
                            divDetalle.InnerHtml = htmlDetalleSituacional;
                        }

                        if (htmlAcciones.Length > 0)
                        {
                            htmlAcciones = "<ul>" + htmlAcciones + "</ul>";
                            divAcciones.InnerHtml = htmlAcciones;
                        }
                    }
                }
                else //PERFIL Y EXPEDIENTE
                {
                    trEtapaFinanciamiento.Visible = true;
                    lblNomEtapa.Text = dt.Rows[0]["TIPO_PROYECTO"].ToString();
                    lblNomContratista.Text = "Contratista/Consorcio";
                    lblNomMontoContrato.Text = "Monto Contratado";
                    lblNomNroContrato.Text = "Nro Contrato";
                    lblNomEstado.Text = "ESTADO " + lblNomEtapa.Text;
                    trResidente.Visible = false;
                    trInspector.Visible = false;

                    _BEEjecucion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                    //_BEEjecucion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());
                    _BEEjecucion.tipoFinanciamiento = _BELiquidacion.tipoFinanciamiento;

                    DataTable dtAvanceFisico = new DataTable();
                    dtAvanceFisico = _objBLEjecucion.spMON_AvanceFisico(_BEEjecucion);

                    DataRow[] result = dtAvanceFisico.Select("flagAyuda = '1'");
                    if (result.Count() > 0)
                    {
                        string htmlDetalleSituacional = "";
                        string htmlAcciones = "";

                        int contador = 0;
                        int totalA = result.Count();

                        for (int i = 0; i < result.Count() && i < 10; i++)
                        {
                            if (result[totalA - 1]["situacion"].ToString().Length > 0)
                            {
                                htmlDetalleSituacional = htmlDetalleSituacional + string.Format("<li>{0}: {1}</li>", result[totalA - 1]["fecha"].ToString(), result[totalA - 1]["situacion"].ToString());
                                contador = contador + 1;
                                totalA = totalA - 1;
                            }

                            if (vIdTipo.Equals("3") && result[totalA]["accionesRealizadas"].ToString().Length > 0) //POR CONTRATA
                            {
                                htmlAcciones = htmlAcciones + string.Format("<li>{0}: {1}</li>", result[totalA]["fecha"].ToString(), result[totalA]["accionesRealizadas"].ToString());
                            }
                        }

                        if (htmlDetalleSituacional.Length > 0)
                        {
                            htmlDetalleSituacional = "<ul>" + htmlDetalleSituacional + "</ul>";
                            divDetalle.InnerHtml = htmlDetalleSituacional;
                        }

                        if (htmlAcciones.Length > 0)
                        {
                            htmlAcciones = "<ul>" + htmlAcciones + "</ul>";
                            divAcciones.InnerHtml = htmlAcciones;
                        }
                    }

                }


                CargaFinanciamientoTab0(Convert.ToInt32(Request.QueryString["id"]));
                int id_solicitudes = Convert.ToInt32(dt.Rows[0]["id_solicitudes"].ToString());




                DataSet ds = new DataSet();
                ds = _objBLFinanciamiento.spMEF_Financiamiento(Convert.ToInt32(lblSnip.Text));

                grdDetalleSOSEMTab0.DataSource = ds.Tables[0];
                grdDetalleSOSEMTab0.DataBind();

                Double resultadoTotal = 0;
                string vFecha = "";// = DateTime.Now.ToString("dd/MM/yyyy");
                if (ds.Tables[1].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    //resultadoTotal = ListSosem.Sum(elemento => Convert.ToDouble(elemento.devAcumulado));
                    resultadoTotal = Convert.ToDouble(dt.Compute("Sum(devAcumulado)", ""));

                    vFecha = ds.Tables[1].Rows[0][0].ToString();
                }
                else if (ds.Tables[2].Rows.Count > 0)
                {
                    vFecha = ds.Tables[2].Rows[0][0].ToString();
                }

                lblMsjSosem.Text = "Con fecha " + vFecha + " la Unidad Ejecutora cuenta con un monto devengado de S/ " + resultadoTotal.ToString("N") + " de acuerdo al Sistema SSI - MEF .";

            }
            else
            {
                lblTitle.Text = "NOTA: NO EXISTE INFORMACIÓN PARA MOSTRAR.";

                string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }


        }



        protected void CargaFinanciamientoTab0(int idProy)
        {

            //Colocando resumen de financiamientp etapa - https://trello.com/c/uqVY2SZM/41-obras
            DataSet ds = new DataSet();
            ds = _objBLFinanciamiento.paSSP_MON_rFinanciamiento(idProy);
            DataTable dtSumatorias = new DataTable();
            dtSumatorias = ds.Tables[0];

            double montoMVCS = 0;
            double montoCofinanciamiento = 0;
            double porcCofinanciamiento = 0;
            double porcMVCS = 0;

            if (dtSumatorias.Rows.Count > 0)
            {
                montoMVCS = Convert.ToDouble(dtSumatorias.Rows[0]["SumMontoTransferido"]);
            }


            DataTable dtCofinanciamiento = new DataTable();
            dtCofinanciamiento = ds.Tables[1];

            if (dtCofinanciamiento.Rows.Count > 0)
            {
                montoCofinanciamiento = Convert.ToDouble(dtCofinanciamiento.Rows[0]["SumCofinanciamiento"]);
            }

            double Total = montoMVCS + montoCofinanciamiento;

            lblTotalInversionTab0.Text = Total.ToString("N"); ;
            lblMontoMVCSTransferido.Text = montoMVCS.ToString("N");
            lblMontoFirmantes.Text = montoCofinanciamiento.ToString("N");

            if (montoMVCS != 0) { porcMVCS = (montoMVCS * 100) / Total; }
            if (montoCofinanciamiento != 0) { porcCofinanciamiento = (montoCofinanciamiento * 100) / Total; }

            lblPorcentajeMontoMVCS.Text = porcMVCS.ToString("N") + "%";
            lblPorcentajeMontoFirmantes.Text = porcCofinanciamiento.ToString("N") + "%";
        }

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "AyudaMemoria_SNIP_" + lblSnip.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }

    }
}