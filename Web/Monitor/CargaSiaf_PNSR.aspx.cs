﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Entity;
using System.Data;
using OfficeOpenXml;
using System.IO;
using Business;

namespace Web.Monitor
{
    public partial class CargaSiaf_PNSR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            subirArchivos(FileUpload1);
        }

        private void subirArchivos(FileUpload file)
        {
            //divError.Visible = false;
            //divErrorCarga.Visible = false;
            try
            {
                string rutaGuardar = Server.MapPath(ConfigurationManager.AppSettings["RutaCargaExcelTMP"]);
                if (file.HasFile == true)
                {
                    var fileExcel = rutaGuardar + file.FileName;
                    file.SaveAs(fileExcel);

                    var dataExcel = ExcelToTecnicoPersonalNE(fileExcel);
                    if (dataExcel == null || dataExcel.Count == 0)
                    {
                        MostrarMensajeClient("El excel no contiene registros para procesar");
                        return;
                    }
                                    
                    var lista_error =  1;
                    if (lista_error !=1)
                    {
                        MostrarMensajeClient("El excel contiene  registros que son requeridos, completar y volver a cargar el archivo!");
                        divErrorCarga.Visible = true;
                        grillaErrorCarga.DataSource = lista_error;
                        grillaErrorCarga.DataBind();
                        return;
                    }
                    else
                    {
                        
                        int id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                        var resultado = new BL_MON_Ejecucion().CargaSiaf(dataExcel, id_usuario, ddlAnio.SelectedValue, ddlMes.SelectedValue);
                        MostrarMensajeClient("Se cargó correctamente todo los " + dataExcel.Count + " registros");
                      
                    }
                }
                else
                {
                    MostrarMensajeClient("Debe cargar el excel con el formato indicado.");
                }
            }
            catch (Exception ex)
            {
                //mosrrar mmenaje error
                Console.Write(ex.Message);

            }
        }

        public List<BE_MON_SIAF_PNSR> ExcelToTecnicoPersonalNE(string fileName)
        {
            //object dataList = null;
            List<BE_MON_SIAF_PNSR> dataList = new List<BE_MON_SIAF_PNSR>();

            if (File.Exists(fileName))
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    using (var stream = File.OpenRead(fileName))
                    {
                        package.Load(stream);
                    }
                    ExcelWorksheet ws = package.Workbook.Worksheets.First();
                    //dataList = WorksheetToDataTable(ws, true);
                    dataList = WorksheetToTecnicoPersonalNE(ws, true);
                }
            }
            return dataList;
        }

        private List<BE_MON_SIAF_PNSR> WorksheetToTecnicoPersonalNE(ExcelWorksheet ws, bool hasHeader = true)
        {

            //Instanciar lista a retornar
            List<BE_MON_SIAF_PNSR> lista = new List<BE_MON_SIAF_PNSR>();
            //
            DataTable dt = new DataTable(ws.Name);
            int totalCols = ws.Dimension.End.Column;
            int totalRows = ws.Dimension.End.Row;
            int startRow = hasHeader ? 2 : 1;
            ExcelRange wsRow;
            DataRow dr;
            foreach (var firstRowCell in ws.Cells[1, 1, 1, totalCols])
            {
                dt.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            }

            for (int rowNum = startRow; rowNum <= totalRows; rowNum++)
            {
                wsRow = ws.Cells[rowNum, 1, rowNum, totalCols];
                dr = dt.NewRow();
                foreach (var cell in wsRow)
                {
                    dr[cell.Start.Column - 1] = cell.Text;
                }

                dt.Rows.Add(dr);
                var item = new BE_MON_SIAF_PNSR();

                item.ANO_EJE = Convert.ToString(dr.ItemArray[0]);
                item.EXPEDIENTE = Convert.ToString(dr.ItemArray[1]);
                item.SEC_AREA = Convert.ToString(dr.ItemArray[2]);
                item.FASE = Convert.ToString(dr.ItemArray[3]);
                item.COD_DOC = Convert.ToString(dr.ItemArray[4]);
                item.COD_DOC_NOMBRE = Convert.ToString(dr.ItemArray[5]);
                item.NUM_DOC = Convert.ToString(dr.ItemArray[6]);
                item.NOTAS = Convert.ToString(dr.ItemArray[7]);
                item.FECHA_DOC = Convert.ToString(dr.ItemArray[8]);
                item.SEC_FUNC = Convert.ToString(dr.ItemArray[9]);
                item.SEC_FUNC_NOMBRE = Convert.ToString(dr.ItemArray[10]);
                item.PROD_PRY = Convert.ToString(dr.ItemArray[11]);
                item.META = Convert.ToString(dr.ItemArray[12]);
                item.FINALIDAD = Convert.ToString(dr.ItemArray[13]);
                item.MONTO = Convert.ToString(dr.ItemArray[14]);
                item.MONTO_NACIONAL = Convert.ToString(dr.ItemArray[15]);
                item.FECHA_AUTORIZACION = Convert.ToString(dr.ItemArray[16]);
                item.NUM_DOC_B = Convert.ToString(dr.ItemArray[17]);
                item.ANO_PROCESO = Convert.ToString(dr.ItemArray[18]);
                item.MES_PROCESO = Convert.ToString(dr.ItemArray[19]);
                item.DIA_PROCESO = Convert.ToString(dr.ItemArray[20]);
                item.CERTIFICADO_GLOSA = Convert.ToString(dr.ItemArray[21]);
                item.USUARIO_CREACION_CLT = Convert.ToString(dr.ItemArray[22]);
                item.FECHA_FIN_CONTRATO = Convert.ToString(dr.ItemArray[23]);

                lista.Add(item);
            }

            return lista;
        }

        private string ValidarCampo(string Detalle_Error, string mensaje)
        {
            return Detalle_Error == null ? mensaje : Detalle_Error + " | " + mensaje;
        }

        private void MostrarMensajeClient(string mensaje)
        {
            string script = "<script>alert('" + mensaje + "');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }
    }
}