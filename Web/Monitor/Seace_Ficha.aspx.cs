﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class Seace_Ficha : System.Web.UI.Page
    {
   
        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();
                     
        DataTable dt = new DataTable();

        //public void Grabar_Log()
        //{
        //    BL_LOG _objBLLog = new BL_LOG();
        //    BE_LOG _BELog = new BE_LOG();

        //    string ip;
        //    string hostName;
        //    string pagina;
        //    string tipoDispositivo = "";
        //    string agente = "";
        //    try
        //    {
        //        ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        ip = "";
        //    }

        //    try
        //    {
        //        hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
        //    }
        //    catch (Exception ex)
        //    {
        //        hostName = "";
        //    }

        //    try
        //    {
        //        pagina = HttpContext.Current.Request.Url.AbsoluteUri;
        //    }
        //    catch (Exception ex)
        //    {
        //        pagina = "";
        //    }

        //    try
        //    {
        //        string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
        //        agente = uAg;
        //        Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
        //        bool isMobile = regEx.IsMatch(uAg);
        //        if (isMobile)
        //        {
        //            tipoDispositivo = "Movil";
        //        }
        //        else if (Request.Browser.IsMobileDevice)
        //        {
        //            tipoDispositivo = "Movil";
        //        }
        //        else
        //        {
        //            tipoDispositivo = "PC";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("El error es : " + ex.Message);
        //    }

        //    _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
        //    _BELog.Ip = ip;
        //    _BELog.HostName = hostName;
        //    _BELog.Pagina = pagina;
        //    _BELog.Agente = agente;
        //    _BELog.TipoDispositivo = tipoDispositivo;

        //    int val = _objBLLog.spi_Log(_BELog);
        //}

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //Grabar_Log();
                }
                catch (Exception)
                {
                    //throw;
                }

                lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";
            }

            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }

            CargaInformacionGeneral();


        }


        //modificando
        protected void CargaInformacionGeneral()
        {
            int identificador = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;

            DataSet ds = new DataSet();
            ds = _objBLFinanciamiento.spMON_SEACE_BienesServicio_Detalle(identificador);
            dt = ds.Tables[0];

            string pFlagAdjudicado = "";
            if (dt.Rows.Count > 0)
            {
                pFlagAdjudicado = dt.Rows[0]["flagAdjudicado"].ToString();
                lblIdentificador.Text = dt.Rows[0]["IDENTIFICADOR"].ToString();
                lblEntidadConvocante.Text = dt.Rows[0]["ENTIDAD"].ToString();
                
                lblDetalle.Text = "DETALLE DEL PROCESO DE SELECCION DE " + dt.Rows[0]["OBJETO"].ToString();
                               
                lblDescripcion.Text = dt.Rows[0]["DescripcionItem"].ToString();
                //lblDescripcionItem.Text = dt.Rows[0]["DescripcionItem"].ToString();
                lblTipoSeleccion.Text = dt.Rows[0]["TipoSeleccion"].ToString();
                lblModalidad.Text = dt.Rows[0]["MODALIDAD"].ToString();
                lblNormativa.Text = dt.Rows[0]["NORMATIVA"].ToString();
                lblSistemaContratacion.Text = dt.Rows[0]["Sistema_Contratacion"].ToString();
                lblNomenclatura.Text = dt.Rows[0]["NOMENCLATURA"].ToString();
                lblFechaPublicacion.Text = dt.Rows[0]["FechaPublicacion"].ToString();
                lblValorReferencial.Text = dt.Rows[0]["ValorReferencial"].ToString();
                if (!lblValorReferencial.Text.Equals(""))
                {
                    lblValorReferencial.Text = Convert.ToDecimal(lblValorReferencial.Text).ToString("N2");
                }

                lblEstadoItem.Text = dt.Rows[0]["EstadoItem"].ToString();
                lblFechaBuenaPRo.Text = dt.Rows[0]["FechaBuenaPro"].ToString();
                lblFechaBuenaProConsentida.Text = dt.Rows[0]["FechaBuenaProConsentida"].ToString();
                lblValorAdjudicado.Text = dt.Rows[0]["ValorAdjudicadoItem"].ToString();
                if (!lblValorAdjudicado.Text.Equals(""))
                {
                    lblValorAdjudicado.Text = Convert.ToDecimal(lblValorAdjudicado.Text).ToString("N2");
                }
                lblMoneda.Text = dt.Rows[0]["MONEDA"].ToString();
                lblNroContrato.Text = dt.Rows[0]["NroCONTRATO"].ToString();
                lblFechaContrato.Text = dt.Rows[0]["FechaContrato"].ToString();
                string urlContrato = dt.Rows[0]["urlContrato"].ToString();

                if (urlContrato.Length > 0)
                {
                    StringBuilder strUrlContrato = new StringBuilder();
                    strUrlContrato.Append("");

                    strUrlContrato.Append("<a style ='text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000' href = '" + urlContrato + "' target ='_blank' title='Ver contrato'>");
                    strUrlContrato.Append("<img src='../img/pdf_48x48.png'  width='32px' /></ a >");

                    lblURLContrato.Text = strUrlContrato.ToString();
                }

                lblRucGanador.Text = dt.Rows[0]["RUC_CODIGO_GANADOR"].ToString();
                lblEmpresaGanador.Text = dt.Rows[0]["GANADOR"].ToString();

                string pTipoProveedor = dt.Rows[0]["TIPO_PROVEEDOR"].ToString();
                String TipoGanador = dt.Rows[0]["CONSORCIO"].ToString();

                if (pTipoProveedor.Equals("Consorcio"))
                {
                    lblNommEmpresa.Text = "3.1. CONSORCIO";

                    trEmpresa1.Visible = true;
                    trEmpresa2.Visible = true;
                    trEmpresa3.Visible = true;
                    trEmpresa4.Visible = true;

                    trConsorcio1.Visible = true;
                    trConsorcio2.Visible = true;

                    DataView dv = new DataView(dt);
                    dv.RowFilter = "CONSORCIO='S'";

                    DataTable distinctValues = dv.ToTable(true, "GANADOR", "NOMBRE_MIEMBRO_CONSORCIO", "RUC_MIEMBRO_CONSORCIO", "PorcentajeParticipacion");

                    DataTable dtTemp = dv.ToTable(true, "GANADOR", "NOMBRE_MIEMBRO_CONSORCIO", "RUC_MIEMBRO_CONSORCIO", "PorcentajeParticipacion", "UrlGarantia");

                    Session["consorcio"] = dtTemp;

                    grdConsorcio.DataSource = distinctValues;
                    grdConsorcio.DataBind();
                }
                else
                {
                    if (TipoGanador.Equals("N"))
                    {
                        trEmpresa1.Visible = true;
                        trEmpresa2.Visible = true;
                        trEmpresa3.Visible = true;
                        trEmpresa4.Visible = true;
                        //trEmpresa5.Visible = true;


                        DataRow[] foundRows;
                        foundRows = dt.Select("CONSORCIO='N'");

                        if (foundRows.Count() > 0)
                        {
                            StringBuilder strUrlGarantias = new StringBuilder();
                            strUrlGarantias.Append("");

                            foreach (DataRow dr in foundRows)
                            {
                                if (dr["UrlGarantia"].ToString().Count() > 0)
                                {
                                    strUrlGarantias.Append("<a style ='text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000' href = '" + dr["UrlGarantia"].ToString() + "' target ='_blank' title='Ver contrato'>");
                                    strUrlGarantias.Append("<img src='../img/pdf_48x48.png'  width='32px' /></ a >");
                                }
                            }

                            lblUrlGarantias.Text = strUrlGarantias.ToString();
                        }
                    }
                }

                if (pFlagAdjudicado.Equals("1"))
                {
                    // En caso mas de un item convocado
                    if (ds.Tables[1].Rows.Count > 1)
                    {
                        string vItemTotal = "";

                        tblResultadoIndividual.Visible = false;
                        string htmlItem = "";
                        decimal fValorItemTotal = 0;
                        htmlItem = htmlItem + "<table cellspacing = '0' style = 'font-family: Calibri; font-size: 13px; width: 100%' cellpadding = '5'>";
                        int iItem=0;
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            iItem = iItem + 1;
                            string pValorItemReferencial = "";

                            if (!row["ValorReferencial"].ToString().Equals(""))
                            {
                                pValorItemReferencial = Convert.ToDecimal(row["ValorReferencial"].ToString()).ToString("N2");
                                fValorItemTotal = fValorItemTotal + Convert.ToDecimal(row["ValorReferencial"].ToString());
                            }
                            vItemTotal = vItemTotal + row["DESCRIPCION_ITEM"].ToString() + " (<b>Valor Referencial:</b> " + pValorItemReferencial + ")</br>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td colspan = '2' style = 'text-align:left; font-weight:bold'> ITEM " + iItem + ": " + row["DESCRIPCION_ITEM"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray; width:148px;'>ESTADO DE ITEM</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["EstadoItem"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>FECHA DE BUENA PRO</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["FechaBuenaPro"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>FECHA DE BUENA PRO CONSENTIDA</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["fechaBuenaProConsentida"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            string pValorItemAdjudicado = "";
                            if (!row["ValorAdjudicadoItem"].ToString().Equals(""))
                            {
                                pValorItemAdjudicado = Convert.ToDecimal(row["ValorAdjudicadoItem"].ToString()).ToString("N2");
                            }
                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>VALOR ADJUDICADO ITEM</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + pValorItemAdjudicado + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>MONEDA</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["Moneda"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>N° CONTRATO</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["NroContrato"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>FECHA DE CONTRATO</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["FechaContrato"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            StringBuilder strUrlContrato = new StringBuilder();
                            strUrlContrato.Append("");
                            strUrlContrato.Append("<a style ='text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000' href = '" + row["urlContrato"].ToString() + "' target ='_blank' title='Ver contrato'>");
                            strUrlContrato.Append("<img src='../img/pdf_48x48.png'  width='32px' /></ a >");

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>VER CONTRATO</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + strUrlContrato.ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>RUC / CODIGO GANADOR</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["RUC_CODIGO_GANADOR"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";

                            htmlItem = htmlItem + "<tr>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'>GANADOR</td>";
                            htmlItem = htmlItem + "<td class='celdaTabla' style='border: 1px dotted gray'> " + row["GANADOR"].ToString() + "</td>";
                            htmlItem = htmlItem + "</tr>";
                        }
                        htmlItem = htmlItem + "</table></br>";
                        lblTableMultiple.Text = htmlItem;

                        lblDescripcion.Text = vItemTotal;
                        lblValorReferencial.Text = fValorItemTotal.ToString("N2");
                    }
                }
                else
                {
                    // En caso mas de un item convocado
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        string vItemTotal = "";
                        decimal fValorItemTotal = 0;
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            string pValorItemReferencial = "";
                            if (!row["ValorReferencial"].ToString().Equals(""))
                            {
                                pValorItemReferencial = Convert.ToDecimal(row["ValorReferencial"].ToString()).ToString("N2");
                                fValorItemTotal = fValorItemTotal + Convert.ToDecimal(row["ValorReferencial"].ToString());
                            }
                            vItemTotal = vItemTotal + row["DescripcionItem"].ToString() + " (<b>Valor Referencial:</b> " + pValorItemReferencial + ")</br>";
                        }
                        lblDescripcion.Text = vItemTotal;
                        lblValorReferencial.Text = fValorItemTotal.ToString("N2");
                    }
                }
            }

        }

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "SEACE_FICHA_" + lblIdentificador.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }

        protected void imgbtnContrato_Click(object sender, ImageClickEventArgs e)
        {
            string script = "<script>window.open('" + lblURLContrato.Text + "','_blank') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        }

        protected void grdConsorcio_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNombreConsorcio = (Label)e.Row.FindControl("NOMBRE_MIEMBRO_CONSORCIO");
                Label lblUrl = (Label)e.Row.FindControl("lblUrlGarantias");

                DataTable dt = (DataTable)Session["consorcio"];

                if (dt.Rows.Count > 0)
                {
                    StringBuilder strUrlGarantias = new StringBuilder();
                    strUrlGarantias.Append("");

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["UrlGarantia"].ToString().Count() > 0 && lblNombreConsorcio.Text.Equals(dr["NOMBRE_MIEMBRO_CONSORCIO"].ToString()))
                        {
                            strUrlGarantias.Append("<a style ='text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000' href = '" + dr["UrlGarantia"].ToString() + "' target ='_blank' title='Ver contrato'>");
                            strUrlGarantias.Append("<img src='../img/pdf_48x48.png'  width='32px' /></ a >");
                        }
                    }

                    lblUrl.Text = strUrlGarantias.ToString();
                }

            }
        }
    }
}