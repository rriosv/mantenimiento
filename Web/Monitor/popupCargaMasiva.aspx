﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="popupCargaMasiva.aspx.cs" Inherits="Web.Monitor.popupCargaMasiva" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--<table border="0" style="width: 100%">
                <tr>
                   <td style="width: 70%">
                       <div style="text-align: right">
                            <asp:Button ID="Button1" runat="server" Text="Cerrar" Height="21px" OnClick="Button1_Click" Width="48px" />
                       </div>
                   </td>
                </tr>
            </table>--%>

            <table border="0" style="width: 100%">
                <tr>
                   <td style="width: 70%">
                        <asp:FileUpload ID="FileUpload" runat="server" />
                        <asp:Button Text="Cargar" runat="server" OnClick="Upload_Click" />
                    </td>
                    <td style="width: 30%">
                        <div style="text-align: right">
                            <asp:HyperLink ID="HyperLink1" CssClass="btn btn-default" NavigateUrl="~/Documentos/Padron_Beneficiario(formato).xlsx" runat="server">Descargar Formato</asp:HyperLink>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <hr />
        <div runat="server" Visible="false" id="divError">
            <%--<h6>Los siguientes registros no se cargaron. Por favor verifique en el excel</h6>--%>
            <asp:GridView ID="grillaError" runat="server" AutoGenerateColumns="false"
                CellPadding="2" CellSpacing="1" BorderStyle="None" ForeColor="#333333" GridLines="None"
                BorderWidth="1px" BackColor="Silver" Font-Bold="False" Font-Names="Calibri" Font-Size="Small" Font-Strikeout="False" Font-Underline="False">
                
                <EditRowStyle BackColor="#999999" />
                <FooterStyle
                    BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>
                <PagerStyle ForeColor="White"
                    HorizontalAlign="Center" BackColor="#284775"></PagerStyle>
                <HeaderStyle ForeColor="White" Font-Bold="true"
                    BackColor="#5D7B9D"></HeaderStyle>

            
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

            
                <Columns>
                    
                    <asp:BoundField HeaderText="#" DataField="Numero" />
                    <asp:BoundField HeaderText="APELLIDO PATERNO" DataField="apellidoPaterno" />
                    <asp:BoundField HeaderText="APELLIDO MATERNO" DataField="apellidoMaterno" />
                    <asp:BoundField HeaderText="NOMBRES" DataField="nombres" />
                    <asp:BoundField HeaderText="SEXO" DataField="sexo" />
                    <asp:BoundField HeaderText="FECHA NACIMIENTO" DataField="vfechaNacimiento" />
                    <asp:BoundField HeaderText="TIPO DOC." DataField="Tipo_DocMiembroNE" />
                    <asp:BoundField HeaderText="DNI" DataField="dni" />
                    <asp:BoundField HeaderText="TIPO DOMICILIO" DataField="Tipo_Domicilio" />
                    <asp:BoundField HeaderText="DIRECCION" DataField="direccionDomicilio" />
                    <asp:BoundField HeaderText="N° DOMICILIO" DataField="nroDomicilio" />
                    <asp:BoundField HeaderText="TIPO BENEFICIO" DataField="Tipo_Beneficio" />
                    <asp:BoundField HeaderText="ESTADO BENEFICIARIO" DataField="Estado_Beneficiario" />
                    <asp:BoundField HeaderText="N° MIEMBROS H." DataField="nroMiembrosHombres" />
                    <asp:BoundField HeaderText="N° MIEMBROS M." DataField="nroMiembrosMujeres" />
                    <asp:BoundField HeaderText="CONEXION AGUA" DataField="Conexion_Agua" />
                    <asp:BoundField HeaderText="UBS" DataField="Ubs" />
                    <asp:BoundField HeaderText="TIPO UBS" DataField="Tipo_Ubs" />
                    <asp:BoundField HeaderText="ESTADO PREDIO" DataField="Estado_Predio" />
                    <asp:BoundField HeaderText="OBSERVACIONES" DataField="observaciones" />
                    <asp:BoundField HeaderText="Detalle Error" DataField="Detalle_Error" />


                </Columns>
                <SelectedRowStyle ForeColor="#333333" Font-Bold="true" Font-Names="Calibri"
                    BackColor="#E2DED6"></SelectedRowStyle>
                <RowStyle Font-Names="Calibri"
                    BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
