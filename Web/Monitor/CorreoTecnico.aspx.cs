﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using Business;
using Entity;
using System.Net.Mail;
using System.Net;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class CorreoTecnico : System.Web.UI.Page
    {
        BLUtil _BLUtil = new BLUtil();
        BEProyecto _BEProyecto = new BEProyecto();
        BLProyecto objBLProyecto = new BLProyecto();
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
                Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");


        }

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Grabar_Log();
                string correo = "", texto = "";
                string correoUsuario = "";
                string CC = "";

                BEProyecto ObjBEProyecto = new BEProyecto();
                ObjBEProyecto.Id_proyecto = Convert.ToInt32(Request.QueryString["id"]); ;
                DataTable dtProyecto = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);

                texto = "Estimad@s: </br></br> ";

                texto = texto + "Mucho se agradecería la actualización con caracter de urgencia del proyecto: <span style=font-weight:bold;color:#0000FF>" + dtProyecto.Rows[0]["nom_proyecto"].ToString() + " </span> ";
                if (dtProyecto.Rows[0]["cod_snip"].ToString() == "0")
                {
                    texto = texto + ". </br></br>";
                }
                else
                {
                    texto = texto + "con <span style=font-weight:bold;color:#0000FF> SNIP " + dtProyecto.Rows[0]["cod_snip"].ToString() + "</span>. </br></br> ";
                }

                texto = texto + "Saludos" + "</br></br> ";
                texto = texto + "<Table style='font-size:12px; font-style:italic'><tr><td >Sistema de Seguimiento de Proyectos - SSP</td></tr><tr><td>Oficina General de Estadística e Informática - OGEI</td></tr></Table>";
                ckeditor.Text = texto;

                //DataTable objcorreoT = _BLUtil.spSOL_Informacion(12, id_tecnico, null, 0);
                //if (objcorreoT.Rows.Count > 0) correo = correo + objcorreoT.Rows[0]["correo"].ToString() + ",";

                List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();
                ListBandeja = objBLbandeja.F_spMON_CorreobyIdProyecto(ObjBEProyecto.Id_proyecto);

                foreach (BE_MON_BANDEJA item in ListBandeja)
                {
                    //if (item.nivel == "C")
                    //{
                    //    CC = CC + item.correo + ',';
                    //}
                    //else
                    //{
                    correo = correo + item.correo + ',';
                    //}

                }

                DataTable objcorreoU = _BLUtil.spSOL_Informacion(10, Convert.ToInt32(Session["IdUsuario"]), null, 0);
                if (objcorreoU.Rows.Count > 0)
                {
                    correoUsuario = correoUsuario + objcorreoU.Rows[0]["correo"].ToString();
                }

                if (correo.Length > 0)
                {
                    correo = correo.Substring(0, correo.Length - 1);
                }

                txtpara.Text = correo;
                txtCC.Text = correoUsuario;
                txtasunto.Text = "Actualización de proyecto - SNIP " + dtProyecto.Rows[0]["cod_snip"].ToString();
                ckeditor.Focus();
            }
        }
        protected void btncancelar_Click(object sender, EventArgs e)
        {
            string script = "<script>close();</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }
        protected void btnenviar_Click(object sender, EventArgs e)
        {
            string script;
            try
            {
                BLUtil _BLUtil = new BLUtil();
                MailMessage mail = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                mail.To.Add(txtpara.Text);
                mail.Subject = txtasunto.Text;
                if (txtCC.Text.Length > 0)
                {
                    mail.CC.Add(txtCC.Text);
                }

                mail.Body = ckeditor.Text;
                mail.IsBodyHtml = true;
                smtp.Send(mail);
                script = "<script>alert('Se envio el correo'); close();</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            catch (Exception)
            {

                script = "<script>alert('Error no se envio el correo - verificar');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }
    }
}