﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ficha_Proyecto.aspx.cs" Inherits="Web.Monitor.Ficha_Proyecto" Culture="es-PE" UICulture="es-PE"  %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>FICHA TECNICA DE PROYECTO</title>
    <link rel="Stylesheet" href="../css/print.css" type="text/css" media="print" />
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }
        .center {
            margin:auto;
        }
        @media print {
            .btnPrint {
                display: none !important;
                position: absolute;
            }
        }

        .tdimprimir {
            background-color: #E0E0E0;
            height: 35px;
            vertical-align: middle;
            text-align: center;
            font-family: Arial;
        }
        .centrar {
            margin:auto;
            
        }
    </style>

    <script type="text/javascript">
        function imprimir(nombre) {
            print();
            //document.getElementById("icon_word").style.display = 'none';
            //document.getElementById("title").style.display = 'inline';
            //document.getElementById("div_imagen").style.display = 'none';
            //var ficha = document.getElementById(nombre);
            //var ventimp = window.open(' ', 'popimpr');
       
            //ventimp.document.write(ficha.innerHTML);
            //ventimp.document.close();
            //ventimp.print();
            //document.getElementById("icon_word").style.display = 'inline';
            //document.getElementById("title").style.display = 'none';
            //document.getElementById("div_imagen").style.display = 'inline';
            //ventimp.close();
        }

        function imprimir_PMIB(nombre) {
            print();

            //            document.getElementById("lnk_imprimir").style.display = 'none';
            //            var ficha = document.getElementById(nombre);
            //            var ventimp = window.open(' ', 'popimpr');
            //            ventimp.document.write('<head><link rel="STYLESHEET" type="text/css" href="../CSS/Monitoreo.css"/></head>');
            //            //            ventimp.document.write('<head><link rel="STYLESHEET" type="text/css" href="../CSS/print_Imprimir.css" media="print" /></head>');
            //            ventimp.document.write(ficha.innerHTML);
            //            ventimp.document.close();
            //           // ventimp.print();/
            //            document.getElementById("lnk_imprimir").style.display = 'inline';
            //            ventimp.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        </asp:ScriptManager>
        <center>
            
            <div id="div_Ficha_PMIB" runat="server" style="width: 750px; height: auto;">
                <center>
                    <div id="div_imagen" style="margin-bottom:5px;">
                        <img src="../img/LogoMVCS1-estado.png" height="50" alt="" />
                    </div>
                    
                    <span style="font-family: 'Arial'; font-size: 16pt"><b>FICHA T&Eacute;CNICA DE PROYECTO</b></span>
                    <br />
                    
                <asp:Label runat="server" Font-Size="9pt"  Text="" ID="lblFechaInforme"></asp:Label>
                    <div id="icon_word">
                        <center>
                            <table style="width: 100%">
                             <tr>
                            <td align="right">
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnExportar_Click" ToolTip="Exportar a PDF." />
                            </td>
                                <td align="left">
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                           <%--     <tr>
                                    <td align="center">

                                        <a href="javascript:imprimir_PMIB('div_Ficha_PMIB')" alt="Imprimir" title="Imprimir" style="border: none">
                                            <img src="../img/print.png" class="btnPrint" alt="" width="25px" height="25px" style="border: none" /></a>

                                    </td>
                                </tr>--%>
                                                           
                            </table>
                        </center>
                    </div>
                 
                    <div style="width: 100%; text-align: left; padding-top:5px; padding-bottom:5px;">
                        <span style="font-family: 'Arial'; font-size: 10pt"><b>DATOS GENERALES:</b></span>
                    </div>
                  
                    <div style="width: 100%; font-family: 'Arial'; font-size: 9pt">
                        <table style="width: 100%" cellpadding="5">
                            <tbody>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">C&Oacute;DIGO SNIP</td>
                                    <td class="celdaTabla" style="border: 1px solid gray"><b>
                                        <asp:Label ID="lblSnip_PMIB" runat="server" Text=""></asp:Label></b></td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">NOMBRE DEL PROYECTO</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblNom_Proyecto_PMIB" runat="server" Text=""></asp:Label></td>
                                </tr>

                                 <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">MONTO DE INVERSI&Oacute;N TOTAL</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">

                                        <asp:Label ID="lblMoneda_PMIB" runat="server" Text="S/."></asp:Label><asp:Label ID="lblMonto1_PMIB" runat="server" Text=""></asp:Label>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">POBLACI&Oacute;N BENEFICIADA</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblPoblación_PMIB" runat="server" Text=""></asp:Label>
                                         <asp:Label ID="Label8" runat="server" Text=" Habitantes (Fuente Formato SNIP 03)"></asp:Label>
                                    </td>
                                </tr>

                                 <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">ESTADO SITUACIONAL</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblEstado_PMIB" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">UBICACI&Oacute;N</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <div>
                                            <table>
                                                <tr>
                                                    <td>Distrito&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                                                    <td>
                                                        <asp:Label ID="lblDistrito_PMIB" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>Provincia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                                                    <td>
                                                        <asp:Label ID="lblProvincia_PMIB" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>Departamento :</td>
                                                    <td>
                                                        <asp:Label ID="lblDepartamento_PMIB" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">APORTES</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <div>
                                            <table cellpadding="2" cellspacing="0">
                                                <tr>
                                                    <td align="center">MVCS :</td>
                                                    <td style="width: 80px;">
                                                        <asp:Label ID="lblPorcentajeMontoMVCS_PMIB" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                    </td>
                                                    <td>Monto : S/.</td>
                                                    <td>
                                                        <asp:Label ID="lblMontoSNIPTab0" runat="server" Text="0" Width="90px"></asp:Label>
                                                        <asp:TextBox ID="txtMontoSNIPTab0" Visible="false" Style="text-align: center" Width="80px" runat="server" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">MUNIC. : </td>
                                                    <td>
                                                        <asp:Label ID="lblPorcentajeMontoMUNIC_PMIB" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                    </td>
                                                    <td>Monto : S/.</td>
                                                    <td>
                                                        <asp:Label ID="lblMontoMUNIC_PMIB" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">BENEF. :</td>
                                                    <td>
                                                        <asp:Label ID="lblPorcentajeMontoBENEF_PMIB" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                    </td>
                                                    <td>Monto : S/.</td>
                                                    <td>
                                                        <asp:Label ID="lblMontoBENEF_PMIB" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">TOTAL<br />
                                                        APORTES :</td>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="(100%) "></asp:Label>

                                                    </td>
                                                    <td>Monto : S/.</td>
                                                    <td>
                                                        <asp:Label ID="txtTotalInversionTab0" runat="server" Text="0"></asp:Label>
                                                        <asp:TextBox ID="txtAcumuladogrdContraTab0" runat="server" Style="text-align: center"
                                                            Enabled="false" Visible="false" Width="60px" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">UNIDAD EJECUTORA</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblOperador_PMIB" runat="server" Text=""></asp:Label></td>
                                </tr>
                                
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">MODALIDAD DE EJECUCI&Oacute;N</td>
                                    <td class="celdaTabla" style="border: 1px solid gray"><b>
                                        <asp:Label ID="lblModalidad_PMIB" runat="server" Text=""></asp:Label></b></td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">PLAZO DE EJECUCI&Oacute;N CONTRACTUAL</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblPlazo_PMIB" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text="(Días Calendario)"></asp:Label></td>
                                </tr>
                               
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">CONTRATISTA</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblContratista_PMIB" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">DISPOSITIVO LEGAL DE<br />
                                        TRANSFERENCIA</td>
                                    <td class="celdaTabla" style="border: 1px solid gray; vertical-align: middle">
                                       <%-- <b>
                                        <asp:Label ID="lblDispositivoLegal_PMIB" runat="server" Text=""></asp:Label></b>--%>
                                         <asp:Label ID="lblMsjTransferencia" runat="server" Text="No hubo transferencia hasta el momento."></asp:Label><br />
                            <br />
                            <dx:ASPxGridView ID="xgrdTransferencias" runat="server" CssClass="center">
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="dispositivo_aprobacion" VisibleIndex="0" Caption="Dispositivo de transferencia">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="strfecha_aprobacion" VisibleIndex="1" Caption="Fecha">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="montoAprobacion" VisibleIndex="6" UnboundType="Decimal" Caption="Monto Transferido (S/.)">
                                        <FooterCellStyle ForeColor="Black" Font-Bold="true" />

                                        <PropertiesTextEdit DisplayFormatString="N2" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                                <TotalSummary>

                                    <dx:ASPxSummaryItem FieldName="montoAprobacion" SummaryType="Sum" Tag="TOTAL" DisplayFormat="C" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">FECHA DE INICIO DE OBRA</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblfinicio_PMIB" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">FECHA DE TERMINO REAL</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">
                                        <asp:Label ID="lblffin_PMIB" runat="server" Text=""></asp:Label></td>
                                </tr>
                                
                               
                                <tr>
                                    <td class="celdaTabla" style="border: 1px solid gray; width: 250px">METAS F&Iacute;SICAS</td>
                                    <td class="celdaTabla" style="border: 1px solid gray">

                                         <asp:Label runat="server" ID="lblMetas"></asp:Label>

                                     
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br />

                    <asp:Image ID="imgProyectoPMIB" BorderStyle="Solid" BorderWidth="4px" Width="350px" BorderColor="#c8130b" runat="server" Visible="false" />
                    <asp:Image ID="imgProyectoPMIB2" BorderStyle="Solid" BorderWidth="4px" Width="350px" BorderColor="#c8130b" runat="server" Visible="false" />
                    <br />

                </center>
            </div>
        </center>
    </form>
</body>
</html>
