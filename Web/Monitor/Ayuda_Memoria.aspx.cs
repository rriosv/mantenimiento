﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Web.Monitor
{
    public partial class Ayuda_Memoria : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
      
        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        DataTable dt = new DataTable();

        string pUrl="";
        string pfilepath="";

        protected void Page_Load(object sender, EventArgs e)
        {
                      
            // GRABAR LOG
            //RegisterAsyncTask(new PageAsyncTask(AsynRegistrarLog));
           

            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["idtipo"] != null && Request.QueryString["id"].Length > 0 && Request.QueryString["idtipo"].Length > 0)
                {
                    lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";

                    if (Request.Browser.IsMobileDevice && imgbtnExportar.Visible == true)
                    {
                        imgbtnImprimir.Visible = false;
                        imgbtnExportar.Visible = false;
                        imgbtnExportarMobile.Visible = true;
                    }

                    CargaInformacionGeneral();

                    try
                    {
                        string t = Request.QueryString["t"].ToString();

                        if (t == "p")
                        {
                            imgbtnImprimir.Visible = false;
                            imgbtnExportar.Visible = false;
                        }
                    }
                    catch
                    { }

                }
                else
                {
                    lblTitle.Text = "NOTA: PARAMETROS INCORRECTOS.";

                    string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        //modificando
        protected void CargaInformacionGeneral()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;
            _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());


            dt = _objBLLiquidacion.spMON_Ayuda_Memoria(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {

                lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblUnico.Text = dt.Rows[0]["iCodUnificado"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblMontoSnip.Text = dt.Rows[0]["MONTO_SNIP"].ToString();
                lblMontoInversionTotalMEF.Text = dt.Rows[0]["monto_inversion"].ToString();

                if (_BELiquidacion.tipoFinanciamiento == 0)
                {
                    _BELiquidacion.tipoFinanciamiento= Convert.ToInt32(dt.Rows[0]["id_TipoFinanciamiento"].ToString());
                }

                string fechaMEFUpdate = dt.Rows[0]["FechaUpdateMEF"].ToString();
                lblMsjMontoInversion.Text = " (Fuente - SSI al " + fechaMEFUpdate + ")";

                lblMontoConvenio.Text = dt.Rows[0]["MONTO_CONVENIO"].ToString();
                lblEjecutora.Text = dt.Rows[0]["UNIDAD_EJECUTARORA"].ToString();
                lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOS"].ToString()).ToString("N0");

                if (dt.Rows[0]["id_tipoPrograma"].ToString().Equals("2"))
                {
                    if (Convert.ToInt32(dt.Rows[0]["BENEFICIARIOSDIRECTOS"].ToString()) > 0)
                    {
                        lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOSDIRECTOS"].ToString()).ToString("N0") + " directos";
                    }
                    else
                    {
                        lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOS"].ToString()).ToString("N0")+ " Habitantes (Fuente Formato SNIP 03)";
                    }
                }
                else
                {
                    lblBeneficiario.Text = lblBeneficiario.Text + " Habitantes (Fuente Formato SNIP 03)";
                }
                

                BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
                List<BE_MON_Financiamiento> ListTransferencia = new List<BE_MON_Financiamiento>();
                ListTransferencia = _objBLTransferencia.F_spSOL_TransferenciaByProyecto(Convert.ToInt32(Request.QueryString["id"].ToString()));
                xgrdTransferencias.DataSource = ListTransferencia;
                xgrdTransferencias.DataBind();

                if (ListTransferencia.Count > 0)
                {
                    decimal sum = ListTransferencia.Where(s => s.Id == "1" | s.Id == "2").Select(c => Convert.ToDecimal(c.montoAprobacion)).Sum();
                    decimal sumPIA = ListTransferencia.Where(s => s.Id == "3").Select(c => Convert.ToDecimal(c.montoAprobacion)).Sum();
                    decimal conv = Convert.ToDecimal(lblMontoConvenio.Text);
                    string val;
                    if (conv == 0)
                    {
                        val = "0.00";
                        lblMsjTransferencia.Text = "Se ha transferido S/" + sum.ToString("N") + ".";
                        LblAsignacionPIA.Text = "Se ha asignado a través del PIA S/" + sumPIA.ToString("N") + ".";
                    }
                    else
                    {
                        val = ((sum * 100) / conv).ToString("N");
                        lblMsjTransferencia.Text = "Se ha transferido S/" + sum.ToString("N") + " (" + val + "%).";

                        val = ((sumPIA * 100) / conv).ToString("N");
                        LblAsignacionPIA.Text = "Se ha asignado a través del PIA  S/" + sumPIA.ToString("N") + " (" + val + "%).";
                    }
                }

                lblFechaInicio1.Text = dt.Rows[0]["FECHAINICIO"].ToString();
                lblFechaTerminoContractual.Text = dt.Rows[0]["FechaTerminoContractual"].ToString();
                lblFechaTerminoReal.Text = dt.Rows[0]["FECHA_FIN"].ToString();
                lblPlazoEjecucion1.Text = dt.Rows[0]["PLAZOEJECUCION"].ToString();

                lblEstadoSituacional.Text = dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();
                lblPorcentajeAvance.Text = dt.Rows[0]["FISICOREAL"].ToString();
                lblAvanceProgramado.Text = dt.Rows[0]["FisicoProgramado"].ToString();
                lblAvanceFinanciero.Text = dt.Rows[0]["AvanceFinanciero"].ToString();
                lblAvanceFinanciero.Text = Convert.ToDouble(lblAvanceFinanciero.Text).ToString("N") + "% (Fuente - SSI)";

                lblModalidad1.Text = dt.Rows[0]["MODALIDAD"].ToString();

                string vIdTipo = dt.Rows[0]["id_tipoSubModalidadFinanciamiento"].ToString();


                if (vIdTipo.Equals("1")) // TRANSFERENCIA
                {
                    if (lblModalidad1.Text == "Directa")
                    {
                        trd1.Visible = false;
                        trd2.Visible = false;
                        trd3.Visible = false;
                        trd4.Visible = false;
                        //trd5.Visible = false;
                        lblCargoAyudaMemoria.Text = "Jefe Supervisor";
                    }
                }
                else if (vIdTipo.Equals("3")) // POR CONTRATA
                {

                    trMonTotalAyudaMemoria.Visible = false;
                    trAportesAyudaMemoria.Visible = false;
                    //trMontoCompromAyudaMemoria.Visible = false;
                    trTransferenciasAyudaMemoria.Visible = false;
                    lblCargoAyudaMemoria.Text = "Jefe Supervisor";
                    lblMsjTransferencia.Visible = false;
                    xgrdTransferencias.Visible = false;

                    PanelTransferencia.Visible = false;

                    if (xgrdTransferencias.Rows.Count > 0)
                    {
                        PanelTransferencia.Visible = true;
                        xgrdTransferencias.Visible = true;
                        lblMsjTransferencia.Visible = true;
                        trTransferenciasAyudaMemoria.Visible = true;
                    }
                }

                if (dt.Rows[0]["NroContratoSupervision"].ToString().Equals("1"))
                {
                    lblNomContratista.Text = "Contratista/Consorcio Obra";
                }
                else
                {
                    lblNomContratista.Text = "Consorcio Obra";
                }

                lblContratista.Text = dt.Rows[0]["CONTRATISTA"].ToString();

                lblNroContratoSupervision.Text = dt.Rows[0]["NroContratoSupervision"].ToString();
                lblNroContratoObra.Text = dt.Rows[0]["NroContratoObra"].ToString();

                lblMontoContratado.Text = dt.Rows[0]["MONTOCONSORCIO"].ToString();
                lblMontoContratado.Text = Convert.ToDouble(lblMontoContratado.Text).ToString("N");

                lblEmpresaSupervisora.Text = dt.Rows[0]["EmpresaSupervisor"].ToString();

                lblMontoSupervision.Text = dt.Rows[0]["MontoSupervisor"].ToString();
                lblMontoSupervision.Text = Convert.ToDouble(lblMontoSupervision.Text).ToString("N");


                lblSupervisor.Text = dt.Rows[0]["SUPERVISORDESIGNADO"].ToString();
                lblResidente.Text = dt.Rows[0]["RESIDENTEOBRA"].ToString();
                lblInspector.Text = dt.Rows[0]["INSPECTOR"].ToString();

                if (_BELiquidacion.tipoFinanciamiento == 3) //OBRA
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                    List<BE_MON_Ejecucion> _ListAvance = _objBLEjecucion.F_MON_DetalleSituacional(_BEEjecucion);
                    if (_ListAvance.Count > 0)
                    {
                        string htmlDetalleSituacional = "";
                        string htmlAcciones = "";
                        int contador = 0;

                        for (int i = 0; i < _ListAvance.Count && i < 5; i++)
                        {
                            if (_ListAvance.ElementAt(i).Evaluacion.Length > 0)
                            {
                                htmlDetalleSituacional = htmlDetalleSituacional + string.Format("<li>{0}: {1}</li>", _ListAvance.ElementAt(i).Date_fechaVisita.ToString("dd/MM/yyyy"), _ListAvance.ElementAt(i).Evaluacion.ToString());
                                contador = contador + 1;
                            }

                            if (_ListAvance.ElementAt(i).Recomendacion.Length > 0)
                            {
                                htmlAcciones = htmlAcciones + string.Format("<li>{0}: {1}</li>", _ListAvance.ElementAt(i).Date_fechaVisita.ToString("dd/MM/yyyy"), _ListAvance.ElementAt(i).Recomendacion.ToString());
                            }
                        }

                        if (htmlDetalleSituacional.Length > 0)
                        {
                            htmlDetalleSituacional = "<ul>" + htmlDetalleSituacional + "</ul>";
                            divDetalle.InnerHtml = htmlDetalleSituacional;
                        }

                        if (htmlAcciones.Length > 0)
                        {
                            htmlAcciones = "<ul>" + htmlAcciones + "</ul>";
                            divAcciones.InnerHtml = htmlAcciones;
                        }
                    }
                }
                else //PERFIL Y EXPEDIENTE
                {
                    trEtapaFinanciamiento.Visible = true;
                    lblNomEtapa.Text = dt.Rows[0]["TIPO_PROYECTO"].ToString();
                    lblNomContratista.Text = "Contratista/Consorcio";
                    lblNomMontoContrato.Text = "Monto Contratado";
                    lblNomNroContrato.Text = "Nro Contrato";
                    lblNomEstado.Text = "ESTADO " + lblNomEtapa.Text;
                    trResidente.Visible = false;
                    trInspector.Visible = false;

                    _BEEjecucion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                    //_BEEjecucion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());
                    _BEEjecucion.tipoFinanciamiento = _BELiquidacion.tipoFinanciamiento;

                    DataTable dtAvanceFisico = new DataTable();
                    dtAvanceFisico = _objBLEjecucion.spMON_AvanceFisico(_BEEjecucion);

                    DataRow[] result = dtAvanceFisico.Select("flagAyuda = '1'");
                    if (result.Count() > 0)
                    {
                        string htmlDetalleSituacional = "";
                        string htmlAcciones = "";

                        int contador = 0;
                        int totalA = result.Count();

                        for (int i = 0; i < result.Count() && i < 10; i++)
                        {
                            if (result[totalA - 1]["situacion"].ToString().Length > 0)
                            {
                                htmlDetalleSituacional = htmlDetalleSituacional + string.Format("<li>{0}: {1}</li>", result[totalA - 1]["fecha"].ToString(), result[totalA - 1]["situacion"].ToString());
                                contador = contador + 1;
                                totalA = totalA - 1;
                            }

                            if (vIdTipo.Equals("3") && result[totalA]["accionesRealizadas"].ToString().Length > 0) //POR CONTRATA
                            {
                                htmlAcciones = htmlAcciones + string.Format("<li>{0}: {1}</li>", result[totalA]["fecha"].ToString(), result[totalA]["accionesRealizadas"].ToString());
                            }
                        }

                        if (htmlDetalleSituacional.Length > 0)
                        {
                            htmlDetalleSituacional = "<ul>" + htmlDetalleSituacional + "</ul>";
                            divDetalle.InnerHtml = htmlDetalleSituacional;
                        }

                        if (htmlAcciones.Length > 0)
                        {
                            htmlAcciones = "<ul>" + htmlAcciones + "</ul>";
                            divAcciones.InnerHtml = htmlAcciones;
                        }
                    }

                }


                CargaFinanciamientoTab0(Convert.ToInt32(Request.QueryString["id"]));
                int id_solicitudes = Convert.ToInt32(dt.Rows[0]["id_solicitudes"].ToString());

                //MUESTRA METAS

                int id_tipoPrograma = Convert.ToInt32(dt.Rows[0]["id_tipoPrograma"].ToString());
                Metas(id_solicitudes, id_tipoPrograma);
                              

                DataSet ds = new DataSet();
                ds = _objBLFinanciamiento.spMEF_Financiamiento(Convert.ToInt32(lblSnip.Text));

                grdDetalleSOSEMTab0.DataSource = ds.Tables[0];
                grdDetalleSOSEMTab0.DataBind();

                Double resultadoTotal = 0;
                string vFecha = "";// = DateTime.Now.ToString("dd/MM/yyyy");
                if (ds.Tables[1].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    //resultadoTotal = ListSosem.Sum(elemento => Convert.ToDouble(elemento.devAcumulado));
                    resultadoTotal = Convert.ToDouble(dt.Compute("Sum(devAcumulado)", ""));
             
                    vFecha = ds.Tables[1].Rows[0][0].ToString();
                }
                else if (ds.Tables[2].Rows.Count>0)
                {
                    vFecha = ds.Tables[2].Rows[0][0].ToString();
                }

                lblMsjSosem.Text = "Con fecha " + vFecha + " la Unidad Ejecutora cuenta con un monto devengado de S/ " + resultadoTotal.ToString("N") + " de acuerdo al Sistema SSI - MEF .";

            }
            else
            {
                lblTitle.Text = "NOTA: NO EXISTE INFORMACIÓN PARA MOSTRAR.";

                string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }


        }

        //public async Task AsynRegistrarLog()
        //{
        //    BL_LOG _objBLLog = new BL_LOG();
        //    BE_LOG _BELog = new BE_LOG();

        //    string ip;
        //    string hostName;
        //    string pagina;
        //    string tipoDispositivo = "";
        //    string agente = "";

        //    ip = (Request.ServerVariables["REMOTE_ADDR"] == null?"":Request.ServerVariables["REMOTE_ADDR"].ToString());
        //    hostName = (Request.ServerVariables["remote_addr"] == null ? "" : (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName));
        //    pagina = HttpContext.Current.Request.Url.AbsoluteUri;

        //    if (Request.ServerVariables["HTTP_USER_AGENT"] != null)
        //    {
        //        string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
        //        agente = uAg;
        //        Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
        //        bool isMobile = regEx.IsMatch(uAg);
        //        if (isMobile)
        //        {
        //            tipoDispositivo = "Movil";
        //        }
        //        else if (Request.Browser.IsMobileDevice)
        //        {
        //            tipoDispositivo = "Movil";
        //        }
        //        else
        //        {
        //            tipoDispositivo = "PC";
        //        }
        //    }

        //    if (Session["IdUsuario"] != null)
        //    {
        //        _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
        //    }
        //    else
        //    {
        //        _BELog.Id_usuario = 0;
        //    }
            
        //    _BELog.Ip = ip;
        //    _BELog.HostName = hostName;
        //    _BELog.Pagina = pagina;
        //    _BELog.Agente = agente;
        //    _BELog.TipoDispositivo = tipoDispositivo;

        //    int val = _objBLLog.spi_Log(_BELog);
         
        //}

        protected void CargaFinanciamientoTab0(int idProy)
        {

            //Colocando resumen de financiamientp etapa - https://trello.com/c/uqVY2SZM/41-obras
            DataSet ds = new DataSet();
            ds = _objBLFinanciamiento.paSSP_MON_rFinanciamiento(idProy);
            DataTable dtSumatorias = new DataTable();
            dtSumatorias = ds.Tables[0];

            double montoMVCS = 0;
            double montoCofinanciamiento = 0;
            double porcCofinanciamiento = 0;
            double porcMVCS = 0;

            if (dtSumatorias.Rows.Count > 0)
            {
                montoMVCS = Convert.ToDouble(dtSumatorias.Rows[0]["SumMontoTransferido"]);
            }


            DataTable dtCofinanciamiento = new DataTable();
            dtCofinanciamiento = ds.Tables[1];

            if (dtCofinanciamiento.Rows.Count > 0)
            {
                montoCofinanciamiento = Convert.ToDouble(dtCofinanciamiento.Rows[0]["SumCofinanciamiento"]);
            }

            double Total = montoMVCS + montoCofinanciamiento;

            lblTotalInversionTab0.Text = Total.ToString("N"); ;
            lblMontoMVCSTransferido.Text = montoMVCS.ToString("N");
            lblMontoFirmantes.Text = montoCofinanciamiento.ToString("N");

            if (montoMVCS != 0) { porcMVCS = (montoMVCS * 100) / Total; }
            if (montoCofinanciamiento != 0) { porcCofinanciamiento = (montoCofinanciamiento * 100) / Total; }

            lblPorcentajeMontoMVCS.Text = porcMVCS.ToString("N") + "%";
            lblPorcentajeMontoFirmantes.Text = porcCofinanciamiento.ToString("N") + "%";
        }
                
        private void Metas(int id, int iTipoPrograma)
        {
            int pIdProyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            DataSet ds = objBLProyecto.paSSP_SOL_rMetasAyudaMemoria(pIdProyecto, 2,1); // 1: Calidad; 2:Monitoreo

            DataTable dtSistema = ds.Tables[0];
            DataTable dtMetas = ds.Tables[1];

            string htmlProgTotal = "";

            if (dtMetas.Rows.Count > 0)
            {
                foreach (DataRow rowSistema in dtSistema.Rows)
                {
                    string htmlProg = "";
                    foreach (DataRow rowP in dtMetas.Rows)
                    {
                        if (rowP["idSistema"].ToString().Equals(rowSistema["idSistema"].ToString()))
                        {
                            htmlProg = htmlProg +
                                       "<tr>" +
                                            "<td> <ul style = 'margin:0px;-moz-padding-start: 25px;' ><li>" + rowP["Componente"].ToString() + "</li></ul></td>" +
                                       "</tr>";
                        }
                    }

                    if (htmlProg.Length > 0)
                    {
                        htmlProgTotal = htmlProgTotal +
                                    "<table class='table'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        htmlProg +
                                    "</table>";
                    }


                }

                StringBuilder strMetas = new StringBuilder();
                strMetas.Append(htmlProgTotal);
                lblMetas.Text = strMetas.ToString();
            }
            else if (id > 0 && (iTipoPrograma==1 || iTipoPrograma == 3 || iTipoPrograma == 7 ))
            {

                DataTable _obj;
                BEVarPnsu _objVarPnsu = new BEVarPnsu();
                _objVarPnsu.Id_Solicitudes = id;
                _obj = objBLProyecto.spSOL_Seguimiento_Variable_PNSU(_objVarPnsu);
                if (_obj.Rows.Count > 0)
                {
                    StringBuilder strMetasPNSU = new StringBuilder();
                    strMetasPNSU.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");

                    //AGUAR POTABLE
                    if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) == 0)
                    {
                    }
                    else
                    {
                        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Agua Potable</td></tr>");

                        if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) != 0)
                        {

                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Captación.</li></ul></td>", _obj.Rows[0]["a_captacion_nuc"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_conduccion_cacd"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_conduccion_nuc"].ToString());

                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de conducción.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_conduccion_cacd"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_conduccion_nuc"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua potable.</li></ul></td>", _obj.Rows[0]["a_trataPotable_num"].ToString(), _obj.Rows[0]["a_trataPotable_cap"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_impulsion_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_impulsion_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de impulsion.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Estación de bombeo.</li></ul></td>", _obj.Rows[0]["a_estacion_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Reservorio(s).</li></ul></td>", _obj.Rows[0]["a_reservorio_nur"].ToString(), _obj.Rows[0]["a_reservorio_carm"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_aduccion_nua"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_aduccion_caad"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de linea(s) de aducción.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_redes_nur"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_redes_card"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de  Redes de distribución.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucn"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucr"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Piletas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucp"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }



                    }

                    //ALCANTARILLADO
                    if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_planta_capl"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString()) == 0.00)
                    {
                    }
                    else
                    {
                        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Alcantarillado</td></tr>");

                        if (Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_efluente_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de linea(s) efluente de agua residual tratada.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua(s) residual.</li></ul></td>", _obj.Rows[0]["s_planta_nup"].ToString(), _obj.Rows[0]["s_planta_capl"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_agua_nua"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de emisor(s).</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_impulsion_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de impulsión.</li></ul></td>", dTotalMetas.ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Camara de Bombeo.</li></ul></td>", _obj.Rows[0]["s_camara_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_colectores_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de Redes de alcantarillado sanitario.</li></ul></td>", dTotalMetas.ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucn"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucr"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) básica de saneamiento.</li></ul></td>", _obj.Rows[0]["s_conexion_nuclo"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }
                    }

                    //DRENAJE PLUVIAL
                    if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString()) == 0.00)
                    {
                    }
                    else
                    {

                        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Drenaje Pluvial</td></tr>");

                        if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_tuberia_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de tuberia de conexión.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_cuneta_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de cunetas.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de boca de tormenta.</li></ul></td>", _obj.Rows[0]["d_tormenta_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de conexión.</li></ul></td>", _obj.Rows[0]["d_conexion_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de inspección.</li></ul></td>", _obj.Rows[0]["d_inspeccion_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_secundario_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de colector(s) secundario.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_principal_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de colector(s) principal.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }
                    }

                    strMetasPNSU.Append("</table>");
                    lblMetas.Text = strMetasPNSU.ToString();


                }
            }

            //StringBuilder strMetas = new StringBuilder();
            //strMetas.Append(htmlProgTotal);

            //DataTable dtAgua = ds.Tables[0];
            //DataTable dtAlcantarillado = ds.Tables[1];
            //if (dtAgua.Rows.Count > 0 || dtAlcantarillado.Rows.Count>0)
            //{
            //    StringBuilder strMetasPNSU = new StringBuilder();
            //    strMetasPNSU.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");
            //    //AGUA POTABLE
            //    if (dtAgua.Rows.Count > 0)
            //    {
            //        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Agua Potable</td></tr>");
            //        foreach (DataRow dr in dtAgua.Rows)
            //        {
            //            strMetasPNSU.Append("<tr>");
            //            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0}</li></ul></td>", dr[0].ToString()));
            //            strMetasPNSU.Append("</tr>");
            //        }
            //    }

            //    //ALCANTARILLADO
            //    if (dtAlcantarillado.Rows.Count > 0)
            //    {
            //        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Alcantarillado</td></tr>");
            //        foreach (DataRow dr in dtAlcantarillado.Rows)
            //        {
            //            strMetasPNSU.Append("<tr>");
            //            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0}</li></ul></td>", dr[0].ToString()));
            //            strMetasPNSU.Append("</tr>");
            //        }
            //    }

            //    strMetasPNSU.Append("</table>");
            //    lblMetas.Text = strMetasPNSU.ToString();
            //}
            //else if(id>0) 
            //{

            //    DataTable _obj;
            //    BEVarPnsu _objVarPnsu = new BEVarPnsu();
            //    _objVarPnsu.Id_Solicitudes = id;
            //    _obj = objBLProyecto.spSOL_Seguimiento_Variable_PNSU(_objVarPnsu);
            //    if (_obj.Rows.Count > 0)
            //    {
            //        StringBuilder strMetasPNSU = new StringBuilder();
            //        strMetasPNSU.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");

            //        //AGUAR POTABLE
            //        if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) == 0)
            //        {
            //        }
            //        else
            //        {
            //            strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Agua Potable</td></tr>");

            //            if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) != 0)
            //            {

            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Captación.</li></ul></td>", _obj.Rows[0]["a_captacion_nuc"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_conduccion_cacd"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_conduccion_nuc"].ToString());

            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de conducción.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_conduccion_cacd"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_conduccion_nuc"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua potable.</li></ul></td>", _obj.Rows[0]["a_trataPotable_num"].ToString(), _obj.Rows[0]["a_trataPotable_cap"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_impulsion_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_impulsion_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de impulsion.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Estación de bombeo.</li></ul></td>", _obj.Rows[0]["a_estacion_num"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Reservorio(s).</li></ul></td>", _obj.Rows[0]["a_reservorio_nur"].ToString(), _obj.Rows[0]["a_reservorio_carm"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_aduccion_nua"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_aduccion_caad"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de linea(s) de aducción.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_redes_nur"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_redes_card"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de  Redes de distribución.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucn"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucr"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Piletas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucp"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }



            //        }

            //        //ALCANTARILLADO
            //        if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_planta_capl"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString()) == 0.00)
            //        {
            //        }
            //        else
            //        {
            //            strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Alcantarillado</td></tr>");

            //            if (Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_efluente_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de linea(s) efluente de agua residual tratada.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua(s) residual.</li></ul></td>", _obj.Rows[0]["s_planta_nup"].ToString(), _obj.Rows[0]["s_planta_capl"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_agua_nua"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de emisor(s).</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_impulsion_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de impulsión.</li></ul></td>", dTotalMetas.ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Camara de Bombeo.</li></ul></td>", _obj.Rows[0]["s_camara_num"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_colectores_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de Redes de alcantarillado sanitario.</li></ul></td>", dTotalMetas.ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucn"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucr"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) básica de saneamiento.</li></ul></td>", _obj.Rows[0]["s_conexion_nuclo"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }
            //        }

            //        //DRENAJE PLUVIAL
            //        if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) == 0 &&
            //                Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString()) == 0.00 &&
            //                Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString()) == 0.00)
            //        {
            //        }
            //        else
            //        {

            //            strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Drenaje Pluvial</td></tr>");

            //            if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_tuberia_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de tuberia de conexión.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_cuneta_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de cunetas.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de boca de tormenta.</li></ul></td>", _obj.Rows[0]["d_tormenta_num"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de conexión.</li></ul></td>", _obj.Rows[0]["d_conexion_num"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) != 0)
            //            {
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de inspección.</li></ul></td>", _obj.Rows[0]["d_inspeccion_num"].ToString()));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_secundario_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de colector(s) secundario.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }

            //            if (Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) != 0)
            //            {
            //                double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_principal_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString());
            //                strMetasPNSU.Append("<tr>");
            //                strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de colector(s) principal.</li></ul></td>", dTotalMetas.ToString("N2")));
            //                strMetasPNSU.Append("</tr>");
            //            }
            //        }

            //        strMetasPNSU.Append("</table>");
            //        lblMetas.Text = strMetasPNSU.ToString();


            //    }
            //}
        }

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
           string strnom = "AyudaMemoria_SNIP_" + lblSnip.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }

    }
}