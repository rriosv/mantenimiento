﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Ficha_MitigacionRiesgo_Plantilla : System.Web.UI.Page
    {
        BL_MON_Riesgo _objBLRiesgo = new BL_MON_Riesgo();
        BE_MON_Riesgo _BERiesgo = new BE_MON_Riesgo();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        protected void Page_Load(object sender, EventArgs e)
        {
            LblID_PROYECTO.Text = (Request.QueryString["id"].ToString());
            
            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            _BELiquidacion.tipoFinanciamiento = 3;

            DataTable dt = _objBLLiquidacion.spMON_Ayuda_Memoria(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {

                lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblUnidadEjecutora.Text = dt.Rows[0]["UNIDAD_EJECUTARORA"].ToString();
                lblModalidadEjecucion.Text = dt.Rows[0]["MODALIDAD"].ToString();
                lblID_Programa.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                lblID_TipoFinanciamiento.Text = dt.Rows[0]["id_tipoSubModalidadFinanciamiento"].ToString();
                lblBeneficiario.Text = Convert.ToDouble(dt.Rows[0]["BENEFICIARIOS"].ToString()).ToString("N0");
                             

                if (lblID_TipoFinanciamiento.Text.Equals("4"))  // NE
                {
                    trTransferencia.Visible = false;
                    trModalidad.Visible = false;
                }
                else
                {
                    BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
                    List<BE_MON_Financiamiento> ListTransferencia = new List<BE_MON_Financiamiento>();
                    ListTransferencia = _objBLTransferencia.F_spSOL_TransferenciaByProyecto(Convert.ToInt32(Request.QueryString["id"].ToString()));

                    if (ListTransferencia.Count > 0)
                    {
                        decimal sum = ListTransferencia.Select(c => Convert.ToDecimal(c.montoAprobacion)).Sum();
                        lblMontoTransferido.Text = "S/" + sum.ToString("N2");
                    }
                }
                

                string vEstado =  dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();
                if (vEstado.IndexOf("-") > 0)
                {
                    vEstado = vEstado.Substring(0, vEstado.IndexOf("-") - 1);
                }
                     
                lblEstado.Text = vEstado;
                
            }

            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    //imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }


            CargaPlantillaFichaRiesgoTab9(0);
        }

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }



            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);

        }
              

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "ActaVerificacion_SNIP_" + lblSnip.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }

        protected void CargaPlantillaFichaRiesgoTab9(int pIdRiesgo)
        {

            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            DataTable dt = _objBLRiesgo.spMON_RiesgosFicha(pIdProyecto, pIdRiesgo);
            Session["FichasRiesgoTab9"] = dt;

            string vEstado = "0";
            if (lblEstado.Text.Length > 5)
            {
                if (lblEstado.Text.Substring(0, 5).Equals("Actos"))
                {
                    vEstado = "41";
                }
            }

            if (lblID_TipoFinanciamiento.Text.Equals("4")) // SOLO POR NUCLEO EJECUTOR
            {
                if (lblID_Programa.Text.Equals("1") || lblID_Programa.Text.Equals("3")) //SANEAMIENTO
                {
                    if (vEstado.Equals("41"))
                    {
                        lblTitle.Text = "Seguimiento a la Fase 01: Actos Previos";
                        tblFirmas.Visible = false;
                    }
                    else
                    {
                        lblTitle.Text = "Acta de Cumplimiento de Obligaciones Contractuales Esenciales <br> (Modalidad Núcleo Ejecutor)";
                        lblNota1.Text = "La presente acta es elaborada en función de la información proporcionada por el Núcleo Ejecutor, teniendo en cuenta presunción de la veracidad reflejado en la tomas fotográficas, y en la suscripción del acta respectiva.";
                    }
                }
                else //PNVR
                {
                    if (vEstado.Equals("41"))
                    {
                        int valorItem5 = Convert.ToInt32(Request.QueryString["pTipo"].ToString());

                        if (valorItem5 == 2)
                        {
                            lblTitle.Text = "Acta de Cumplimiento de Obligaciones Contractuales Esenciales <br> (Modalidad Núcleo Ejecutor)";
                            lblNota1.Text = "La presente acta es elaborada en función de la información proporcionada por el Núcleo Ejecutor, teniendo en cuenta presunción de la veracidad reflejado en la tomas fotográficas, y en la suscripción del acta respectiva.";

                            lblPersonal2.Text = "Gestor Social";
                            lblPersonal3.Text = "";
                            lblPersonal4.Text = "";
                        }
                        else
                        {
                            lblTitle.Text = "Seguimiento a la Fase 01: Actos Previos";
                            tblFirmas.Visible = false;
                        }
                    }
                    else
                    {
                        lblTitle.Text = "Acta de Cumplimiento de Obligaciones Contractuales Esenciales <br> (Modalidad Núcleo Ejecutor)";
                        lblNota1.Text = "La presente acta es elaborada en función de la información proporcionada por el Núcleo Ejecutor (Contratista y Supervisión), teniendo en cuenta presunción de la veracidad reflejado en la tomas fotográficas, y en la suscripción del acta respectiva.";
                    }
                }
            }
            else
            {
                if (vEstado.Equals("41"))
                {
                    lblTitle.Text = "Seguimiento al Proceso de Selección";
                    tblFirmas.Visible = false;
                }
                else
                {
                    lblTitle.Text = "Acta de Cumplimiento de Obligaciones Contractuales Esenciales";
                }
            }


            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    tr.ID = "trRiesgoTab9i" + dr["id_itemRiesgo"].ToString();

                    if (dr["nivel"].ToString().Equals("0"))
                    {
                        tr.Style["border"] = "1px solid";
                    }

                    
                    //COLOR
                    HtmlTableCell td0 = new HtmlTableCell();
                    td0.ID = "tdRiesgo0Tab9i" + dr["id_itemRiesgo"].ToString();
                    td0.Style["width"] = "10px";
                    td0.Style["background-color"] = "#ffffff";
                    tr.Controls.Add(td0);

                    // NOMBRE de ITEM
                    HtmlTableCell td = new HtmlTableCell();
                    td.ID = "tdRiesgo1Tab9i" + dr["id_itemRiesgo"].ToString();
                    td.Attributes.Add("align", "left");
                    td.Style["padding-left"] = "10px";
                    td.Style["width"] = "40%";

                    Label lblNameTab9 = new Label();
                    lblNameTab9.ID = "lblItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString();
                    lblNameTab9.Text = dr["nro"].ToString() + dr["nombre"].ToString();

                    if (dr["nivel"].ToString().Equals("0"))
                    {
                        lblNameTab9.Style["font-weight"] = "bold";
                    }
                    td.Controls.Add(lblNameTab9);
                    tr.Controls.Add(td);

                    // SI NO
                    HtmlTableCell td2 = new HtmlTableCell();
                    td2.ID = "tdRiesgo2Tab9i" + dr["id_itemRiesgo"].ToString();
                    td2.Attributes.Add("align", "left");
                    td2.Style["padding-left"] = "10px";
                    td2.Style["width"] = "15%";

                    if (dr["nivel"].ToString().Equals("1"))
                    {
                        RadioButtonList rb = new RadioButtonList();
                        rb.ID = "rbItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString();
                        rb.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                        rb.Items.Add(new ListItem("SI", "1"));
                        rb.Items.Add(new ListItem("NO", "0"));
                        rb.Enabled = false;
                        td2.Controls.Add(rb);
                    }
                    tr.Controls.Add(td2);

                    //Comentario
                    HtmlTableCell td3 = new HtmlTableCell();
                    td3.ID = "tdRiesgo3Tab9i" + dr["id_itemRiesgo"].ToString();
                    td3.Attributes.Add("align", "left");
                    td3.Style["padding-left"] = "10px";
                    td3.Style["width"] = "50%";

                    if (dr["nivel"].ToString().Equals("1") && (dr["id_itemRiesgo"].ToString().Equals("12") || dr["id_itemRiesgo"].ToString().Equals("2") || dr["id_itemRiesgo"].ToString().Equals("3") || dr["id_itemRiesgo"].ToString().Equals("4") || dr["id_itemRiesgo"].ToString().Equals("5") ))
                    {
                        TextBox txtComentario = new TextBox();
                        txtComentario.ID = "txtItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString();
                        //txtComentario.TextMode = TextBoxMode.MultiLine;
                        txtComentario.Style["width"] = "100%";
                        txtComentario.Style["height"] = "60px";
                        if (dr["flagRegistrar"].ToString().Equals("0"))
                        {
                            txtComentario.Enabled = false;
                        }
                        td3.Controls.Add(txtComentario);
                    }
                    tr.Controls.Add(td3);


                    //if (tblFirmas.Visible == true)
                    //{
                    //    if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) >5)
                    //        tblMitigacionTab9.Controls.Add(tr);
                    //}
                    //else
                    //{
                    //    if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) < 6)
                    //        tblMitigacionTab9.Controls.Add(tr);
                    //}

                    if (lblID_TipoFinanciamiento.Text.Equals("4")) //NE
                    {
                        if (lblID_Programa.Text.Equals("5"))//PNVR
                        {
                            if (tblFirmas.Visible == true)
                            {
                                //ACTOS PREVIOS
                                if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) > 32)
                                    tblMitigacionTab9.Controls.Add(tr);
                                if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) == 28)
                                    tblMitigacionTab9.Controls.Add(tr);
                            }
                            else
                            {
                                if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) < 33)
                                    tblMitigacionTab9.Controls.Add(tr);
                            }
                        }
                        else
                        {
                            if (tblFirmas.Visible == true)
                            {
                                if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) > 33)
                                    tblMitigacionTab9.Controls.Add(tr);
                            }
                            else
                            {
                                if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) < 34)
                                    tblMitigacionTab9.Controls.Add(tr);
                            }
                        }
                    }
                    else //Por Transferencia
                    {
                        if (tblFirmas.Visible == true)
                        {
                            if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) > 5)
                                tblMitigacionTab9.Controls.Add(tr);
                        }
                        else
                        {
                            if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) < 6)
                                tblMitigacionTab9.Controls.Add(tr);
                        }

                    }
                }
            }
        }

    
    }
}