﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Registro_ExpedienteTecnico : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();

        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();
        BE_MON_CONVENIO_DIRECTA _BE_MON_CONVENIO_DIRECTA = new BE_MON_CONVENIO_DIRECTA();

        BL_MON_SOSEM _objBLSOSEM = new BL_MON_SOSEM();
        BE_MON_SOSEM _BESOSEM = new BE_MON_SOSEM();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        DataTable dt = new DataTable();

        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();

        BL_MON_Historial _objBLHistorial = new BL_MON_Historial();
        BE_MON_Historial _BEHistorial = new BE_MON_Historial();

        BLUtil _Metodo = new BLUtil();

        BL_MON_Dec_Viabilidad _objBLDecViabilidad = new BL_MON_Dec_Viabilidad();
        BL_MON_RegistroInversion _objBLRegistroInversion = new BL_MON_RegistroInversion();
               
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {
                Response.Redirect("~/login.aspx?ms=1");
            }


            Session.Timeout = 120;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);

            if (!IsPostBack)
            {
                BtnRefrescar_Click(null, null);

                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                // Session["id_proyecto"] = Convert.ToInt32(LblID_PROYECTO.Text);

                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();
                lblID_PERFIL_USUARIO.Text = Session["PerfilUsuario"].ToString();
                lblCOD_SUBSECTOR.Text = Session["CodSubsector"].ToString();
                               
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int idacceso = _objBLProceso.spMON_Acceso(_BEProceso);
                lblID_ACCESO.Text = idacceso.ToString();

                if (idacceso == 0)
                {
                    OcultarRegistro();
                }

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);

                if (dt.Rows.Count > 0)
                {
                    lblNombProy.Text = dt.Rows[0]["nom_proyecto"].ToString();
                    lblSNIP.Text = dt.Rows[0]["cod_snip"].ToString();

                    if (lblSNIP.Text == "0")
                    {
                        trVigencia1.Visible = false;
                        trVigencia2.Visible = false;
                        trVigencia3.Visible = false;
                    }

                    ifrGraficoSosem.Attributes["src"] = "iframe_GraficoSOSEM?snip=" + lblSNIP.Text;

                    string depa = dt.Rows[0]["depa"].ToString();
                    string prov = dt.Rows[0]["prov"].ToString();
                    string dist = dt.Rows[0]["dist"].ToString();
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;

                    lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                    lblCOD_SUBSECTOR.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                    lblTipoFinanciamiento.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();
                                    

                    lblIdEstadoRegistro.Text = dt.Rows[0]["idEstadoProyecto"].ToString();
                    if (lblIdEstadoRegistro.Text.Equals("3"))
                    {
                        lblEstadoRegistro.Text = "FINALIZADO";
                        btnFinalizarTab3.Text = "Activar";
                    }
                    else
                    {
                        lblEstadoRegistro.Text = "ACTIVADO";
                        btnFinalizarTab3.Text = "Finalizar";
                    }

                }
                CargaTab0(Convert.ToInt32(LblID_PROYECTO.Text));
                CargaTab1();
                CargaTab2();
                CargaTab3();
                CargaTab4();
                CargaTab6();
                CargaTab7();
                CargaTab8();
                CargaTab9(Convert.ToInt32(LblID_PROYECTO.Text));

                if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 68)
                {
                    TabPanelProcesoSeleccion.Visible = false;
                    TabPanelEstadoEjecuccion.Visible = false;
                    TabPanelAdicional.Visible = false;
                    TabPanelLiquidacion.Visible = false;
                    TabPanelFotografico.Visible = false;
                    TabPanelConclusiones.Visible = false;

                }

                // COORDINADORES PMIB , PNSU, PNSR, PNVR, PASLC
                if (lblIdEstadoRegistro.Text.Equals("3") && (lblID_PERFIL_USUARIO.Text.Equals("23") || lblID_PERFIL_USUARIO.Text.Equals("24") || lblID_PERFIL_USUARIO.Text.Equals("71") || lblID_PERFIL_USUARIO.Text.Equals("74") || lblID_PERFIL_USUARIO.Text.Equals("76") || LblID_USUARIO.Text.Equals("481")))
                {
                    btnFinalizarTab3.Visible = true;
                }
                else if (lblIdEstadoRegistro.Text.Equals("3"))
                {
                    btnFinalizarTab3.Visible = false;
                }
            }

            //Agregar menu lateral por etapa - https://trello.com/c/uqVY2SZM/41-obras
            BL_MON_Menu oMenu = new BL_MON_Menu();

            List<BE_MON_Menu> ListMenu = new List<BE_MON_Menu>();
            ListMenu = oMenu.paSSP_MON_rEtapas(idProy, 3);
            int currentIndex = 0;

            for (int i = 0; i < ListMenu.Count; i++)
            {
                BE_MON_Menu menu = ListMenu[i];
                HtmlGenericControl li = new HtmlGenericControl("li");
                if (menu.idProyecto == idProy)
                {
                    li.Attributes.Add("class", "active");

                    //Colocando tipos de financiamiento - https://trello.com/c/uqVY2SZM/41-obras
                    DataSet dsTipo = new DataSet();
                    dsTipo = _objBLFinanciamiento.paSSP_MON_rTipoFinanciamientos(menu.idTipoFinanciamiento);
                    DataTable dtTipoFinanc = new DataTable();
                    dtTipoFinanc = dsTipo.Tables[0];

                    foreach (DataRow row in dtTipoFinanc.Rows)
                    {
                        slTipoFinanc.Items.Add(new ListItem(row["vNombre"].ToString(), row["idTIpoFinanciamiento"].ToString()));
                    }
                    currentIndex = i;
                }

                menuObra.Controls.Add(li);

                HtmlGenericControl anchor = new HtmlGenericControl("a");

                string aspxPage = "";
                switch (menu.idTipoFinanciamiento)
                {
                    case 1:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_PreInversion.aspx";
                        }
                        else
                        {
                            aspxPage = "Registro_PreInversion_PorTransferencia.aspx";
                        }
                        break;
                    case 2:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_ExpedienteTecnico.aspx";
                        }
                        else
                        {
                            aspxPage = "Registro_ExpedienteTecnico_PorTransferencia.aspx";
                        }
                        break;
                    case 3:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_EjecucionContrata.aspx";
                        }
                        else
                        {
                            if (menu.idModalidadFinanciamiento == 4)
                            { aspxPage = "Registro_NE.aspx"; }
                            else
                            {
                                if (menu.idModalidadFinanciamiento == 2)
                                {
                                    aspxPage = "Registro_Jica.aspx";
                                }
                                else { aspxPage = "Registro_Obra.aspx"; }
                            }
                        }
                        break;
                    default:
                        aspxPage = "Registro_ExpedienteTecnico.aspx";
                        break;
                }

                //anchor.Attributes.Add("href", "/Monitor/" + aspxPage + "?id=" + menu.idProyecto);
                anchor.Attributes.Add("href", "" + aspxPage + "?id=" + menu.idProyecto + "&token=" + Request.QueryString["token"]);
                anchor.InnerText = menu.tipoFinanciamiento;
                li.Controls.Add(anchor);
            }

            if (ListMenu.Count == currentIndex + 1)
            {
                btnNuevoProy.Disabled = false;
            }
                        
        }

        protected void OcultarRegistro()
        {
            //btnFlagFinalizarTab3.Visible = false;

            //if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 23 || Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 24)
            //{
            //    btnFlagDesFinalizarTab3.Visible = true;
            //}

            btnGuardarInfFinancieraTab0.Visible = false;
            imgbtn_agregarConvenioDirecta.Visible = false;
            grdConvenioDirectaTab0.Columns[0].Visible = false;
            grdConvenioDirectaTab0.Columns[9].Visible = false;

            imgbtn_agregarSOSEM.Visible = false;
            grdSosemTab0.Columns[0].Visible = false;
            grdSosemTab0.Columns[19].Visible = false;

            //btnAgregarContrapartidasTab0.Visible = false;
            //btnAgregarTransferenciaTab0.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            //imgbtn_agregarSOSEM.Visible = false;
            imgAgregaPlazoTab4.Visible = false;
            imgbtnPlaTab4.Visible = false;
            imgbtnPreTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = false;
            //btnGuardarTab7.Visible = false;
            btnGuardarTab3.Visible = false;
            btnGuardarTab2.Visible = false;
            imgbtn_CoordinadorTab2.Visible = false;
            btnGuardarTab1.Visible = false;

            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;

            grdSeguimientoProcesoTab1.Columns[0].Visible = false;
            grdSeguimientoProcesoTab1.Columns[11].Visible = false;
            grdConsultoresTab1.Columns[0].Visible = false;
            grdConsultoresTab1.Columns[4].Visible = false;

            grdAvanceFisicoTab2.Columns[0].Visible = false;
            grdAvanceFisicoTab2.Columns[14].Visible = false;
            imgbtnAvanceFisicoTab2.Visible = false;

            imgbtnAgregarParalizacionTAb4.Visible = false;
            grdParalizacionTab4.Columns[0].Visible = false;
            grdParalizacionTab4.Columns[11].Visible = false;
            imgbtnAgregarPenalidadTab4.Visible = false;
            grdPenalidadTab4.Columns[0].Visible = false;
            grdPenalidadTab4.Columns[7].Visible = false;

            grdPlazoTab4.Columns[0].Visible = false;
            grdPlazoTab4.Columns[11].Visible = false;
            imgAgregaPlazoTab4.Visible = false;
            grdPresupuestoTab4.Columns[0].Visible = false;
            grdPresupuestoTab4.Columns[12].Visible = false;
            grdDeductivoTab4.Columns[0].Visible = false;
            grdDeductivoTab4.Columns[12].Visible = false;

            grdPanelTab6.Columns[0].Visible = false;
            grdPanelTab6.Columns[8].Visible = false;
            imgbtnAgregarPanelTab6.Visible = false;

            //btnGuardarMetasPMIB.Visible = false;

            btnGuardarInversionTab9.Visible = false;
            imgbtn_AgregarRegistroEstudioTab9.Visible = false;
            grdRegistroEstudioTab9.Columns[0].Visible = false;
            grdRegistroEstudioTab9.Columns[6].Visible = false;
            imgbtn_AgregarSupervisionEstudioTab9.Visible = false;
            grdSupervisionEstudioTab9.Columns[0].Visible = false;
            grdSupervisionEstudioTab9.Columns[5].Visible = false;
            imgbtn_AgregarRevisionEstudioTab9.Visible = false;
            grdRevisionEstudioTab9.Columns[0].Visible = false;
            grdRevisionEstudioTab9.Columns[6].Visible = false;
            imgbtn_AgregarAprobacionETTab9.Visible = false;
            grdAprobacionETTab9.Columns[0].Visible = false;
            grdAprobacionETTab9.Columns[6].Visible = false;

            imgbtnAgregarHistorialTab8.Visible = false;
            grdHistorialTab8.Columns[0].Visible = false;
            grdHistorialTab8.Columns[7].Visible = false;

            imgbtnEditarUE.Visible = false;
            imgbtnEditarUbigeo.Visible = false;

       

            lnkbtnBuscarRUCConsorcioTab1.Visible = false;
            lnkbtnBuscarRUCContratistaTab1.Visible = false;
            lnkbtnBuscarRUCMiembroConsorcioTab1.Visible = false;

            btnFinalizarTab3.Visible = false;

            UPTabContainerDetalles.Update();
        }

 
       
        protected Boolean validaArchivoFotos(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".bmp" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".jpg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar solo fotos en formato: BMP, JPG, JPEG o PNG.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaArchivo(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaArchivo(FileUpload file, string titulo = "Archivo")
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('" + titulo + " solo hasta 10MB');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaFecha(TextBox textBox, string titulo, bool menorIgualFecActual = false, bool requerido = false)
        {

            if (requerido && textBox.Text == "")
            {
                string script = "<script>alert('Ingrese la " + titulo + ".');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (textBox.Text != "")
            {
                if (_Metodo.ValidaFecha(textBox.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de " + titulo + ".');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (menorIgualFecActual && Convert.ToDateTime(textBox.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('" + titulo + " no puede ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }
            }

            return true;
        }

        protected void GeneraIcoFile(string lnk, ImageButton imgbtn)
        {
            if (lnk != "")
            {

                string ext = lnk.Substring(lnk.Length - 3, 3);

                ext = ext.ToLower();

                if (ext == "pdf")
                {
                    imgbtn.ImageUrl = "~/img/pdf.gif";
                }

                if (ext == "xls" || ext == "lsx")
                {
                    imgbtn.ImageUrl = "~/img/xls.gif";

                }

                if (ext == "doc" || ext == "ocx")
                {
                    imgbtn.ImageUrl = "~/img/doc.gif";

                }

                if (ext == "png" || ext == "PNG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }
                if (ext == "jpg" || ext == "JPG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                imgbtn.ToolTip = lnk;
            }
            else
            {
                imgbtn.ToolTip = "";
                imgbtn.ImageUrl = "~/img/blanco.png";
            }

        }

        #region Tab0

        protected void CargaTab0(int idproy)
        {
            _BEFinanciamiento.id_proyecto = idproy;
            _BEFinanciamiento.tipoFinanciamiento = 2;

            CargaSOSEMEjecutorasTab0();
            CargaDdlTipoConvenio();
            CargaConveniosDirectaTab0();
            CargaFinanSOSEMTab0();

            //CargaDdlSistemaInversion();
            CargaDatosTab0();


        }

        protected Boolean ValidarConvenioDirectaTab0()
        {
            if (ddlTipoConvenioTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione el Tipo de Convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (txtEntidadConvenioTab0.Text=="")
            {
                string script = "<script>alert('Ingrese la entidad Firmante.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (txtDetalleConvenioTab0.Text == "")
            {
                string script = "<script>alert('Ingrese el detalle de convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (txtObjetoConvenioTab0.Text == "")
            {
                string script = "<script>alert('Ingrese el objeto de convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (!validaFecha(txtFechaConvenioTab0, "Fecha de Convenio", true, true))
                return false;

            if (!validaFecha(txtFechaVenceConvenioTab0, "Fecha de Vencimiento de Convenio", false, true))
                return false;

            DateTime fecConvenio = Convert.ToDateTime(txtFechaConvenioTab0.Text);
            DateTime fecVenceConvenio = Convert.ToDateTime(txtFechaVenceConvenioTab0.Text);
            if ((fecVenceConvenio <= fecConvenio))
            {
                string script = "<script>alert('La Fecha de Vencimiento debe ser mayor al de Convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            return true;
        }
        protected void CargaConveniosDirectaTab0()
        {
            int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdConvenioDirectaTab0.DataSource = _objBLFinanciamiento.F_spMON_ConvenioDirecta(id_proyecto);
            grdConvenioDirectaTab0.DataBind();

        }
        protected void CargaDdlTipoConvenio()
        {
            ddlTipoConvenioTab0.DataSource = _objBLFinanciamiento.spMON_rParametro(28);
            ddlTipoConvenioTab0.DataTextField = "nombre";
            ddlTipoConvenioTab0.DataValueField = "valor";
            ddlTipoConvenioTab0.DataBind();

            ddlTipoConvenioTab0.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }
        protected void imgbtn_agregarConvenioDirecta_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConvenioDirecta.Visible = true;
            imgbtn_agregarConvenioDirecta.Visible = false;
            btnGuardarRegistroConvenioDirectaTab0.Visible = true;
            btnActualizarRegistroConvenioDirectaTab0.Visible = false;
            lblNomUsuarioConvenioDirecta.Text = "";
            ddlTipoConvenioTab0.SelectedValue = "";
            txtDetalleConvenioTab0.Text = "";
            txtFechaConvenioTab0.Text = "";
            txtEntidadConvenioTab0.Text = "";
            txtObjetoConvenioTab0.Text = "";
            txtFechaVenceConvenioTab0.Text = "";
            Up_Tab0.Update();
        }
        protected void btnCancelarRegistroConvenioDirectaTab0_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConvenioDirecta.Visible = false;
            imgbtn_agregarConvenioDirecta.Visible = true;
            Up_Tab0.Update();

        }
        protected void btnGuardarRegistroConvenioDirectaTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarConvenioDirectaTab0())
            {
                _BE_MON_CONVENIO_DIRECTA.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE_MON_CONVENIO_DIRECTA.iTipoConvenio = Convert.ToInt32(ddlTipoConvenioTab0.SelectedValue);
                _BE_MON_CONVENIO_DIRECTA.detalle = txtDetalleConvenioTab0.Text;
                _BE_MON_CONVENIO_DIRECTA.vFechaConvenio = VerificaFecha(txtFechaConvenioTab0.Text).ToString();
                _BE_MON_CONVENIO_DIRECTA.entidadFirmante = txtEntidadConvenioTab0.Text;
                _BE_MON_CONVENIO_DIRECTA.objetoConvenio = txtObjetoConvenioTab0.Text;
                _BE_MON_CONVENIO_DIRECTA.vFechaVencimiento = VerificaFecha(txtFechaVenceConvenioTab0.Text).ToString();
                _BE_MON_CONVENIO_DIRECTA.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLFinanciamiento.spi_MON_ConvenioDirecta(_BE_MON_CONVENIO_DIRECTA);

                if (val == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaConveniosDirectaTab0();
                }
                else
                {
                    string script = "<script>alert('¡Vuelva a Intentarlo después.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                Panel_AgregarConvenioDirecta.Visible = false;
                imgbtn_agregarConvenioDirecta.Visible = true;
                Up_Tab0.Update();
            }

        }
        protected void btnActualizarRegistroConvenioDirectaTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarConvenioDirectaTab0())
            {
                _BE_MON_CONVENIO_DIRECTA.id_convenioDirecta = Convert.ToInt32(lblRegistroConvenioDirecta.Text);
                _BE_MON_CONVENIO_DIRECTA.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE_MON_CONVENIO_DIRECTA.iTipoConvenio = Convert.ToInt32(ddlTipoConvenioTab0.SelectedValue);
                _BE_MON_CONVENIO_DIRECTA.detalle = txtDetalleConvenioTab0.Text;
                _BE_MON_CONVENIO_DIRECTA.vFechaConvenio = VerificaFecha(txtFechaConvenioTab0.Text).ToString();
                _BE_MON_CONVENIO_DIRECTA.entidadFirmante = txtEntidadConvenioTab0.Text;
                _BE_MON_CONVENIO_DIRECTA.objetoConvenio = txtObjetoConvenioTab0.Text;
                _BE_MON_CONVENIO_DIRECTA.vFechaVencimiento = VerificaFecha(txtFechaVenceConvenioTab0.Text).ToString();
                _BE_MON_CONVENIO_DIRECTA.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLFinanciamiento.spu_MON_ConvenioDirecta(_BE_MON_CONVENIO_DIRECTA, 1);

                if (val == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaConveniosDirectaTab0();
                    ddlTipoConvenioTab0.SelectedValue = "";
                    txtDetalleConvenioTab0.Text = "";
                    txtFechaConvenioTab0.Text = "";
                    txtEntidadConvenioTab0.Text = "";
                    txtObjetoConvenioTab0.Text = "";
                    txtFechaVenceConvenioTab0.Text = "";
                }
                else
                {
                    string script = "<script>alert('¡Vuelva a Intentarlo después.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                Panel_AgregarConvenioDirecta.Visible = false;
                imgbtn_agregarConvenioDirecta.Visible = true;
                Up_Tab0.Update();
            }
        }
        protected void grdConvenioDirectaTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_convenioDirecta;
            id_convenioDirecta = grdConvenioDirectaTab0.SelectedDataKey.Value.ToString();
            lblRegistroConvenioDirecta.Text = id_convenioDirecta.ToString();
            GridViewRow row = grdConvenioDirectaTab0.SelectedRow;
            ddlTipoConvenioTab0.SelectedValue = ((Label)row.FindControl("lbliTipoConvenio")).Text;
            txtDetalleConvenioTab0.Text = Server.HtmlDecode(row.Cells[4].Text);
            txtFechaConvenioTab0.Text = Server.HtmlDecode(row.Cells[5].Text);
            txtEntidadConvenioTab0.Text = Server.HtmlDecode(row.Cells[6].Text);
            txtObjetoConvenioTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtFechaVenceConvenioTab0.Text = Server.HtmlDecode(row.Cells[8].Text);

            lblNomUsuarioConvenioDirecta.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            btnGuardarRegistroConvenioDirectaTab0.Visible = false;
            btnActualizarRegistroConvenioDirectaTab0.Visible = true;
            imgbtn_agregarConvenioDirecta.Visible = false;
            Panel_AgregarConvenioDirecta.Visible = true;
            Up_Tab0.Update();
        }
        protected void grdConvenioDirectaTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdConvenioDirectaTab0.DataKeys[CurrentRow.RowIndex].Value as object;
                _BE_MON_CONVENIO_DIRECTA.id_convenioDirecta = Convert.ToInt32(objTemp.ToString());
                _BE_MON_CONVENIO_DIRECTA.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLFinanciamiento.spu_MON_ConvenioDirecta(_BE_MON_CONVENIO_DIRECTA, 2);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaConveniosDirectaTab0();
                    Up_Tab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grdConvenioDirectaTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

    
        protected void CargaDatosTab0()
        {
            dt = _objBLFinanciamiento.F_spMON_FinanciamientoDirecta(Convert.ToInt32(LblID_PROYECTO.Text));

            if (dt.Rows.Count > 0)
            {
                txtCostoEEstudioTab0.Text = dt.Rows[0]["costoEtapa"].ToString();
                txtCostoSEstudioTab0.Text = dt.Rows[0]["costoSupervision"].ToString();
                string total = dt.Rows[0]["CostoTotal"].ToString();
                txtCostoTEstudioTab0.Text = (total == "" )? "" : string.Format("{0:N2}", decimal.Parse(total));

                lnkBtnCostoETab0.Text = dt.Rows[0]["urlCostoEtapa"].ToString();
                GeneraIcoFile(lnkBtnCostoETab0.Text, imgBtnCostoETab0);
                lnkBtnCostoSTab0.Text = dt.Rows[0]["urlCostoSupervision"].ToString();
                GeneraIcoFile(lnkBtnCostoSTab0.Text, imgBtnCostoSTab0);
                lnkBtnPlanETab0.Text = dt.Rows[0]["urlElaboracionEtapa"].ToString();
                GeneraIcoFile(lnkBtnPlanETab0.Text, imgBtnPlanEEstudioTab0);
                lnkBtnPlanSEstudioTab0.Text = dt.Rows[0]["urlElaboracionSupervision"].ToString();
                GeneraIcoFile(lnkBtnPlanSEstudioTab0.Text, imgBtnPlanSEstudioTab0);

                //ddlSistemaInversionTab0.SelectedValue = dt.Rows[0]["iTipoSistemaInversion"].ToString();

                //string strInfraestructuraAguaPotable = dt.Rows[0]["flagInfraestructuraAguaPotable"].ToString();
                //string strInfraestructuraAlcantarillado = dt.Rows[0]["flagInfraestructuraAlcantarillado"].ToString();
                //string strInfraestructuraAguaResidual = dt.Rows[0]["flagInfraestructuraAguaResidual"].ToString();
                //if (strInfraestructuraAguaPotable != "")
                //{
                //    switch (strInfraestructuraAguaPotable)
                //    {
                //        case "0": radioNo1.Checked = true; break;
                //        case "1": radioSi1.Checked = true; break;
                //        default: radioNoApp1.Checked = true; break;
                //    }
                //}

                //if (strInfraestructuraAlcantarillado != "")
                //{
                //    switch (strInfraestructuraAlcantarillado)
                //    {
                //        case "0": radioNo2.Checked = true; break;
                //        case "1": radioSi2.Checked = true; break;
                //        default: radioNoApp2.Checked = true; break;
                //    }
                //}

                //if (strInfraestructuraAguaResidual != "")
                //{
                //    switch (strInfraestructuraAguaResidual)
                //    {
                //        case "0": radioNo3.Checked = true; break;
                //        case "1": radioSi3.Checked = true; break;
                //        default: radioNoApp3.Checked = true; break;
                //    }
                //}

                lblNomUsuarioInfoFinanciera.Text = "Actualizó: " + dt.Rows[0]["usuarioFinanciamiento"].ToString() + " - " + dt.Rows[0]["fechaUpdateFinanciamiento"].ToString();
                //lblNomUsuarioInversion.Text = "Actualizó: " + dt.Rows[0]["usuarioSistemaInversion"].ToString() + " - " + dt.Rows[0]["fechaUpdateSistemaInversion"].ToString();
                //lblNomUsuarioPmi.Text = "Actualizó: " + dt.Rows[0]["usuarioServicio"].ToString() + " - " + dt.Rows[0]["fechaUpdateServicio"].ToString();
            }
        }
        protected void btnGuardarInfFinancieraTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarInfFinancieraTab0())
            {
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                string urlCostoEtapa = FileUploadCostoETab0.HasFile ? _Metodo.uploadfile(FileUploadCostoETab0) : lnkBtnCostoETab0.Text;
                string urlCostoSupervision = FileUploadCostoSTab0.HasFile ? _Metodo.uploadfile(FileUploadCostoSTab0) : lnkBtnCostoSTab0.Text;
                string urlElaboracionEtapa = FileUploadPlanETab0.HasFile ? _Metodo.uploadfile(FileUploadPlanETab0) : lnkBtnPlanETab0.Text;
                string urlElaboracionSupervision = FileUploadPlanSEstudioTab0.HasFile ? _Metodo.uploadfile(FileUploadPlanSEstudioTab0) : lnkBtnPlanSEstudioTab0.Text;
                decimal costoEtapa = 0;
                decimal.TryParse(txtCostoEEstudioTab0.Text, out costoEtapa);
                decimal costoSupervision = 0;
                decimal.TryParse(txtCostoSEstudioTab0.Text, out costoSupervision);

                int val = _objBLFinanciamiento.F__spu_MON_FinanciamientoDirecta(id_proyecto, costoEtapa, costoSupervision, urlCostoEtapa, urlCostoSupervision, urlElaboracionEtapa, urlElaboracionSupervision, "", id_usuario);
                if (val == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosTab0();
                    Up_Tab0.Update();
                }
                else
                {
                    string script = "<script>alert('¡Vuelva a Intentarlo después.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }

        }
        protected void lnkbtnDocTab0_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            Type tipo = sender.GetType();
            string NameArchivo = "";
            if (tipo.Name == "ImageButton")
            {
                String id = ((ImageButton)sender).ID;
                id = "lnk" + id.Substring(3);
                NameArchivo = ((LinkButton)UpdatePanel1.FindControl(id)).Text;
            }
            else
                NameArchivo = ((LinkButton)sender).Text;

            FileInfo toDownload = new FileInfo(pat + NameArchivo);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + NameArchivo);
                Response.End();
            }
        }
        protected Boolean ValidarInfFinancieraTab0()
        {
            if (!validaArchivo(FileUploadCostoETab0, "Documento D. Costos de Estudio"))
                return false;
            if (!validaArchivo(FileUploadCostoSTab0, "Documento D. Costos de Supervisión"))
                return false;
            if (!validaArchivo(FileUploadPlanETab0, "TDR/Plan de Trabajo Elab. Estudio"))
                return false;
            if (!validaArchivo(FileUploadPlanSEstudioTab0, " TDR/Plan de Trabajo Sup. Estudio"))
                return false;
            return true;
        }

      
        protected void btnDetalleDevengadoTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMDevengadoMensualizadoTab0(Convert.ToInt32(id));
            UpdatePanel3.Update();
            MPE_DetalleSOSEMTab0.Show();
        }
        protected void btnFuenteTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMFuenteFinanciamientoTab0(Convert.ToInt32(id));
            Up_FuenteFinanciamientoTAb0.Update();
            MPE_FuenteFinanciamientoTab0.Show();
        }
        protected void grdSosemTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento_sosem;
            id_seguimiento_sosem = grdSosemTab0.SelectedDataKey.Value.ToString();
            lblRegistroObras.Text = id_seguimiento_sosem.ToString();
            GridViewRow row = grdSosemTab0.SelectedRow;
            ddlanioSOSEMTAb0.SelectedValue = ((Label)row.FindControl("lblEntidad")).Text;
            txtPiaSOSEMTab0.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtPimAcumTab0.Text = Server.HtmlDecode(row.Cells[4].Text);
            txtDevAcumTab0.Text = Server.HtmlDecode(row.Cells[5].Text);
            txtEneSosTab0.Text = Server.HtmlDecode(row.Cells[6].Text);
            txtFebSosTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtMarSosTab0.Text = Server.HtmlDecode(row.Cells[8].Text);
            txtAbrSosTab0.Text = Server.HtmlDecode(row.Cells[9].Text);
            txtMaySosTab0.Text = Server.HtmlDecode(row.Cells[10].Text);
            txtJunSosTab0.Text = Server.HtmlDecode(row.Cells[11].Text);
            txtJulSosTab0.Text = Server.HtmlDecode(row.Cells[12].Text);
            txtAgoSosTab0.Text = Server.HtmlDecode(row.Cells[13].Text);
            txtSepSosTab0.Text = Server.HtmlDecode(row.Cells[14].Text);
            txtOctSosTab0.Text = Server.HtmlDecode(row.Cells[15].Text);
            txtNovSosTab0.Text = Server.HtmlDecode(row.Cells[16].Text);
            txtDicSosTab0.Text = Server.HtmlDecode(row.Cells[17].Text);
            txtComproAnualTab0.Text = Server.HtmlDecode(row.Cells[18].Text);
            lblNomUsuarioSosem.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            btnGuardarRegistroSOSEMTab0.Visible = false;
            btnActualizarRegistroSOSEMTab0.Visible = true;
            imgbtn_agregarSOSEM.Visible = false;

            Panel_AgregarSOSEM.Visible = true;
            Up_Tab0.Update();
        }
        protected void grdSosemTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSosemTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BESOSEM.id_seguimiento_sosem = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLSOSEM.spud_MON_SOSEM_ELIMINAR(_BESOSEM);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    CargaFinanSOSEMTab0();
                    Up_Tab0.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grdSosemTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            double total;
            int anio;
            total = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblMonto = (Label)e.Row.FindControl("lblMontoAprob");
                anio = Convert.ToInt32(((Label)e.Row.FindControl("lblEntidad")).Text);


                if (DateTime.Now.Year == anio)
                {
                    total = Convert.ToDouble(lblValorAvance.Text) + Convert.ToDouble(Server.HtmlDecode(e.Row.Cells[4].Text));

                }
                else
                {
                    total = Convert.ToDouble(lblValorAvance.Text) + Convert.ToDouble(Server.HtmlDecode(e.Row.Cells[5].Text));

                }

                lblValorAvance.Text = total.ToString("N");

                //if (Convert.ToDouble(txtTotalInversionTab0.Text) < total)
                //{
                //    lblAlertaSosemTab0.Visible = true;
                //}
                //else
                //{
                //    lblAlertaSosemTab0.Visible = false;
                //}



            }
        }
        protected void imgbtn_agregarSOSEM_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSOSEM.Visible = true;
            imgbtn_agregarSOSEM.Visible = false;
            btnGuardarRegistroSOSEMTab0.Visible = true;
            btnActualizarRegistroSOSEMTab0.Visible = false;
            lblNomUsuarioSosem.Text = "";
            Up_Tab0.Update();
        }
        protected void btnCancelarRegistroSOSEM_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSOSEM.Visible = false;
            imgbtn_agregarSOSEM.Visible = true;
            Up_Tab0.Update();

        }
        protected void btnGuardarRegistroSOSEMTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarSOSEMTab0())
            {
                _BESOSEM.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BESOSEM.tipoFinanciamiento = 2;

                _BESOSEM.anio = ddlanioSOSEMTAb0.SelectedValue;
                _BESOSEM.pia = txtPiaSOSEMTab0.Text;
                _BESOSEM.pimAcumulado = txtPimAcumTab0.Text;
                _BESOSEM.devAcumulado = txtDevAcumTab0.Text;
                _BESOSEM.ene = txtEneSosTab0.Text;
                _BESOSEM.feb = txtFebSosTab0.Text;
                _BESOSEM.mar = txtMarSosTab0.Text;
                _BESOSEM.abr = txtAbrSosTab0.Text;
                _BESOSEM.may = txtMaySosTab0.Text;
                _BESOSEM.jun = txtJunSosTab0.Text;
                _BESOSEM.jul = txtJulSosTab0.Text;
                _BESOSEM.ago = txtAgoSosTab0.Text;
                _BESOSEM.sep = txtSepSosTab0.Text;
                _BESOSEM.oct = txtOctSosTab0.Text;
                _BESOSEM.nov = txtNovSosTab0.Text;
                _BESOSEM.dic = txtDicSosTab0.Text;
                _BESOSEM.compromisoAnual = txtComproAnualTab0.Text;

                _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLSOSEM.spi_MON_FinanSOSEM(_BESOSEM);

                if (val == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaFinanSOSEMTab0();
                    ddlanioSOSEMTAb0.SelectedValue = "";
                    txtPiaSOSEMTab0.Text = "0";
                    txtPimAcumTab0.Text = "0";
                    txtDevAcumTab0.Text = "0";
                    txtEneSosTab0.Text = "0";
                    txtFebSosTab0.Text = "0";
                    txtMarSosTab0.Text = "0";
                    txtAbrSosTab0.Text = "0";
                    txtMaySosTab0.Text = "0";
                    txtJunSosTab0.Text = "0";
                    txtJulSosTab0.Text = "0";
                    txtAgoSosTab0.Text = "0";
                    txtSepSosTab0.Text = "0";
                    txtOctSosTab0.Text = "0";
                    txtNovSosTab0.Text = "0";
                    txtDicSosTab0.Text = "0";
                    txtComproAnualTab0.Text = "0";
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSOSEM.Visible = false;
                imgbtn_agregarSOSEM.Visible = true;
                Up_Tab0.Update();
            }

        }
        protected void btnActualizarRegistroSOSEMTab0_OnClick(object sender, EventArgs e)
        {

            if (ValidarSOSEMTab0())
            {

                _BESOSEM.id_seguimiento_sosem = Convert.ToInt32(lblRegistroObras.Text);
                //_BEFinanciamiento.tipo = 1;

                _BESOSEM.anio = ddlanioSOSEMTAb0.SelectedValue;
                _BESOSEM.pia = txtPiaSOSEMTab0.Text;
                _BESOSEM.pimAcumulado = txtPimAcumTab0.Text;
                _BESOSEM.devAcumulado = txtDevAcumTab0.Text;
                _BESOSEM.ene = txtEneSosTab0.Text;
                _BESOSEM.feb = txtFebSosTab0.Text;
                _BESOSEM.mar = txtMarSosTab0.Text;
                _BESOSEM.abr = txtAbrSosTab0.Text;
                _BESOSEM.may = txtMaySosTab0.Text;
                _BESOSEM.jun = txtJunSosTab0.Text;
                _BESOSEM.jul = txtJulSosTab0.Text;
                _BESOSEM.ago = txtAgoSosTab0.Text;
                _BESOSEM.sep = txtSepSosTab0.Text;
                _BESOSEM.oct = txtOctSosTab0.Text;
                _BESOSEM.nov = txtNovSosTab0.Text;
                _BESOSEM.dic = txtDicSosTab0.Text;
                _BESOSEM.compromisoAnual = txtComproAnualTab0.Text;
                _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLSOSEM.spud_MON_SOSEM_EDITAR(_BESOSEM);

                if (val == 1)
                {
                    string script = "<script>alert('Actualización Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaFinanSOSEMTab0();
                    ddlanioSOSEMTAb0.SelectedValue = "";
                    txtPiaSOSEMTab0.Text = "";
                    txtPimAcumTab0.Text = "";
                    txtDevAcumTab0.Text = "";
                    txtEneSosTab0.Text = "";
                    txtFebSosTab0.Text = "";
                    txtMarSosTab0.Text = "";
                    txtAbrSosTab0.Text = "";
                    txtMaySosTab0.Text = "";
                    txtJunSosTab0.Text = "";
                    txtJulSosTab0.Text = "";
                    txtAgoSosTab0.Text = "";
                    txtSepSosTab0.Text = "";
                    txtOctSosTab0.Text = "";
                    txtNovSosTab0.Text = "";
                    txtDicSosTab0.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSOSEM.Visible = false;
                imgbtn_agregarSOSEM.Visible = true;
                Up_Tab0.Update();
            }



        }
        protected Boolean ValidarSOSEMTab0()
        {
            Boolean result;
            result = true;

            if (ddlanioSOSEMTAb0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Año');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;

        }
        protected void CargaFinanSOSEMTab0()
        {
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEFinanciamiento.tipoFinanciamiento = 2;

            grdSosemTab0.DataSource = _objBLFinanciamiento.F_spMON_SeguimientoSOSEM(_BEFinanciamiento);
            grdSosemTab0.DataBind();


        }

        protected void CargaSOSEMEjecutorasTab0()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSOSEMEjecutoras.DataSource = _objBLFinanciamiento.F_spMON_SosemEjecutora(snip);
            grdSOSEMEjecutoras.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }
        protected void CargaSOSEMDevengadoMensualizadoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdDetalleSOSEMTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemDevengadoMensualizado(snip, idEjecutora);
            grdDetalleSOSEMTab0.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }
        protected void CargaSOSEMFuenteFinanciamientoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdFuenteFinanciamientoTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemFuenteFinanciamiento(snip, idEjecutora);
            grdFuenteFinanciamientoTab0.DataBind();
        }

        #endregion

        #region Tab1

        protected void FechaTermino_OnTextChanged(object sender, EventArgs e)
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);
                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }
        }

        //CAMBIOS
        //protected void CargaOBRAEjecutorasTab0SEACEConsultoria()
        //{
        //    int snip = Convert.ToInt32(lblSNIP.Text);

        //    grdSEACEConsultoria.DataSource = _objBLFinanciamiento.spMON_SEACE_Consultoria(snip);
        //    grdSEACEConsultoria.DataBind();
        //}
        protected void CargaTab1()
        {
            CargaddlTipoAdjudicacionTab1(ddlAdjudicacionTab1);
            CargaddlTipoAdjudicacionTab1(ddlTipoAdjudicacionTab1);
            CargaDllTipoResultado();
            cargaSeguimientoProcesoTab1();

            CargaDatosTab1();
            TabActivoProceso();
            cargaConsultoresTab1();
        }

        protected void ddlProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcesoTab1.SelectedValue != "")
            {
                CargaDatosTab1();
                cargaSeguimientoProcesoTab1();
            }

            TabActivoProceso();
            Up_Tab1.Update();
        }
        protected void ActualizaFechasTab1()
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);
                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }
        }
        protected void CargaDatosTab1()
        {
            lblTitSeace.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " (Obligatorio)";
            lnkbtnSeaceAutoPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " POR SEACE";
            lnkbtnSeaceManualPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " DE FORMA MANUAL";

            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 2;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);

            if (dt.Rows.Count > 0)
            {
                ddlModalidadTab1.SelectedValue = dt.Rows[0]["id_tipoModalidad"].ToString();
                ddlAdjudicacionTab1.SelectedValue = dt.Rows[0]["id_tipoAdjudicacion"].ToString();
                txtNroLicTab1.Text = dt.Rows[0]["NroLic"].ToString();
                TxtAnio2Tab1.Text = dt.Rows[0]["anio"].ToString();
                txtDetalleTab1.Text = dt.Rows[0]["detalle"].ToString();
                txtProTab1.Text = dt.Rows[0]["buenaPro"].ToString();
                txtProConsentidaTab1.Text = dt.Rows[0]["buenaProConsentida"].ToString();
                txtValorReferencialTab1.Text = dt.Rows[0]["valorReferencial"].ToString();
                txtMontoContratadoTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                txtNroContrato.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                txtFechaFirmaContratoTab1.Text = dt.Rows[0]["fechaFirmaContrato"].ToString();

                lblNomActualizaProcesoSeleccion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                string valCheck = dt.Rows[0]["id_tipoEmpresa"].ToString();
                if (valCheck != "")
                {
                    if (Convert.ToInt32(valCheck) == 1)
                    {
                        rbConsultorTab1.Checked = true;
                        rbConsorcioTab1.Checked = false;
                        //rbConsorcioTab1.Enabled = false;

                        lblNomConsultorTab1.Font.Bold = true;
                        lblNomConsorcioTab1.Font.Bold = false;

                        Panel_ConsultorTab1.Visible = true;
                        Panel_ConsorcioTab1.Visible = false;
                        Up_rbProcesoTab1.Update();

                        txtNombConsultorTab1.Text = dt.Rows[0]["nombreContratista"].ToString();
                        txtRucConsultorTab1.Text = dt.Rows[0]["rucContratista"].ToString();
                        lnkbtnContratoContratistaTab1.Text = dt.Rows[0]["urlContrato"].ToString();
                        txtRepresentanteLegalConsultorTab1.Text = dt.Rows[0]["representanteContratista"].ToString();
                        GeneraIcoFile(lnkbtnContratoContratistaTab1.Text, imgbtnContratoContratistaTab1);

                    }
                    if (Convert.ToInt32(valCheck) == 2)
                    {
                        rbConsorcioTab1.Checked = true;
                        rbConsultorTab1.Checked = false;
                        //rbConsultorTab1.Enabled = false;

                        lblNomConsorcioTab1.Font.Bold = true;
                        lblNomConsultorTab1.Font.Bold = false;

                        Panel_ConsorcioTab1.Visible = true;
                        Panel_ConsultorTab1.Visible = false;
                        Up_rbProcesoTab1.Update();


                        lnkbtnCartaTab1.Text = dt.Rows[0]["urlCartaFielConsorcio"].ToString();
                        lnkbtnContratoTab1.Text = dt.Rows[0]["urlContrato"].ToString();


                        txtNomconsorcioTab1.Text = dt.Rows[0]["nombreConsorcio"].ToString();
                        txtRucConsorcioTab1.Text = dt.Rows[0]["rucConsorcio"].ToString();
                        txtTelefonoConsorcioTab1.Text = dt.Rows[0]["telefonoConsorcio"].ToString();
                        txtRepresentanteTab1.Text = dt.Rows[0]["representateConsorcio"].ToString();

                        GeneraIcoFile(lnkbtnCartaTab1.Text, imgbtnAdjCartaTab1);
                        GeneraIcoFile(lnkbtnContratoTab1.Text, imgbtnContratoTab1);

                    }
                }
                else
                {
                    rbConsorcioTab1.Checked = false;
                    rbConsultorTab1.Checked = false;
                    rbConsultorTab1.Enabled = true;
                    rbConsorcioTab1.Enabled = true;
                    Panel_ConsultorTab1.Visible = false;
                    Panel_ConsorcioTab1.Visible = false;
                    lblNomConsorcioTab1.Font.Bold = false;
                    lblNomConsultorTab1.Font.Bold = false;

                    txtNombConsultorTab1.Text = "";
                    txtRucConsultorTab1.Text = "";
                    txtNomconsorcioTab1.Text = "";
                    txtRucConsorcioTab1.Text = "";
                    txtTelefonoConsorcioTab1.Text = "";
                    txtRepresentanteTab1.Text = "";

                    cargaConsultoresTab1();
                    Up_rbProcesoTab1.Update();
                }


            }
            else
            {
                ddlAdjudicacionTab1.SelectedValue = "";
                txtNroLicTab1.Text = "";
                TxtAnio2Tab1.Text = "";
                txtDetalleTab1.Text = "";
                txtProTab1.Text = "";
                txtProConsentidaTab1.Text = "";
                txtValorReferencialTab1.Text = "";

                txtMontoContratadoTab1.Text = "";
                txtNroContrato.Text = "";
                txtNomconsorcioTab1.Text = "";
                txtRucConsorcioTab1.Text = "";
                txtTelefonoConsorcioTab1.Text = "";
                txtRepresentanteTab1.Text = "";
                lblNomActualizaProcesoSeleccion.Text = "";
                lblNomConsorcioTab1.Font.Bold = false;
                lblNomConsultorTab1.Font.Bold = false;
                rbConsorcioTab1.Checked = false;
                rbConsultorTab1.Checked = false;
                rbConsultorTab1.Enabled = true;
                rbConsorcioTab1.Enabled = true;
                Panel_ConsultorTab1.Visible = false;
                Panel_ConsorcioTab1.Visible = false;
                imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoContratistaTab1.Text = "";
                imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoTab1.Text = "";

                Up_Tab1.Update();
            }

            // INFORMACION AUTOMATICA SEACE
            CargaCodigoSeace();
            CargaProcesosRelSEACE();

        }

        protected void CargaddlTipoAdjudicacionTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoAdjudicacion();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void rbConsultorTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsultorTab1.Font.Bold = true;
            lblNomConsorcioTab1.Font.Bold = false;
            Up_lblNomConsultorTab1.Update();
            Up_lblNomConsorcioTab1.Update();

            rbConsorcioTab1.Checked = false;
            Up_rbConsorcioTab1.Update();
            Panel_ConsultorTab1.Visible = true;
            Panel_ConsorcioTab1.Visible = false;
            Up_rbProcesoTab1.Update();
        }

        protected void rbConsorcioTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsorcioTab1.Font.Bold = true;
            lblNomConsultorTab1.Font.Bold = false;
            Up_lblNomConsorcioTab1.Update();
            Up_lblNomConsultorTab1.Update();

            rbConsultorTab1.Checked = false;
            Up_rbConsultorTab1.Update();
            Panel_ConsorcioTab1.Visible = true;
            Panel_ConsultorTab1.Visible = false;
            Up_rbProcesoTab1.Update();

        }

        protected void cargaSeguimientoProcesoTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 2;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdSeguimientoProcesoTab1.DataSource = _objBLProceso.F_spMON_SeguimientoProcesoSeleccion(_BEProceso);
            grdSeguimientoProcesoTab1.DataBind();
        }

        protected void CargaDllTipoResultado()
        {
            ddlResultadoTab1.DataSource = _objBLEjecucion.F_spMON_TipoResultado();
            ddlResultadoTab1.DataTextField = "nombre";
            ddlResultadoTab1.DataValueField = "valor";
            ddlResultadoTab1.DataBind();

            ddlResultadoTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }


        protected void grdSeguimientoProcesoTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab1");

                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void imgDocTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdSeguimientoProcesoTab1.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdSeguimientoProcesoTab1.Rows[row.RowIndex].FindControl("imgDocTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtn_AgregarSeguimientoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;

            TxtConvocatoriaTab1.Text = "";
            txtFechaPubliTab1.Text = "";
            ddlTipoAdjudicacionTab1.SelectedValue = "";
            ddlResultadoTab1.SelectedValue = "";
            txtObservacion.Text = "";

            imgbtnSeguimientoTAb1.ImageUrl = "~/img/blanco.png";
            LnkbtnSeguimientoTAb1.Text = "";
            lblNomUsuarioSeguimientoProceso.Text = "";
            btnGuardarSeguimientoProcesoTab1.Visible = true;
            btnModificarSeguimientoProcesoTab1.Visible = false;

            Up_Tab1.Update();
        }

        protected void btnCancelaSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = true;
            Up_Tab1.Update();
        }


        protected Boolean ValidarSeguimientoTab1()
        {
            Boolean result;
            result = true;

            if (TxtConvocatoriaTab1.Text == "")
            {
                string script = "<script>alert('Ingrese convocatoria.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaFecha(txtFechaPubliTab1, "Fecha de Publicación", true, true))
                return false;

            if (Convert.ToDateTime(txtFechaPubliTab1.Text) > DateTime.Now.Date)
            {
                string script = "<script>alert('La Fecha de Publicación no puede ser mayor a la actual.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (txtProTab1.Text.Length > 0 && Convert.ToDateTime(txtFechaPubliTab1.Text) > Convert.ToDateTime(txtProTab1.Text))
            {
                string script = "<script>alert('La fecha de publicación debe ser menor o igual a la fecha de buena pro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoAdjudicacionTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de adjudicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlResultadoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione resultado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaArchivo(FileUploadSeguimientoTAb1))
                return false;

            return result;
        }

        protected void btnGuardarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSeguimientoTab1())
            {
                //if (validaArchivo(FileUploadSeguimientoTAb1))
                //{
                //    if (validaArchivo(FileUploadContratoContratistaTab1))
                //    {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 2;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaPubliTab1.Text);
                _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                _BEProceso.resultado = ddlResultadoTab1.SelectedValue;
                _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                _BEProceso.observacion = txtObservacion.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLProceso.spi_MON_SeguimientoProcesoSeleccion(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtConvocatoriaTab1.Text = "";
                    txtFechaPubliTab1.Text = "";
                    ddlTipoAdjudicacionTab1.SelectedValue = "";
                    ddlResultadoTab1.SelectedValue = "";
                    txtObservacion.Text = "";

                    Panel_AgregarSeguimientoTab1.Visible = false;
                    imgbtn_AgregarSeguimientoTab1.Visible = true;

                    cargaSeguimientoProcesoTab1();
                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                //    }
                //}
            }

        }

        protected void cargaConsultoresTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 2;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdConsultoresTab1.DataSource = _objBLProceso.F_spMON_GrupoConsorcio(_BEProceso);
            grdConsultoresTab1.DataBind();


        }

        protected void imgbtn_AgregarConsultorTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = true;
            btn_modificarConsultor.Visible = false;
            Up_Tab1.Update();
        }

        protected void btn_cancelarConsultor_OnClick(object sender, EventArgs e)
        {

            txtContratistaGrupTab1.Text = "";
            txtRucGrupTab1.Text = "";
            txtRepresentanGrupTAb1.Text = "";
            txtRepresentanGrupTAb1.Text = "";
            Panel_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void lnkbtnCartaTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnCartaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnCartaTab1.Text);
                Response.End();
            }

        }

        protected void lnkbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnAdjCartaTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnCartaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnCartaTab1.Text);
                Response.End();
            }

        }

        protected void imgbtnContratoContratistaTab1_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoContratistaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoContratistaTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }


        protected Boolean ValidarGrupoConsorcioTab1()
        {
            Boolean result;
            result = true;

            if (txtContratistaGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese nombre de contratista.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtRucGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese N° de RUC.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtRepresentanGrupTAb1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Representante Legal.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            return result;
        }

        protected void btn_guardarConsultor_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (validaArchivo(FileUploadAdjCartaTab1))
                {
                    if (validaArchivo(FileUploadAdjContratoTab1))
                    {
                        if (ValidarGrupoConsorcioTab1())
                        {
                            //REGISTRAR PROCESO DE SELECCION INDIRECTA
                            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEProceso.tipoFinanciamiento = 2;
                            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                            _BEProceso.id_tipoModalidad = 2; //Expediente por default es Indirecto
                            if (ddlAdjudicacionTab1.SelectedValue == "")
                            {
                                _BEProceso.id_tipoAdjudicacion = 0;
                            }
                            else
                            { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }
                            if (txtNroLicTab1.Text == "")
                            { txtNroLicTab1.Text = "0"; }
                            _BEProceso.norLic = (txtNroLicTab1.Text);
                            _BEProceso.anio = TxtAnio2Tab1.Text;
                            _BEProceso.detalle = txtDetalleTab1.Text;
                            _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                            _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                            if (txtValorReferencialTab1.Text == "")
                            { txtValorReferencialTab1.Text = "0"; }
                            _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                            if (txtMontoContratadoTab1.Text == "")
                            { txtMontoContratadoTab1.Text = "0"; }
                            _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                            _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);

                            if (rbConsultorTab1.Checked == true)
                            {
                                _BEProceso.id_tipoEmpresa = 1;
                                _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                                _BEProceso.rucContratista = txtRucConsultorTab1.Text;


                                if (FileUploadContratoContratistaTab1.HasFile)
                                {
                                    _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                                }
                                else
                                { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }

                            }

                            if (rbConsorcioTab1.Checked == true)
                            {
                                _BEProceso.id_tipoEmpresa = 2;
                                _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                                _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                                _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                                _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                                if (FileUploadAdjCartaTab1.HasFile)
                                {
                                    _BEProceso.urlCartaConsorcio = _Metodo.uploadfile(FileUploadAdjCartaTab1);
                                }
                                else
                                { _BEProceso.urlCartaConsorcio = lnkbtnCartaTab1.Text; }

                                if (FileUploadAdjContratoTab1.HasFile)
                                {
                                    _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                                }
                                else
                                { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                            }

                            _BEProceso.Date_fechaConvenio = VerificaFecha("");
                            _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
                            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int valI = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);


                            //REGISTRAR CONSORCIO
                            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEProceso.tipoFinanciamiento = 2;
                            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                            _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                            _BEProceso.rucContratista = txtRucGrupTab1.Text;
                            _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLProceso.spi_MON_GrupoConsorcio(_BEProceso);

                            if (val == 1 && valI == 1)
                            {
                                string script = "<script>alert('Se registró correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                cargaConsultoresTab1();

                                txtContratistaGrupTab1.Text = "";
                                txtRucGrupTab1.Text = "";
                                txtRepresentanGrupTAb1.Text = "";

                                Panel_AgregarConsultorTab1.Visible = false;
                                imgbtn_AgregarConsultorTab1.Visible = true;

                                Up_Tab1.Update();

                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }

                        }
                    }
                }
            }
        }

        protected Boolean ValidarResultadoTab1()
        {
            Boolean result;
            result = true;

            if (!validaFecha(txtProTab1, "Fecha de Buena Pro", true))
                return false;
            if (!validaFecha(txtFechaFirmaContratoTab1, "Fecha de Firma de Contrato", true))
                return false;
            if (!validaFecha(txtProConsentidaTab1, "Fecha de Buena Pro Consentida", true))
                return false;

            if (!validaArchivo(FileUploadAdjCartaTab1, "Carta Fiel"))
                return false;
            if (!validaArchivo(FileUploadAdjContratoTab1, "Documento de Contrato"))
                return false;
            if (!validaArchivo(FileUploadContratoContratistaTab1, "Documento de Contrato"))
                return false;

            if (!txtProTab1.Text.Equals(""))
            {
                if (Convert.ToDateTime(txtProTab1.Text) > DateTime.Today)
                {
                    string script = "<script>alert('La Fecha de Buena Pro ingresada es mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (!txtProConsentidaTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProConsentidaTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro Consentida.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }
            }

            if (!txtFechaFirmaContratoTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtFechaFirmaContratoTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de firma de contrato.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de firma de contrato ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }

                if (!txtProConsentidaTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProConsentidaTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato no puede ser menor a la Fecha de Buena Pro Consentida.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }

                if (txtFechaInicioTab2.Text.Length > 0)
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > Convert.ToDateTime(txtFechaInicioTab2.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato debe ser menor a la fecha de inicio.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }

            if (ddlModalidadTab1.SelectedValue.ToString() == "")
            {
                string script = "<script>alert('Seleccione la Modalidad de Ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

           
            if (txtMontoContratadoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese monto contratado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

           

            if (rbConsultorTab1.Checked == true)
            {
                if (txtNombConsultorTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese nombre del contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (txtRucConsultorTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese RUC de contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }

            if (rbConsorcioTab1.Checked == true)
            {
                if (txtNomconsorcioTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese nombre del consorcio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                //if (txtRucConsorcioTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese RUC de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}
                //if (txtRepresentanteTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese representante legal de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}

            }

            return result;
        }



        protected void btnGuardarTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                //if (validaArchivo(FileUploadAdjCartaTab1))
                //{
                //    if (validaArchivo(FileUploadAdjContratoTab1))
                //    {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 2;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                //_BEProceso.id_tipoModalidad = 2; //Expediente por default es Indirecto
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                if (ddlAdjudicacionTab1.SelectedValue == "")
                {
                    _BEProceso.id_tipoAdjudicacion = 0;
                }
                else
                { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }
                if (txtNroLicTab1.Text == "")
                { txtNroLicTab1.Text = "0"; }
                _BEProceso.norLic = (txtNroLicTab1.Text);
                _BEProceso.anio = TxtAnio2Tab1.Text;
                _BEProceso.detalle = txtDetalleTab1.Text;
                _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                if (txtValorReferencialTab1.Text == "")
                { txtValorReferencialTab1.Text = "0"; }
                _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                if (txtMontoContratadoTab1.Text == "")
                { txtMontoContratadoTab1.Text = "0"; }
                _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);

                if (rbConsultorTab1.Checked == true)
                {
                    _BEProceso.id_tipoEmpresa = 1;
                    _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                    _BEProceso.rucContratista = txtRucConsultorTab1.Text;


                    if (FileUploadContratoContratistaTab1.HasFile)
                    {
                        _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                    }
                    else
                    { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }

                }

                if (rbConsorcioTab1.Checked == true)
                {
                    _BEProceso.id_tipoEmpresa = 2;
                    _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                    _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                    _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                    _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                    if (FileUploadAdjCartaTab1.HasFile)
                    {
                        _BEProceso.urlCartaConsorcio = _Metodo.uploadfile(FileUploadAdjCartaTab1);
                    }
                    else
                    { _BEProceso.urlCartaConsorcio = lnkbtnCartaTab1.Text; }

                    if (FileUploadAdjContratoTab1.HasFile)
                    {
                        _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                    }
                    else
                    { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                }

                _BEProceso.Date_fechaConvenio = VerificaFecha("");
                _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);


                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosTab1();

                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                //    }
                //}
            }


        }

        #endregion

        #region Tab2
        protected void CargaTab2()
        {


            //TabPanelProcesoSeleccionSEACE.Enabled = false;
            //TabPanelProcesoSeleccion.Enabled = true;
            CargaDatosTab2();
            //if (ddlEstadoTab2.SelectedValue == "21" )
            //{
            //    lblnNomSubEstadoTab2.Visible = true;
            //    ddlSubEstadoTab2.Visible = true;
            //    CargaSubTipoAdjudicacionTab2(ddlEstadoTab2.SelectedValue);

            //}
            //else
            //{
            //    lblnNomSubEstadoTab2.Visible = false;
            //    ddlSubEstadoTab2.Visible = false;
            //}
            dt = CargaEmpleadoObraTab2(4);
            grd_HistorialCoordinador.DataSource = dt;
            grd_HistorialCoordinador.DataBind();

            //if (dt.Rows.Count > 0)
            //{ txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString(); }
            //txtCordinadorTab2.Text = 
            CargaAvanceFisico();

            if (lblCOD_SUBSECTOR.Text == "1")
            {
                trGEI.Visible = true;
            }

        }
        #region Coordinador
        protected DataTable CargaEmpleadoObraTab2(int tipo)
        {
            DataTable dt;
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoObraExp(_BEEjecucion);
            return dt;
        }
        protected void imgbtn_CoordinadorTab2_OnClick(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = true;
            btnGuardar_Coordinador.Visible = true;
            btnModificar_Coordinador.Visible = false;
            Up_Tab2.Update();
        }
        protected void btnCancelarCoordinador_Onclick(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = false;

            Up_Tab2.Update();
        }
        protected void btnGuardarCoordinador_OnClick(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de Responsable.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 4;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spi_MON_EmpleadoObraExp(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró Responsable correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //txtCordinadorTab2.Text = txtCoordinador.Text;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoObraTab2(4);
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    Panel_Coordinador.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grd_HistorialCoordinador_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialCoordinador.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;//PARA LA ELIMINACION
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;

                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoObraTab2(4);
                    if (dt.Rows.Count > 0)
                    {
                        txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCordinadorTab2.Text = "";
                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();

                    Panel_Coordinador.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grd_HistorialCoordinador_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialCoordinador.SelectedDataKey.Value.ToString();
            lblIdCoordinadorM.Text = id;
            GridViewRow row = grd_HistorialCoordinador.SelectedRow;

            txtCoordinador.Text = ((Label)row.FindControl("lblnombreS")).Text;
            txtFechaC.Text = ((Label)row.FindControl("lblFechaSS")).Text;

            btnGuardar_Coordinador.Visible = false;
            btnModificar_Coordinador.Visible = true;
            Panel_Coordinador.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }
        protected void btnModificarCoordinador_OnClick(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIdCoordinadorM.Text);
                _BEEjecucion.Tipo = 1;//valor q indica la modificacion
                                      // _BEEjecucion.empleadoObra = txtu.Text;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el coordinador correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(4);
                    if (dt.Rows.Count > 0)
                    {
                        txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCordinadorTab2.Text = "";

                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    Panel_Coordinador.Visible = false;
                    btnModificar_Coordinador.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        #endregion

        protected void CargaDatosTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 2;

            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                lblNombreEstadoGeneral.Text = dt.Rows[0]["vEstadoEjecucion"].ToString().ToUpper();
                lblIdEstado.Text = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();
                lblIdSubEstado.Text = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
                lblIdSubEstado2.Text = dt.Rows[0]["id_TipoEstadoParalizado"].ToString();

                txtCordinadorTab2.Text = dt.Rows[0]["coordinadorMVCS"].ToString();

                txtFechaInicioTab2.Text = dt.Rows[0]["fechaInicio"].ToString();
                txtPlazoEjecucionTab2.Text = dt.Rows[0]["plazoEjecucion"].ToString();
                txtFechaInicioContractualTab2.Text = dt.Rows[0]["fechaInicioContractual"].ToString();

                txtFechaInicioReal.Text = dt.Rows[0]["fechaInicioReal"].ToString();
                txtFechaFinContractual.Text = dt.Rows[0]["fechaFinContractual"].ToString();
                txtFechaTerminoReal.Text = dt.Rows[0]["fechaFinReal"].ToString();
                txtFechaTermino.Text = dt.Rows[0]["fechaFin"].ToString();
                lblNombActuaTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                if (dt.Rows[0]["usuarioUpdateEstado"].ToString().Length > 0)
                {
                    lblFechaEstado.Text = "(Actualizó: " + dt.Rows[0]["usuarioUpdateEstado"].ToString() + " - " + dt.Rows[0]["fechaUpdateEstado"].ToString() + ")";
                }
            }
        }

        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected Boolean ValidarEjecucionTab2()
        {
            Boolean result;
            result = true;

            if (txtPlazoEjecucionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese Plazo de ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }

            if (txtFechaInicioTab2.Text != "")
            {
                if (_Metodo.ValidaFecha(txtFechaInicioTab2.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Inicio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (Convert.ToDateTime(txtFechaInicioTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La Fecha de Inicio no puede ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }
            }

            if (txtFechaTermino.Text != "")
            {
                if (_Metodo.ValidaFecha(txtFechaTermino.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Termino.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaTermino.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La Fecha de Termino Real no debe ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (txtFechaInicioTab2.Text.Length>0 && (Convert.ToDateTime(txtFechaTermino.Text) < Convert.ToDateTime(txtFechaInicioTab2.Text)))
                {
                    string script = "<script>alert('La fecha de termino real debe ser mayor  a la fecha de inicio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                }

                if (txtFechaTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaTerminoReal.Text) > Convert.ToDateTime(txtFechaTab3.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser menor o igual a la fecha de liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            return result;
        }

        protected void btnGuardarTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarEjecucionTab2())
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 2;

                _BEEjecucion.coordinadorMVCS = txtCordinadorTab2.Text;

                _BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaInicioTab2.Text);
                if (txtPlazoEjecucionTab2.Text == "")
                {
                    txtPlazoEjecucionTab2.Text = "0";
                }
                _BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                _BEEjecucion.Date_fechaInicioContractual = VerificaFecha(txtFechaInicioContractualTab2.Text);
                _BEEjecucion.Date_fechaInicioReal = VerificaFecha(txtFechaInicioReal.Text);
                _BEEjecucion.Date_fechaFinContractual = VerificaFecha(txtFechaFinContractual.Text);
                _BEEjecucion.Date_fechaFinReal = VerificaFecha(txtFechaTerminoReal.Text);
                _BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaTermino.Text);
                _BEEjecucion.Date_fechaEntregaTerreno = VerificaFecha("");
                _BEEjecucion.Date_fechaAdelantoMaterial = VerificaFecha("");
                _BEEjecucion.Date_fechaAdelantoDirecto = VerificaFecha("");
                _BEEjecucion.Date_fechaRecepcion = VerificaFecha("");
                _BEEjecucion.Date_fechaFinProbable = VerificaFecha("");
                _BEEjecucion.telefonocoordinador = txtTelefonoCoordinadorTab2.Text;
                _BEEjecucion.correocoordinador = txtCorreoCoordinadorTab2.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                _BEEjecucion.urlMetasFisicas = "";

                _BEEjecucion.urlMemoriasDescriptivas = "";
                _BEEjecucion.urlFichaRecepcion = "";

                //int val = _objBLEjecucion.spi_MON_EstadoEjecuccion(_BEEjecucion);
                int val = _objBLEjecucion.spi_MON_EstadoEjecuccionTransferencia(_BEEjecucion); //Funciona Modalidad X CONTRATA

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosTab2();

                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void CargaAvanceFisico()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 2;
            DataTable dt = _objBLEjecucion.spMON_AvanceFisico(_BEEjecucion);
            if (dt.Rows.Count > 0)
            {
                lblAvanceFisicoTab2.Text = (dt.Rows[dt.Rows.Count - 1]["avance"]).ToString();
            }

            grdAvanceFisicoTab2.DataSource = dt;
            grdAvanceFisicoTab2.DataBind();

        }
        protected void imgbtnAvanceFisicoTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;

            lblNomUsuarioAvance.Text = "";
            btnModificarAvanceTab2.Visible = false;
            btnGuardarAvanceTab2.Visible = true;

            imgbtnAvanceTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnAvanceTab2.Text = "";

            txtInformeTab2.Text = "";
            txtFechaTab2.Text = "";
            txtAvanceTab2.Text = "0.00";
            txtAvanceFinancieroTab2.Text = "0.00";
            txtSituacionTab2.Text = "";
            txtAccionesPrevistasTab2.Text = "";
            txtAccionesRealizadasTab2.Text = "";

            chbAyudaMemoriaTab2.Checked = true;
            chbAyudaMemoriaTab2.Enabled = true;
            lblNomUsuarioFlagAyudaTab2.Text = "";

            CargaTipoEstadosDetalle();
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);

            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = false;
            imgbtnAvanceFisicoTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void btnGuardarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                //if (validaArchivo(FileUploadAvanceTab2))
                //{
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 2;

                _BEEjecucion.informe = txtInformeTab2.Text;
                _BEEjecucion.Date_fecha = Convert.ToDateTime(txtFechaTab2.Text);
                _BEEjecucion.avance = txtAvanceTab2.Text;
                _BEEjecucion.financieroReal = txtAvanceFinancieroTab2.Text;
                _BEEjecucion.situacion = txtSituacionTab2.Text;
                _BEEjecucion.accionesPorRealizar = txtAccionesPrevistasTab2.Text;
                _BEEjecucion.accionesRealizadas = txtAccionesRealizadasTab2.Text;
                _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue);
                _BEEjecucion.id_tipoSubEstadoEjecucion = ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") ? "0" : ddlTipoSubEstadoInformeTab2.SelectedValue;

                _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                if (chbAyudaMemoriaTab2.Checked == true)
                {
                    _BEEjecucion.flagAyuda = "1";
                }
                else
                {
                    _BEEjecucion.flagAyuda = "0";
                }
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spi_MON_AvanceFisico(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();

                    txtInformeTab2.Text = "";
                    txtFechaTab2.Text = "";
                    txtAvanceTab2.Text = "";
                    txtSituacionTab2.Text = "";

                    Panel_AgregarAvanceTab2.Visible = false;
                    imgbtnAvanceFisicoTab2.Visible = true;
                    CargaDatosTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                //}
            }

        }

        //protected void btnComponentesPMIB_Click(object sender, EventArgs e)
        //{
        //    VarPmib();
        //    Up_VarPMIB.Update();
        //    MPE_PMIB.Show();
        //}

        //private void VarPmib()
        //{
        //    DataTable _obj;
        //    BEVarPimb _objVarPimb = new BEVarPimb();
        //    _objVarPimb.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
        //    _obj = objBLProyecto.spSOL_Seguimiento_Variable_PMIB(_objVarPimb);

        //    if (_obj.Rows.Count > 0)
        //    {
        //        txtP_concreto_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_concreto_m2"].ToString()).ToString("N");
        //        txtP_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["P_concreto_co"].ToString()).ToString("N");
        //        txtP_concreto_cu.Text = (Convert.ToDouble(txtP_concreto_co.Text) / Convert.ToDouble(txtP_concreto_m2.Text)).ToString("N");

        //        txtP_asfalto_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_asfalto_m2"].ToString()).ToString("N");
        //        txtP_asfalto_co.Text = Convert.ToDouble(_obj.Rows[0]["P_asfalto_co"].ToString()).ToString("N");
        //        txtP_asfalto_cu.Text = (Convert.ToDouble(txtP_asfalto_co.Text) / Convert.ToDouble(txtP_asfalto_m2.Text)).ToString("N");

        //        txtP_emboquillado_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_emboquillado_m2"].ToString()).ToString("N");//
        //        txtP_emboquillado_co.Text = Convert.ToDouble(_obj.Rows[0]["P_emboquillado_co"].ToString()).ToString("N");//
        //        txtP_emboquillado_cu.Text = (Convert.ToDouble(txtP_emboquillado_co.Text) / Convert.ToDouble(txtP_emboquillado_m2.Text)).ToString("N");

        //        txtP_adoquinado_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_adoquinado_m2"].ToString()).ToString("N");//
        //        txtP_adoquinado_co.Text = Convert.ToDouble(_obj.Rows[0]["P_adoquinado_co"].ToString()).ToString("N");//
        //        txtP_adoquinado_cu.Text = (Convert.ToDouble(txtP_adoquinado_co.Text) / Convert.ToDouble(txtP_adoquinado_m2.Text)).ToString("N");

        //        divAutomatica(txtP_concreto_m2, txtP_concreto_cu, txtP_concreto_co);
        //        divAutomatica(txtP_asfalto_m2, txtP_asfalto_cu, txtP_asfalto_co);
        //        divAutomatica(txtP_emboquillado_m2, txtP_emboquillado_cu, txtP_emboquillado_co);
        //        divAutomatica(txtP_adoquinado_m2, txtP_adoquinado_cu, txtP_adoquinado_co);

        //        lblCostoP.Text = (Convert.ToDouble(txtP_concreto_co.Text) + Convert.ToDouble(txtP_asfalto_co.Text) + Convert.ToDouble(txtP_emboquillado_co.Text) + Convert.ToDouble(txtP_adoquinado_co.Text)).ToString("N");


        //        txtV_concreto_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_concreto_m2"].ToString()).ToString("N");//
        //        txtV_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["V_concreto_co"].ToString()).ToString("N");//
        //        txtV_concreto_cu.Text = (Convert.ToDouble(txtV_concreto_co.Text) / Convert.ToDouble(txtV_concreto_m2.Text)).ToString("N");

        //        txtV_adoquinado_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_adoquinado_m2"].ToString()).ToString("N");//
        //        txtV_adoquinado_co.Text = Convert.ToDouble(_obj.Rows[0]["V_adoquinado_co"].ToString()).ToString("N");//
        //        txtV_adoquinado_cu.Text = (Convert.ToDouble(txtV_adoquinado_co.Text) / Convert.ToDouble(txtV_adoquinado_m2.Text)).ToString("N");

        //        txtV_piedra_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_piedra_m2"].ToString()).ToString("N");
        //        txtV_piedra_co.Text = Convert.ToDouble(_obj.Rows[0]["V_piedra_co"].ToString()).ToString("N");
        //        txtV_piedra_cu.Text = (Convert.ToDouble(txtV_piedra_co.Text) / Convert.ToDouble(txtV_piedra_m2.Text)).ToString("N");

        //        txtV_emboquillado_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_emboquillado_m2"].ToString()).ToString("N");//
        //        txtV_emboquillado_co.Text = Convert.ToDouble(_obj.Rows[0]["V_emboquillado_co"].ToString()).ToString("N");//
        //        txtV_emboquillado_cu.Text = (Convert.ToDouble(txtV_emboquillado_co.Text) / Convert.ToDouble(txtV_emboquillado_m2.Text)).ToString("N");

        //        divAutomatica(txtV_concreto_m2, txtV_concreto_cu, txtV_concreto_co);
        //        divAutomatica(txtV_adoquinado_m2, txtV_adoquinado_cu, txtV_adoquinado_co);
        //        divAutomatica(txtV_piedra_m2, txtV_piedra_cu, txtV_piedra_co);
        //        divAutomatica(txtV_emboquillado_m2, txtV_emboquillado_cu, txtV_emboquillado_co);

        //        lblCostoV.Text = (Convert.ToDouble(txtV_concreto_co.Text) + Convert.ToDouble(txtV_adoquinado_co.Text) + Convert.ToDouble(txtV_piedra_co.Text) + Convert.ToDouble(txtV_emboquillado_co.Text)).ToString("N");


        //        txtD_concreto_ml.Text = Convert.ToDouble(_obj.Rows[0]["D_concreto_ml"].ToString()).ToString("N");
        //        txtD_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["D_concreto_co"].ToString()).ToString("N");
        //        txtD_concreto_cu.Text = (Convert.ToDouble(txtD_concreto_co.Text) / Convert.ToDouble(txtD_concreto_ml.Text)).ToString("N");

        //        txtD_piedra_ml.Text = _obj.Rows[0]["D_piedra_ml"].ToString();
        //        txtD_piedra_co.Text = _obj.Rows[0]["D_piedra_co"].ToString();
        //        txtD_piedra_cu.Text = (Convert.ToDouble(txtD_piedra_co.Text) / Convert.ToDouble(txtD_piedra_ml.Text)).ToString("N");

        //        divAutomatica(txtD_concreto_ml, txtD_concreto_cu, txtD_concreto_co);
        //        divAutomatica(txtD_piedra_ml, txtD_piedra_cu, txtD_piedra_co);

        //        lblCostoD.Text = (Convert.ToDouble(txtD_concreto_co.Text) + Convert.ToDouble(txtD_piedra_co.Text)).ToString("N");


        //        txtO_muro_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_muro_m2"].ToString()).ToString("N");
        //        txtO_muro_co.Text = Convert.ToDouble(_obj.Rows[0]["O_muro_co"].ToString()).ToString("N");
        //        txtO_muro_cu.Text = (Convert.ToDouble(txtO_muro_co.Text) / Convert.ToDouble(txtO_muro_m2.Text)).ToString("N");

        //        txtO_ponton_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_ponton_m2"].ToString()).ToString("N");
        //        txtO_ponton_co.Text = Convert.ToDouble(_obj.Rows[0]["O_ponton_co"].ToString()).ToString("N");
        //        txtO_ponton_cu.Text = (Convert.ToDouble(txtO_ponton_co.Text) / Convert.ToDouble(txtO_ponton_m2.Text)).ToString("N");

        //        txtO_bermas_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_bermas_m2"].ToString()).ToString("N");
        //        txtO_bermas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_bermas_co"].ToString()).ToString("N");
        //        txtO_bermas_cu.Text = (Convert.ToDouble(txtO_bermas_co.Text) / Convert.ToDouble(txtO_bermas_m2.Text)).ToString("N");

        //        txtO_verdes_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_verdes_m2"].ToString()).ToString("N");
        //        txtO_verdes_co.Text = Convert.ToDouble(_obj.Rows[0]["O_verdes_co"].ToString()).ToString("N");
        //        txtO_verdes_cu.Text = (Convert.ToDouble(txtO_verdes_co.Text) / Convert.ToDouble(txtO_verdes_m2.Text)).ToString("N");

        //        txtO_jardineras_u.Text = _obj.Rows[0]["O_jardineras_u"].ToString();
        //        txtO_jardineras_co.Text = Convert.ToDouble(_obj.Rows[0]["O_jardineras_co"].ToString()).ToString("N");
        //        txtO_jardineras_cu.Text = (Convert.ToDouble(txtO_jardineras_co.Text) / Convert.ToDouble(txtO_jardineras_u.Text)).ToString("N");

        //        txtO_plantones_u.Text = _obj.Rows[0]["O_plantones_u"].ToString();
        //        txtO_plantones_co.Text = Convert.ToDouble(_obj.Rows[0]["O_plantones_co"].ToString()).ToString("N");
        //        txtO_plantones_cu.Text = (Convert.ToDouble(txtO_plantones_co.Text) / Convert.ToDouble(txtO_plantones_u.Text)).ToString("N");

        //        txtO_sanitarias_u.Text = _obj.Rows[0]["O_sanitarias_u"].ToString();
        //        txtO_sanitarias_co.Text = Convert.ToDouble(_obj.Rows[0]["O_sanitarias_co"].ToString()).ToString("N");
        //        txtO_sanitarias_cu.Text = (Convert.ToDouble(txtO_sanitarias_co.Text) / Convert.ToDouble(txtO_sanitarias_u.Text)).ToString("N");

        //        txtO_electricas_u.Text = _obj.Rows[0]["O_electricas_u"].ToString();
        //        txtO_electricas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_electricas_co"].ToString()).ToString("N");
        //        txtO_electricas_cu.Text = (Convert.ToDouble(txtO_electricas_co.Text) / Convert.ToDouble(txtO_electricas_u.Text)).ToString("N");

        //        txtO_bancas_u.Text = _obj.Rows[0]["O_bancas_u"].ToString();
        //        txtO_bancas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_bancas_co"].ToString()).ToString("N");
        //        txtO_bancas_cu.Text = (Convert.ToDouble(txtO_bancas_co.Text) / Convert.ToDouble(txtO_bancas_u.Text)).ToString("N");

        //        txtO_basureros_u.Text = _obj.Rows[0]["O_basureros_u"].ToString();
        //        txtO_basureros_co.Text = Convert.ToDouble(_obj.Rows[0]["O_basureros_co"].ToString()).ToString("N");
        //        txtO_basureros_cu.Text = (Convert.ToDouble(txtO_basureros_co.Text) / Convert.ToDouble(txtO_basureros_u.Text)).ToString("N");

        //        txtO_pergolas_u.Text = _obj.Rows[0]["O_pergolas_u"].ToString();
        //        txtO_pergolas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_pergolas_co"].ToString()).ToString("N");
        //        txtO_pergolas_cu.Text = (Convert.ToDouble(txtO_pergolas_co.Text) / Convert.ToDouble(txtO_pergolas_u.Text)).ToString("N");

        //        txtO_glorietas_u.Text = _obj.Rows[0]["O_glorietas_u"].ToString();
        //        txtO_glorietas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_glorietas_co"].ToString()).ToString("N");
        //        txtO_glorietas_cu.Text = (Convert.ToDouble(txtO_glorietas_co.Text) / Convert.ToDouble(txtO_glorietas_u.Text)).ToString("N");

        //        txtO_otros_u.Text = _obj.Rows[0]["O_otros_u"].ToString();
        //        txtO_otros_co.Text = Convert.ToDouble(_obj.Rows[0]["O_otros_co"].ToString()).ToString("N");
        //        txtO_otros_cu.Text = (Convert.ToDouble(txtO_otros_co.Text) / Convert.ToDouble(txtO_otros_u.Text)).ToString("N");
        //        txtObservacionesPMIB.Text = _obj.Rows[0]["observaciones"].ToString();

        //        lblUsuarioMetasPMIB.Text = "Actualizó: " + _obj.Rows[0]["usuario"].ToString() + " - " + _obj.Rows[0]["fecha_update"].ToString();

        //        divAutomatica(txtO_muro_m2, txtO_muro_cu, txtO_muro_co);
        //        divAutomatica(txtO_ponton_m2, txtO_ponton_cu, txtO_ponton_co);
        //        divAutomatica(txtO_bermas_m2, txtO_bermas_cu, txtO_bermas_co);
        //        divAutomatica(txtO_verdes_m2, txtO_verdes_cu, txtO_verdes_co);

        //        divAutomatica2(txtO_jardineras_u, txtO_jardineras_cu, txtO_jardineras_co);
        //        divAutomatica2(txtO_plantones_u, txtO_plantones_cu, txtO_plantones_co);
        //        divAutomatica2(txtO_sanitarias_u, txtO_sanitarias_cu, txtO_sanitarias_co);
        //        divAutomatica2(txtO_electricas_u, txtO_electricas_cu, txtO_electricas_co);

        //        divAutomatica2(txtO_bancas_u, txtO_bancas_cu, txtO_bancas_co);
        //        divAutomatica2(txtO_basureros_u, txtO_basureros_cu, txtO_basureros_co);
        //        divAutomatica2(txtO_pergolas_u, txtO_pergolas_cu, txtO_pergolas_co);
        //        divAutomatica2(txtO_glorietas_u, txtO_glorietas_cu, txtO_glorietas_co);
        //        divAutomatica2(txtO_otros_u, txtO_otros_cu, txtO_otros_co);

        //        lblCostoO.Text = (Convert.ToDouble(txtO_muro_co.Text) + Convert.ToDouble(txtO_ponton_co.Text) + Convert.ToDouble(txtO_bermas_co.Text) + Convert.ToDouble(txtO_verdes_co.Text) + Convert.ToDouble(txtO_jardineras_co.Text) + Convert.ToDouble(txtO_plantones_co.Text) + Convert.ToDouble(txtO_sanitarias_co.Text) + Convert.ToDouble(txtO_electricas_co.Text) + Convert.ToDouble(txtO_bancas_co.Text) + Convert.ToDouble(txtO_basureros_co.Text) + Convert.ToDouble(txtO_pergolas_co.Text) + Convert.ToDouble(txtO_glorietas_co.Text) + Convert.ToDouble(txtO_otros_co.Text)).ToString("N");

        //        lblCostoTotalObra.Text = (Convert.ToDouble(lblCostoP.Text) + Convert.ToDouble(lblCostoV.Text) + Convert.ToDouble(lblCostoD.Text) + Convert.ToDouble(lblCostoO.Text)).ToString("N");

        //    }
        //    else
        //    {
        //        //llenarcampos();
        //    }

        //}

        //protected void divAutomatica(TextBox uni, TextBox cu, TextBox co)
        //{
        //    if (uni.Text == "")
        //    { uni.Text = "0.00"; }
        //    else
        //    { uni.Text = Convert.ToDouble(uni.Text).ToString("N"); }

        //    if (co.Text == "")
        //    { co.Text = "0.00"; }
        //    else
        //    { co.Text = Convert.ToDouble(co.Text).ToString("N"); }

        //    if (uni.Text == "0.00" || uni.Text == "0")
        //    { cu.Text = "0.00"; }
        //    else { cu.Text = (Convert.ToDouble(co.Text) / Convert.ToDouble(uni.Text)).ToString("N"); }

        //}

        //protected void divAutomatica2(TextBox uni, TextBox cu, TextBox co)
        //{
        //    if (uni.Text == "")
        //    { uni.Text = "0"; }
        //    else
        //    { uni.Text = Convert.ToDouble(uni.Text).ToString(); }
        //    if (co.Text == "")
        //    { co.Text = "0.00"; }
        //    else
        //    { co.Text = Convert.ToDouble(co.Text).ToString("N"); }

        //    if (uni.Text == "0.00" || uni.Text == "0")
        //    { cu.Text = "0.00"; }
        //    else { cu.Text = (Convert.ToDouble(co.Text) / Convert.ToDouble(uni.Text)).ToString("N"); }

        //}

        //protected void Pavimento_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtP_concreto_m2, txtP_concreto_cu, txtP_concreto_co);
        //    divAutomatica(txtP_asfalto_m2, txtP_asfalto_cu, txtP_asfalto_co);
        //    divAutomatica(txtP_emboquillado_m2, txtP_emboquillado_cu, txtP_emboquillado_co);
        //    divAutomatica(txtP_adoquinado_m2, txtP_adoquinado_cu, txtP_adoquinado_co);
        //    CostosTotales();
        //}

        //protected void Veredas_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtV_concreto_m2, txtV_concreto_cu, txtV_concreto_co);
        //    divAutomatica(txtV_adoquinado_m2, txtV_adoquinado_cu, txtV_adoquinado_co);
        //    divAutomatica(txtV_piedra_m2, txtV_piedra_cu, txtV_piedra_co);
        //    divAutomatica(txtV_emboquillado_m2, txtV_emboquillado_cu, txtV_emboquillado_co);
        //    CostosTotales();
        //}

        //protected void Drenaje_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtD_concreto_ml, txtD_concreto_cu, txtD_concreto_co);
        //    divAutomatica(txtD_piedra_ml, txtD_piedra_cu, txtD_piedra_co);
        //    CostosTotales();
        //}

        //protected void Otros_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtO_muro_m2, txtO_muro_cu, txtO_muro_co);
        //    divAutomatica(txtO_ponton_m2, txtO_ponton_cu, txtO_ponton_co);
        //    divAutomatica(txtO_bermas_m2, txtO_bermas_cu, txtO_bermas_co);
        //    divAutomatica(txtO_verdes_m2, txtO_verdes_cu, txtO_verdes_co);

        //    divAutomatica2(txtO_jardineras_u, txtO_jardineras_cu, txtO_jardineras_co);
        //    divAutomatica2(txtO_plantones_u, txtO_plantones_cu, txtO_plantones_co);
        //    divAutomatica2(txtO_sanitarias_u, txtO_sanitarias_cu, txtO_sanitarias_co);
        //    divAutomatica2(txtO_electricas_u, txtO_electricas_cu, txtO_electricas_co);

        //    divAutomatica2(txtO_bancas_u, txtO_bancas_cu, txtO_bancas_co);
        //    divAutomatica2(txtO_basureros_u, txtO_basureros_cu, txtO_basureros_co);
        //    divAutomatica2(txtO_pergolas_u, txtO_pergolas_cu, txtO_pergolas_co);
        //    divAutomatica2(txtO_glorietas_u, txtO_glorietas_cu, txtO_glorietas_co);
        //    divAutomatica(txtO_otros_u, txtO_otros_cu, txtO_otros_co);
        //    CostosTotales();
        //}

        //protected void CostosTotales()
        //{
        //    lblCostoP.Text = (Convert.ToDouble(txtP_concreto_co.Text) + Convert.ToDouble(txtP_asfalto_co.Text) + Convert.ToDouble(txtP_emboquillado_co.Text) + Convert.ToDouble(txtP_adoquinado_co.Text)).ToString("N");

        //    lblCostoV.Text = (Convert.ToDouble(txtV_concreto_co.Text) + Convert.ToDouble(txtV_adoquinado_co.Text) + Convert.ToDouble(txtV_piedra_co.Text) + Convert.ToDouble(txtV_emboquillado_co.Text)).ToString("N");
        //    lblCostoD.Text = (Convert.ToDouble(txtD_concreto_co.Text) + Convert.ToDouble(txtD_piedra_co.Text)).ToString("N");

        //    lblCostoO.Text = (Convert.ToDouble(txtO_muro_co.Text) + Convert.ToDouble(txtO_ponton_co.Text) + Convert.ToDouble(txtO_bermas_co.Text) + Convert.ToDouble(txtO_verdes_co.Text) + Convert.ToDouble(txtO_jardineras_co.Text) + Convert.ToDouble(txtO_plantones_co.Text) + Convert.ToDouble(txtO_sanitarias_co.Text) + Convert.ToDouble(txtO_electricas_co.Text) + Convert.ToDouble(txtO_bancas_co.Text) + Convert.ToDouble(txtO_basureros_co.Text) + Convert.ToDouble(txtO_pergolas_co.Text) + Convert.ToDouble(txtO_glorietas_co.Text) + Convert.ToDouble(txtO_otros_co.Text)).ToString("N");

        //    lblCostoTotalObra.Text = (Convert.ToDouble(lblCostoP.Text) + Convert.ToDouble(lblCostoV.Text) + Convert.ToDouble(lblCostoD.Text) + Convert.ToDouble(lblCostoO.Text)).ToString("N");

        //}

        //protected void btnGuardarMetasPMIB_Click(object sender, EventArgs e)
        //{
        //    BEVarPimb _BEVarPimb = new BEVarPimb();

        //    _BEVarPimb.Id_Solicitudes = Convert.ToInt64(LblID_SOLICITUD.Text);

        //    _BEVarPimb.P_concreto_m2 = Convert.ToDouble(txtP_concreto_m2.Text);
        //    _BEVarPimb.P_concreto_co = Convert.ToDouble(txtP_concreto_co.Text);
        //    _BEVarPimb.P_asfalto_m2 = Convert.ToDouble(txtP_asfalto_m2.Text);
        //    _BEVarPimb.P_asfalto_co = Convert.ToDouble(txtP_asfalto_co.Text);
        //    _BEVarPimb.P_emboquillado_m2 = Convert.ToDouble(txtP_emboquillado_m2.Text);
        //    _BEVarPimb.P_emboquillado_co = Convert.ToDouble(txtP_emboquillado_co.Text);
        //    _BEVarPimb.P_adoquinado_m2 = Convert.ToDouble(txtP_adoquinado_m2.Text);
        //    _BEVarPimb.P_adoquinado_co = Convert.ToDouble(txtP_adoquinado_co.Text);

        //    _BEVarPimb.V_concreto_m2 = Convert.ToDouble(txtV_concreto_m2.Text);
        //    _BEVarPimb.V_concreto_co = Convert.ToDouble(txtV_concreto_co.Text);
        //    _BEVarPimb.V_adoquinado_m2 = Convert.ToDouble(txtV_adoquinado_m2.Text);
        //    _BEVarPimb.V_adoquinado_co = Convert.ToDouble(txtV_adoquinado_co.Text);
        //    _BEVarPimb.V_piedra_m2 = Convert.ToDouble(txtV_piedra_m2.Text);
        //    _BEVarPimb.V_piedra_co = Convert.ToDouble(txtV_piedra_co.Text);
        //    _BEVarPimb.V_emboquillado_m2 = Convert.ToDouble(txtV_emboquillado_m2.Text);
        //    _BEVarPimb.V_emboquillado_co = Convert.ToDouble(txtV_emboquillado_co.Text);

        //    _BEVarPimb.D_concreto_ml = Convert.ToDouble(txtD_concreto_ml.Text);
        //    _BEVarPimb.D_concreto_co = Convert.ToDouble(txtD_concreto_co.Text);
        //    _BEVarPimb.D_piedra_ml = Convert.ToDouble(txtD_piedra_ml.Text);
        //    _BEVarPimb.D_piedra_co = Convert.ToDouble(txtD_piedra_co.Text);

        //    _BEVarPimb.O_muro_m2 = Convert.ToDouble(txtO_muro_m2.Text);
        //    _BEVarPimb.O_muro_co = Convert.ToDouble(txtO_muro_co.Text);
        //    _BEVarPimb.O_ponton_m2 = Convert.ToDouble(txtO_ponton_m2.Text);
        //    _BEVarPimb.O_ponton_co = Convert.ToDouble(txtO_ponton_co.Text);
        //    _BEVarPimb.O_bermas_m2 = Convert.ToDouble(txtO_bermas_m2.Text);
        //    _BEVarPimb.O_bermas_co = Convert.ToDouble(txtO_bermas_co.Text);
        //    _BEVarPimb.O_verdes_m2 = Convert.ToDouble(txtO_verdes_m2.Text);
        //    _BEVarPimb.O_verdes_co = Convert.ToDouble(txtO_verdes_co.Text);

        //    _BEVarPimb.O_jardineras_u = Convert.ToInt32(txtO_jardineras_u.Text);
        //    _BEVarPimb.O_jardineras_co = Convert.ToDouble(txtO_jardineras_co.Text);
        //    _BEVarPimb.O_plantones_u = Convert.ToInt32(txtO_plantones_u.Text);
        //    _BEVarPimb.O_plantones_co = Convert.ToDouble(txtO_plantones_co.Text);
        //    _BEVarPimb.O_sanitarias_u = Convert.ToInt32(txtO_sanitarias_u.Text);
        //    _BEVarPimb.O_sanitarias_co = Convert.ToDouble(txtO_sanitarias_co.Text);
        //    _BEVarPimb.O_electricas_u = Convert.ToInt32(txtO_electricas_u.Text);
        //    _BEVarPimb.O_electricas_co = Convert.ToDouble(txtO_electricas_co.Text);

        //    _BEVarPimb.O_bancas_u = Convert.ToInt32(txtO_bancas_u.Text);
        //    _BEVarPimb.O_bancas_co = Convert.ToDouble(txtO_bancas_co.Text);
        //    _BEVarPimb.O_basureros_u = Convert.ToInt32(txtO_basureros_u.Text);
        //    _BEVarPimb.O_basureros_co = Convert.ToDouble(txtO_basureros_co.Text);
        //    _BEVarPimb.O_pergolas_u = Convert.ToInt32(txtO_pergolas_u.Text);
        //    _BEVarPimb.O_pergolas_co = Convert.ToDouble(txtO_pergolas_co.Text);
        //    _BEVarPimb.O_glorietas_u = Convert.ToInt32(txtO_glorietas_u.Text);
        //    _BEVarPimb.O_glorietas_co = Convert.ToDouble(txtO_glorietas_co.Text);
        //    _BEVarPimb.O_otros_u = Convert.ToDouble(txtO_otros_u.Text);
        //    _BEVarPimb.O_otros_co = Convert.ToDouble(txtO_otros_co.Text);
        //    _BEVarPimb.Observaciones = txtObservacionesPMIB.Text;

        //    _BEVarPimb.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //    objBLProyecto.spiuSOL_IngresarVariablePimb(_BEVarPimb);

        //    //string script = "<script>alert('Se actualizó las metas.'); window.opener.location.reload(); window.close();</script>";
        //    string script = "<script>alert('Se actualizó las metas.');</script>";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        //    VarPmib();
        //    Up_VarPMIB.Update();

        //}


        protected void chbFlagAyuda_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var valor = grdAvanceFisicoTab2.DataKeys[row.RowIndex].Value;
            //_BEEjecucion.id_tabla = Convert.ToInt32(valor.ToString());
            string FlagAyudaLectura = ((Label)row.FindControl("lblAyudaObtiene")).Text;
            Boolean FlagAyuda = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;
            Boolean result;
            if (FlagAyudaLectura == "1")
            {
                result = true;
            }
            else
            {
                result = false;
            }

            if (FlagAyuda == result)
            {
            }
            else
            {
                _BEEjecucion.id_avanceFisico = Convert.ToInt32(valor.ToString());
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                if (FlagAyuda == true)
                {
                    _BEEjecucion.flagAyuda = "1";
                }
                else
                {
                    _BEEjecucion.flagAyuda = "0";
                }

                int val;
                try
                {
                    val = _objBLEjecucion.spu_MON_AvanceFisicoByFlagAyudaMemoria(_BEEjecucion);
                }
                catch (Exception ex)
                {
                    val = 0;
                }

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el flag de Ayuda Memoria correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_AgregarAvanceTab2.Visible = false;
                    imgbtnAvanceFisicoTab2.Visible = true;
                    CargaAvanceFisico();

                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }


        #endregion

        #region Tab3

        protected void CargaTab3()
        {
            CargaLiquidacion();
        }

        protected void CargaLiquidacion()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BELiquidacion.tipoFinanciamiento = 2;

            dt = _objBLLiquidacion.sp_MON_Liquidacion(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                txtLiquidacionTab3.Text = dt.Rows[0]["resolucion"].ToString();
                txtFechaTab3.Text = dt.Rows[0]["fecha"].ToString();
                txtMontoTab3.Text = dt.Rows[0]["montoLiquidacion"].ToString();
                //txtSaldoTab3.Text = dt.Rows[0]["saldoDevolver"].ToString();

                txtResolucionLiqSupervision.Text = dt.Rows[0]["resolucionSupervision"].ToString();
                txtFechaLiqSupervision.Text = dt.Rows[0]["fechaLiqSupervision"].ToString();
                txtMontoLiqSupervision.Text = dt.Rows[0]["montoLiqSupervision"].ToString();

                LnkbtnLiquidacionTab3.Text = dt.Rows[0]["urlLiquidacion"].ToString();
                LnkbtnActaTab3.Text = dt.Rows[0]["urlActaCierre"].ToString();
                //LnkbtnDevolucionTab3.Text = dt.Rows[0]["urlDocDevolucion"].ToString();
                lnkbtnLiqSupervisionTab3.Text = dt.Rows[0]["urlLiquidacionSupervision"].ToString();

                double val1 = 0;
                if (txtMontoTab3.Text != "")
                { val1 = Convert.ToDouble(txtMontoTab3.Text); }

                double val2 = 0;
                if (txtMontoLiqSupervision.Text != "")
                { val2 = Convert.ToDouble(txtMontoLiqSupervision.Text); }

                double val3 = 0;
                //if (txtSaldoTab3.Text != "")
                //{ val3 = Convert.ToDouble(txtSaldoTab3.Text); }

                //txtTotalTab3.Text = (val1 + val2 + val3).ToString("N");

                lblNomLiquidacion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                GeneraIcoFile(LnkbtnLiquidacionTab3.Text, imgbtnLiquidacionTab3);
                GeneraIcoFile(lnkbtnLiqSupervisionTab3.Text, imgbtnLiqSupervisionTab3);
                GeneraIcoFile(LnkbtnActaTab3.Text, imgbtnActaTab3);
                //GeneraIcoFile(LnkbtnDevolucionTab3.Text, imgbtnDevolucionTab3);


            }

        }

      
        protected Boolean validaLiquidacionTab3()
        {
            if (!validaArchivo(FileUploadLiquidacionTab3, "Documento de Liquidación de Ejecución"))
                return false;
            //if (!validaArchivo(FileUploadDevolucionTab3, "Documento de Devolución"))
            //    return false;
            if (!validaArchivo(FileUploadActaTab3, "Documento de Cierre de Convenio"))
                return false;
            if (!validaArchivo(FileUploadLiqSupervisionTab3, "Documento de Liquidación de Supervisión"))
                return false;
            if (!validaFecha(txtFechaTab3, "Fecha de Liquidacion de Consult.", true))
                return false;
            if (!validaFecha(txtFechaLiqSupervision, "Fecha de Liquidacion de Superv.", true))
                return false;

            if (txtFechaTab3.Text.Length > 0)
            {
                if (txtFechaTermino.Text.Length > 0 && Convert.ToDateTime(txtFechaTab3.Text) < Convert.ToDateTime(txtFechaTermino.Text))
                {
                    string script = "<script>alert('La fecha de liquidación debe ser mayor a la fecha de termino real.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

            }

            return true;
        }
        protected void btnGuardarTab3_OnClick(object sender, EventArgs e)
        {
            if (validaLiquidacionTab3())
            {
                _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BELiquidacion.tipoFinanciamiento = 2;

                _BELiquidacion.resolucion = txtLiquidacionTab3.Text;
                _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaTab3.Text);
                _BELiquidacion.monto = txtMontoTab3.Text;
                //_BELiquidacion.saldoDevolver = txtSaldoTab3.Text;

                _BELiquidacion.ResolucionSupervision = txtResolucionLiqSupervision.Text;
                _BELiquidacion.Date_fechaLiqSupervision = VerificaFecha(txtFechaLiqSupervision.Text);
                _BELiquidacion.MontoLiqSupervision = txtMontoLiqSupervision.Text;

                _BELiquidacion.Date_fechaLiqObraComplem = VerificaFecha("");

                _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                if (FileUploadLiqSupervisionTab3.HasFile)
                {
                    _BELiquidacion.UrlLiquidacionSupervision = _Metodo.uploadfile(FileUploadLiqSupervisionTab3);
                }
                else
                { _BELiquidacion.UrlLiquidacionSupervision = lnkbtnLiqSupervisionTab3.Text; }

                if (FileUploadLiquidacionTab3.HasFile)
                {
                    _BELiquidacion.urlLiquidacion = _Metodo.uploadfile(FileUploadLiquidacionTab3);
                }
                else
                { _BELiquidacion.urlLiquidacion = LnkbtnLiquidacionTab3.Text; }

                if (FileUploadActaTab3.HasFile)
                {
                    _BELiquidacion.urlActaCierre = _Metodo.uploadfile(FileUploadActaTab3);
                }
                else
                { _BELiquidacion.urlActaCierre = LnkbtnActaTab3.Text; }

                //if (FileUploadDevolucionTab3.HasFile)
                //{
                //    _BELiquidacion.urlDocDevolucion = _Metodo.uploadfile(FileUploadDevolucionTab3);
                //}
                //else
                //{ _BELiquidacion.urlDocDevolucion = LnkbtnDevolucionTab3.Text; }


                _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLLiquidacion.spi_MON_Liquidacion(_BELiquidacion);


                if (val == 1)
                {
                    string script = "<script>alert('Se guardo correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaLiquidacion();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }


                Up_Tab3.Update();
            }

            //        }

            //    }

            //}

        }

        //protected void LnkbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        //{

        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
        //    FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
        //        Response.End();
        //    }

        //}

        //protected void imgbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        //{
        //    ImageButton boton;
        //    boton = (ImageButton)sender;

        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
        //    FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
        //        Response.End();
        //    }

        //}

        protected void LnkbtnLiquidacion_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnLiquidacionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void txtMontoTab3_OnClick(object sender, EventArgs e)
        {
            double val1 = 0;
            if (txtMontoTab3.Text != "")
            { val1 = Convert.ToDouble(txtMontoTab3.Text); }
            txtMontoTab3.Text = val1.ToString("N");

            double val2 = 0;
            if (txtMontoLiqSupervision.Text != "")
            { val2 = Convert.ToDouble(txtMontoLiqSupervision.Text); }

            double val3 = 0;
            //if (txtSaldoTab3.Text != "")
            //{ val3 = Convert.ToDouble(txtSaldoTab3.Text); }
            //txtSaldoTab3.Text = val3.ToString("N");

            //txtTotalTab3.Text = (val1 + val2 + val3).ToString("N");

            Up_Tab3.Update();

        }

        #endregion

        #region Tab4
        protected void CargaTab4()
        {
            cargaParalizacionTab4();
            cargaPlazoTAb4();
            cargaPersupuestoTab4();
            cargaDeductivoTab4();
            CargaDdlTipoPenalidad();
            cargaPenalidadTab4();
        }

        protected void cargaParalizacionTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 6;

            grdParalizacionTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdParalizacionTab4.DataBind();
        }
        protected void grdParalizacionTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocParalizacionTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(txtTotalParalizacionTab4.Text) + Convert.ToInt32(dias.Text);
                txtTotalParalizacionTab4.Text = TotalDias.ToString();

                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        protected void grdParalizacionTab4_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_ParalizacionTab4.Text = grdParalizacionTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdParalizacionTab4.SelectedRow;

            txtDiasParalizadosTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            //txtDiasParalizadosTab4.Enabled = false;
            txtResoluciónParalizacionTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolucionParalizacionTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoParalizacionTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionParalizacionTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            lnkbtnParalizacionTab4.Text = ((ImageButton)row.FindControl("imgDocParalizacionTab4")).ToolTip;
            lblNomActualizaParalizacionTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicioParalizacionTab4.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFinParalizacionTab4.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(lnkbtnParalizacionTab4.Text, imgbtnParalizacionTab4);

            btnGuardarParalizacionTab4.Visible = false;
            btnModificarParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;
            Panel_ParalizacionTab4.Visible = true;

            Up_Tab4.Update();
        }
        protected void grdParalizacionTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdParalizacionTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    txtTotalParalizacionTab4.Text = "0";
                    cargaParalizacionTab4();
                    //ActualizaFechasTab1();
                    Up_Tab4.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void imgDocParalizacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdParalizacionTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdParalizacionTab4.Rows[row.RowIndex].FindControl("imgDocParalizacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtnAgregarParalizacionTAb4_Click(object sender, ImageClickEventArgs e)
        {
            Panel_ParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;

            txtDiasParalizadosTab4.Text = "";
            txtDiasParalizadosTab4.Enabled = false;
            txtResoluciónParalizacionTab4.Text = "";
            txtFechaResolucionParalizacionTab4.Text = "";
            txtConceptoParalizacionTab4.Text = "";
            txtObservacionParalizacionTab4.Text = "";
            txtFechaInicioParalizacionTab4.Text = "";
            txtFechaFinParalizacionTab4.Text = "";

            imgbtnParalizacionTab4.ImageUrl = "~/img/blanco.png";
            lnkbtnParalizacionTab4.Text = "";
            lblNomActualizaParalizacionTab4.Text = "";

            txtDiasParalizadosTab4.Enabled = true;
            btnGuardarParalizacionTab4.Visible = true;
            btnModificarParalizacionTab4.Visible = false;
            Up_Tab4.Update();
        }
        protected void btnGuardarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {

                //if (validaArchivo(FileUpload_ParalizacionTab4))
                //{
                _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEAmpliacion.tipoFinanciamiento = 2;
                _BEAmpliacion.id_tipo_ampliacion = 6;

                //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;
                _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ParalizacionTab4);
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                int dias = (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days + 1;
                _BEAmpliacion.ampliacion = dias.ToString();

                int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    txtTotalParalizacionTab4.Text = "0";
                    cargaParalizacionTab4();
                    //ActualizaFechasTab1();
                    //txtAmpPlazoPlaTab4.Text = "";
                    //txtResolAproPlaTab4.Text = "";
                    //txtFechaResolPlaTab4.Text = "";
                    //txtConceptoPlaTab4.Text = "";
                    //txtObservacionPlaTab4.Text = "";
                    //txtFechaInicio.Text = "";
                    //txtFechaFin.Text = "";

                    Panel_ParalizacionTab4.Visible = false;
                    imgbtnAgregarParalizacionTAb4.Visible = true;

                    Up_Tab4.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                //}
            }
        }
        protected void btnModificarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {
                //if (validaArchivo(FileUpload_ParalizacionTab4))
                //{
                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_ParalizacionTab4.Text);
                //_BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                //_BEAmpliacion.tipoFinanciamiento = 3;
                //_BEAmpliacion.id_tipo_ampliacion = 6;

                //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                int dias = (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days + 1;
                _BEAmpliacion.ampliacion = dias.ToString();

                if (FileUpload_ParalizacionTab4.HasFile && FileUpload_ParalizacionTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ParalizacionTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = lnkbtnParalizacionTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);

                //int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                if (resul == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    Panel_ParalizacionTab4.Visible = false;
                    imgbtnAgregarParalizacionTAb4.Visible = true;

                    txtTotalParalizacionTab4.Text = "0";
                    cargaParalizacionTab4();
                    ActualizaFechasTab1();

                    Up_Tab4.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                //}
            }
        }
        protected void btnCancelarParalizacionTab4_Click(object sender, EventArgs e)
        {
            Panel_ParalizacionTab4.Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = true;

            Up_Tab4.Update();
        }
        protected Boolean ValidarParalizacionTab4()
        {
            Boolean result;
            result = true;

            if (!validaFecha(txtFechaResolucionParalizacionTab4, "Fecha de Informe", true))
                return false;
            if (!validaFecha(txtFechaInicioParalizacionTab4, "Fecha de inicio de paralización", true, true))
                return false;
            if (!validaFecha(txtFechaFinParalizacionTab4, "Fecha fin de paralización", false, true))
                return false;

            if (txtFechaInicioTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text) <= Convert.ToDateTime(txtFechaInicioTab2.Text))
            {
                string script = "<script>alert('La fecha de inicio de paralización debe ser mayor a la fecha de inicio de la obra.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) < Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text))
            {
                string script = "<script>alert('La fecha fin debe ser mayor o igual a la fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaTermino.Text.Length > 0 && Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) >= Convert.ToDateTime(txtFechaTermino.Text))
            {
                string script = "<script>alert('La fecha de fin de paralización debe ser menor a la fecha de termino real de la obra.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (!validaArchivo(FileUpload_ParalizacionTab4))
                return false;

            return result;
        }
        protected void lnkbtnParalizacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnParalizacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void cargaPenalidadTab4()
        {
            int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdPenalidadTab4.DataSource = _objBLAmpliacion.sp_MON_Penalidad(id_proyecto);
            grdPenalidadTab4.DataBind();
        }
        protected void CargaDdlTipoPenalidad()
        {
            ddlTipoPenalidadTab4.DataSource = _objBLFinanciamiento.spMON_rParametro(30);
            ddlTipoPenalidadTab4.DataTextField = "nombre";
            ddlTipoPenalidadTab4.DataValueField = "valor";
            ddlTipoPenalidadTab4.DataBind();

            ddlTipoPenalidadTab4.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }
        protected void imgbtnAgregarPenalidadTab4_Click(object sender, ImageClickEventArgs e)
        {
            Panel_PenalidadTab4.Visible = true;
            imgbtnAgregarPenalidadTab4.Visible = false;

            ddlTipoPenalidadTab4.SelectedValue = "";
            txtDetalleIncumplimientoTab4.Text = "";
            txtFechaPenalidadTab4.Text = "";
            txtMontoPenalidadTab4.Text = "";
            lblNomActualizaPenalidadTab4.Text = "";

            btnGuardarPenalidadTab4.Visible = true;
            btnModificarPenalidadTab4.Visible = false;
            Up_Tab4.Update();
        }
        protected void btnGuardarPenalidadTab4_Click(object sender, EventArgs e)
        {
            if (ValidarPenalidadTab4())
            {
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int iTipoPenalidad = Convert.ToInt32(ddlTipoPenalidadTab4.SelectedValue);
                string detalleIncumplimiento = txtDetalleIncumplimientoTab4.Text;
                DateTime fechaPenalidad = VerificaFecha(txtFechaPenalidadTab4.Text);
                Decimal montoPenalidad = 0;
                decimal.TryParse(txtMontoPenalidadTab4.Text, out montoPenalidad);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.spi_MON_Penalidad(id_proyecto, iTipoPenalidad, detalleIncumplimiento, fechaPenalidad, montoPenalidad, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    cargaPenalidadTab4();
                    ddlTipoPenalidadTab4.SelectedValue = "";
                    txtFechaPenalidadTab4.Text = "";
                    txtDetalleIncumplimientoTab4.Text = "";
                    txtMontoPenalidadTab4.Text = "0";

                    Panel_PenalidadTab4.Visible = false;
                    imgbtnAgregarPenalidadTab4.Visible = true;

                    Up_Tab4.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void btnModificarPenalidadTab4_Click(object sender, EventArgs e)
        {
            if (ValidarPenalidadTab4())
            {
                int id_penalidad = Convert.ToInt32(lblId_PenalidadTab4.Text);
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int iTipoPenalidad = Convert.ToInt32(ddlTipoPenalidadTab4.SelectedValue);
                string detalleIncumplimiento = txtDetalleIncumplimientoTab4.Text;
                DateTime fechaPenalidad = VerificaFecha(txtFechaPenalidadTab4.Text);
                Decimal montoPenalidad = 0;
                decimal.TryParse(txtMontoPenalidadTab4.Text, out montoPenalidad);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int resul;
                resul = _objBLAmpliacion.spud_MON_Penalidad(id_penalidad, 1, iTipoPenalidad, detalleIncumplimiento, fechaPenalidad, montoPenalidad, id_usuario);

                if (resul == 1)
                {
                    string script = "<script>alert('¡Se grabaron correctamente los datos ingresados.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    cargaPenalidadTab4();
                    ddlTipoPenalidadTab4.SelectedValue = "";
                    txtFechaPenalidadTab4.Text = "";
                    txtDetalleIncumplimientoTab4.Text = "";
                    txtMontoPenalidadTab4.Text = "0";

                    Panel_PenalidadTab4.Visible = false;
                    imgbtnAgregarPenalidadTab4.Visible = true;

                    Up_Tab4.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void btnCancelarPenalidadTab4_Click(object sender, EventArgs e)
        {
            Panel_PenalidadTab4.Visible = false;
            imgbtnAgregarPenalidadTab4.Visible = true;

            Up_Tab4.Update();
        }
        protected Boolean ValidarPenalidadTab4()
        {
            Boolean result = true;
            if (ddlTipoPenalidadTab4.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione el Tipo de Penalidad.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                result = false;
                return result;
            }

            if (!validaFecha(txtFechaPenalidadTab4, "Fecha de Penalidad", true, true))
                return false;
            return result;
        }
        protected void grdPenalidadTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {


        }
        protected void grdPenalidadTab4_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblId_PenalidadTab4.Text = grdPenalidadTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdPenalidadTab4.SelectedRow;

            ddlTipoPenalidadTab4.SelectedValue = ((Label)row.FindControl("lbliTipoPenalidad")).Text;
            txtDetalleIncumplimientoTab4.Text = ((Label)row.FindControl("lbldetalleIncumplimiento")).Text;
            txtFechaPenalidadTab4.Text = ((Label)row.FindControl("lblfechaPenalidad")).Text;
            txtMontoPenalidadTab4.Text = Server.HtmlDecode(row.Cells[7].Text);  //((Label)row.FindControl("lblMontoPenalidad")).Text;

            lblNomActualizaPenalidadTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnGuardarPenalidadTab4.Visible = false;
            btnModificarPenalidadTab4.Visible = true;
            imgbtnAgregarPenalidadTab4.Visible = false;
            Panel_PenalidadTab4.Visible = true;

            Up_Tab4.Update();
        }
        protected void grdPenalidadTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPenalidadTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                int id_penalidad = Convert.ToInt32(objTemp.ToString());
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.spud_MON_Penalidad(id_penalidad, 2, 0, "", DateTime.Now, 0, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    cargaPenalidadTab4();
                    //ActualizaFechasTab1();
                    Up_Tab4.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }


        protected void cargaPlazoTAb4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 1;

            grdPlazoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPlazoTab4.DataBind();

        }

        protected void grdPlazoTab4_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPlaTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(lblTotalDias.Text) + Convert.ToInt32(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);



                lblTotalDias.Text = TotalDias.ToString();
            }

        }

        protected void cargaPersupuestoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 2;

            grdPresupuestoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPresupuestoTab4.DataBind();

        }

        protected void grdPresupuestoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPreTab4");
                Label dias = (Label)e.Row.FindControl("lblPresupuesto");

                TotalDias = Convert.ToDouble(lblPresupuestoAdicional.Text) + Convert.ToDouble(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                lblPresupuestoAdicional.Text = TotalDias.ToString("N");
            }
        }

        protected void cargaDeductivoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 2;
            _BEAmpliacion.id_tipo_ampliacion = 3;

            grdDeductivoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdDeductivoTab4.DataBind();

        }

        protected void grdDeductivoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double TotalDias;
            TotalDias = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocDedTab4");
                Label dias = (Label)e.Row.FindControl("lbldeductivo");

                TotalDias = Convert.ToDouble(lblDeductivoTotal.Text) + Convert.ToDouble(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                lblDeductivoTotal.Text = TotalDias.ToString("N");
            }

        }

        protected void imgAgregaPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = false;
            btnGuardarPlazoTab4.Visible = true;
            txtAmpPlazoPlaTab4.Text = "";
            txtResolAproPlaTab4.Text = "";
            txtFechaResolPlaTab4.Text = "";
            txtConceptoPlaTab4.Text = "";
            txtObservacionPlaTab4.Text = "";
            txtFechaInicio.Text = "";
            txtFechaFin.Text = "";

            imgbtnPlaTab4.ImageUrl = "~/img/blanco.png";
            LnkbtnPlaTab4.Text = "";
            lblNomUsuarioAmpDias.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = false;
            imgAgregaPlazoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPlazoTab4()
        {
            Boolean result;
            result = true;

            if (txtAmpPlazoPlaTab4.Text == "")
            {
                string script = "<script>alert('Ingrese dias de Ampliación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaFecha(txtFechaInicio, "Fecha de Inicio", true, true))
                return false;
            if (!validaFecha(txtFechaResolPlaTab4, "Fecha de Resolución", true, true))
                return false;
            if (!validaFecha(txtFechaFin, "Fecha Fin",false,true))
                return false;
            if (txtFechaFin.Text != "")
            {
                if (Convert.ToDateTime(txtFechaInicio.Text) > Convert.ToDateTime(txtFechaFin.Text))
                {
                    string script = "<script>alert('La Fecha de Inicio no puede ser mayor a la Fecha de Fin.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }
            }

            if (!validaArchivo(FileUploadPlaTab4))
                return false;

            if (txtAmpPlazoPlaTab4.Text == "")
            {
                string script = "<script>alert('Ingrese dias de Ampliación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtResolAproPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaResolPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPlazoTab4())
            {
                //if (validaArchivo(FileUploadPlaTab4))
                //{
                _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEAmpliacion.tipoFinanciamiento = 2;
                _BEAmpliacion.id_tipo_ampliacion = 1;

                _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    lblTotalDias.Text = "0";
                    cargaPlazoTAb4();
                    ActualizaFechasTab1();
                    txtAmpPlazoPlaTab4.Text = "";
                    txtResolAproPlaTab4.Text = "";
                    txtFechaResolPlaTab4.Text = "";
                    txtConceptoPlaTab4.Text = "";
                    txtObservacionPlaTab4.Text = "";
                    txtFechaInicio.Text = "";
                    txtFechaFin.Text = "";

                    Panel_PlazoTab4.Visible = false;
                    imgAgregaPlazoTab4.Visible = true;

                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                //}
            }

        }

        protected void imgDocPlaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPlazoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPlazoTab4.Rows[row.RowIndex].FindControl("imgDocPlaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imbtnAgregarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            txtMontoPreTab4.Text = "";
            txtVincuPreTab4.Text = "";
            txtResolAproPreTab4.Text = "";
            txtFechaPreTab4.Text = "";
            txtIncidenciaPreTab4.Text = "";

            txtConceptoPreTab4.Text = "";
            txtObsPreTab4.Text = "";

            imgbtnPreTab4.ImageUrl = "~/img/blanco.png";
            LnkbtnPreTab4.Text = "";
            lblNomUsuarioPresupuesto.Text = "";
            btnGuardarPresupuestoTab4.Visible = true;
            btnModificarPresupuestoTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void btnCancelarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPresupuestoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaFecha(txtFechaPreTab4, "Fecha de Resolución", true))
                return false;

            if (!validaArchivo(FileUploadPreTab4))
                return false;

            //if (txtVincuPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

        

            if (txtIncidenciaPreTab4.Text == "")
            {
                string script = "<script>alert('Ingresar porcentaje de incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                if (ValidaDecimal(txtIncidenciaPreTab4.Text) == false)
                {
                    string script = "<script>alert('Ingrese porcentaje de incidencia válido, por ejemplo: 92.2 ');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    return false;
                }

                if (Convert.ToDecimal(txtIncidenciaPreTab4.Text) > 100)
                {
                    string script = "<script>alert('Ingrese porcentaje de incidencia válido. No debe ser mayor a 100%. ');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    return false;
                }
            }

            //if (txtConceptoPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPresupuestoTab4())
            {
                //if (validaArchivo(FileUploadPreTab4))
                //{
                _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEAmpliacion.tipoFinanciamiento = 2;
                _BEAmpliacion.id_tipo_ampliacion = 2;

                _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;

                _BEAmpliacion.Date_fechaInicio = VerificaFecha("");
                _BEAmpliacion.Date_FechaFin = VerificaFecha("");


                _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                _BEAmpliacion.observacion = txtObsPreTab4.Text;
                _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);

                int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    lblPresupuestoAdicional.Text = "0";

                    cargaPersupuestoTab4();
                    txtMontoPreTab4.Text = "";
                    txtVincuPreTab4.Text = "";
                    txtResolAproPreTab4.Text = "";
                    txtFechaPreTab4.Text = "";
                    txtIncidenciaPreTab4.Text = "";

                    txtConceptoPreTab4.Text = "";
                    txtObsPreTab4.Text = "";

                    Panel_PresupuestoTab4.Visible = false;
                    imbtnAgregarPresupuestoTab4.Visible = true;

                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                //}
            }
        }

        protected void imgDocPreTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPresupuestoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPresupuestoTab4.Rows[row.RowIndex].FindControl("imgDocPreTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = false;
            btnGuardarDeductivoTab4.Visible = true;
            txtMontoDedTab4.Text = "";
            txtVincuDedTab4.Text = "";
            txtResolAproDedTab4.Text = "";
            txtFechaDedTab4.Text = "";
            txtIncidenciaDedTab4.Text = "";

            txtConceptoDedTab4.Text = "";
            txtObsDedTab4.Text = "";

            imgbtnDedTab4.ImageUrl = "~/img/blanco.png";
            LnkbtnDedTab4.Text = "";
            lblNomUsuarioDeductivo.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarDeductivoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaFecha(txtFechaDedTab4, "Fecha de Resolución", true))
                return false;

            if (!validaArchivo(FileUploadDedTab4))
                return false;
            //if (txtVincuDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaDedTab4.Text == "")
            {
                string script = "<script>alert('Ingresar porcentaje de incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                if (ValidaDecimal(txtIncidenciaDedTab4.Text) == false)
                {
                    string script = "<script>alert('Ingrese porcentaje de incidencia válido, por ejemplo: 92.2 ');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    return false;
                }

                if (Convert.ToDecimal(txtIncidenciaDedTab4.Text) > 100)
                {
                    string script = "<script>alert('Ingrese porcentaje de incidencia válido. No debe ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    return false;
                }
            }


            //if (txtConceptoDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarDeductivoTab4())
            {
                //if (validaArchivo(FileUploadDedTab4))
                //{
                _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEAmpliacion.tipoFinanciamiento = 2;
                _BEAmpliacion.id_tipo_ampliacion = 3;

                _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                _BEAmpliacion.Date_fechaInicio = VerificaFecha("");
                _BEAmpliacion.Date_FechaFin = VerificaFecha("");

                _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                _BEAmpliacion.observacion = txtObsDedTab4.Text;
                _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);

                int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    lblDeductivoTotal.Text = "0";

                    cargaDeductivoTab4();

                    txtMontoDedTab4.Text = "";
                    txtVincuDedTab4.Text = "";
                    txtResolAproDedTab4.Text = "";
                    txtFechaDedTab4.Text = "";
                    txtIncidenciaDedTab4.Text = "";

                    txtConceptoDedTab4.Text = "";
                    txtObsDedTab4.Text = "";

                    Panel_deductivoTab4.Visible = false;
                    imgbtnDeductivoTab4.Visible = true;

                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                //}
            }
        }

        protected void imgDocDedTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdDeductivoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdDeductivoTab4.Rows[row.RowIndex].FindControl("imgDocDedTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        #endregion

        #region Tab6

        protected void CargaTab6()
        {
            CargaPanelTab6();
            CargaTipoDocumentoMonitorTab6();
        }

        protected void CargaTipoDocumentoMonitorTab6()
        {
            ddlTipoArchivoTab6.DataSource = _objBLBandeja.F_spMON_TipoDocumentoMonitor();
            ddlTipoArchivoTab6.DataValueField = "valor";
            ddlTipoArchivoTab6.DataTextField = "nombre";
            ddlTipoArchivoTab6.DataBind();

            ddlTipoArchivoTab6.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void CargaPanelTab6()
        {
            _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdPanelTab6.DataSource = _objBLBandeja.F_spMON_DocumentoMonitor(_BEBandeja);
            grdPanelTab6.DataBind();
        }

        protected void imgbtnAgregarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnGuardarPanelTab6.Visible = true;
            btnModificarPanelTab6.Visible = false;
            lblNomUsuarioDocumento.Text = "";

            txtFechaTab6.Text = "";
            txtDescripcionTab6.Text = "";
            txtNombreArchivoTab6.Text = "";
            ddlTipoArchivoTab6.SelectedValue = "";
            imgbtnDocTab6.ImageUrl = "~/img/blanco.png";
            LnkbtnDocTab6.Text = "";
            Up_Tab6.Update();
        }

        protected void btnCancelarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = false;
            imgbtnAgregarPanelTab6.Visible = true;
            Up_Tab6.Update();
        }

        protected void imgDocTab6_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdPanelTab6.Rows[row.RowIndex].FindControl("imgDocTab6");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void btnGuardarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;
                _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLBandeja.spi_MON_DocumentoMonitor(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Registro correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaPanelTab6();
                    Panel_FotograficoTab6.Visible = false;
                    imgbtnAgregarPanelTab6.Visible = true;
                    Up_Tab6.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void grdPanelTab6_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab6");

                GeneraIcoFile(imb.ToolTip, imb);



            }
        }


        protected void grdPanelTab6_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPanelTab6.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.Id_documentoMonitor = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.tipo = "2";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);

                int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaPanelTab6();
                    Up_Tab6.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPanelTab6_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_documento_monitor.Text = grdPanelTab6.SelectedDataKey.Value.ToString();

            GridViewRow row = grdPanelTab6.SelectedRow;

            txtNombreArchivoTab6.Text = ((Label)row.FindControl("lblNombre")).Text;
            txtDescripcionTab6.Text = ((Label)row.FindControl("lblDescripcion")).Text;

            ddlTipoArchivoTab6.SelectedValue = ((Label)row.FindControl("lblIDTipo")).Text;

            txtFechaTab6.Text = ((Label)row.FindControl("lblFecha")).Text;
            LnkbtnDocTab6.Text = ((ImageButton)row.FindControl("imgDocTab6")).ToolTip;

            lblNomUsuarioDocumento.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnDocTab6.Text, imgbtnDocTab6);



            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnModificarPanelTab6.Visible = true;
            btnGuardarPanelTab6.Visible = false;
            Up_Tab6.Update();
        }

        protected Boolean validarPanelTab6()
        {
            Boolean result;
            result = true;

            if (txtNombreArchivoTab6.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (ddlTipoArchivoTab6.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaFecha(txtFechaTab6, "Fecha", true))
                return false;
         

            if (FileUpload_Tab6.HasFile == false && LnkbtnDocTab6.Text == "")
            {
                string script = "<script>alert('Adjuntar archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (ddlTipoArchivoTab6.SelectedValue.Equals("1"))
                {
                    if (validaArchivoFotos(FileUpload_Tab6) == false)
                    {
                        result = false;
                        return result;
                    }
                }
            }

            return result;


        }

        protected void btnModificarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    _BEBandeja.Id_documentoMonitor = Convert.ToInt32(lblId_documento_monitor.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;

                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEBandeja.tipo = "1";

                    if (FileUpload_Tab6.HasFile)
                    {
                        _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    }
                    else
                    {
                        _BEBandeja.UrlDoc = LnkbtnDocTab6.Text;
                    }


                    int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;
                        Up_Tab6.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }



        }

        protected void LnkbtnDocTab6_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkbtnDocTab6.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        #endregion

        #region Tab7

        protected void CargaTab7()
        {

        }

        protected void btnModificarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSeguimientoTab1())
            {


                _BEProceso.id_seguimiento = Convert.ToInt32(lblRegistroSeguimientoP.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaPubliTab1.Text);
                _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                _BEProceso.resultado = ddlResultadoTab1.SelectedValue;

                if (FileUploadSeguimientoTAb1.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSeguimientoTAb1.Text;
                }


                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);


                _BEProceso.observacion = txtObservacion.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Procesos_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    cargaSeguimientoProcesoTab1();


                    TxtConvocatoriaTab1.Text = "";
                    txtFechaPubliTab1.Text = "";
                    ddlTipoAdjudicacionTab1.SelectedValue = "";
                    ddlResultadoTab1.SelectedValue = "";
                    txtObservacion.Text = "";
                    LnkbtnSeguimientoTAb1.Text = "";
                    imgbtnSeguimientoTAb1.Visible = false;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSeguimientoTab1.Visible = false;
                btnModificarSeguimientoProcesoTab1.Visible = true;
                cargaSeguimientoProcesoTab1();
                Up_Tab1.Update();
                imgbtn_AgregarSeguimientoTab1.Visible = true;

            }



        }

        protected void LnkbtnSeguimientoTAb1_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSeguimientoTAb1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdSeguimientoProcesoTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoProcesoTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_seguimiento = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Procesos_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSeguimientoProcesoTab1();
                    Up_Tab1.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdSeguimientoProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdSeguimientoProcesoTab1.SelectedDataKey.Value.ToString();
            lblRegistroSeguimientoP.Text = id_seguimiento.ToString();
            GridViewRow row = grdSeguimientoProcesoTab1.SelectedRow;
            TxtConvocatoriaTab1.Text = ((Label)row.FindControl("lblConvocatoria")).Text;
            txtFechaPubliTab1.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoAdjudicacionTab1.Text = ((Label)row.FindControl("lblTipoAdjudicacion")).Text;
            ddlResultadoTab1.SelectedValue = ((Label)row.FindControl("lblResultado")).Text;
            txtObservacion.Text = ((Label)row.FindControl("lblobservacion")).Text;
            LnkbtnSeguimientoTAb1.Text = ((ImageButton)row.FindControl("imgDocTab1")).ToolTip;
            lblNomUsuarioSeguimientoProceso.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnSeguimientoTAb1.Text, imgbtnSeguimientoTAb1);



            cargaSeguimientoProcesoTab1();
            Up_Tab1.Update();
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            btnModificarSeguimientoProcesoTab1.Visible = true;
            btnGuardarSeguimientoProcesoTab1.Visible = false;

        }

        protected void btnModificarPlazoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPlazoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Ampliacion_Plazos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;


                _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);


                if (FileUploadPlaTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPlaTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    lblTotalDias.Text = "0";

                    txtAmpPlazoPlaTab4.Text = "";
                    txtResolAproPlaTab4.Text = "";
                    txtFechaResolPlaTab4.Text = "";
                    txtConceptoPlaTab4.Text = "";
                    txtObservacionPlaTab4.Text = "";
                    LnkbtnPlaTab4.Text = "";
                    txtFechaInicio.Text = "";
                    txtFechaFin.Text = "";
                    imgbtnPlaTab4.Visible = false;
                    imgAgregaPlazoTab4.Visible = true;
                    //cargaPlazoTAb4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PlazoTab4.Visible = false;
                cargaPlazoTAb4();
                ActualizaFechasTab1();
                btnModificarPlazoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void imgbtnPlaTabb4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPlaTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void grdPlazoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPlazoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    lblTotalDias.Text = "0";
                    cargaPlazoTAb4();
                    ActualizaFechasTab1();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPlazoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPlazoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Ampliacion_Plazos.Text = id_ampliacion.ToString();
            GridViewRow row = grdPlazoTab4.SelectedRow;

            txtAmpPlazoPlaTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            txtResolAproPlaTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolPlaTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoPlaTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionPlaTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            LnkbtnPlaTab4.Text = ((ImageButton)row.FindControl("imgDocPlaTab4")).ToolTip;
            txtFechaInicio.Text = ((Label)row.FindControl("lblfechaInicio")).Text;
            txtFechaFin.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(LnkbtnPlaTab4.Text, imgbtnPlaTab4);



            btnGuardarPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            Panel_PlazoTab4.Visible = true;
            Up_Tab4.Update();


        }

        protected void btnModificarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPresupuestoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Adicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;
                _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                _BEAmpliacion.observacion = txtObsPreTab4.Text;

                if (FileUploadPreTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPreTab4.Text;
                }



                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    lblPresupuestoAdicional.Text = "0";

                    txtMontoPreTab4.Text = "";
                    txtVincuPreTab4.Text = "";
                    txtResolAproPreTab4.Text = "";
                    txtFechaPreTab4.Text = "";
                    txtIncidenciaPreTab4.Text = "";
                    LnkbtnPreTab4.Text = "";
                    txtConceptoPreTab4.Text = "";
                    txtObsPreTab4.Text = "";
                    imgbtnPreTab4.Visible = false;
                    imbtnAgregarPresupuestoTab4.Visible = true;

                    cargaPersupuestoTab4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PresupuestoTab4.Visible = false;
                cargaPersupuestoTab4();
                btnModificarPresupuestoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void LnkbtnPreTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPreTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void grdPresupuestoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPresupuestoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaPersupuestoTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPresupuestoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPresupuestoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Adicional.Text = id_ampliacion.ToString();
            GridViewRow row = grdPresupuestoTab4.SelectedRow;

            txtMontoPreTab4.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtVincuPreTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproPreTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaPreTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaPreTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;
            txtConceptoPreTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsPreTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;


            LnkbtnPreTab4.Text = ((ImageButton)row.FindControl("imgDocPreTab4")).ToolTip;
            lblNomUsuarioPresupuesto.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnPreTab4.Text, imgbtnPreTab4);


            btnGuardarPresupuestoTab4.Visible = false;
            btnModificarPresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            Panel_PresupuestoTab4.Visible = true;

            Up_Tab4.Update();


        }

        protected void btnModificarDeductivoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarDeductivoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Deductivo.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                _BEAmpliacion.observacion = txtObsDedTab4.Text;

                if (FileUploadDedTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnDedTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    lblDeductivoTotal.Text = "0";

                    txtMontoDedTab4.Text = "";
                    txtVincuDedTab4.Text = "";
                    txtResolAproDedTab4.Text = "";
                    txtFechaDedTab4.Text = "";
                    txtIncidenciaDedTab4.Text = "";

                    txtConceptoDedTab4.Text = "";
                    txtObsDedTab4.Text = "";
                    LnkbtnDedTab4.Text = "";
                    imgbtnDedTab4.Visible = false;
                    imgbtnDeductivoTab4.Visible = true;
                    cargaDeductivoTab4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_deductivoTab4.Visible = false;
                cargaDeductivoTab4();
                btnModificarDeductivoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }


        protected void LnkbtnDedTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnDedTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void grdDeductivoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdDeductivoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaDeductivoTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdDeductivoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdDeductivoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Deductivo.Text = id_ampliacion.ToString();
            GridViewRow row = grdDeductivoTab4.SelectedRow;

            txtMontoDedTab4.Text = Server.HtmlDecode(row.Cells[2].Text);
            txtVincuDedTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproDedTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaDedTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaDedTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;

            txtConceptoDedTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsDedTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioDeductivo.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            LnkbtnDedTab4.Text = ((ImageButton)row.FindControl("imgDocDedTab4")).ToolTip;


            GeneraIcoFile(LnkbtnDedTab4.Text, imgbtnDedTab4);

            btnGuardarDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            Panel_deductivoTab4.Visible = true;

            Up_Tab4.Update();


        }


        protected void grdAvanceFisicoTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoTab2.SelectedDataKey.Value.ToString();
            lblRegistro_Avance.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoTab2.SelectedRow;

            txtInformeTab2.Text = ((Label)row.FindControl("lblInforme")).Text;
            txtFechaTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtAvanceTab2.Text = ((Label)row.FindControl("lblAvance")).Text;
            txtAvanceFinancieroTab2.Text = ((Label)row.FindControl("lblAvanceFinaciero")).Text;
            txtSituacionTab2.Text = ((Label)row.FindControl("lblSitucion")).Text;

            lnkbtnAvanceTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;
            GeneraIcoFile(lnkbtnAvanceTab2.Text, imgbtnAvanceTab2);

            txtAccionesPrevistasTab2.Text = ((Label)row.FindControl("lblAccionesPrevistas")).Text;
            txtAccionesRealizadasTab2.Text = ((Label)row.FindControl("lblAccionesRealizadas")).Text;

            CargaTipoEstadosDetalle();
            ddlTipoEstadoInformeTab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoEjecuccion")).Text;
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);
            ddlTipoSubEstadoInformeTab2.SelectedValue = ((Label)row.FindControl("lblId_tipoSubEstadoEjecucion")).Text;

            chbAyudaMemoriaTab2.Checked = (((CheckBox)row.FindControl("chbFlagAyuda")).Checked);
            chbAyudaMemoriaTab2.Enabled = false;

            lblNomUsuarioFlagAyudaTab2.Text = "(Actualizó: " + ((Label)row.FindControl("lblId_UsuarioFlag")).Text + " - " + ((Label)row.FindControl("lblFecha_UpdateFlag")).Text + ")";

            lblNomUsuarioAvance.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnGuardarAvanceTab2.Visible = false;
            btnModificarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;
            Panel_AgregarAvanceTab2.Visible = true;

            Up_Tab2.Update();


        }

        protected void grdAvanceFisicoTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_AvanceFisico_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected bool ValidarAvanceTab2()
        {
            string result = "";

            if (txtFechaTab2.Text == "")
            {
                result = result + "*Ingrese fecha.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha.\\n";
            }
            else if (Convert.ToDateTime(txtFechaTab2.Text) > DateTime.Today)
            {
                result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

            }

            if (txtAvanceTab2.Text == "")
            {
                result = result + "*Ingresar avance físico .\\n";
            }
            else if (ValidaDecimal(txtAvanceTab2.Text) == false)
            {
                result = result + "*Ingrese avance fisico valido, por ejemplo: 92.2 .\\n";
            }
            else if (Convert.ToDecimal(txtAvanceTab2.Text) > 100)
            {
                result = result + "*Ingrese avance fisico válido. No debe ser mayor a 100%.\\n";
            }


            if (txtAvanceFinancieroTab2.Text == "")
            {
                result = result + "*Ingresar avance financiero .\\n";
            }
            else if (ValidaDecimal(txtAvanceFinancieroTab2.Text) == false)
            {
                result = result + "*Ingrese avance financiero valido, por ejemplo: 92.2 .\\n";
            }
            else if (Convert.ToDecimal(txtAvanceFinancieroTab2.Text) > 100)
            {
                result = result + "*Ingrese avance financiero válido. No debe ser mayor a 100%.\\n";
            }


            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals(""))
            {
                result = result + "*Seleccionar estado.\\n";
            }
            else if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") && ddlTipoSubEstadoInformeTab2.Visible == true)
            {
                result = result + "*Seleccionar Sub Estado.\\n";
            }

        

            // Concluido - En Elaboración
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("22") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("35"))
            {
                if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlTipoEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.\\n";
                }
           }

            // Concluido
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("22"))
            {
                if (txtFechaTermino.Text.Length == 0)
                {
                    result = result + "*Para registrar el estado Concluido debe registrar primero la fecha de termino real.\\n";
                }

            }

            if (lblIdEstado.Text.Trim() != "")
            {
                if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue), 2)))
                {
                    result = result + "*No puede volver el proyecto a Actos Previos.\\n";

                }
            }

            if (!validaArchivo(FileUploadAvanceTab2))
            { return false; }

            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                return false;
            }
            else
            {
                return true;
            }

        }

        protected void btnModificarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {

                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblRegistro_Avance.Text);
                    //_BEFinanciamiento.tipoFinanciamiento = 3;
                    _BEEjecucion.informe = txtInformeTab2.Text;
                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtFechaTab2.Text);
                    _BEEjecucion.avance = txtAvanceTab2.Text;
                    _BEEjecucion.financieroReal = txtAvanceFinancieroTab2.Text;
                    _BEEjecucion.situacion = txtSituacionTab2.Text;
                    _BEEjecucion.accionesPorRealizar = txtAccionesPrevistasTab2.Text;
                    _BEEjecucion.accionesRealizadas = txtAccionesRealizadasTab2.Text;
                    _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue);
                    _BEEjecucion.id_tipoSubEstadoEjecucion = ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") ? "0" : ddlTipoSubEstadoInformeTab2.SelectedValue;

                    if (FileUploadAvanceTab2.PostedFile.ContentLength > 0)
                    {
                        _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDoc = lnkbtnAvanceTab2.Text;
                    }

                    if (chbAyudaMemoriaTab2.Checked == true)
                    {
                        _BEEjecucion.flagAyuda = "1";
                    }
                    else
                    {
                        _BEEjecucion.flagAyuda = "0";
                    }

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int resul;
                    resul = _objBLEjecucion.spud_MON_AvanceFisico_Estudio_Editar(_BEEjecucion);
                    if (resul == 1)
                    {
                        string script = "<script>alert('Registro Correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        txtInformeTab2.Text = "";
                        txtFechaTab2.Text = "";
                        txtAvanceTab2.Text = "";
                        txtSituacionTab2.Text = "";

                        imgbtnAvanceFisicoTab2.Visible = true;
                        CargaAvanceFisico();

                        Panel_AgregarAvanceTab2.Visible = false;
                        CargaAvanceFisico();
                        btnModificarAvanceTab2.Visible = true;

                        CargaDatosTab2();
                        Up_Tab2.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }



                }
            }
        }
        protected void grdConsultoresTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdConsultoresTab1.SelectedDataKey.Value.ToString();
            lblRegistroContratista.Text = id_seguimiento.ToString();
            GridViewRow row = grdConsultoresTab1.SelectedRow;

            txtContratistaGrupTab1.Text = ((Label)row.FindControl("lblNombConsultor")).Text;
            txtRucGrupTab1.Text = ((Label)row.FindControl("lblRuc")).Text;
            txtRepresentanGrupTAb1.Text = ((Label)row.FindControl("lblRepresentante")).Text;

            cargaConsultoresTab1();
            Up_Tab1.Update();
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = false;
            btn_modificarConsultor.Visible = true;

        }

        protected void btn_modificarConsultor_OnClick(object sender, EventArgs e)
        {

            if (ValidarGrupoConsorcioTab1())
            {

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(lblRegistroContratista.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                _BEProceso.rucContratista = txtRucGrupTab1.Text;
                _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Consorcio_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();

                    txtContratistaGrupTab1.Text = "";
                    txtRucGrupTab1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";

                    Panel_AgregarConsultorTab1.Visible = false;
                    cargaConsultoresTab1();
                    btn_modificarConsultor.Visible = true;
                    imgbtn_AgregarConsultorTab1.Visible = true;
                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }


        protected void grdConsultoresTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdConsultoresTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Consorcio_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaConsultoresTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        #endregion

        #region Tab8

        protected void CargaTab8()
        {

            grdHistorialTab8.DataSource = _objBLHistorial.spMON_Proyecto_HistorialMonitoreo(Convert.ToInt32(LblID_PROYECTO.Text));
            grdHistorialTab8.DataBind();

        }



        protected void lnkbtnDocTab8_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnDocTab8.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnDocTab8.Text);
                Response.End();
            }

        }

        protected void grdHistorialTab8_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab8");

                GeneraIcoFile(imb.ToolTip, imb);

            }


        }

        protected void imgDocTab8_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdHistorialTab8.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdHistorialTab8.Rows[row.RowIndex].FindControl("imgDocTab8");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void lnkbtnVerTab8_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            // string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            // int id_ampliacion = Convert.ToInt32(grdHistorialTab8.DataKeys[row.RowIndex].Values[0].ToString());

            Label id_proyecto = (Label)grdHistorialTab8.Rows[row.RowIndex].FindControl("lblId_proyectoTab8");

            string script = "<script>window.open('Registro_ExpedienteTecnico.aspx?id=" + id_proyecto.Text + "&acceso=0','_blank')</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            Up_Tab8.Update();
        }
        protected Boolean validarPermiso()
        {
            Boolean result;
            result = true;

            if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 23 || Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 24)
            {

            }
            else
            {
                string script = "<script>alert('No tiene permiso para realizar la acción seleccionada.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            return result;

        }
        protected void grdHistorialTab8_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (validarPermiso())
            {
                if (e.CommandName == "eliminar")
                {
                    Control ctl = e.CommandSource as Control;
                    GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                    object objTemp = grdHistorialTab8.DataKeys[CurrentRow.RowIndex].Value as object;

                    _BEHistorial.Id_proyecto_historial = Convert.ToInt32(objTemp.ToString());
                    _BEHistorial.Tipo = 2;
                    _BEHistorial.Id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEHistorial.Date_fecha = Convert.ToDateTime("09/09/2099");

                    int val;
                    try
                    {
                        val = _objBLHistorial.spud_MON_Proyecto_HistorialMonitoreo(_BEHistorial);
                    }
                    catch (Exception ex)
                    {
                        val = 0;
                    }


                    if (val == 1)
                    {
                        string script = "<script>alert('Eliminación Correcta.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaTab8();
                        Up_Tab8.Update();


                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }

        protected void grdHistorialTab8_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grdHistorialTab8.SelectedDataKey.Value.ToString();
            lblId_proyectoHistorial.Text = id.ToString();

            GridViewRow row = grdHistorialTab8.SelectedRow;
            txtFechaTab8.Text = ((Label)row.FindControl("lblFechaTab8")).Text;
            txtComentarioTab8.Text = ((Label)row.FindControl("lblComentarioTab8")).Text;
            lnkbtnDocTab8.Text = ((ImageButton)row.FindControl("imgDocTab8")).ToolTip;

            GeneraIcoFile(lnkbtnDocTab8.Text, imgbtnDocTab8);

            btnModificarHistorialTab8.Visible = true;
            btnGuardarHistorialTab8.Visible = false;
            Panel_HistorialTab8.Visible = true;
            imgbtnAgregarHistorialTab8.Visible = false;
            Up_Tab8.Update();

        }


        protected void imgbtnAgregarHistorialTab8_OnClick(object sender, EventArgs e)
        {
            Panel_HistorialTab8.Visible = true;
            imgbtnAgregarHistorialTab8.Visible = false;
            btnGuardarHistorialTab8.Visible = true;
            btnModificarHistorialTab8.Visible = false;

            txtFechaTab8.Text = "";
            txtComentarioTab8.Text = "";
            imgbtnDocTab8.ImageUrl = "~/img/blanco.png";
            lnkbtnDocTab8.Text = "";

            Up_Tab8.Update();


        }


        protected void btnCancelarHistorialTab8_OnClick(object sender, EventArgs e)
        {
            Panel_HistorialTab8.Visible = false;
            imgbtnAgregarHistorialTab8.Visible = true;
            Up_Tab8.Update();
        }

        protected void btnGuardarHistorialTab8_OnClick(object sender, EventArgs e)
        {
            //if (validaArchivo(FileUploadTab8))
            //{

            if (ValidarHistorialTab8())
            {
                _BEHistorial.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEHistorial.Id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEHistorial.Snip = lblSNIP.Text;
                _BEHistorial.TipoFinanciamiento = 2;
                _BEHistorial.Comentario = txtComentarioTab8.Text;
                _BEHistorial.Date_fecha = Convert.ToDateTime(txtFechaTab8.Text);
                _BEHistorial.Url = _Metodo.uploadfile(FileUploadTab8);

                int val;
                try
                {
                    val = _objBLHistorial.spi_MON_PROYECTO_HistorialMonitoreo(_BEHistorial);
                }
                catch (Exception ex)
                {
                    val = 0;
                }



                if (val > 1)
                {
                    string script = "<script>alert('Se registró correctamente.');close();window.open('Registro_ExpedienteTecnico.aspx?id=" + val + "&acceso=1','_blank')</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Up_Tab8.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
            //}

        }
        protected void btnModificarHistorialTab8_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadTab8))
            {

                if (ValidarHistorialTab8())
                {
                    _BEHistorial.Id_proyecto_historial = Convert.ToInt32(lblId_proyectoHistorial.Text);
                    _BEHistorial.Id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEHistorial.Comentario = txtComentarioTab8.Text;
                    _BEHistorial.Date_fecha = Convert.ToDateTime(txtFechaTab8.Text);
                    _BEHistorial.Url = _Metodo.uploadfile(FileUploadTab8);
                    _BEHistorial.Tipo = 1;

                    int val;
                    try
                    {
                        val = _objBLHistorial.spud_MON_Proyecto_HistorialMonitoreo(_BEHistorial);
                    }
                    catch (Exception ex)
                    {
                        val = 0;
                    }



                    if (val == 1)
                    {
                        string script = "<script>alert('Se Actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaTab8();
                        Panel_HistorialTab8.Visible = false;
                        imgbtnAgregarHistorialTab8.Visible = true;
                        Up_Tab8.Update();


                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected Boolean ValidarHistorialTab8()
        {
            Boolean result;
            result = true;

            if (!validaFecha(txtFechaTab8, "Fecha"))
                return false;

            if (txtComentarioTab8.Text.Length < 18)
            {
                string script = "<script>alert('Ingresar más de 10 caracteres en el comentario.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaArchivo(FileUploadTab8))
                return false;

            return result;

        }


        #endregion

        #region Tab9

        protected void CargaTab9(int idproy)
        {
            CargaDdlSistemaInversion();
            CargaDatosTab9();

            CargaDatosRegistroEstudioTab9();

            CargaDatosSupervisionEstudioTab9();

            CargaDatosRevisionEstudioTab9();

            CargaDdlTipoDocumentoTab9(ddlTipoDocumentoTab9,3);
            CargaDdlTipoDocumentoTab9(ddlTipoDocumento2Tab9, 4);
            CargaDdlTipoRegistroTab9();
            CargaDatosAprobacionETTab9();
            //_BEFinanciamiento.id_proyecto = idproy;
            //_BEFinanciamiento.tipoFinanciamiento = 1;
            //CargaSOSEMEjecutorasTab0();
            //CargaDdlTipoConvenio();
            //CargaConveniosDirectaTab0();
            //CargaFinanSOSEMTab0();



        }
        protected void CargaDatosTab9()
        {
            dt = _objBLFinanciamiento.F_spMON_FinanciamientoDirecta(Convert.ToInt32(LblID_PROYECTO.Text));

            if (dt.Rows.Count > 0)
            {
                //txtCostoEEstudioTab0.Text = dt.Rows[0]["costoEtapa"].ToString();
                //txtCostoSEstudioTab0.Text = dt.Rows[0]["costoSupervision"].ToString();
                //string total = dt.Rows[0]["CostoTotal"].ToString();
                //txtCostoTEstudioTab0.Text = total == "" ? total : decimal.Parse(total).ToString("N2");

                //lnkBtnCostoETab0.Text = dt.Rows[0]["urlCostoEtapa"].ToString();
                //GeneraIcoFile(lnkBtnCostoETab0.Text, imgBtnCostoETab0);
                //lnkBtnCostoSTab0.Text = dt.Rows[0]["urlCostoSupervision"].ToString();
                //GeneraIcoFile(lnkBtnCostoSTab0.Text, imgBtnCostoSTab0);
                //lnkBtnPlanETab0.Text = dt.Rows[0]["urlElaboracionEtapa"].ToString();
                //GeneraIcoFile(lnkBtnPlanETab0.Text, imgBtnPlanEEstudioTab0);
                //lnkBtnPlanSEstudioTab0.Text = dt.Rows[0]["urlElaboracionSupervision"].ToString();
                //GeneraIcoFile(lnkBtnPlanSEstudioTab0.Text, imgBtnPlanSEstudioTab0);

                ddlSistemaInversionTab9.SelectedValue = dt.Rows[0]["iTipoSistemaInversion"].ToString();
                lblNomUsuarioInversion.Text = "Actualizó: " + dt.Rows[0]["usuarioSistemaInversion"].ToString() + " - " + dt.Rows[0]["fechaUpdateSistemaInversion"].ToString();
            }
        }

        protected void CargaDdlSistemaInversion()
        {
            ddlSistemaInversionTab9.DataSource = _objBLFinanciamiento.spMON_rParametro(29).Tables[0];
            ddlSistemaInversionTab9.DataTextField = "nombre";
            ddlSistemaInversionTab9.DataValueField = "valor";
            ddlSistemaInversionTab9.DataBind();
            ddlSistemaInversionTab9.Items.Insert(0, new ListItem("-SELECCIONE-", ""));
        }
        protected void btnGuardarInversionTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarInversionTab9())
            {
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int iTipoSistema = Convert.ToInt32(ddlSistemaInversionTab9.SelectedValue);

                int val = _objBLFinanciamiento.F_spu_MON_SistemaInversion(id_proyecto, iTipoSistema, id_usuario);
                if (val == 1)
                {
                    string script = "<script>alert('¡Se grabó correctamente el tipo de sistema de inversión usado.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('¡Vuelva a Intentarlo después.!');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected Boolean ValidarInversionTab9()
        {
            string iTipoSistemaInversion = ddlSistemaInversionTab9.SelectedValue;
            if (iTipoSistemaInversion == "")
            {
                string script = "<script>alert('¡Seleccione el Sistema de Inversión Usado.!');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            return true;
        }

        protected void CargaDatosRegistroEstudioTab9()
        {
            int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdRegistroEstudioTab9.DataSource = _objBLRegistroInversion.spMON_FaseInversion(id_proyecto);
            grdRegistroEstudioTab9.DataBind();
        }
        protected void CargaDdlTipoRegistroTab9()
        {
            ddlTipoRegistroTab9.DataSource = _objBLFinanciamiento.spMON_rParametro(33);
            ddlTipoRegistroTab9.DataTextField = "nombre";
            ddlTipoRegistroTab9.DataValueField = "valor";
            ddlTipoRegistroTab9.DataBind();
            ddlTipoRegistroTab9.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void grdRegistroEstudioTab9_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdRegistroEstudioTab9_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_registro = grdRegistroEstudioTab9.SelectedDataKey.Value.ToString();
            lblRegistroRegistroE.Text = id_registro.ToString();
            GridViewRow row = grdRegistroEstudioTab9.SelectedRow;
            //lblIdTipoRegistroInversion
            ddlTipoRegistroTab9.SelectedValue = ((Label)row.FindControl("lblIdTipoRegistroInversion")).Text;
            txtFechaRegistroETab9.Text = ((Label)row.FindControl("lblFechaRegistro")).Text;
            txtMontoRTab9.Text = Server.HtmlDecode(row.Cells[3].Text); ;//((Label)row.FindControl("lblMontoRegistrado")).Text;
            txtEnlaceRTab9.Text = ((Label)row.FindControl("lblEnlace")).Text;
            lblNomUsuarioRegistroEstudio.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            Up_Tab9.Update();
            Panel_AgregarRegistroEstudioTab9.Visible = true;
            imgbtn_AgregarRegistroEstudioTab9.Visible = false;
            btnModificarRegistroEstudioTab9.Visible = true;
            btnGuardarRegistroEstudioTab9.Visible = false;

        }
        protected void grdRegistroEstudioTab9_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdRegistroEstudioTab9.DataKeys[CurrentRow.RowIndex].Value as object;
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int id_registro = Convert.ToInt32(objTemp.ToString());
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLRegistroInversion.spud_MON_FaseInversion(id_proyecto,0,0,DateTime.Now,"",id_usuario,id_registro,2);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosRegistroEstudioTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void imgbtn_AgregarRegistroEstudioTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarRegistroEstudioTab9.Visible = true;
            imgbtn_AgregarRegistroEstudioTab9.Visible = false;
            lblNomUsuarioRegistroEstudio.Text = "";
            btnGuardarRegistroEstudioTab9.Visible = true;
            btnModificarRegistroEstudioTab9.Visible = false;
            Up_Tab9.Update();
        }
        protected Boolean ValidarRegistroEstudioTab9()
        {
            Boolean result;
            result = true;

            if (!validaFecha(txtFechaRegistroETab9,"Fecha de Registro",true,true))
                return false;

            if (ddlTipoRegistroTab9.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione el Tipo de Registro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            return result;
        }
        protected void btnGuardarRegistroEstudioTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarRegistroEstudioTab9())
            {
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int iTipoRegistro = Convert.ToInt32(ddlTipoRegistroTab9.SelectedValue);
                decimal montoRegistrado = (txtMontoRTab9.Text=="")?0:Convert.ToDecimal(txtMontoRTab9.Text);
                string enlace = txtEnlaceRTab9.Text;
                DateTime fechaRegistro = VerificaFecha(txtFechaRegistroETab9.Text);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLRegistroInversion.spi_MON_FaseInversion(id_proyecto,iTipoRegistro,montoRegistrado,fechaRegistro,enlace,id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtMontoRTab9.Text = "0.00";
                    txtFechaRegistroETab9.Text = "";
                    txtEnlaceRTab9.Text = "";
                    ddlTipoRegistroTab9.SelectedValue= "";

                    Panel_AgregarRegistroEstudioTab9.Visible = false;
                    imgbtn_AgregarRegistroEstudioTab9.Visible = true;
                    CargaDatosRegistroEstudioTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void btnModificarRegistroEstudioTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarRegistroEstudioTab9())
            {
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                int id_registro = Convert.ToInt32(lblRegistroRegistroE.Text);
                int iTipoRegistro = Convert.ToInt32(ddlTipoRegistroTab9.SelectedValue);
                decimal montoRegistrado = Convert.ToDecimal(txtMontoRTab9.Text);
                string enlace = txtEnlaceRTab9.Text;
                DateTime fechaRegistro = VerificaFecha(txtFechaRegistroETab9.Text);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int resul = _objBLRegistroInversion.spud_MON_FaseInversion(id_proyecto,iTipoRegistro,montoRegistrado,fechaRegistro,enlace,id_usuario,id_registro,1);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosRegistroEstudioTab9();
                    txtMontoRTab9.Text = "0.00";
                    txtFechaRegistroETab9.Text = "";
                    txtEnlaceRTab9.Text = "";
                    ddlTipoRegistroTab9.SelectedValue = "";
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                Panel_AgregarRegistroEstudioTab9.Visible = false;
                btnModificarRegistroEstudioTab9.Visible = true;
                CargaDatosRegistroEstudioTab9();
                Up_Tab9.Update();
                imgbtn_AgregarRegistroEstudioTab9.Visible = true;
            }
        }
        protected void btnCancelarRegistroEstudioTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarRegistroEstudioTab9.Visible = false;
            imgbtn_AgregarRegistroEstudioTab9.Visible = true;
            Up_Tab9.Update();
        }

        protected void CargaDatosSupervisionEstudioTab9()
        {
            int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdSupervisionEstudioTab9.DataSource = _objBLDecViabilidad.spMON_Aprobacion(id_proyecto, 5);
            grdSupervisionEstudioTab9.DataBind();
        }
        protected void grdSupervisionEstudioTab9_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDoc1Tab9");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        protected void grdSupervisionEstudioTab9_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_aprobacion = grdSupervisionEstudioTab9.SelectedDataKey.Value.ToString();
            lblRegistroSupervisionE.Text = id_aprobacion.ToString();
            GridViewRow row = grdSupervisionEstudioTab9.SelectedRow;
            txtDetalleDocumento1.Text = ((Label)row.FindControl("lblDetalleDocumento")).Text;
            txtFechaSupervisionETab9.Text = ((Label)row.FindControl("lblFecha")).Text;

            LnkBtnSupervisionEstudioTab9.Text = ((ImageButton)row.FindControl("imgDoc1Tab9")).ToolTip;
            lblNomUsuarioSupervisionEstudio.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            GeneraIcoFile(LnkBtnSupervisionEstudioTab9.Text, imgbtnSupervisionEstudioTab9);

            //cargaSeguimientoProcesoTab1();
            Up_Tab9.Update();
            Panel_AgregarSupervisionEstudioTab9.Visible = true;
            imgbtn_AgregarSupervisionEstudioTab9.Visible = false;
            btnModificarSupervisionEstudioTab9.Visible = true;
            btnGuardarSupervisionEstudioTab9.Visible = false;

        }
        protected void grdSupervisionEstudioTab9_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSupervisionEstudioTab9.DataKeys[CurrentRow.RowIndex].Value as object;
                int id_aprobacion = Convert.ToInt32(objTemp.ToString());
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLDecViabilidad.spud_MON_Aprobacion(id_aprobacion, 0, "", DateTime.Now, "", 2, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosSupervisionEstudioTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void imgDoc1Tab9_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_aprobacion = Convert.ToInt32(grdSupervisionEstudioTab9.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdSupervisionEstudioTab9.Rows[row.RowIndex].FindControl("imgDoc1Tab9");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtn_AgregarSupervisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSupervisionEstudioTab9.Visible = true;
            imgbtn_AgregarSupervisionEstudioTab9.Visible = false;
            lblNomUsuarioSupervisionEstudio.Text = "";
            btnGuardarSupervisionEstudioTab9.Visible = true;
            btnModificarSupervisionEstudioTab9.Visible = false;
            Up_Tab9.Update();
        }
        protected void LnkBtnSupervisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkBtnSupervisionEstudioTab9.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected Boolean ValidarSupervisionEstudioTab9()
        {
            Boolean result;
            result = true;

            if (txtDetalleDocumento1.Text == "")
            {
                string script = "<script>alert('Ingrese el detalle del documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                result = false;
                return result;
            }

            if (txtFechaSupervisionETab9.Text == "")
            {
                string script = "<script>alert('Ingrese la Fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaSupervisionETab9.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if ((DateTime.Now.Date - Convert.ToDateTime(txtFechaSupervisionETab9.Text)).Days < 0)
            {
                string script = "<script>alert('La Fecha debe ser menor o igual a la Fecha Actual.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (!validaArchivo(FileUploadSupervisionEstudioTab9))
                return false;

            return result;
        }
        protected void btnGuardarSupervisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarSupervisionEstudioTab9())
            {
                //if (validaArchivo(FileUploadSupervisionEstudioTab9))
                //{
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                string detalleDocumento = txtDetalleDocumento1.Text;
                DateTime fecha = VerificaFecha(txtFechaSupervisionETab9.Text);
                string urlDoc = _Metodo.uploadfile(FileUploadSupervisionEstudioTab9);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLDecViabilidad.spi_MON_Aprobacion(id_proyecto, 5, 0, detalleDocumento, fecha, urlDoc, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtDetalleDocumento1.Text = "";
                    txtFechaSupervisionETab9.Text = "";
                    LnkBtnSupervisionEstudioTab9.Text = "";

                    Panel_AgregarSupervisionEstudioTab9.Visible = false;
                    imgbtn_AgregarSupervisionEstudioTab9.Visible = true;
                    CargaDatosSupervisionEstudioTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                //}
            }
        }
        protected void btnModificarSupervisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarSupervisionEstudioTab9())
            {
                int id_aprobacion = Convert.ToInt32(lblRegistroSupervisionE.Text);
                string detalleDocumento = txtDetalleDocumento1.Text;
                DateTime fecha = VerificaFecha(txtFechaSupervisionETab9.Text);
                string urlDoc = "";
                if (FileUploadSupervisionEstudioTab9.HasFile && validaArchivo(FileUploadSupervisionEstudioTab9))
                    urlDoc = _Metodo.uploadfile(FileUploadSupervisionEstudioTab9);
                else
                    urlDoc = LnkBtnSupervisionEstudioTab9.Text;
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int resul = _objBLDecViabilidad.spud_MON_Aprobacion(id_aprobacion, 0, detalleDocumento, fecha, urlDoc, 1, id_usuario);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosSupervisionEstudioTab9();
                    txtDetalleDocumento1.Text = "";
                    txtFechaSupervisionETab9.Text = "";
                    LnkBtnSupervisionEstudioTab9.Text = "";
                    imgbtnSupervisionEstudioTab9.Visible = false;
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                Panel_AgregarSupervisionEstudioTab9.Visible = false;
                btnModificarSupervisionEstudioTab9.Visible = true;
                CargaDatosSupervisionEstudioTab9();
                Up_Tab9.Update();
                imgbtn_AgregarSupervisionEstudioTab9.Visible = true;
            }
        }
        protected void btnCancelarSupervisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSupervisionEstudioTab9.Visible = false;
            imgbtn_AgregarSupervisionEstudioTab9.Visible = true;
            Up_Tab9.Update();
        }

        protected void CargaDatosRevisionEstudioTab9()
        {
            int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdRevisionEstudioTab9.DataSource = _objBLDecViabilidad.spMON_Aprobacion(id_proyecto, 3);
            grdRevisionEstudioTab9.DataBind();
        }
        protected void CargaDdlTipoDocumentoTab9(DropDownList ddl, int idTipoDocumentoAprobacion)
        {
            ddl.DataSource = _objBLDecViabilidad.spMON_TipoDocumentoAprobacion(idTipoDocumentoAprobacion);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void grdRevisionEstudioTab9_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab9");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        protected void grdRevisionEstudioTab9_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_aprobacion = grdRevisionEstudioTab9.SelectedDataKey.Value.ToString();
            lblRegistroRevisionE.Text = id_aprobacion.ToString();
            GridViewRow row = grdRevisionEstudioTab9.SelectedRow;
            txtDetalleDocumento.Text = ((Label)row.FindControl("lblDetalleDocumento")).Text;
            txtFechaRevisionETab9.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoDocumentoTab9.SelectedValue = ((Label)row.FindControl("lblIdTipoDocumentoAprobacion")).Text;

            LnkBtnRevisionEstudioTab9.Text = ((ImageButton)row.FindControl("imgDocTab9")).ToolTip;
            lblNomUsuarioRevisionEstudio.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            GeneraIcoFile(LnkBtnRevisionEstudioTab9.Text, imgbtnRevisionEstudioTab9);

            //cargaSeguimientoProcesoTab1();
            Up_Tab9.Update();
            Panel_AgregarRevisionEstudioTab9.Visible = true;
            imgbtn_AgregarRevisionEstudioTab9.Visible = false;
            btnModificarRevisionEstudioTab9.Visible = true;
            btnGuardarRevisionEstudioTab9.Visible = false;

        }
        protected void grdRevisionEstudioTab9_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdRevisionEstudioTab9.DataKeys[CurrentRow.RowIndex].Value as object;
                int id_aprobacion = Convert.ToInt32(objTemp.ToString());
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLDecViabilidad.spud_MON_Aprobacion(id_aprobacion, 0, "", DateTime.Now, "", 2, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosRevisionEstudioTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void imgDocTab9_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_aprobacion = Convert.ToInt32(grdRevisionEstudioTab9.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdRevisionEstudioTab9.Rows[row.RowIndex].FindControl("imgDocTab9");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtn_AgregarRevisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarRevisionEstudioTab9.Visible = true;
            imgbtn_AgregarRevisionEstudioTab9.Visible = false;
            lblNomUsuarioRevisionEstudio.Text = "";
            btnGuardarRevisionEstudioTab9.Visible = true;
            btnModificarRevisionEstudioTab9.Visible = false;
            Up_Tab9.Update();
        }
        protected void LnkBtnRevisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkBtnRevisionEstudioTab9.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected Boolean ValidarRevisionEstudioTab9()
        {
            Boolean result;
            result = true;

            if (txtDetalleDocumento.Text == "")
            {
                string script = "<script>alert('Ingrese el detalle del documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                result = false;
                return result;
            }

            if (txtFechaRevisionETab9.Text == "")
            {
                string script = "<script>alert('Ingrese la Fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaRevisionETab9.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if ((DateTime.Now.Date - Convert.ToDateTime(txtFechaRevisionETab9.Text)).Days < 0)
            {
                string script = "<script>alert('La Fecha debe ser menor o igual a la Fecha Actual.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoDocumentoTab9.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione el tipo de documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaArchivo(FileUploadRevisionEstudioTab9))
                return false;

            return result;
        }
        protected void btnGuardarRevisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarRevisionEstudioTab9())
            {
                //if (validaArchivo(FileUploadRevisionEstudioTab9))
                //{
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                string detalleDocumento = txtDetalleDocumento.Text;
                DateTime fecha = VerificaFecha(txtFechaRevisionETab9.Text);
                int id_tipoDocumentoAprobacion = Convert.ToInt32(ddlTipoDocumentoTab9.SelectedValue);
                string urlDoc = _Metodo.uploadfile(FileUploadRevisionEstudioTab9);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLDecViabilidad.spi_MON_Aprobacion(id_proyecto, 3, id_tipoDocumentoAprobacion, detalleDocumento, fecha, urlDoc, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtDetalleDocumento.Text = "";
                    txtFechaRevisionETab9.Text = "";
                    ddlTipoDocumentoTab9.SelectedValue = "";
                    LnkBtnRevisionEstudioTab9.Text = "";

                    Panel_AgregarRevisionEstudioTab9.Visible = false;
                    imgbtn_AgregarRevisionEstudioTab9.Visible = true;
                    CargaDatosRevisionEstudioTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                //}
            }
        }
        protected void btnModificarRevisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarRevisionEstudioTab9())
            {
                int id_aprobacion = Convert.ToInt32(lblRegistroRevisionE.Text);
                string detalleDocumento = txtDetalleDocumento.Text;
                DateTime fecha = VerificaFecha(txtFechaRevisionETab9.Text);
                int id_tipoDocumentoAprobacion = Convert.ToInt32(ddlTipoDocumentoTab9.SelectedValue);
                string urlDoc = "";
                if (FileUploadRevisionEstudioTab9.PostedFile.ContentLength > 0)
                    urlDoc = _Metodo.uploadfile(FileUploadRevisionEstudioTab9);
                else
                    urlDoc = LnkBtnRevisionEstudioTab9.Text;
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int resul = _objBLDecViabilidad.spud_MON_Aprobacion(id_aprobacion, id_tipoDocumentoAprobacion, detalleDocumento, fecha, urlDoc, 1, id_usuario);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosRevisionEstudioTab9();
                    txtDetalleDocumento.Text = "";
                    txtFechaRevisionETab9.Text = "";
                    ddlTipoDocumentoTab9.SelectedValue = "";
                    LnkBtnRevisionEstudioTab9.Text = "";
                    imgbtnRevisionEstudioTab9.Visible = false;
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                Panel_AgregarRevisionEstudioTab9.Visible = false;
                btnModificarRevisionEstudioTab9.Visible = true;
                CargaDatosRevisionEstudioTab9();
                Up_Tab9.Update();
                imgbtn_AgregarRevisionEstudioTab9.Visible = true;
            }
        }
        protected void btnCancelarRevisionEstudioTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarRevisionEstudioTab9.Visible = false;
            imgbtn_AgregarRevisionEstudioTab9.Visible = true;
            Up_Tab9.Update();
        }

        protected void CargaDatosAprobacionETTab9()
        {
            int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdAprobacionETTab9.DataSource = _objBLDecViabilidad.spMON_Aprobacion(id_proyecto, 4);
            grdAprobacionETTab9.DataBind();
        }
        protected void grdAprobacionETTab9_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDoc2Tab9");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        protected void grdAprobacionETTab9_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_aprobacion = grdAprobacionETTab9.SelectedDataKey.Value.ToString();
            lblRegistroAprobacionE.Text = id_aprobacion.ToString();
            GridViewRow row = grdAprobacionETTab9.SelectedRow;
            txtDetalleAprobacionDocumento.Text = ((Label)row.FindControl("lblDetalleDocumento")).Text;
            txtFechaAprobacionETab9.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoDocumento2Tab9.SelectedValue = ((Label)row.FindControl("lblIdTipoDocumentoAprobacion")).Text;

            LnkBtnAprobacionETTab9.Text = ((ImageButton)row.FindControl("imgDoc2Tab9")).ToolTip;
            lblNomUsuarioAprobacionET.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            GeneraIcoFile(LnkBtnAprobacionETTab9.Text, imgbtnAprobacionETTab9);

            //cargaSeguimientoProcesoTab1();
            Up_Tab9.Update();
            Panel_AgregarAprobacionETTab9.Visible = true;
            imgbtn_AgregarAprobacionETTab9.Visible = false;
            btnModificarAprobacionETTab9.Visible = true;
            btnGuardarAprobacionETTab9.Visible = false;

        }
        protected void grdAprobacionETTab9_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAprobacionETTab9.DataKeys[CurrentRow.RowIndex].Value as object;
                int id_aprobacion = Convert.ToInt32(objTemp.ToString());
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLDecViabilidad.spud_MON_Aprobacion(id_aprobacion, 0, "", DateTime.Now, "", 2, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosAprobacionETTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void imgDoc2Tab9_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_aprobacion = Convert.ToInt32(grdAprobacionETTab9.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdAprobacionETTab9.Rows[row.RowIndex].FindControl("imgDoc2Tab9");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtn_AgregarAprobacionETTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAprobacionETTab9.Visible = true;
            imgbtn_AgregarAprobacionETTab9.Visible = false;
            lblNomUsuarioAprobacionET.Text = "";
            btnGuardarAprobacionETTab9.Visible = true;
            btnModificarAprobacionETTab9.Visible = false;
            Up_Tab9.Update();
        }
        protected void LnkBtnAprobacionETTab9_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkBtnAprobacionETTab9.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected Boolean ValidarAprobacionETTab9()
        {
            Boolean result;
            result = true;

            if (txtDetalleAprobacionDocumento.Text == "")
            {
                string script = "<script>alert('Ingrese el detalle del documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                result = false;
                return result;
            }

            if (txtFechaAprobacionETab9.Text == "")
            {
                string script = "<script>alert('Ingrese la Fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaAprobacionETab9.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if ((DateTime.Now.Date - Convert.ToDateTime(txtFechaAprobacionETab9.Text)).Days < 0)
            {
                string script = "<script>alert('La Fecha debe ser menor o igual a la Fecha Actual.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoDocumento2Tab9.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione el tipo de documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (!validaArchivo(FileUploadAprobacionETTab9))
                return false;

            return result;
        }
        protected void btnGuardarAprobacionETTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarAprobacionETTab9())
            {
                //if (validaArchivo(FileUploadAprobacionETTab9))
                //{
                int id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                string detalleDocumento = txtDetalleAprobacionDocumento.Text;
                DateTime fecha = VerificaFecha(txtFechaAprobacionETab9.Text);
                int id_tipoDocumentoAprobacion = Convert.ToInt32(ddlTipoDocumento2Tab9.SelectedValue);
                string urlDoc = _Metodo.uploadfile(FileUploadAprobacionETTab9);
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLDecViabilidad.spi_MON_Aprobacion(id_proyecto, 4, id_tipoDocumentoAprobacion, detalleDocumento, fecha, urlDoc, id_usuario);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtDetalleAprobacionDocumento.Text = "";
                    txtFechaAprobacionETab9.Text = "";
                    ddlTipoDocumento2Tab9.SelectedValue = "";
                    LnkBtnAprobacionETTab9.Text = "";

                    Panel_AgregarAprobacionETTab9.Visible = false;
                    imgbtn_AgregarAprobacionETTab9.Visible = true;
                    CargaDatosAprobacionETTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                //}
            }
        }
        protected void btnModificarAprobacionETTab9_OnClick(object sender, EventArgs e)
        {
            if (ValidarAprobacionETTab9())
            {
                int id_aprobacion = Convert.ToInt32(lblRegistroAprobacionE.Text);
                string detalleDocumento = txtDetalleAprobacionDocumento.Text;
                DateTime fecha = VerificaFecha(txtFechaAprobacionETab9.Text);
                int id_tipoDocumentoAprobacion = Convert.ToInt32(ddlTipoDocumento2Tab9.SelectedValue);
                string urlDoc = "";
                if (FileUploadAprobacionETTab9.PostedFile.ContentLength > 0)
                    urlDoc = _Metodo.uploadfile(FileUploadAprobacionETTab9);
                else
                    urlDoc = LnkBtnAprobacionETTab9.Text;
                int id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int resul = _objBLDecViabilidad.spud_MON_Aprobacion(id_aprobacion, id_tipoDocumentoAprobacion, detalleDocumento, fecha, urlDoc, 1, id_usuario);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosAprobacionETTab9();
                    txtDetalleAprobacionDocumento.Text = "";
                    txtFechaAprobacionETab9.Text = "";
                    ddlTipoDocumento2Tab9.SelectedValue = "";
                    LnkBtnAprobacionETTab9.Text = "";
                    imgbtnAprobacionETTab9.Visible = false;
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                Panel_AgregarAprobacionETTab9.Visible = false;
                btnModificarAprobacionETTab9.Visible = true;
                CargaDatosAprobacionETTab9();
                Up_Tab9.Update();
                imgbtn_AgregarAprobacionETTab9.Visible = true;
            }
        }
        protected void btnCancelarAprobacionETTab9_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAprobacionETTab9.Visible = false;
            imgbtn_AgregarAprobacionETTab9.Visible = true;
            Up_Tab9.Update();
        }
        #endregion


        protected void imgbtnLiqSupervisionTab3_Click(object sender, ImageClickEventArgs e)
        {

        }


        protected void imgDocAvanceTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocAvanceTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnAvanceTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = lnkbtnAvanceTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdAvanceFisicoTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);

                CheckBox chbFlagAyuda = (CheckBox)e.Row.FindControl("chbFlagAyuda");

                if (lblID_ACCESO.Text == "0")
                {
                    chbFlagAyuda.Enabled = false;

                }

                if (chbFlagAyuda.Text == "1")
                {
                    chbFlagAyuda.Checked = true;
                    chbFlagAyuda.Text = "";
                }
                else
                {
                    chbFlagAyuda.Checked = false;
                    chbFlagAyuda.Text = "";
                }
            }
        }

    
        protected void TabContainerDetalles_ActiveTabChanged(object sender, EventArgs e)
        {
            ViewState["ActiveTabIndex"] = TabContainerDetalles.ActiveTabIndex;
        }

        protected void ddlTipoEstadoInformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);

            //Up_EstadoDetalleTab2.Update();

            Up_SubEstadoNombreTab2.Update();
            Up_SubEstadoDetalleTab2.Update();
        }

        protected void CargaTipoSubEstadosDetalle(string TipoEstadoEjecucion)
        {
            ddlTipoSubEstadoInformeTab2.ClearSelection();
            if (!TipoEstadoEjecucion.Equals(""))
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();
                list = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(TipoEstadoEjecucion));
                ddlTipoSubEstadoInformeTab2.DataSource = list;
                ddlTipoSubEstadoInformeTab2.DataTextField = "nombre";
                ddlTipoSubEstadoInformeTab2.DataValueField = "valor";
                ddlTipoSubEstadoInformeTab2.DataBind();

                //ddlTipoSubEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
                if (list.Count == 0)
                {
                    lblNombreSubEstado.Visible = false;
                    ddlTipoSubEstadoInformeTab2.Visible = false;
                }
                else
                {
                    lblNombreSubEstado.Visible = true;
                    ddlTipoSubEstadoInformeTab2.Visible = true;
                }
            }
            ddlTipoSubEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void CargaTipoEstadosDetalle()
        {
            ddlTipoEstadoInformeTab2.ClearSelection();

            ddlTipoEstadoInformeTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(2,0); //EXP. TECNICO
            ddlTipoEstadoInformeTab2.DataTextField = "nombre";
            ddlTipoEstadoInformeTab2.DataValueField = "valor";
            ddlTipoEstadoInformeTab2.DataBind();

            ddlTipoEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        public bool ValidaDecimal(string inValue)
        {
            bool bValid;
            try
            {
                decimal myDT = Convert.ToDecimal(inValue);
                bValid = true;

            }
            catch (Exception e)
            {
                bValid = false;
            }


            return bValid;
        }

        protected void imgbtnEditarUE_Click(object sender, ImageClickEventArgs e)
        {
            string script = "<script>$('#modalEditarUE').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void btnModificarUE_Click(object sender, EventArgs e)
        {
            ObjBEProyecto.id = Convert.ToInt32(LblID_PROYECTO.Text);
            if (LblID_SOLICITUD.Text == "")
            {
                ObjBEProyecto.Id_Solicitudes = 0;
            }
            else
            {
                ObjBEProyecto.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
            }
            ObjBEProyecto.Entidad_ejec = txtUnidadEjecutora.Text;
            int val = objBLProyecto.U_MON_UnidadEjecutora(ObjBEProyecto);

            if (val == 1)
            {
                string script = "<script>$('#modalEditarUE').modal('hide');alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                }
                UPTabContainerDetalles.Update();



            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void grdUbicacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdUbicacion.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.flagActivo = 0;
                _BEBandeja.CCPP = ((Label)CurrentRow.FindControl("lblUbigeo")).Text + ((Label)CurrentRow.FindControl("lblUbigeoCCPP")).Text;
                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarUbicacion_Click(object sender, ImageClickEventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = true;
            //btnModificarUbicacionMetasTab0.Visible = false;
            Up_UbigeoTab0.Update();
        }

        protected Boolean ValidarUbigeoTab0()
        {
            Boolean result;
            result = true;

            if (ddlDepartamentoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Departamento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlProvinciaTab0.SelectedValue == "")
            {

                string script = "<script>alert('Ingrese Provincia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;


            }

            if (ddlDistritoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Distrito.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;
        }

        protected void btnGuardarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            if (ValidarUbigeoTab0())
            {
                _BEBandeja.flagActivo = 1;
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                if (ddlCCPPTab0.SelectedValue.Equals(""))
                {
                    _BEBandeja.CCPP = ddlDepartamentoTab0.SelectedValue + ddlProvinciaTab0.SelectedValue + ddlDistritoTab0.SelectedValue;
                }
                else
                {
                    _BEBandeja.CCPP = ddlCCPPTab0.SelectedValue;
                }

                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void btnCancelarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = false;
        }

        protected void ddlDepartamentoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlProvinciaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlDistritoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = _objBLBandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("- SELECCIONAR -", ""));

        }

        protected void CargaUbigeosProyectos(int idProyecto)
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = _objBLBandeja.F_spMON_Ubigeo(idProyecto);
            grdUbicacion.DataSource = ListUbigeos;
            grdUbicacion.DataBind();



        }

        protected void imgbtnEditarUbigeo_Click(object sender, ImageClickEventArgs e)
        {
            CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));

            cargaUbigeo(ddlDepartamentoTab0, "1", null, null, null);
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);

            Up_UbigeoTab0.Update();
            //string script = "<script>ShowPopupRegistroInformeVisita();</script>";
            string script = "<script>$('#modalUbigeo').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void BtnRefrescar_Click(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);
            DataTable dt = (new BLProyecto().spu_MON_UltimaActualizacion(idProy));
            if (dt.Rows.Count > 0)
            {
                LblUltimaModificacion.Text = Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortTimeString();
                LblUsuario.Text = dt.Rows[0]["Usuario"].ToString();
            }
            else
            {
                LblUltimaModificacion.Text = "";
                LblUsuario.Text = "";
            }
        }

        protected void btnFinalizarTab3_Click(object sender, EventArgs e)
        {
            if (lblIdEstadoRegistro.Text.Equals("2")) //ACTIVO
            {
                _BEEjecucion.id_estadoProyecto = 3;
            }
            else
            {
                _BEEjecucion.id_estadoProyecto = 2;
            }
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_EstadoProyecto(_BEEjecucion);

            if (val == 1)
            {
                //CargaLiquidacion();

                string script = "<script>alert('Se Actualizó Correctamente'); window.location.reload()</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void lnkbtnBuscarRUCContratistaTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsultorTab1, txtNombConsultorTab1, txtRepresentanteLegalConsultorTab1, divMsjBusquedaRucContratistaTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }


        protected void lnkbtnBuscarRUCConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsorcioTab1, txtNomconsorcioTab1, txtRepresentanteTab1, divMsjBusquedaRucConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void lnkbtnBuscarRUCMiembroConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucGrupTab1, txtContratistaGrupTab1, txtRepresentanGrupTAb1, divMsjBusquedaRucMiembroConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void btnVigenciaVencida_Click(object sender, EventArgs e)
        {
            string script = "<script>$('#modalVigencia').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            dt = _objBLFinanciamiento.spMON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text));

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["flagActivoReporte"].ToString() == "1")
                {
                    btnRegistrarVencimiento.Enabled = true;
                }

                if (dt.Rows[0]["fechaUpdateVencimiento"].ToString().Length > 0)
                {
                    ddlFlagVigencia.SelectedValue = dt.Rows[0]["flagVigenciaVencida"].ToString();
                    ddlTipoAccionVigencia.SelectedValue = dt.Rows[0]["idTipoAccionVencimiento"].ToString();

                    if (ddlFlagVigencia.SelectedValue.Equals("1"))
                    {
                        divAccionVigencia.Visible = true;
                    }
                    else
                    {
                        divAccionVigencia.Visible = false;
                        ddlTipoAccionVigencia.SelectedValue = "";
                    }

                    if (ddlTipoAccionVigencia.SelectedValue == "")
                    {
                        divAccionVigencia.Visible = false;
                    }

                    lblNomActualizaVencimiento.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdateVencimiento"].ToString();
                }
            }

            Up_PanelVigencia.Update();
        }

        protected String ValidarVigenciaVencia()
        {
            string msj = "";


            if (ddlFlagVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar etapa con vigencia vencida.\\n";
            }

            if (ddlFlagVigencia.SelectedValue.Equals("1") && ddlTipoAccionVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de acción.\\n";
            }


            return msj;

        }
        protected void btnRegistrarVencimiento_Click(object sender, EventArgs e)
        {
            string msjVal = ValidarVigenciaVencia();

            if (msjVal.Length > 0)
            {
                string script = "<script>alert('" + msjVal + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                int pTipoAccion = 0;
                if (ddlTipoAccionVigencia.SelectedValue != "")
                {
                    pTipoAccion = Convert.ToInt32(ddlTipoAccionVigencia.SelectedValue);
                }

                DataSet ds = new DataSet();
                ds = _objBLFinanciamiento.paSSP_spiu_MON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(LblID_USUARIO.Text), Convert.ToInt32(ddlFlagVigencia.SelectedValue), pTipoAccion);

                if (ds.Tables[0].Rows[0]["vCod"].ToString() == "200")
                {
                    string script = "<script>$('#modalVigencia').modal('hide');alert('Se registró la vigencia de vencimiento. Se actualizará la página');window.location.reload()</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Hubo un error, volver a intentar.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void ddlFlagVigencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFlagVigencia.SelectedValue.Equals("1"))
            {
                divAccionVigencia.Visible = true;
            }
            else
            {
                divAccionVigencia.Visible = false;
                ddlTipoAccionVigencia.SelectedValue = "";
            }

            Up_PanelVigencia.Update();
        }

        protected void TabActivoProceso()
        {
            if (grSeguimientoSeace.Rows.Count > 0)
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }
            else if (grdSeguimientoProcesoTab1.Rows.Count > 0 || txtValorReferencialTab1.Text.Length > 0)
            {
                PanelSeaceAuto.Visible = false;
                PanelSeaceManual.Visible = true;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";
            }
            else
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }

            //Por ahora se desactiva todo 2020.11.13
            lnkbtnSeaceAutoPanel.Visible = false;
            lnkbtnSeaceManualPanel.Visible = false;
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
        }

        protected void grdSEACEObra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSEACEObra.PageIndex = e.NewPageIndex;
            CargaProcesosRelSEACE();
        }

        protected void CargaProcesosRelSEACE()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSEACEObra.DataSource = _objBLFinanciamiento.spMON_SEACE_Obra(snip, Convert.ToInt32(LblID_PROYECTO.Text));
            grdSEACEObra.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void CargaCodigoSeace()
        {
            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            grSeguimientoSeace.DataSource = _objBLFinanciamiento.spMON_CodigoSeace(pIdProyecto, Convert.ToInt32(ddlProcesoTab1.SelectedValue));
            grSeguimientoSeace.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void BtnRegistrarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            if (TxtCodigoSeace.Text.Length == 0)
            {
                string script = "<script>alert('Inresar código de SEACE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script3", script, false);
            }
            else
            {
                CodigosSeace _BE = new CodigosSeace();
                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(TxtCodigoSeace.Text);
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se registro correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnCancelarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            Panel_RegistroSeace.Visible = false;
            imgbtnAgregarSeguimientoSeace.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtnAgregarSeguimientoSeace_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RegistroSeace.Visible = true;
            imgbtnAgregarSeguimientoSeace.Visible = false;
            Up_Tab1.Update();
        }

        protected void grSeguimientoSeace_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grSeguimientoSeace.DataKeys[CurrentRow.RowIndex].Value as object;

                CodigosSeace _BE = new CodigosSeace();

                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(objTemp.ToString());
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grSeguimientoSeace_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblEstadoItem = (Label)e.Row.FindControl("lblEstadoItem");
                Label lblEstado = (Label)e.Row.FindControl("lblEstado");
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");

                string caseSwitch = lblEstadoItem.Text;

                lblEstado.Text = lblEstadoItem.Text;
                lblEstado.ToolTip = lblEstadoItem.Text;

                switch (caseSwitch)
                {
                    case "APELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO POR RECORTE PRESUPUESTAL":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "DESIERTO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUBSCRIPCION DEL CONTRATO POR DECISION DE LA ENTIDAD":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUSCRIBIO EL CONTRATO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NULO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "SUSPENDIDO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "ADJUDICADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "ADJUDICADO A PRORRATA":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONSENTIDO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONTRATADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    default:
                        lblEstado.CssClass = "label label-primary";
                        break;
                }

                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");

                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;

                }
            }
        }

        protected void grdSEACEObra_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");
                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");
                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;
                }
            }
        }
        protected void lnkbtnSeaceAutoPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = true;
            PanelSeaceManual.Visible = false;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";

            Up_Tab1.Update();
        }

        protected void lnkbtnSeaceManualPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";

            Up_Tab1.Update();
        }
    }

}