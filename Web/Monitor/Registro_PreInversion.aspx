﻿<%@ Page Title="PreInversion" Language="C#" MasterPageFile="~/MasterPages/Registro.Master" AutoEventWireup="true" CodeBehind="Registro_PreInversion.aspx.cs" Inherits="Web.Monitor.Registro_PreInversion" Culture="es-PE" UICulture="es-PE" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ddlEstado {
            border: 0px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        .CajaDialogo {
            background-color: #CEE3F6;
            border-width: 4px;
            padding: 0px;
            width: 200px;
            font-weight: bold;
        }

            .CajaDialogo div {
                margin: 5px;
                text-align: center;
            }

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            font-family: Calibri;
        }

        input[type=text] {
            /*	background-image: url(../img/pdf.gif); 
	background-repeat: repeat-x;*/
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-family: Tahoma;
            font-size: 11px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }


        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            margin-top: -50px;
            margin-left: 100px;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }

        .Div_Fondo {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: fixed;
            z-index: 1004;
            top: 0;
            left: 0;
            right: 0;
            bottom: -1px;
            height: auto;
        }

        .tablaPmiTab0 tr td:nth-child(n+2) {
            width: 50px;
            vertical-align: middle;
            text-align: center;
        }

        .tablaPmiTab0 input[type="radio"] {
            width: 20px;
            height: 20px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.gif" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; font-weight: bold">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:UpdatePanel ID="UPTabContainerDetalles" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table class="tablaRegistroTitulo">
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblNombProy" runat="server" Text="" Font-Size="10" CssClass="titulo" Style="font-size: 14pt;"></asp:Label>
                                        <asp:Button ID="DoNothing" runat="server" Enabled="false" Style="display: none;" />
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblUBICACION" runat="server" Text="" CssClass="titulo2"></asp:Label>
                                        <asp:ImageButton ID="imgbtnEditarUbigeo" runat="server" ImageUrl="../img/edit_20x20.png" OnClick="imgbtnEditarUbigeo_Click" ToolTip="Editar Ubigeo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="Label1" runat="server" Text="SNIP : " CssClass="titulo2"></asp:Label>
                                        <asp:Label ID="lblSNIP" runat="server" Text="" CssClass="titulo2"></asp:Label>
                                        <asp:Label ID="lblUE" runat="server" CssClass="titulo2" Text=""></asp:Label>
                                        <asp:ImageButton ID="imgbtnEditarUE" runat="server" ImageUrl="../img/edit_20x20.png" OnClick="imgbtnEditarUE_Click" ToolTip="Editar Unidad Ejecutora" />
                                        <asp:Label ID="LblID_PROYECTO" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="LblID_SOLICITUD" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="LblID_USUARIO" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblID_PERFIL_USUARIO" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblCOD_SUBSECTOR" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblTipoFinanciamiento" runat="server" Text="ESTUDIO PREINVERSIÓN" CssClass="tituloTipo"
                                            Style="font-size: 13pt;"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:UpdatePanel runat="server" ID="Up_EstadoSituacional" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table cellpadding="2" cellspacing="2">
                                                    <tr>
                                                        <td align="right"><b>ESTADO :</b>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblNombreEstadoGeneral" runat="server" Text=""></asp:Label>
                                                            <asp:Label ID="lblIdEstado" runat="server" Text="" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblIdSubEstado" runat="server" Text="" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblIdSubEstado2" runat="server" Text="" Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblFechaEstado" runat="server" Text="" Font-Size="10px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 1px"></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 5px">
                            <div id="cssmenu" style="display: none;">
                                <h4>ETAPAS 
                            <button id="btnNuevoProy" disabled="disabled" runat="server" title="Agregar Saldo" class="btn btn-primary pull-right" data-toggle="modal" data-target="#newProjModal">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                                    <%--<span class="glyphicon glyphicon-info-sign" data-toggle="popover" data-trigger="hover" data-content="Para poder agregar una etapa, procure antes seleccionar la última etapa que haya en esta columna" aria-hidden="true"></span>--%>
                                    <span class="glyphicon glyphicon-info-sign" data-toggle="popover" data-trigger="hover" data-content="Para poder agregar un saldo, la última etapa debe encontrarse en estado <b>CONCLUIDO-LIQUIDADA</b>" data-html="true" aria-hidden="true"></span>
                                </h4>
                                <ul style="padding-left: 0;" id="menuObra" runat="server">
                                </ul>
                            </div>
                            <asp:TabContainer runat="server" Visible="true" ID="TabContainerDetalles" ActiveTabIndex="0" Width="1310px" CssClass="ajax__tab_blueGrad-theme pull-right" AutoPostBack="true" OnActiveTabChanged="TabContainerDetalles_ActiveTabChanged">
                                <asp:TabPanel runat="server" ID="TabPanelFinanciamiento" HeaderText="I. FINANCIAMIENTO" TabIndex="0">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="LblCabeceraTabPanel0" runat="server" Text="I. FINANCIAMIENTO"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab0" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="0" cellpadding="0" width="1100px">
                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" colspan="4" align="left" style="padding-left: 30px">I. FINANCIAMIENTO
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>1. CONVENIOS/ADENDAS </b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img src="linea.png" width="98%" height="10px" alt="" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table style="margin: auto;">
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:GridView runat="server" ID="grdConvenioDirectaTab0" EmptyDataText="No hay registros en Convenios/Adendas"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                DataKeyNames="id_convenioDirecta"
                                                                                                OnRowDataBound="grdConvenioDirectaTab0_OnRowDataBound"
                                                                                                OnSelectedIndexChanged="grdConvenioDirectaTab0_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdConvenioDirectaTab0_OnRowCommand"
                                                                                                CellPadding="2" CellSpacing="2" Width="1020px">
                                                                                                <Columns>

                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:TemplateField HeaderText="N°" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblNro" Text='<%# Eval("nro") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Height="30px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblid_convenioDirecta" Text='<%# Eval("id_convenioDirecta") %>' runat="server"></asp:Label>
                                                                                                            <asp:Label ID="lbliTipoConvenio" Text='<%# Eval("iTipoConvenio") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>


                                                                                                    <asp:BoundField DataField="tipo" HeaderText="Tipo" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="detalle" HeaderText="Detalle Convenio Y/O Adenda" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="vFechaConvenio" HeaderText="Fecha Convenio" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="entidadFirmante" HeaderText="Entidad Firmante" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="objetoConvenio" HeaderText="Objeto del Convenio" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="vFechaVencimiento" HeaderText="Fecha Vencimiento del Convenio" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar el registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td align="right" valign="top" style="width: 26px">
                                                                                            <asp:ImageButton ID="imgbtn_agregarConvenioDirecta" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Convenio o Adenda"
                                                                                                OnClick="imgbtn_agregarConvenioDirecta_OnClick" />
                                                                                        </td>
                                                                                    </tr>

                                                                                    <%--  <tr>
                                                                                        <td align="left" style="padding-left: 80px;">
                                                                                            <asp:Label ID="Label2" runat="server" Text="0" Visible="false"></asp:Label>
                                                                                            <asp:Label ID="Label5" runat="server" Text="Hay mayor financiamiento." ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                                                                                        </td>
                                                                                    </tr>--%>


                                                                                    <tr>
                                                                                        <td align="center" colspan="2">
                                                                                            <asp:Panel ID="Panel_AgregarConvenioDirecta" Visible="false" runat="server" Width="92%">
                                                                                                <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                    <table width="95%" class="tablaRegistro">
                                                                                                        <tr>
                                                                                                            <td colspan="8" align="center">
                                                                                                                <b>REGISTRO DE CONVENIOS/ADENDAS</b><asp:Label ID="lblRegistroConvenioDirecta" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 6px"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Tipo :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:DropDownList ID="ddlTipoConvenioTab0" runat="server">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td align="right">Entidad Firmante:
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtEntidadConvenioTab0" runat="server" Width="200px" Text=""></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right" style="width: 100px">Detalle :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtDetalleConvenioTab0" runat="server" Width="200px" Text=""> </asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right">Objeto del Convenio :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtObjetoConvenioTab0" runat="server" Width="200px" Text=""></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha Convenio :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaConvenioTab0" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFechaConvenioTab0" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaConvenioTab0" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Vencimiento del Convenio :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaVenceConvenioTab0" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtFechaVenceConvenioTab0" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechaVenceConvenioTab0" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="6" align="left" style="padding-left: 50px;">
                                                                                                                <asp:Label ID="lblNomUsuarioConvenioDirecta" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="8">
                                                                                                                <asp:Button ID="btnGuardarRegistroConvenioDirectaTab0" runat="server" Text="Guardar" OnClick="btnGuardarRegistroConvenioDirectaTab0_OnClick" />
                                                                                                                <asp:Button ID="btnActualizarRegistroConvenioDirectaTab0" runat="server" Text="Modificar" Visible="false" OnClick="btnActualizarRegistroConvenioDirectaTab0_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarRegistroConvenioDirectaTab0" runat="server" Text="Cancelar" OnClick="btnCancelarRegistroConvenioDirectaTab0_OnClick" />
                                                                                                            </td>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>2.- INFORMACION FINANCIERA </b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img src="linea.png" width="98%" height="10px" alt="" />
                                                                            </td>
                                                                        </tr>
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <tr>
                                                                                    <td valign="top" align="center" style="width: 50%">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td width="100px"></td>
                                                                                                <td align="left"><strong>A)</strong> Costo de Elaboración de Estudio (S/.):
                                                                                                </td>
                                                                                                <td align="left">
                                                                                                    <asp:TextBox ID="txtCostoEEstudioTab0" runat="server" Width="100px" Text="0"></asp:TextBox>
                                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" TargetControlID="txtCostoEEstudioTab0" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="100px"></td>
                                                                                                <td align="left"><strong>B)</strong> Costo de Supervisión del Estudio (S/.):</td>
                                                                                                <td align="left">
                                                                                                    <asp:TextBox ID="txtCostoSEstudioTab0" runat="server" Width="100px" Text="0"></asp:TextBox>
                                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server" TargetControlID="txtCostoSEstudioTab0" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="100px"></td>
                                                                                                <td align="left"><strong>C)</strong> Costo total del Estudio(A + B)  (S/.):</td>
                                                                                                <td align="left">
                                                                                                    <asp:TextBox ID="txtCostoTEstudioTab0" runat="server" Width="100px" Text="0" Enabled="false"></asp:TextBox>
                                                                                                    <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server" TargetControlID="txtCostoTEstudioTab0" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />--%>		
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top" align="center" style="width: 50%">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td align="left"><strong>D)</strong> Adjuntar Desagregado de costos del Estudio.</td>
                                                                                                <td align="left">
                                                                                                    <asp:FileUpload ID="FileUploadCostoETab0" runat="server" />
                                                                                                    <asp:ImageButton ID="imgBtnCostoETab0" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnDocTab0_Click" />
                                                                                                    <asp:LinkButton ID="lnkBtnCostoETab0" runat="server" OnClick="lnkbtnDocTab0_Click"></asp:LinkButton>
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left"><strong>E)</strong> Adjuntar Desagregado de costo de Supervisión.</td>
                                                                                                <td align="left">
                                                                                                    <asp:FileUpload ID="FileUploadCostoSTab0" runat="server" />
                                                                                                    <asp:ImageButton ID="imgBtnCostoSTab0" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnDocTab0_Click" />
                                                                                                    <asp:LinkButton ID="lnkBtnCostoSTab0" runat="server" OnClick="lnkbtnDocTab0_Click"></asp:LinkButton>
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left"><strong>F)</strong> TDR/Plan de Trabajo Elab. Estudio.</td>
                                                                                                <td align="left">
                                                                                                    <asp:FileUpload ID="FileUploadPlanETab0" runat="server" />
                                                                                                    <asp:ImageButton ID="imgBtnPlanEEstudioTab0" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnDocTab0_Click" />
                                                                                                    <asp:LinkButton ID="lnkBtnPlanETab0" runat="server" OnClick="lnkbtnDocTab0_Click"></asp:LinkButton>
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left"><strong>G)</strong> TDR/Plan de Trabajo Sup. Estudio.</td>
                                                                                                <td align="left">
                                                                                                    <asp:FileUpload ID="FileUploadPlanSEstudioTab0" runat="server" />
                                                                                                    <asp:ImageButton ID="imgBtnPlanSEstudioTab0" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnDocTab0_Click" />
                                                                                                    <asp:LinkButton ID="lnkBtnPlanSEstudioTab0" runat="server" OnClick="lnkbtnDocTab0_Click"></asp:LinkButton>
                                                                                                </td>
                                                                                                <td width="50px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 10px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" align="left">
                                                                                                    <asp:Label ID="Label7" runat="server" Font-Bold="True" ForeColor="Red" Text="* Máximo 10 MB por Archivo"></asp:Label></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="left" style="padding-left: 50px;">
                                                                                        <asp:Label ID="lblNomUsuarioInfoFinanciera" runat="server" Font-Size="10px"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" colspan="4">
                                                                                        <asp:Button ID="btnGuardarInfFinancieraTab0" runat="server" Text="Guardar" OnClick="btnGuardarInfFinancieraTab0_OnClick" />
                                                                                    </td>
                                                                                </tr>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <%--<asp:PostBackTrigger ControlID="FileUploadCostoETab0"/>--%>
                                                                                <asp:PostBackTrigger ControlID="imgBtnCostoETab0" />
                                                                                <asp:PostBackTrigger ControlID="lnkBtnCostoETab0" />

                                                                                <asp:PostBackTrigger ControlID="imgBtnCostoSTab0" />
                                                                                <asp:PostBackTrigger ControlID="lnkBtnPlanETab0" />

                                                                                <asp:PostBackTrigger ControlID="imgBtnPlanEEstudioTab0" />
                                                                                <asp:PostBackTrigger ControlID="lnkBtnPlanETab0" />

                                                                                <asp:PostBackTrigger ControlID="imgBtnPlanSEstudioTab0" />
                                                                                <asp:PostBackTrigger ControlID="lnkBtnPlanSEstudioTab0" />

                                                                                <asp:PostBackTrigger ControlID="btnGuardarInfFinancieraTab0" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>

                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                       
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>4. PROGRAMACIÓN PRESUPUESTARIA</b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img src="linea.png" width="98%" height="10px" alt="" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table style="margin: auto;">
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:GridView runat="server" ID="grdSosemTab0" EmptyDataText="No hay registro en Programación Presupuestaria"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                DataKeyNames="id_seguimiento_sosem" CellPadding="2" CellSpacing="2" Width="1020px"
                                                                                                OnRowDataBound="grdSosemTab0_OnRowDataBound"
                                                                                                OnSelectedIndexChanged="grdSosemTab0_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdSosemTab0_OnRowCommand">
                                                                                                <Columns>

                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:TemplateField HeaderText="Año" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblEntidad" Text='<%# Eval("anio") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Height="30px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAvanceF" Text='<%# Eval("id_seguimiento_sosem") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>


                                                                                                    <asp:BoundField DataField="pia" HeaderText="Pia" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="pimAcumulado" HeaderText="Pim. Acumulado" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="devAcumulado" HeaderText="Dev. Acum." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="ene" HeaderText="Ene" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="feb" HeaderText="FEB" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="mar" HeaderText="MAR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="abr" HeaderText="ABR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="may" HeaderText="MAY" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="jun" HeaderText="JUN" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="jul" HeaderText="JUL" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="ago" HeaderText="AGO" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="sep" HeaderText="SET" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="oct" HeaderText="OCT" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="nov" HeaderText="NOV" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="dic" HeaderText="DIC" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="compromisoAnual" HeaderText="Compromiso Anual" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("strFechaUpdate") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td align="right" valign="top" style="width: 26px">
                                                                                            <asp:ImageButton ID="imgbtn_agregarSOSEM" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Avance de Info Obras"
                                                                                                OnClick="imgbtn_agregarSOSEM_OnClick" />
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td align="left" style="padding-left: 80px;">
                                                                                            <asp:Label ID="lblValorAvance" runat="server" Text="0" Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lblAlertaSosemTab0" runat="server" Text="Hay mayor financiamiento." ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td align="center" colspan="2">
                                                                                            <asp:Panel ID="Panel_AgregarSOSEM" Visible="false" runat="server" Width="92%">
                                                                                                <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                    <table width="95%" class="tablaRegistro">
                                                                                                        <tr>
                                                                                                            <td colspan="8" align="center">
                                                                                                                <b>PROGRAMACIÓN PRESUPUESTARIA</b><asp:Label ID="lblRegistroObras" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 6px"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Año :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:DropDownList ID="ddlanioSOSEMTAb0" runat="server">
                                                                                                                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2021">2021</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2013">2013</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2012">2012</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2011">2011</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2010">2010</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2009">2009</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2008">2008</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2007">2007</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2006">2006</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td align="right">Pia (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtPiaSOSEMTab0" runat="server" Width="60px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPiaSOSEMTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Pim. Acum. (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtPimAcumTab0" runat="server" Width="80px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtPimAcumTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right" style="width: 100px">Dev. Acum. (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtDevAcumTab0" runat="server" Width="80px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtDevAcumTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right" style="width: 100px">Enero (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtEneSosTab0" runat="server" Width="50px" Text="0"> </asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtEneSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Febrero (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFebSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtFebSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Marzo (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtMarSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtMarSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Abril (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtAbrSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtAbrSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Mayo (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtMaySosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtMaySosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Junio (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtJunSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtJunSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Julio (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtJulSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtJulSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Agosto (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtAgoSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtAgoSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Septiembre (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtSepSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtSepSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Octubre (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtOctSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtOctSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Noviembre (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtNovSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtNovSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Diciembre (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtDicSosTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtDicSosTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Compromiso Anual (S/.):
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtComproAnualTab0" runat="server" Width="50px" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtComproAnualTab0"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td colspan="6" align="left" style="padding-left: 50px;">
                                                                                                                <asp:Label ID="lblNomUsuarioSosem" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="8">
                                                                                                                <asp:Button ID="btnGuardarRegistroSOSEMTab0" runat="server" Text="Guardar" OnClick="btnGuardarRegistroSOSEMTab0_OnClick" />
                                                                                                                <asp:Button ID="btnActualizarRegistroSOSEMTab0" runat="server" Text="Modificar" Visible="false" OnClick="btnActualizarRegistroSOSEMTab0_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarRegistroSOSEM" runat="server" OnClick="btnCancelarRegistroSOSEM_OnClick"
                                                                                                                    Text="Cancelar" />
                                                                                                            </td>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>5. SEGUIMIENTO AL AVANCE FINANCIERO</b>
                                                                                <asp:Label ID="Label20" runat="server" Font-Size="10px" Text="(Fuente: SSI)"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img src="linea.png" width="98%" height="10px" alt="" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center" style="height: 10px">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <iframe runat="server" id="ifrGraficoSosem" class="iframeSOSEM"></iframe>

                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:GridView runat="server" ID="grdSOSEMEjecutoras" EmptyDataText="No hay información financiera para mostrar."
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                DataKeyNames="idEjecutora" CellPadding="2" CellSpacing="2" Width="500px">
                                                                                                <Columns>

                                                                                                    <asp:TemplateField HeaderText="EJECUTORA" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblEjecutora" Text='<%# Eval("nombreEjecutora") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Height="30px" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="..." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="btnDetalleDevengadoTab0" runat="server" ImageUrl="~/img/btn_info_finan.png" ToolTip="Ver información mensualizada." OnClick="btnDetalleDevengadoTab0_Click" />
                                                                                                            <asp:ImageButton ID="btnFuenteTab0" runat="server" ImageUrl="~/img/btn_fuente.png" ToolTip="Ver información por fuente de financiamiento." OnClick="btnFuenteTab0_Click" />

                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:BoundField DataField="pimAcumulado" HeaderText="Pim. Acumulado" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="devAcumulado" HeaderText="Dev. Acum." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>



                                                                                                    <asp:TemplateField Visible="true" HeaderText="Actualizado al">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("strFechaUpdate") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                       
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelProcesoSeleccion" HeaderText="II. PROCESO DE SELECCIÓN"
                                    TabIndex="1">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="LblCabeceraTabPanel1" runat="server" Text="II. PROCESO DE SELECCIÓN"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2" width="1100px">
                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal" style="background-color: #ecf0f5">

                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" colspan="2" align="left" style="padding-left: 30px">II. PROCESO DE SELECCIÓN 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div class="box">
                                                                                    <table>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="right" style="padding-left: 80px">PROCESO DE SELECCIÓN : 
                                                                                                </td>
                                                                                                <td align="left">
                                                                                                    <asp:DropDownList ID="ddlProcesoTab1" runat="server" Width="130px" AutoPostBack="true"
                                                                                                        OnSelectedIndexChanged="ddlProcesoTab1_OnSelectedIndexChanged">
                                                                                                        <asp:ListItem Value="1" Selected="True">PREINVERSION</asp:ListItem>
                                                                                                        <asp:ListItem Value="2">SUPERVISIÓN</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="right" style="padding-left: 80px">MODALIDAD EJECUCIÓN :
                                                                                                </td>
                                                                                                <td align="left">
                                                                                                    <asp:DropDownList ID="ddlModalidadTab1" runat="server" Width="130px">
                                                                                                        <asp:ListItem Value="">-SELECCIONE-</asp:ListItem>
                                                                                                        <asp:ListItem Value="1">DIRECTA</asp:ListItem>
                                                                                                        <asp:ListItem Value="2">INDIRECTA</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="box" id="bxProceoSeleccion">
                                                                                    <asp:LinkButton runat="server" ID="lnkbtnSeaceAutoPanel" OnClick="lnkbtnSeaceAutoPanel_Click"></asp:LinkButton>
                                                                                    <asp:LinkButton runat="server" ID="lnkbtnSeaceManualPanel" OnClick="lnkbtnSeaceManualPanel_Click"></asp:LinkButton>
                                                                                    <asp:Panel runat="server" ID="PanelSeaceAuto">
                                                                                        <br />
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td align="left" style="padding-left: 80px">
                                                                                                    <b>
                                                                                                        <asp:Label runat="server" Text="SEGUIMIENTO (Obligatorio)" ID="lblTitSeace"></asp:Label>
                                                                                                    </b>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <img alt="" src="linea.png" width="98%" height="10px" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="center">
                                                                                                    <table style="border-collapse: separate !important; border-spacing: 6px;">
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:GridView runat="server" ID="grSeguimientoSeace" EmptyDataText="No hay registro."
                                                                                                                    ShowHeaderWhenEmpty="True"
                                                                                                                    AutoGenerateColumns="False" OnRowCommand="grSeguimientoSeace_RowCommand"
                                                                                                                    OnRowDataBound="grSeguimientoSeace_RowDataBound"
                                                                                                                    CellPadding="2" CellSpacing="2"
                                                                                                                    Width="900px" DataKeyNames="CodigoConvocatoria">
                                                                                                                    <Columns>
                                                                                                                        <asp:BoundField Visible="true" DataField="CodigoConvocatoria" HeaderText="Identificador Convocatoria">
                                                                                                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                                                                        </asp:BoundField>

                                                                                                                        <asp:TemplateField HeaderText="Objeto Contratación" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblObjetoContratacion" Text='<%# Eval("OBJETOCONTRACTUAL") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                                <asp:Label ID="lblMsjNroItem" runat="server" Text="" Visible="false"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Tipo de Selección" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblTipoSeleccion" Text='<%# Eval("TipoSeleccion") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Nomenclatura" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblNOMENCLATURA" Text='<%# Eval("NOMENCLATURA") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Fecha Convocatoria" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblFechaPublicacion" Text='<%# Eval("FechaConvocatoria") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Cronograma" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <a class="enlace" target="_blank" onclick="<%# "loadModalIframe('CalendarioSeace',"+Eval("CodigoConvocatoria")+")" %>" title="Ver Cromograma de Convocatoria">
                                                                                                                                    <img src="../img/newFecha.png" alt="Ver Detalle" onmouseover="this.src='../img/newFecha2.png';" onmouseout="this.src='../img/newFecha.png';" />
                                                                                                                                </a>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Estado" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblEstadoItem" Text='<%# Eval("EstadoItem") %>' runat="server" Visible="false"></asp:Label>

                                                                                                                                <asp:Label ID="lblEstado" Text="" runat="server" data-toggle="tooltip" data-placement="right"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:BoundField Visible="true" DataField="ValorReferencial" DataFormatString="{0:N2}" HeaderText="Valor Referencial"></asp:BoundField>

                                                                                                                        <asp:TemplateField HeaderText="Detalle" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <a class="enlace" target="_blank" onclick="<%# "loadModalIframe('FichaDetalleSeace',"+Eval("CodigoConvocatoria")+")" %>"
                                                                                                                                    title="Ver Detalle">
                                                                                                                                    <img src="../img/helpDocument.png" alt="Ver Detalle" onmouseover="this.src='../img/helpDoc2.png';" onmouseout="this.src='../img/helpDocument.png';" />
                                                                                                                                </a>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Elim.">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="../img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                                                <asp:Label ID="lblNroItem" runat="server" Text='<%# Eval("NroItem") %>' Visible="false"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle Width="24px" />
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                    <HeaderStyle CssClass="GridHeader2" Height="30px" />
                                                                                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                                                                                    <RowStyle HorizontalAlign="Center" />
                                                                                                                </asp:GridView>
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <asp:ImageButton ID="imgbtnAgregarSeguimientoSeace" runat="server" ImageUrl="~/img/add.png"
                                                                                                                    onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                                    Height="30px" AlternateText="Agregar" ToolTip="Agregar registro." OnClick="imgbtnAgregarSeguimientoSeace_Click" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="center">
                                                                                                                <asp:Panel ID="Panel_RegistroSeace" Visible="false" runat="server" Width="100%">
                                                                                                                    <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                                        <table class="tablaRegistro" cellpadding="2" width="94%">
                                                                                                                            <tr>
                                                                                                                                <td align="center" colspan="2">
                                                                                                                                    <b>REGISTRO DE CÓDIGOS SEACE </b>
                                                                                                                                    <asp:Label ID="Label17" runat="server" Visible="false"></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="height: 10px"></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td align="right">Identificador convocatoria:</td>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox runat="server" ID="TxtCodigoSeace"></asp:TextBox>
                                                                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server" TargetControlID="TxtCodigoSeace"
                                                                                                                                        FilterType="Numbers" Enabled="True" />
                                                                                                                                </td>
                                                                                                                            </tr>

                                                                                                                            <tr>
                                                                                                                                <td colspan="2" align="center">
                                                                                                                                    <p class="bg-info" style="padding: 10px; margin: 10px">
                                                                                                                                        La información complementaria de SEACE será registrado de forma automática.
                                                                                                                                    </p>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td colspan="2" align="center">
                                                                                                                                    <asp:Button ID="BtnRegistrarSeguimientoSeace" runat="server" Text="Guardar" OnClick="BtnRegistrarSeguimientoSeace_Click" />
                                                                                                                                    <asp:Button ID="btnCancelarSeguimientoSeace" runat="server" Text="Cancelar" OnClick="btnCancelarSeguimientoSeace_Click" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </asp:Panel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <br />
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="left" style="padding-left: 80px">
                                                                                                    <b>POSIBLES PROCESOS RELACIONADOS (Informativo) </b>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <img alt="" src="linea.png" width="98%" height="10px" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top" align="center">
                                                                                                    <asp:GridView runat="server" ID="grdSEACEObra" EmptyDataText="No hay información registrada"
                                                                                                        ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" HeaderStyle-Wrap="true"
                                                                                                        CellPadding="2" CellSpacing="2" Width="900px" OnPageIndexChanging="grdSEACEObra_PageIndexChanging"
                                                                                                        OnRowDataBound="grdSEACEObra_RowDataBound"
                                                                                                        PageSize="10" AllowPaging="True" AllowSorting="True">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField HeaderText="Item" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblitemSeleccionBienesServicios" Text='<%# Eval("item") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                    <asp:HiddenField ID="hfIDENTIFICADOR" runat="server" Value='<%# Eval("IDENTIFICADOR")%>' />
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Identificador Convocatoria" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblCodigoConvocatoria" Text='<%# Eval("IDENTIFICADOR") %>' runat="server" CssClass="tablaGrilla"></asp:Label>

                                                                                                                    <%--       <button class="btn btn-primary" onclick="alert('¿Estas seguro de registrar el código de convocatoria seleccionado?')" >Registrar</button>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField HeaderText="Objeto Contratación" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblObjetoContratacion" Text='<%# Eval("Objeto") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                    <asp:Label ID="lblMsjNroItem" runat="server" Text="" Visible="false"></asp:Label>
                                                                                                                    <asp:Label ID="lblNroItem" runat="server" Text='<%# Eval("NroItem") %>' Visible="false"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Tipo de Selección" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblTipoSeleccion" Text='<%# Eval("TipoSeleccion") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Nomenclatura" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblNOMENCLATURA" Text='<%# Eval("NOMENCLATURA") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Fecha Convocatoria" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblFechaConvocatoria" Text='<%# Eval("FechaConvocatoria") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Cronograma" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblFechaProg" Text='<%# Eval("BuenaProProgramada") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                    <a class="enlace" target="_blank" onclick="<%# "loadModalIframe('CalendarioSeace',"+Eval("IDENTIFICADOR")+")" %>" title="Ver Cromograma de Convocatoria">
                                                                                                                        <img src="../img/newFecha.png" alt="Ver Detalle" onmouseover="this.src='../img/newFecha2.png';" onmouseout="this.src='../img/newFecha.png';" />
                                                                                                                    </a>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Estado" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblEstadoItem" Text='<%# Eval("EstadoItem") %>' runat="server"></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:BoundField Visible="true" DataField="ValorReferencial" DataFormatString="{0:N2}" HeaderText="Valor Referencial" HeaderStyle-CssClass="GridHeader3"></asp:BoundField>

                                                                                                            <asp:TemplateField HeaderText="Ver Resultados" Visible="true">
                                                                                                                <ItemTemplate>
                                                                                                                    <a class="enlace" target="_blank" onclick="<%# "loadModalIframe('FichaDetalleSeace',"+Eval("IDENTIFICADOR")+")" %>"
                                                                                                                        title="Ver Detalle">
                                                                                                                        <img src="../img/helpDocument.png" alt="Ver Detalle" onmouseover="this.src='../img/helpDoc2.png';" onmouseout="this.src='../img/helpDocument.png';" />
                                                                                                                    </a>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle CssClass="GridHeader3" />
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle Height="25px" />
                                                                                                        <EditRowStyle BackColor="#FFFFB7" />
                                                                                                    </asp:GridView>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                    <asp:Panel runat="server" ID="PanelSeaceManual">
                                                                                        <br />
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td colspan="2" style="height: 10px"></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td colspan="2" align="left" style="padding-left: 80px"><b>I. SEGUIMIENTO AL PROCESO DE SELECCIÓN </b></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td colspan="2" align="center">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:GridView runat="server" ID="grdSeguimientoProcesoTab1"
                                                                                                                    EmptyDataText="No hay registro de Seguimiento de Proceso"
                                                                                                                    ShowHeaderWhenEmpty="True"
                                                                                                                    AutoGenerateColumns="False"
                                                                                                                    RowStyle-HorizontalAlign="Center"
                                                                                                                    CellPadding="2" CellSpacing="2"
                                                                                                                    DataKeyNames="id_seguimiento"
                                                                                                                    OnRowDataBound="grdSeguimientoProcesoTab1_OnRowDataBound"
                                                                                                                    Width="800px"
                                                                                                                    OnSelectedIndexChanged="grdSeguimientoProcesoTab1_OnSelectedIndexChanged"
                                                                                                                    OnRowCommand="grdSeguimientoProcesoTab1_OnRowCommand">

                                                                                                                    <Columns>
                                                                                                                        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                                        <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                                        </asp:BoundField>
                                                                                                                        <asp:TemplateField Visible="false">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblIdSeguimiento" Text='<%# Eval("id_seguimiento") %>' runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText="Convocatoria" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblConvocatoria" Text='<%# Eval("convocatoria") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Fecha Publicación de Convocatoria" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblFecha" Text='<%# Eval("fechaPublicacion") %>' runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" Width="100px" />
                                                                                                                        </asp:TemplateField>


                                                                                                                        <asp:TemplateField HeaderText="Tipo de Adjudicación" Visible="false">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblTipoAdjudicacion" Text='<%# Eval("id_tipoAdjudicacion") %>' runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Tipo de Adjudicación" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblAdjudicacion" Text='<%# Eval("tipo_adjudicacion") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Resultado" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblNombResultado" Text='<%# Eval("resultado") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Resultado" Visible="false">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblResultado" Text='<%# Eval("idResultado") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:ImageButton ID="imgDocTab1" runat="server" OnClick="imgDocTab1_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                                    ToolTip='<%# Eval("urlDoc") %>' />
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Observación" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblobservacion" Text='<%# Eval("observacion") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField Visible="false">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>

                                                                                                                        <asp:TemplateField Visible="false">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblFecha_Update" Text='<%# Eval("strFecha_update") %>' runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>


                                                                                                                    </Columns>
                                                                                                                    <HeaderStyle Height="15px" />

                                                                                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                                                                                </asp:GridView>
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <asp:ImageButton ID="imgbtn_AgregarSeguimientoTab1" runat="server" ImageUrl="~/img/add.png"
                                                                                                                    onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';" Height="30px"
                                                                                                                    AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtn_AgregarSeguimientoTab1_OnClick" /></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center">
                                                                                                                <asp:Panel ID="Panel_AgregarSeguimientoTab1" Visible="false" runat="server" Width="100%">

                                                                                                                    <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                                        <table class="tablaRegistro" cellpadding="2" width="94%">
                                                                                                                            <tr>
                                                                                                                                <td colspan="4" align="center"><b>REGISTRO DE SEGUIMIENTO DEL PROCESO</b><asp:Label ID="lblRegistroSeguimientoP" runat="server" Visible="false"></asp:Label></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <tr>
                                                                                                                                    <td colspan="2" style="height: 10px"></td>
                                                                                                                                </tr>
                                                                                                                                <td align="right">Convocatoria :</td>
                                                                                                                                <td align="left">
                                                                                                                                    <asp:TextBox ID="TxtConvocatoriaTab1" runat="server" Width="250px"></asp:TextBox></td>

                                                                                                                                <td align="right" style="width: 100px">Fecha Publicación Convocatoria :</td>
                                                                                                                                <td align="left">
                                                                                                                                    <asp:TextBox ID="txtFechaPubliTab1" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txtFechaPubliTab1" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                                    <asp:CalendarExtender ID="CalendarExtender9" runat="server" TargetControlID="txtFechaPubliTab1"
                                                                                                                                        Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td align="right" style="width: 100px">Tipo de Adjudicación :</td>
                                                                                                                                <td align="left">
                                                                                                                                    <asp:DropDownList ID="ddlTipoAdjudicacionTab1" runat="server">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>


                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td align="right">Resultado :</td>
                                                                                                                                <td align="left">
                                                                                                                                    <asp:DropDownList ID="ddlResultadoTab1" runat="server">
                                                                                                                                    </asp:DropDownList></td>

                                                                                                                                <td align="right">Observación :</td>
                                                                                                                                <td align="left">
                                                                                                                                    <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td align="right">Documento :</td>
                                                                                                                                <td align="left" colspan="3">
                                                                                                                                    <asp:FileUpload ID="FileUploadSeguimientoTAb1" runat="server" />
                                                                                                                                    <asp:ImageButton ID="imgbtnSeguimientoTAb1" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " />
                                                                                                                                    <asp:LinkButton ID="LnkbtnSeguimientoTAb1" runat="server" OnClick="LnkbtnSeguimientoTAb1_OnClick"></asp:LinkButton>

                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="height: 10px"></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td colspan="4" align="left">
                                                                                                                                    <asp:Label ID="Label8" runat="server" Font-Bold="True" ForeColor="Red" Text="* Máximo 10 MB por Archivo"></asp:Label></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td colspan="4" align="left" style="padding-left: 30px;">
                                                                                                                                    <asp:Label ID="lblNomUsuarioSeguimientoProceso" runat="server" Font-Size="10px"></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>

                                                                                                                            <tr>
                                                                                                                                <td align="center" colspan="4">
                                                                                                                                    <asp:Button ID="btnGuardarSeguimientoProcesoTab1" runat="server" Text="Guardar" OnClick="btnGuardarSeguimientoProcesoTab1_OnClick" />
                                                                                                                                    <asp:Button ID="btnModificarSeguimientoProcesoTab1" runat="server" Text="Modificar" Visible="false" OnClick="btnModificarSeguimientoProcesoTab1_OnClick" />
                                                                                                                                    <asp:Button ID="btnCancelaSeguimientoProcesoTab1" runat="server" OnClick="btnCancelaSeguimientoProcesoTab1_OnClick" Text="Cancelar" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </asp:Panel>
                                                                                                            </td>
                                                                                                            <td></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td>
                                                                                                    <br />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left" style="padding-left: 80px"><b>II. RESULTADO DEL PROCESO DE SELECCIÓN </b></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td colspan="2" style="height: 10px"></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td colspan="2" align="center">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td colspan="4">
                                                                                                                <table width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="right">Tipo Adjudicación :
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:DropDownList ID="ddlAdjudicacionTab1" runat="server">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td align="right">NroLic :
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:TextBox ID="txtNroLicTab1" runat="server"></asp:TextBox>
                                                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txtNroLicTab1"
                                                                                                                                FilterType="Numbers" Enabled="True" />
                                                                                                                        </td>

                                                                                                                        <td align="right">Año :
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:TextBox ID="TxtAnio2Tab1" runat="server" Width="60px" MaxLength="4">
                                                                                                                            </asp:TextBox>
                                                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="TxtAnio2Tab1"
                                                                                                                                FilterType="Numbers" Enabled="True" />
                                                                                                                        </td>

                                                                                                                        <td align="right">Siglas :
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:TextBox ID="txtDetalleTab1" runat="server"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Fecha Buena Pro :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtProTab1" runat="server" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtProTab1" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Buena Pro Consentida :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtProConsentidaTab1" runat="server" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:CalendarExtender ID="CalendarExtender8" runat="server" TargetControlID="txtProConsentidaTab1" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Valor Referencial (S/.) :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtValorReferencialTab1" runat="server"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txtValorReferencialTab1"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Monto Contratado (S/.):</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox runat="server" ID="txtMontoContratadoTab1" Text="" Width="80px"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txtMontoContratadoTab1"
                                                                                                                    FilterType="Numbers,Custom" Enabled="True" ValidChars="." />
                                                                                                            </td>
                                                                                                            <td align="right">Nro Contrato :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox runat="server" ID="txtNroContrato" Text="" Width="120px"></asp:TextBox>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha de Firma de Contrato :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaFirmaContratoTab1" runat="server" MaxLength="10" placeholder="dd/mm/yyyy" Width="90"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txtFechaFirmaContratoTab1"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender17" runat="server" TargetControlID="txtFechaFirmaContratoTab1"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_lblNomConsultorTab1" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:Label ID="lblNomConsultorTab1" runat="server" Text="Contratista :"></asp:Label>
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>

                                                                                                            <td align="left">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_rbConsultorTab1" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:RadioButton ID="rbConsultorTab1" runat="server" AutoPostBack="true" OnCheckedChanged="rbConsultorTab1_OnCheckedChanged" />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                            <td align="right">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_lblNomConsorcioTab1" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:Label ID="lblNomConsorcioTab1" runat="server" Text="Consorcio :"></asp:Label>
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                            <td align="left">

                                                                                                                <asp:UpdatePanel runat="server" ID="Up_rbConsorcioTab1" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:RadioButton ID="rbConsorcioTab1" runat="server" AutoPostBack="true" OnCheckedChanged="rbConsorcioTab1_OnCheckedChanged" />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>

                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td colspan="4" align="left">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_rbProcesoTab1" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:Panel Width="100%" runat="server" ID="Panel_ConsultorTab1" Visible="false" CssClass="PanelOpcionalIzq">
                                                                                                                            <div style="width: 50%; padding: 5px 10px 5px 10px" class="CuadrosEmergentes">
                                                                                                                                <table width="100%">
                                                                                                                                    <tr>
                                                                                                                                        <td align="right">RUC :
                                                                                                                                        </td>
                                                                                                                                        <td align="left">
                                                                                                                                            <asp:TextBox runat="server" ID="txtRucConsultorTab1" Text="" MaxLength="11"></asp:TextBox>
                                                                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txtRucConsultorTab1"
                                                                                                                                                FilterType="Numbers" Enabled="True" />
                                                                                                                                            <asp:LinkButton ID="lnkbtnBuscarRUCContratistaTab1" runat="server" OnClick="lnkbtnBuscarRUCContratistaTab1_Click" CssClass="btn btn-success"><i class="glyphicon glyphicon-search"></i> Buscar</asp:LinkButton>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <div id="divMsjBusquedaRucContratistaTab1" runat="server">
                                                                                                                                            </div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="right">Nombre del Contratista :
                                                                                                                                        </td>
                                                                                                                                        <td align="left">
                                                                                                                                            <asp:TextBox runat="server" ID="txtNombConsultorTab1" Text="" Width="300px" Enabled="false"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                        <td align="right">Repre. Legal : </td>
                                                                                                                                        <td align="left">
                                                                                                                                            <asp:TextBox ID="txtRepresentanteLegalConsultorTab1" runat="server" Width="200px" Enabled="false"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="right">Contrato :
                                                                                                                                        </td>
                                                                                                                                        <td align="left">
                                                                                                                                            <asp:FileUpload ID="FileUploadContratoContratistaTab1" runat="server" />
                                                                                                                                            <asp:ImageButton ID="imgbtnContratoContratistaTab1" runat="server" ImageUrl="~/img/blanco.png"
                                                                                                                                                AlternateText=" " OnClick="imgbtnContratoContratistaTab1_OnClick" />
                                                                                                                                            <asp:LinkButton ID="lnkbtnContratoContratistaTab1" runat="server" Visible="false"></asp:LinkButton>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </div>


                                                                                                                        </asp:Panel>

                                                                                                                        <asp:Panel Width="100%" runat="server" ID="Panel_ConsorcioTab1" Visible="false" CssClass="PanelOpcionalDer">


                                                                                                                            <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                                                <table width="100%">
                                                                                                                                    <tr>
                                                                                                                                        <td valign="top" style="width: 50%">
                                                                                                                                            <table width="100%">
                                                                                                                                                <tr>
                                                                                                                                                    <td>
                                                                                                                                                        <asp:GridView runat="server" ID="grdConsultoresTab1"
                                                                                                                                                            DataKeyNames="id_grupo_consorcio"
                                                                                                                                                            RowStyle-HorizontalAlign="Center"
                                                                                                                                                            CellPadding="2" CellSpacing="2"
                                                                                                                                                            EmptyDataText="No hay data"
                                                                                                                                                            ShowHeaderWhenEmpty="True"
                                                                                                                                                            AutoGenerateColumns="False"
                                                                                                                                                            OnSelectedIndexChanged="grdConsultoresTab1_OnSelectedIndexChanged"
                                                                                                                                                            OnRowCommand="grdConsultoresTab1_OnRowCommand">

                                                                                                                                                            <Columns>
                                                                                                                                                                <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                                                                                <asp:TemplateField HeaderText="Miembro del Consorcio" Visible="true">
                                                                                                                                                                    <ItemTemplate>
                                                                                                                                                                        <asp:Label ID="lblNombConsultor" Text='<%# Eval("nombreContratista") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                                                                                    </ItemTemplate>
                                                                                                                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                                                                                                                </asp:TemplateField>

                                                                                                                                                                <asp:TemplateField HeaderText="RUC" Visible="true">
                                                                                                                                                                    <ItemTemplate>
                                                                                                                                                                        <asp:Label ID="lblRuc" Text='<%# Eval("rucContratista") %>' runat="server"></asp:Label>
                                                                                                                                                                    </ItemTemplate>
                                                                                                                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                                                                                                                </asp:TemplateField>

                                                                                                                                                                <asp:TemplateField HeaderText="Representante Legal" Visible="true">
                                                                                                                                                                    <ItemTemplate>
                                                                                                                                                                        <asp:Label ID="lblRepresentante" Text='<%# Eval("Representante") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                                                                                    </ItemTemplate>
                                                                                                                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                                                                                                                </asp:TemplateField>
                                                                                                                                                                <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                                                                                    <ItemTemplate>
                                                                                                                                                                        <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                                                                                    </ItemTemplate>
                                                                                                                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                                                                                                                </asp:TemplateField>
                                                                                                                                                            </Columns>
                                                                                                                                                            <HeaderStyle Height="15px" />

                                                                                                                                                            <EditRowStyle BackColor="#FFFFB7" />
                                                                                                                                                        </asp:GridView>
                                                                                                                                                    </td>
                                                                                                                                                    <td valign="top">
                                                                                                                                                        <asp:ImageButton ID="imgbtn_AgregarConsultorTab1" runat="server" ImageUrl="~/img/add.png"
                                                                                                                                                            onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';" Height="30px"
                                                                                                                                                            AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtn_AgregarConsultorTab1_OnClick" /></td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="left">

                                                                                                                                                        <asp:Panel ID="Panel_AgregarConsultorTab1" Visible="false" runat="server" Width="95%">

                                                                                                                                                            <div style="width: 90%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                                                                                <table cellpadding="2" width="97%">
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td colspan="4" align="center"><b>MIEMBROS DEL CONSORCIO</b><asp:Label ID="lblRegistroContratista" runat="server" Visible="false"></asp:Label></td>
                                                                                                                                                                    </tr>

                                                                                                                                                                    <tr>
                                                                                                                                                                        <td colspan="2" style="height: 10px"></td>
                                                                                                                                                                    </tr>

                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="right" style="width: 100px">RUC :
                                                                                                                                                                        </td>
                                                                                                                                                                        <td align="left">
                                                                                                                                                                            <asp:TextBox ID="txtRucGrupTab1" runat="server" MaxLength="11" Width="80px"></asp:TextBox>
                                                                                                                                                                            <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender32" FilterType="Numbers"
                                                                                                                                                                                TargetControlID="txtRucGrupTab1"></asp:FilteredTextBoxExtender>
                                                                                                                                                                            <asp:LinkButton ID="lnkbtnBuscarRUCMiembroConsorcioTab1" runat="server" OnClick="lnkbtnBuscarRUCMiembroConsorcioTab1_Click" CssClass="btn btn-success"><i class="glyphicon glyphicon-search"></i> Buscar</asp:LinkButton>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <div id="divMsjBusquedaRucMiembroConsorcioTab1" runat="server">
                                                                                                                                                                            </div>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>

                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="right">Contratista :</td>
                                                                                                                                                                        <td align="left">
                                                                                                                                                                            <asp:TextBox ID="txtContratistaGrupTab1" runat="server" Enabled="false"></asp:TextBox></td>

                                                                                                                                                                    </tr>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="right">Repre. Legal :</td>
                                                                                                                                                                        <td align="left">
                                                                                                                                                                            <asp:TextBox ID="txtRepresentanGrupTAb1" runat="server" Enabled="false"></asp:TextBox>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>

                                                                                                                                                                    <tr>
                                                                                                                                                                        <td style="height: 10px"></td>
                                                                                                                                                                    </tr>
                                                                                                                                                                    <tr>

                                                                                                                                                                        <td align="center" colspan="4">
                                                                                                                                                                            <asp:Button ID="btn_guardarConsultor" runat="server" Text="Guardar" OnClick="btn_guardarConsultor_OnClick" />
                                                                                                                                                                            <asp:Button ID="btn_modificarConsultor" runat="server" Text="Modificar" OnClick="btn_modificarConsultor_OnClick" />
                                                                                                                                                                            <asp:Button ID="btn_cancelarConsultor" runat="server" OnClick="btn_cancelarConsultor_OnClick" Text="Cancelar" />
                                                                                                                                                                        </td>
                                                                                                                                                                </table>
                                                                                                                                                            </div>
                                                                                                                                                        </asp:Panel>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                        <td valign="top" style="width: 50%">
                                                                                                                                            <table width="100%" class="tablaRegistro">
                                                                                                                                                <tr>
                                                                                                                                                    <td align="right">RUC:
                                                                                                                                                    </td>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <asp:TextBox ID="txtRucConsorcioTab1" runat="server" MaxLength="11"></asp:TextBox>
                                                                                                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender49" FilterType="Numbers"
                                                                                                                                                            TargetControlID="txtRucConsorcioTab1"></asp:FilteredTextBoxExtender>
                                                                                                                                                        <asp:LinkButton ID="lnkbtnBuscarRUCConsorcioTab1" runat="server" OnClick="lnkbtnBuscarRUCConsorcioTab1_Click" CssClass="btn btn-success"><i class="glyphicon glyphicon-search"></i> Buscar</asp:LinkButton>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td>
                                                                                                                                                        <div id="divMsjBusquedaRucConsorcioTab1" runat="server">
                                                                                                                                                        </div>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="right">Nombre del Consorcio :
                                                                                                                                                    </td>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <asp:TextBox ID="txtNomconsorcioTab1" runat="server" Width="300px"></asp:TextBox>
                                                                                                                                                    </td>

                                                                                                                                                </tr>




                                                                                                                                                <tr>
                                                                                                                                                    <td align="right">Representante Legal :
                                                                                                                                                    </td>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <asp:TextBox ID="txtRepresentanteTab1" Width="200px" runat="server"></asp:TextBox>
                                                                                                                                                    </td>

                                                                                                                                                </tr>

                                                                                                                                                <tr>
                                                                                                                                                    <td align="right">Teléfono :
                                                                                                                                                    </td>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <asp:TextBox ID="txtTelefonoConsorcioTab1" runat="server"></asp:TextBox>
                                                                                                                                                        <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender147" FilterType="Numbers" TargetControlID="txtTelefonoConsorcioTab1"></asp:FilteredTextBoxExtender>
                                                                                                                                                    </td>

                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="right">Carta Fiel :</td>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <asp:FileUpload ID="FileUploadAdjCartaTab1" runat="server" /><br />
                                                                                                                                                        <asp:ImageButton ID="imgbtnAdjCartaTab1" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="imgbtnAdjCartaTab1_OnClik" />
                                                                                                                                                        <asp:LinkButton ID="lnkbtnCartaTab1" runat="server" OnClick="lnkbtnCartaTab1_OnClik"></asp:LinkButton>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td align="right">Contrato :</td>
                                                                                                                                                    <td align="left">
                                                                                                                                                        <asp:FileUpload ID="FileUploadAdjContratoTab1" runat="server" />
                                                                                                                                                        <asp:ImageButton ID="imgbtnContratoTab1" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="imgbtnContratoTab1_OnClik" />
                                                                                                                                                        <asp:LinkButton ID="lnkbtnContratoTab1" runat="server" OnClick="lnkbtnContratoTab1_OnClik"></asp:LinkButton>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </table>
                                                                                                                                        </td>
                                                                                                                                        <td></td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </div>


                                                                                                                        </asp:Panel>
                                                                                                                    </ContentTemplate>
                                                                                                                    <Triggers>
                                                                                                                        <asp:PostBackTrigger ControlID="lnkbtnCartaTab1" />
                                                                                                                        <asp:PostBackTrigger ControlID="imgbtnAdjCartaTab1" />
                                                                                                                        <asp:PostBackTrigger ControlID="lnkbtnContratoTab1" />
                                                                                                                        <asp:PostBackTrigger ControlID="imgbtnContratoTab1" />
                                                                                                                        <asp:PostBackTrigger ControlID="imgbtnContratoContratistaTab1" />
                                                                                                                    </Triggers>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <br />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left" style="padding-left: 50px;">
                                                                                                    <asp:Label ID="lblNomActualizaProcesoSeleccion" runat="server" Font-Size="10px"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="center">
                                                                                                    <asp:Button ID="btnGuardarTab1" runat="server" Text="Guardar" OnClick="btnGuardarTab1_OnClick" />
                                                                                                </td>
                                                                                            </tr>


                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnGuardarTab1" />
                                                <asp:PostBackTrigger ControlID="btnGuardarSeguimientoProcesoTab1" />
                                                <asp:PostBackTrigger ControlID="btnModificarSeguimientoProcesoTab1" />
                                                <asp:PostBackTrigger ControlID="LnkbtnSeguimientoTAb1" />
                                                <asp:PostBackTrigger ControlID="grdSeguimientoProcesoTab1" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                       
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelEstadoEjecuccion" HeaderText="III. ESTADO DE EJECUCIÓN"
                                    TabIndex="2">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="LblCabeceraTabPanel2" runat="server" Text="III. ESTADO DE EJECUCIÓN"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>

                                        <asp:UpdatePanel ID="Up_Tab2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>

                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2" width="1100px">


                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" colspan="2" align="left" style="padding-left: 30px">III. ESTADO DE EJECUCIÓN
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center">
                                                                                <table width="90%">
                                                                                    <tr>
                                                                                        <td align="right" style="width: 200px">Responsable del Estudio :
                                                                                        </td>
                                                                                        <td align="left" colspan="3">
                                                                                            <asp:TextBox ID="txtCordinadorTab2" runat="server" Width="300px" Enabled="false"></asp:TextBox>
                                                                                            <asp:ImageButton CssClass="imgActualizar" ID="imgbtn_CoordinadorTab2" runat="server"
                                                                                                ImageUrl="~/img/edit.png" onmouseover="this.src='../img/edit2.png';" onmouseout="this.src='../img/edit.png';"
                                                                                                Height="25px" OnClick="imgbtn_CoordinadorTab2_OnClick" AlternateText="Agregar"
                                                                                                ToolTip="Actualizar Coodinador Designado" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" align="left">
                                                                                            <asp:Panel runat="server" ID="Panel_Coordinador" Visible="false" Width="60%">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table style="margin: auto">
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="center">
                                                                                                                <asp:GridView runat="server" ID="grd_HistorialCoordinador" EmptyDataText="No hay data"
                                                                                                                    ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                                    CellPadding="2" CellSpacing="2" DataKeyNames="id_residente_obra" OnRowCommand="grd_HistorialCoordinador_OnRowCommand"
                                                                                                                    OnSelectedIndexChanged="grd_HistorialCoordinador_OnSelectedIndexChanged" Width="100%">
                                                                                                                    <Columns>
                                                                                                                        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image"
                                                                                                                            SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit." HeaderStyle-Width="18px"></asp:CommandField>
                                                                                                                        <asp:TemplateField HeaderText="Responsable del Estudio" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblnombreS" Text='<%# Eval("nombre") %>' CssClass="tablaGrilla" runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" Width="200px" />
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText="Fecha Designación" Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblFechaSS" CssClass="tablaGrilla" Text='<%# Eval("fecha") %>' runat="server"></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" Width="60px" />
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');"
                                                                                                                                    CommandName="eliminar" Width="18px" />
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle CssClass="GridHeader2" />
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                    <HeaderStyle Height="15px" />
                                                                                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                                                                                </asp:GridView>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Responsable del Estudio:
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtCoordinador" runat="server"></asp:TextBox>
                                                                                                                <asp:Label ID="lblIdCoordinadorM" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>

                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha Designación:
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaC" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender102" runat="server" TargetControlID="txtFechaC"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender40" runat="server" TargetControlID="txtFechaC"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="center">
                                                                                                                <asp:Button ID="btnGuardar_Coordinador" runat="server" Text="Guardar" OnClick="btnGuardarCoordinador_OnClick" />
                                                                                                                <asp:Button ID="btnModificar_Coordinador" runat="server" Text="Modificar" OnClick="btnModificarCoordinador_OnClick"
                                                                                                                    Visible="false" />
                                                                                                                <asp:Button ID="btnCancelar_Coordinador" runat="server" OnClick="btnCancelarCoordinador_Onclick"
                                                                                                                    Text="Cancelar" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right">
                                                                                            <%--Telefono Coordinador :--%>
                                                                                        </td>
                                                                                        <td align="left" colspan="4">
                                                                                            <asp:TextBox ID="txtTelefonoCoordinadorTab2" runat="server" Width="90px" MaxLength="20"
                                                                                                Visible="false"></asp:TextBox>
                                                                                            <%--   &nbsp;&nbsp; Correo Coordinador : --%>
                                                                                            <asp:TextBox ID="txtCorreoCoordinadorTab2" runat="server" Width="170px" Visible="false"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                          
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="2">
                                                                                <table width="90%">
                                                                                    <tr>
                                                                                        <td align="right" style="width: 200px">Fecha de Inicio :
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtFechaInicioTab2" runat="server" Width="80px" AutoPostBack="true"
                                                                                                OnTextChanged="FechaTermino_OnTextChanged" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txtFechaInicioTab2"
                                                                                                FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                            <asp:CalendarExtender ID="CalendarExtender10" runat="server" TargetControlID="txtFechaInicioTab2"
                                                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                        </td>
                                                                                        <td align="right">F. Term. Contrac. + Ampl. + Paraliz.:
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtFechaTerminoReal" runat="server" Width="80px" placeholder="dd/mm/yyyy" Enabled="false"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="txtFechaTerminoReal"
                                                                                                FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                            <asp:CalendarExtender ID="CalendarExtender15" runat="server" TargetControlID="txtFechaTerminoReal"
                                                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right">Plazo de Ejecución (Días Calendario) :
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtPlazoEjecucionTab2" runat="server" Width="40px" AutoPostBack="true"
                                                                                                OnTextChanged="FechaTermino_OnTextChanged" Text="0"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtPlazoEjecucionTab2"
                                                                                                FilterType="Numbers" Enabled="True" />
                                                                                        </td>
                                                                                        <td align="right">Fecha de Termino Real :
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtFechaTermino" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="txtFechaTermino"
                                                                                                FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                            <asp:CalendarExtender ID="CalendarExtender16" runat="server" TargetControlID="txtFechaTermino"
                                                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <tr>
                                                                                            <td align="right">Fecha de Termino Contractual :
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:TextBox ID="txtFechaFinContractual" runat="server" Width="80px" placeholder="dd/mm/yyyy" Enabled="false"></asp:TextBox>
                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="txtFechaFinContractual"
                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                <asp:CalendarExtender ID="CalendarExtender14" runat="server" TargetControlID="txtFechaFinContractual"
                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 50px;">
                                                                                <asp:Label ID="lblNombActuaTab2" runat="server" Font-Size="10px"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center">
                                                                                <asp:Button ID="btnGuardarTab2" runat="server" Text="Guardar" OnClick="btnGuardarTab2_OnClick" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 20px"></td>
                                                                        </tr>

                                                                        <tr runat="server" id="trGEI" visible="false">
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>COMPROMISO DE GASES EFECTO INVERNADERO : </b>
                                                                                <asp:Button runat="server" ID="btnCompromisoGEI" CssClass="btn btn-primary" OnClientClick="loadModalIframe('CompromisosGEI', p)" Text="SEGUIMIENTO" />
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td colspan="2" style="height: 20px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>AVANCE FÍSICO DE PRE INVERSIÓN </b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img alt="" src="linea.png" width="98%" height="10px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:GridView runat="server" ID="grdAvanceFisicoTab2" EmptyDataText="No hay avance registrado"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                DataKeyNames="id_avanceFisico" CellPadding="2" CellSpacing="2" Width="1000px" Height="30px"
                                                                                                OnSelectedIndexChanged="grdAvanceFisicoTab2_OnSelectedIndexChanged" OnRowCommand="grdAvanceFisicoTab2_OnRowCommand"
                                                                                                OnRowDataBound="grdAvanceFisicoTab2_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image"
                                                                                                        SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" Height="30px" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:TemplateField HeaderText="Informe" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblInforme" Text='<%# Eval("informe") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha" Text='<%# Eval("fecha") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Avance Fisico (%)" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAvance" Text='<%# Eval("avance") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Avance Financiero (%)" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAvanceFinaciero" Text='<%# Eval("financieroReal") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Estado" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTipoEstadoEjecucion" Text='<%# Eval("TipoEstadoEjecucion") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Sub Estado" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTipoSubEstadoEjecucion" Text='<%# Eval("TipoSubEstadoEjecucion") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Detalle del Estado Situacional" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSitucion" Text='<%# Eval("situacion") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Acciones Previstas" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAccionesPrevistas" Text='<%# Eval("accionesPrevistas") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Acciones Realizadas" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAccionesRealizadas" Text='<%# Eval("accionesRealizadas") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocTab2" runat="server" OnClick="imgDocTab2_Click" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="A. M." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chbFlagAyuda" runat="server" Text='<%# Eval("flagAyuda") %>' OnCheckedChanged="chbFlagAyuda_CheckedChanged" AutoPostBack="true" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');"
                                                                                                                CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="A. M." Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAyudaObtiene" runat="server" Text='<%# Eval("flagAyuda") %>'></asp:Label>
                                                                                                            <asp:Label ID="lblId_UsuarioFlag" Text='<%# Eval("usuarioSecundario") %>' runat="server"></asp:Label>
                                                                                                            <asp:Label ID="lblFecha_UpdateFlag" Text='<%# Eval("fecha_updateSecundario") %>' runat="server"></asp:Label>
                                                                                                            <asp:Label ID="lblId_TipoEstadoEjecuccion" Text='<%# Eval("id_TipoEstadoEjecuccion") %>' runat="server" Visible="false"></asp:Label>
                                                                                                            <asp:Label ID="lblId_tipoSubEstadoEjecucion" Text='<%# Eval("id_tipoSubEstadoEjecucion") %>' runat="server" Visible="false"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtnAvanceFisicoTab2" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtnAvanceFisicoTab2_OnClick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <table class="tablaRegistro">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <b>Avance Físico (%):</b>
                                                                                                        <asp:Label ID="lblAvanceFisicoTab2" runat="server" Width="40px" Text="0.00" Font-Size="16px"></asp:Label><b></b>

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_AgregarAvanceTab2" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="90%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE AVANCE</b><asp:Label ID="lblRegistro_Avance" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right" style="width: 20%">Informe :
                                                                                                            </td>
                                                                                                            <td align="left" style="width: 80%" colspan="3">
                                                                                                                <asp:TextBox ID="txtInformeTab2" runat="server" Width="200px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaTab2" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" TargetControlID="txtFechaTab2"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender11" runat="server" TargetControlID="txtFechaTab2"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Estado :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_EstadoDetalleTab2" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:DropDownList ID="ddlTipoEstadoInformeTab2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoEstadoInformeTab2_SelectedIndexChanged">
                                                                                                                        </asp:DropDownList><br />
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>

                                                                                                            <td align="right">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_SubEstadoNombreTab2" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:Label ID="lblNombreSubEstado" runat="server" Text="Sub Estado :"></asp:Label>
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:UpdatePanel runat="server" ID="Up_SubEstadoDetalleTab2" UpdateMode="Conditional">
                                                                                                                    <ContentTemplate>
                                                                                                                        <asp:DropDownList ID="ddlTipoSubEstadoInformeTab2" runat="server">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </ContentTemplate>
                                                                                                                </asp:UpdatePanel>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Avance Fisico(%) :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtAvanceTab2" runat="server" Width="60px" MaxLength="6"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender31" TargetControlID="txtAvanceTab2"
                                                                                                                    FilterType="Numbers,Custom" ValidChars="." Enabled="true"></asp:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                            <td align="right">Avance Financiero(%) :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtAvanceFinancieroTab2" runat="server" Width="60px" MaxLength="6"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender107" TargetControlID="txtAvanceFinancieroTab2"
                                                                                                                    FilterType="Numbers,Custom" ValidChars="." Enabled="true"></asp:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Situación :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtSituacionTab2" runat="server" TextMode="MultiLine" Width="550px"
                                                                                                                    Height="60px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Acciones Previstas :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtAccionesPrevistasTab2" runat="server" TextMode="MultiLine" Width="550px"
                                                                                                                    Height="60px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Acciones Realizadas :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtAccionesRealizadasTab2" runat="server" TextMode="MultiLine" Width="550px"
                                                                                                                    Height="60px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right" style="width: 100px; font-weight: bold">¿Ayuda Memoria?:
                                                                                                            </td>
                                                                                                            <td align="left" style="width: 200px" colspan="3">
                                                                                                                <asp:CheckBox ID="chbAyudaMemoriaTab2" runat="server" />
                                                                                                                <asp:Label ID="lblNomUsuarioFlagAyudaTab2" runat="server" Text="" Font-Size="11px"></asp:Label>
                                                                                                            </td>

                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right" style="width: 100px; vertical-align: top;">Documento:
                                                                                                            </td>
                                                                                                            <td colspan="3" align="left">
                                                                                                                <asp:FileUpload ID="FileUpload_DocumentoTab2" runat="server" />
                                                                                                                <asp:ImageButton ID="imgbtnDocumentoTab2" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnDocumentoTab2_Click" />
                                                                                                                <asp:LinkButton ID="lnkbtnDocumentoTab2" runat="server" OnClick="lnkbtnDocumentoTab2_Click"></asp:LinkButton>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 50px;">
                                                                                                                <asp:Label ID="lblNomUsuarioAvance" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarAvanceTab2" runat="server" Text="Guardar" OnClick="btnGuardarAvanceTab2_OnClick" />
                                                                                                                <asp:Button ID="btnModificarAvanceTab2" runat="server" Text="Modificar" OnClick="btnModificarAvanceTab2_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarAvanceTab2" runat="server" Text="Cancelar" OnClick="btnCancelarAvanceTab2_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <br />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <%--<tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <asp:UpdatePanel ID="Up_Componentes" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:Button ID="btnComponentesPMIB" runat="server" Text="METAS PMIB" Width="140px" OnClick="btnComponentesPMIB_Click" />
                                                                                        <br />
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>--%>

                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:Panel runat="server" ID="PanelNotasAdicionales" Visible="false">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                                <b>NOTAS ADICIONALES SISEM: </b>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <img alt="" src="linea.png" width="98%" height="10px" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="center">
                                                                                                <asp:TextBox ID="TxtNotasAdicionales" runat="server" TextMode="MultiLine" Height="150"
                                                                                                    Width="85%">

                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" style="height: 10px"></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>


                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnGuardarAvanceTab2" />
                                                <asp:PostBackTrigger ControlID="btnModificarAvanceTab2" />
                                                <asp:PostBackTrigger ControlID="lnkbtnDocumentoTab2" />
                                                <asp:PostBackTrigger ControlID="imgbtnDocumentoTab2" />
                                                <asp:PostBackTrigger ControlID="grdAvanceFisicoTab2" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelAdicional" HeaderText="IV. INF. ADICIONAL" TabIndex="3">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="LblCabeceraTabPanel4" runat="server" Text="IV. INF. ADICIONAL"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2"
                                                        width="1100px">

                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" align="left" style="padding-left: 30px">IV. INFORMACIÓN ADICIONAL
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="padding-left: 80px"><b>PARALIZACIÓNES </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:GridView runat="server" ID="grdParalizacionTab4" EmptyDataText="No hay paralizaciones registradas"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                                                                DataKeyNames="id_ampliacion" RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2" Width="950px" OnRowDataBound="grdParalizacionTab4_RowDataBound"
                                                                                                OnSelectedIndexChanged="grdParalizacionTab4_SelectedIndexChanged"
                                                                                                OnRowCommand="grdParalizacionTab4_RowCommand">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblid_ampliacion" Text='<%# Eval("id_ampliacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Días. Parali." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblPlazo" Text='<%# Eval("ampliacionPlazo") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="80px" Height="20px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha Inicio" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblfechainicio" Text='<%# Eval("fechaInicio") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha Fin" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblfechaFin" Text='<%# Eval("fechaFin") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Informe" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAprobación" Text='<%# Eval("resolAprobacion") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha Informe" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFechaResol" Text='<%# Eval("fechaResolucion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Concepto" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblConcepto" Text='<%# Eval("concepto") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Observaciones" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblObservaciones" Text='<%# Eval("observaciones") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocParalizacionTab4" runat="server" OnClick="imgDocParalizacionTab4_Click" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />

                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtnAgregarParalizacionTAb4" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtnAgregarParalizacionTAb4_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="Label25" runat="server" Text="Total de días paralizados:"></asp:Label>
                                                                                            <asp:TextBox ID="txtTotalParalizacionTab4" Enabled="false" runat="server" CssClass="txtCenter" Width="40px" Text="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_ParalizacionTab4" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="85%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE PARALIZACIÓN</b><br />
                                                                                                                <asp:Label ID="Label2" runat="server" Font-Size="10px" Text="(Registro del tiempo paralizado que no fue considerado en las ampliaciones de plazo.)" CssClass="tablaRegistro"></asp:Label>
                                                                                                                <asp:Label ID="lblId_ParalizacionTab4" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Fecha Inicio :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaInicioParalizacionTab4" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtFechaInicioParalizacionTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender12" runat="server" TargetControlID="txtFechaInicioParalizacionTab4" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Fin :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaFinParalizacionTab4" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender150" runat="server" TargetControlID="txtFechaFinParalizacionTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender24" runat="server" TargetControlID="txtFechaFinParalizacionTab4" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr runat="server" visible="false">
                                                                                                            <td align="right">Tiempo Paralizado (días Calendario):
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtDiasParalizadosTab4" runat="server" Width="60px" MaxLength="9"></asp:TextBox>
                                                                                                                <asp:Label ID="Label29" runat="server" ForeColor="Red" Font-Size="12px" Text="(*) Verifique bien este campo, luego de registrar no podrá modificarlo."></asp:Label>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtDiasParalizadosTab4" FilterType="Numbers" Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Informe :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtResoluciónParalizacionTab4" runat="server" Width="200px" MaxLength="200"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Informe :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaResolucionParalizacionTab4" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtFechaResolucionParalizacionTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender13" runat="server" TargetControlID="txtFechaResolucionParalizacionTab4" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>


                                                                                                        <tr>
                                                                                                            <td align="right">Concepto :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtConceptoParalizacionTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px">
                                                                                                                </asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Observaciones :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtObservacionParalizacionTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Documento :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:FileUpload ID="FileUpload_ParalizacionTab4" runat="server" />
                                                                                                                <asp:ImageButton ID="imgbtnParalizacionTab4" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " OnClick="lnkbtnParalizacionTab4_Click" />
                                                                                                                <asp:LinkButton ID="lnkbtnParalizacionTab4" runat="server" OnClick="lnkbtnParalizacionTab4_Click"></asp:LinkButton>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="Label30" runat="server" Font-Size="10px" ForeColor="#2169d4" Text="NOTA: Los días paralizados modificará la programación de obra." CssClass="tablaRegistro" Visible="false"></asp:Label>
                                                                                                                <br />
                                                                                                                <asp:Label ID="lblNomActualizaParalizacionTab4" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarParalizacionTab4" runat="server" Text="Registrar" OnClick="btnGuardarParalizacionTab4_Click" />
                                                                                                                <asp:Button ID="btnModificarParalizacionTab4" runat="server" Text="Modificar" OnClick="btnModificarParalizacionTab4_Click" />
                                                                                                                <asp:Button ID="btnCancelarParalizacionTab4" runat="server" Text="Cancelar" OnClick="btnCancelarParalizacionTab4_Click" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="padding-left: 80px"><b>AGREGAR AMPLIACIÓN PLAZO </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:GridView runat="server" ID="grdPlazoTab4" EmptyDataText="No hay ampliación de plazo registrado"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                                                                DataKeyNames="id_ampliacion" RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2" Width="950px" OnRowDataBound="grdPlazoTab4_OnDataBound"
                                                                                                OnSelectedIndexChanged="grdPlazoTab4_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdPlazoTab4_OnRowCommand">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblid_ampliacion" Text='<%# Eval("id_ampliacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Ampl. Plazo (Días)" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblPlazo" Text='<%# Eval("ampliacionPlazo") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="80px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha Inicio" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblfechainicio" Text='<%# Eval("fechaInicio") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha Fin" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblfechaFin" Text='<%# Eval("fechaFin") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Resol. Aprobación" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAprobación" Text='<%# Eval("resolAprobacion") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Resol. Fecha" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFechaResol" Text='<%# Eval("fechaResolucion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Concepto" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblConcepto" Text='<%# Eval("concepto") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Observaciones" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblObservaciones" Text='<%# Eval("observaciones") %>' CssClass="tablaRegistro" runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocPlaTab4" runat="server" OnClick="imgDocPlaTab4_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" " ToolTip='<%# Eval("urlDoc") %>' />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgAgregaPlazoTab4" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgAgregaPlazoTab4_OnClick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="Label9" runat="server" Text="Total de días ampliados:"></asp:Label>
                                                                                            <asp:TextBox ID="lblTotalDias" Enabled="false" runat="server" CssClass="txtCenter" Width="40px" Text="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_PlazoTab4" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="75%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE APLIACIÓN DE PLAZO</b><asp:Label ID="lblId_Registro_Ampliacion_Plazos" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Amp. Plazo :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtAmpPlazoPlaTab4" runat="server" Width="60px" MaxLength="9"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtAmpPlazoPlaTab4" FilterType="Numbers" Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Resol. Aprob. :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtResolAproPlaTab4" runat="server" Width="100px"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right">Resol.Fecha :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaResolPlaTab4" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender95" runat="server" TargetControlID="txtFechaResolPlaTab4" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtFechaResolPlaTab4"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha Inicio :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaInicio" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender100" runat="server" TargetControlID="txtFechaInicio"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender32" runat="server" TargetControlID="txtFechaInicio"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Fin :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaFin" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender101" runat="server" TargetControlID="txtFechaFin"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender33" runat="server" TargetControlID="txtFechaFin"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>


                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Concepto :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtConceptoPlaTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px">
                                                                                                                </asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Observaciones :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtObservacionPlaTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Documento :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:FileUpload ID="FileUploadPlaTab4" runat="server" />
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgbtnPlaTab4" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " />
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="LnkbtnPlaTab4" runat="server" OnClick="imgbtnPlaTabb4_OnClick"></asp:LinkButton>
                                                                                                                </td>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomUsuarioAmpDias" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarPlazoTab4" runat="server" Text="Guardar" OnClick="btnGuardarPlazoTab4_OnClick" />
                                                                                                                <asp:Button ID="btnModificarPlazoTab4" runat="server" Text="Modificar" OnClick="btnModificarPlazoTab4_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarPlazoTab4" runat="server" Text="Cancelar" OnClick="btnCancelarPlazoTab4_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="padding-left: 80px"><b>AGREGAR PRESUPUESTO ADICIONAL </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:GridView runat="server" ID="grdPresupuestoTab4" EmptyDataText="No hay ampliación de presupuesto registrado"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                DataKeyNames="id_ampliacion" OnRowDataBound="grdPresupuestoTab4_OnRowDataBound"
                                                                                                CellPadding="2" CellSpacing="2"
                                                                                                Width="950px"
                                                                                                OnSelectedIndexChanged="grdPresupuestoTab4_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdPresupuestoTab4_OnRowCommand">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>

                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblid_ampliacion" Text='<%# Eval("id_ampliacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="montoPresup" HeaderText="Monto Presup. (S/.)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="60px" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:TemplateField HeaderText="" Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblPresupuesto" Text='<%# Eval("montoPresup") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Vinculación" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblVinculacion" Text='<%# Eval("vinculacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Resol. Aprob." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblResolAprob" Text='<%# Eval("resolAprobacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Resol. Fecha" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFechaResol" Text='<%# Eval("fechaResolucion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="% Incidencia" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIncidencia" Text='<%# Eval("porcentajeIncidencia") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Concepto" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblConcepto" Text='<%# Eval("concepto") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Observaciones" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblObservaciones" Text='<%# Eval("observaciones") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocPreTab4" runat="server" OnClick="imgDocPreTab4_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />

                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imbtnAgregarPresupuestoTab4" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imbtnAgregarPresupuestoTab4_OnClick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="Label10" runat="server" Text="Total de presupuesto adicional:"></asp:Label>
                                                                                            <asp:TextBox ID="lblPresupuestoAdicional" Enabled="false" runat="server" CssClass="txtCenter" Width="40px" Text="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_PresupuestoTab4" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="75%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE PRESUPUESTO ADICIONAL</b><asp:Label ID="lblId_Registro_Presupuesto_Adicional" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>


                                                                                                        <tr>
                                                                                                            <td align="right">Monto. Presup. :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtMontoPreTab4" runat="server" Width="60px" MaxLength="18"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtMontoPreTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Vinculación :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtVincuPreTab4" runat="server" Width="60px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Resol. Aprob. :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtResolAproPreTab4" runat="server" Width="100px"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Resol.:
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaPreTab4" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender96" runat="server" TargetControlID="txtFechaPreTab4" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtFechaPreTab4"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>

                                                                                                            </td>
                                                                                                        </tr>



                                                                                                        <tr>
                                                                                                            <td align="right">% Incidencia :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtIncidenciaPreTab4" runat="server" Width="40px" MaxLength="6"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtIncidenciaPreTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Concepto :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtConceptoPreTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Observaciones :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtObsPreTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Documento :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">

                                                                                                                <asp:FileUpload ID="FileUploadPreTab4" runat="server" />
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgbtnPreTab4" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " />
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="LnkbtnPreTab4" runat="server" OnClick="LnkbtnPreTab4_OnClick"></asp:LinkButton>
                                                                                                                </td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomUsuarioPresupuesto" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarPresupuestoTab4" runat="server" Text="Guardar" OnClick="btnGuardarPresupuestoTab4_OnClick" />
                                                                                                                <asp:Button ID="btnModificarPresupuestoTab4" runat="server" Text="Modificar" OnClick="btnModificarPresupuestoTab4_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarPresupuestoTab4" runat="server" Text="Cancelar" OnClick="btnCancelarPresupuestoTab4_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="padding-left: 80px"><b>AGREGAR PRESUPUESTO DEDUCTIVO (REDUCCIONES) </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top" align="center">
                                                                                            <asp:GridView runat="server" ID="grdDeductivoTab4" EmptyDataText="No hay reducción de presupuesto registrado"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                                                                DataKeyNames="id_ampliacion" OnRowDataBound="grdDeductivoTab4_OnRowDataBound" RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2"
                                                                                                Width="950px"
                                                                                                OnSelectedIndexChanged="grdDeductivoTab4_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdDeductivoTab4_OnRowCommand">

                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:BoundField DataField="montoPresup" HeaderText="Monto Presup. (S/.)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="60px" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:TemplateField HeaderText="" Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lbldeductivo" Text='<%# Eval("montoPresup") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblid_ampliacion" Text='<%# Eval("id_ampliacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>




                                                                                                    <asp:TemplateField HeaderText="Vinculación" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblVinculacion" Text='<%# Eval("vinculacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Resol. Aprob." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblResolAprob" Text='<%# Eval("resolAprobacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Resol. Fecha" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFechaResol" Text='<%# Eval("fechaResolucion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="% Incidencia" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIncidencia" Text='<%# Eval("porcentajeIncidencia") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Concepto" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblConcepto" Text='<%# Eval("concepto") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Observaciones" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblObservaciones" Text='<%# Eval("observaciones") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocDedTab4" runat="server" OnClick="imgDocDedTab4_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />

                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top" align="left">
                                                                                            <asp:ImageButton ID="imgbtnDeductivoTab4" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtnDeductivoTab4_OnClick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="Label11" runat="server" Text="Total de presupuesto deductivo:"></asp:Label>
                                                                                            <asp:TextBox ID="lblDeductivoTotal" Enabled="false" runat="server" CssClass="txtCenter" Width="40px" Text="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_deductivoTab4" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="75%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE PRESUPUESTO DEDUCTIVO</b><asp:Label ID="lblId_Registro_Presupuesto_Deductivo" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>


                                                                                                        <tr>
                                                                                                            <td align="right">Monto. Presup. :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtMontoDedTab4" runat="server" Width="60px"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txtMontoDedTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                            <td align="right">Vinculación :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtVincuDedTab4" runat="server" Width="60px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Resol. Aprob. :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtResolAproDedTab4" runat="server" Width="100px"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right">Fecha Resol. :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaDedTab4" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender98" runat="server" TargetControlID="txtFechaDedTab4" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="txtFechaDedTab4"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>



                                                                                                        <tr>
                                                                                                            <td align="right">% Incidencia :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtIncidenciaDedTab4" runat="server" Width="40px" MaxLength="6"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtIncidenciaDedTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Concepto :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtConceptoDedTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Observaciones :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtObsDedTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Documento :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:FileUpload ID="FileUploadDedTab4" runat="server" />
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgbtnDedTab4" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " />
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="LnkbtnDedTab4" runat="server" OnClick="LnkbtnDedTab4_OnClick"></asp:LinkButton>
                                                                                                                </td>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomUsuarioDeductivo" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarDeductivoTab4" runat="server" Text="Guardar" OnClick="btnGuardarDeductivoTab4_OnClick" />
                                                                                                                <asp:Button ID="btnModificarDeductivoTab4" runat="server" Text="Modificar" OnClick="btnModificarDeductivoTab4_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarDeductivoTab4" runat="server" Text="Cancelar" OnClick="btnCancelarDeductivoTab4_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="padding-left: 80px"><b>PENALIDADES </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:GridView runat="server" ID="grdPenalidadTab4" EmptyDataText="No hay penalidades registradas"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                                                                DataKeyNames="id_penalidad" RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2" Width="950px" OnRowDataBound="grdPenalidadTab4_RowDataBound"
                                                                                                OnSelectedIndexChanged="grdPenalidadTab4_SelectedIndexChanged"
                                                                                                OnRowCommand="grdPenalidadTab4_RowCommand">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:BoundField DataField="nro" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lbliTipoPenalidad" Text='<%# Eval("iTipoPenalidad") %>' runat="server"></asp:Label>
                                                                                                            <asp:Label ID="lblid_penalidad" Text='<%# Eval("id_penalidad") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Tipo Penalidad" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTipoPenalidad" Text='<%# Eval("tipoPenalidad") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Height="20px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Detalle del Incumplimiento" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lbldetalleIncumplimiento" Text='<%# Eval("detalleIncumplimiento") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha Penalidad" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblfechaPenalidad" Text='<%# Eval("vFechaPenalidad") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="montoPenalidad" HeaderText="Monto Penalidad (S/.)" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:BoundField>
                                                                                                    <%--<asp:TemplateField HeaderText="Monto Penalidad" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblMontoPenalidad" Text='<%# Eval("montoPenalidad") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>--%>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtnAgregarPenalidadTab4" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Penalidad" OnClick="imgbtnAgregarPenalidadTab4_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_PenalidadTab4" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="85%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE PENALIDAD</b><br />
                                                                                                                <asp:Label ID="lblId_PenalidadTab4" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Tipo Penalidad :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:DropDownList ID="ddlTipoPenalidadTab4" runat="server">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha Penalidad :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaPenalidadTab4" runat="server" Width="80px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" TargetControlID="txtFechaPenalidadTab4"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender21" runat="server" TargetControlID="txtFechaPenalidadTab4" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Detalle del Incumplimiento :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtDetalleIncumplimientoTab4" runat="server" TextMode="MultiLine" Width="300px" Height="50px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Monto Penalidad (S/.) :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtMontoPenalidadTab4" runat="server" Text="0"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server" TargetControlID="txtMontoPenalidadTab4" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomActualizaPenalidadTab4" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarPenalidadTab4" runat="server" Text="Registrar" OnClick="btnGuardarPenalidadTab4_Click" />
                                                                                                                <asp:Button ID="btnModificarPenalidadTab4" runat="server" Text="Modificar" OnClick="btnModificarPenalidadTab4_Click" />
                                                                                                                <asp:Button ID="btnCancelarPenalidadTab4" runat="server" Text="Cancelar" OnClick="btnCancelarPenalidadTab4_Click" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </td>


                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>

                                                <asp:PostBackTrigger ControlID="btnGuardarParalizacionTab4" />
                                                <asp:PostBackTrigger ControlID="imgbtnParalizacionTab4" />
                                                <asp:PostBackTrigger ControlID="lnkbtnParalizacionTab4" />
                                                <asp:PostBackTrigger ControlID="grdParalizacionTab4" />

                                                <asp:PostBackTrigger ControlID="btnGuardarPlazoTab4" />
                                                <asp:PostBackTrigger ControlID="btnModificarPlazoTab4" />
                                                <asp:PostBackTrigger ControlID="LnkbtnPlaTab4" />


                                                <asp:PostBackTrigger ControlID="grdPlazoTab4" />

                                                <asp:PostBackTrigger ControlID="btnGuardarPresupuestoTab4" />
                                                <asp:PostBackTrigger ControlID="btnModificarPresupuestoTab4" />
                                                <asp:PostBackTrigger ControlID="LnkbtnPreTab4" />


                                                <asp:PostBackTrigger ControlID="grdPresupuestoTab4" />

                                                <asp:PostBackTrigger ControlID="grdDeductivoTab4" />
                                                <asp:PostBackTrigger ControlID="btnGuardarDeductivoTab4" />
                                                <asp:PostBackTrigger ControlID="btnModificarDeductivoTab4" />
                                                <asp:PostBackTrigger ControlID="LnkbtnDedTab4" />

                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelLiquidacion" HeaderText="V. LIQUIDACIÓN Y CIERRE" TabIndex="4">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="LblCabeceraTabPanel3" runat="server" Text="V. LIQUIDACIÓN Y CIERRE"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2"
                                                        width="1100px">
                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" align="left" style="padding-left: 30px">V. LIQUIDACIÓN Y CIERRE CONTRATO/CONVENIO
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="center">
                                                                                <table width="85%" class="tablaRegistro">
                                                                                    <tr>
                                                                                        <td align="right">a)Resolución de Liq. de Consul. :</td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtLiquidacionTab3" runat="server" Width="250px"></asp:TextBox><asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender174" runat="server" TargetControlID="txtLiquidacionTab3"
                                                                                                FilterType="Custom" ValidChars=" abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZúÚíÍáÁéÉóÓÜü.-°_0123456789./" Enabled="True" />
                                                                                        </td>
                                                                                        <td align="right">Fecha :</td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtFechaTab3" runat="server" Width="70px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                            <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtFechaTab3"
                                                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td align="right">Monto de Liq. (S/.) :</td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtMontoTab3" runat="server" Style="text-align: right;" Width="80px" Text="0.00" OnTextChanged="txtMontoTab3_OnClick" AutoPostBack="true"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="txtMontoTab3"
                                                                                                FilterType="Custom, Numbers" ValidChars="." Enabled="True" />

                                                                                        </td>
                                                                                        <td align="right">Liq. De Ejecucion:</td>
                                                                                        <td align="left" colspan="3">
                                                                                            <asp:FileUpload ID="FileUploadLiquidacionTab3" runat="server" />
                                                                                            <asp:ImageButton ID="imgbtnLiquidacionTab3" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                OnClick="imgbtnLiquidacionTab3_OnClik" />
                                                                                            <asp:LinkButton ID="LnkbtnLiquidacionTab3" runat="server" OnClick="LnkbtnLiquidacion_OnClik">
                                                                                            </asp:LinkButton>

                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td align="right">b)  Resolución de Liq. De Superv. De Consul.:
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtResolucionLiqSupervision" runat="server" Width="300px"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender175" runat="server" TargetControlID="txtResolucionLiqSupervision"
                                                                                                FilterType="Custom" ValidChars=" abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZúÚíÍáÁéÉóÓÜü.-°_0123456789./" Enabled="True" />
                                                                                        </td>
                                                                                        <td align="right">Fecha :
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtFechaLiqSupervision" runat="server" Width="70px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender169" runat="server" TargetControlID="txtFechaLiqSupervision"
                                                                                                FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                            <asp:CalendarExtender ID="CalendarExtender38" runat="server" TargetControlID="txtFechaLiqSupervision"
                                                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right">Monto de Liq. (S/.) :
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtMontoLiqSupervision" Style="text-align: right;" Text="0.00" runat="server" Width="80px" AutoPostBack="true" OnTextChanged="txtMontoTab3_OnClick"></asp:TextBox>
                                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender171" runat="server" TargetControlID="txtMontoLiqSupervision"
                                                                                                FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                                                                        </td>
                                                                                        <td align="right">Liq. de Supervision:
                                                                                        </td>
                                                                                        <td align="left" colspan="3">
                                                                                            <asp:FileUpload ID="FileUploadLiqSupervisionTab3" runat="server" />
                                                                                            <asp:ImageButton ID="imgbtnLiqSupervisionTab3" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                OnClick="imgbtnLiqSupervisionTab3_Click" /><br />
                                                                                            <asp:LinkButton ID="lnkbtnLiqSupervisionTab3" runat="server">
                                                                                            </asp:LinkButton>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td align="right">Acta y/o oficio de Cierre de Convenio :</td>
                                                                                        <td align="left" colspan="3">
                                                                                            <asp:FileUpload ID="FileUploadActaTab3" runat="server" />
                                                                                            <asp:ImageButton ID="imgbtnActaTab3" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                OnClick="imgbtnActaTab3_OnClik" />
                                                                                            <asp:LinkButton ID="LnkbtnActaTab3" runat="server" OnClick="LnkbtnActaTab3_OnClik">
                                                                                            </asp:LinkButton>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td style="height: 10px"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" align="left">
                                                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="Red" Text="* Máximo 10 MB por Archivo"></asp:Label></td>
                                                                                    </tr>

                                                                                </table>
                                                                         
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" align="left" style="padding-left: 50px;">
                                                                                <asp:Label ID="lblNomLiquidacion" runat="server" Font-Size="10px"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button runat="server" ID="btnGuardarTab3" Text="GUARDAR" OnClick="btnGuardarTab3_OnClick" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 20px"></td>
                                                                        </tr>
                                                                        <tr id="trVigencia1" runat="server" visible="true">
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>VIGENCIA VENCIDA </b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trVigencia2" runat="server" visible="true">
                                                                            <td colspan="2">
                                                                                <img alt="" src="linea.png" width="98%" height="10px" /></td>
                                                                        </tr>
                                                                        <tr id="trVigencia3" runat="server" visible="true">
                                                                            <td align="left" colspan="2">
                                                                                <table width="550px" class="tablaRegistro" style="padding-left: 100px" >
                                                                                    <tr>
                                                                                        <td>
                                                                                            <p>
                                                                                                En caso la etapa actual se encuentra desactualizado (vigencia vencida) y se debe retrotraer a una etapa anterior o requiera la actualización de la etapa actual. Se creará un nuevo registro.
                                                                                            </p>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button runat="server" ID="btnVigenciaVencida" Text="REGISTRAR" CssClass="btn btn-primary" OnClick="btnVigenciaVencida_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 20px"></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>FINALIZACIÓN DE REGISTRO </b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img alt="" src="linea.png" width="98%" height="10px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td align="left" style="padding-left: 50px">
                                                                                            <span class='tooltips'><i class='fa fa-info-circle font-lg'></i><span>
                                                                                                <b>ACTIVO:</b> El proyecto puede ser actualizado.
                                                                                                <br />
                                                                                                <b>FINALIZADO:</b> El proyecto no pueder ser actualizado ni reasignado.
                                                                                            </span></span>
                                                                                            Estado de Registro:</td>

                                                                                        <td align="center">

                                                                                            <asp:Label runat="server" Text="ACTIVAR" Font-Size="14px" ID="lblEstadoRegistro" ForeColor="#006600" Font-Bold="true"></asp:Label>
                                                                                            <asp:Label runat="server" Font-Size="14px" ID="lblIdEstadoRegistro" Visible="false"></asp:Label>

                                                                                            <asp:Button runat="server" ID="btnFinalizarTab3" CssClass="btn btn-warning" OnClick="btnFinalizarTab3_Click" />
                                                                                            <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1333" runat="server" TargetControlID="btnFinalizarTab3"
                                                                                                ConfirmText="¿Está seguro de continuar? Activar: El proyecto podrá ser actualizado, Finalizar: El proyecto no podrá ser actualizado." Enabled="True"></asp:ConfirmButtonExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br />
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnGuardarTab3" />

                                                <asp:PostBackTrigger ControlID="LnkbtnLiquidacionTab3" />
                                                <asp:PostBackTrigger ControlID="imgbtnLiquidacionTab3" />
                                                <asp:PostBackTrigger ControlID="LnkbtnActaTab3" />
                                                <asp:PostBackTrigger ControlID="imgbtnActaTab3" />

                                                <%--<asp:PostBackTrigger ControlID="imgbtnDevolucionTab3" />
                                                <asp:PostBackTrigger ControlID="LnkbtnDevolucionTab3" />--%>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelFotografico" Visible="true" HeaderText="VI. DOCUMENTOS"
                                    TabIndex="5">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="Label6" runat="server" Text="VI. DOCUMENTOS"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab6" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2"
                                                        width="1100px">

                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" align="left" style="padding-left: 30px">VI. DOCUMENTOS
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:GridView runat="server" ID="grdPanelTab6" EmptyDataText="No hay información registrada"
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2" Width="800px" DataKeyNames="Id_documentoMonitor"
                                                                                                OnSelectedIndexChanged="grdPanelTab6_OnSelectedIndexChanged" OnRowCommand="grdPanelTab6_OnRowCommand" OnRowDataBound="grdPanelTab6_OnRowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>

                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="10px" />
                                                                                                    </asp:BoundField>

                                                                                                    <asp:TemplateField HeaderText="Nombre de Archivo" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblNombre" Text='<%# Eval("nombre") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Tipo" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTipo" Text='<%# Eval("tipo") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIDTipo" Text='<%# Eval("id_tipoDocumentoMonitor") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Descripción" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblDescripcion" Text='<%# Eval("descripcion") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Fecha" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha" Text='<%# Eval("fecha") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Ver" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocTab6" runat="server" OnClick="imgDocTab6_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />

                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtnAgregarPanelTab6" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Cartas Fianzas" OnClick="imgbtnAgregarPanelTab6_OnClick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_FotograficoTab6" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="85%" class="tablaRegistro">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO DE ARCHIVOS</b>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Nombre del Archivo :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtNombreArchivoTab6" runat="server" Width="300px" MaxLength="100"></asp:TextBox>
                                                                                                                <asp:Label ID="lblId_documento_monitor" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>

                                                                                                        </tr>


                                                                                                        <tr>
                                                                                                            <td align="right">Descripción :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:TextBox ID="txtDescripcionTab6" runat="server" Width="450px" TextMode="MultiLine" Height="60px" MaxLength="500"></asp:TextBox>
                                                                                                            </td>

                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Tipo de Archivo :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:DropDownList ID="ddlTipoArchivoTab6" runat="server">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>

                                                                                                            <td align="right">Fecha :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaTab6" runat="server" Width="65px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender161" runat="server" TargetControlID="txtFechaTab6"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender31" runat="server" TargetControlID="txtFechaTab6"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>

                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Archivo :
                                                                                                            </td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:FileUpload ID="FileUpload_Tab6" runat="server" />
                                                                                                                <asp:ImageButton ID="imgbtnDocTab6" runat="server" ImageUrl="~/img/blanco.png" AlternateText="" OnClick="LnkbtnDocTab6_OnClick" />
                                                                                                                <asp:LinkButton ID="LnkbtnDocTab6" runat="server" OnClick="LnkbtnDocTab6_OnClick" Visible="false"></asp:LinkButton>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td colspan="2" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomUsuarioDocumento" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarPanelTab6" runat="server" Text="Guardar" OnClick="btnGuardarPanelTab6_OnClick" />
                                                                                                                <asp:Button ID="btnModificarPanelTab6" runat="server" Text="Modificar" OnClick="btnModificarPanelTab6_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarPanelTab6" runat="server" Text="Cancelar" OnClick="btnCancelarPanelTab6_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="grdPanelTab6" />
                                                <asp:PostBackTrigger ControlID="btnGuardarPanelTab6" />
                                                <asp:PostBackTrigger ControlID="imgbtnDocTab6" />
                                                <asp:PostBackTrigger ControlID="LnkbtnDocTab6" />

                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelDecViabilidad" HeaderText="VII. DECLARATORIA VIABILIDAD" TabIndex="9">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="Label5" runat="server" Text="VII. DECLARATORIA VIABILIDAD"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab9" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2" width="1100px">
                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" colspan="2" align="left" style="padding-left: 30px">VII. DECLARATORIA VIABILIDAD
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>1) Informe de Supervisión del Estudio de PreInversión (Aprobación Técnica)</b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img alt="" src="linea.png" width="98%" height="10px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:GridView runat="server" ID="grdSupervisionEstudioTab9"
                                                                                                EmptyDataText="No hay registro de Supervisión de Estudio"
                                                                                                ShowHeaderWhenEmpty="True"
                                                                                                AutoGenerateColumns="False"
                                                                                                RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2"
                                                                                                DataKeyNames="id_aprobacion"
                                                                                                OnRowDataBound="grdSupervisionEstudioTab9_OnRowDataBound"
                                                                                                Width="800px"
                                                                                                OnSelectedIndexChanged="grdSupervisionEstudioTab9_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdSupervisionEstudioTab9_OnRowCommand">

                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIdAprobacion" Text='<%# Eval("id_aprobacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="DETALLE DOCUMENTO" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblDetalleDocumento" Text='<%# Eval("detalleDocumento") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="FECHA" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha" Text='<%# Eval("vFecha") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="100px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDoc1Tab9" runat="server" OnClick="imgDoc1Tab9_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');"
                                                                                                                CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtn_AgregarSupervisionEstudioTab9" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';" Height="30px"
                                                                                                AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtn_AgregarSupervisionEstudioTab9_OnClick" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:Panel ID="Panel_AgregarSupervisionEstudioTab9" Visible="false" runat="server" Width="100%">

                                                                                                <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                    <table class="tablaRegistro" cellpadding="2" width="94%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center"><b>REGISTRO DE SUPERVISIÓN DEL ESTUDIO</b><asp:Label ID="lblRegistroSupervisionE" runat="server" Visible="false"></asp:Label></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <tr>
                                                                                                                <td colspan="2" style="height: 10px"></td>
                                                                                                            </tr>
                                                                                                            <td align="right">Detalle de Documento :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtDetalleDocumento1" runat="server" TextMode="MultiLine" Width="350px" Height="50px">
                                                                                                                </asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right" style="width: 100px">Fecha :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaSupervisionETab9" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" TargetControlID="txtFechaSupervisionETab9" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender20" runat="server" TargetControlID="txtFechaSupervisionETab9"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Archivo :</td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:FileUpload ID="FileUploadSupervisionEstudioTab9" runat="server" />
                                                                                                                <asp:ImageButton ID="imgbtnSupervisionEstudioTab9" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " />
                                                                                                                <asp:LinkButton ID="LnkBtnSupervisionEstudioTab9" runat="server" OnClick="LnkBtnSupervisionEstudioTab9_OnClick"></asp:LinkButton>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomUsuarioSupervisionEstudio" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarSupervisionEstudioTab9" runat="server" Text="Guardar" OnClick="btnGuardarSupervisionEstudioTab9_OnClick" />
                                                                                                                <asp:Button ID="btnModificarSupervisionEstudioTab9" runat="server" Text="Modificar" Visible="false" OnClick="btnModificarSupervisionEstudioTab9_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarSupervisionEstudioTab9" runat="server" OnClick="btnCancelarSupervisionEstudioTab9_OnClick" Text="Cancelar" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" style="padding-left: 80px">
                                                                                <b>2) Revisión del Estudio de Pre Inversión </b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img alt="" src="linea.png" width="98%" height="10px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:GridView runat="server" ID="grdRevisionEstudioTab9"
                                                                                                EmptyDataText="No hay registro de Revisión de Estudio"
                                                                                                ShowHeaderWhenEmpty="True"
                                                                                                AutoGenerateColumns="False"
                                                                                                RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2"
                                                                                                DataKeyNames="id_aprobacion"
                                                                                                OnRowDataBound="grdRevisionEstudioTab9_OnRowDataBound"
                                                                                                Width="800px"
                                                                                                OnSelectedIndexChanged="grdRevisionEstudioTab9_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdRevisionEstudioTab9_OnRowCommand">

                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIdAprobacion" Text='<%# Eval("id_aprobacion") %>' runat="server"></asp:Label>
                                                                                                            <asp:Label ID="lblIdTipoDocumentoAprobacion" Text='<%# Eval("id_tipoDocumentoAprobacion") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="110px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="TIPO DOCUMENTOS APROBACIÓN" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTipoDocumento" Text='<%# Eval("tipoDocumento") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="DETALLE DOCUMENTO" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblDetalleDocumento" Text='<%# Eval("detalleDocumento") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="FECHA" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha" Text='<%# Eval("vFecha") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="100px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocTab9" runat="server" OnClick="imgDocTab9_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtn_AgregarRevisionEstudioTab9" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';" Height="30px"
                                                                                                AlternateText="Agregar" ToolTip="Agregar Transferencias" OnClick="imgbtn_AgregarRevisionEstudioTab9_OnClick" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:Panel ID="Panel_AgregarRevisionEstudioTab9" Visible="false" runat="server" Width="100%">

                                                                                                <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                                                    <table class="tablaRegistro" cellpadding="2" width="94%">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center"><b>REGISTRO DE REVISIÓN DEL ESTUDIO</b><asp:Label ID="lblRegistroRevisionE" runat="server" Visible="false"></asp:Label></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <tr>
                                                                                                                <td colspan="2" style="height: 10px"></td>
                                                                                                            </tr>
                                                                                                            <td align="right">Detalle de Documento :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtDetalleDocumento" runat="server" TextMode="MultiLine" Width="350px" Height="50px">
                                                                                                                </asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="right" style="width: 100px">Fecha :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaRevisionETab9" runat="server" Width="80px" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" TargetControlID="txtFechaRevisionETab9" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender19" runat="server" TargetControlID="txtFechaRevisionETab9"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right" style="width: 100px">Tipo de Documento :</td>
                                                                                                            <td align="left">
                                                                                                                <asp:DropDownList ID="ddlTipoDocumentoTab9" runat="server">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Archivo :</td>
                                                                                                            <td align="left" colspan="3">
                                                                                                                <asp:FileUpload ID="FileUploadRevisionEstudioTab9" runat="server" />
                                                                                                                <asp:ImageButton ID="imgbtnRevisionEstudioTab9" runat="server" ImageUrl="~/img/blanco.png" AlternateText=" " />
                                                                                                                <asp:LinkButton ID="LnkBtnRevisionEstudioTab9" runat="server" OnClick="LnkBtnRevisionEstudioTab9_OnClick"></asp:LinkButton>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="left" style="padding-left: 30px;">
                                                                                                                <asp:Label ID="lblNomUsuarioRevisionEstudio" runat="server" Font-Size="10px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarRevisionEstudioTab9" runat="server" Text="Guardar" OnClick="btnGuardarRevisionEstudioTab9_OnClick" />
                                                                                                                <asp:Button ID="btnModificarRevisionEstudioTab9" runat="server" Text="Modificar" Visible="false" OnClick="btnModificarRevisionEstudioTab9_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarRevisionEstudioTab9" runat="server" OnClick="btnCancelarRevisionEstudioTab9_OnClick" Text="Cancelar" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="height: 10px"></td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>

                                                <asp:PostBackTrigger ControlID="btnGuardarSupervisionEstudioTab9" />
                                                <asp:PostBackTrigger ControlID="btnModificarSupervisionEstudioTab9" />
                                                <asp:PostBackTrigger ControlID="LnkBtnSupervisionEstudioTab9" />
                                                <asp:PostBackTrigger ControlID="imgbtnSupervisionEstudioTab9" />

                                                <asp:PostBackTrigger ControlID="btnGuardarRevisionEstudioTab9" />
                                                <asp:PostBackTrigger ControlID="btnModificarRevisionEstudioTab9" />
                                                <asp:PostBackTrigger ControlID="LnkBtnRevisionEstudioTab9" />
                                                <asp:PostBackTrigger ControlID="imgbtnRevisionEstudioTab9" />
                                                <%--<asp:PostBackTrigger ControlID="grdRevisionEstudioTab9" />--%>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanelHistorial" HeaderText="VIII. HISTORIAL" TabIndex="7" Visible="false">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="Label12" runat="server" Text="VIII. HISTORIAL"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="Up_Tab8" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <center>
                                                    <br />
                                                    <table border="0" cellspacing="2" cellpadding="2"
                                                        width="1100px">

                                                        <tr>
                                                            <td>
                                                                <div class="BloqueInfoTotal">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="titulo3" align="left" style="padding-left: 30px">VIII. HISTORIAL
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:GridView runat="server" ID="grdHistorialTab8" EmptyDataText="No hay historial de procesos."
                                                                                                ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                                                                CellPadding="2" CellSpacing="2"
                                                                                                DataKeyNames="id_proyecto_historialMonitoreo"
                                                                                                OnRowDataBound="grdHistorialTab8_OnRowDataBound"
                                                                                                OnSelectedIndexChanged="grdHistorialTab8_OnSelectedIndexChanged"
                                                                                                OnRowCommand="grdHistorialTab8_OnRowCommand">
                                                                                                <Columns>
                                                                                                    <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>
                                                                                                    <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="20px" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:TemplateField HeaderText="Fecha" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblFechaTab8" Text='<%# Eval("fecha") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Comentario" Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblComentarioTab8" Text='<%# Eval("comentario") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" Width="600px" />
                                                                                                    </asp:TemplateField>


                                                                                                    <asp:TemplateField Visible="false">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId_proyectoTab8" Text='<%# Eval("id_proyecto") %>' runat="server"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>


                                                                                                    <asp:TemplateField HeaderText="Doc." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgDocTab8" runat="server" OnClick="imgDocTab8_OnClick" ImageUrl="~/img/blanco.png" AlternateText=" "
                                                                                                                ToolTip='<%# Eval("urlDoc") %>' />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText=" " Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:LinkButton runat="server" ID="lnkbtnVerTab8" OnClick="lnkbtnVerTab8_OnClick">Ver</asp:LinkButton>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>


                                                                                                    <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle CssClass="GridHeader2" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <HeaderStyle Height="15px" />
                                                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <asp:ImageButton ID="imgbtnAgregarHistorialTab8" runat="server" ImageUrl="~/img/add.png"
                                                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                                Height="30px" AlternateText="Agregar" ToolTip="Agregar Cartas Fianzas" OnClick="imgbtnAgregarHistorialTab8_OnClick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <asp:Panel ID="Panel_HistorialTab8" Visible="false" runat="server">
                                                                                                <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                                                                    <table width="90%" class="tablaRegistro">
                                                                                                        <tr>
                                                                                                            <td colspan="4" align="center">
                                                                                                                <b>REGISTRO PARA DETENER PROCESO ACTUAL</b><asp:Label ID="lblId_proyectoHistorial" runat="server" Visible="false"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="right">Fecha :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtFechaTab8" runat="server" Width="70px" MaxLength="10" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender149" runat="server" TargetControlID="txtFechaTab8"
                                                                                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                                                                <asp:CalendarExtender ID="CalendarExtender18" runat="server" TargetControlID="txtFechaTab8"
                                                                                                                    Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                                                            </td>

                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="right">Comentario :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:TextBox ID="txtComentarioTab8" runat="server" Width="600px" MaxLength="750" TextMode="MultiLine" Height="150px"></asp:TextBox>

                                                                                                            </td>

                                                                                                        </tr>


                                                                                                        <tr>
                                                                                                            <td align="right">Archivo :
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <asp:FileUpload ID="FileUploadTab8" runat="server" />
                                                                                                                <asp:ImageButton ID="imgbtnDocTab8" runat="server" ImageUrl="~/img/blanco.png" AlternateText="" OnClick="lnkbtnDocTab8_OnClick" />
                                                                                                                <asp:LinkButton ID="lnkbtnDocTab8" runat="server" OnClick="lnkbtnDocTab8_OnClick"></asp:LinkButton>
                                                                                                            </td>
                                                                                                        </tr>


                                                                                                        <tr>
                                                                                                            <td style="height: 10px"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" colspan="4">
                                                                                                                <asp:Button ID="btnGuardarHistorialTab8" runat="server" Text="Guardar" OnClick="btnGuardarHistorialTab8_OnClick" />
                                                                                                                <asp:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnGuardarHistorialTab8"
                                                                                                                    ConfirmText="¿Está seguro de detener el proceso? Una vez registrado el proceso actual pasara a ser parte del historial." Enabled="True"></asp:ConfirmButtonExtender>
                                                                                                                <asp:Button ID="btnModificarHistorialTab8" runat="server" Text="Modificar" OnClick="btnModificarHistorialTab8_OnClick" />
                                                                                                                <asp:Button ID="btnCancelarHistorialTab8" runat="server" Text="Cancelar" OnClick="btnCancelarHistorialTab8_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </asp:Panel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br />
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </center>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkbtnDocTab8" />
                                                <asp:PostBackTrigger ControlID="btnGuardarHistorialTab8" />
                                                <asp:PostBackTrigger ControlID="grdHistorialTab8" />
                                                <asp:PostBackTrigger ControlID="imgbtnDocTab8" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                                                                               
                            </asp:TabContainer>
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />


    <%-- Informacion detalla por año--%>
    <asp:LinkButton ID="LinkButtonDetalleSOSEMTab0" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="MPE_DetalleSOSEMTab0" runat="server" TargetControlID="LinkButtonDetalleSOSEMTab0"
        BackgroundCssClass="modalBackground" OkControlID="imgbtnCerrarDetalleSosemTab0" DropShadow="true"
        PopupControlID="PanelDetalleSOSEM">
    </asp:ModalPopupExtender>
    <asp:Panel ID="PanelDetalleSOSEM" runat="server" Visible="true" Height="330px" Width="1100px"
        CssClass="modalPopup">
        <table width="100%" style="background-color: #E2ECF3">
            <tr>
                <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label21" CssClass="titulo2" runat="server" Font-Bold="true">INFORMACIÓN DETALLA POR MES</asp:Label>
                </td>
                <td align="right" style="width: 26px">
                    <asp:ImageButton ID="imgbtnCerrarDetalleSosemTab0" runat="server" AlternateText="Cerrar ventana"
                        ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                        Height="25px" Width="26px" />
                </td>

            </tr>

        </table>
        <center>
            <br />
            <asp:Panel ID="Panel8" runat="server" Height="280px" Width="1080px" ScrollBars="Horizontal">
                <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:GridView runat="server" ID="grdDetalleSOSEMTab0" EmptyDataText="No hay registro."
                            ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                            CellPadding="2" CellSpacing="2" Width="98%" Font-Size="12px">
                            <Columns>


                                <asp:TemplateField HeaderText="Año" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntidad" Text='<%# Eval("anio") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>

                                <asp:BoundField DataField="pia" HeaderText="Pia" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="pimAcumulado" HeaderText="Pim. Acumulado" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="devAcumulado" HeaderText="Dev. Acum." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="ene" HeaderText="ENE" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="feb" HeaderText="FEB" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="mar" HeaderText="MAR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="abr" HeaderText="ABR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="may" HeaderText="MAY" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="jun" HeaderText="JUN" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="jul" HeaderText="JUL" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="ago" HeaderText="AGO" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="sep" HeaderText="SET" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="oct" HeaderText="OCT" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="nov" HeaderText="NOV" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="dic" HeaderText="DIC" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="compromisoAnual" HeaderText="Compromiso Anual" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>


                                <asp:TemplateField Visible="True" HeaderText="Actualizado al">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecha_Update" Text='<%# Eval("strFechaUpdate") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle Height="15px" />
                            <EditRowStyle BackColor="#FFFFB7" />
                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <br />
        </center>
    </asp:Panel>

    <%-- Informacion segun fuente de financiamiento--%>
    <asp:LinkButton ID="lnkbtnFuenteFinanciamientoTab0" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="MPE_FuenteFinanciamientoTab0" runat="server" TargetControlID="lnkbtnFuenteFinanciamientoTab0"
        BackgroundCssClass="modalBackground" OkControlID="imgbtnCerrarFuenteTab0" DropShadow="true"
        PopupControlID="Panel_FuenteFinanciamientoTab0">
    </asp:ModalPopupExtender>
    <asp:Panel ID="Panel_FuenteFinanciamientoTab0" runat="server" Visible="true" Height="330px" Width="650px"
        CssClass="modalPopup">
        <table width="100%" style="background-color: #E2ECF3">
            <tr>
                <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label22" CssClass="titulo2" runat="server" Font-Bold="true">INFORMACIÓN SEGÚN FUENTE DE FINANCIAMIENTO</asp:Label>
                </td>
                <td align="right" style="width: 26px">
                    <asp:ImageButton ID="imgbtnCerrarFuenteTab0" runat="server" AlternateText="Cerrar ventana"
                        ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                        Height="25px" Width="26px" />
                </td>
            </tr>

        </table>
        <center>
            <br />

            <asp:Panel ID="Panel9" runat="server" Height="280px" Width="630px" ScrollBars="Horizontal">
                <asp:UpdatePanel runat="server" ID="Up_FuenteFinanciamientoTAb0" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:GridView runat="server" ID="grdFuenteFinanciamientoTab0" EmptyDataText="No hay registro."
                            ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                            CellPadding="2" CellSpacing="2" Width="98%" Font-Size="12px">
                            <Columns>

                                <asp:TemplateField HeaderText="Fuente Financiamiento" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntidad" Text='<%# Eval("fuenteFinanciamiento") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Año" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntidad" Text='<%# Eval("anio") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>


                                <asp:BoundField DataField="pimAcumulado" HeaderText="PIM" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="devAcumulado" HeaderText="Dev." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:TemplateField Visible="True" HeaderText="Actualizado al">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecha_Update" Text='<%# Eval("strFechaUpdate") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle Height="15px" />
                            <EditRowStyle BackColor="#FFFFB7" />
                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>

            <br />

        </center>
    </asp:Panel>

    <%// Modal para creacion de nueva etapa -  https://trello.com/c/uqVY2SZM/41-obras %>
    <div class="modal fade" id="newProjModal" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Saldo</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="slTipoFinanc">Tipo Financiamiento</label>
                        <select class="form-control" id="slTipoFinanc" name="slTipoFinanc" runat="server">
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="chkGenEtapa" value="0" style="display: none;" />
                        </label>
                    </div>
                    <div class="form-group" id="divEtapaNro">
                        <%--<label for="txtNumEtapa">Etapa Nro. </label>--%>
                        <input type="text" class="form-control" id="txtNumEtapa" name="txtNumEtapa" value="" style="display: none;" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="button" class="btn btn-primary" id="btnValidEtapa" value="Guardar" />
                </div>
            </div>
        </div>
    </div>

    <%-- Editar Unidad Ejecutora--%>
    <div class="modal fade" id="modalEditarUE" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;MODIFICAR UNIDAD EJECUTORA</h4>
                </div>
                <div class="modal-body">
                    <table style="margin: auto">
                        <tr>
                            <td>UNIDAD EJECUTORA :</td>
                            <td>
                                <asp:TextBox ID="txtUnidadEjecutora" runat="server" Width="350px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnModificarUE" runat="server" Text="MODIFICAR" OnClick="btnModificarUE_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Registro de Ubigeos--%>
    <div class="modal fade" id="modalUbigeo" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;REGISTRAR UBIGEOS</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" ID="Up_UbigeoTab0" UpdateMode="Conditional">
                        <ContentTemplate>

                            <table cellspacing="2" cellpadding="0" width="90%" style="margin: auto">
                                <tr>
                                    <td>
                                        <asp:GridView runat="server" ID="grdUbicacion" EmptyDataText="No hay registro de agentes."
                                            ShowHeaderWhenEmpty="True"
                                            AutoGenerateColumns="False"
                                            CellPadding="2" CellSpacing="2"
                                            Width="100%" DataKeyNames="id"
                                            OnRowCommand="grdUbicacion_RowCommand">
                                            <Columns>
                                                <asp:CommandField Visible="false" ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderText="Edit.">
                                                    <HeaderStyle CssClass="GridHeader2" Width="25px" />
                                                </asp:CommandField>

                                                <asp:TemplateField HeaderText="Departamento">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomDepa" Text='<%# Eval("depa") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" Height="30px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Provincia">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomProv" Text='<%# Eval("prov") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Distrito">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomDist" Text='<%# Eval("dist") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUbigeo" Text='<%# Eval("codigo") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="CCPP">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomCCPP" Text='<%# Eval("CCPP") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUbigeoCCPP" Text='<%# Eval("ubigeoCCPP") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Elim.">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" Width="24px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomUsuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="GridHeader2" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle Height="15px" />
                                            <EditRowStyle BackColor="#FFFFB7" />
                                            <RowStyle HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </td>
                                    <td style="width: 30px; vertical-align: top">
                                        <asp:ImageButton ID="imgbtnAgregarUbicacion" runat="server" ImageUrl="~/img/add.png"
                                            onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                            AlternateText="Agregar" ToolTip="Agregar" Width="30px" OnClick="imgbtnAgregarUbicacion_Click" />
                                    </td>
                                </tr>

                            </table>
                            <br />
                            <br />
                            <asp:Panel ID="Panel_UbicacionMetasTab0" Visible="False" runat="server" Width="90%">
                                <div style="width: 95%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                    <table style="border-collapse: separate !important; border-spacing: 4px; width: 100%">
                                        <tr>
                                            <td colspan="6" align="center">
                                                <b>REGISTRO DE UBICACIÓN </b>
                                                <asp:Label ID="lblIdUbigeoTab0" runat="server" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px;"></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Departamento :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlDepartamentoTab0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartamentoTab0_SelectedIndexChanged"></asp:DropDownList>
                                            </td>

                                            <td align="right">Provincia :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlProvinciaTab0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProvinciaTab0_SelectedIndexChanged"></asp:DropDownList>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="text-right">Distrito :
                                            </td>
                                            <td class="text-left">
                                                <asp:DropDownList ID="ddlDistritoTab0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDistritoTab0_SelectedIndexChanged"></asp:DropDownList>
                                            </td>
                                            <td class="text-right">CCPP :
                                            </td>
                                            <td class="text-left">
                                                <asp:DropDownList ID="ddlCCPPTab0" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" style="padding-left: 50px; height: 20px;">
                                                <asp:Label ID="lblNomUsuarioUbigeoTab0" runat="server" Font-Size="10px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6">
                                                <asp:Button ID="btnGuardarUbicacionMetasTab0" runat="server" Text="Agregar" OnClick="btnGuardarUbicacionMetasTab0_Click" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnCancelarUbicacionMetasTab0" runat="server" Text="Cancelar" OnClick="btnCancelarUbicacionMetasTab0_Click" CssClass="btn btn-default" />
                                            </td>
                                    </table>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <br />
    <br />

    <div>

        <asp:UpdatePanel runat="server" ID="upultimamodificacion">
            <ContentTemplate>
                <table align="left">
                    <tr style="height: 60px;">
                        <td>Última actualización:</td>
                        <td>
                            <asp:Label runat="server" ID="LblUsuario"></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="LblUltimaModificacion"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <asp:LinkButton ID="BtnRefrescar" runat="server" CssClass="btn btn-info" data-toggle="tooltip" data-placement="top" title="Visualizar la última fecha de modificación" OnClick="BtnRefrescar_Click">
                                    <i class="glyphicon glyphicon-refresh fa-1x"></i>
                            </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnRefrescar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

     <%-- Modal de vigencia vencida --%>
    <div class="modal fade" id="modalVigencia" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">VIGENCIA VENCIDA</h4>
                </div>
                <asp:UpdatePanel runat="server" ID="Up_PanelVigencia" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-body">
                            <div class="row form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Etapa con vigencia vencida</label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList runat="server" ID="ddlFlagVigencia" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFlagVigencia_SelectedIndexChanged">
                                            <asp:ListItem Value="">SELECCIONAR</asp:ListItem>
                                            <asp:ListItem Value="0">NO</asp:ListItem>
                                            <asp:ListItem Value="1">SI</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group" id="divAccionVigencia" runat="server" visible="false">
                                    <label class="col-sm-3 control-label">Acción</label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList runat="server" ID="ddlTipoAccionVigencia" CssClass="form-control">
                                            <asp:ListItem Value="">SELECCIONAR</asp:ListItem>
                                            <asp:ListItem Value="1">Actualización de etapa</asp:ListItem>
                                            <asp:ListItem Value="2">Etapa retrotraida</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                 <div class="form-group" >
                                     <asp:Label runat="server" ID="lblNomActualizaVencimiento" class="col-sm-5 control-label"></asp:Label>
                                     </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button runat="server" ID="btnRegistrarVencimiento" OnClick="btnRegistrarVencimiento_Click" Text="REGISTRAR" Enabled="false" ToolTip="Solo se permite el registro de la última etapa activa." />
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <!-- iFrame Generico-->
    <div class="modal fade" id="modalIframe" role="dialog">
        <div id="modalContent" class="modal-dialog modal-lg" role="document" style="width: 70%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span id="iconoTitulo" class="glyphicon glyphicon-stats"></span>&nbsp;&nbsp;
                        <span id="txtTituloFrame"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <iframe id="IfrmGeneral" runat="server" width="100%" height="650px"></iframe>
                </div>
                <div id="footerFrame"></div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="Server">

    <script src="<%= ResolveClientUrl("~/scripts/jquery-1.10.2.min.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/scripts/jquery.jaTextCounter-1.0.js") %>" type="text/javascript"></script>
    <script type="text/javascript">

        function pageLoad() {

            $(function () {
                $("#<% = txtSituacionTab2.ClientID %>").TextCounter(500);
                $("#<% = txtAccionesPrevistasTab2.ClientID %>").TextCounter(500);
                $("#<% = txtAccionesRealizadasTab2.ClientID %>").TextCounter(500);
                $("#<% = txtDescripcionTab6.ClientID %>").TextCounter(500);

                $("#<% = txtDetalleIncumplimientoTab4.ClientID %>").TextCounter(500);
                $("#<% = txtDetalleDocumento.ClientID %>").TextCounter(500);
                $("#<% = txtDetalleDocumento1.ClientID %>").TextCounter(500);
            })
        }
    </script>

    <script type="text/javascript">


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 11005;

        }

        function HabilitarpopupLlamada() {
            popup = document.getElementById('MPEDiv_Carta');
            popup.style.display = 'block';
            popup2 = document.getElementById('Div_Fondo');
            popup2.style.display = 'block';
        }

    </script>

    <script src="../sources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <% //AGREGANDO js aparte para no tocar los previamente incluidos y que realizan funciones especificas - https://trello.com/c/uqVY2SZM/41-obras %>
    <script type="text/javascript">
        var p = "<%= LblID_PROYECTO.Text %>";
        var u = "<%= LblID_USUARIO.Text %>";

        function loadModalIframe(pTipo, pId1) {
            let pIdProyecto = p;
            let src = "";
            switch (pTipo) {
                //case "fichamitigacionriesgo":
                //    $('#txtTituloFrame').text('GESTIÓN DEL RIESGO')
                //    $('#modalContent').attr('style', 'width:960px');
                //    src = "Ficha_MitigacionRiesgo.aspx?id=" + pIdProyecto + "&idR=" + pId1 + '&t=r';
                //    break;
                //case "Ficha_MitigacionRiesgo_Plantilla":
                //    $('#txtTituloFrame').text('GESTIÓN DEL RIESGO')
                //    $('#iconoTitulo').removeClass('glyphicon-stats').addClass('glyphicon-save');
                //    $('#modalContent').attr('style', 'width:960px');
                //    src = "Ficha_MitigacionRiesgo_Plantilla.aspx?id=" + pIdProyecto + "&idR=0&t=r";
                //    break;
                <%--case "curvaFisica":
                    $('#txtTituloFrame').text('CURVA S')
                    $('#ContentPlaceHolder1_IfrmGeneral').attr('height', '800px');
                    $('#modalContent').attr('style', 'width:94%');

                    src = "<%= ConfigurationSettings.AppSettings["UrlFrontEnd"] %>" + "CurvaAvance/Visor?pIdSistema=1&pIdProyecto=" + pId1;
                    break;--%>
                case "coordenadaProyecto":
                    $('#txtTituloFrame').text('REGISTRAR COORDENADA DEL PROYECTO')
                    $('#iconoTitulo').removeClass('glyphicon-stats').addClass('glyphicon-pencil');
                    $('#modalContent').attr('style', 'width:1260px');
                    src = "<%= ConfigurationSettings.AppSettings["UrlFrontEnd"] %>" + "meta/CoordenadasProyecto/" + "<%=  Request.QueryString["token"]%>" + "/" + pId1 + "?ptipometa=0&petaparegistro=2";

                    var divFooter = $('#footerFrame');
                    divFooter.addClass('modal-footer');
                    divFooter.html("<button type='button' class='btn btn-primary' id='btnGrabarCoor'>Grabar</button><button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>");

                    $('#btnGrabarCoor').on('click', function () {
                        $('#ContentPlaceHolder1_IfrmGeneral')[0].contentWindow.postMessage(
                            {
                                "controller": "meta",
                                "method": "index",
                                "request": "save",
                            }, "*");
                    });
                     $('#modalIframe').on('shown.bs.modal', function () {
                         $('#btnGrabarCoor').prop("disabled", false);
                   });

                    $('#modalIframe').on('hidden.bs.modal', function () {
                         $('#btnGrabarCoor').hide();
                    });

                    window.addEventListener("message", function (event) {
                        var dataJson = event.data;
                        var thisBtn = null;
                        if (dataJson.controller == "meta" && dataJson.method == "coordenadasproyecto") {
                            if (dataJson.request == "search") {
                                switch (dataJson.status) {
                                    case "success":
                                         $('#btnGrabarCoor').show().removeClass('hidden');
                                        break;
                                }
                            } else if (dataJson.request == "save") {
                                switch (dataJson.status) {
                                    case "beforeSend":
                                        thisBtn =  $('#btnGrabarCoor');
                                        var btnCache = thisBtn.html();
                                        thisBtn.html('Procesando <span class="fa fa-spinner fa-spin" aria-hidden="true"></span>');
                                        thisBtn.prop('disabled',true)
                                        break;
                                    case "success":
                                        $('#modalIframe').modal('hide');
                                        break;
                                    case "complete":
                                        thisBtn = $('#btnGrabarCoor');
                                        thisBtn.html(btnCache);
                                        thisBtn.prop('disabled',true)
                                        break;
                                }
                            }
                        }
                    }, false);
                    break;
                case "CompromisosGEI":
                    $('#txtTituloFrame').text('');
                    $('#iconoTitulo').removeClass('glyphicon-stats');
                    $('#modalContent').attr('style', 'width:960px');
                    src = "iframe_RegistrarCND.aspx?id=" + pIdProyecto + '&t=r';
                    break;
                case "FichaDetalleSeace":
                    $('#txtTituloFrame').text('FICHA DE PROCESO DE SELECCIÓN');
                    $('#iconoTitulo').removeClass('glyphicon-stats');
                    $('#modalContent').attr('style', 'width:960px');
                    src = "Seace_Ficha.aspx?id=" + pId1 + '&t=r';
                    break;
                case "CalendarioSeace":
                    $('#txtTituloFrame').text('CROMOGRAMA');
                    $('#iconoTitulo').removeClass('glyphicon-stats');
                    $('#modalContent').removeClass('modal-lg');
                    $('#modalContent').attr('style', 'width:');
                    src = "Seace_Cronograma.aspx?id=" + pId1 + '&t=r';
                    break;
            }
            $("#ContentPlaceHolder1_IfrmGeneral").attr("src", src);
            $('#modalIframe').modal('show');
        }
    </script>
    <script src="../js/registroPreinversion.js" type="text/javascript"></script>

</asp:Content>
