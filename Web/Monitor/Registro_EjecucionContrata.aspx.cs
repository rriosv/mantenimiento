﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Registro_EjecucionContrata : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();

        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_SOSEM _objBLSOSEM = new BL_MON_SOSEM();
        BE_MON_SOSEM _BESOSEM = new BE_MON_SOSEM();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();
        BLUtil _Metodo = new BLUtil();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);

            if (!IsPostBack)
            {
                if (Session["LoginUsuario"] == null)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                    Response.Redirect("~/login.aspx?ms=1");
                }
                BtnRefrescar_Click(null, null);

                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();
                lblID_PERFIL_USUARIO.Text = Session["PerfilUsuario"].ToString();
                lblCOD_SUBSECTOR.Text = Session["CodSubsector"].ToString();
                 

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int idacceso = _objBLProceso.spMON_Acceso(_BEProceso);
                Session["FlagAcceso"] = idacceso;
                lblID_ACCESO.Text = idacceso.ToString();

                if (idacceso == 0)
                {
                    OcultarRegistro();
                }

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    lblNombProy.Text = dt.Rows[0]["nom_proyecto"].ToString();

                    string depa = dt.Rows[0]["depa"].ToString();
                    string prov = dt.Rows[0]["prov"].ToString();
                    string dist = dt.Rows[0]["dist"].ToString();
                    lblSNIP.Text = dt.Rows[0]["cod_snip"].ToString();

                    if (lblSNIP.Text == "0")
                    {
                        trVigencia1.Visible = false;
                        trVigencia2.Visible = false;
                        trVigencia3.Visible = false;
                    }

                    ifrGraficoSosem.Attributes["src"] = "iframe_GraficoSOSEM?snip=" + lblSNIP.Text;

                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    //lblPrograma.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                    LblID_SOLICITUD.Text = dt.Rows[0]["id_solicitudes"].ToString();


                    lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();
                    lblCOD_SUBSECTOR.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                    lblID_SUBPROGRAMA.Text = dt.Rows[0]["id_tipoSubPrograma"].ToString();
                    lblTipoFinanciamiento.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                                      
                    ///
                    lblIdEstadoRegistro.Text = dt.Rows[0]["idEstadoProyecto"].ToString();
                    if (lblIdEstadoRegistro.Text.Equals("3"))
                    {
                        lblEstadoRegistro.Text = "FINALIZADO";
                        btnFinalizarTab3.Text = "Activar";
                    }
                    else
                    {
                        lblEstadoRegistro.Text = "ACTIVADO";
                        btnFinalizarTab3.Text = "Finalizar";
                    }

                }
                CargaTab0(Convert.ToInt32(LblID_PROYECTO.Text));
                CargaTab1();
                CargaTab4();
                CargaTab2();
                CargaTab3();
                
                CargaTab6();
                //CargaTab7();
                //CargaTab8();
                //CargaTab9();


                if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 68)
                {
                    TabPanelProcesoSeleccionTab1.Visible = false;
                    TabPanelEstadoEjecuccionTab2.Visible = false;
                    TabPanelAdicionalTab4.Visible = false;
                    TabPanelLiquidacionTab3.Visible = false;
                    TabPanelDocumentosTab6.Visible = false;
                    //TabPanelConclusionesTab7.Visible = false;
                    //TabPanelHistorialTab8.Visible = false;

                    btnAgregarContrapartidasTab0.Visible = false;

                    imgbtn_agregarSOSEM.Visible = false;

                    grdContrapartidasTab0.Columns[0].Visible = false;
                    grdContrapartidasTab0.Columns[8].Visible = false;

                    grdSosemTab0.Columns[0].Visible = false;
                    grdSosemTab0.Columns[19].Visible = false;


                }

                if (lblCOD_SUBSECTOR.Text != "2") //PMIB
                {
                    grdContrapartidasTab0.Columns[3].Visible = false;
                    grdContrapartidasTab0.Columns[4].Visible = false;
                }


                // COORDINADORES PMIB , PNSU, PNSR, PNVR, PASLC
                if (lblIdEstadoRegistro.Text.Equals("3") && (lblID_PERFIL_USUARIO.Text.Equals("23") || lblID_PERFIL_USUARIO.Text.Equals("24") || lblID_PERFIL_USUARIO.Text.Equals("71") || lblID_PERFIL_USUARIO.Text.Equals("74") || lblID_PERFIL_USUARIO.Text.Equals("76") || LblID_USUARIO.Text.Equals("481")))
                {
                    btnFinalizarTab3.Visible = true;
                }
                else if (lblIdEstadoRegistro.Text.Equals("3"))
                {
                    btnFinalizarTab3.Visible = false;
                }

            }

            //Agregar menu lateral por etapa - https://trello.com/c/uqVY2SZM/41-obras
            BL_MON_Menu oMenu = new BL_MON_Menu();

            List<BE_MON_Menu> ListMenu = new List<BE_MON_Menu>();
            ListMenu = oMenu.paSSP_MON_rEtapas(idProy, 1);
            int currentIndex = 0;
            btnNuevoProy.Visible = false;

            for (int i = 0; i < ListMenu.Count; i++)
            {
                BE_MON_Menu menu = ListMenu[i];
                HtmlGenericControl li = new HtmlGenericControl("li");
                if (menu.idProyecto == idProy)
                {
                    li.Attributes.Add("class", "active");

                    //Colocando tipos de financiamiento - https://trello.com/c/uqVY2SZM/41-obras
                    DataSet dsTipo = new DataSet();
                    dsTipo = _objBLFinanciamiento.paSSP_MON_rTipoFinanciamientos(menu.idTipoFinanciamiento);
                    DataTable dtTipoFinanc = new DataTable();
                    dtTipoFinanc = dsTipo.Tables[0];

                    foreach (DataRow row in dtTipoFinanc.Rows)
                    {
                        slTipoFinanc.Items.Add(new ListItem(row["vNombre"].ToString(), row["idTIpoFinanciamiento"].ToString()));
                    }
                    currentIndex = i;
                }

                menuObra.Controls.Add(li);

                HtmlGenericControl anchor = new HtmlGenericControl("a");

                string aspxPage = "";
                switch (menu.idTipoFinanciamiento)
                {
                    case 1:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_PreInversion.aspx";
                        }
                        else
                        {
                            aspxPage = "Registro_PreInversion_PorTransferencia.aspx";
                        }
                        break;
                    case 2:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_ExpedienteTecnico.aspx";
                        }
                        else
                        {
                            aspxPage = "Registro_ExpedienteTecnico_PorTransferencia.aspx";
                        }
                        break;
                    case 3:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_EjecucionContrata.aspx";
                        }
                        else
                        {
                            if (menu.idModalidadFinanciamiento == 4)
                            { aspxPage = "Registro_NE.aspx"; }
                            else
                            {
                                if (menu.idModalidadFinanciamiento == 2)
                                {
                                    aspxPage = "Registro_Jica.aspx";
                                }
                                else { aspxPage = "Registro_Obra.aspx"; }
                            }
                        }
                        break;
                    default:
                        aspxPage = "Registro_Obra.aspx";
                        break;
                }

                //anchor.Attributes.Add("href", "/Monitor/"+ aspxPage + "?id=" + menu.idProyecto);
                anchor.Attributes.Add("href", "" + aspxPage + "?id=" + menu.idProyecto + "&token=" + Request.QueryString["token"]);
                anchor.InnerText = menu.tipoFinanciamiento;
                li.Controls.Add(anchor);

                if (menu.flagUltimo == 1 && menu.idProyecto == idProy)
                {
                    btnNuevoProy.Disabled = false;
                    btnNuevoProy.Visible = true;
                }
            }

        }

        protected void OcultarRegistro()
        {
            btnAgregarContrapartidasTab0.Visible = false;

            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            imgbtn_agregarSOSEM.Visible = false;

            imgbtnAvanceFisicoTab2.Visible = false;
            imgbtnPlaTab4.Visible = false;
            imgbtnPreTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = false;
            btnGuardarLiquidacionTab3.Visible = false;

            btnGuardarTab1.Visible = false;
            btnCambiarModalidadTab1.Visible = false;
            btnGuardarDirectoTab1.Visible = false;
            btnGuardarInfoTab2.Visible = false;

            grdContrapartidasTab0.Columns[0].Visible = false;
            grdContrapartidasTab0.Columns[6].Visible = false;

            grdSosemTab0.Columns[0].Visible = false;
            grdSosemTab0.Columns[19].Visible = false;

            grdSeguimientoProcesoTab1.Columns[0].Visible = false;
            grdSeguimientoProcesoTab1.Columns[11].Visible = false;
            grdConsultoresTab1.Columns[0].Visible = false;
            grdConsultoresTab1.Columns[5].Visible = false;
            grdSubcontratosTab1.Columns[0].Visible = false;
            grdSubcontratosTab1.Columns[13].Visible = false;
            imgbtnAgregarSubcontratoTab1.Visible = false;

            imgbtn_residenteObraTab2.Visible = false;
            //imgbtn_SupervisorDesignado.Visible = false;

            ImgbtnActualizarInspector.Visible = false;
            grdFianzasTab2.Columns[0].Visible = false;
            grdFianzasTab2.Columns[16].Visible = false;
            imgbtnAgregarFianzasTab4.Visible = false;
            grdAvanceFisicoTab2.Columns[0].Visible = false;
            grdAvanceFisicoTab2.Columns[16].Visible = false;

            imgbtnAvanceFisicoTab2.Visible = false;

            grdAvanceFisicoAdicionalTab2.Columns[0].Visible = false;
            grdAvanceFisicoAdicionalTab2.Columns[20].Visible = false;

            imgbtnAvanceFisicoAdicionalTab2.Visible = false;

            grdEvaluacionTab2.Columns[0].Visible = false;
            grdEvaluacionTab2.Columns[21].Visible = false;
            imgbtnAgregarEvaluacionTab2.Visible = false;

            btnGenerarProgramacionTab2.Visible = false;
            grdProgramacionTab2.Columns[0].Visible = false;

            //btnGuardarCertificacion.Visible = false;

            grdCertificacionTab4.Columns[0].Visible = false;
            grdCertificacionTab4.Columns[5].Visible = false;
            imgbtnAgregarCertificacionTab4.Visible = false;

            grdPlazoTab4.Columns[0].Visible = false;
            grdPlazoTab4.Columns[9].Visible = false;
            imgAgregaPlazoTab4.Visible = false;

            grdPresupuestoTab4.Columns[0].Visible = false;
            grdPresupuestoTab4.Columns[12].Visible = false;

            grdDeductivoTab4.Columns[0].Visible = false;
            grdDeductivoTab4.Columns[13].Visible = false;

            grdAdendasTab4.Columns[0].Visible = false;
            grdAdendasTab4.Columns[8].Visible = false;
            imgbtnAdendaTab4.Visible = false;

            grdPanelTab6.Columns[0].Visible = false;
            grdPanelTab6.Columns[8].Visible = false;
            imgbtnAgregarPanelTab6.Visible = false;

            btnGuardarRecepcionTab3.Visible = false;
            btnGuardarSostenibilidadTab3.Visible = false;

            grdParalizacionTab4.Columns[0].Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = false;

            imgbtnEditarUE.Visible = false;
            imgbtnEditarUbigeo.Visible = false;

            lnkbtnBuscarRUCConsorcioTab1.Visible = false;
            lnkbtnBuscarRUCContratistaTab1.Visible = false;
            lnkbtnBuscarRUCMiembroConsorcioTab1.Visible = false;

            btnGuardarCierreTab3.Visible = false;
            btnFinalizarTab3.Visible = false;

            UPTabContainerDetalles.Update();
        }
             
        [WebMethod]
        public static string GuardarEtapa(string idProyecto, string idUsuario, string idTipoFinanciamiento, string vEtapa, string idEtapa)
        {

            BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
            DataSet ds = new DataSet();
            if (idEtapa == "") idEtapa = "0";
            ds = _objBLFinanciamiento.paSSP_MON_gPROYECTO(Int32.Parse(idProyecto), Int32.Parse(idUsuario), Int32.Parse(idTipoFinanciamiento), Int32.Parse(vEtapa), Int32.Parse(idEtapa));
            DataTable dtResultado = new DataTable();
            dtResultado = ds.Tables[1];
            return dtResultado.Rows[0]["vCod"].ToString();
        }

        protected Boolean validaArchivoFotos(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".bmp" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".jpg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar solo fotos en formato: BMP, JPG, JPEG o PNG.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaArchivo(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        public bool ValidaDecimal(string inValue)
        {
            bool bValid;
            try
            {
                decimal myDT = Convert.ToDecimal(inValue);
                bValid = true;

            }
            catch (Exception e)
            {
                bValid = false;
            }


            return bValid;
        }

        protected void GeneraIcoFile(string lnk, ImageButton imgbtn)
        {
            if (lnk != "")
            {

                string ext = lnk.Substring(lnk.Length - 3, 3);
                ext = ext.ToLower();

                if (ext == "pdf")
                {
                    imgbtn.ImageUrl = "~/img/pdf.gif";
                }

                if (ext == "xls" || ext == "lsx")
                {
                    imgbtn.ImageUrl = "~/img/xls.gif";

                }

                if (ext == "doc" || ext == "ocx")
                {
                    imgbtn.ImageUrl = "~/img/doc.gif";

                }

                if (ext == "png" || ext == "PNG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }
                if (ext == "jpg" || ext == "JPG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "peg" || ext == "PEG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "bmp" || ext == "BMP")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                imgbtn.ToolTip = lnk;
            }
            else
            {
                imgbtn.ToolTip = "";
                imgbtn.ImageUrl = "~/img/blanco.png";
            }

        }

        #region Tab0

        protected void CargaTab0(int idproy)
        {
            CargaTipoAporteTab0();

            _BEFinanciamiento.id_proyecto = idproy;
            _BEFinanciamiento.tipoFinanciamiento = 3;
            //dt = _objBLFinanciamiento.spMON_infoFinanciamiento(_BEFinanciamiento);
            //if (dt.Rows.Count > 0)
            //{
            //    txtMontoSNIPTab0.Text = dt.Rows[0]["montoMVCS"].ToString();
            //}

            CargaContrapartidaTab0();

            CargaSOSEMEjecutorasTab0();
            //CargaFinanSOSEMTab0();

        }

        protected void CargaContrapartidaTab0()
        {
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            _BEFinanciamiento.tipoFinanciamiento = 3;

            grdContrapartidasTab0.DataSource = _objBLFinanciamiento.F_spMON_FinanContrapartida(_BEFinanciamiento);
            grdContrapartidasTab0.DataBind();

            totalMontoGrdContrapartida();


            double val1 = 0;
            if (txtAcumuladogrdContraTab0.Text != "")
            { val1 = Convert.ToDouble(txtAcumuladogrdContraTab0.Text); }

            //double val2 = 0;
            //if (txtMontoSNIPTab0.Text != "")
            //{ val2 = Convert.ToDouble(txtMontoSNIPTab0.Text); }

            //txtTotalInversionTab0.Text = (val1 + val2).ToString("N");


        }

        protected void CargaSOSEMEjecutorasTab0()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSOSEMEjecutoras.DataSource = _objBLFinanciamiento.F_spMON_SosemEjecutora(snip);
            grdSOSEMEjecutoras.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }

        protected void CargaSOSEMDevengadoMensualizadoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdDetalleSOSEMTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemDevengadoMensualizado(snip, idEjecutora);
            grdDetalleSOSEMTab0.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }


        protected void CargaSOSEMFuenteFinanciamientoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdFuenteFinanciamientoTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemFuenteFinanciamiento(snip, idEjecutora);
            grdFuenteFinanciamientoTab0.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }

        protected void grdSosemTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            double total;
            int anio;
            total = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblMonto = (Label)e.Row.FindControl("lblMontoAprob");
                anio = Convert.ToInt32(((Label)e.Row.FindControl("lblEntidad")).Text);


                if (DateTime.Now.Year == anio)
                {
                    total = Convert.ToDouble(lblValorAvance.Text) + Convert.ToDouble(Server.HtmlDecode(e.Row.Cells[4].Text));

                }
                else
                {
                    total = Convert.ToDouble(lblValorAvance.Text) + Convert.ToDouble(Server.HtmlDecode(e.Row.Cells[5].Text));

                }

                lblValorAvance.Text = total.ToString("N");

                //if (Convert.ToDouble(txtTotalInversionTab0.Text) < total)
                //{
                //    lblAlertaSosemTab0.Visible = true;
                //}
                //else
                //{
                //    lblAlertaSosemTab0.Visible = false;
                //}



            }
        }
        protected void CargaFinanSOSEMTab0()
        {
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEFinanciamiento.tipoFinanciamiento = 3;

            grdSosemTab0.DataSource = _objBLFinanciamiento.F_spMON_SeguimientoSOSEM(_BEFinanciamiento);
            grdSosemTab0.DataBind();


        }


        protected void CargaTipoAporteTab0()
        {
            ddlTipoAporteTab0.DataSource = _objBLFinanciamiento.F_spMON_TipoAporte();
            ddlTipoAporteTab0.DataTextField = "nombre";
            ddlTipoAporteTab0.DataValueField = "valor";
            ddlTipoAporteTab0.DataBind();

            ddlTipoAporteTab0.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }


        protected void btnAgregarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            btnModificarContrapartidasTab0.Visible = false;
            btnGuardarContrapartidasTab0.Visible = true;
            Panel_ContrapartidaTab0.Visible = true;
            btnAgregarContrapartidasTab0.Visible = false;
            lblNomUsuarioContrapartida.Text = "";
            Up_Tab0.Update();

        }

        protected void totalMontoGrdContrapartida()
        {
            _BEFinanciamiento.tipoGrd = 5;
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            txtAcumuladogrdContraTab0.Text = _objBLFinanciamiento.F_spMON_MontoTotalGrd(_BEFinanciamiento);

            if (txtAcumuladogrdContraTab0.Text != "")
            {
                txtAcumuladogrdContraTab0.Text = Convert.ToDouble(txtAcumuladogrdContraTab0.Text).ToString("N2");
            }
        }

        //protected void totalMontoGrdTransferenciaTab0()
        //{
        //    _BEFinanciamiento.tipoGrd = 6;
        //    _BEFinanciamiento.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
        //    dt = _objBLFinanciamiento.spMON_MontoTotalGrd(_BEFinanciamiento);
        //    if (dt.Rows.Count > 0)
        //    {
        //        txtAcumuladorTranferenciaTab0.Text = dt.Rows[0]["total"].ToString();
        //    }
        //}

        protected void btnGuardarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarContrapartidaTab0())
            {

                _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEFinanciamiento.tipoFinanciamiento = 3;
                _BEFinanciamiento.entidad_cooperante = txtEntidadCooperanteTab0.Text;
                _BEFinanciamiento.documento = txtDocumentoTab0.Text;
                _BEFinanciamiento.monto = txtMontoTab0.Text;
                _BEFinanciamiento.id_tipo_aporte = Convert.ToInt32(ddlTipoAporteTab0.SelectedValue.ToString());
                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEFinanciamiento.MontoObra = txtMontoObraTab0.Text;
                _BEFinanciamiento.MontoSupervision = txtMontoSupervisionTab0.Text;
                int val = _objBLFinanciamiento.spi_MON_FinanContrapartida(_BEFinanciamiento);


                if (val == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaContrapartidaTab0();

                    txtEntidadCooperanteTab0.Text = "";
                    txtDocumentoTab0.Text = "";
                    txtMontoTab0.Text = "";
                    ddlTipoAporteTab0.SelectedValue = "";
                    txtMontoObraTab0.Text = "";
                    txtMontoSupervisionTab0.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                Panel_ContrapartidaTab0.Visible = false;
                btnAgregarContrapartidasTab0.Visible = true;
                Up_Tab0.Update();
            }
        }

        protected void btnCancelarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            Panel_ContrapartidaTab0.Visible = false;
            btnAgregarContrapartidasTab0.Visible = true;
            Up_Tab0.Update();

        }



        protected Boolean ValidarContrapartidaTab0()
        {
            Boolean result;
            result = true;

            if (txtEntidadCooperanteTab0.Text == "")
            {
                string script = "<script>alert('Ingrese contrapartida.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtDocumentoTab0.Text == "")
            //{
            //    string script = "<script>alert('Ingrese documento.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (ddlTipoAporteTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de aporte.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;

        }

        protected Boolean ValidarSOSEMTab0()
        {
            Boolean result;
            result = true;

            if (ddlanioSOSEMTAb0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Año');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;

        }


        protected void btnActualizarRegistroSOSEMTab0_OnClick(object sender, EventArgs e)
        {

            if (ValidarSOSEMTab0())
            {

                _BESOSEM.id_seguimiento_sosem = Convert.ToInt32(lblRegistroObras.Text);
                //_BEFinanciamiento.tipo = 1;

                _BESOSEM.anio = ddlanioSOSEMTAb0.SelectedValue;
                _BESOSEM.pia = txtPiaSOSEMTab0.Text;
                _BESOSEM.pimAcumulado = txtPimAcumTab0.Text;
                _BESOSEM.devAcumulado = txtDevAcumTab0.Text;
                _BESOSEM.ene = txtEneSosTab0.Text;
                _BESOSEM.feb = txtFebSosTab0.Text;
                _BESOSEM.mar = txtMarSosTab0.Text;
                _BESOSEM.abr = txtAbrSosTab0.Text;
                _BESOSEM.may = txtMaySosTab0.Text;
                _BESOSEM.jun = txtJunSosTab0.Text;
                _BESOSEM.jul = txtJulSosTab0.Text;
                _BESOSEM.ago = txtAgoSosTab0.Text;
                _BESOSEM.sep = txtSepSosTab0.Text;
                _BESOSEM.oct = txtOctSosTab0.Text;
                _BESOSEM.nov = txtNovSosTab0.Text;
                _BESOSEM.dic = txtDicSosTab0.Text;
                _BESOSEM.compromisoAnual = txtComproAnualTab0.Text;
                _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLSOSEM.spud_MON_SOSEM_EDITAR(_BESOSEM);

                if (val == 1)
                {
                    string script = "<script>alert('Actualización Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaFinanSOSEMTab0();
                    ddlanioSOSEMTAb0.SelectedValue = "";
                    txtPiaSOSEMTab0.Text = "";
                    txtPimAcumTab0.Text = "";
                    txtDevAcumTab0.Text = "";
                    txtEneSosTab0.Text = "";
                    txtFebSosTab0.Text = "";
                    txtMarSosTab0.Text = "";
                    txtAbrSosTab0.Text = "";
                    txtMaySosTab0.Text = "";
                    txtJunSosTab0.Text = "";
                    txtJulSosTab0.Text = "";
                    txtAgoSosTab0.Text = "";
                    txtSepSosTab0.Text = "";
                    txtOctSosTab0.Text = "";
                    txtNovSosTab0.Text = "";
                    txtDicSosTab0.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSOSEM.Visible = false;
                imgbtn_agregarSOSEM.Visible = true;
                Up_Tab0.Update();
            }



        }

        protected void LnkbtnCartaTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnCartaTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnAvanceTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnAvanceTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnAvanceAdicionalTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void LnkbtnSubContrato_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSubContrato.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnSeguimientoTAb1_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSeguimientoTAb1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtn_agregarSOSEM_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSOSEM.Visible = true;
            imgbtn_agregarSOSEM.Visible = false;
            lblNomUsuarioSosem.Text = "";

            ddlanioSOSEMTAb0.SelectedValue = "";
            txtPiaSOSEMTab0.Text = "0";
            txtPimAcumTab0.Text = "0";
            txtDevAcumTab0.Text = "0";
            txtEneSosTab0.Text = "0";
            txtFebSosTab0.Text = "0";
            txtMarSosTab0.Text = "0";
            txtAbrSosTab0.Text = "0";
            txtMaySosTab0.Text = "0";
            txtJunSosTab0.Text = "0";
            txtJulSosTab0.Text = "0";
            txtAgoSosTab0.Text = "0";
            txtSepSosTab0.Text = "0";
            txtOctSosTab0.Text = "0";
            txtNovSosTab0.Text = "0";
            txtDicSosTab0.Text = "0";

            btnGuardarRegistroSOSEMTab0.Visible = true;
            btnActualizarRegistroSOSEMTab0.Visible = false;
            Up_Tab0.Update();
        }

        protected void btnCancelarRegistroSOSEM_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSOSEM.Visible = false;
            imgbtn_agregarSOSEM.Visible = true;
            Up_Tab0.Update();

        }

        //protected void imgbtn_AgregarSeguimientoTab0_OnClick(object sender, EventArgs e)
        //{
        //    Panel_AgregarSOSEM.Visible = true;
        //    imgbtn_agregarSOSEM.Visible = false;
        //    Up_Tab0.Update();
        //}

        protected void btnGuardarRegistroSOSEMTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarSOSEMTab0())
            {
                _BESOSEM.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BESOSEM.tipoFinanciamiento = 3;

                _BESOSEM.anio = ddlanioSOSEMTAb0.SelectedValue;
                _BESOSEM.pia = txtPiaSOSEMTab0.Text;
                _BESOSEM.pimAcumulado = txtPimAcumTab0.Text;
                _BESOSEM.devAcumulado = txtDevAcumTab0.Text;
                _BESOSEM.ene = txtEneSosTab0.Text;
                _BESOSEM.feb = txtFebSosTab0.Text;
                _BESOSEM.mar = txtMarSosTab0.Text;
                _BESOSEM.abr = txtAbrSosTab0.Text;
                _BESOSEM.may = txtMaySosTab0.Text;
                _BESOSEM.jun = txtJunSosTab0.Text;
                _BESOSEM.jul = txtJulSosTab0.Text;
                _BESOSEM.ago = txtAgoSosTab0.Text;
                _BESOSEM.sep = txtSepSosTab0.Text;
                _BESOSEM.oct = txtOctSosTab0.Text;
                _BESOSEM.nov = txtNovSosTab0.Text;
                _BESOSEM.dic = txtDicSosTab0.Text;
                _BESOSEM.compromisoAnual = txtComproAnualTab0.Text;

                _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLSOSEM.spi_MON_FinanSOSEM(_BESOSEM);

                if (val == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaFinanSOSEMTab0();
                    ddlanioSOSEMTAb0.SelectedValue = "";
                    txtPiaSOSEMTab0.Text = "0";
                    txtPimAcumTab0.Text = "0";
                    txtDevAcumTab0.Text = "0";
                    txtEneSosTab0.Text = "0";
                    txtFebSosTab0.Text = "0";
                    txtMarSosTab0.Text = "0";
                    txtAbrSosTab0.Text = "0";
                    txtMaySosTab0.Text = "0";
                    txtJunSosTab0.Text = "0";
                    txtJulSosTab0.Text = "0";
                    txtAgoSosTab0.Text = "0";
                    txtSepSosTab0.Text = "0";
                    txtOctSosTab0.Text = "0";
                    txtNovSosTab0.Text = "0";
                    txtDicSosTab0.Text = "0";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSOSEM.Visible = false;
                imgbtn_agregarSOSEM.Visible = true;
                Up_Tab0.Update();
            }

        }

        protected void grdContrapartidasTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_financiamiento_contrapartida;
            id_financiamiento_contrapartida = grdContrapartidasTab0.SelectedDataKey.Value.ToString();
            lblIDTipoContrapartida.Text = id_financiamiento_contrapartida.ToString();
            GridViewRow row = grdContrapartidasTab0.SelectedRow;
            txtEntidadCooperanteTab0.Text = ((Label)row.FindControl("lblEntidad")).Text;
            txtDocumentoTab0.Text = ((Label)row.FindControl("lblDocumento")).Text;
            txtMontoTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
            ddlTipoAporteTab0.SelectedValue = ((Label)row.FindControl("lblId_TipoAporte")).Text;
            ddlTipoAporteTab0.SelectedValue = ((Label)row.FindControl("lblId_TipoAporte")).Text;
            txtMontoObraTab0.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtMontoSupervisionTab0.Text = Server.HtmlDecode(row.Cells[4].Text);


            lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnModificarContrapartidasTab0.Visible = true;
            btnGuardarContrapartidasTab0.Visible = false;
            Panel_ContrapartidaTab0.Visible = true;
            btnAgregarContrapartidasTab0.Visible = false;
            Up_Tab0.Update();


        }



        protected void grdSosemTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento_sosem;
            id_seguimiento_sosem = grdSosemTab0.SelectedDataKey.Value.ToString();
            lblRegistroObras.Text = id_seguimiento_sosem.ToString();
            GridViewRow row = grdSosemTab0.SelectedRow;
            ddlanioSOSEMTAb0.SelectedValue = ((Label)row.FindControl("lblEntidad")).Text;
            txtPiaSOSEMTab0.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtPimAcumTab0.Text = Server.HtmlDecode(row.Cells[4].Text);
            txtDevAcumTab0.Text = Server.HtmlDecode(row.Cells[5].Text);
            txtEneSosTab0.Text = Server.HtmlDecode(row.Cells[6].Text);
            txtFebSosTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtMarSosTab0.Text = Server.HtmlDecode(row.Cells[8].Text);
            txtAbrSosTab0.Text = Server.HtmlDecode(row.Cells[9].Text);
            txtMaySosTab0.Text = Server.HtmlDecode(row.Cells[10].Text);
            txtJunSosTab0.Text = Server.HtmlDecode(row.Cells[11].Text);
            txtJulSosTab0.Text = Server.HtmlDecode(row.Cells[12].Text);
            txtAgoSosTab0.Text = Server.HtmlDecode(row.Cells[13].Text);
            txtSepSosTab0.Text = Server.HtmlDecode(row.Cells[14].Text);
            txtOctSosTab0.Text = Server.HtmlDecode(row.Cells[15].Text);
            txtNovSosTab0.Text = Server.HtmlDecode(row.Cells[16].Text);
            txtDicSosTab0.Text = Server.HtmlDecode(row.Cells[17].Text);
            txtComproAnualTab0.Text = Server.HtmlDecode(row.Cells[18].Text);

            lblNomUsuarioSosem.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnGuardarRegistroSOSEMTab0.Visible = false;
            btnActualizarRegistroSOSEMTab0.Visible = true;
            imgbtn_agregarSOSEM.Visible = false;

            Panel_AgregarSOSEM.Visible = true;
            Up_Tab0.Update();


        }

        protected void grdSeguimientoProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdSeguimientoProcesoTab1.SelectedDataKey.Value.ToString();
            lblRegistroSeguimientoP.Text = id_seguimiento.ToString();
            GridViewRow row = grdSeguimientoProcesoTab1.SelectedRow;
            TxtConvocatoriaTab1.Text = ((Label)row.FindControl("lblConvocatoria")).Text;
            txtFechaPubliTab1.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoAdjudicacionTab1.Text = ((Label)row.FindControl("lblTipoAdjudicacion")).Text;
            ddlResultadoTab1.SelectedValue = ((Label)row.FindControl("lblIDResultado")).Text;
            txtObservacion.Text = ((Label)row.FindControl("lblobservacionTab1")).Text;
            LnkbtnSeguimientoTAb1.Text = ((ImageButton)row.FindControl("imgDocTab1")).ToolTip;
            lblNomUsuarioSeguimientoProceso.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnSeguimientoTAb1.Text, imgbtnSeguimientoTAb1);



            cargaSeguimientoProcesoTab1();
            Up_Tab1.Update();
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            btnModificarSeguimientoProcesoTab1.Visible = true;
            btnGuardarSeguimientoProcesoTab1.Visible = false;

        }

        protected void grdSubcontratosTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_subContratos;
            id_subContratos = grdSubcontratosTab1.SelectedDataKey.Value.ToString();
            lblId_SubContratos.Text = id_subContratos.ToString();
            GridViewRow row = grdSubcontratosTab1.SelectedRow;

            ddlRubroTab1.Text = ((Label)row.FindControl("lblId_Rubros")).Text;
            txtnombreTab1.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtMontoTab1.Text = Convert.ToDouble(((Label)row.FindControl("lblmonto")).Text).ToString("N");
            txtPlazoTab1.Text = ((Label)row.FindControl("lblPlazo")).Text;
            ddlModalidaSubcontratoTab1.Text = ((Label)row.FindControl("lblId_Modalidad")).Text;
            LnkbtnSubContrato.Text = ((ImageButton)row.FindControl("imgDocSubContratoTab1")).ToolTip;

            GeneraIcoFile(LnkbtnSubContrato.Text, imgbtnSubContrato);


            Panel_SubcontratosTab1.Visible = true;
            btnGuardarSubcontratosTab1.Visible = false;
            btnModificarSubcontratosTab1.Visible = true;
            imgbtnAgregarSubcontratoTab1.Visible = false;

        }


        //protected void grdResponsableTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id_responsable;
        //    id_responsable = grdResponsableTab1.SelectedDataKey.Value.ToString();
        //    lblId_Responsable_Obra.Text = id_responsable.ToString();
        //    GridViewRow row = grdResponsableTab1.SelectedRow;
        //    txtCargoTab1.Text = ((Label)row.FindControl("lblcargo")).Text;
        //    txtResolucionTab1.Text = ((Label)row.FindControl("lblResolucion")).Text;
        //    txtNombreResponsableTab1.Text = ((Label)row.FindControl("lblNombre")).Text;
        //    txtCipTab1.Text = ((Label)row.FindControl("lblCip")).Text;
        //    ddlModalidadResponsableTab1.Text = ((Label)row.FindControl("lblId_Modalidad_Responsable")).Text;
        //    LnkbtnResponsableObraTab1c.Text = ((ImageButton)row.FindControl("imgDocResponsableTab1")).ToolTip;

        //    GeneraIcoFile(LnkbtnResponsableObraTab1c.Text, imgbtnResponsableObraTab1);

        //    Panel_ResponsableTab1.Visible = true;
        //    btnGuardarResponsableObraTab1.Visible = false;
        //    btnModificarResponsableObraTab1.Visible = true;
        //    imgbtnAgregarResponsableTab1.Visible = false;
        //}

        protected void grdConsultoresTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_grupo_consorcio;
            id_grupo_consorcio = grdConsultoresTab1.SelectedDataKey.Value.ToString();
            lblId_Registro_Contratista.Text = id_grupo_consorcio.ToString();
            GridViewRow row = grdConsultoresTab1.SelectedRow;
            txtContratistaGrupTab1.Text = ((Label)row.FindControl("lblNombConsultor")).Text;
            txtRucGrupTab1.Text = ((Label)row.FindControl("lblRuc")).Text;
            txtRepresentanGrupTAb1.Text = ((Label)row.FindControl("lblRepresentante")).Text;
            Panel_AgregarConsultorTab1.Visible = true;
            btn_guardarConsultor.Visible = false;
            btn_modificarConsultor.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
        }

        protected void grdFianzasTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_carta_fianza;
            id_carta_fianza = grdFianzasTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_Carta_Fianza.Text = id_carta_fianza.ToString();
            GridViewRow row = grdFianzasTab2.SelectedRow;
            txtEntidadFinancieraTab2.Text = ((Label)row.FindControl("lblEntidadFinanciera")).Text;
            txtNroCartaF.Text = ((Label)row.FindControl("lblNroCarta")).Text;
            ddlTipoTab2.Text = ((Label)row.FindControl("lblIdTipo")).Text;
            txtFechaInicioVigenciaTab2.Text = ((Label)row.FindControl("lblFechaInicio")).Text;

            txtFechaFinVigenciaTab2.Text = ((Label)row.FindControl("lblFechaFin")).Text;
            txtmontoFianzaTab2.Text = ((Label)row.FindControl("lblmontoTab2")).Text;
            if (txtmontoFianzaTab2.Text != "")
            {
                txtmontoFianzaTab2.Text = Convert.ToDouble(txtmontoFianzaTab2.Text).ToString("N");
            }

            ddlTipoAccion.SelectedValue = ((Label)row.FindControl("lblIdTipoAccion")).Text;
            //txtmontoFianzaTab2.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtPorcentajeTab2.Text = ((Label)row.FindControl("lblPorcentaje")).Text;
            chkVerificado.Checked = ((CheckBox)row.FindControl("chb_flagVerifico")).Checked;
            //chkRenovado.Checked = ((CheckBox)row.FindControl("chb_flagRenueva")).Checked;
            txtFechaRenovacion.Text = ((Label)row.FindControl("lblRenovacion")).Text;
            lnkbtnAccionTab2.Text = ((ImageButton)row.FindControl("imgDocAccionGriTab2")).ToolTip;
            GeneraIcoFile(lnkbtnAccionTab2.Text, imgbtnAccionTab2);
            LnkbtnCartaTab2.Text = ((ImageButton)row.FindControl("imgDocCartaTab2")).ToolTip;
            GeneraIcoFile(LnkbtnCartaTab2.Text, imgbtnCartaTab2);

            lblNomUsuarioCarta.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            if (ddlTipoAccion.SelectedValue.ToString().Equals("2"))
            {
                lblTitDocAccion.Text = "Fecha de Renovación";
                txtFechaRenovacion.Visible = true;
                FileUploadAccionTab2.Visible = false;
            }
            else if (ddlTipoAccion.SelectedValue.ToString().Equals("3") || ddlTipoAccion.SelectedValue.ToString().Equals("4"))
            {
                lblTitDocAccion.Text = "Doc. " + ddlTipoAccion.SelectedItem.Text;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                FileUploadAccionTab2.Visible = true;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else
            {
                lblTitDocAccion.Text = "";
                FileUploadAccionTab2.Visible = false;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }


            Panel_fianzas.Visible = true;
            imgbtnAgregarFianzasTab4.Visible = false;

            btnGuardarFianzasTab2.Visible = false;
            btnModificarFianzasTab2.Visible = true;
            Up_Tab2.Update();

        }




        protected void btnModificarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarContrapartidaTab0())
            {

                _BEFinanciamiento.id_financiamientoContrapartida = Convert.ToInt32(lblIDTipoContrapartida.Text);
                _BEFinanciamiento.tipo = 1;
                _BEFinanciamiento.entidad_cooperante = txtEntidadCooperanteTab0.Text;
                _BEFinanciamiento.documento = txtDocumentoTab0.Text;
                _BEFinanciamiento.monto = txtMontoTab0.Text;
                _BEFinanciamiento.id_tipo_aporte = Convert.ToInt32(ddlTipoAporteTab0.SelectedValue.ToString());
                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEFinanciamiento.MontoObra = txtMontoObraTab0.Text;
                _BEFinanciamiento.MontoSupervision = txtMontoSupervisionTab0.Text;


                int val = _objBLFinanciamiento.spud_MON_FinanContrapartida(_BEFinanciamiento);

                if (val == 1)
                {
                    string script = "<script>alert('Actualización Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaContrapartidaTab0();

                    txtEntidadCooperanteTab0.Text = "";
                    txtDocumentoTab0.Text = "";
                    txtMontoTab0.Text = "";
                    ddlTipoAporteTab0.SelectedValue = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                Panel_ContrapartidaTab0.Visible = false;
                btnAgregarContrapartidasTab0.Visible = true;
                Up_Tab0.Update();
            }
        }

        protected void grdContrapartidasTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdContrapartidasTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEFinanciamiento.id_financiamientoContrapartida = Convert.ToInt32(objTemp.ToString());
                _BEFinanciamiento.tipo = 2;
                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLFinanciamiento.spud_MON_FinanContrapartida(_BEFinanciamiento);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaContrapartidaTab0();
                    Up_Tab0.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdSosemTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSosemTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BESOSEM.id_seguimiento_sosem = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BESOSEM.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLSOSEM.spud_MON_SOSEM_ELIMINAR(_BESOSEM);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    CargaFinanSOSEMTab0();
                    Up_Tab0.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }




        protected void grdAvanceFisicoTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_AvanceFisico_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdAvanceFisicoAdicionalTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoAdicionalTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.D_spd_MON_InformeAvanceAdicionalPorContrata(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdFianzasTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdFianzasTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_carta_fianza = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_Seguimiento_Carta_Fianza_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaCartasFianzasTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }



        #endregion

        #region Tab1


        //CAMBIOS

        //protected void CargaOBRAEjecutorasTab0SEACEObra()
        //{
        //    int snip = Convert.ToInt32(lblSNIP.Text);

        //    grdSEACEObra.DataSource = _objBLFinanciamiento.spMON_SEACE_Obra(snip, Convert.ToInt32(LblID_PROYECTO.Text));
        //    grdSEACEObra.DataBind();
        //    //totalMontoGrdTransferenciaTab0();
        //}

        //protected void CargaOBRAEjecutorasTab0SEACEConsultoria()
        //{
        //    int snip = Convert.ToInt32(lblSNIP.Text);

        //    grdSEACEConsultoria.DataSource = _objBLFinanciamiento.spMON_SEACE_Consultoria(snip);
        //    grdSEACEConsultoria.DataBind();
        //    //totalMontoGrdTransferenciaTab0();
        //}

        //protected void CargaOBRAEjecutorasTab0SEACEBienesServicios()
        //{
        //    int snip = Convert.ToInt32(lblSNIP.Text);

        //    grdSEACEBienesServicios.DataSource = _objBLFinanciamiento.spMON_SEACE_BienesServicios(snip);
        //    grdSEACEBienesServicios.DataBind();
        //    //totalMontoGrdTransferenciaTab0();
        //}


        protected void CargaTab1()
        {
            CargaddlTipoAdjudicacionTab1(ddlAdjudicacionTab1);
            CargaddlTipoAdjudicacionTab1(ddlTipoAdjudicacionTab1);
            cargaDDlTipoContratacionTab1();
            CargaDllModalidadTab1();
            CargaDllModalidadTab1(ddlModalidaSubcontratoTab1);

            CargaListaRubro();
            CargaDllTipoResultado();
            CargaDatosTab1();

            if (Panel_ProcesoIndirectoTab1.Visible == true)
            {
                TabActivoProceso();
            }
        }

        protected void cargaConsultoresTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdConsultoresTab1.DataSource = _objBLProceso.F_spMON_GrupoConsorcio(_BEProceso);
            grdConsultoresTab1.DataBind();


        }

        protected void CargaDatosTab1()
        {
            lblTitSeace.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " (Obligatorio)";
            lnkbtnSeaceAutoPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " POR SEACE";
            lnkbtnSeaceManualPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " DE FORMA MANUAL";

            if (ddlProcesoTab1.SelectedValue == "1" && btnGenerarProgramacionTab2.Enabled == false)
            {
                txtMontoContratadoTab1.Enabled = false;
            }
            else
            {
                txtMontoContratadoTab1.Enabled = true;
            }

            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);
            string modal;
            if (dt.Rows.Count > 0)
            {
                modal = dt.Rows[0]["id_tipoModalidad"].ToString();

                switch (modal)
                {
                    case "":
                        {
                            ddlModalidadTab1.SelectedValue = "5";
                            return;
                        }
                    case "1": //DIRECTA
                        {
                            lblNomActualizaProcesoDirecta.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                            Panel_ProcesoDirectoTab1.Visible = true;
                            btnGuardarDirectoTab1.Visible = true;
                            Panel_ProcesoIndirectoTab1.Visible = false;
                            //cargaSeguimientoProcesoTab1();
                            btnCambiarModalidadTab1.Visible = true;
                            // CargaDatosTab1();
                            cargaSubcontratosTab1();
                            //CargaResponsableTab1();
                            return;
                        }
                    case "5": //INDIRECTA - POR CONTRATA
                        {
                            lblNomActualizaProcesoDirecta.Text = "";
                            ddlAdjudicacionTab1.SelectedValue = dt.Rows[0]["id_tipoAdjudicacion"].ToString();
                            txtNroLicTab1.Text = dt.Rows[0]["NroLic"].ToString();
                            TxtAnio2Tab1.Text = dt.Rows[0]["anio"].ToString();
                            txtDetalleTab1.Text = dt.Rows[0]["detalle"].ToString();
                            txtProTab1.Text = dt.Rows[0]["buenaPro"].ToString();
                            txtProConsentidaTab1.Text = dt.Rows[0]["buenaProConsentida"].ToString();
                            txtValorReferencialTab1.Text = dt.Rows[0]["valorReferencial"].ToString();
                            ddlTipoContratacionTab1.SelectedValue = dt.Rows[0]["id_tipoContratacion"].ToString();
                            txtMontoContratadoTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                            if (txtMontoContratadoTab1.Text.Length > 0)
                            {
                                txtMontoContratadoTab2.Text = txtMontoContratadoTab1.Text;
                            }


                            txtNroContrato.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                            txtFechaFirmaContratoTab1.Text = dt.Rows[0]["fechaFirmaContrato"].ToString();
                            lblNomActualizaProcesoSeleccion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                            // lblNomUsuarioOtraModalidad.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                            string valCheck = dt.Rows[0]["id_tipoEmpresa"].ToString();

                            rbConsultorTab1.Checked = false;
                            rbConsorcioTab1.Checked = false;
                            rbConsorcioTab1.Enabled = true;
                            rbConsultorTab1.Enabled = true;

                            if (valCheck != "")
                            {
                                if (Convert.ToInt32(valCheck) == 1)
                                {
                                    rbConsultorTab1.Checked = true;
                                    rbConsorcioTab1.Checked = false;
                                    //  rbConsorcioTab1.Enabled = false;

                                    lblNomConsultorTab1.Font.Bold = true;
                                    lblNomConsorcioTab1.Font.Bold = false;

                                    Panel_ConsultorTab1.Visible = true;
                                    Panel_ConsorcioTab1.Visible = false;


                                    Up_rbProcesoTab1.Update();

                                    txtNombConsultorTab1.Text = dt.Rows[0]["nombreContratista"].ToString();
                                    txtRucConsultorTab1.Text = dt.Rows[0]["rucContratista"].ToString();
                                    lnkbtnContratoContratistaTab1.Text = dt.Rows[0]["urlContrato"].ToString();
                                    txtRepresentanteLegalConsultorTab1.Text = dt.Rows[0]["representanteContratista"].ToString();
                                    GeneraIcoFile(lnkbtnContratoContratistaTab1.Text, imgbtnContratoContratistaTab1);

                                }
                                if (Convert.ToInt32(valCheck) == 2)
                                {
                                    rbConsorcioTab1.Checked = true;
                                    rbConsultorTab1.Checked = false;
                                    //  rbConsultorTab1.Enabled = false;

                                    lblNomConsorcioTab1.Font.Bold = true;
                                    lblNomConsultorTab1.Font.Bold = false;

                                    Panel_ConsorcioTab1.Visible = true;
                                    Panel_ConsultorTab1.Visible = false;

                                    cargaConsultoresTab1();

                                    Up_rbProcesoTab1.Update();


                                    lnkbtnContratoTab1.Text = dt.Rows[0]["urlContrato"].ToString();

                                    txtNomconsorcioTab1.Text = dt.Rows[0]["nombreConsorcio"].ToString();
                                    txtRucConsorcioTab1.Text = dt.Rows[0]["rucConsorcio"].ToString();
                                    txtTelefonoConsorcioTab1.Text = dt.Rows[0]["telefonoConsorcio"].ToString();
                                    txtRepresentanteTab1.Text = dt.Rows[0]["representateConsorcio"].ToString();

                                    GeneraIcoFile(lnkbtnContratoTab1.Text, imgbtnContratoTab1);

                                }
                            }
                            else
                            {
                                rbConsorcioTab1.Checked = false;
                                rbConsultorTab1.Checked = false;
                                rbConsultorTab1.Enabled = true;
                                rbConsorcioTab1.Enabled = true;
                                Panel_ConsultorTab1.Visible = false;
                                Panel_ConsorcioTab1.Visible = false;

                                txtNombConsultorTab1.Text = "";
                                txtRucConsultorTab1.Text = "";

                                txtNomconsorcioTab1.Text = "";
                                txtRucConsorcioTab1.Text = "";
                                txtTelefonoConsorcioTab1.Text = "";
                                txtRepresentanteTab1.Text = "";

                                lblNomConsultorTab1.Font.Bold = false;
                                lblNomConsorcioTab1.Font.Bold = false;

                                cargaConsultoresTab1();
                                Up_rbProcesoTab1.Update();

                                imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                                lnkbtnContratoContratistaTab1.Text = "";
                                imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                                lnkbtnContratoTab1.Text = "";
                            }
                            Panel_ProcesoIndirectoTab1.Visible = true;
                            Panel_ProcesoDirectoTab1.Visible = false;
                            btnGuardarDirectoTab1.Visible = false;
                            cargaSeguimientoProcesoTab1();
                            // CargaDatosTab1();
                            //btnCambiarModalidadTab1.Visible = true; //No debe permitir el cambio de modalidad.

                            return;
                        }

                    default:
                        // You can use the default case.
                        return;
                }

            }
            else
            {
                ddlAdjudicacionTab1.SelectedValue = "";
                txtNroLicTab1.Text = "";
                TxtAnio2Tab1.Text = "";
                txtDetalleTab1.Text = "";
                txtProTab1.Text = "";
                txtProConsentidaTab1.Text = "";
                txtValorReferencialTab1.Text = "";
                ddlTipoContratacionTab1.SelectedValue = "";
                txtMontoContratadoTab1.Text = "";
                txtNroContrato.Text = "";
                txtNomconsorcioTab1.Text = "";
                txtRucConsorcioTab1.Text = "";
                txtTelefonoConsorcioTab1.Text = "";
                txtRepresentanteTab1.Text = "";

                rbConsorcioTab1.Checked = false;
                rbConsultorTab1.Checked = false;
                rbConsultorTab1.Enabled = true;
                rbConsorcioTab1.Enabled = true;
                Panel_ConsultorTab1.Visible = false;
                Panel_ConsorcioTab1.Visible = false;
                lblNomActualizaProcesoSeleccion.Text = "";
                lblNomActualizaProcesoDirecta.Text = "";

                imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoContratistaTab1.Text = "";
                imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoTab1.Text = "";

            }
            cargaConsultoresTab1();
            cargaSeguimientoProcesoTab1();

            // INFORMACION AUTOMATICA SEACE
            CargaCodigoSeace();
            CargaProcesosRelSEACE();

            Up_Tab1.Update();

        }

        protected void CargaDllModalidadTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoModalidad_Contrato();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaDllTipoResultado()
        {
            ddlResultadoTab1.DataSource = _objBLEjecucion.F_spMON_TipoResultado();
            ddlResultadoTab1.DataTextField = "nombre";
            ddlResultadoTab1.DataValueField = "valor";
            ddlResultadoTab1.DataBind();

            ddlResultadoTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaDllModalidadTab1()
        {
            ddlModalidadTab1.Items.Clear();

            //ddlModalidadTab1.Items.Insert(0, new ListItem("Directa", "1"));
            //ddlModalidadTab1.Items.Insert(0, new ListItem("Indirecta", "2"));
            //ddlModalidadTab1.Items.Insert(0, new ListItem("Otra Modalidad", "3"));
            ddlModalidadTab1.Items.Insert(0, new ListItem("Por Contrata", "5"));
            //ddlModalidadTab1.DataSource = _objBLProceso.F_spMON_TipoModalidad();
            //ddlModalidadTab1.DataTextField = "nombre";
            //ddlModalidadTab1.DataValueField = "valor";
            //ddlModalidadTab1.DataBind();

            //ddlModalidadTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaListaRubro()
        {
            ddlRubroTab1.DataSource = _objBLProceso.F_spMON_ListaRubro();
            ddlRubroTab1.DataTextField = "nombre";
            ddlRubroTab1.DataValueField = "valor";
            ddlRubroTab1.DataBind();

            ddlRubroTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void cargaDDlTipoContratacionTab1()
        {
            ddlTipoContratacionTab1.DataSource = _objBLProceso.F_spMON_TipoContratacion();
            ddlTipoContratacionTab1.DataTextField = "nombre";
            ddlTipoContratacionTab1.DataValueField = "valor";
            ddlTipoContratacionTab1.DataBind();

            ddlTipoContratacionTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void ddlProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (ddlProcesoTab1.SelectedValue != "")
            {
                //ddlModalidadTab1.SelectedValue = "";
                CargaDatosTab1();

                if (ddlModalidadTab1.SelectedValue != "")
                {

                }
                else
                {
                    Panel_ProcesoIndirectoTab1.Visible = true;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    btnCambiarModalidadTab1.Visible = false;
                }
            }


            if (Convert.ToInt32(lblCOD_SUBSECTOR.Text) == 1)
            {
                Panel_ProcesoDirectoTab1.Visible = false;
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;
            }

            if (Panel_ProcesoIndirectoTab1.Visible == true)
            {
                TabActivoProceso();
            }

            Up_Tab1.Update();
        }

        protected void btnCambiarModalidadTab1_OnClick(object sender, EventArgs e)
        {
            string valorModalidad = ddlModalidadTab1.SelectedValue;
            int val;
            if (valorModalidad == "1")
            {
                ddlModalidadTab1.SelectedValue = "2";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);

                Panel_ProcesoIndirectoTab1.Visible = true;
                Panel_ProcesoDirectoTab1.Visible = false;
                btnGuardarDirectoTab1.Visible = false;
                cargaSeguimientoProcesoTab1();
                CargaDatosTab1();
                btnCambiarModalidadTab1.Visible = true;


            }

            if (valorModalidad == "2")
            {
                ddlModalidadTab1.SelectedValue = "3";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);


                btnGuardarDirectoTab1.Visible = true;

                //cargaSeguimientoProcesoTab1();
                btnCambiarModalidadTab1.Visible = true;

                Panel_ProcesoIndirectoTab1.Visible = false;
                Panel_ProcesoDirectoTab1.Visible = false;
                btnGuardarDirectoTab1.Visible = false;

                CargaDatosTab1();
                //cargaSubcontratosTab1();
                //CargaResponsableTab1();


            }

            if (valorModalidad == "3")
            {
                ddlModalidadTab1.SelectedValue = "1";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);

                Panel_ProcesoDirectoTab1.Visible = true;
                btnGuardarDirectoTab1.Visible = true;
                Panel_ProcesoIndirectoTab1.Visible = false;
                //cargaSeguimientoProcesoTab1();
                btnCambiarModalidadTab1.Visible = true;
                CargaDatosTab1();
                cargaSubcontratosTab1();
                //CargaResponsableTab1();
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;
                Up_Tab1.Update();
            }

        }

        protected void btnGuardarDirectoTab1_OnClick(object sender, EventArgs e)
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
            //_BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue);
            //_BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

            //_BEProceso.norLic = Convert.ToInt32(txtNroLicTab1.Text);
            //_BEProceso.anio = TxtAnio2Tab1.Text;
            //_BEProceso.detalle = txtDetalleTab1.Text;
            _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
            _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
            //_BEProceso.valorReferencial = txtValorReferencialTab1.Text;

            //_BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
            //_BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
            //if (rbConsultorTab1.Checked == true)
            //{
            //    _BEProceso.id_tipoEmpresa = 1;
            //    _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
            //    _BEProceso.rucContratista = txtRucConsultorTab1.Text;

            //}

            //if (rbConsorcioTab1.Checked == true)
            //{
            //    _BEProceso.id_tipoEmpresa = 2;
            //    _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
            //    _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
            //    _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
            //    _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

            //    if (FileUploadAdjContratoTab1.HasFile)
            //    {
            //        _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
            //    }
            //    else
            //    { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


            //}

            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
            //_BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);
            int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);//2


            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                CargaDatosTab1();

                Up_Tab1.Update();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }


        protected void lnkbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoContratistaTab1_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoContratistaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoContratistaTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void ddlModalidadTab1_OnSelectIndexChanged(object sender, EventArgs e)
        {

            if (ddlProcesoTab1.SelectedValue != "")
            {
                CargaDatosTab1();

                if (ddlModalidadTab1.SelectedValue != "")
                {
                    if (ddlModalidadTab1.SelectedValue == "1")
                    {
                        Panel_ProcesoDirectoTab1.Visible = true;
                        btnGuardarDirectoTab1.Visible = true;
                        Panel_ProcesoIndirectoTab1.Visible = false;

                        //cargaSeguimientoProcesoTab1();

                        cargaSubcontratosTab1();
                        //CargaResponsableTab1();

                    }

                    if (ddlModalidadTab1.SelectedValue == "2")
                    {
                        Panel_ProcesoIndirectoTab1.Visible = true;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        cargaSeguimientoProcesoTab1();
                        // CargaDatosTab1();

                    }

                    if (ddlModalidadTab1.SelectedValue == "3")
                    {
                        Panel_ProcesoIndirectoTab1.Visible = false;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        //cargaSeguimientoProcesoTab1();
                        // CargaDatosTab1();

                    }


                    Up_Tab1.Update();
                }
                else
                {
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;

                }
            }

            if (Convert.ToInt32(lblCOD_SUBSECTOR.Text) == 1)
            {
                Panel_ProcesoDirectoTab1.Visible = false;
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;

            }


            Up_Tab1.Update();
        }

        protected void cargaSeguimientoProcesoTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;


            grdSeguimientoProcesoTab1.DataSource = _objBLProceso.F_spMON_SeguimientoProcesoSeleccion(_BEProceso);
            grdSeguimientoProcesoTab1.DataBind();
           
        }

        protected void grdSeguimientoProcesoTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab1");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void CargaddlTipoAdjudicacionTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoAdjudicacion();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected Boolean ValidarSeguimientoTab1()
        {
            Boolean result;
            result = true;

            if (TxtConvocatoriaTab1.Text == "")
            {
                string script = "<script>alert('Ingrese convocatoria.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaPubliTab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de publicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            if (txtProTab1.Text.Length > 0 && Convert.ToDateTime(txtFechaPubliTab1.Text) >Convert.ToDateTime(txtProTab1.Text))
            {
                string script = "<script>alert('La fecha de publicación debe ser menor o igual a la fecha de Buena Pro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoAdjudicacionTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de adjudicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlResultadoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione resultado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }

        protected void btnGuardarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSeguimientoTab1())
            {
                if (validaArchivo(FileUploadSeguimientoTAb1))
                {
                    if (validaArchivo(FileUploadContratoContratistaTab1))
                    {

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                        _BEProceso.Date_fechaPublicacion = Convert.ToDateTime(txtFechaPubliTab1.Text);
                        _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                        _BEProceso.resultado = ddlResultadoTab1.SelectedValue;
                        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                        _BEProceso.observacion = txtObservacion.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                        int val = _objBLProceso.spi_MON_SeguimientoProcesoSeleccion(_BEProceso);
                                             

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            TxtConvocatoriaTab1.Text = "";
                            txtFechaPubliTab1.Text = "";
                            ddlTipoAdjudicacionTab1.SelectedValue = "";
                            ddlResultadoTab1.SelectedValue = "";
                            txtObservacion.Text = "";

                            Panel_AgregarSeguimientoTab1.Visible = false;
                            imgbtn_AgregarSeguimientoTab1.Visible = true;
                            cargaSeguimientoProcesoTab1();
                            Up_Tab1.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }

                    }
                }
            }

        }
               
        //protected void imgDocResponsableTab1_OnClick(object sender, EventArgs e)
        //{
        //    ImageButton boton;
        //    GridViewRow row;
        //    boton = (ImageButton)sender;
        //    row = (GridViewRow)boton.NamingContainer;
        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


        //    ImageButton url = (ImageButton)grdResponsableTab1.Rows[row.RowIndex].FindControl("imgDocResponsableTab1");
        //    string urlDocumento = url.ToolTip;

        //    FileInfo toDownload = new FileInfo(pat + urlDocumento);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + urlDocumento);
        //        Response.End();
        //    }
        //}

        protected void imgDocTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdSeguimientoProcesoTab1.Rows[row.RowIndex].FindControl("imgDocTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgDocSubContratoTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdSubcontratosTab1.Rows[row.RowIndex].FindControl("imgDocSubContratoTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void rbConsultorTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsultorTab1.Font.Bold = true;
            lblNomConsorcioTab1.Font.Bold = false;
            Up_lblNomConsultorTab1.Update();
            Up_lblNomConsorcioTab1.Update();

            rbConsorcioTab1.Checked = false;
            Up_rbConsorcioTab1.Update();
            Panel_ConsultorTab1.Visible = true;
            Panel_ConsorcioTab1.Visible = false;
            Up_rbProcesoTab1.Update();
        }

        protected void rbConsorcioTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsorcioTab1.Font.Bold = true;
            lblNomConsultorTab1.Font.Bold = false;
            Up_lblNomConsorcioTab1.Update();
            Up_lblNomConsultorTab1.Update();

            rbConsultorTab1.Checked = false;
            Up_rbConsultorTab1.Update();
            Panel_ConsorcioTab1.Visible = true;
            Panel_ConsultorTab1.Visible = false;
            Up_rbProcesoTab1.Update();

        }

        protected void imgbtn_AgregarSeguimientoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            lblNomUsuarioSeguimientoProceso.Text = "";
            btnGuardarSeguimientoProcesoTab1.Visible = true;
            btnModificarSeguimientoProcesoTab1.Visible = false;
            Up_Tab1.Update();
        }

        protected void imgbtn_AgregarProceso_OnClick(object sender, EventArgs e)
        {
            //PanelSeguimientoProceso.Visible = true;
            //imgbtn_agregarProceso.Visible = false;
            lblNomUsuarioSeguimientoProceso.Text = "";
            btnGuardarSeguimientoProcesoTab1.Visible = true;
            btnModificarSeguimientoProcesoTab1.Visible = false;
            Up_Tab1.Update();
        }

        protected void btnCancelarProceso_OnClick(object sender, EventArgs e)
        {
            //PanelSeguimientoProceso.Visible = false;
            //imgbtn_agregarProceso.Visible = true;
            Up_Tab1.Update();
        }

        protected void btnCancelaSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtn_AgregarConsultorTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = true;
            btn_modificarConsultor.Visible = false;
            Up_Tab1.Update();
        }

        protected void btn_cancelarConsultor_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected Boolean ValidarResultadoTab1()
        {
            Boolean result;
            result = true;

            //if (ddlAdjudicacionTab1.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione tipo de Adjudicación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtNroLicTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese N° de Licitación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (TxtAnio2Tab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese año.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtDetalleTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese detalle.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (!txtProTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }


                }
            }

            if (!txtProConsentidaTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProConsentidaTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro Consentida.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }
            }

            if (!txtFechaFirmaContratoTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtFechaFirmaContratoTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de firma de contrato.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de firma de contrato ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato debe ser mayor o igual a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }

                if (!txtProConsentidaTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProConsentidaTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato debe ser mayor o igual a la Fecha de Buena Pro Consentida.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length > 0 && ddlProcesoTab1.SelectedValue.Equals("1"))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato debe ser menor o igual a la Fecha de Entrega de Terreno.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }
            }


            if (rbConsultorTab1.Checked == true)
            {
                if (txtNombConsultorTab1.Text == "")
                {
                    txtNombConsultorTab1.Focus();
                    string script = "<script>alert('Ingrese nombre del contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (txtRucConsultorTab1.Text == "")
                {
                    txtRucConsultorTab1.Focus();
                    string script = "<script>alert('Ingrese RUC de contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }



            if (rbConsorcioTab1.Checked == true)
            {
                if (txtNomconsorcioTab1.Text == "")
                {
                    txtNomconsorcioTab1.Focus();

                    string script = "<script>alert('Ingrese nombre del consorcio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                //if (txtRucConsorcioTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese RUC de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}
                //if (txtRepresentanteTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese representante legal de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}

            }

            //if (txtNroContrato.Text == "")
            //{
            //    string script = "<script>alert('Ingrese N° de contrato.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}



            return result;
        }

        protected void btnGuardarTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarResultadoTab1())
            {
                if (validaArchivo(FileUploadAdjContratoTab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                    if (ddlAdjudicacionTab1.SelectedValue == "")
                    {
                        _BEProceso.id_tipoAdjudicacion = 0;
                    }
                    else
                    { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }

                    _BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;
                    _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);

                    if (txtNroLicTab1.Text == "")
                    { txtNroLicTab1.Text = "0"; }
                    _BEProceso.norLic = (txtNroLicTab1.Text);

                    _BEProceso.anio = TxtAnio2Tab1.Text;
                    _BEProceso.detalle = txtDetalleTab1.Text;
                    _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                    _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                    if (txtValorReferencialTab1.Text == "")
                    { txtValorReferencialTab1.Text = "0"; }
                    _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                    if (txtMontoContratadoTab1.Text == "")
                    { txtMontoContratadoTab1.Text = "0"; }

                    _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                    _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
                    if (rbConsultorTab1.Checked == true)
                    {
                        _BEProceso.id_tipoEmpresa = 1;
                        _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                        _BEProceso.rucContratista = txtRucConsultorTab1.Text;
                        _BEProceso.representanteContratista = txtRepresentanteLegalConsultorTab1.Text;
                        if (FileUploadContratoContratistaTab1.HasFile)
                        {
                            _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                        }
                        else
                        { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }
                    }

                    if (rbConsorcioTab1.Checked == true)
                    {
                        _BEProceso.id_tipoEmpresa = 2;
                        _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                        _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                        _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                        _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                        if (FileUploadAdjContratoTab1.HasFile)
                        {
                            _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                        }
                        else
                        { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                    }

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEProceso.Date_fechaConvenio = VerificaFecha("");

                    int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso); //1


                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab1();

                        Up_Tab1.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                }
            }


        }

        protected Boolean ValidarGrupoConsorcioTab1()
        {
            Boolean result;
            result = true;

            if (txtContratistaGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese nombre de contratista.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtRucGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese N° de RUC.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtRepresentanGrupTAb1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Representante Legal.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            return result;
        }


        protected void btn_guardarConsultor_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (ValidarGrupoConsorcioTab1())
                {
                    if (validaArchivo(FileUploadAdjContratoTab1))
                    {
                        //REGISTRAR PROCESO DE SELECCION INDIRECTA

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                        if (ddlAdjudicacionTab1.SelectedValue == "")
                        {
                            _BEProceso.id_tipoAdjudicacion = 0;
                        }
                        else
                        { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }

                        _BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;
                        _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);

                        if (txtNroLicTab1.Text == "")
                        { txtNroLicTab1.Text = "0"; }
                        _BEProceso.norLic = (txtNroLicTab1.Text);

                        _BEProceso.anio = TxtAnio2Tab1.Text;
                        _BEProceso.detalle = txtDetalleTab1.Text;
                        _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                        _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                        if (txtValorReferencialTab1.Text == "")
                        { txtValorReferencialTab1.Text = "0"; }
                        _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                        if (txtMontoContratadoTab1.Text == "")
                        { txtMontoContratadoTab1.Text = "0"; }

                        _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                        _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
                        if (rbConsultorTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 1;
                            _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                            _BEProceso.rucContratista = txtRucConsultorTab1.Text;

                            if (FileUploadContratoContratistaTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }
                        }

                        if (rbConsorcioTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 2;
                            _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                            _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                            _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                            _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                            if (FileUploadAdjContratoTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                        }

                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        _BEProceso.Date_fechaConvenio = VerificaFecha("");

                        int valTab1 = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso); //1


                        //REGISTRA CONTRATISTA
                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                        _BEProceso.rucContratista = txtRucGrupTab1.Text;
                        _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        int val = _objBLProceso.spi_MON_GrupoConsorcio(_BEProceso);

                        if (val == 1 && valTab1 == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            cargaConsultoresTab1();

                            txtContratistaGrupTab1.Text = "";
                            txtRucGrupTab1.Text = "";
                            txtRepresentanGrupTAb1.Text = "";

                            Panel_AgregarConsultorTab1.Visible = false;
                            imgbtn_AgregarConsultorTab1.Visible = true;

                            Up_Tab1.Update();

                            CargaDatosTab1();
                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }

                }
            }

        }


        protected Boolean ValidarSubContratoTab1()
        {
            Boolean result;
            result = true;

            if (ddlRubroTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Rubro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtnombreTab1.Text == "")
            {
                string script = "<script>alert('Ingrese Nombre');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese monto.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtPlazoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese Plazo (Días).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlModalidaSubcontratoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese modalidad.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }

        protected void btnGuardarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSubContratoTab1())
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.nombre = txtnombreTab1.Text;
                _BEProceso.id_rubro = Convert.ToInt32(ddlRubroTab1.SelectedValue);
                _BEProceso.plazo = Convert.ToInt32(txtPlazoTab1.Text);
                _BEProceso.monto = txtMontoTab1.Text;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidaSubcontratoTab1.SelectedValue);
                _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spi_MON_SubContrato(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSubcontratosTab1();

                    // ddlModalidadTab1.SelectedValue = "";
                    txtnombreTab1.Text = "";
                    ddlRubroTab1.SelectedValue = "";
                    txtPlazoTab1.Text = "";
                    txtMontoTab1.Text = "";
                    ddlModalidaSubcontratoTab1.SelectedValue = "";

                    Panel_SubcontratosTab1.Visible = false;
                    imgbtnAgregarSubcontratoTab1.Visible = true;

                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }

        protected void imgbtnAgregarSubcontratoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_SubcontratosTab1.Visible = true;
            imgbtnAgregarSubcontratoTab1.Visible = false;
            btnGuardarSubcontratosTab1.Visible = true;
            btnModificarSubcontratosTab1.Visible = false;
            Up_Tab1.Update();
        }

        protected void btnCancelarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {
            Panel_SubcontratosTab1.Visible = false;
            imgbtnAgregarSubcontratoTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void grdSubcontratosTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocSubContratoTab1");

                GeneraIcoFile(imb.ToolTip, imb);


            }
        }

        protected void cargaSubcontratosTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdSubcontratosTab1.DataSource = _objBLProceso.F_spMON_SubContrato(_BEProceso);
            grdSubcontratosTab1.DataBind();


        }

        //protected void CargaResponsableTab1()
        //{
        //    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

        //    //grdResponsableTab1.DataSource = _objBLProceso.spMON_ResponsableObra(_BEProceso);
        //    //grdResponsableTab1.DataBind();


        //}

        //protected void imgbtnAgregarResponsableTab1_OnClick(object sender, EventArgs e)
        //{
        //    Panel_ResponsableTab1.Visible = true;
        //    imgbtnAgregarResponsableTab1.Visible = false;
        //    btnGuardarResponsableObraTab1.Visible = true;
        //    btnModificarResponsableObraTab1.Visible = false;
        //    Up_Tab1.Update();
        //}

        //protected void btnCancelarResponsableTab1_OnClick(object sender, EventArgs e)
        //{
        //    Panel_ResponsableTab1.Visible = false;
        //    imgbtnAgregarResponsableTab1.Visible = true;
        //    Up_Tab1.Update();
        //}

        //protected Boolean ValidarResponsableTab1()
        //{
        //    Boolean result;
        //    result = true;

        //    if (txtCargoTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese cargo.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    if (txtNombreResponsableTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese Nombre');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }


        //    if (txtResolucionTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese N° de Resolución.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    if (txtCipTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese CIP.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    if (ddlModalidadResponsableTab1.SelectedValue == "")
        //    {
        //        string script = "<script>alert('Ingrese Modalidad.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }


        //    return result;
        //}

        //protected void btnGuardarResponsableObraTab1_OnClick(object sender, EventArgs e)
        //{
        //    if (ValidarResponsableTab1())
        //    {
        //        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

        //        _BEProceso.nombre = txtNombreResponsableTab1.Text;
        //        _BEProceso.cargo = txtCargoTab1.Text;
        //        _BEProceso.nroResolucion = (txtResolucionTab1.Text);
        //        _BEProceso.cip = txtCipTab1.Text;
        //        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadResponsableTab1.SelectedValue);
        //        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResponsableTab1);
        //        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLProceso.spi_MON_ResponsableObra(_BEProceso);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se registró correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //CargaResponsableTab1();

        //            txtnombreTab1.Text = "";
        //            txtCargoTab1.Text = "";
        //            txtResolucionTab1.Text = "";
        //            txtCipTab1.Text = "";
        //            ddlModalidadResponsableTab1.SelectedValue = "";

        //            Panel_ResponsableTab1.Visible = false;
        //            imgbtnAgregarResponsableTab1.Visible = true;

        //            Up_Tab1.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }


        //}
        protected void grdResponsableTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocResponsableTab1");

                GeneraIcoFile(imb.ToolTip, imb);



            }
        }

        protected void btnModificarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSeguimientoTab1())
            {


                _BEProceso.id_seguimiento = Convert.ToInt32(lblRegistroSeguimientoP.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                _BEProceso.Date_fechaPublicacion = Convert.ToDateTime(txtFechaPubliTab1.Text);
                _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                _BEProceso.resultado = ddlResultadoTab1.SelectedValue;

                if (FileUploadSeguimientoTAb1.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSeguimientoTAb1.Text;
                }


                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);


                _BEProceso.observacion = txtObservacion.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Procesos_Editar(_BEProceso);
                               

                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();


                    TxtConvocatoriaTab1.Text = "";
                    txtFechaPubliTab1.Text = "";
                    ddlTipoAdjudicacionTab1.SelectedValue = "";
                    ddlResultadoTab1.SelectedValue = "";
                    txtObservacion.Text = "";
                    LnkbtnSeguimientoTAb1.Text = "";
                    imgbtnSeguimientoTAb1.Visible = false;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSeguimientoTab1.Visible = false;
                btnModificarSeguimientoProcesoTab1.Visible = true;
                cargaSeguimientoProcesoTab1();
                Up_Tab1.Update();
                imgbtn_AgregarSeguimientoTab1.Visible = true;

            }



        }


        protected void btnModificarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSubContratoTab1())
            {


                _BEProceso.id_subContratos = Convert.ToInt32(lblId_SubContratos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.id_rubro = Convert.ToInt32(ddlRubroTab1.SelectedValue);
                _BEProceso.nombre = txtnombreTab1.Text;
                _BEProceso.monto = txtMontoTab1.Text;
                _BEProceso.plazo = Convert.ToInt32(txtPlazoTab1.Text);
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidaSubcontratoTab1.SelectedValue);

                if (FileUploadSubContrato.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSubContrato.Text;
                }





                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_SubContrato_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    ddlRubroTab1.SelectedValue = "";
                    txtnombreTab1.Text = "";
                    txtMontoTab1.Text = "";
                    txtPlazoTab1.Text = "";
                    ddlModalidaSubcontratoTab1.SelectedValue = "";
                    LnkbtnSubContrato.Text = "";
                    imgbtnSubContrato.Visible = false;
                    imgbtnAgregarSubcontratoTab1.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_SubcontratosTab1.Visible = false;
                cargaSubcontratosTab1();
                btnModificarSubcontratosTab1.Visible = true;
                Up_Tab1.Update();

            }


        }

        //protected void btnModificarResponsableObraTab1_OnClick(object sender, EventArgs e)
        //{

        //    if (ValidarResponsableTab1())
        //    {


        //        _BEProceso.id_responsable = Convert.ToInt32(lblId_Responsable_Obra.Text);
        //        //_BEFinanciamiento.tipoFinanciamiento = 3;
        //        _BEProceso.cargo = txtCargoTab1.Text;
        //        _BEProceso.nroResolucion = (txtResolucionTab1.Text);
        //        _BEProceso.nombre = txtNombreResponsableTab1.Text;
        //        _BEProceso.cip = txtCipTab1.Text;
        //        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadResponsableTab1.SelectedValue);

        //        if (FileUploadResponsableTab1.PostedFile.ContentLength > 0)
        //        {
        //            _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResponsableTab1);
        //        }
        //        else
        //        {
        //            _BEProceso.urlDoc = LnkbtnResponsableObraTab1c.Text;
        //        }

        //        //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResponsableTab1);

        //        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        int resul;
        //        resul = _objBLProceso.spud_MON_Seguimiento_Responsable_Editar(_BEProceso);
        //        if (resul == 1)
        //        {
        //            string script = "<script>alert('Registro Correcto.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //            CargaFinanTransferencia();


        //            txtCargoTab1.Text = "";
        //            txtResolucionTab1.Text = "";
        //            txtNombreResponsableTab1.Text = "";
        //            txtCipTab1.Text = "";
        //            ddlModalidadResponsableTab1.SelectedValue = "";
        //            LnkbtnResponsableObraTab1c.Text = "";
        //            imgbtnResponsableObraTab1.Visible = false;




        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //        Panel_ResponsableTab1.Visible = false;
        //        //CargaResponsableTab1();
        //        btnModificarResponsableObraTab1.Visible = true;
        //        imgbtnAgregarResponsableTab1.Visible = true;
        //        Up_Tab1.Update();

        //    }
        //}


        protected void btn_modificarConsultor_OnClick(object sender, EventArgs e)
        {

            if (ValidarGrupoConsorcioTab1())
            {

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(lblId_Registro_Contratista.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                _BEProceso.rucContratista = txtRucGrupTab1.Text;
                _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Consorcio_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();

                    txtContratistaGrupTab1.Text = "";
                    txtRucGrupTab1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarConsultorTab1.Visible = false;
                cargaConsultoresTab1();
                btn_modificarConsultor.Visible = true;
                imgbtn_AgregarConsultorTab1.Visible = true;
                Up_Tab1.Update();


            }
        }

        protected void grdSeguimientoProcesoTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoProcesoTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_seguimiento = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Procesos_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSeguimientoProcesoTab1();
                    Up_Tab1.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdSubcontratosTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSubcontratosTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_subContratos = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_SubContrato_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSubcontratosTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        //protected void grdResponsableTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grdResponsableTab1.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEProceso.id_responsable = Convert.ToInt32(objTemp.ToString());
        //        //_BEFinaTra.tipo = 2;
        //        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLProceso.spud_MON_Seguimiento_Responsable_Eliminar(_BEProceso);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //CargaResponsableTab1();
        //            Up_Tab1.Update();
        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        protected void grdConsultoresTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdConsultoresTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Consorcio_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaConsultoresTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        #endregion

        #region Tab2
        protected void CargaTab2()
        {
            CargaTipoCartaFianza();

            dt = CargaEmpleadoObraTab2(2);
            grd_HistorialInspectorTab2.DataSource = dt;
            grd_HistorialInspectorTab2.DataBind();

            dt = CargaEmpleadoObraTab2(1);
            grd_HistorialResidenteTab2.DataSource = dt;
            grd_HistorialResidenteTab2.DataBind();

            //dt = CargaEmpleadoObraTab2(3);
            //grd_HistorialSupervisorTab2.DataSource = dt;
            //grd_HistorialSupervisorTab2.DataBind();


            CargaTipoAdjudicacionTab2();

            CargaDatosTab2();

            if (!this.IsPostBack)
            {
                grdAvanceFisicoTab2.PageIndex = Int32.MaxValue;
                grdAvanceFisicoAdicionalTab2.PageIndex = Int32.MaxValue;
                grdEvaluacionTab2.PageIndex = Int32.MaxValue;
                grdProgramacionTab2.PageIndex = Int32.MaxValue;
                grdFianzasTab2.PageIndex = Int32.MaxValue;
            }

            CargaTipoAccion();
            cargaCartasFianzasTab2();
            CargaAdelanto();
            CargaAvanceFisico();
            CargaTipoMonitoreo();
            //CargaTipoDocumentos();

            CargaTipoEstadoSituacionalTab2();
            CargaTipoValorizacion();

            cargarEvaluacionTab2();
            cargaProgramacionTab2();

         
            //cargarConexionesPNSR();
        }

        protected void CargaTipoCartaFianza()
        {
            List<BE_MON_BANDEJA> ListComboTipo = new List<BE_MON_BANDEJA>();

            ListComboTipo = _objBLEjecucion.F_spMON_TipoCartaFianza();

            ddlTipoTab2.DataSource = ListComboTipo;
            ddlTipoTab2.DataTextField = "nombre";
            ddlTipoTab2.DataValueField = "valor";
            ddlTipoTab2.DataBind();

            ddlTipoTab2.Items.Insert(0, new ListItem("SELECCIONAR", ""));

            ddlTipoFianzaSupervisionTab2.DataSource = ListComboTipo;
            ddlTipoFianzaSupervisionTab2.DataTextField = "nombre";
            ddlTipoFianzaSupervisionTab2.DataValueField = "valor";
            ddlTipoFianzaSupervisionTab2.DataBind();

            ddlTipoFianzaSupervisionTab2.Items.Insert(0, new ListItem("SELECCIONAR", ""));


        }

        protected void CargaTipoAccion()
        {
            List<BE_MON_BANDEJA> ListComboTipo = new List<BE_MON_BANDEJA>();

            ListComboTipo = _objBLEjecucion.F_spMON_TipoAccionCarta();
            ddlTipoAccion.DataSource = ListComboTipo;
            ddlTipoAccion.DataTextField = "nombre";
            ddlTipoAccion.DataValueField = "valor";
            ddlTipoAccion.DataBind();

            ddlTipoAccion.Items.Insert(0, new ListItem("SELECCIONAR", ""));

            ddlTipoAccionSupervisionTab2.DataSource = ListComboTipo;
            ddlTipoAccionSupervisionTab2.DataTextField = "nombre";
            ddlTipoAccionSupervisionTab2.DataValueField = "valor";
            ddlTipoAccionSupervisionTab2.DataBind();

            ddlTipoAccionSupervisionTab2.Items.Insert(0, new ListItem("SELECCIONAR", ""));

        }
        protected void CargaTipoValorizacion()
        {
            //ddlValorizacionTab2.DataSource = _objBLEjecucion.F_spMON_TipoValorizacion();
            //ddlValorizacionTab2.DataTextField = "nombre";
            //ddlValorizacionTab2.DataValueField = "valor";
            //ddlValorizacionTab2.DataBind();

            ddlValorizacionTab2.Items.Insert(0, new ListItem("Valorización Normal", "1"));
            ddlValorizacionTab2.Items.Insert(0, new ListItem("Valorización por Mayores Gastos", "3"));
            ddlValorizacionTab2.Items.Insert(0, new ListItem("Valorizacion por Intereses", "4"));

            ddlValorizacionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlTipoValorizacionSupervisionTab2.DataSource = _objBLEjecucion.F_spMON_TipoValorizacion();
            ddlTipoValorizacionSupervisionTab2.DataTextField = "nombre";
            ddlTipoValorizacionSupervisionTab2.DataValueField = "valor";
            ddlTipoValorizacionSupervisionTab2.DataBind();

            ddlTipoValorizacionSupervisionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoMonitoreo()
        {
            ddlTipoMonitoreo.DataSource = _objBLEjecucion.F_spMON_ListarTipoMonitoreo();
            ddlTipoMonitoreo.DataTextField = "nombre";
            ddlTipoMonitoreo.DataValueField = "valor";
            ddlTipoMonitoreo.DataBind();

            ddlTipoMonitoreo.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoDocumentos()
        {
            //ddlTipoDocumento.DataSource = _objBLEjecucion.F_spMON_ListarTipoDocumentoEvaluacionRecomendacion();
            //ddlTipoDocumento.DataTextField = "nombre";
            //ddlTipoDocumento.DataValueField = "valor";
            //ddlTipoDocumento.DataBind();

            //ddlTipoDocumento.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected DataTable CargaEmpleadoObraTab2(int tipo)
        {
            DataTable dt;
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoObra(_BEEjecucion);
            return dt;

            //  grd.DataBind();
        }

        protected DataTable CargaEmpleadoObraSupervisionTab2(int tipo)
        {
            DataTable dt;
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoObraSupervision(_BEEjecucion);
            return dt;

            //  grd.DataBind();
        }

        protected void grdAvanceFisicoTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlFechaProgramacionInformeTab2.Visible = false;
            txtFechaProgramacion.Visible = true;

            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_Avance.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoTab2.SelectedRow;
            txtFechaProgramacion.Text = ((Label)row.FindControl("lblMesTab2")).Text;
            ddlValorizacionTab2.Text = ((Label)row.FindControl("lblid_tipoValorizacionTab2")).Text;
            txtfechaPresentacionTab2.Text = ((Label)row.FindControl("lblFechaPresentacionTab2")).Text;
            //txtfechaTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtMontoTab2.Text = ((Label)row.FindControl("lblMontoSinIGVTab2")).Text;
            txtMontoReajusteTab2.Text = ((Label)row.FindControl("lblMontoReajusteTab2")).Text;
            txtMontoDirectoTab2.Text = ((Label)row.FindControl("lblDirectoTab2")).Text;
            txtMontoMaterialesTab2.Text = ((Label)row.FindControl("lblMaterialesTab2")).Text;
            txtValorizacionEjecutadaTab2.Text = ((Label)row.FindControl("lblMontoEjecutadoTab2")).Text;
            txtFisiRealTab2.Text = ((Label)row.FindControl("lblFisicoReal")).Text;
            lblFisRealPreAcumuladoTab2.Text = (Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) - Convert.ToDouble(txtFisiRealTab2.Text)).ToString();
            txtFisiProgTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            lblFisProgPreAcumuladoTab2.Text = (Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text) - Convert.ToDouble(txtFisiProgTab2.Text)).ToString();
            txtFinanRealTab2.Text = ((Label)row.FindControl("lblFinancieroReal")).Text;
            lblFinancieroRealPreAcumuladoTab2.Text = (Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text) - Convert.ToDouble(txtFinanRealTab2.Text)).ToString();
            txtFinanProgTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            lblFinancieroProgPreAcumuladoTab2.Text = (Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text) - Convert.ToDouble(txtFinanProgTab2.Text)).ToString();

            txtIGVTab2.Text = ((Label)row.FindControl("lblIGVTab2")).Text;
            ddlEstadoSituacionalTab2.Text = ((Label)row.FindControl("lblID_tipoSitucion")).Text;
            txtObservaciontab2.Text = ((Label)row.FindControl("lblObservacionTab2")).Text;
            txtFechaPagoTab2.Text = ((Label)row.FindControl("lblFechaPagoTab2")).Text;

            //txtNroAdicionalTab2.Text = ((Label)row.FindControl("lblAdicional")).Text;
            LnkbtnAvanceTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;

            lblNomUsuarioAvance.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnAvanceTab2.Text, imgbtnAvanceTab2);


            btnGuardarAvanceTab2.Visible = false;
            btnModificarAvanceTab2.Visible = true;
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;

            Up_Tab2.Update();

        }


        protected void grdAvanceFisicoAdicionalTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoAdicionalTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_AvanceAdicional.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoAdicionalTab2.SelectedRow;
            //ddlValorizacionAdicionalTab2.Text = ((Label)row.FindControl("lblid_tipoValorizacion")).Text;
            txtNroAdicionalAdicionalTab2.Text = ((Label)row.FindControl("lblNroAdicional")).Text;
            txtValorizacionAdicionalTab2.Text = ((Label)row.FindControl("lblNroValorizacionAdicional")).Text;
            txtFechaPresentacionAdicionalTab2.Text = ((Label)row.FindControl("lblFechaPresentacionTab2")).Text;
            txtMontoSinIGVAdicionalTab2.Text = ((Label)row.FindControl("lblMontoSinIGVTab2")).Text;
            txtMontoReajusteAdicionalTab2.Text = ((Label)row.FindControl("lblMontoReajusteTab2")).Text;
            txtMontoDirectoAdicional.Text = ((Label)row.FindControl("lblDirectoTab2")).Text;
            txtMontoMaterialesAdicional.Text = ((Label)row.FindControl("lblMaterialesTab2")).Text;
            txtFisiRealAdicional.Text = ((Label)row.FindControl("lblFisicoReal")).Text;
            txtFisiProgAdicionalTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            txtFinanRealAdicionalTab2.Text = ((Label)row.FindControl("lblFinancieroReal")).Text;
            txtFinanProgAdicionalTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            txtIGVAdicionalTab2.Text = ((Label)row.FindControl("lblIGVTab2")).Text;
            txtObservacionAdicionaltab2.Text = ((Label)row.FindControl("lblObservacionTab2")).Text;
            txtFechaPagoAdicionalTab2.Text = ((Label)row.FindControl("lblFechaPagoTab2")).Text;
            ddlEstadoSituacionalAdicionalTab2.Text = ((Label)row.FindControl("lblID_tipoSitucion")).Text;

            LnkbtnAvanceAdicionalTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;

            lblNomUsuarioAvanceAdicional.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnAvanceAdicionalTab2.Text, imgbtnAvanceAdicionalTab2);

            btnGuardarAvanceAdicionalTab2.Visible = false;
            btnModificarAvanceAdicionalTab2.Visible = true;
            Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoAdicionalTab2.Visible = false;

            Up_Tab2.Update();

        }

        protected void cargaCartasFianzasTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdFianzasTab2.DataSource = _objBLEjecucion.spMON_CartaFianza(_BEEjecucion);
            grdFianzasTab2.DataBind();
        }

        protected void grdFianzasTab2_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDiasRestante = (Label)e.Row.FindControl("lblDiasRestante");
                if (Convert.ToInt32(lblDiasRestante.Text) < 1)
                {
                    lblDiasRestante.ForeColor = Color.Red;
                }

                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocCartaTab2");
                GeneraIcoFile(imb.ToolTip, imb);

               

                CheckBox chkVer = (CheckBox)e.Row.FindControl("chb_flagVerifico");
                if (chkVer.Text == "1")
                {
                    chkVer.Checked = true;
                }
                chkVer.Text = "";

                ImageButton imgDocAccionGriTab2 = (ImageButton)e.Row.FindControl("imgDocAccionGriTab2");
                if (imgDocAccionGriTab2.ToolTip.Length > 0)
                {
                    GeneraIcoFile(imgDocAccionGriTab2.ToolTip, imgDocAccionGriTab2);
                }
                else { imgDocAccionGriTab2.Visible = false; }

                Label lblIdAccion = (Label)e.Row.FindControl("lblIdTipoAccion");
                if (lblIdAccion.Text.Equals("2") || lblIdAccion.Text.Equals("3") || lblIdAccion.Text.Equals("4"))
                {
                    lblDiasRestante.Text = "";
                }

                if (lblID_ACCESO.Text.Equals("0"))
                {
                    chkVer.Enabled = false;
                    //chkRenuev.Enabled = false;
                }
            }
        }

        protected void imgDocCartaTab2_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdFianzasTab2.Rows[row.RowIndex].FindControl("imgDocCartaTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void CargaAvanceFisico()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListAvance = new List<BE_MON_Ejecucion>();
            ListAvance = _objBLEjecucion.F_spMON_InformeAvancePorContrata(_BEEjecucion);

            grdAvanceFisicoTab2.DataSource = ListAvance;
            grdAvanceFisicoTab2.DataBind();

            lblMontoTotalTab2.Text = ((from prod in ListAvance
                                       select Convert.ToDecimal(prod.monto)).Sum()).ToString("N2");

            if (txtMontoContratadoTab1.Text.Length == 0)
            {
                txtMontoContratadoTab1.Text = "0";
            }

            lblSaldoMontoContratado.Text = (Convert.ToDouble(txtMontoContratadoTab1.Text) - Convert.ToDouble(lblMontoTotalTab2.Text)).ToString("N2");


            lblAmortizacionDirectoTab2.Text = ((from prod in ListAvance
                                                select Convert.ToDecimal(prod.amortizacionDirecto)).Sum()).ToString("N2");

            if (lblTotalMontoAdelantoDirecto.Text.Length == 0)
            {
                lblTotalMontoAdelantoDirecto.Text = "0";
            }

            lblSaldoAmortizadoDirecto.Text = (Convert.ToDouble(lblTotalMontoAdelantoDirecto.Text) - (Convert.ToDouble(lblAmortizacionDirectoTab2.Text)*1.18)).ToString("N2");

            lblAmortizacionMaterialTab2.Text = ((from prod in ListAvance
                                                 select Convert.ToDecimal(prod.amortizacionMateriales)).Sum()).ToString("N2");

            if (lblTotalMontoAdelantoMaterial.Text.Length == 0)
            {
                lblTotalMontoAdelantoMaterial.Text = "0";
            }
            lblSadoAmortizadoMaterial.Text = (Convert.ToDouble(lblTotalMontoAdelantoMaterial.Text) - (Convert.ToDouble(lblAmortizacionMaterialTab2.Text)*1.18)).ToString("N2");

            lblFisicoRealAcumuladoTab2.Text = ((from prod in ListAvance
                                       select Convert.ToDecimal(prod.fisicoReal)).Sum()).ToString();

            lblFisicoProgAcumuladoTab2.Text = ((from prod in ListAvance
                                       select Convert.ToDecimal(prod.fisicoProgramado)).Sum()).ToString();

            lblFinancieroRealAcumuladoTab2.Text = ((from prod in ListAvance
                                           select Convert.ToDecimal(prod.financieroReal)).Sum()).ToString();

            lblFinancieroProgAcumuladoTab2.Text = ((from prod in ListAvance
                                           select Convert.ToDecimal(prod.financieroProgramado)).Sum()).ToString();

            var ListOrderFechaUpdate = ListAvance.OrderByDescending(x => x.fecha_update).ToList();

            if (ListOrderFechaUpdate.Count > 0 && ListOrderFechaUpdate.ElementAt(0).fecha_update != null)
            {
                lblFechaUltimaActualizacionValorizacionTab2.Text = "(Actualizó: " + ListOrderFechaUpdate.ElementAt(0).usuario + " - " + ListOrderFechaUpdate.ElementAt(0).fecha_update + ")";
            }

            List<BE_MON_Ejecucion> ListAvanceAdicional = new List<BE_MON_Ejecucion>();
            ListAvanceAdicional = _objBLEjecucion.F_spMON_InformeAvanceAdicionalPorContrata(_BEEjecucion);

            grdAvanceFisicoAdicionalTab2.DataSource = ListAvanceAdicional;
            grdAvanceFisicoAdicionalTab2.DataBind();

            txtMontoTotalAdicionalTab2.Text = ((from prod in ListAvanceAdicional
                                                select Convert.ToDecimal(prod.monto)).Sum()).ToString("N2");


        }

        protected void imgDocAvanceTab2_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocAvanceTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void chb_flagVerifico_Check(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var val2 = grdFianzasTab2.DataKeys[row.RowIndex].Value;
            _BEEjecucion.id_tabla = Convert.ToInt32(val2.ToString());

            CheckBox chb_flagVerifico = (CheckBox)grdFianzasTab2.Rows[row.RowIndex].FindControl("chb_flagVerifico");
            if (chb_flagVerifico.Checked == true)
            {
                _BEEjecucion.flagVerificado = "1";
            }

            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_CartaFianza_FlagVerifica(_BEEjecucion);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                cargaCartasFianzasTab2();
                Up_Tab2.Update();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }


        protected void imgbtnAvanceFisicoTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = true;
            //Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;
            lblNomUsuarioAvance.Text = "";
            btnGuardarAvanceTab2.Visible = true;
            btnModificarAvanceTab2.Visible = false;

            ddlFechaProgramacionInformeTab2.Visible = true;
            txtFechaProgramacion.Visible = false;
            Up_Tab2.Update();
        }

        protected void imgbtnAvanceFisicoAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoAdicionalTab2.Visible = false;
            lblNomUsuarioAvanceAdicional.Text = "";
            btnGuardarAvanceAdicionalTab2.Visible = true;
            btnModificarAvanceAdicionalTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = false;
            imgbtnAvanceFisicoTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceAdicionalTab2.Visible = false;
            imgbtnAvanceFisicoAdicionalTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void imgbtn_residenteObraTab2_OnClick(object sender, EventArgs e)
        {
            lnkbtnDocResidenteTab2.Text = "";
            imgbtnDocResidenteTab2.ImageUrl = "~/img/blanco.png";
            Panel_HistorialResidenteTab2.Visible = true;
            btnGuardarResidenteTab2.Visible = true;
            btnModificarResidenteTab2.Visible = false;
            Up_Tab2.Update();
        }

        //protected void imgbtn_SupervisorTab2_OnClick(object sender, EventArgs e)
        //{
        //    Panel_HistorialSupervisorTab2.Visible = true;
        //    btnGuardarSupervisorTab2.Visible = true;
        //    btnModificarSupervisorTab2.Visible = false;

        //    Up_Tab2.Update();
        //}


        protected void imgbtnMostrarFecha_OnClick(object sender, EventArgs e)
        {
            //Panel_HistorialSupervisorTab2.Visible = true;
            //btnGuardarSupervisorTab2.Visible = true;
            //btnModificarSupervisorTab2.Visible = false;

            //Up_Tab2.Update();

            //lblFecha.Visible = true;
            //txtFechaTerminoTab2.Visible = true;
        }

        protected void btnCancelarResidenteTab2_Onclick(object sender, EventArgs e)
        {
            Panel_HistorialResidenteTab2.Visible = false;

            Up_Tab2.Update();
        }

        //protected void btnCancelarSupervisorTab2_Onclick(object sender, EventArgs e)
        //{
        //    Panel_HistorialSupervisorTab2.Visible = false;

        //    Up_Tab2.Update();
        //}

        protected void ImgbtnActualizarInspector_OnClick(object sender, EventArgs e)
        {
            imgbtnDocInspectorTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnDocInspectorTab2.Text="";
            Panel_HistorialInspectorTab2.Visible = true;
            btnGrabarInspectorTab2.Visible = true;
            btnModificarInspectorTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void btnCancelarInspectorTab2_Onclick(object sender, EventArgs e)
        {
            Panel_HistorialInspectorTab2.Visible = false;

            Up_Tab2.Update();
        }

        protected void CargaTipoAdjudicacionTab2()
        {
            ddlEstadoTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(3,4);
            ddlEstadoTab2.DataTextField = "nombre";
            ddlEstadoTab2.DataValueField = "valor";
            ddlEstadoTab2.DataBind();

            ddlEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoEstadoSituacionalTab2()
        {
            List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();

            ListBandeja = _objBLEjecucion.F_spMON_TipoEstadoSituacional();
            ddlEstadoSituacionalTab2.DataSource = ListBandeja;
            ddlEstadoSituacionalTab2.DataTextField = "nombre";
            ddlEstadoSituacionalTab2.DataValueField = "valor";
            ddlEstadoSituacionalTab2.DataBind();

            ddlEstadoSituacionalTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlEstadoSituacionalAdicionalTab2.DataSource = ListBandeja;
            ddlEstadoSituacionalAdicionalTab2.DataTextField = "nombre";
            ddlEstadoSituacionalAdicionalTab2.DataValueField = "valor";
            ddlEstadoSituacionalAdicionalTab2.DataBind();

            ddlEstadoSituacionalAdicionalTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlEstadoSituacionalSupervisionTab2.DataSource = ListBandeja;
            ddlEstadoSituacionalSupervisionTab2.DataTextField = "nombre";
            ddlEstadoSituacionalSupervisionTab2.DataValueField = "valor";
            ddlEstadoSituacionalSupervisionTab2.DataBind();

            ddlEstadoSituacionalSupervisionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoSubEstadosDetalle(string TipoEstadoEjecucion)
        {
            ddlSubEstadoTab2.Items.Clear();
            if (TipoEstadoEjecucion.Equals(""))
            {
                ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
            }
            else
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                list = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(TipoEstadoEjecucion));

                ddlSubEstadoTab2.DataSource = list;
                ddlSubEstadoTab2.DataTextField = "nombre";
                ddlSubEstadoTab2.DataValueField = "valor";
                ddlSubEstadoTab2.DataBind();

                ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

                if (list.Count == 0)
                {
                    spSubestado.Visible = false;
                    ddlSubEstadoTab2.Visible = false;
                }
                else
                {
                    spSubestado.Visible = true;
                    ddlSubEstadoTab2.Visible = true;
                }
            }
        }

        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected Boolean ValidarEjecucionTab2()
        {
            Boolean result;
            result = true;

           
            if (txtPlazoEjecucionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese plazo de ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaEntregaTerrenoTab2.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaEntregaTerrenoTab2.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de entrega de terreno no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de entrega de terreno no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaFirmaContratoTab1.Text.Length > 0 && Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text) < Convert.ToDateTime(txtFechaFirmaContratoTab1.Text))
                {
                    string script = "<script>alert('La fecha de entrega de terreno debe ser mayor o igual a la fecha de firma de contrato.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }
            if (txtFechaInicioTab2.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaInicioTab2.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de inicio de terreno no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaInicioTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de inicio no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaInicioTab2.Text) < Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text))
                {
                    string script = "<script>alert('La fecha de inicio debe ser mayor o igual a la fecha de entrega de terreno.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaTerminoReal.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaTerminoReal.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de termino real no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaTerminoReal.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de termino real no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaInicioTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaTerminoReal.Text) <= Convert.ToDateTime(txtFechaInicioTab2.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser mayor a la fecha de inicio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaRecepcionFinalTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaTerminoReal.Text) > Convert.ToDateTime(txtFechaRecepcionFinalTab3.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser menor o igual a la fecha de acta de recepción final.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            return result;
        }

        protected void CargaDatosTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {

                lblNombreEstadoGeneral.Text = dt.Rows[0]["vEstadoEjecucion"].ToString().ToUpper();
                lblIdEstado.Text = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();
                lblIdSubEstado.Text = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
                lblIdSubEstado2.Text = dt.Rows[0]["id_TipoEstadoParalizado"].ToString();



                txtAdministradorTab2.Text = dt.Rows[0]["administrador"].ToString();
                txtTelefonoAdmTab2.Text = dt.Rows[0]["telefonoAdministrador"].ToString();
                txtCorreoAdmTab2.Text = dt.Rows[0]["correoAdministrador"].ToString();

                txtFechaInicioTab2.Text = dt.Rows[0]["fechaInicio"].ToString();
                txtPlazoEjecucionTab2.Text = dt.Rows[0]["plazoEjecucion"].ToString();
                //txtFechaInicioContractualTab2.Text = dt.Rows[0]["fechaInicioContractual"].ToString();
                //txtFechaInicioReal.Text = dt.Rows[0]["fechaInicioReal"].ToString();
                txtFechaFinContractual.Text = dt.Rows[0]["fechaFinContractual"].ToString();

                txtFechaFinReprogramado.Text = dt.Rows[0]["fechaFinReal"].ToString();
                txtFechaTerminoReal.Text = dt.Rows[0]["fechaFin"].ToString();

                txtFechaTerminoProbableTab2.Text = dt.Rows[0]["fechaFinProbable"].ToString();

                //if (txtFechaTerminoTab2.Text.Length > 0)
                //{
                //    lblFecha.Visible = true;
                //    txtFechaTerminoTab2.Visible = true;
                //    imgbtnMostrarFecha.Visible = false;

                //}
                if (dt.Rows[0]["flagProgramacion"].ToString() == "1")
                {
                    //txtFechaEntregaTerrenoTab2.Enabled = false;
                    txtFechaInicioTab2.Enabled = false;
                    txtPlazoEjecucionTab2.Enabled = false;
                    btnGenerarProgramacionTab2.Enabled = false;

                    if (ddlProcesoTab1.SelectedValue == "1")
                    {
                        txtMontoContratadoTab1.Enabled = false;
                    }
                    else
                    {
                        txtMontoContratadoTab1.Enabled = true;
                    }

                }

                txtFechaEntregaTerrenoTab2.Text = dt.Rows[0]["FechaEntregaTerreno"].ToString();
                // txtFechaRecepcionTab2.Text = dt.Rows[0]["FechaRecepcion"].ToString();
                //txtCordinadorTab2.Text = dt.Rows[0]["coordinadorMVCS"].ToString();
                //TxtSuperTab2.Text = dt.Rows[0]["SupervisorDesignado"].ToString();
                TxtSuperTab2.Text = dt.Rows[0]["JefeSupervision"].ToString();
                txtResidenteTab2.Text = dt.Rows[0]["ResidenteObra"].ToString();
                txtInspectorTab2.Text = dt.Rows[0]["Inspector"].ToString();
                //txtCoordinadorUETab2.Text = dt.Rows[0]["coordinadorUE"].ToString();
                //txtTelefonoUETab2.Text = dt.Rows[0]["telefonoUE"].ToString();
                //txtCorreoUETab2.Text = dt.Rows[0]["correoUE"].ToString();
                //txtTelefonoCoordinadorUETab2.Text = dt.Rows[0]["telefonocoordinador"].ToString();
                //txtCorreoCoordinadorUETab2.Text = dt.Rows[0]["correocoordinador"].ToString();

                // txtFechaTR.Text = dt.Rows[0]["fechafinalR"].ToString();
                //txtMontoAdelantoDirecTab2.Text = dt.Rows[0]["MontoAdelantoDirecto"].ToString();
                //txtMontoAdelantoMatTab2.Text = dt.Rows[0]["MontoAdelantoMaterial"].ToString();
                //txtFechaAdelantoDirecTab2.Text = dt.Rows[0]["fechaAdelantoDirecto"].ToString();
                //txtFechaAdelantoMatTab2.Text = dt.Rows[0]["fechaAdelantoMaterial"].ToString();

                //lnkbtnFichaRecepcionTab2.Text = dt.Rows[0]["urlFichaRecepcion"].ToString();
                //GeneraIcoFile(lnkbtnFichaRecepcionTab2.Text, imgbtnFichaRecepcionTab2);

                lblNombActuaTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                if (dt.Rows[0]["usuarioUpdateEstado"].ToString().Length > 0)
                {
                    lblFechaEstadoTab2.Text = "Actualizó: " + dt.Rows[0]["usuarioUpdateEstado"].ToString() + " - " + dt.Rows[0]["fechaUpdateEstado"].ToString();
                }
                ActualizaFechasTab1();

                //TxtNotasAdicionales.Text = dt.Rows[0]["notasAdicionales"].ToString();

                //if (TxtNotasAdicionales.Text.Length > 0)
                //{
                //    PanelNotasAdicionales.Visible = true; 

            }
            else
            {
                lblNombreEstadoGeneral.Text = "Actos Previos - Por Convoca (Falta registrar estado)";
                lblIdEstado.Text = "41"; //Actos Previos
                lblIdSubEstado.Text = "70"; //Por Convocar
                lblIdSubEstado2.Text = "";
            }
        }


        protected void CargaDatosSupervisionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            dt = _objBLEjecucion.spMON_EjecucionSupervision(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {

                txtFechaInicioSupervisionTab2.Text = dt.Rows[0]["fechaInicio"].ToString();
                txtPlazoServicioTab2.Text = dt.Rows[0]["plazoEjecucion"].ToString();
                txtJefeSupervisionTab2.Text = dt.Rows[0]["SupervisorDesignado"].ToString();

                if (dt.Rows[0]["flagAdelanto"].ToString() == "1")
                {
                    rbFlagAdelantoTab2.SelectedValue = "1";
                    txtMontoAdelantoSupervisionTab2.Text = dt.Rows[0]["MontoAdelantoDirecto"].ToString();
                    txtFechaAdelantoSupervisionTab2.Text = dt.Rows[0]["fechaAdelantoDirecto"].ToString();
                    Panel_AdelantoSuper.Visible = true;
                }
                else
                {
                    rbFlagAdelantoTab2.SelectedValue = "0";
                    txtMontoAdelantoSupervisionTab2.Text = "0";
                    txtFechaAdelantoSupervisionTab2.Text = "0";
                    Panel_AdelantoSuper.Visible = false;
                }


                //lnkbtnFichaRecepcionTab2.Text = dt.Rows[0]["urlFichaRecepcion"].ToString();
                //GeneraIcoFile(lnkbtnFichaRecepcionTab2.Text, imgbtnFichaRecepcionTab2);

                lblNomActualizaSupervisionTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
            }

        }

        protected void CargaInformes(string tipo, GridView grd)
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;
            _BEEjecucion.id_tipodocumento = tipo;

            grd.DataSource = _objBLEjecucion.F_MON_InformeSupervision(_BEEjecucion);
            grd.DataBind();

        }
        protected void cargaCartasFianzaSupervisionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdFianzaSupervisionTab2.DataSource = _objBLEjecucion.spMON_CartaFianzaSupervision(_BEEjecucion);
            grdFianzaSupervisionTab2.DataBind();
        }
        protected void ActualizaFechasTab1()
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);

                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaFinReprogramado.Text = (fecha.ToString()).Substring(0, 10);

                int diasParalizados;
                diasParalizados = Convert.ToInt32(txtTotalParalizacionTab4.Text);
                fecha = fecha.AddDays(diasParalizados);
                txtFechaTerminoRealFinal.Text = (fecha.ToString()).Substring(0, 10);


                //txtPlazoVigenteTab2.Text = (dias + diasAmpliados + diasParalizados).ToString();
                txtPlazoVigenteTab2.Text = (dias + diasAmpliados).ToString();
            }
        }

        protected void FechaTermino_OnTextChanged(object sender, EventArgs e)
        {

            if (txtFechaInicioTab2.Text == "")
            {
                txtFechaFinContractual.Text = "";
                txtFechaFinReprogramado.Text = "";

            }

            ActualizaFechasTab1();
        }
        protected void btnGuardarInfoTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarEjecucionTab2())
            {

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;

                _BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaInicioTab2.Text);
                _BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                //_BEEjecucion.Date_fechaInicioContractual = VerificaFecha(txtFechaInicioContractualTab2.Text);
                //_BEEjecucion.Date_fechaInicioReal = VerificaFecha(txtFechaInicioReal.Text);
                //_BEEjecucion.Date_fechaFinContractual = VerificaFecha(txtFechaFinContractual.Text);
                _BEEjecucion.Date_fechaFinContractual = VerificaFecha(txtFechaFinContractual.Text);
                _BEEjecucion.Date_fechaFinReal = VerificaFecha(txtFechaFinReprogramado.Text);
                _BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaTerminoReal.Text);

                _BEEjecucion.Date_fechaEntregaTerreno = VerificaFecha(txtFechaEntregaTerrenoTab2.Text);
                //_BEEjecucion.Date_fechaRecepcion = VerificaFecha(txtFechaRecepcionTab2.Text);
                _BEEjecucion.supervisorDesignado = TxtSuperTab2.Text;
                _BEEjecucion.residenteObra = txtResidenteTab2.Text;
                _BEEjecucion.inspector = txtInspectorTab2.Text;

                _BEEjecucion.administrador = txtAdministradorTab2.Text;
                _BEEjecucion.telefono = txtTelefonoAdmTab2.Text;
                _BEEjecucion.correo = txtCorreoAdmTab2.Text;
                //_BEEjecucion.telefonocoordinador = txtTelefonoCoordinadorUETab2.Text;
                //_BEEjecucion.correocoordinador = txtCorreoCoordinadorUETab2.Text;

                //_BEEjecucion.montoAdelantoDirecto = txtMontoAdelantoDirecTab2.Text;
                //_BEEjecucion.montoAdelantoMaterial = txtMontoAdelantoMatTab2.Text;
                //_BEEjecucion.Date_fechaAdelantoDirecto = VerificaFecha(txtFechaAdelantoDirecTab2.Text);
                //_BEEjecucion.Date_fechaAdelantoMaterial = VerificaFecha(txtFechaAdelantoMatTab2.Text);

                _BEEjecucion.Date_fechaFinProbable = VerificaFecha(txtFechaTerminoProbableTab2.Text);

                // _BEEjecucion.urlFichaRecepcion = _Metodo.uploadfile(FileUploadFichaRecepcionTab2);

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spi_MON_EstadoEjecuccionPorContrata(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente el estado de ejecución.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosTab2();

                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdAvanceFisicoTab2_onRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);


            }
        }

        protected void grdAvanceFisicoAdicionalTab2_onRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected Boolean ValidarAvanceTab2()
        {
            Boolean result;
            result = true;


            if (btnGuardarAvanceTab2.Visible == true)
            {
                if (ddlFechaProgramacionInformeTab2.Text == "")
                {
                    string script = "<script>alert('Ingresar fecha programación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }
            if (ddlValorizacionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo Valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToInt32(ddlValorizacionTab2.SelectedValue) > 1)
            {
                if (txtNroAdicionalTab2.Text == "" || txtNroAdicionalTab2.Text == "0")
                {
                    string script = "<script>alert('Ingresar N° Adicional de Valorización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtfechaPresentacionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de presentación a la entidad.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtfechaPresentacionTab2.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de presentación a la entidad.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {
                if (Convert.ToDateTime(txtfechaPresentacionTab2.Text) > DateTime.Today)
                {
                    string script = "<script>alert('La fecha de presentación a la entidad ingresada es mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtMontoTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto sin IGV (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ValidaDecimal(txtMontoTab2.Text) == false)
            {
                string script = "<script>alert('Ingrese monto sin IGV (S/.) valido, por ejemplo: 10000.02 ');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtMontoReajusteTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto de reajuste (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ValidaDecimal(txtMontoReajusteTab2.Text) == false)
            {
                string script = "<script>alert('Ingrese monto reajuste(S/.) valido, por ejemplo: 10000.02 ');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtMontoDirectoTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto de amortización directo (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtMontoMaterialesTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto de amortización de materiales (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFisiRealTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {
                if (Convert.ToDouble(txtFisiRealTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance físico real no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFisiProgTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {
                if (Convert.ToDouble(txtFisiProgTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance físico programado no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }


            if (txtFinanRealTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {
                if (Convert.ToDouble(txtFinanRealTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance financiero real no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }


            if (txtFinanProgTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {
                if (Convert.ToDouble(txtFinanProgTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance financiero programado no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (Convert.ToDouble(txtFisiRealTab2.Text) > 0 && Convert.ToDouble(txtFinanRealTab2.Text) == 0)
            {
                string script = "<script>alert('Debe ingresar el avance financiero real cuando el avance físico real es mayor a 0.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToDouble(txtFisiProgTab2.Text) > 0 && Convert.ToDouble(txtFinanProgTab2.Text) == 0)
            {
                string script = "<script>alert('Debe ingresar el avance financiero programado cuando el avance físico programado es mayor a 0.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (btnGuardarAvanceTab2.Visible == true)
            {
                if (Convert.ToDouble(txtFisiRealTab2.Text) + Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico real acumulado es: " + (Convert.ToDouble(txtFisiRealTab2.Text) + Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico programado acumulado es: " + (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero real acumulado es: " + (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero programado acumulado es: " + (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }

            if (btnModificarAvanceTab2.Visible == true)
            {
                if (Convert.ToDouble(txtFisiRealTab2.Text) + Convert.ToDouble(lblFisRealPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico real acumulado es: " + (Convert.ToDouble(txtFisiRealTab2.Text) + Convert.ToDouble(lblFisRealPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisProgPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico programado acumulado es: " + (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisProgPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero real acumulado es: " + (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero programado acumulado es: " + (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }

            if (txtIGVTab2.Text == "")
            {
                string script = "<script>alert('Ingresar IGV %.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlEstadoSituacionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese estado situacional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaPagoTab2.Text != "")
            {
                //string script = "<script>alert('Ingresar fecha de pago.');</script>";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //result = false;
                //return result;

                if (_Metodo.ValidaFecha(txtFechaPagoTab2.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de pago.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {
                    if (Convert.ToDateTime(txtFechaPagoTab2.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La fecha de pago ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }
            return result;
        }

        protected Boolean ValidarAvanceAdicionalTab2()
        {
            Boolean result;
            result = true;



            if (ddlValorizacionAdicionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo Valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToInt32(ddlValorizacionAdicionalTab2.SelectedValue) > 1)
            {
                if (txtNroAdicionalAdicionalTab2.Text == "" || txtNroAdicionalAdicionalTab2.Text == "0")
                {
                    string script = "<script>alert('Ingrese N° Adicional de Valorización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            //if (txtfechaAdicionalTab2.Text == "")
            //{
            //    string script = "<script>alert('Ingrese fecha.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (_Metodo.ValidaFecha(txtfechaAdicionalTab2.Text) == false)
            //{
            //    string script = "<script>alert('Formato no valido de Fecha de valorización.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}
            //else
            //{

            //    if (Convert.ToDateTime(txtfechaAdicionalTab2.Text) > DateTime.Today)
            //    {
            //        string script = "<script>alert('La fecha de valorización ingresada es mayor a la fecha de hoy.');</script>";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //        result = false;
            //        return result;
            //    }

            //}

            if (ValidaDecimal(txtMontoDirectoAdicional.Text) == false)
            {
                string script = "<script>alert('Ingrese monto amortización directo adicional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ValidaDecimal(txtMontoMaterialesAdicional.Text) == false)
            {
                string script = "<script>alert('Ingrese monto amortización material adicional');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiRealAdicional.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiProgAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFinanRealAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFinanProgAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (ddlEstadoSituacionalAdicionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese estado situacional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }
        protected void ddlEstadoTab2_OnSelecdIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadosDetalle(ddlEstadoTab2.SelectedValue);

            if (ddlEstadoTab2.SelectedValue == "5")
            {
                ddlSubEstadoTab2.SelectedValue = "82";
                spSubestado.Visible = true;
                ddlSubEstadoTab2.Visible = true;

                //ddlSubEstado2Tab2.SelectedValue = "";
                spSubestado2.Visible = true;
                ddlSubEstado2Tab2.Visible = true;

                CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                
                lblNombreProblematicaTab2.Visible = true;
                //ddlTipoProblematicaTab2.Visible = true;
                chbProblematicaTab2.Visible = true;
                Up_NombreProblematicaTab2.Update();
                Up_TipoProblematicaTab2.Update();

                CargaTipoSubEstadoParalizado(ddlSubEstado2Tab2.SelectedValue);
                //Up_SubEstado2DetalleTab2.Update();
            }
            else
            {
                lblNombreProblematicaTab2.Visible = false;
                //ddlTipoProblematicaTab2.Visible = false;
                chbProblematicaTab2.Visible = false;
                Up_NombreProblematicaTab2.Update();
                Up_TipoProblematicaTab2.Update();


                if (ddlSubEstadoTab2.Visible == false)
                {
                    //ddlSubEstadoTab2.SelectedValue = "";
                    //spSubestado.Visible = false;
                    //ddlSubEstadoTab2.Visible = false;

                    ddlSubEstado2Tab2.SelectedValue = "";
                    spSubestado2.Visible = false;
                    ddlSubEstado2Tab2.Visible = false;

                }
                else
                {
                    //ddlSubEstadoTab2.SelectedValue = "";
                    //spSubestado.Visible = true;
                    //ddlSubEstadoTab2.Visible = true;

                    ddlSubEstado2Tab2.SelectedValue = "";
                    spSubestado2.Visible = false;
                    ddlSubEstado2Tab2.Visible = false;

                }

            }

            if (ddlSubEstadoTab2.SelectedValue.Equals("87")) // SUSPENSION DE PLAZO
            {
                trSuspensionTab2.Visible = true;
                trReinicioTab2.Visible = true;
            }
            else
            {
                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
            }
            Up_FechasSuspensionTab2.Update();

            Up_EstadoSituacional.Update();
        }

        protected void btnGuardarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {
                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_Avance.Text);
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionTab2.SelectedValue;

                    _BEEjecucion.Date_fechaInforme = Convert.ToDateTime(txtfechaPresentacionTab2.Text);
                    _BEEjecucion.monto = txtMontoTab2.Text;
                    _BEEjecucion.montoReajuste = txtMontoReajusteTab2.Text;
                    _BEEjecucion.montoAdelantoDirecto = txtMontoDirectoTab2.Text;
                    _BEEjecucion.montoAdelantoMaterial = txtMontoMaterialesTab2.Text;
                    _BEEjecucion.montoEjecutado = txtValorizacionEjecutadaTab2.Text;
                    _BEEjecucion.FisicoEjec = txtFisiRealTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgTab2.Text;
                    _BEEjecucion.porcentaje = txtIGVTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalTab2.SelectedValue);
                    _BEEjecucion.observacion = txtObservaciontab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    _BEEjecucion.Date_fechaPago = VerificaFecha(txtFechaPagoTab2.Text);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    //_BEEjecucion.NroAdicional = txtNroAdicionalTab2.Text;

                    int val = _objBLEjecucion.U_spu_MON_InformeAvancePorContrata(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoTab2.Text = "";
                        //txtfechaTab2.Text = "";
                        txtFisiRealTab2.Text = "";
                        txtFisiProgTab2.Text = "";
                        txtFinanRealTab2.Text = "";
                        txtFinanProgTab2.Text = "";
                        //ddlEstadoSituacionalTab2.SelectedValue = "";
                        txtObservaciontab2.Text = "";
                        ddlValorizacionTab2.SelectedValue = "";
                        txtNroAdicionalTab2.Text = "";

                        Panel_AgregarAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        protected void btnGuardarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceAdicionalTab2())
            {
                if (validaArchivo(FileUploadAvanceAdicionalTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;

                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionAdicionalTab2.SelectedValue;
                    _BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;
                    _BEEjecucion.nroValorizacionAdicional = txtValorizacionAdicionalTab2.Text;
                    _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionAdicionalTab2.Text);
                    _BEEjecucion.monto = txtMontoSinIGVAdicionalTab2.Text;
                    _BEEjecucion.montoReajuste = txtMontoReajusteAdicionalTab2.Text;
                    _BEEjecucion.montoAdelantoDirecto = txtMontoDirectoAdicional.Text;
                    _BEEjecucion.montoAdelantoMaterial = txtMontoMaterialesAdicional.Text;
                    _BEEjecucion.fisicoReal = txtFisiRealAdicional.Text;
                    _BEEjecucion.fisicoProgramado = txtFisiProgAdicionalTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealAdicionalTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgAdicionalTab2.Text;
                    _BEEjecucion.porcentaje = txtIGVAdicionalTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalAdicionalTab2.SelectedValue);
                    _BEEjecucion.observacion = txtObservacionAdicionaltab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                    _BEEjecucion.Date_fechaPago = VerificaFecha(txtFechaPagoAdicionalTab2.Text);
                    //_BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaAdicionalTab2.Text);
                    //_BEEjecucion.monto = txtMontoAdicionalTab2.Text;

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_spi_MON_InformeAvanceAdicionalPorContrata(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoDirectoAdicional.Text = "";
                        txtMontoMaterialesAdicional.Text = "";
                        txtFisiRealAdicional.Text = "";
                        txtFisiProgAdicionalTab2.Text = "";
                        txtFinanRealAdicionalTab2.Text = "";
                        txtFinanProgAdicionalTab2.Text = "";
                        ddlEstadoSituacionalAdicionalTab2.SelectedValue = "";
                        txtObservacionAdicionaltab2.Text = "";
                        ddlValorizacionAdicionalTab2.SelectedValue = "";
                        txtNroAdicionalAdicionalTab2.Text = "";

                        Panel_AgregarAvanceAdicionalTab2.Visible = false;
                        imgbtnAvanceFisicoAdicionalTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        protected Boolean ValidarFianzaTab2()
        {
            Boolean result;
            result = true;

            if (txtEntidadFinancieraTab2.Text == "")
            {
                string script = "<script>alert('Ingrese Entidad Financiera.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtNroCartaF.Text == "")
            {
                string script = "<script>alert('Ingrese N° Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
          

            if (ddlTipoTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFechaInicioVigenciaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio de vigencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFechaFinVigenciaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de fin de vigencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtmontoFianzaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtPorcentajeTab2.Text == "")
            {
                string script = "<script>alert('Ingrese % de cubrimiento de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtPorcentajeTab2.Text != "")
            {
                if (lblCOD_SUBSECTOR.Text.Equals("1"))
                {
                    if (ddlTipoTab2.SelectedValue == "1")
                    {
                        if (Convert.ToDouble(txtPorcentajeTab2.Text) > 10.00)
                        {
                            string script = "<script>alert('El % de cubrimiento debe ser menor a 10%');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            result = false;
                            return result;
                        }

                    }

                    if (ddlTipoTab2.SelectedValue == "2")
                    {
                        if (Convert.ToDouble(txtPorcentajeTab2.Text) > 20.00)
                        {
                            string script = "<script>alert('El % de cubrimiento debe ser menor a 20%');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            result = false;
                            return result;
                        }

                    }

                    if (ddlTipoTab2.SelectedValue == "3")
                    {
                        if (Convert.ToDouble(txtPorcentajeTab2.Text) > 40.00)
                        {
                            string script = "<script>alert('El % de cubrimiento debe ser menor a 40%');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            result = false;
                            return result;
                        }

                    }
                }
            }

            if (ddlTipoAccion.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de acción.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            } else if (ddlTipoAccion.SelectedValue.Equals("2") && txtFechaRenovacion.Text=="")
            {
                string script = "<script>alert('Ingresar fecha de renovación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }

        protected void btnCancelarFianzasTab2_OnClick(object sender, EventArgs e)
        {
            Panel_fianzas.Visible = false;
            imgbtnAgregarFianzasTab4.Visible = true;
            Up_Tab2.Update();
        }

        protected void ddlValorizacionTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlValorizacionTab2.SelectedValue == "1" || ddlValorizacionTab2.SelectedValue == "")
            {
                lblAdicional.Visible = false;
                txtNroAdicionalTab2.Visible = false;

            }
            else
            {
                lblAdicional.Visible = true;
                txtNroAdicionalTab2.Visible = true;
            }

            Up_lblAdicionalTab2.Update();
            Up_txtAdicionalTab2.Update();
        }

        protected void btnGuardarFianzasTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarFianzaTab2())
            {
                if (validaArchivo(FileUploadCartaTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                    _BEEjecucion.entidadFinanciera = txtEntidadFinancieraTab2.Text;
                    _BEEjecucion.nroCarta = txtNroCartaF.Text;
                    _BEEjecucion.tipoCarta = Convert.ToInt32(ddlTipoTab2.SelectedValue);

                    _BEEjecucion.Date_fechaInicio = Convert.ToDateTime(txtFechaInicioVigenciaTab2.Text);
                    _BEEjecucion.Date_fechaFin = Convert.ToDateTime(txtFechaFinVigenciaTab2.Text);
                    _BEEjecucion.Date_fechaRenovacion = Convert.ToDateTime(VerificaFecha(txtFechaRenovacion.Text));
                    _BEEjecucion.monto = txtmontoFianzaTab2.Text;

                    _BEEjecucion.porcentaje = txtPorcentajeTab2.Text;
                    if (chkVerificado.Checked == true)
                    {
                        _BEEjecucion.flagVerificado = "1";
                    }

                    //if (chkRenovado.Checked == true)
                    //{
                    //    _BEEjecucion.flagRenovado = "1";
                    //}

                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaTab2);
                    _BEEjecucion.idTipoAccion = ddlTipoAccion.SelectedValue;
                    _BEEjecucion.urlDocAccion = _Metodo.uploadfile(FileUploadAccionTab2);

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.spi_MON_CartaFianza(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaCartasFianzasTab2();

                        txtEntidadFinancieraTab2.Text = "";
                        txtNroCartaF.Text = "";
                        ddlTipoTab2.SelectedValue = "";
                        txtFechaInicioVigenciaTab2.Text = "";
                        txtFechaFinVigenciaTab2.Text = "";
                        txtmontoFianzaTab2.Text = "";
                        txtPorcentajeTab2.Text = "";

                        Panel_fianzas.Visible = false;
                        imgbtnAgregarFianzasTab4.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        protected void totalMontoGrdTab2(int tipo, Label txt)
        {
            _BEFinanciamiento.tipoGrd = tipo;
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            txt.Text = _objBLFinanciamiento.F_spMON_MontoTotalGrd(_BEFinanciamiento);
        }

        protected void btnGuardarResidenteTab2_OnClick(object sender, EventArgs e)
        {
            if (txtResidenteObraTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de residente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIPR.Text.Trim() == "" && txtCapR.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 1;

                _BEEjecucion.empleadoObra = txtResidenteObraTab2.Text;
                _BEEjecucion.CIP = txtCIPR.Text;
                _BEEjecucion.telefono = txtTelefonoR.Text;
                _BEEjecucion.correo = txtCorreoR.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCapR.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaR.Text);
                _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadDocResidenteTab2);

                int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtResidenteTab2.Text = txtResidenteObraTab2.Text;
                    txtResidenteObraTab2.Text = "";
                    txtTelefonoR.Text = "";
                    txtCorreoR.Text = "";
                    txtCIPR.Text = "";
                    txtCapR.Text = "";
                    txtFechaR.Text = "";


                    //RESIDENTE
                    dt = CargaEmpleadoObraTab2(1);
                    grd_HistorialResidenteTab2.DataSource = dt;
                    grd_HistorialResidenteTab2.DataBind();

                    Panel_HistorialResidenteTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
            //}





        }

        //protected void btnGuardarSupervisorTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtSupervisorS.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else if (txtCIPS.Text.Trim() == "" && txtCapS.Text.Trim() == "")
        //    {
        //        string script = "<script>alert('Ingresar CIP o CAP.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEEjecucion.tipoempleado = 3;

        //        _BEEjecucion.empleadoObra = txtSupervisorS.Text;
        //        _BEEjecucion.CIP = txtCIPS.Text;
        //        _BEEjecucion.telefono = txtTelefonoS.Text;
        //        _BEEjecucion.correo = txtCorreoS.Text;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.cap = txtCapS.Text;
        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaS.Text);
        //        int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se registró supervisor correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtSupervisorS.Text = "";
        //            txtTelefonoS.Text = "";
        //            txtCIPS.Text = "";
        //            txtCorreoS.Text = "";
        //            txtCapS.Text = "";
        //            txtFechaS.Text = "";

        //            //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);

        //            //SUPERVISOR
        //            dt = CargaEmpleadoObraTab2(3);
        //            grd_HistorialSupervisorTab2.DataSource = dt;
        //            grd_HistorialSupervisorTab2.DataBind();

        //            Panel_HistorialSupervisorTab2.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        protected void btnGrabarInspectorTab2_OnClick(object sender, EventArgs e)
        {
            if (txtInspector.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de inspector.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIP.Text.Trim() == "" && txtCap.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 2;

                _BEEjecucion.empleadoObra = txtInspector.Text;
                _BEEjecucion.telefono = txtTelefono.Text;
                _BEEjecucion.correo = txtCorreo.Text;
                _BEEjecucion.CIP = txtCIP.Text;
                _BEEjecucion.cap = txtCap.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaP.Text);
                _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadDocInspectorTab2);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtInspectorTab2.Text = txtInspector.Text;
                    txtInspector.Text = "";
                    txtTelefono.Text = "";
                    txtCorreo.Text = "";
                    txtCIP.Text = "";
                    txtCap.Text = "";
                    txtFechaP.Text = "";

                    //INSPECTOR
                    dt = CargaEmpleadoObraTab2(2);
                    grd_HistorialInspectorTab2.DataSource = dt;
                    grd_HistorialInspectorTab2.DataBind();

                    Panel_HistorialInspectorTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnModificarAvanceTab2_OnClick(object sender, EventArgs e)
        {

            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {
                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_Avance.Text);
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionTab2.SelectedValue;

                    _BEEjecucion.Date_fechaInforme = Convert.ToDateTime(txtfechaPresentacionTab2.Text);
                    _BEEjecucion.monto = txtMontoTab2.Text;
                    _BEEjecucion.montoReajuste = txtMontoReajusteTab2.Text;
                    _BEEjecucion.montoAdelantoDirecto = txtMontoDirectoTab2.Text;
                    _BEEjecucion.montoAdelantoMaterial = txtMontoMaterialesTab2.Text;
                    _BEEjecucion.montoEjecutado = txtValorizacionEjecutadaTab2.Text;
                    _BEEjecucion.FisicoEjec = txtFisiRealTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgTab2.Text;
                    _BEEjecucion.porcentaje = txtIGVTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalTab2.SelectedValue);
                    _BEEjecucion.observacion = txtObservaciontab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    _BEEjecucion.Date_fechaPago = VerificaFecha(txtFechaPagoTab2.Text);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    //_BEEjecucion.NroAdicional = txtNroAdicionalTab2.Text;

                    int val = _objBLEjecucion.U_spu_MON_InformeAvancePorContrata(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se modificó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoTab2.Text = "";
                        //txtfechaTab2.Text = "";
                        txtFisiRealTab2.Text = "";
                        txtFisiProgTab2.Text = "";
                        txtFinanRealTab2.Text = "";
                        txtFinanProgTab2.Text = "";
                        //ddlEstadoSituacionalTab2.SelectedValue = "";
                        txtObservaciontab2.Text = "";
                        ddlValorizacionTab2.SelectedValue = "";
                        txtNroAdicionalTab2.Text = "";

                        Panel_AgregarAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }
        }

        protected void btnModificarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {

            if (ValidarAvanceAdicionalTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {
                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_AvanceAdicional.Text);
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionAdicionalTab2.SelectedValue;

                    _BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;
                    _BEEjecucion.nroValorizacionAdicional = txtValorizacionAdicionalTab2.Text;
                    _BEEjecucion.Date_fechaInforme = Convert.ToDateTime(txtFechaPresentacionAdicionalTab2.Text);
                    _BEEjecucion.monto = txtMontoSinIGVAdicionalTab2.Text;
                    _BEEjecucion.montoReajuste = txtMontoReajusteAdicionalTab2.Text;

                    _BEEjecucion.amortizacionDirecto = txtMontoDirectoAdicional.Text;
                    _BEEjecucion.amortizacionMateriales = txtMontoMaterialesAdicional.Text;
                    _BEEjecucion.fisicoReal = txtFisiRealAdicional.Text;
                    _BEEjecucion.fisicoProgramado = txtFisiProgAdicionalTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealAdicionalTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgAdicionalTab2.Text;

                    _BEEjecucion.porcentaje = txtIGVAdicionalTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalAdicionalTab2.SelectedValue);
                    _BEEjecucion.observacion = txtObservacionAdicionaltab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                    _BEEjecucion.Date_fechaPago = VerificaFecha(txtFechaPagoAdicionalTab2.Text);
                    //_BEEjecucion.Date_fechaPago = Convert.ToDateTime(txtFechaPagoAdicionalTab2.Text);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    if (FileUploadAvanceAdicionalTab2.PostedFile.ContentLength > 0)
                    {
                        _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDoc = LnkbtnAvanceAdicionalTab2.Text;
                    }

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int resul;
                    resul = _objBLEjecucion.U_spi_MON_InformeAvanceAdicionalPorContrata(_BEEjecucion);
                    if (resul == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        // ddlValorizacionTab2.SelectedValue = "";
                        txtMontoDirectoAdicional.Text = "";
                        txtMontoMaterialesAdicional.Text = "";
                        txtFisiRealAdicional.Text = "";
                        txtFisiProgAdicionalTab2.Text = "";
                        txtFinanRealAdicionalTab2.Text = "";
                        txtFinanProgAdicionalTab2.Text = "";
                        txtObservacionAdicionaltab2.Text = "";
                        ddlEstadoSituacionalAdicionalTab2.SelectedValue = "";
                        txtNroAdicionalAdicionalTab2.Text = "";
                        LnkbtnAvanceAdicionalTab2.Text = "";
                        imgbtnAvanceAdicionalTab2.Visible = false;
                        imgbtnAvanceFisicoAdicionalTab2.Visible = true;

                        CargaAvanceFisico();



                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                    Panel_AgregarAvanceAdicionalTab2.Visible = false;
                    //CargaAvanceFisico();
                    btnModificarAvanceAdicionalTab2.Visible = true;
                    Up_Tab2.Update();
                }

            }
        }

        protected void btnModificarFianzasTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarFianzaTab2())
            {
                _BEEjecucion.id_carta_fianza = Convert.ToInt32(lblId_Registro_Carta_Fianza.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEEjecucion.entidadFinanciera = txtEntidadFinancieraTab2.Text;
                _BEEjecucion.nroCarta = txtNroCartaF.Text;
                _BEEjecucion.tipoCarta = Convert.ToInt32(ddlTipoTab2.SelectedValue);
                _BEEjecucion.Date_fechaInicio = Convert.ToDateTime(txtFechaInicioVigenciaTab2.Text);
                _BEEjecucion.Date_fechaFin = Convert.ToDateTime(txtFechaFinVigenciaTab2.Text);
                _BEEjecucion.Date_fechaRenovacion = Convert.ToDateTime(VerificaFecha(txtFechaRenovacion.Text));
                _BEEjecucion.monto = txtmontoFianzaTab2.Text;
                _BEEjecucion.porcentaje = txtPorcentajeTab2.Text;

                _BEEjecucion.idTipoAccion = ddlTipoAccion.SelectedValue;
                if (chkVerificado.Checked == true)
                {
                    _BEEjecucion.flagVerificado = "1";
                }

                //if (chkRenovado.Checked == true)
                //{
                //    _BEEjecucion.flagRenovado = "1";
                //}


                if (FileUploadCartaTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = LnkbtnCartaTab2.Text;
                }

                if (FileUploadAccionTab2.Visible == true &&  FileUploadAccionTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDocAccion = _Metodo.uploadfile(FileUploadAccionTab2);
                }
                else
                {
                    _BEEjecucion.urlDocAccion = lnkbtnAccionTab2.Text;
                }

                //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaTab2);


                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLEjecucion.spud_MON_Seguimiento_Carta_Fianza_Editar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaCartasFianzasTab2();

                    txtEntidadFinancieraTab2.Text = "";
                    txtNroCartaF.Text = "";
                    ddlTipoTab2.SelectedValue = "";
                    txtFechaInicioVigenciaTab2.Text = "";
                    txtFechaFinVigenciaTab2.Text = "";
                    txtmontoFianzaTab2.Text = "";
                    txtPorcentajeTab2.Text = "";
                    LnkbtnCartaTab2.Text = "";
                    lnkbtnAccionTab2.Text = "";
                    
                    imgbtnCartaTab2.Visible = false;
                    imgbtnAgregarFianzasTab4.Visible = true;
                    Panel_fianzas.Visible = false;
                    chkVerificado.Checked = false;
                    //chkRenovado.Checked = false;
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_fianzas.Visible = false;
                    cargaCartasFianzasTab2();
                    //btnModificarFianzasTab2.Visible = true;

                    Up_Tab2.Update();

                }
            }
        }

        protected void imgbtnAgregarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            //Nuevo registro
            lblID_evaluacionTab2.Text = "0";

            string flagContinuarRegistro = "1";
            string msjContinuarRegistro = "";

            //if (lblIdSubEstado.Text.Equals("87")) //SUSPENSION  VERIFICAR REGISTRO DE FECHA DE REINICIO
            //{
            //    dt = new DataTable();
            //    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            //    dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);

            //    if (dt.Rows.Count > 0)
            //    {
            //        string vIdSubEstado = (dt.Rows[dt.Rows.Count - 1]["id_tipoSubEstadoEjecucion"]).ToString();
            //        string fechaReinicio = (dt.Rows[dt.Rows.Count - 1]["fechaReinicio"]).ToString();
            //        if (vIdSubEstado.Equals("87") && fechaReinicio.Length <= 1)
            //        {
            //            flagContinuarRegistro = "0";
            //            msjContinuarRegistro = "Para continuar con el registro del Avance, debe registrar la fecha de reinicio de la Obra.";
            //        }
            //    }
            //}

            if (flagContinuarRegistro.Equals("0"))
            {
                string script = "<script>alert('" + msjContinuarRegistro + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                Panel_ObservacionTab2.Visible = true;
                imgbtnAgregarEvaluacionTab2.Visible = false;
                lblNomUsuarioObservaciones.Text = "";
                btnGuardarEvaluacionTab2.Visible = true;
                btnModificarEvaluacionTab2.Visible = false;

                chbAyudaMemoriaTab4.Checked = false;
                txtFechaVisitaTab2.Text = "";
                txtEvaluacionTab2.Text = "";
                txtRecomendacionTab2.Text = "";
                txtProgramadoTab2.Text = "";
                txtEjecutadoTab2.Text = "";
                ddlTipoMonitoreo.SelectedValue = "";
                txtNroTramie.Text = "";
                txtTipoDocumento.Text = "";
                ddlTipoDocumento.SelectedValue = "";
                txtAsunto.Text = "";
                txtProgramadoTab2.Text = "0";
                txtEjecutadoTab2.Text = "0";
                txtFinanEjecutadoTab2.Text = "0";
                txtFinanProgramadoTab2.Text = "0";

                lblNomUsuarioFlagAyudaTab4.Text = "";

                imgbtnActaTab4.ImageUrl = "~/img/blanco.png";
                imgbtnInformeTab4.ImageUrl = "~/img/blanco.png";
                imgbtnOficioTab4.ImageUrl = "~/img/blanco.png";

                LnkbtnInformeTab4.Text = "";
                LnkbtnOficioTab4.Text = "";
                LnkbtnActaTab4.Text = "";

                ddlEstadoTab2.SelectedValue = "";
                ddlSubEstadoTab2.SelectedValue = "";
                ddlSubEstado2Tab2.SelectedValue = "";

                chbAyudaMemoriaTab4.Enabled = true;
                ddlEstadoTab2.Enabled = true;
                ddlSubEstadoTab2.Enabled = true;
                ddlSubEstado2Tab2.Enabled = true;

                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
                Up_FechasSuspensionTab2.Update();

                chbProblematicaTab2.Items.Clear();
                txtFechaInicioSuspensionTab2.Text = "";
                txtFechaFinalReinicioTab2.Text = "";

                Panel_InformacionComplementaria.Visible = false;
                Up_Tab2.Update();
            }
        }

        //protected void grd_HistorialSupervisorTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grd_HistorialSupervisorTab2.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
        //        _BEEjecucion.Tipo = 2;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.Date_fecha = DateTime.Now;
        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            // TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtSupervisorS.Text = "";
        //            txtTelefonoS.Text = "";
        //            txtCIPS.Text = "";
        //            txtCorreoS.Text = "";
        //            dt = CargaEmpleadoObraTab2(3);
        //            if (dt.Rows.Count > 0)
        //            {

        //                TxtSuperTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                TxtSuperTab2.Text = "";

        //            }


        //            Panel_HistorialSupervisorTab2.Visible = false;
        //            Up_Tab2.Update();



        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grd_HistorialSupervisorTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id;
        //    id = grd_HistorialSupervisorTab2.SelectedDataKey.Value.ToString();
        //    lblIDSupervisor.Text = id;
        //    GridViewRow row = grd_HistorialSupervisorTab2.SelectedRow;

        //    txtSupervisorS.Text = ((Label)row.FindControl("lblnombreS")).Text;
        //    txtCIPS.Text = ((Label)row.FindControl("lblCIP")).Text;
        //    txtTelefonoS.Text = ((Label)row.FindControl("lblTelefonoS")).Text;
        //    txtCorreoS.Text = ((Label)row.FindControl("lblCorreoS")).Text;
        //    txtCapS.Text = ((Label)row.FindControl("lblCapS")).Text;
        //    txtFechaS.Text = ((Label)row.FindControl("lblFechaSS")).Text;

        //    btnGuardarSupervisorTab2.Visible = false;
        //    btnModificarSupervisorTab2.Visible = true;
        //    Panel_HistorialSupervisorTab2.Visible = true;
        //    Up_Tab2.Update();
        //    //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    //btnModificarContrapartidasTab0.Visible = true;
        //    //btnGuardarContrapartidasTab0.Visible = false;
        //    //Panel_ContrapartidaTab0.Visible = true;
        //    //btnAgregarContrapartidasTab0.Visible = false;
        //    //Up_Tab0.Update();


        //}

        //protected void btnModificarSupervisorTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtSupervisorS.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else if (txtCIPS.Text.Trim() == "" && txtCapS.Text.Trim() == "")
        //    {
        //        string script = "<script>alert('Ingresar CIP o CAP.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIDSupervisor.Text);
        //        _BEEjecucion.Tipo = 1;
        //        // _BEEjecucion.empleadoObra = txtu.Text;
        //        _BEEjecucion.empleadoObra = txtSupervisorS.Text;
        //        _BEEjecucion.CIP = txtCIPS.Text;
        //        _BEEjecucion.telefono = txtTelefonoS.Text;
        //        _BEEjecucion.correo = txtCorreoS.Text;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.cap = txtCapS.Text;
        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaS.Text);

        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se modificó el supervisor correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtSupervisorS.Text = "";
        //            txtTelefonoS.Text = "";
        //            txtCIPS.Text = "";
        //            txtCorreoS.Text = "";
        //            txtCapS.Text = "";
        //            txtFechaS.Text = "";

        //            //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
        //            dt = CargaEmpleadoObraTab2(3);
        //            if (dt.Rows.Count > 0)
        //            {

        //                TxtSuperTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                TxtSuperTab2.Text = "";

        //            }

        //            Panel_HistorialSupervisorTab2.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        protected void grd_HistorialResidenteTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialResidenteTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta de residente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtResidenteObraTab2.Text = "";
                    txtTelefonoR.Text = "";
                    txtCIPR.Text = "";
                    txtCorreoR.Text = "";
                    dt = CargaEmpleadoObraTab2(1);
                    grd_HistorialResidenteTab2.DataSource = dt;
                    grd_HistorialResidenteTab2.DataBind();

                    if (dt.Rows.Count > 0)
                    {

                        txtResidenteTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtResidenteTab2.Text = "";

                    }


                    Panel_HistorialResidenteTab2.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialResidenteTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialResidenteTab2.SelectedDataKey.Value.ToString();
            lblId_Residente.Text = id;
            GridViewRow row = grd_HistorialResidenteTab2.SelectedRow;

            txtResidenteObraTab2.Text = ((Label)row.FindControl("lblResidente")).Text;
            txtCIPR.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtTelefonoR.Text = ((Label)row.FindControl("lblTelefonoR")).Text;
            txtCorreoR.Text = ((Label)row.FindControl("lblCorreoR")).Text;
            txtCapR.Text = ((Label)row.FindControl("lblcapR")).Text;
            txtFechaR.Text = ((Label)row.FindControl("lblFechaRR")).Text;

            lnkbtnDocResidenteTab2.Text = ((ImageButton)row.FindControl("imgDocResidenteGri")).ToolTip;
            GeneraIcoFile(lnkbtnDocResidenteTab2.Text, imgbtnDocResidenteTab2);

            btnGuardarResidenteTab2.Visible = false;
            btnModificarResidenteTab2.Visible = true;
            Panel_HistorialResidenteTab2.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }

        protected void btnModificarResidenteTab2_OnClick(object sender, EventArgs e)
        {
            if (txtResidenteObraTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de residente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIPR.Text.Trim() == "" && txtCapR.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblId_Residente.Text);
                _BEEjecucion.Tipo = 1;
                // _BEEjecucion.empleadoObra = txtu.Text;
                _BEEjecucion.empleadoObra = txtResidenteObraTab2.Text;
                _BEEjecucion.CIP = txtCIPR.Text;
                _BEEjecucion.telefono = txtTelefonoR.Text;
                _BEEjecucion.correo = txtCorreoR.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCapR.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaR.Text);

                if (FileUploadDocResidenteTab2.HasFile)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadDocResidenteTab2);
                }
                else
                { _BEEjecucion.urlDoc = lnkbtnDocResidenteTab2.Text; }

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);


                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el residente correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtResidenteObraTab2.Text = "";
                    txtTelefonoR.Text = "";
                    txtCIPR.Text = "";
                    txtCorreoR.Text = "";
                    txtCapR.Text = "";
                    txtFechaR.Text = "";


                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(1);
                    grd_HistorialResidenteTab2.DataSource = dt;
                    grd_HistorialResidenteTab2.DataBind();

                    if (dt.Rows.Count > 0)
                    {

                        txtResidenteTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtResidenteTab2.Text = "";

                    }

                    Panel_HistorialResidenteTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialInspectorTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialInspectorTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Date_fecha = DateTime.Now;
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta de Inspector.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtInspector.Text = "";
                    txtTelefono.Text = "";
                    txtCIP.Text = "";
                    txtCorreo.Text = "";
                    dt = CargaEmpleadoObraTab2(2);
                    grd_HistorialInspectorTab2.DataSource = dt;
                    grd_HistorialInspectorTab2.DataBind();

                    if (dt.Rows.Count > 0)
                    {

                        txtInspectorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtInspectorTab2.Text = "";

                    }


                    Panel_HistorialInspectorTab2.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialInspectorTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialInspectorTab2.SelectedDataKey.Value.ToString();
            lblID_Inspector.Text = id;
            GridViewRow row = grd_HistorialInspectorTab2.SelectedRow;

            txtInspector.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtCIP.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtTelefono.Text = ((Label)row.FindControl("lblTelefono")).Text;
            txtCorreo.Text = ((Label)row.FindControl("lblCorreo")).Text;
            txtCap.Text = ((Label)row.FindControl("lblCap")).Text;
            txtFechaP.Text = ((Label)row.FindControl("lblFechaII")).Text;

            lnkbtnDocInspectorTab2.Text = ((ImageButton)row.FindControl("imgDocInspectorGri")).ToolTip;
            GeneraIcoFile(lnkbtnDocInspectorTab2.Text, imgbtnDocInspectorTab2);

            btnGrabarInspectorTab2.Visible = false;
            btnModificarInspectorTab2.Visible = true;
            Panel_HistorialInspectorTab2.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }

        protected void btnModificarInspectorTab2_OnClick(object sender, EventArgs e)
        {
            if (txtInspector.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de Inspector.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIP.Text.Trim() == "" && txtCap.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblID_Inspector.Text);
                _BEEjecucion.Tipo = 1;
                // _BEEjecucion.empleadoObra = txtu.Text;
                _BEEjecucion.empleadoObra = txtInspector.Text;
                _BEEjecucion.CIP = txtCIP.Text;
                _BEEjecucion.telefono = txtTelefono.Text;
                _BEEjecucion.correo = txtCorreo.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCap.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaP.Text);

                if (FileUploadDocInspectorTab2.HasFile)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadDocInspectorTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = lnkbtnDocInspectorTab2.Text;
                }

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el Inspector correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtInspector.Text = "";
                    txtTelefono.Text = "";
                    txtCIP.Text = "";
                    txtCorreo.Text = "";
                    txtCap.Text = "";
                    txtFechaP.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(2);
                    grd_HistorialInspectorTab2.DataSource = dt;
                    grd_HistorialInspectorTab2.DataBind();

                    if (dt.Rows.Count > 0)
                    {

                        txtInspectorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtInspectorTab2.Text = "";

                    }

                    Panel_HistorialInspectorTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void cargarEvaluacionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                lblAvanceEjecutadoInfomeTab2.Text = (dt.Rows[dt.Rows.Count - 1]["fisicoEjecutado"]).ToString();
                lblAvanceProgramadoInfomeTab2.Text = (dt.Rows[dt.Rows.Count - 1]["fisicoProgramado"]).ToString();
            }
            grdEvaluacionTab2.DataSource = dt;
            grdEvaluacionTab2.DataBind();



        }

        protected void btnGuardarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_OficioTab4))
            {
                if (validaArchivo(FileUpload_InformeTab4))
                {
                    if (validaArchivo(FileUpload_ActaTab4))
                    {
                        if (ValidarEvaluacionRecomendacionTab2())
                        {

                            /*inicio guardar estado*/
                            if (ValidarEstadoSituacional())
                            {

                                /*otro guardado inicio*/
                                _BEEjecucion.tipoFinanciamiento = 3;
                                _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                                _BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;
                                _BEEjecucion.id_tipoEstadoParalizado = ddlSubEstado2Tab2.SelectedValue;
                                string vDetalleProblemas = "";
                                if (ddlSubEstadoTab2.SelectedValue.Equals("82"))//Paralizados permanentes
                                {
                                    //List<ListItem> selected = new List<ListItem>();
                                    foreach (ListItem item in chbProblematicaTab2.Items)
                                    {
                                        //string myJson = "{'Username': 'myusername','Password':'pass'}";
                                        vDetalleProblemas = vDetalleProblemas + "{\"idSubEstado\":\"" + ddlSubEstadoTab2.SelectedValue + "\", \"idProblematica\":\"" + item.Value;
                                        if (item.Selected)
                                        {
                                            vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"1\"},";
                                        }
                                        else
                                        {
                                            vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"0\"},";
                                        }
                                    }

                                    vDetalleProblemas = vDetalleProblemas.Substring(0, vDetalleProblemas.Length - 1);

                                    vDetalleProblemas = "[" + vDetalleProblemas + "]";
                                }
                                else
                                {
                                    if (ddlSubEstadoTab2.SelectedValue.Equals("87"))//Suspension
                                    {
                                        _BEEjecucion.Date_fechaInicio = _Metodo.VerificaFecha(txtFechaInicioSuspensionTab2.Text);
                                        _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinalReinicioTab2.Text);
                                    }
                                }
                                _BEEjecucion.detalleProblematica = vDetalleProblemas;
                                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                                _BEEjecucion.Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text);
                                _BEEjecucion.Evaluacion = txtEvaluacionTab2.Text;
                                _BEEjecucion.Recomendacion = txtRecomendacionTab2.Text;
                                _BEEjecucion.FisicoEjec = txtEjecutadoTab2.Text;
                                _BEEjecucion.FisicoProg = txtProgramadoTab2.Text;

                                _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab4);
                                _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab4);
                                _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab4);
                                
                                _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                                _BEEjecucion.nrotramite = txtNroTramie.Text;

                                //_BEEjecucion.tipodocumento = txtTipoDocumento.Text;
                                _BEEjecucion.id_tipodocumento = ddlTipoDocumento.SelectedValue;
                                _BEEjecucion.asunto = txtAsunto.Text;
                                _BEEjecucion.financieroReal = txtFinanEjecutadoTab2.Text;
                                _BEEjecucion.financieroProgramado = txtFinanProgramadoTab2.Text;

                                if (chbAyudaMemoriaTab4.Checked == true)
                                {
                                    _BEEjecucion.flagAyuda = "1";
                                }
                                else
                                {
                                    _BEEjecucion.flagAyuda = "0";
                                }

                                int val;

                                val = _objBLEjecucion.spi_MON_EvaluacionRecomendacion(_BEEjecucion);
                                lblID_evaluacionTab2.Text = val.ToString();


                                if (val > 0)
                                {


                                    cargarEvaluacionTab2();
                                    CargaDatosTab2();

                                    //txtFechaVisitaTab2.Text = "";
                                    //txtEvaluacionTab2.Text = "";
                                    //txtRecomendacionTab2.Text = "";
                                    //txtProgramadoTab2.Text = "";
                                    //txtEjecutadoTab2.Text = "";
                                    //ddlTipoMonitoreo.SelectedValue = "";
                                    //txtNroTramie.Text = "";
                                    //txtTipoDocumento.Text = "";
                                    //ddlTipoDocumento.SelectedValue = "";
                                    //txtAsunto.Text = "";
                                    //Panel_ObservacionTab2.Visible = false;
                                    //imgbtnAgregarEvaluacionTab2.Visible = true;
                                    //lblNomUsuarioFlagAyudaTab4.Text = "";
                                    btnGuardarEvaluacionTab2.Visible = false;
                                    Panel_InformacionComplementaria.Visible = true;

                                    Up_Tab2.Update();
                                    Up_EstadoSituacional.Update();

                                    string script = "<script>alert('Se registró Correctamente.');</script>";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                }
                                else
                                {
                                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                }
                                /*otro guardado fin */


                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                return;

                            }
                            //}

                            /*fin guardar estado*/



                        }
                    }
                }


            }


        }

       

       

        protected void lnkbtnRegistrarPanelFotosGrilla_Click(object sender, EventArgs e)
        {
            if (lblID_evaluacionTab2.Text.Length == 0)
            {
                string script = "<script>alert('Debe registrar primero el avance para luego continuar con el registro del panel fotográfico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                //lblIdRecomendacionInformeMonitoreoPOPU.Text = lblID_evaluacionTab2.Text;
                //ImageButton boton = default(ImageButton);
                //GridViewRow row = default(GridViewRow);
                //boton = (ImageButton)sender;
                //row = (GridViewRow)boton.NamingContainer;
                lblIDEvaluacionRecomendacionFotoTab2.Text = lblID_evaluacionTab2.Text;


                cargaDetalleFotosVisita();
                PN_ListaAvanceFoto.Visible = true;
                PN_AvanceFotoFormulario.Visible = false;
                Up_AVANCEFOTO.Update();

                MPE_AVANCE_FOTO.Show();
            }
        }

        protected void cargaDetalleFotosVisita()
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIDEvaluacionRecomendacionFotoTab2.Text);

            grdAvanceFoto.DataSource = _objBLEjecucion.spMON_DetalleFotoVisita(_BEEjecucion);
            grdAvanceFoto.DataBind();

        }

        protected void imgbtnFotosTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton = default(ImageButton);
            GridViewRow row = default(GridViewRow);
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grdEvaluacionTab2.DataKeys[row.RowIndex]["id_evaluacionRecomendacion"].ToString();
            hfIdEvaluacionRecomentacion.Value = id;
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(id);

            DataTable dt = _objBLEjecucion.spMON_DetalleFotoVisitaMovil(_BEEjecucion);
            if (dt.Rows.Count > 0)
            {
                MPE_ElegirFoto.Show();
                grdFotoMovil.DataSource = dt;
                grdFotoMovil.DataBind();
                UpdatePanelFotoMovil.Update();
            }
            else
            {
                string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        

        }
        protected void grdAvanceFoto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //jc
            if (e.CommandName == "EliminarAvanceFoto")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFoto.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.comentario = "";
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.urlDoc = "";
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_DetalleFotoVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDetalleFotosVisita();
                    Up_Tab2.Update();
                    MPE_AVANCE_FOTO.Show();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdAvanceFoto_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblIdDetalleFoto.Text = grdAvanceFoto.SelectedDataKey.Value.ToString();
            GridViewRow row = grdAvanceFoto.SelectedRow;

            txtDescripcionFoto.Text = ((Label)row.FindControl("lblDescripcion")).Text;
            lnkCargarAvanceFoto.Text = ((ImageButton)row.FindControl("imgAvanceFoto")).ToolTip;
            GeneraIcoFile(lnkCargarAvanceFoto.Text, imgBtnCargarAvanceFoto);
            lblNomUsuarioAvanceFoto.Text = "Actualizó: " + ((Label)row.FindControl("lblUsuario")).Text + " - " + ((Label)row.FindControl("lblFechaUpdate")).Text;

            Up_AVANCEFOTO.Update();

            btnModificarAvanceFoto.Visible = true;
            PN_AvanceFotoFormulario.Visible = true;
            btnGuardarAvanceFoto.Visible = false;
            MPE_AVANCE_FOTO.Show();

        }
        protected void btnAgregarAvanceFoto_Click(object sender, ImageClickEventArgs e)
        {
            Up_AVANCEFOTO.Update();
            btnModificarAvanceFoto.Visible = false;
            PN_ListaAvanceFoto.Visible = true;
            PN_AvanceFotoFormulario.Visible = true;
            btnGuardarAvanceFoto.Visible = true;
            lnkCargarAvanceFoto.Text = "";
            //txtFechaAvanceFoto.Text = "";
            imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
            lblNomUsuarioAvanceFoto.Text = "";
        }
        protected void btnGuardarAvanceFoto_Click(object sender, EventArgs e)
        {
            if (txtDescripcionFoto.Text.Length == 0)
            {
                string script = "<script>alert('Ingresar una breve descripción.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                if (validaArchivoFotos(FileUploadCargarAvanceFoto))
                {
                    _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIDEvaluacionRecomendacionFotoTab2.Text);
                    _BEEjecucion.comentario = txtDescripcionFoto.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCargarAvanceFoto);

                    //AsyncFileUpload AsyncFileUploadPDF = (AsyncFileUpload)Session["FotoCargaAvance"];
                    //_BEEjecucion.urlDoc = _Metodo.uploadAsyncFileMonitoreo(AsyncFileUploadPDF);

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_MON_DetalleFotoVisita(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        PN_AvanceFotoFormulario.Visible = false;
                        cargaDetalleFotosVisita();

                        MPE_AVANCE_FOTO.Show();
                        txtDescripcionFoto.Text = "";
                        lnkCargarAvanceFoto.Text = "";
                        imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
                        PN_AvanceFotoFormulario.Visible = false;
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        protected void btnModificarAvanceFoto_Click(object sender, EventArgs e)
        {

            if (validaArchivoFotos(FileUploadCargarAvanceFoto))
            {

                _BEEjecucion.id_item = Convert.ToInt32(lblIdDetalleFoto.Text);

                if (FileUploadCargarAvanceFoto.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCargarAvanceFoto);
                }
                else
                {
                    _BEEjecucion.urlDoc = lnkCargarAvanceFoto.Text;
                }

                _BEEjecucion.comentario = txtDescripcionFoto.Text;
                _BEEjecucion.Tipo = 1;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLEjecucion.UD_MON_DetalleFotoVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se Actualizo correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaDetalleFotosVisita();
                    MPE_AVANCE_FOTO.Show();
                    txtDescripcionFoto.Text = "";
                    lnkCargarAvanceFoto.Text = "";
                    imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
                    PN_AvanceFotoFormulario.Visible = false;


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void btnCancelarAvanceFoto_Click(object sender, EventArgs e)
        {
            Up_AVANCEFOTO.Update();
            PN_AvanceFotoFormulario.Visible = false;

        }

        protected void lnkCargarAvanceFoto_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkCargarAvanceFoto.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgAvanceFoto_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdAvanceFoto.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdAvanceFoto.Rows[row.RowIndex].FindControl("imgAvanceFoto");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdAvanceFoto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgAvanceFoto");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void btnGenerarPanelFotografico_Click(object sender, EventArgs e)
        {
            string id = lblIDEvaluacionRecomendacionFotoTab2.Text;
            string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }


        protected void btnVerPanelFotografico_Click(object sender, EventArgs e)
        {
            MPE_ElegirFoto.Hide();

            string script = "<script>('#modalElegirFoto').modal('hide');window.open('PanelFotografico_Visita.aspx?idE=" + hfIdEvaluacionRecomentacion.Value + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void btnVerFotosMovil_Click(object sender, EventArgs e)
        {
            MPE_ElegirFoto.Hide();

            string script = "<script>$('#modalElegirFoto').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected Boolean ValidarEvaluacionRecomendacionTab2()
        {
            //Boolean result;
            //result = true;

            string result="";


            if (ddlTipoMonitoreo.SelectedValue == "")
            {
                result = result + "*Ingrese tipo de monitoreo.\\n";
            }

            if (txtFechaVisitaTab2.Text == "")
            {
                result = result + "*Ingresar Fecha de Visita.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaVisitaTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha de Visita.\\n";
            }
            else
            {
                if (Convert.ToDateTime(txtFechaVisitaTab2.Text) > DateTime.Today)
                {
                    result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

                }
            }

            if (ddlEstadoTab2.SelectedValue.Equals(""))
            {
                result = result + "*Seleccionar estado.\\n";
            }
            else if (ddlSubEstadoTab2.SelectedValue.Equals("") && ddlSubEstadoTab2.Visible == true)
            {
                result = result + "*Seleccionar Sub Estado.\\n";
            }

            if (ddlSubEstado2Tab2.Visible && ddlSubEstado2Tab2.SelectedValue.Equals(""))//SUB ESTADO 2
            {
                result = result + "*Seleccionar Sub Estado 2.\\n";
            }

            //Suspensión del plazo de ejecución
            if (ddlSubEstadoTab2.SelectedValue.Equals("87") && txtFechaInicioSuspensionTab2.Text.Length < 10)
            {
                result = result + "*Ingresar fecha de suspensión.\\n";
            }

            if (txtFechaInicioSuspensionTab2.Text.Length > 9)
            {
                if (_Metodo.ValidaFecha(txtFechaInicioSuspensionTab2.Text) == false)
                {
                    result = result + "*Formato no valido de fecha de suspensión.\\n";
                }
                else
                {

                    if (Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text) > DateTime.Today)
                    {
                        result = result + "*La fecha de suspensión ingresada es mayor a la fecha de hoy.\\n";
                    }
                }
            }

            if (txtFechaFinalReinicioTab2.Text.Length > 9)
            {
                if (_Metodo.ValidaFecha(txtFechaFinalReinicioTab2.Text) == false)
                {
                    result = result + "*Formato no valido de fecha de reinicio.\\n";
                }
                else
                {
                    if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) > DateTime.Today)
                    {
                        result = result + "*La fecha de reinicio ingresada es mayor a la fecha de hoy.\\n";
                    }

                    if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) < Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text))
                    {
                        result = result + "*La fecha de reinicio debe ser mayor a la fecha de suspensión.\\n";
                    }
                }
            }

            if (ddlSubEstado2Tab2.SelectedValue.Equals("82")) // PALIZADO PERMANENTE
            {
                int contadorProblemas = 0;
                foreach (ListItem item in chbProblematicaTab2.Items)
                {
                    if (item.Selected)
                    {
                        contadorProblemas = contadorProblemas + 1;
                    }
                }

                if (contadorProblemas > 0)
                {
                    result = result + "*Seleccionar problematica.\\n";
                }
            }

            //ACctos Previos - Proceso de Seleccion - Indirecta
            if (ddlSubEstadoTab2.SelectedValue.Equals("72") && ddlProcesoTab1.SelectedValue == "1" && ddlModalidadTab1.SelectedValue == "2")
            {
                if (grdSeguimientoProcesoTab1.Rows.Count == 0)
                {
                    result = result + "*Debe registrar primero el seguimiento al proceso de selección.\\n";
                }
            }
            //ACctos Previos - Proceso de Seleccion - Por Iniciar
            if (ddlSubEstadoTab2.SelectedValue.Equals("71") && ddlProcesoTab1.SelectedValue == "1" && ddlModalidadTab1.SelectedValue == "2")
            {
                if (grdSeguimientoProcesoTab1.Rows.Count == 0)
                {
                    result = result + "*Debe registrar primero el seguimiento al proceso de selección.\\n";
                }

                if (txtProTab1.Text.Equals("") || txtProConsentidaTab1.Text.Equals("") || txtFechaFirmaContratoTab1.Text.Equals(""))
                {
                    result = result + "*Debe registrar primero los campos del resultado al proceso de selección.\\n";
                }
            }

            // Actos Previos - En Ejecución - Paralizada
            if (ddlEstadoTab2.SelectedValue.Equals("41") || ddlEstadoTab2.SelectedValue.Equals("4") || ddlEstadoTab2.SelectedValue.Equals("5"))
            {
                if (txtFechaTerminoReal.Text.Length > 0)
                {
                    result = result + "*No puede registrar la fecha de termino real en el estado  : " + ddlEstadoTab2.SelectedItem.Text + ".\\n";
                }
            }

            // En Ejecución
            if (ddlEstadoTab2.SelectedValue.Equals("4"))
            {
                if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.\\n";
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length == 0) //FECHA DE ENTREGA DE TERRENO
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de entrega de terrno.\\n";
                }
            }

            // Concluido  - Paralizada
            if (ddlEstadoTab2.SelectedValue.Equals("6") || ddlEstadoTab2.SelectedValue.Equals("5"))
            {
                if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.\\n";
                   
                }
            }

            // Concluido  - Post ejecución
            if (ddlEstadoTab2.SelectedValue.Equals("6") || ddlEstadoTab2.SelectedValue.Equals("43"))
            {
                if (txtFechaTerminoReal.Text.Length == 0)
                {
                    result = result + "*Para registrar el estado "+ddlEstadoTab2.SelectedItem.Text+" debe registrar primero la fecha de termino real.\\n";
                }

                //En Liquidación, Liquidada, Recepcionada
                if ((ddlSubEstadoTab2.SelectedValue.Equals("75") || ddlSubEstadoTab2.SelectedValue.Equals("77") || ddlSubEstadoTab2.SelectedValue.Equals("78")) && txtFechaRecepcionFinalTab3.Text.Equals(""))
                {
                    result = result + "*Para registrar el estado Concluido y subestado " + ddlSubEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de acta recepción final.\\n";
                }
            }

            //Transferencia
            if (ddlSubEstadoTab2.SelectedValue == "44" && (ddlTipoReceptoraTab3.SelectedValue.Equals("") || txtUnidadReceptoraTab3.Text.Equals("") || ddlOperatividadTab3.SelectedValue == "" || txtFechaActaTransferenciaTab3.Text == "" || LnkbtnSostenibilidadTab3.Text.Equals("")))
            {
                result = result + "*Ingrese tipo, nombre, operatividad, fecha y documento de la entidad receptora(Datos de sostenibilidad) antes de cambiar el estado a Transferencia.\\n";

            }

            //Cierre
            if (ddlSubEstadoTab2.SelectedValue == "45" && (ddlFormatoCierreTabCierre.SelectedValue.Equals("") || txtFechaFormato14TabCierre.Text.Equals("") || lnkbtnFormatoCierreTabCierre.Text.Equals("")))
            {
                string script = "<script>alert('Ingrese tipo de fomato de cierre, fecha de cierre y documento de cierre antes de cambiar el estado a Cerrado');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }



            if (txtEjecutadoTab2.Text == "")
            {
                result = result + "*Ingresar avance físico ejecutado.\\n";
            }
            else if (Convert.ToDouble(txtEjecutadoTab2.Text) > 100)
            {
                result = result + "*El avance físico ejecutado no puede ser mayor a 100%.\\n";

            }
            if (txtProgramadoTab2.Text == "")
            {
                result = result + "*Ingresar avance físico programado.\\n";
            }
            else
            {
                if (Convert.ToDouble(txtProgramadoTab2.Text) > 100)
                {
                    result = result + "*El avance físico programado no puede ser mayor a 100%.\\n";
                }
            }

            if (txtFinanEjecutadoTab2.Text == "")
            {
                result = result + "*Ingresar avance financiero ejecutado.\\n";
            }
            else
            {
                if (Convert.ToDouble(txtFinanEjecutadoTab2.Text) > 100)
                {
                    result = result + "*El avance financiero ejecutado no puede ser mayor a 100%.\\n";
                }
            }


            if (txtFinanProgramadoTab2.Text == "")
            {
                result = result + "*Ingresar avance financiero programado.\\n";

            }
            else
            {
                if (Convert.ToDouble(txtFinanProgramadoTab2.Text) > 100)
                {
                    result = result + "*El avance financiero programado no puede ser mayor a 100%.\\n";
                }
            }

            //CONSISTENCIA DE AVANCES REGISTRADOS
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            List<BE_MON_Ejecucion> ListAvance = new List<BE_MON_Ejecucion>();
            ListAvance = _objBLEjecucion.F_spMON_EvaluacionRecomendacion(_BEEjecucion);

            if (btnGuardarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 0)
            {
                result = result + "*Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.\\n";
            }
            else if (btnModificarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 1)
            {
                result = result + "*Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.\\n";
            }

            if (txtFechaVisitaTab2.Text.Length > 0)
            {
                BE_MON_Ejecucion v1 = new BE_MON_Ejecucion { Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text), Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text) };
                BE_MON_Ejecucion itemMayor = null;
                BE_MON_Ejecucion itemMenor = null;
                //Item mayor a la fecha ingresada
                if (ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
                {
                    itemMayor = ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).First();

                    if (Convert.ToDouble(itemMayor.FisicoEjec) < Convert.ToDouble(txtEjecutadoTab2.Text))
                    {
                        result = result + "*El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.FisicoEjec + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }

                    if (Convert.ToDouble(itemMayor.financieroReal) < Convert.ToDouble(txtFinanEjecutadoTab2.Text))
                    {
                        result = result + "*El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.financieroReal + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }
                }
                //Item menor a la fecha ingresada
                if (ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
                {
                    itemMenor = ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Last();

                    if (Convert.ToDouble(itemMenor.FisicoEjec) > Convert.ToDouble(txtEjecutadoTab2.Text))
                    {
                        result = result + "*El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.FisicoEjec + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }

                    if (Convert.ToDouble(itemMenor.financieroReal) > Convert.ToDouble(txtFinanEjecutadoTab2.Text))
                    {
                        result = result + "*El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.financieroReal + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }
                }
            }

            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void btnModificarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_OficioTab4))
            {
                if (validaArchivo(FileUpload_InformeTab4))
                {
                    if (validaArchivo(FileUpload_ActaTab4))
                    {

                        if (ValidarEvaluacionRecomendacionTab2())
                        {
                            /*otro guardar inicio*/
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                            _BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;
                            _BEEjecucion.id_tipoEstadoParalizado = ddlSubEstado2Tab2.SelectedValue;
                            string vDetalleProblemas = "";
                            if (ddlSubEstadoTab2.SelectedValue.Equals("82"))//Paralizados permanentes
                            {
                                //List<ListItem> selected = new List<ListItem>();
                                foreach (ListItem item in chbProblematicaTab2.Items)
                                {
                                    //string myJson = "{'Username': 'myusername','Password':'pass'}";
                                    vDetalleProblemas = vDetalleProblemas + "{\"idSubEstado\":\"" + ddlSubEstadoTab2.SelectedValue + "\", \"idProblematica\":\"" + item.Value;
                                    if (item.Selected)
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"1\"},";
                                    }
                                    else
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"0\"},";
                                    }
                                }

                                vDetalleProblemas = vDetalleProblemas.Substring(0, vDetalleProblemas.Length - 1);

                                vDetalleProblemas = "[" + vDetalleProblemas + "]";
                            }
                            else
                            {
                                if (ddlSubEstadoTab2.SelectedValue.Equals("87"))//Suspension
                                {
                                    _BEEjecucion.Date_fechaInicio = _Metodo.VerificaFecha(txtFechaInicioSuspensionTab2.Text);
                                    _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinalReinicioTab2.Text);
                                }
                            }
                            _BEEjecucion.detalleProblematica = vDetalleProblemas;
                            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            _BEEjecucion.Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text);

                            _BEEjecucion.Evaluacion = txtEvaluacionTab2.Text;
                            _BEEjecucion.Recomendacion = txtRecomendacionTab2.Text;
                            _BEEjecucion.FisicoEjec = txtEjecutadoTab2.Text;
                            _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                            _BEEjecucion.FisicoProg = txtProgramadoTab2.Text;
                            _BEEjecucion.nrotramite = txtNroTramie.Text;
                            _BEEjecucion.id_tipodocumento = ddlTipoDocumento.SelectedValue;
                            _BEEjecucion.asunto = txtAsunto.Text;
                            _BEEjecucion.Tipo = 1;
                            _BEEjecucion.financieroProgramado = txtFinanProgramadoTab2.Text;
                            _BEEjecucion.financieroReal = txtFinanEjecutadoTab2.Text;

                            if (chbAyudaMemoriaTab4.Checked == true)
                            {
                                _BEEjecucion.flagAyuda = "1";
                            }
                            else
                            {
                                _BEEjecucion.flagAyuda = "0";
                            }

                            if (FileUpload_ActaTab4.HasFile)
                            {
                                _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlActa = LnkbtnActaTab4.Text;
                            }

                            if (FileUpload_InformeTab4.HasFile)
                            {
                                _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlInforme = LnkbtnInformeTab4.Text;
                            }


                            if (FileUpload_OficioTab4.HasFile)
                            {
                                _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlOficio = LnkbtnOficioTab4.Text;
                            }

                            int val;
                            try
                            {
                                val = _objBLEjecucion.spud_MON_EvaluacionRecomendacion(_BEEjecucion);
                            }
                            catch (Exception ex)
                            {
                                val = 0;
                            }



                            if (val == 1)
                            {



                                txtFechaVisitaTab2.Text = "";
                                txtEvaluacionTab2.Text = "";
                                txtRecomendacionTab2.Text = "";
                                txtProgramadoTab2.Text = "";
                                txtEjecutadoTab2.Text = "";
                                ddlTipoMonitoreo.SelectedValue = "";
                                txtNroTramie.Text = "";
                                txtTipoDocumento.Text = "";
                                ddlTipoDocumento.SelectedValue = "";
                                txtAsunto.Text = "";
                                chbAyudaMemoriaTab4.Checked = false;
                                Panel_ObservacionTab2.Visible = false;
                                imgbtnAgregarEvaluacionTab2.Visible = true;

                                cargarEvaluacionTab2();
                                CargaDatosTab2();
                                Up_Tab2.Update();

                                string script = "<script>alert('Se Actualizó Correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }




                        }
                    }
                }
            }

        }
        protected void btnCancelarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            Panel_ObservacionTab2.Visible = false;
            imgbtnAgregarEvaluacionTab2.Visible = true;
            Up_Tab2.Update();
        }



        protected void grdEvaluacionTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_evaluacionRecomendacion;
            id_evaluacionRecomendacion = grdEvaluacionTab2.SelectedDataKey.Value.ToString();
            lblID_evaluacionTab2.Text = id_evaluacionRecomendacion.ToString();

            GridViewRow row = grdEvaluacionTab2.SelectedRow;
            string xidTipoEstado = ((Label)row.FindControl("lblId_TipoEstadoEjecuccion")).Text;
            string xidTipoSubEstado = ((Label)row.FindControl("lblId_tipoSubEstadoEjecucion")).Text;

            if (xidTipoSubEstado.Equals("83")) //Paralizados Temporales
            {
                string script = "<script>alert('No se puede editar esta información.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                txtFechaVisitaTab2.Text = ((Label)row.FindControl("lblFechaVisita")).Text;
                txtEvaluacionTab2.Text = ((Label)row.FindControl("lblEvaluacion")).Text;

                txtRecomendacionTab2.Text = ((Label)row.FindControl("lblRecomendacion")).Text;

                txtEjecutadoTab2.Text = ((Label)row.FindControl("lblFisicoEjec")).Text;
                txtProgramadoTab2.Text = ((Label)row.FindControl("lblFisicoProg")).Text;

                txtFinanProgramadoTab2.Text = ((Label)row.FindControl("lblFinancieroProg")).Text;
                txtFinanEjecutadoTab2.Text = ((Label)row.FindControl("lblFinancieroEjec")).Text;

                ddlTipoMonitoreo.Text = ((Label)row.FindControl("lblid_tipoMonitor")).Text;
                txtNroTramie.Text = ((Label)row.FindControl("lblNroTramite")).Text;
                ddlTipoDocumento.Text = ((Label)row.FindControl("lblid_tipoDocumento")).Text;
                txtAsunto.Text = ((Label)row.FindControl("lblAsunto")).Text;

                chbAyudaMemoriaTab4.Checked = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;

                LnkbtnActaTab4.Text = ((ImageButton)row.FindControl("imgDocActaTab4")).ToolTip;
                LnkbtnInformeTab4.Text = ((ImageButton)row.FindControl("imgDocInformeTab4")).ToolTip;
                LnkbtnOficioTab4.Text = ((ImageButton)row.FindControl("imgDocOficioTab4")).ToolTip;

                lblNomUsuarioFlagAyudaTab4.Text = "(Actualizó: " + ((Label)row.FindControl("lblId_UsuarioFlag")).Text + " - " + ((Label)row.FindControl("lblFecha_UpdateFlag")).Text + ")";
                lblNomUsuarioObservaciones.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

                GeneraIcoFile(LnkbtnActaTab4.Text, imgbtnActaTab4);
                GeneraIcoFile(LnkbtnInformeTab4.Text, imgbtnInformeTab4);
                GeneraIcoFile(LnkbtnOficioTab4.Text, imgbtnOficioTab4);

                btnModificarEvaluacionTab2.Visible = true;
                btnGuardarEvaluacionTab2.Visible = false;
                Panel_ObservacionTab2.Visible = true;
                imgbtnAgregarEvaluacionTab2.Visible = false;
                chbAyudaMemoriaTab4.Enabled = false;

                //aca ini
                CargaTipoAdjudicacionTab2();
               
                ddlEstadoTab2.SelectedValue = xidTipoEstado;
                CargaTipoSubEstadosDetalle(ddlEstadoTab2.SelectedValue);
                ddlSubEstadoTab2.SelectedValue = xidTipoSubEstado;

                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
                Up_FechasSuspensionTab2.Update();
                if (ddlEstadoTab2.SelectedValue.Equals("5"))
                {
                    CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                    ddlSubEstado2Tab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                    ddlSubEstado2Tab2.Visible = true;
                    spSubestado2.Visible = true;
                    CargaTipoSubEstadoParalizado(ddlSubEstado2Tab2.SelectedValue); //TIPO PROBLEMATICA

                    string idDetalleProblematica = ((Label)row.FindControl("lblId_DetalleProblematica")).Text;
                    string[] itemProblematica = idDetalleProblematica.Split(',');
                    foreach (string item in itemProblematica)
                    {
                        foreach (ListItem itemChkb in chbProblematicaTab2.Items)
                        {
                            if (itemChkb.Value == item)
                            {
                                itemChkb.Selected = true;
                            }
                        }
                    }

                    lblNombreProblematicaTab2.Visible = true;
                    //ddlTipoProblematicaTab2.Visible = true;
                    chbProblematicaTab2.Visible = true;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();
                }
                else
                {
                    //ddlSubEstado2Tab2.SelectedValue = "";
                    ddlSubEstado2Tab2.Visible = false;
                    spSubestado2.Visible = false;

                    lblNombreProblematicaTab2.Visible = false;
                    chbProblematicaTab2.Items.Clear();
                    chbProblematicaTab2.Visible = false;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();

                    if (xidTipoSubEstado.Equals("87")) // Suspension de plazo
                    {
                        trSuspensionTab2.Visible = true;
                        trReinicioTab2.Visible = true;

                        txtFechaInicioSuspensionTab2.Text = ((Label)row.FindControl("lblFechaSuspensionGrilla")).Text;
                        txtFechaFinalReinicioTab2.Text = ((Label)row.FindControl("lblFechaReinicioGrilla")).Text;
                        Up_FechasSuspensionTab2.Update();

                        //Cargar SubEstado2
                        CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                        ddlSubEstado2Tab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                        ddlSubEstado2Tab2.Visible = true;
                        spSubestado2.Visible = true;

                    }

                    if (xidTipoSubEstado.Equals("80")) // Atrasado
                    {
                        //Cargar SubEstado2
                        CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                        ddlSubEstado2Tab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                        ddlSubEstado2Tab2.Visible = true;
                        spSubestado2.Visible = true;
                    }
                }
                //acater
                Panel_InformacionComplementaria.Visible = true;

                //VALIDACION PARA EVITAR CAMBIO DE ESTADOS
                string vfecha = ((Label)row.FindControl("lblFecha_Update")).Text;
                if (vfecha.Length > 10 && !xidTipoEstado.Equals(""))
                {
                    if (Convert.ToDateTime(vfecha.Substring(0, 10)).ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        ddlEstadoTab2.Enabled = true;
                        ddlSubEstadoTab2.Enabled = true;
                        ddlSubEstado2Tab2.Enabled = true;
                    }
                    else
                    {
                        if (!LblID_USUARIO.Text.Equals("481")) //USUARIO MONITOR PUEDE MODIFIAR LOS ESTADOS
                        {
                            ddlEstadoTab2.Enabled = false;
                            ddlSubEstadoTab2.Enabled = false;
                            ddlSubEstado2Tab2.Enabled = false;
                        }
                        if (ddlSubEstado2Tab2.Visible && ddlSubEstado2Tab2.SelectedValue.Equals(""))
                        {
                            ddlSubEstado2Tab2.Enabled = true;
                        }
                        if (lblCOD_SUBSECTOR.Text.Equals("3"))
                        {
                            ddlSubEstadoTab2.Enabled = true;
                            ddlSubEstado2Tab2.Enabled = true;
                        }
                    }
                }



                Up_Tab2.Update();
            }
        }

        protected void grdEvaluacionTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdEvaluacionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fechaVisita = Convert.ToDateTime("09/09/2012");

                int val = _objBLEjecucion.spud_MON_EvaluacionRecomendacion(_BEEjecucion);

                if (val == 1)
                {
                    CargaDatosTab2();
                    cargarEvaluacionTab2();
                    Up_Tab2.Update();

                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grdEvaluacionTab2_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imbActa = (ImageButton)e.Row.FindControl("imgDocActaTab4");

                GeneraIcoFile(imbActa.ToolTip, imbActa);

                ImageButton imbInforme = (ImageButton)e.Row.FindControl("imgDocInformeTab4");
                GeneraIcoFile(imbInforme.ToolTip, imbInforme);

                ImageButton imbOficio = (ImageButton)e.Row.FindControl("imgDocOficioTab4");
                GeneraIcoFile(imbOficio.ToolTip, imbOficio);

                CheckBox chbFlagAyuda = (CheckBox)e.Row.FindControl("chbFlagAyuda");
                if (lblID_ACCESO.Text.Equals("0"))
                {
                    chbFlagAyuda.Enabled = false;
                }

                if (chbFlagAyuda.Text == "1")
                {
                    chbFlagAyuda.Checked = true;
                    chbFlagAyuda.Text = "";
                }
                else
                {
                    chbFlagAyuda.Checked = false;
                    chbFlagAyuda.Text = "";
                }
            }
        }

        protected void chbFlagAyuda_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var valor = grdEvaluacionTab2.DataKeys[row.RowIndex].Value;
            //_BEEjecucion.id_tabla = Convert.ToInt32(valor.ToString());
            Boolean FlagAyuda = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(valor.ToString());
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


            if (FlagAyuda == true)
            {
                _BEEjecucion.flagAyuda = "1";
            }
            else
            {
                _BEEjecucion.flagAyuda = "0";
            }

            int val;
            try
            {
                val = _objBLEjecucion.spu_MON_EvaluacionRecomendacionByFlag(_BEEjecucion);
            }
            catch (Exception ex)
            {
                val = 0;
            }



            if (val == 1)
            {
                string script = "<script>alert('Se Actualizó Correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                cargarEvaluacionTab2();

                txtFechaVisitaTab2.Text = "";
                txtEvaluacionTab2.Text = "";
                txtRecomendacionTab2.Text = "";
                txtProgramadoTab2.Text = "";
                txtEjecutadoTab2.Text = "";
                ddlTipoMonitoreo.SelectedValue = "";
                txtNroTramie.Text = "";
                txtTipoDocumento.Text = "";
                ddlTipoDocumento.SelectedValue = "";
                txtAsunto.Text = "";
                chbAyudaMemoriaTab4.Checked = false;
                Panel_ObservacionTab2.Visible = false;
                imgbtnAgregarEvaluacionTab2.Visible = true;
                cargarEvaluacionTab2();

                Up_Tab2.Update();

            }

            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }



        }


        protected void imgDocActaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocActaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocInformeTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocInformeTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocOficioTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocOficioTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnActaTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab4.Text);
                Response.End();
            }
        }
        protected void LnkbtnInformeTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnInformeTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnInformeTab4.Text);
                Response.End();
            }
        }
        protected void LnkbtnOficioTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnOficioTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnOficioTab4.Text);
                Response.End();
            }
        }
        //protected void btnComponentesPNSU_OnClick(object sender, EventArgs e)
        //{
        //    VarPnsu();
        //    cargaComponentesPNSU();
        //    Up_Componente.Update();
        //    MPE_PNSU.Show();

        //}

        //private void VarPnsu()
        //{

        //    DataTable _obj;
        //    _BEEjecucion.snip = lblSNIP.Text;
        //    _BEEjecucion.tipoFinanciamiento = 3;

        //    _obj = _objBLEjecucion.spMON_ObtenerVariablesPNSU(_BEEjecucion);

        //    if (_obj.Rows.Count > 0)
        //    {
        //        txtA_captacion_nuc.Text = _obj.Rows[0]["a_captacion_nuc"].ToString();//
        //        txtA_captacion_cacl.Text = _obj.Rows[0]["a_captacion_cacl"].ToString();//

        //        txtA_trataPotable_num.Text = _obj.Rows[0]["a_trataPotable_num"].ToString();//
        //        txtA_trataPotable_cap.Text = _obj.Rows[0]["a_trataPotable_cap"].ToString();//

        //        txtA_conduccion_nuc.Text = _obj.Rows[0]["a_conduccion_nuc"].ToString();
        //        txtA_conduccion_cacd.Text = _obj.Rows[0]["a_conduccion_cacd"].ToString();

        //        txtA_impulsion_num.Text = _obj.Rows[0]["a_impulsion_num"].ToString();
        //        txtA_impulsion_cap.Text = _obj.Rows[0]["a_impulsion_cap"].ToString();

        //        txtA_estacion_num.Text = _obj.Rows[0]["a_estacion_num"].ToString();

        //        txtA_reservorio_nur.Text = _obj.Rows[0]["a_reservorio_nur"].ToString();
        //        txtA_reservorio_carm.Text = _obj.Rows[0]["a_reservorio_carm"].ToString();

        //        txtA_aduccion_nua.Text = _obj.Rows[0]["a_aduccion_nua"].ToString();
        //        txtA_aduccion_caad.Text = _obj.Rows[0]["a_aduccion_caad"].ToString();

        //        txtA_redes_nur.Text = _obj.Rows[0]["a_redes_nur"].ToString();
        //        txtA_redes_card.Text = _obj.Rows[0]["a_redes_card"].ToString();

        //        txtA_conexion_nucn.Text = _obj.Rows[0]["a_conexion_nucn"].ToString();
        //        txtA_conexion_nucr.Text = _obj.Rows[0]["a_conexion_nucr"].ToString();
        //        txtA_conexion_nucp.Text = _obj.Rows[0]["a_conexion_nucp"].ToString();

        //        txtS_colector_num.Text = _obj.Rows[0]["s_colectores_num"].ToString();
        //        txtS_colector_cap.Text = _obj.Rows[0]["s_colectores_cap"].ToString();


        //        txtS_camara_num.Text = _obj.Rows[0]["s_camara_num"].ToString();

        //        txtS_planta_nup.Text = _obj.Rows[0]["s_planta_nup"].ToString();
        //        txtS_planta_capl.Text = _obj.Rows[0]["s_planta_capl"].ToString();

        //        txtS_agua_nua.Text = _obj.Rows[0]["s_agua_nua"].ToString();
        //        txtS_agua_caad.Text = _obj.Rows[0]["s_agua_caad"].ToString();

        //        txtS_conexion_nucn.Text = _obj.Rows[0]["s_conexion_nucn"].ToString();
        //        txtS_conexion_nucr.Text = _obj.Rows[0]["s_conexion_nucr"].ToString();
        //        txtS_conexion_nuclo.Text = _obj.Rows[0]["s_conexion_nuclo"].ToString();

        //        txtS_efluente_num.Text = _obj.Rows[0]["s_efluente_num"].ToString();
        //        txtS_efluente_cap.Text = _obj.Rows[0]["s_efluente_cap"].ToString();

        //        txtS_impulsion_num.Text = _obj.Rows[0]["s_impulsion_num"].ToString();
        //        txtS_impulsion_cap.Text = _obj.Rows[0]["s_impulsion_cap"].ToString();

        //        txtD_tuberia_num.Text = _obj.Rows[0]["d_tuberia_num"].ToString();
        //        txtD_tuberia_cap.Text = _obj.Rows[0]["d_tuberia_cap"].ToString();

        //        txtD_cuneta_num.Text = _obj.Rows[0]["d_cuneta_num"].ToString();
        //        txtD_cuneta_cap.Text = _obj.Rows[0]["d_cuneta_cap"].ToString();

        //        txtD_tormenta_num.Text = _obj.Rows[0]["d_tormenta_num"].ToString();
        //        txtD_conexion_num.Text = _obj.Rows[0]["d_conexion_num"].ToString();
        //        txtD_inspeccion_num.Text = _obj.Rows[0]["d_inspeccion_num"].ToString();

        //        txtD_secundario_num.Text = _obj.Rows[0]["d_secundario_num"].ToString();
        //        txtD_secundario_cap.Text = _obj.Rows[0]["d_secundario_cap"].ToString();

        //        txtD_principal_num.Text = _obj.Rows[0]["d_principal_num"].ToString();
        //        txtD_principal_cap.Text = _obj.Rows[0]["d_principal_cap"].ToString();

        //    }

        //}

        //protected void LlenarPNSU()
        //{
        //    txtA_captacion.Text = "0";
        //    txtA_conduccion.Text = "0";
        //    txtA_estacion.Text = "0";
        //    txtA_impulsion.Text = "0";
        //    txtA_trata.Text = "0";
        //    txtA_reservorio.Text = "0";
        //    txtA_aduccion.Text = "0";
        //    txtA_redes.Text = "0";
        //    txtA_conexion_nueva.Text = "0";
        //    txtA_conexion_piletas.Text = "0";
        //    txtA_conexion_rehab.Text = "0";

        //    txtS_efluente.Text = "0";
        //    txtS_camara.Text = "0";
        //    txtS_emisor.Text = "0";
        //    txtS_Impulsion.Text = "0";
        //    txtS_planta.Text = "0";
        //    txtS_redes.Text = "0";
        //    txtS_conexion_nueva.Text = "0";
        //    txtS_conexion_rehab.Text = "0";
        //    txtS_conexion_unid.Text = "0";
        //    txtS_Impulsion.Text = "0";
        //    txtS_redes.Text = "0";

        //    txtD_tuberia.Text = "0";
        //    txtD_cuneta.Text = "0";
        //    txtD_tormenta.Text = "0";
        //    txtD_conexion.Text = "0";
        //    txtD_inspeccion.Text = "0";
        //    txtD_secundario.Text = "0";
        //    txtD_principal.Text = "0";

        //    txtA_captacion_p.Text = "0";
        //    txtA_conduccion_p.Text = "0";
        //    txtA_estacion_p.Text = "0";
        //    txtA_impulsion_p.Text = "0";
        //    txtA_trata_p.Text = "0";
        //    txtA_reservorio_p.Text = "0";
        //    txtA_aduccion_p.Text = "0";
        //    txtA_redes_p.Text = "0";
        //    txtA_conexion_nueva_p.Text = "0";
        //    txtA_conexion_piletas_p.Text = "0";
        //    txtA_conexion_rehab_p.Text = "0";

        //    txtS_efluente_p.Text = "0";
        //    txtS_camara_p.Text = "0";
        //    txtS_emisor_p.Text = "0";
        //    txtS_Impulsion_p.Text = "0";
        //    txtS_planta_p.Text = "0";
        //    txtS_redes_p.Text = "0";
        //    txtS_conexion_nueva_p.Text = "0";
        //    txtS_conexion_rehab_p.Text = "0";
        //    txtS_conexion_unid_p.Text = "0";
        //    txtS_Impulsion_p.Text = "0";
        //    txtS_redes_p.Text = "0";

        //    txtD_tuberia_p.Text = "0";
        //    txtD_cuneta_p.Text = "0";
        //    txtD_tormenta_p.Text = "0";
        //    txtD_conexion_p.Text = "0";
        //    txtD_inspeccion_p.Text = "0";
        //    txtD_secundario_p.Text = "0";
        //    txtD_principal_p.Text = "0";
        //}

        //protected void cargaComponentesPNSU()
        //{
        //    _BEVarPNSU.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEVarPNSU.TipoFinanciamiento = 3;

        //    dt = _objBLEjecucion.spMON_ComponentesPNSU(_BEVarPNSU);


        //    if (dt.Rows.Count > 0)
        //    {
        //        txtA_captacion.Text = dt.Rows[0]["a_captacion"].ToString();//
        //        txtA_conduccion.Text = dt.Rows[0]["a_conduccion"].ToString();//
        //        txtA_trata.Text = dt.Rows[0]["a_trata"].ToString();//
        //        txtA_reservorio.Text = dt.Rows[0]["a_reservorio"].ToString();//
        //        txtA_aduccion.Text = dt.Rows[0]["a_Aduccion"].ToString();
        //        txtA_redes.Text = dt.Rows[0]["a_redes"].ToString();
        //        txtA_conexion_nueva.Text = dt.Rows[0]["a_conexion_nueva"].ToString();
        //        txtA_conexion_piletas.Text = dt.Rows[0]["a_conexion_piletas"].ToString();
        //        txtA_conexion_rehab.Text = dt.Rows[0]["a_conexion_rehab"].ToString();
        //        txtA_impulsion.Text = dt.Rows[0]["a_impulsion"].ToString();
        //        txtA_estacion.Text = dt.Rows[0]["a_estacion"].ToString();

        //        txtS_efluente.Text = dt.Rows[0]["s_efluente"].ToString();
        //        txtS_camara.Text = dt.Rows[0]["s_camara"].ToString();
        //        txtS_emisor.Text = dt.Rows[0]["s_emisor"].ToString();
        //        txtS_planta.Text = dt.Rows[0]["s_planta"].ToString();
        //        txtS_conexion_nueva.Text = dt.Rows[0]["s_conexion_nueva"].ToString();
        //        txtS_conexion_rehab.Text = dt.Rows[0]["s_conexion_rehab"].ToString();
        //        txtS_conexion_unid.Text = dt.Rows[0]["s_conexion_unid"].ToString();
        //        txtS_Impulsion.Text = dt.Rows[0]["s_impulsion"].ToString();
        //        txtS_redes.Text = dt.Rows[0]["s_redes"].ToString();

        //        txtD_tuberia.Text = dt.Rows[0]["d_tuberia"].ToString();
        //        txtD_cuneta.Text = dt.Rows[0]["d_cuneta"].ToString();
        //        txtD_tormenta.Text = dt.Rows[0]["d_tormenta"].ToString();
        //        txtD_conexion.Text = dt.Rows[0]["d_conexion"].ToString();
        //        txtD_inspeccion.Text = dt.Rows[0]["d_inspeccion"].ToString();
        //        txtD_secundario.Text = dt.Rows[0]["d_secundarios"].ToString();
        //        txtD_principal.Text = dt.Rows[0]["d_principal"].ToString();

        //        txtA_captacion_p.Text = dt.Rows[0]["a_captacion_p"].ToString();//
        //        txtA_conduccion_p.Text = dt.Rows[0]["a_conduccion_p"].ToString();//
        //        txtA_trata_p.Text = dt.Rows[0]["a_planta_p"].ToString();//
        //        txtA_reservorio_p.Text = dt.Rows[0]["a_reservorio_p"].ToString();//
        //        txtA_aduccion_p.Text = dt.Rows[0]["a_aduccion_p"].ToString();
        //        txtA_redes_p.Text = dt.Rows[0]["a_redes_p"].ToString();
        //        txtA_conexion_nueva_p.Text = dt.Rows[0]["a_conexion_nueva_p"].ToString();
        //        txtA_conexion_piletas_p.Text = dt.Rows[0]["a_conexion_piletas_p"].ToString();
        //        txtA_conexion_rehab_p.Text = dt.Rows[0]["a_conexion_rehab_p"].ToString();
        //        txtA_impulsion_p.Text = dt.Rows[0]["a_impulsion_p"].ToString();
        //        txtA_estacion_p.Text = dt.Rows[0]["a_estacion_p"].ToString();

        //        txtS_efluente_p.Text = dt.Rows[0]["s_linea_p"].ToString();
        //        txtS_camara_p.Text = dt.Rows[0]["s_camara_p"].ToString();
        //        txtS_emisor_p.Text = dt.Rows[0]["s_emisor_p"].ToString();
        //        txtS_planta_p.Text = dt.Rows[0]["s_planta_p"].ToString();
        //        txtS_conexion_nueva_p.Text = dt.Rows[0]["s_conexion_nueva_p"].ToString();
        //        txtS_conexion_rehab_p.Text = dt.Rows[0]["s_conexion_rehab_p"].ToString();
        //        txtS_conexion_unid_p.Text = dt.Rows[0]["s_conexion_unid_p"].ToString();
        //        txtS_Impulsion_p.Text = dt.Rows[0]["s_impulsion_p"].ToString();
        //        txtS_redes_p.Text = dt.Rows[0]["s_redes_p"].ToString();

        //        txtD_tuberia_p.Text = dt.Rows[0]["d_tuberia_p"].ToString();
        //        txtD_cuneta_p.Text = dt.Rows[0]["d_cuneta_p"].ToString();
        //        txtD_tormenta_p.Text = dt.Rows[0]["d_boca_p"].ToString();
        //        txtD_conexion_p.Text = dt.Rows[0]["d_conexion_p"].ToString();
        //        txtD_inspeccion_p.Text = dt.Rows[0]["d_inpeccion_p"].ToString();
        //        txtD_secundario_p.Text = dt.Rows[0]["d_secundarios_p"].ToString();
        //        txtD_principal_p.Text = dt.Rows[0]["d_principal_p"].ToString();

        //        lblUsuarioComponentesPNSU.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
        //        UpdatePanel2.Update();
        //    }
        //    else
        //    {
        //        lblUsuarioComponentesPNSU.Text = ".";
        //        UpdatePanel2.Update();
        //        LlenarPNSU();
        //    }


        //}


        //protected void btnGuardarComponentesPNSU_OnClick(object sender, EventArgs e)
        //{
        //    _BEVarPNSU.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEVarPNSU.TipoFinanciamiento = 3;
        //    _BEVarPNSU.A_captacion = Convert.ToInt32(txtA_captacion.Text);
        //    _BEVarPNSU.A_conduccion = Convert.ToInt32(txtA_conduccion.Text);
        //    _BEVarPNSU.A_trata = Convert.ToInt32(txtA_trata.Text);
        //    _BEVarPNSU.A_reservorio = Convert.ToInt32(txtA_reservorio.Text);
        //    _BEVarPNSU.A_Aduccion = Convert.ToInt32(txtA_aduccion.Text);
        //    _BEVarPNSU.A_redes = Convert.ToInt32(txtA_redes.Text);
        //    _BEVarPNSU.A_conexion_nueva = Convert.ToInt32(txtA_conexion_nueva.Text);
        //    _BEVarPNSU.A_conexion_piletas = Convert.ToInt32(txtA_conexion_piletas.Text);
        //    _BEVarPNSU.A_conexion_rehab = Convert.ToInt32(txtA_conexion_rehab.Text);
        //    _BEVarPNSU.A_impulsion = Convert.ToInt32(txtA_impulsion.Text);
        //    _BEVarPNSU.A_estacion = Convert.ToInt32(txtA_estacion.Text);

        //    _BEVarPNSU.S_efluente = Convert.ToInt32(txtS_efluente.Text);
        //    _BEVarPNSU.S_camara = Convert.ToInt32(txtS_camara.Text);
        //    _BEVarPNSU.S_emisor = Convert.ToInt32(txtS_emisor.Text);
        //    _BEVarPNSU.S_planta = Convert.ToInt32(txtS_planta.Text);
        //    _BEVarPNSU.S_conexion_nueva = Convert.ToInt32(txtS_conexion_nueva.Text);
        //    _BEVarPNSU.S_conexion_rehab = Convert.ToInt32(txtS_conexion_rehab.Text);
        //    _BEVarPNSU.S_conexion_unid = Convert.ToInt32(txtS_conexion_unid.Text);
        //    _BEVarPNSU.S_redes = Convert.ToInt32(txtS_redes.Text);
        //    _BEVarPNSU.S_impulsion = Convert.ToInt32(txtS_Impulsion.Text);

        //    _BEVarPNSU.D_tuberia = Convert.ToInt32(txtD_tuberia.Text);
        //    _BEVarPNSU.D_cuneta = Convert.ToInt32(txtD_cuneta.Text);
        //    _BEVarPNSU.D_tormenta = Convert.ToInt32(txtD_tormenta.Text);
        //    _BEVarPNSU.D_conexion = Convert.ToInt32(txtD_conexion.Text);
        //    _BEVarPNSU.D_inspeccion = Convert.ToInt32(txtD_inspeccion.Text);
        //    _BEVarPNSU.D_secundario = Convert.ToInt32(txtD_secundario.Text);
        //    _BEVarPNSU.D_principal = Convert.ToInt32(txtD_principal.Text);

        //    _BEVarPNSU.A_captacion_p = Convert.ToInt32(txtA_captacion_p.Text);
        //    _BEVarPNSU.A_conduccion_p = Convert.ToInt32(txtA_conduccion_p.Text);
        //    _BEVarPNSU.A_trata_p = Convert.ToInt32(txtA_trata_p.Text);
        //    _BEVarPNSU.A_reservorio_p = Convert.ToInt32(txtA_reservorio_p.Text);
        //    _BEVarPNSU.A_Aduccion_p = Convert.ToInt32(txtA_aduccion_p.Text);
        //    _BEVarPNSU.A_redes_p = Convert.ToInt32(txtA_redes_p.Text);
        //    _BEVarPNSU.A_conexion_nueva_p = Convert.ToInt32(txtA_conexion_nueva_p.Text);
        //    _BEVarPNSU.A_conexion_piletas_p = Convert.ToInt32(txtA_conexion_piletas_p.Text);
        //    _BEVarPNSU.A_conexion_rehab_p = Convert.ToInt32(txtA_conexion_rehab_p.Text);
        //    _BEVarPNSU.A_impulsion_p = Convert.ToInt32(txtA_impulsion_p.Text);
        //    _BEVarPNSU.A_estacion_p = Convert.ToInt32(txtA_estacion_p.Text);

        //    _BEVarPNSU.S_efluente_p = Convert.ToInt32(txtS_efluente_p.Text);
        //    _BEVarPNSU.S_camara_p = Convert.ToInt32(txtS_camara_p.Text);
        //    _BEVarPNSU.S_emisor_p = Convert.ToInt32(txtS_emisor_p.Text);
        //    _BEVarPNSU.S_planta_p = Convert.ToInt32(txtS_planta_p.Text);
        //    _BEVarPNSU.S_conexion_nueva_p = Convert.ToInt32(txtS_conexion_nueva_p.Text);
        //    _BEVarPNSU.S_conexion_rehab_p = Convert.ToInt32(txtS_conexion_rehab_p.Text);
        //    _BEVarPNSU.S_conexion_unid_p = Convert.ToInt32(txtS_conexion_unid_p.Text);
        //    _BEVarPNSU.S_redes_p = Convert.ToInt32(txtS_redes_p.Text);
        //    _BEVarPNSU.S_impulsion_p = Convert.ToInt32(txtS_Impulsion_p.Text);

        //    _BEVarPNSU.D_tuberia_p = Convert.ToInt32(txtD_tuberia_p.Text);
        //    _BEVarPNSU.D_cuneta_p = Convert.ToInt32(txtD_cuneta_p.Text);
        //    _BEVarPNSU.D_tormenta_p = Convert.ToInt32(txtD_tormenta_p.Text);
        //    _BEVarPNSU.D_conexion_p = Convert.ToInt32(txtD_conexion_p.Text);
        //    _BEVarPNSU.D_inspeccion_p = Convert.ToInt32(txtD_inspeccion_p.Text);
        //    _BEVarPNSU.D_secundario_p = Convert.ToInt32(txtD_secundario_p.Text);
        //    _BEVarPNSU.D_principal_p = Convert.ToInt32(txtD_principal_p.Text);

        //    _BEVarPNSU.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //    int val = _objBLEjecucion.spi_MON_ComponentesPNSU(_BEVarPNSU);

        //    if (val == 1)
        //    {
        //        string script = "<script>alert('Se registró Correctamente.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        VarPnsu();
        //        cargaComponentesPNSU();
        //        Up_Componente.Update();

        //    }
        //    else
        //    {
        //        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    }

        //}

        //protected void btnComponentesPMIB_Click(object sender, EventArgs e)
        //{
        //    VarPmib();
        //    Up_VarPMIB.Update();
        //    MPE_PMIB.Show();
        //}

        //private void VarPmib()
        //{
        //    DataTable _obj;
        //    BEVarPimb _objVarPimb = new BEVarPimb();
        //    _objVarPimb.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
        //    _obj = objBLProyecto.spSOL_Seguimiento_Variable_PMIB(_objVarPimb);

        //    if (_obj.Rows.Count > 0)
        //    {
        //        txtP_concreto_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_concreto_m2"].ToString()).ToString("N");
        //        txtP_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["P_concreto_co"].ToString()).ToString("N");
        //        txtP_concreto_cu.Text = (Convert.ToDouble(txtP_concreto_co.Text) / Convert.ToDouble(txtP_concreto_m2.Text)).ToString("N");

        //        txtP_asfalto_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_asfalto_m2"].ToString()).ToString("N");
        //        txtP_asfalto_co.Text = Convert.ToDouble(_obj.Rows[0]["P_asfalto_co"].ToString()).ToString("N");
        //        txtP_asfalto_cu.Text = (Convert.ToDouble(txtP_asfalto_co.Text) / Convert.ToDouble(txtP_asfalto_m2.Text)).ToString("N");

        //        txtP_emboquillado_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_emboquillado_m2"].ToString()).ToString("N");//
        //        txtP_emboquillado_co.Text = Convert.ToDouble(_obj.Rows[0]["P_emboquillado_co"].ToString()).ToString("N");//
        //        txtP_emboquillado_cu.Text = (Convert.ToDouble(txtP_emboquillado_co.Text) / Convert.ToDouble(txtP_emboquillado_m2.Text)).ToString("N");

        //        txtP_adoquinado_m2.Text = Convert.ToDouble(_obj.Rows[0]["P_adoquinado_m2"].ToString()).ToString("N");//
        //        txtP_adoquinado_co.Text = Convert.ToDouble(_obj.Rows[0]["P_adoquinado_co"].ToString()).ToString("N");//
        //        txtP_adoquinado_cu.Text = (Convert.ToDouble(txtP_adoquinado_co.Text) / Convert.ToDouble(txtP_adoquinado_m2.Text)).ToString("N");

        //        divAutomatica(txtP_concreto_m2, txtP_concreto_cu, txtP_concreto_co);
        //        divAutomatica(txtP_asfalto_m2, txtP_asfalto_cu, txtP_asfalto_co);
        //        divAutomatica(txtP_emboquillado_m2, txtP_emboquillado_cu, txtP_emboquillado_co);
        //        divAutomatica(txtP_adoquinado_m2, txtP_adoquinado_cu, txtP_adoquinado_co);

        //        lblCostoP.Text = (Convert.ToDouble(txtP_concreto_co.Text) + Convert.ToDouble(txtP_asfalto_co.Text) + Convert.ToDouble(txtP_emboquillado_co.Text) + Convert.ToDouble(txtP_adoquinado_co.Text)).ToString("N");


        //        txtV_concreto_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_concreto_m2"].ToString()).ToString("N");//
        //        txtV_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["V_concreto_co"].ToString()).ToString("N");//
        //        txtV_concreto_cu.Text = (Convert.ToDouble(txtV_concreto_co.Text) / Convert.ToDouble(txtV_concreto_m2.Text)).ToString("N");

        //        txtV_adoquinado_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_adoquinado_m2"].ToString()).ToString("N");//
        //        txtV_adoquinado_co.Text = Convert.ToDouble(_obj.Rows[0]["V_adoquinado_co"].ToString()).ToString("N");//
        //        txtV_adoquinado_cu.Text = (Convert.ToDouble(txtV_adoquinado_co.Text) / Convert.ToDouble(txtV_adoquinado_m2.Text)).ToString("N");

        //        txtV_piedra_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_piedra_m2"].ToString()).ToString("N");
        //        txtV_piedra_co.Text = Convert.ToDouble(_obj.Rows[0]["V_piedra_co"].ToString()).ToString("N");
        //        txtV_piedra_cu.Text = (Convert.ToDouble(txtV_piedra_co.Text) / Convert.ToDouble(txtV_piedra_m2.Text)).ToString("N");

        //        txtV_emboquillado_m2.Text = Convert.ToDouble(_obj.Rows[0]["V_emboquillado_m2"].ToString()).ToString("N");//
        //        txtV_emboquillado_co.Text = Convert.ToDouble(_obj.Rows[0]["V_emboquillado_co"].ToString()).ToString("N");//
        //        txtV_emboquillado_cu.Text = (Convert.ToDouble(txtV_emboquillado_co.Text) / Convert.ToDouble(txtV_emboquillado_m2.Text)).ToString("N");

        //        divAutomatica(txtV_concreto_m2, txtV_concreto_cu, txtV_concreto_co);
        //        divAutomatica(txtV_adoquinado_m2, txtV_adoquinado_cu, txtV_adoquinado_co);
        //        divAutomatica(txtV_piedra_m2, txtV_piedra_cu, txtV_piedra_co);
        //        divAutomatica(txtV_emboquillado_m2, txtV_emboquillado_cu, txtV_emboquillado_co);

        //        lblCostoV.Text = (Convert.ToDouble(txtV_concreto_co.Text) + Convert.ToDouble(txtV_adoquinado_co.Text) + Convert.ToDouble(txtV_piedra_co.Text) + Convert.ToDouble(txtV_emboquillado_co.Text)).ToString("N");


        //        txtD_concreto_ml.Text = Convert.ToDouble(_obj.Rows[0]["D_concreto_ml"].ToString()).ToString("N");
        //        txtD_concreto_co.Text = Convert.ToDouble(_obj.Rows[0]["D_concreto_co"].ToString()).ToString("N");
        //        txtD_concreto_cu.Text = (Convert.ToDouble(txtD_concreto_co.Text) / Convert.ToDouble(txtD_concreto_ml.Text)).ToString("N");

        //        txtD_piedra_ml.Text = _obj.Rows[0]["D_piedra_ml"].ToString();
        //        txtD_piedra_co.Text = _obj.Rows[0]["D_piedra_co"].ToString();
        //        txtD_piedra_cu.Text = (Convert.ToDouble(txtD_piedra_co.Text) / Convert.ToDouble(txtD_piedra_ml.Text)).ToString("N");

        //        divAutomatica(txtD_concreto_ml, txtD_concreto_cu, txtD_concreto_co);
        //        divAutomatica(txtD_piedra_ml, txtD_piedra_cu, txtD_piedra_co);

        //        lblCostoD.Text = (Convert.ToDouble(txtD_concreto_co.Text) + Convert.ToDouble(txtD_piedra_co.Text)).ToString("N");

        //        txtL_administracion_m2.Text = Convert.ToDouble(_obj.Rows[0]["l_administracion_m2"].ToString()).ToString("N");
        //        txtL_administracion_co.Text = Convert.ToDouble(_obj.Rows[0]["l_administracion_co"].ToString()).ToString("N");
        //        txtL_losa_m2.Text = Convert.ToDouble(_obj.Rows[0]["l_losa_m2"].ToString()).ToString("N");
        //        txtL_losa_co.Text = Convert.ToDouble(_obj.Rows[0]["l_losa_co"].ToString()).ToString("N");
        //        txtL_tribunas_m2.Text = Convert.ToDouble(_obj.Rows[0]["l_tribuna_m2"].ToString()).ToString("N");
        //        txtL_tribunas_co.Text = Convert.ToDouble(_obj.Rows[0]["l_tribuna_co"].ToString()).ToString("N");
        //        txtL_cerco_ml.Text = Convert.ToDouble(_obj.Rows[0]["l_cerco_ml"].ToString()).ToString("N");
        //        txtL_cerco_co.Text = Convert.ToDouble(_obj.Rows[0]["l_cerco_co"].ToString()).ToString("N");
        //        txtL_iluminacion_u.Text = Convert.ToDouble(_obj.Rows[0]["l_iluminacion_u"].ToString()).ToString("N");
        //        txtL_iluminacion_co.Text = Convert.ToDouble(_obj.Rows[0]["l_iluminacion_co"].ToString()).ToString("N");

        //        divAutomatica(txtL_administracion_m2, txtL_administracion_cu, txtL_administracion_co);
        //        divAutomatica(txtL_losa_m2, txtL_losa_cu, txtL_losa_co);
        //        divAutomatica(txtL_tribunas_m2, txtL_tribunas_cu, txtL_tribunas_co);
        //        divAutomatica(txtL_cerco_ml, txtL_cerco_cu, txtL_cerco_co);
        //        divAutomatica2(txtL_iluminacion_u, txtL_iluminacion_cu, txtL_iluminacion_co);

        //        lblCostoL.Text = (Convert.ToDouble(txtL_administracion_co.Text) + Convert.ToDouble(txtL_cerco_co.Text) + Convert.ToDouble(txtL_iluminacion_co.Text) + Convert.ToDouble(txtL_losa_co.Text) + Convert.ToDouble(txtL_tribunas_co.Text)).ToString("N");

        //        txtC_puestas_m2.Text = Convert.ToDouble(_obj.Rows[0]["c_puestas_m2"].ToString()).ToString("N");
        //        txtC_puestas_co.Text = Convert.ToDouble(_obj.Rows[0]["c_puestas_co"].ToString()).ToString("N");
        //        txtC_administracion_m2.Text = Convert.ToDouble(_obj.Rows[0]["c_administracion_m2"].ToString()).ToString("N");
        //        txtC_administracion_co.Text = Convert.ToDouble(_obj.Rows[0]["c_administracion_co"].ToString()).ToString("N");
        //        txtC_cerco_ml.Text = Convert.ToDouble(_obj.Rows[0]["c_cerco_ml"].ToString()).ToString("N");
        //        txtC_cerco_co.Text = Convert.ToDouble(_obj.Rows[0]["c_cerco_co"].ToString()).ToString("N");

        //        divAutomatica(txtC_administracion_m2, txtC_administracion_cu, txtC_administracion_co);
        //        divAutomatica(txtC_cerco_ml, txtC_cerco_cu, txtC_cerco_co);
        //        divAutomatica(txtC_puestas_m2, txtC_puestas_cu, txtC_puestas_co);

        //        lblCostoC.Text = (Convert.ToDouble(txtC_administracion_co.Text) + Convert.ToDouble(txtC_cerco_co.Text) + Convert.ToDouble(txtC_puestas_co.Text)).ToString("N");

        //        txtO_muro_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_muro_m2"].ToString()).ToString("N");
        //        txtO_muro_co.Text = Convert.ToDouble(_obj.Rows[0]["O_muro_co"].ToString()).ToString("N");
        //        txtO_muro_cu.Text = (Convert.ToDouble(txtO_muro_co.Text) / Convert.ToDouble(txtO_muro_m2.Text)).ToString("N");

        //        txtO_ponton_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_ponton_m2"].ToString()).ToString("N");
        //        txtO_ponton_co.Text = Convert.ToDouble(_obj.Rows[0]["O_ponton_co"].ToString()).ToString("N");
        //        txtO_ponton_cu.Text = (Convert.ToDouble(txtO_ponton_co.Text) / Convert.ToDouble(txtO_ponton_m2.Text)).ToString("N");

        //        txtO_bermas_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_bermas_m2"].ToString()).ToString("N");
        //        txtO_bermas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_bermas_co"].ToString()).ToString("N");
        //        txtO_bermas_cu.Text = (Convert.ToDouble(txtO_bermas_co.Text) / Convert.ToDouble(txtO_bermas_m2.Text)).ToString("N");

        //        txtO_verdes_m2.Text = Convert.ToDouble(_obj.Rows[0]["O_verdes_m2"].ToString()).ToString("N");
        //        txtO_verdes_co.Text = Convert.ToDouble(_obj.Rows[0]["O_verdes_co"].ToString()).ToString("N");
        //        txtO_verdes_cu.Text = (Convert.ToDouble(txtO_verdes_co.Text) / Convert.ToDouble(txtO_verdes_m2.Text)).ToString("N");

        //        txtO_jardineras_u.Text = _obj.Rows[0]["O_jardineras_u"].ToString();
        //        txtO_jardineras_co.Text = Convert.ToDouble(_obj.Rows[0]["O_jardineras_co"].ToString()).ToString("N");
        //        txtO_jardineras_cu.Text = (Convert.ToDouble(txtO_jardineras_co.Text) / Convert.ToDouble(txtO_jardineras_u.Text)).ToString("N");

        //        txtO_plantones_u.Text = _obj.Rows[0]["O_plantones_u"].ToString();
        //        txtO_plantones_co.Text = Convert.ToDouble(_obj.Rows[0]["O_plantones_co"].ToString()).ToString("N");
        //        txtO_plantones_cu.Text = (Convert.ToDouble(txtO_plantones_co.Text) / Convert.ToDouble(txtO_plantones_u.Text)).ToString("N");

        //        txtO_sanitarias_u.Text = _obj.Rows[0]["O_sanitarias_u"].ToString();
        //        txtO_sanitarias_co.Text = Convert.ToDouble(_obj.Rows[0]["O_sanitarias_co"].ToString()).ToString("N");
        //        txtO_sanitarias_cu.Text = (Convert.ToDouble(txtO_sanitarias_co.Text) / Convert.ToDouble(txtO_sanitarias_u.Text)).ToString("N");

        //        txtO_electricas_u.Text = _obj.Rows[0]["O_electricas_u"].ToString();
        //        txtO_electricas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_electricas_co"].ToString()).ToString("N");
        //        txtO_electricas_cu.Text = (Convert.ToDouble(txtO_electricas_co.Text) / Convert.ToDouble(txtO_electricas_u.Text)).ToString("N");

        //        txtO_bancas_u.Text = _obj.Rows[0]["O_bancas_u"].ToString();
        //        txtO_bancas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_bancas_co"].ToString()).ToString("N");
        //        txtO_bancas_cu.Text = (Convert.ToDouble(txtO_bancas_co.Text) / Convert.ToDouble(txtO_bancas_u.Text)).ToString("N");

        //        txtO_basureros_u.Text = _obj.Rows[0]["O_basureros_u"].ToString();
        //        txtO_basureros_co.Text = Convert.ToDouble(_obj.Rows[0]["O_basureros_co"].ToString()).ToString("N");
        //        txtO_basureros_cu.Text = (Convert.ToDouble(txtO_basureros_co.Text) / Convert.ToDouble(txtO_basureros_u.Text)).ToString("N");

        //        txtO_pergolas_u.Text = _obj.Rows[0]["O_pergolas_u"].ToString();
        //        txtO_pergolas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_pergolas_co"].ToString()).ToString("N");
        //        txtO_pergolas_cu.Text = (Convert.ToDouble(txtO_pergolas_co.Text) / Convert.ToDouble(txtO_pergolas_u.Text)).ToString("N");

        //        txtO_glorietas_u.Text = _obj.Rows[0]["O_glorietas_u"].ToString();
        //        txtO_glorietas_co.Text = Convert.ToDouble(_obj.Rows[0]["O_glorietas_co"].ToString()).ToString("N");
        //        txtO_glorietas_cu.Text = (Convert.ToDouble(txtO_glorietas_co.Text) / Convert.ToDouble(txtO_glorietas_u.Text)).ToString("N");

        //        txtO_otros_u.Text = _obj.Rows[0]["O_otros_u"].ToString();
        //        txtO_otros_co.Text = Convert.ToDouble(_obj.Rows[0]["O_otros_co"].ToString()).ToString("N");
        //        txtO_otros_cu.Text = (Convert.ToDouble(txtO_otros_co.Text) / Convert.ToDouble(txtO_otros_u.Text)).ToString("N");

        //        txtO_puente_m2.Text = _obj.Rows[0]["o_puente_m2"].ToString();
        //        txtO_puente_co.Text = Convert.ToDouble(_obj.Rows[0]["o_puente_co"].ToString()).ToString("N");

        //        txtObservacionesPMIB.Text = _obj.Rows[0]["observaciones"].ToString();

        //        lblUsuarioMetasPMIB.Text = "Actualizó: " + _obj.Rows[0]["usuario"].ToString() + " - " + _obj.Rows[0]["fecha_update"].ToString();

        //        divAutomatica(txtO_muro_m2, txtO_muro_cu, txtO_muro_co);
        //        divAutomatica(txtO_ponton_m2, txtO_ponton_cu, txtO_ponton_co);
        //        divAutomatica(txtO_bermas_m2, txtO_bermas_cu, txtO_bermas_co);
        //        divAutomatica(txtO_verdes_m2, txtO_verdes_cu, txtO_verdes_co);

        //        divAutomatica(txtO_puente_m2, txtO_puente_cu, txtO_puente_co);

        //        divAutomatica2(txtO_jardineras_u, txtO_jardineras_cu, txtO_jardineras_co);
        //        divAutomatica2(txtO_plantones_u, txtO_plantones_cu, txtO_plantones_co);
        //        divAutomatica2(txtO_sanitarias_u, txtO_sanitarias_cu, txtO_sanitarias_co);
        //        divAutomatica2(txtO_electricas_u, txtO_electricas_cu, txtO_electricas_co);

        //        divAutomatica2(txtO_bancas_u, txtO_bancas_cu, txtO_bancas_co);
        //        divAutomatica2(txtO_basureros_u, txtO_basureros_cu, txtO_basureros_co);
        //        divAutomatica2(txtO_pergolas_u, txtO_pergolas_cu, txtO_pergolas_co);
        //        divAutomatica2(txtO_glorietas_u, txtO_glorietas_cu, txtO_glorietas_co);
        //        divAutomatica2(txtO_otros_u, txtO_otros_cu, txtO_otros_co);

        //        // lblCostoO.Text = (Convert.ToDouble(txtO_muro_co.Text) + Convert.ToDouble(txtO_ponton_co.Text) + Convert.ToDouble(txtO_bermas_co.Text) + Convert.ToDouble(txtO_verdes_co.Text) + Convert.ToDouble(txtO_jardineras_co.Text) + Convert.ToDouble(txtO_plantones_co.Text) + Convert.ToDouble(txtO_sanitarias_co.Text) + Convert.ToDouble(txtO_electricas_co.Text) + Convert.ToDouble(txtO_bancas_co.Text) + Convert.ToDouble(txtO_basureros_co.Text) + Convert.ToDouble(txtO_pergolas_co.Text) + Convert.ToDouble(txtO_glorietas_co.Text) + Convert.ToDouble(txtO_otros_co.Text) + Convert.ToDouble(txtO_puente_co.Text)).ToString("N");

        //        //  lblCostoTotalObra.Text = (Convert.ToDouble(lblCostoP.Text) + Convert.ToDouble(lblCostoV.Text) + Convert.ToDouble(lblCostoD.Text) + Convert.ToDouble(lblCostoO.Text) + Convert.ToDouble(lblCostoL.Text) + Convert.ToDouble(lblCostoC.Text)).ToString("N");

        //        CostosTotales();

        //    }
        //    else
        //    {
        //        LlenarPMIB();
        //    }

        //}

        //protected void LlenarPMIB()
        //{
        //    txtP_concreto_m2.Text = "0";
        //    txtP_concreto_co.Text = "0";
        //    txtP_asfalto_m2.Text = "0";
        //    txtP_asfalto_co.Text = "0";
        //    txtP_emboquillado_m2.Text = "0";
        //    txtP_emboquillado_co.Text = "0";
        //    txtP_adoquinado_m2.Text = "0";
        //    txtP_adoquinado_co.Text = "0";

        //    txtV_concreto_m2.Text = "0";
        //    txtV_concreto_co.Text = "0";
        //    txtV_adoquinado_m2.Text = "0";
        //    txtV_adoquinado_co.Text = "0";
        //    txtV_piedra_m2.Text = "0";
        //    txtV_piedra_co.Text = "0";
        //    txtV_emboquillado_m2.Text = "0";
        //    txtV_emboquillado_co.Text = "0";

        //    txtD_concreto_ml.Text = "0";
        //    txtD_concreto_co.Text = "0";
        //    txtD_piedra_ml.Text = "0";
        //    txtD_piedra_co.Text = "0";

        //    txtO_muro_m2.Text = "0";
        //    txtO_muro_co.Text = "0";
        //    txtO_ponton_m2.Text = "0";
        //    txtO_ponton_co.Text = "0";
        //    txtO_bermas_m2.Text = "0";
        //    txtO_bermas_co.Text = "0";
        //    txtO_verdes_m2.Text = "0";
        //    txtO_verdes_co.Text = "0";

        //    txtO_jardineras_u.Text = "0";
        //    txtO_jardineras_co.Text = "0";
        //    txtO_plantones_u.Text = "0";
        //    txtO_plantones_co.Text = "0";
        //    txtO_sanitarias_u.Text = "0";
        //    txtO_sanitarias_co.Text = "0";
        //    txtO_electricas_u.Text = "0";
        //    txtO_electricas_co.Text = "0";

        //    txtO_bancas_u.Text = "0";
        //    txtO_bancas_co.Text = "0";
        //    txtO_basureros_u.Text = "0";
        //    txtO_basureros_co.Text = "0";
        //    txtO_pergolas_u.Text = "0";
        //    txtO_pergolas_co.Text = "0";
        //    txtO_glorietas_u.Text = "0";
        //    txtO_glorietas_co.Text = "0";
        //    txtO_otros_u.Text = "0";
        //    txtO_otros_co.Text = "0";

        //    txtO_puente_co.Text = "0";
        //    txtO_puente_m2.Text = "0";

        //    txtL_administracion_co.Text = "0";
        //    txtL_administracion_m2.Text = "0";
        //    txtL_cerco_co.Text = "0";
        //    txtL_cerco_ml.Text = "0";
        //    txtL_iluminacion_co.Text = "0";
        //    txtL_iluminacion_u.Text = "0";
        //    txtL_losa_co.Text = "0";
        //    txtL_losa_m2.Text = "0";
        //    txtL_tribunas_co.Text = "0";
        //    txtL_tribunas_m2.Text = "0";

        //    txtC_administracion_co.Text = "0";
        //    txtC_administracion_m2.Text = "0";
        //    txtC_cerco_co.Text = "0";
        //    txtC_cerco_ml.Text = "0";
        //    txtC_puestas_co.Text = "0";
        //    txtC_puestas_m2.Text = "0";

        //}
        //protected void divAutomatica(TextBox uni, TextBox cu, TextBox co)
        //{
        //    if (uni.Text == "")
        //    { uni.Text = "0.00"; }
        //    else
        //    { uni.Text = Convert.ToDouble(uni.Text).ToString("N"); }

        //    if (co.Text == "")
        //    { co.Text = "0.00"; }
        //    else
        //    { co.Text = Convert.ToDouble(co.Text).ToString("N"); }

        //    if (uni.Text == "0.00" || uni.Text == "0")
        //    { cu.Text = "0.00"; }
        //    else { cu.Text = (Convert.ToDouble(co.Text) / Convert.ToDouble(uni.Text)).ToString("N"); }

        //}

        //protected void divAutomatica2(TextBox uni, TextBox cu, TextBox co)
        //{
        //    if (uni.Text == "")
        //    { uni.Text = "0"; }
        //    else
        //    { uni.Text = Convert.ToDouble(uni.Text).ToString(); }
        //    if (co.Text == "")
        //    { co.Text = "0.00"; }
        //    else
        //    { co.Text = Convert.ToDouble(co.Text).ToString("N"); }

        //    if (uni.Text == "0.00" || uni.Text == "0")
        //    { cu.Text = "0.00"; }
        //    else { cu.Text = (Convert.ToDouble(co.Text) / Convert.ToDouble(uni.Text)).ToString("N"); }

        //}

        //protected void Pavimento_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtP_concreto_m2, txtP_concreto_cu, txtP_concreto_co);
        //    divAutomatica(txtP_asfalto_m2, txtP_asfalto_cu, txtP_asfalto_co);
        //    divAutomatica(txtP_emboquillado_m2, txtP_emboquillado_cu, txtP_emboquillado_co);
        //    divAutomatica(txtP_adoquinado_m2, txtP_adoquinado_cu, txtP_adoquinado_co);
        //    CostosTotales();
        //}

        //protected void Veredas_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtV_concreto_m2, txtV_concreto_cu, txtV_concreto_co);
        //    divAutomatica(txtV_adoquinado_m2, txtV_adoquinado_cu, txtV_adoquinado_co);
        //    divAutomatica(txtV_piedra_m2, txtV_piedra_cu, txtV_piedra_co);
        //    divAutomatica(txtV_emboquillado_m2, txtV_emboquillado_cu, txtV_emboquillado_co);
        //    CostosTotales();
        //}

        //protected void Drenaje_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtD_concreto_ml, txtD_concreto_cu, txtD_concreto_co);
        //    divAutomatica(txtD_piedra_ml, txtD_piedra_cu, txtD_piedra_co);
        //    CostosTotales();
        //}

        //protected void Otros_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtO_muro_m2, txtO_muro_cu, txtO_muro_co);
        //    divAutomatica(txtO_ponton_m2, txtO_ponton_cu, txtO_ponton_co);
        //    divAutomatica(txtO_bermas_m2, txtO_bermas_cu, txtO_bermas_co);
        //    divAutomatica(txtO_verdes_m2, txtO_verdes_cu, txtO_verdes_co);

        //    divAutomatica(txtO_puente_m2, txtO_puente_cu, txtO_puente_co);

        //    divAutomatica2(txtO_jardineras_u, txtO_jardineras_cu, txtO_jardineras_co);
        //    divAutomatica2(txtO_plantones_u, txtO_plantones_cu, txtO_plantones_co);
        //    divAutomatica2(txtO_sanitarias_u, txtO_sanitarias_cu, txtO_sanitarias_co);
        //    divAutomatica2(txtO_electricas_u, txtO_electricas_cu, txtO_electricas_co);

        //    divAutomatica2(txtO_bancas_u, txtO_bancas_cu, txtO_bancas_co);
        //    divAutomatica2(txtO_basureros_u, txtO_basureros_cu, txtO_basureros_co);
        //    divAutomatica2(txtO_pergolas_u, txtO_pergolas_cu, txtO_pergolas_co);
        //    divAutomatica2(txtO_glorietas_u, txtO_glorietas_cu, txtO_glorietas_co);
        //    divAutomatica2(txtO_otros_u, txtO_otros_cu, txtO_otros_co);
        //    CostosTotales();
        //}

        //protected void Losa_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtL_administracion_m2, txtL_administracion_cu, txtL_administracion_co);
        //    divAutomatica(txtL_losa_m2, txtL_losa_cu, txtL_losa_co);
        //    divAutomatica(txtL_tribunas_m2, txtL_tribunas_cu, txtL_tribunas_co);
        //    divAutomatica(txtL_cerco_ml, txtL_cerco_cu, txtL_cerco_co);

        //    divAutomatica2(txtL_iluminacion_u, txtL_iluminacion_cu, txtL_iluminacion_co);

        //    CostosTotales();

        //}

        //protected void Cerco_OnTextChanged(object sender, EventArgs e)
        //{
        //    divAutomatica(txtC_administracion_m2, txtC_administracion_cu, txtC_administracion_co);
        //    divAutomatica(txtC_cerco_ml, txtC_cerco_cu, txtC_cerco_co);
        //    divAutomatica(txtC_puestas_m2, txtC_puestas_cu, txtC_puestas_co);

        //    CostosTotales();

        //}
        //protected void CostosTotales()
        //{
        //    lblCostoP.Text = (Convert.ToDouble(txtP_concreto_co.Text) + Convert.ToDouble(txtP_asfalto_co.Text) + Convert.ToDouble(txtP_emboquillado_co.Text) + Convert.ToDouble(txtP_adoquinado_co.Text)).ToString("N");

        //    lblCostoV.Text = (Convert.ToDouble(txtV_concreto_co.Text) + Convert.ToDouble(txtV_adoquinado_co.Text) + Convert.ToDouble(txtV_piedra_co.Text) + Convert.ToDouble(txtV_emboquillado_co.Text)).ToString("N");
        //    lblCostoD.Text = (Convert.ToDouble(txtD_concreto_co.Text) + Convert.ToDouble(txtD_piedra_co.Text)).ToString("N");

        //    lblCostoL.Text = (Convert.ToDouble(txtL_administracion_co.Text) + Convert.ToDouble(txtL_cerco_co.Text) + Convert.ToDouble(txtL_iluminacion_co.Text) + Convert.ToDouble(txtL_losa_co.Text) + Convert.ToDouble(txtL_tribunas_co.Text)).ToString("N");
        //    lblCostoC.Text = (Convert.ToDouble(txtC_administracion_co.Text) + Convert.ToDouble(txtC_cerco_co.Text) + Convert.ToDouble(txtC_puestas_co.Text)).ToString("N");

        //    lblCostoO.Text = (Convert.ToDouble(txtO_muro_co.Text) + Convert.ToDouble(txtO_ponton_co.Text) + Convert.ToDouble(txtO_bermas_co.Text) + Convert.ToDouble(txtO_verdes_co.Text) + Convert.ToDouble(txtO_jardineras_co.Text) + Convert.ToDouble(txtO_plantones_co.Text) + Convert.ToDouble(txtO_sanitarias_co.Text) + Convert.ToDouble(txtO_electricas_co.Text) + Convert.ToDouble(txtO_bancas_co.Text) + Convert.ToDouble(txtO_basureros_co.Text) + Convert.ToDouble(txtO_pergolas_co.Text) + Convert.ToDouble(txtO_glorietas_co.Text) + Convert.ToDouble(txtO_otros_co.Text) + Convert.ToDouble(txtO_puente_co.Text)).ToString("N");

        //    lblCostoTotalObra.Text = (Convert.ToDouble(lblCostoP.Text) + Convert.ToDouble(lblCostoV.Text) + Convert.ToDouble(lblCostoD.Text) + Convert.ToDouble(lblCostoO.Text) + Convert.ToDouble(lblCostoL.Text) + Convert.ToDouble(lblCostoC.Text)).ToString("N");

        //}

        //protected void btnGuardarMetasPMIB_Click(object sender, EventArgs e)
        //{
        //    BEVarPimb _BEVarPimb = new BEVarPimb();

        //    _BEVarPimb.Id_Solicitudes = Convert.ToInt64(LblID_SOLICITUD.Text);

        //    _BEVarPimb.P_concreto_m2 = Convert.ToDouble(txtP_concreto_m2.Text);
        //    _BEVarPimb.P_concreto_co = Convert.ToDouble(txtP_concreto_co.Text);
        //    _BEVarPimb.P_asfalto_m2 = Convert.ToDouble(txtP_asfalto_m2.Text);
        //    _BEVarPimb.P_asfalto_co = Convert.ToDouble(txtP_asfalto_co.Text);
        //    _BEVarPimb.P_emboquillado_m2 = Convert.ToDouble(txtP_emboquillado_m2.Text);
        //    _BEVarPimb.P_emboquillado_co = Convert.ToDouble(txtP_emboquillado_co.Text);
        //    _BEVarPimb.P_adoquinado_m2 = Convert.ToDouble(txtP_adoquinado_m2.Text);
        //    _BEVarPimb.P_adoquinado_co = Convert.ToDouble(txtP_adoquinado_co.Text);

        //    _BEVarPimb.V_concreto_m2 = Convert.ToDouble(txtV_concreto_m2.Text);
        //    _BEVarPimb.V_concreto_co = Convert.ToDouble(txtV_concreto_co.Text);
        //    _BEVarPimb.V_adoquinado_m2 = Convert.ToDouble(txtV_adoquinado_m2.Text);
        //    _BEVarPimb.V_adoquinado_co = Convert.ToDouble(txtV_adoquinado_co.Text);
        //    _BEVarPimb.V_piedra_m2 = Convert.ToDouble(txtV_piedra_m2.Text);
        //    _BEVarPimb.V_piedra_co = Convert.ToDouble(txtV_piedra_co.Text);
        //    _BEVarPimb.V_emboquillado_m2 = Convert.ToDouble(txtV_emboquillado_m2.Text);
        //    _BEVarPimb.V_emboquillado_co = Convert.ToDouble(txtV_emboquillado_co.Text);

        //    _BEVarPimb.D_concreto_ml = Convert.ToDouble(txtD_concreto_ml.Text);
        //    _BEVarPimb.D_concreto_co = Convert.ToDouble(txtD_concreto_co.Text);
        //    _BEVarPimb.D_piedra_ml = Convert.ToDouble(txtD_piedra_ml.Text);
        //    _BEVarPimb.D_piedra_co = Convert.ToDouble(txtD_piedra_co.Text);

        //    _BEVarPimb.O_muro_m2 = Convert.ToDouble(txtO_muro_m2.Text);
        //    _BEVarPimb.O_muro_co = Convert.ToDouble(txtO_muro_co.Text);
        //    _BEVarPimb.O_ponton_m2 = Convert.ToDouble(txtO_ponton_m2.Text);
        //    _BEVarPimb.O_ponton_co = Convert.ToDouble(txtO_ponton_co.Text);
        //    _BEVarPimb.O_bermas_m2 = Convert.ToDouble(txtO_bermas_m2.Text);
        //    _BEVarPimb.O_bermas_co = Convert.ToDouble(txtO_bermas_co.Text);
        //    _BEVarPimb.O_verdes_m2 = Convert.ToDouble(txtO_verdes_m2.Text);
        //    _BEVarPimb.O_verdes_co = Convert.ToDouble(txtO_verdes_co.Text);

        //    _BEVarPimb.O_jardineras_u = Convert.ToInt32(txtO_jardineras_u.Text);
        //    _BEVarPimb.O_jardineras_co = Convert.ToDouble(txtO_jardineras_co.Text);
        //    _BEVarPimb.O_plantones_u = Convert.ToInt32(txtO_plantones_u.Text);
        //    _BEVarPimb.O_plantones_co = Convert.ToDouble(txtO_plantones_co.Text);
        //    _BEVarPimb.O_sanitarias_u = Convert.ToInt32(txtO_sanitarias_u.Text);
        //    _BEVarPimb.O_sanitarias_co = Convert.ToDouble(txtO_sanitarias_co.Text);
        //    _BEVarPimb.O_electricas_u = Convert.ToInt32(txtO_electricas_u.Text);
        //    _BEVarPimb.O_electricas_co = Convert.ToDouble(txtO_electricas_co.Text);

        //    _BEVarPimb.O_bancas_u = Convert.ToInt32(txtO_bancas_u.Text);
        //    _BEVarPimb.O_bancas_co = Convert.ToDouble(txtO_bancas_co.Text);
        //    _BEVarPimb.O_basureros_u = Convert.ToInt32(txtO_basureros_u.Text);
        //    _BEVarPimb.O_basureros_co = Convert.ToDouble(txtO_basureros_co.Text);
        //    _BEVarPimb.O_pergolas_u = Convert.ToInt32(txtO_pergolas_u.Text);
        //    _BEVarPimb.O_pergolas_co = Convert.ToDouble(txtO_pergolas_co.Text);
        //    _BEVarPimb.O_glorietas_u = Convert.ToInt32(txtO_glorietas_u.Text);
        //    _BEVarPimb.O_glorietas_co = Convert.ToDouble(txtO_glorietas_co.Text);
        //    _BEVarPimb.O_otros_u = Convert.ToInt32(txtO_otros_u.Text);
        //    _BEVarPimb.O_otros_co = Convert.ToDouble(txtO_otros_co.Text);

        //    _BEVarPimb.o_puente_co = Convert.ToDouble(txtO_puente_co.Text);
        //    _BEVarPimb.o_puente_m2 = Convert.ToDouble(txtO_puente_m2.Text);

        //    _BEVarPimb.l_administracion_co = Convert.ToDouble(txtL_administracion_co.Text);
        //    _BEVarPimb.l_administracion_m2 = Convert.ToDouble(txtL_administracion_m2.Text);
        //    _BEVarPimb.l_cerco_co = Convert.ToDouble(txtL_cerco_co.Text);
        //    _BEVarPimb.l_cerco_ml = Convert.ToDouble(txtL_cerco_ml.Text);
        //    _BEVarPimb.l_iluminacion_co = Convert.ToDouble(txtL_iluminacion_co.Text);
        //    _BEVarPimb.l_iluminacion_u = Convert.ToInt32(txtL_iluminacion_u.Text);
        //    _BEVarPimb.l_losa_co = Convert.ToDouble(txtL_losa_co.Text);
        //    _BEVarPimb.l_losa_m2 = Convert.ToDouble(txtL_losa_m2.Text);
        //    _BEVarPimb.l_tribuna_co = Convert.ToDouble(txtL_tribunas_co.Text);
        //    _BEVarPimb.l_tribuna_m2 = Convert.ToDouble(txtL_tribunas_m2.Text);

        //    _BEVarPimb.c_administracion_co = Convert.ToDouble(txtC_administracion_co.Text);
        //    _BEVarPimb.c_administracion_m2 = Convert.ToDouble(txtC_administracion_m2.Text);
        //    _BEVarPimb.c_cerco_co = Convert.ToDouble(txtC_cerco_co.Text);
        //    _BEVarPimb.c_cerco_ml = Convert.ToDouble(txtC_cerco_ml.Text);
        //    _BEVarPimb.c_puestas_co = Convert.ToDouble(txtC_puestas_co.Text);
        //    _BEVarPimb.c_puestas_m2 = Convert.ToDouble(txtC_puestas_m2.Text);



        //    _BEVarPimb.Observaciones = txtObservacionesPMIB.Text;

        //    _BEVarPimb.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //    objBLProyecto.spiuSOL_IngresarVariablePimb(_BEVarPimb);

        //    //string script = "<script>alert('Se actualizó las metas.'); window.opener.location.reload(); window.close();</script>";
        //    string script = "<script>alert('Se actualizó las metas.');</script>";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        //    VarPmib();
        //    Up_VarPMIB.Update();

        //}

        protected void ddlTipoEjecucionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoEjecucionTab2.SelectedValue == "1")
            {
                tblEjecucionSupervision.Visible = false;
                tblEjecucionObra.Visible = true;
            }
            else
            {
                tblEjecucionSupervision.Visible = true;
                tblEjecucionObra.Visible = false;
                CargaDatosSupervisionTab2();

                cargaCartasFianzaSupervisionTab2();

                cargaValorizacionSupervisionTab2();

                CargaInformes("1", grdInformeDiagnosticoTab2);
                CargaInformes("2", grdInformeMensualTab2);
                CargaInformes("3", grdInformeFinalTab2);

                cargaFechaInformeSupervisicionTab2();
            }

            Up_Tab2.Update();
        }
        protected void rbAdelanto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbFlagAdelantoTab2.SelectedValue == "1")
            {
                Panel_AdelantoSuper.Visible = true;
            }
            else
            {
                Panel_AdelantoSuper.Visible = false;
            }

            Up_Tab2.Update();
        }
        protected void imgbtnAgregarCartaSupervisionTab2_Click(object sender, ImageClickEventArgs e)
        {
            Panel_CartaSupervisionTab2.Visible = true;

            btnGuardarFianzaSupervisionTab2.Visible = true;
            lblNomActualizaFianzaSupervisionTab2.Text = "";
            btnModificarFianzaSupervisionTab2.Visible = false;
            imgbtnCartaFianzaSupervisionTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnCartaFianzaSupervisionTab2.Text = "";

            lblTitDocAccionSupervisionTab2.Text = "";
            txtFechaRenovacionSupervisionTab2.Visible = false;
            imgbtnAccionSupervisionTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnAccionSupervisionTab2.Text = "";

        }
        protected void btnVerProfesionalSTab2_Click(object sender, EventArgs e)
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            grdProfesionalSupervisionTab2.DataSource = _objBLEjecucion.F_spMON_ProfesionalTecnicoSupervision(_BEEjecucion);
            grdProfesionalSupervisionTab2.DataBind();

            imgbtnAgregarProfesionalSupervisionTab2.Visible = true;
            Panel_AgregarProfesionalSupervisorTab2.Visible = false;

            MPE_ProfesionalSupervisionTab2.Show();
            Up_ProfesionalSupervisionTab2.Update();
        }
        protected void imgbtnAgregarProfesionalSupervisionTab2_Click(object sender, ImageClickEventArgs e)
        {
            Panel_AgregarProfesionalSupervisorTab2.Visible = true;
            imgbtnAgregarProfesionalSupervisionTab2.Visible = false;

            btnGuardarProfesionalSupervisorTab2.Visible = true;
            btnModificarProfesionalSupervisorTab2.Visible = false;
        }
        protected void btnCancelarProfesionalSupervisorTab2_Click(object sender, EventArgs e)
        {
            Panel_AgregarProfesionalSupervisorTab2.Visible = false;
            imgbtnAgregarProfesionalSupervisionTab2.Visible = true;
        }
        protected void btnModificarProfesionalSupervisorTab2_Click(object sender, EventArgs e)
        {
            if (ValidarProfesionalSupervisionTab2())
            {
                _BEEjecucion.id_item = Convert.ToInt32(lblIDProfesionSupervisionTab2.Text);
                _BEEjecucion.nombre = txtNombreProfesionSupervisionTab2.Text;
                _BEEjecucion.cargo = txtCargoProfesionalSupervisionTab2.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaAsignacionProfesionalTab2.Text);
                _BEEjecucion.telefono = txtTelefonoProfesionalTab2.Text;
                _BEEjecucion.correo = txtCorreoProfesionalTab2.Text;
                _BEEjecucion.porcentaje = txtParticipacionProfesionalTab2.Text;
                _BEEjecucion.Tipo = 1;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_ProfesionalTecnicoSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    grdProfesionalSupervisionTab2.DataSource = _objBLEjecucion.F_spMON_ProfesionalTecnicoSupervision(_BEEjecucion);
                    grdProfesionalSupervisionTab2.DataBind();

                    Panel_AgregarProfesionalSupervisorTab2.Visible = false;
                    Up_ProfesionalSupervisionTab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected Boolean ValidarProfesionalSupervisionTab2()
        {



            Boolean result;
            result = true;

            if (txtNombreProfesionSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de profesional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtCargoProfesionalSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar cargo de profesional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaAsignacionProfesionalTab2.Text == "")
            {
                string script = "<script>alert('Ingresar Fecha de asignación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaAsignacionProfesionalTab2.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de asignación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            //else
            //{

            //    if (Convert.ToDateTime(txtFechaAsignacionProfesionalTab2.Text) > DateTime.Today)
            //    {
            //        string script = "<script>alert('La fecha de asignación ingresada es mayor a la fecha de hoy.');</script>";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //        result = false;
            //        return result;
            //    }

            //}

            if (txtParticipacionProfesionalTab2.Text == "")
            {
                string script = "<script>alert('Ingresar % de participación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }
        protected void btnGuardarProfesionalSupervisorTab2_Click(object sender, EventArgs e)
        {
            if (ValidarProfesionalSupervisionTab2())
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.nombre = txtNombreProfesionSupervisionTab2.Text;
                _BEEjecucion.cargo = txtCargoProfesionalSupervisionTab2.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaAsignacionProfesionalTab2.Text);
                _BEEjecucion.telefono = txtTelefonoProfesionalTab2.Text;
                _BEEjecucion.correo = txtCorreoProfesionalTab2.Text;
                _BEEjecucion.porcentaje = txtParticipacionProfesionalTab2.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.I_spi_MON_ProfesionalTecnicoSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    grdProfesionalSupervisionTab2.DataSource = _objBLEjecucion.F_spMON_ProfesionalTecnicoSupervision(_BEEjecucion);
                    grdProfesionalSupervisionTab2.DataBind();

                    Panel_AgregarProfesionalSupervisorTab2.Visible = false;
                    Up_ProfesionalSupervisionTab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void imgbtnAgregaJefeSupervisionTab2_Click(object sender, ImageClickEventArgs e)
        {
            Panel_AgregarJefeSupervisionTab2.Visible = true;

            dt = CargaEmpleadoObraSupervisionTab2(3);
            grdHistorialJefeSupervisionTab2.DataSource = dt;
            grdHistorialJefeSupervisionTab2.DataBind();

            lnkbtnDocSupervisionTab2.Text = "";
            imgbtnDocSupervisionTab2.ImageUrl = "~/img/blanco.png";
            btnModificarJefeSupervisionTab2.Visible = false;
            btnGrabarJefeSupervisionTab2.Visible = true;
        }
        protected void btnCancelarJefeSupervisionTab2_Click(object sender, EventArgs e)
        {
            Panel_AgregarJefeSupervisionTab2.Visible = false;

            Up_Tab2.Update();
        }
        protected void btnModificarJefeSupervisionTab2_Click(object sender, EventArgs e)
        {
            if (txtNombreJefeSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de jefe supervision.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIDSupervisionTab2.Text);
                _BEEjecucion.Tipo = 1;
                // _BEEjecucion.empleadoObra = txtu.Text;
                _BEEjecucion.empleadoObra = txtNombreJefeSupervisionTab2.Text;
                _BEEjecucion.CIP = txtCIPSupervisionTab2.Text;
                _BEEjecucion.telefono = txtTelefonoSupervisionTab2.Text;
                _BEEjecucion.correo = txtCorreoSupervisionTab2.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCAPSupervisionTab2.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaDesignacionSupervicionTab2.Text);


                if (FileUploadDocSupervisionTab2.HasFile)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadDocSupervisionTab2);
                }
                else
                { _BEEjecucion.urlDoc = lnkbtnDocSupervisionTab2.Text; }

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el supervisor correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtJefeSupervisionTab2.Text = txtNombreJefeSupervisionTab2.Text;
                    txtNombreJefeSupervisionTab2.Text = "";
                    txtTelefonoSupervisionTab2.Text = "";
                    txtCIPSupervisionTab2.Text = "";
                    txtCorreoSupervisionTab2.Text = "";
                    txtCAPSupervisionTab2.Text = "";
                    txtFechaDesignacionSupervicionTab2.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraSupervisionTab2(3);
                    if (dt.Rows.Count > 0)
                    {

                        txtJefeSupervisionTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtJefeSupervisionTab2.Text = "";

                    }

                    Panel_AgregarJefeSupervisionTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void btnGrabarJefeSupervisionTab2_Click(object sender, EventArgs e)
        {
            if (txtNombreJefeSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de jefe de supervisión.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 3;

                _BEEjecucion.empleadoObra = txtNombreJefeSupervisionTab2.Text;
                _BEEjecucion.CIP = txtCIPSupervisionTab2.Text;
                _BEEjecucion.telefono = txtTelefonoSupervisionTab2.Text;
                _BEEjecucion.correo = txtCorreoSupervisionTab2.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCAPSupervisionTab2.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaDesignacionSupervicionTab2.Text);
                _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadDocSupervisionTab2);

                int val = _objBLEjecucion.spi_MON_EmpleadoObraSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró supervisor correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtJefeSupervisionTab2.Text = txtNombreJefeSupervisionTab2.Text;
                    txtNombreJefeSupervisionTab2.Text = "";
                    txtTelefonoSupervisionTab2.Text = "";
                    txtCIPSupervisionTab2.Text = "";
                    txtCorreoSupervisionTab2.Text = "";
                    txtCAPSupervisionTab2.Text = "";
                    txtFechaDesignacionSupervicionTab2.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);

                    //SUPERVISOR
                    dt = CargaEmpleadoObraSupervisionTab2(3);
                    grdHistorialJefeSupervisionTab2.DataSource = dt;
                    grdHistorialJefeSupervisionTab2.DataBind();

                    Panel_AgregarJefeSupervisionTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void imgbtnAgregarValorizacionSupervisionTab2_Click(object sender, ImageClickEventArgs e)
        {
            Panel_AgregarValorizacionSupervisionTab2.Visible = true;
            imgbtnAgregarValorizacionSupervisionTab2.Visible = false;

            btnModificarValorizacionSupervisorTab2.Visible = false;
            btnGuardarValorizacionSupervisorTab2.Visible = true;

            ddlMesSupervisionTab2.Enabled = true;
            ddlMesSupervisionTab2.Visible = true;
            txtMesSupervisionTab2.Text = "";
            txtMesSupervisionTab2.Visible = false;

            Up_Tab2.Update();
        }

        protected Boolean ValidarValorizacionSupervisionTab2()
        {
            Boolean result;
            result = true;

            if (ddlTipoValorizacionSupervisionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo Valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (btnGuardarValorizacionSupervisorTab2.Visible == true)
            {
                if (ddlMesSupervisionTab2.SelectedValue == "")
                {
                    string script = "<script>alert('Seleccione mes.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }
            else
            { }



            //if (Convert.ToInt32(ddlValorizacionAdicionalTab2.SelectedValue) > 1)
            //{
            //    if (txtNroAdicionalAdicionalTab2.Text == "" || txtNroAdicionalAdicionalTab2.Text == "0")
            //    {
            //        string script = "<script>alert('Ingrese N° Adicional de Valorización.');</script>";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //        result = false;
            //        return result;
            //    }
            //}

            if (txtFechaPresentacionSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de presentación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (_Metodo.ValidaFecha(txtFechaPresentacionSupervisionTab2.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de presentación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (txtMontoValorizacionSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ValidaDecimal(txtMontoValorizacionSupervisionTab2.Text) == false)
            {
                string script = "<script>alert('Ingrese monto(S/.) valido, por ejemplo: 10000.02 ');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtFechaLimiteSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de limite de pago.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (_Metodo.ValidaFecha(txtFechaLimiteSupervisionTab2.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha limite de pago.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }



            if (txtAvanceRealSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtAmortizacionDirectaSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto de amortización directa.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlEstadoSituacionalSupervisionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese estado situacional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }
        protected void btnGuardarValorizacionSupervisorTab2_Click(object sender, EventArgs e)
        {
            if (ValidarValorizacionSupervisionTab2())
            {
                if (validaArchivo(FileUploadComprobanteSupervisionTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;

                    _BEEjecucion.id_tipoValorizacion = ddlTipoValorizacionSupervisionTab2.SelectedValue;
                    //_BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;
                    //_BEEjecucion.nroValorizacionAdicional = txtValorizacionAdicionalTab2.Text;

                    _BEEjecucion.Date_fecha = Convert.ToDateTime(ddlMesSupervisionTab2.SelectedValue);

                    _BEEjecucion.Date_fechaInforme = Convert.ToDateTime(txtFechaPresentacionSupervisionTab2.Text);
                    _BEEjecucion.monto = txtMontoValorizacionSupervisionTab2.Text;
                    _BEEjecucion.Date_fechaPago = Convert.ToDateTime(txtFechaLimiteSupervisionTab2.Text);

                    _BEEjecucion.fisicoReal = txtAvanceRealSupervisionTab2.Text;
                    _BEEjecucion.montoAdelantoDirecto = txtAmortizacionDirectaSupervisionTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalSupervisionTab2.SelectedValue);
                    _BEEjecucion.observacion = txtObservacionSupervisionTab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadComprobanteSupervisionTab2);

                    //
                    //_BEEjecucion.monto = txtMontoAdicionalTab2.Text;

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_spi_MON_InformeAvanceSupervisionPorContrata(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaValorizacionSupervisionTab2();

                        Panel_AgregarValorizacionSupervisionTab2.Visible = false;
                        imgbtnAgregarValorizacionSupervisionTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }
        }
        protected void btnModificarValorizacionSupervisorTab2_Click(object sender, EventArgs e)
        {
            if (ValidarValorizacionSupervisionTab2())
            {
                if (validaArchivo(FileUploadComprobanteSupervisionTab2))
                {
                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblIdValorizacionSupervisionTab2.Text);

                    _BEEjecucion.id_tipoValorizacion = ddlTipoValorizacionSupervisionTab2.SelectedValue;
                    //_BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;
                    //_BEEjecucion.nroValorizacionAdicional = txtValorizacionAdicionalTab2.Text;

                    _BEEjecucion.Date_fechaInforme = Convert.ToDateTime(txtFechaPresentacionSupervisionTab2.Text);
                    _BEEjecucion.monto = txtMontoValorizacionSupervisionTab2.Text;
                    _BEEjecucion.Date_fechaPago = Convert.ToDateTime(txtFechaLimiteSupervisionTab2.Text);

                    _BEEjecucion.fisicoReal = txtAvanceRealSupervisionTab2.Text;
                    _BEEjecucion.montoAdelantoDirecto = txtAmortizacionDirectaSupervisionTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalSupervisionTab2.SelectedValue);
                    _BEEjecucion.observacion = txtObservacionSupervisionTab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadComprobanteSupervisionTab2);

                    _BEEjecucion.Tipo = 1;
                    //
                    //_BEEjecucion.monto = txtMontoAdicionalTab2.Text;

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.UD_spud_MON_InformeAvanceSupervisionPorContrata(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaValorizacionSupervisionTab2();

                        //txtMontoAdicionalTab2.Text = "";
                        //txtfechaAdicionalTab2.Text = "";

                        Panel_AgregarValorizacionSupervisionTab2.Visible = false;
                        imgbtnAgregarValorizacionSupervisionTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }
        }
        protected void btnCancelarValorizacionSupervisorTab2_Click(object sender, EventArgs e)
        {
            Panel_AgregarValorizacionSupervisionTab2.Visible = false;
            imgbtnAgregarValorizacionSupervisionTab2.Visible = true;

            Up_Tab2.Update();
        }

        protected Boolean ValidarGeneracionProgramacion()
        {
            Boolean result;
            result = true;

            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = "1";
            dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);
            string vMontoContratado = "";
            if (dt.Rows.Count > 0)
            {
                vMontoContratado = dt.Rows[0]["MontoConsorcio"].ToString();
            }

            if (vMontoContratado.Equals("") || vMontoContratado.Equals("0.00") || vMontoContratado.Equals("0"))
            {
                string script = "<script>alert('Ingresar monto contratado en la pestaña Proceso de Selección');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;
            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);
            string plazo = "";
            string fechaInicio = "";
            if (dt.Rows.Count > 0)
            {
                plazo = dt.Rows[0]["plazoEjecucion"].ToString();
                fechaInicio = dt.Rows[0]["fechaInicio"].ToString();
            }

            if (fechaInicio == "")
            {
                string script = "<script>alert('Registrar Fecha Inicio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (plazo == "")
            {
                string script = "<script>alert('Registrar plazo de ejecución (días).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;

        }
        protected void btnGenerarProgramacionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarGeneracionProgramacion())
            {
                DateTime dtFechaInicio = Convert.ToDateTime(txtFechaInicioTab2.Text);
                int iPlazo = Convert.ToInt32(txtPlazoEjecucionTab2.Text);

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int valor = _objBLEjecucion.I_spi_MON_GeneracionProgramacion(_BEEjecucion);

                if (lblCOD_SUBSECTOR.Text == "3")
                {
                    GeneraProgramacionPNSR(iPlazo, dtFechaInicio);
                }
                else
                {
                    GeneraProgramacionPNSU(iPlazo, dtFechaInicio);
                }

                string script = "<script>alert('Se generó la programación correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                Panel_RegistrarProgramacionTab2.Visible = false;
                //imgbtnAgregarProgramacionTab2.Visible = true;
                cargaProgramacionTab2();
                CargaDatosTab2();
                Up_Tab2.Update();

            }
        }

        protected void GeneraProgramacionPNSU(int iPlazo, DateTime dtFechaInicio)
        {
            if (iPlazo == 1)
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;
                _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                _BEEjecucion.fisicoProgramado = "0";
                _BEEjecucion.financieroProgramado = "0";
                _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(1);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                iPlazo = 0;
            }
            else
            {
                if (iPlazo > 0)
                {
                    iPlazo = iPlazo - 1;
                }

                while (iPlazo > 0)
                {
                    int iDiasMes = System.DateTime.DaysInMonth(dtFechaInicio.Year, dtFechaInicio.Month) - dtFechaInicio.Day;


                    if (iDiasMes < iPlazo)
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                        _BEEjecucion.fisicoProgramado = "0";
                        _BEEjecucion.financieroProgramado = "0";
                        _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                        _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iDiasMes);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                        dtFechaInicio = dtFechaInicio.AddDays(iDiasMes + 1);

                        if (iPlazo - iDiasMes == 1)
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                            _BEEjecucion.fisicoProgramado = "0";
                            _BEEjecucion.financieroProgramado = "0";
                            _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                            _BEEjecucion.Date_fechaFin = dtFechaInicio;
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int valFin = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);
                        }

                        iPlazo = iPlazo - iDiasMes - 1;

                    }
                    else
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                        _BEEjecucion.fisicoProgramado = "0";
                        _BEEjecucion.financieroProgramado = "0";
                        _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                        _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iPlazo);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val2 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                        iPlazo = iPlazo - iPlazo;
                    }

                }

                if (iPlazo <= 0)
                {
                    //iPlazo = iPlazo - 1;
                    DateTime NewFecha = dtFechaInicio.AddMonths(1);

                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.dtFechaProgramada = NewFecha;
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    int val3 = _objBLEjecucion.D_spd_MON_CronogramaProgramacion(_BEEjecucion);
                }
            }
        }

        protected void GeneraProgramacionPNSR(int iPlazo, DateTime dtFechaInicio)
        {
            if (iPlazo == 1)
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;
                _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                _BEEjecucion.fisicoProgramado = "0";
                _BEEjecucion.financieroProgramado = "0";
                _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(1);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                iPlazo = 0;
            }
            else
            {
                if (iPlazo > 0)
                {
                    iPlazo = iPlazo - 1;
                }

                while (iPlazo > 0)
                {
                    //int iDiasMes = System.DateTime.DaysInMonth(dtFechaInicio.Year, dtFechaInicio.Month) - dtFechaInicio.Day;
                    int iDiasMes = 0;
                    if (dtFechaInicio.Day < 16)
                    {
                        iDiasMes = 15 - dtFechaInicio.Day;
                    }
                    else
                    {
                        iDiasMes = System.DateTime.DaysInMonth(dtFechaInicio.Year, dtFechaInicio.Month) - dtFechaInicio.Day + 15;
                    }


                    if (iDiasMes < iPlazo)
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                        _BEEjecucion.fisicoProgramado = "0";
                        _BEEjecucion.financieroProgramado = "0";
                        _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                        _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iDiasMes);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                        dtFechaInicio = dtFechaInicio.AddDays(iDiasMes + 1);

                        if (iPlazo - iDiasMes == 1)
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                            _BEEjecucion.fisicoProgramado = "0";
                            _BEEjecucion.financieroProgramado = "0";
                            _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                            _BEEjecucion.Date_fechaFin = dtFechaInicio;
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int valFin = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);
                        }

                        iPlazo = iPlazo - iDiasMes - 1;

                    }
                    else
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                        _BEEjecucion.fisicoProgramado = "0";
                        _BEEjecucion.financieroProgramado = "0";
                        _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                        _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iPlazo);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val2 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                        iPlazo = iPlazo - iPlazo;
                    }

                }

                if (iPlazo <= 0)
                {
                    //iPlazo = iPlazo - 1;
                    DateTime NewFecha = dtFechaInicio.AddMonths(1);

                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.dtFechaProgramada = NewFecha;
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    int val3 = _objBLEjecucion.D_spd_MON_CronogramaProgramacion(_BEEjecucion);
                }
            }
        }
        protected void cargaProgramacionTab2()
        {
            //Limpiar totales
          
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListProgramacion = new List<BE_MON_Ejecucion>();

            ListProgramacion = _objBLEjecucion.F_spMON_ProgramacionInforme(_BEEjecucion);

            grdProgramacionTab2.DataSource = ListProgramacion;
            grdProgramacionTab2.DataBind();

            lblFisicoProgAcumuladoCronogramTab2.Text = (ListProgramacion.Where(x => x.fisicoProgramado.Length > 0).Sum(x => Convert.ToDouble(x.fisicoProgramado))).ToString("N");
            lblMontoAcumuladoCronogramaTab2.Text = (ListProgramacion.Where(x=>x.monto.Length>0).Sum(x => Convert.ToDouble(x.monto))).ToString("N");

            if (ListProgramacion != null)
            {
                ListProgramacion = (from cust in ListProgramacion
                                    where cust.flagVerificado == "0" && cust.monto.Length>0
                                    select cust).ToList();
            }

            ddlFechaProgramacionInformeTab2.DataSource = ListProgramacion;
            ddlFechaProgramacionInformeTab2.DataTextField = "nombre";
            ddlFechaProgramacionInformeTab2.DataValueField = "id_avanceFisico";
            ddlFechaProgramacionInformeTab2.DataBind();
            ddlFechaProgramacionInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void grdProgramacionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdProgramacionTab2.SelectedDataKey.Value.ToString();
            lblIdProgramacion.Text = id_avanceFisico.ToString();

            GridViewRow row = grdProgramacionTab2.SelectedRow;

            txtInformeTab2.Text = "Del " + ((Label)row.FindControl("lblFechaInicioInformar")).Text + " al " + ((Label)row.FindControl("lblFechaFinInformar")).Text;
            txtInicioInformeTab2.Text = ((Label)row.FindControl("lblFechaInicioInformar")).Text;
            txtFinInformeTab2.Text = ((Label)row.FindControl("lblFechaFinInformar")).Text;
            txtMesTab2.Text = ((Label)row.FindControl("lblMes")).Text;

            txtMontoProgramacionTab2.Text = ((Label)row.FindControl("lblValorizacionProgramado")).Text;
            if (txtMontoProgramacionTab2.Text.Equals(""))
            {
                txtMontoProgramacionTab2.Text = "0";
            }
            lblMontoPreAcumuladoCronogramaTab2.Text = (Convert.ToDouble(lblMontoAcumuladoCronogramaTab2.Text) - Convert.ToDouble(txtMontoProgramacionTab2.Text)).ToString();
            // txtFechaProgramadaTab2.Text = ((Label)row.FindControl("lblFechaProgramada")).Text;

            //txtDiasAsistirTab2.Text = ((Label)row.FindControl("lblDiasAsistir")).Text;


            //txtAvanceFisicoProgramacionTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            //txtAvanceFinancieroProgramacionTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;

            //ddlFuncionProgramacionTab2.SelectedValue = ((Label)row.FindControl("lblIdProyectoTecnico")).Text;

            lblNomUsuarioProgramacionTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            if (((Label)row.FindControl("lblFlagInforme")).Text == "1")
            {
                //btnModificarProgramacionTab2.Visible = false;
                lblMsjProgramacionTab2.Visible = true;
            }
            else
            {
                //btnModificarProgramacionTab2.Visible = true;
                lblMsjProgramacionTab2.Visible = false;
            }
            //btnGuardarProgramacionTab2.Visible = false;

            Panel_RegistrarProgramacionTab2.Visible = true;
            //imgbtnAgregarProgramacionTab2.Visible = false;

            Up_Tab2.Update();
        }


        protected void cargaValorizacionSupervisionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListValorizacion = new List<BE_MON_Ejecucion>();

            lblTotalValorizacionSupervisionTab2.Text = "0";
            lblAmortizacionSupervisionTotal.Text = "0";
            lblAvanceTotalSupervisionTab2.Text = "0";

            ListValorizacion = _objBLEjecucion.F_spMON_InformeAvanceSupervisionPorContrata(_BEEjecucion);
            grdValorizacionSupervisionTab2.DataSource = ListValorizacion;
            grdValorizacionSupervisionTab2.DataBind();

            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListMeses = new List<BE_MON_Ejecucion>();
            ListMeses = _objBLEjecucion.F_spMON_MesesValorizacionSupervision(_BEEjecucion);
            ddlMesSupervisionTab2.DataSource = ListMeses;
            ddlMesSupervisionTab2.DataTextField = "mes";
            ddlMesSupervisionTab2.DataValueField = "Date_fechaFin";
            ddlMesSupervisionTab2.DataBind();

            ddlMesSupervisionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected Boolean ValidarProgramacionTab2()
        {
            Boolean result;
            result = true;

            double totd = 0;
            double tota = 0;
            if (hfTotalAdicional.Value != "")
                tota = Convert.ToDouble(hfTotalAdicional.Value);

            if (hfTotalDeductivo.Value != "")
                totd = Convert.ToDouble(hfTotalDeductivo.Value);

            double totaldivisor = (Convert.ToDouble(txtMontoContratadoTab2.Text) + tota - totd);

            if (txtMontoProgramacionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto de valorización programada.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            double valorFisico;
            //valorFisico = Convert.ToDouble(txtMontoProgramacionTab2.Text) / Convert.ToDouble(txtMontoContratadoTab2.Text);
            valorFisico = Convert.ToDouble(txtMontoProgramacionTab2.Text) / totaldivisor;
            valorFisico = valorFisico * 100;

            if (valorFisico > 100)
            {
                string script = "<script>alert('No se puede registrar avances programados mayor a 100%.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (btnModificarProgramacionTab2.Visible == true)
            {
                double valorFisico2;
                //valorFisico2 = (Convert.ToDouble(lblMontoPreAcumuladoCronogramaTab2.Text) + Convert.ToDouble(txtMontoProgramacionTab2.Text)) / Convert.ToDouble(txtMontoContratadoTab2.Text);
                valorFisico2 = (Convert.ToDouble(lblMontoPreAcumuladoCronogramaTab2.Text) + Convert.ToDouble(txtMontoProgramacionTab2.Text)) / totaldivisor;
                valorFisico2 = valorFisico2 * 100;

                if (valorFisico2 > 101)
                {
                    string script = "<script>alert('El avance físico real acumulado es: " + valorFisico2.ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }
            return result;
        }

        protected void btnModificarProgramacionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarProgramacionTab2())
            {
                _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblIdProgramacion.Text);
                //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionProgramacionTab2.SelectedValue);
                //_BEEjecucion.dtFechaProgramada = VerificaFecha(txtFechaProgramadaTab2.Text);
                _BEEjecucion.monto = txtMontoProgramacionTab2.Text;
                double valorFisico;

                //(Convert.ToDouble(txtMontoContratadoTab1.Text) + Convert.ToDouble(hfTotalAdicional.Value) - Convert.ToDouble(hfTotalDeductivo.Value))
                valorFisico = Convert.ToDouble(txtMontoProgramacionTab2.Text) / (Convert.ToDouble(txtMontoContratadoTab1.Text) + Convert.ToDouble(hfTotalAdicional.Value) - Convert.ToDouble(hfTotalDeductivo.Value));
                valorFisico = valorFisico * 100;

                _BEEjecucion.fisicoProgramado = valorFisico.ToString("N2");

                _BEEjecucion.Tipo = 1;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_ProgramacionInformePorContrata(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modifico la programación correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistrarProgramacionTab2.Visible = false;
                    //imgbtnAgregarProgramacionTab2.Visible = true;
                    cargaProgramacionTab2();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void btnCancelarProgramacionTab2_Click(object sender, EventArgs e)
        {
            Panel_RegistrarProgramacionTab2.Visible = false;
        }
        protected void rbObservacionTab3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbObservacionTab3.SelectedValue == "1")
            {
                Panel_ObservacionesTab3.Visible = true;
            }
            else
            {
                Panel_ObservacionesTab3.Visible = false;
            }

            Up_Tab3.Update();
        }
  
        protected void ddlFechaProgramacionInformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFechaProgramacionInformeTab2.SelectedValue != "")
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;

                List<BE_MON_Ejecucion> ListProgramacion = new List<BE_MON_Ejecucion>();

                ListProgramacion = _objBLEjecucion.F_spMON_ProgramacionInforme(_BEEjecucion);

                ListProgramacion = (from cust in ListProgramacion
                                    where cust.id_avanceFisico == Convert.ToInt32(ddlFechaProgramacionInformeTab2.SelectedValue)
                                    select cust).ToList();

                lblId_Registro_Avance.Text = ListProgramacion.ElementAt(0).id_avanceFisico.ToString();
                txtFisiProgTab2.Text = ListProgramacion.ElementAt(0).fisicoProgramado;
                //txtDiasProgramadoInformesTab2.Text = ListProgramacion.ElementAt(0).diasProgramado.ToString();
                //txtFinanProgTab2.Text = ListProgramacion.ElementAt(0).financieroProgramado;
            }

        }
        protected void txtValorizacionEjecutadaTab2_TextChanged(object sender, EventArgs e)
        {
            if (txtValorizacionEjecutadaTab2.Text == "")
            {
                txtFisiRealTab2.Text = "";
            }
            else
            {
                if (txtMontoContratadoTab1.Text == "")
                {
                    txtFisiRealTab2.Text = "0.00";
                }
                else
                {
                    txtFisiRealTab2.Text = ((Convert.ToDouble(txtValorizacionEjecutadaTab2.Text) / (Convert.ToDouble(txtMontoContratadoTab1.Text) + Convert.ToDouble(hfTotalAdicional.Value) - Convert.ToDouble(hfTotalDeductivo.Value))) * 100).ToString("N2");
                }
            }
        }

        protected Boolean ValidarSupervisionTab2()
        {
            Boolean result;
            result = true;

            if (txtFechaInicioSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de inicio de supervision.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtPlazoServicioTab2.Text == "")
            {
                string script = "<script>alert('Ingresar plazo de servicio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (rbFlagAdelantoTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar adelanto SI/NO.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            return result;
        }
        protected void btnGuardarSupervisionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarSupervisionTab2())
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;

                _BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaInicioSupervisionTab2.Text);
                _BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoServicioTab2.Text);
                _BEEjecucion.supervisorDesignado = txtJefeSupervisionTab2.Text;

                if (rbFlagAdelantoTab2.SelectedValue == "1")
                {
                    _BEEjecucion.flag = 1;
                    _BEEjecucion.montoAdelantoDirecto = txtMontoAdelantoSupervisionTab2.Text;
                    _BEEjecucion.Date_fechaAdelantoDirecto = VerificaFecha(txtFechaAdelantoSupervisionTab2.Text);
                }
                else
                {
                    _BEEjecucion.flag = 0;
                    _BEEjecucion.montoAdelantoDirecto = "0";
                    _BEEjecucion.Date_fechaAdelantoDirecto = VerificaFecha("");
                }

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spiu_MON_EjecucionSupervisionPorContrata(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente la información de supervision.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaDatosSupervisionTab2();

                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdHistorialJefeSupervisionTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdHistorialJefeSupervisionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;

                    dt = CargaEmpleadoObraSupervisionTab2(3);
                    if (dt.Rows.Count > 0)
                    {

                        txtJefeSupervisionTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtJefeSupervisionTab2.Text = "";

                    }


                    Panel_AgregarJefeSupervisionTab2.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdHistorialJefeSupervisionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grdHistorialJefeSupervisionTab2.SelectedDataKey.Value.ToString();
            lblIDSupervisionTab2.Text = id;
            GridViewRow row = grdHistorialJefeSupervisionTab2.SelectedRow;

            txtNombreJefeSupervisionTab2.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtCIPSupervisionTab2.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtTelefonoSupervisionTab2.Text = ((Label)row.FindControl("lblTelefono")).Text;
            txtCorreoSupervisionTab2.Text = ((Label)row.FindControl("lblCorreo")).Text;
            txtCAPSupervisionTab2.Text = ((Label)row.FindControl("lblCap")).Text;
            txtFechaDesignacionSupervicionTab2.Text = ((Label)row.FindControl("lblFechaII")).Text;

            lnkbtnDocSupervisionTab2.Text = ((ImageButton)row.FindControl("imgDocSupervisionGri")).ToolTip;
            GeneraIcoFile(lnkbtnDocSupervisionTab2.Text, imgbtnDocSupervisionTab2);

            btnGrabarJefeSupervisionTab2.Visible = false;
            btnModificarJefeSupervisionTab2.Visible = true;
            btnCancelarJefeSupervisionTab2.Visible = true;
            Up_Tab2.Update();
        }
        protected void grdProfesionalSupervisionTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdProfesionalSupervisionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_ProfesionalTecnicoSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosSupervisionTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdProfesionalSupervisionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grdProfesionalSupervisionTab2.SelectedDataKey.Value.ToString();
            lblIDProfesionSupervisionTab2.Text = id;
            GridViewRow row = grdProfesionalSupervisionTab2.SelectedRow;

            txtNombreProfesionSupervisionTab2.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtCargoProfesionalSupervisionTab2.Text = ((Label)row.FindControl("lblCargo")).Text;
            txtFechaAsignacionProfesionalTab2.Text = ((Label)row.FindControl("lblFechaAsignacion")).Text;
            txtTelefonoProfesionalTab2.Text = ((Label)row.FindControl("lblTelefono")).Text;
            txtCorreoProfesionalTab2.Text = ((Label)row.FindControl("lblCorreo")).Text;
            txtParticipacionProfesionalTab2.Text = ((Label)row.FindControl("lblParticipacion")).Text;

            lblNomActualizaProfesionalsupervisorTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnGuardarProfesionalSupervisorTab2.Visible = false;
            btnModificarProfesionalSupervisorTab2.Visible = true;

            Panel_AgregarProfesionalSupervisorTab2.Visible = true;
            Up_ProfesionalSupervisionTab2.Update();
        }
        protected void btnGuardarFianzaSupervisionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarFianzaSupervisionTab2())
            {
                if (validaArchivo(FileUploadCartaFianzaSupervisionTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                    _BEEjecucion.entidadFinanciera = txtEntidadFinacieraSupervisionTab2.Text;
                    _BEEjecucion.nroCarta = txtNroCartaSupervisionTab2.Text;
                    _BEEjecucion.tipoCarta = Convert.ToInt32(ddlTipoFianzaSupervisionTab2.SelectedValue);

                    _BEEjecucion.Date_fechaInicio = Convert.ToDateTime(txtFechaInicioVigenciaSupervisionTab2.Text);
                    _BEEjecucion.Date_fechaFin = Convert.ToDateTime(txtFechaFinVigenciaSupervisionTab2.Text);
                    _BEEjecucion.Date_fechaRenovacion = Convert.ToDateTime(VerificaFecha(txtFechaRenovacionSupervisionTab2.Text));
                    _BEEjecucion.monto = txtmontoFianzaSupervisionTab2.Text;

                    _BEEjecucion.porcentaje = txtPorcentajeSupervisionTab2.Text;
                    if (chkVerificadoFianzaSupervisionTab2.Checked == true)
                    {
                        _BEEjecucion.flagVerificado = "1";
                    }

                    //if (chkVRenovadoFianzaSupervisionTab2.Checked == true)
                    //{
                    //    _BEEjecucion.flagRenovado = "1";
                    //}

                    _BEEjecucion.idTipoAccion = ddlTipoAccionSupervisionTab2.SelectedValue;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaFianzaSupervisionTab2);
                    _BEEjecucion.urlDocAccion = _Metodo.uploadfile(FileUploadCartaFianzaSupervisionTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.spi_MON_CartaFianzaSupervision(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaCartasFianzaSupervisionTab2();

                        Panel_CartaSupervisionTab2.Visible = false;
                        imgbtnAgregarCartaSupervisionTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }
        }
        protected void btnModificarFianzaSupervisionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarFianzaSupervisionTab2())
            {
                if (validaArchivo(FileUploadCartaFianzaSupervisionTab2))
                {
                    _BEEjecucion.id_carta_fianza = Convert.ToInt32(lblIDCartaFianzaSupervision.Text);

                    _BEEjecucion.entidadFinanciera = txtEntidadFinacieraSupervisionTab2.Text;
                    _BEEjecucion.nroCarta = txtNroCartaSupervisionTab2.Text;
                    _BEEjecucion.tipoCarta = Convert.ToInt32(ddlTipoFianzaSupervisionTab2.SelectedValue);

                    _BEEjecucion.Date_fechaInicio = Convert.ToDateTime(txtFechaInicioVigenciaSupervisionTab2.Text);
                    _BEEjecucion.Date_fechaFin = Convert.ToDateTime(txtFechaFinVigenciaSupervisionTab2.Text);
                    _BEEjecucion.Date_fechaRenovacion = Convert.ToDateTime(VerificaFecha(txtFechaRenovacionSupervisionTab2.Text));
                    _BEEjecucion.monto = txtmontoFianzaSupervisionTab2.Text;

                    _BEEjecucion.porcentaje = txtPorcentajeSupervisionTab2.Text;
                    _BEEjecucion.idTipoAccion = ddlTipoAccionSupervisionTab2.SelectedValue;
                    if (chkVerificadoFianzaSupervisionTab2.Checked == true)
                    {
                        _BEEjecucion.flagVerificado = "1";
                    }

                    if (FileUploadAccionSupervisionTab2.Visible==true && FileUploadAccionSupervisionTab2.PostedFile.ContentLength > 0)
                    { 
                        //if (FileUploadAccionSupervisionTab2.PostedFile.ContentLength > 0)
                            _BEEjecucion.urlDocAccion = _Metodo.uploadfile(FileUploadAccionSupervisionTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDocAccion = lnkbtnAccionSupervisionTab2.Text;
                    }

                    if (FileUploadCartaFianzaSupervisionTab2.PostedFile.ContentLength > 0)
                    {
                        _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaFianzaSupervisionTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDoc = lnkbtnCartaFianzaSupervisionTab2.Text;
                    }

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int val = _objBLEjecucion.spud_MON_Seguimiento_Carta_Fianza_Editar(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaCartasFianzaSupervisionTab2();

                        lnkbtnCartaFianzaSupervisionTab2.Text = "";
                        imgbtnCartaFianzaSupervisionTab2.ImageUrl = "~/img/blanco.png";
                        imgbtnAgregarCartaSupervisionTab2.Visible = true;
                        Panel_CartaSupervisionTab2.Visible = false;

                        chkVerificadoFianzaSupervisionTab2.Checked = false;
                        //chkVRenovadoFianzaSupervisionTab2.Checked = false;
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        Panel_CartaSupervisionTab2.Visible = false;
                        cargaCartasFianzaSupervisionTab2();
                        //btnModificarFianzasTab2.Visible = true;

                        Up_Tab2.Update();

                    }
                }
            }
        }
        protected void btnCancelarFianzaSupervisionTab2_Click(object sender, EventArgs e)
        {
            imgbtnAgregarCartaSupervisionTab2.Visible = true;
            Panel_CartaSupervisionTab2.Visible = false;
        }

        protected Boolean ValidarFianzaSupervisionTab2()
        {
            Boolean result;
            result = true;

            if (txtEntidadFinacieraSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese Entidad Financiera.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtNroCartaSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese N° Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (ddlTipoFianzaSupervisionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

         
            if (txtFechaInicioVigenciaSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio de vigencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFechaFinVigenciaSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de fin de vigencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtmontoFianzaSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtPorcentajeSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese % de cubrimiento de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtPorcentajeSupervisionTab2.Text != "")
            {
                if (ddlTipoFianzaSupervisionTab2.SelectedValue == "1")
                {
                    if (Convert.ToDouble(txtPorcentajeSupervisionTab2.Text) > 10.00)
                    {
                        string script = "<script>alert('El % de cubrimiento debe ser menor a 10%');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }

                if (ddlTipoFianzaSupervisionTab2.SelectedValue == "2")
                {
                    if (Convert.ToDouble(txtPorcentajeSupervisionTab2.Text) > 20.00)
                    {
                        string script = "<script>alert('El % de cubrimiento debe ser menor a 20%');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }

                if (ddlTipoFianzaSupervisionTab2.SelectedValue == "3")
                {
                    if (Convert.ToDouble(txtPorcentajeSupervisionTab2.Text) > 40.00)
                    {
                        string script = "<script>alert('El % de cubrimiento debe ser menor a 40%');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }

            }

            if (ddlTipoAccionSupervisionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de acción');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else if (ddlTipoAccionSupervisionTab2.SelectedValue.Equals("2") && txtFechaRenovacionSupervisionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de renovación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            return result;
        }
        protected void lnkbtnCartaFianzaSupervisionTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnCartaFianzaSupervisionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void chkFlagVerificoSupervision_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var val2 = grdFianzaSupervisionTab2.DataKeys[row.RowIndex].Value;
            _BEEjecucion.id_tabla = Convert.ToInt32(val2.ToString());

            CheckBox chb_flagVerifico = (CheckBox)grdFianzaSupervisionTab2.Rows[row.RowIndex].FindControl("chkFlagVerificoSupervision");
            if (chb_flagVerifico.Checked == true)
            {
                _BEEjecucion.flagVerificado = "1";
            }
           
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_CartaFianza_FlagVerifica(_BEEjecucion);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                cargaCartasFianzasTab2();
                Up_Tab2.Update();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }


        }
           
        protected void grdFianzaSupervisionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_carta_fianza;
            id_carta_fianza = grdFianzaSupervisionTab2.SelectedDataKey.Value.ToString();
            lblIDCartaFianzaSupervision.Text = id_carta_fianza.ToString();
            GridViewRow row = grdFianzaSupervisionTab2.SelectedRow;
            txtEntidadFinacieraSupervisionTab2.Text = ((Label)row.FindControl("lblEntidadFinanciera")).Text;
            txtNroCartaSupervisionTab2.Text = ((Label)row.FindControl("lblNroCarta")).Text;
            ddlTipoFianzaSupervisionTab2.Text = ((Label)row.FindControl("lblIdTipo")).Text;
            txtFechaInicioVigenciaSupervisionTab2.Text = ((Label)row.FindControl("lblFechaInicio")).Text;

            txtFechaFinVigenciaSupervisionTab2.Text = ((Label)row.FindControl("lblFechaFin")).Text;
            txtmontoFianzaSupervisionTab2.Text = ((Label)row.FindControl("lblmontoTab2")).Text;
            if (txtmontoFianzaSupervisionTab2.Text != "")
            {
                txtmontoFianzaSupervisionTab2.Text = Convert.ToDouble(txtmontoFianzaSupervisionTab2.Text).ToString("N");
            }

            //txtmontoFianzaTab2.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtPorcentajeSupervisionTab2.Text = ((Label)row.FindControl("lblPorcentaje")).Text;
            chkVerificadoFianzaSupervisionTab2.Checked = ((CheckBox)row.FindControl("chkFlagVerificoSupervision")).Checked;
            //chkVRenovadoFianzaSupervisionTab2.Checked = ((CheckBox)row.FindControl("chkFlagRenuevaSupervision")).Checked;

            ddlTipoAccionSupervisionTab2.Text = ((Label)row.FindControl("lblIdTipoAccion")).Text;
            txtFechaRenovacionSupervisionTab2.Text = ((Label)row.FindControl("lblRenovacion")).Text;
            lnkbtnAccionSupervisionTab2.Text = ((ImageButton)row.FindControl("imgDocAccionSupervisionGriTab2")).ToolTip;
            GeneraIcoFile(lnkbtnAccionSupervisionTab2.Text, imgbtnAccionSupervisionTab2);

            lnkbtnCartaFianzaSupervisionTab2.Text = ((ImageButton)row.FindControl("imgDocCartaSupervisionTab2")).ToolTip;

            lblNomActualizaFianzaSupervisionTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(lnkbtnCartaFianzaSupervisionTab2.Text, imgbtnCartaFianzaSupervisionTab2);


            if (ddlTipoAccionSupervisionTab2.SelectedValue.ToString().Equals("2"))
            {
                lblTitDocAccionSupervisionTab2.Text = "Fecha de Renovación";
                txtFechaRenovacionSupervisionTab2.Visible = true;
                FileUploadAccionSupervisionTab2.Visible = false;
                
            }
            else if (ddlTipoAccionSupervisionTab2.SelectedValue.ToString().Equals("3") || ddlTipoAccionSupervisionTab2.SelectedValue.ToString().Equals("4"))
            {
                lblTitDocAccionSupervisionTab2.Text = "Doc. " + ddlTipoAccion.SelectedItem.Text;
                txtFechaRenovacionSupervisionTab2.Visible = false;
                FileUploadAccionSupervisionTab2.Visible = true;
            }
            else
            {
                lblTitDocAccionSupervisionTab2.Text = "";
                FileUploadAccionSupervisionTab2.Visible = false;
                //txtFechaRenovacionSupervisionTab2.Text = "";
                txtFechaRenovacionSupervisionTab2.Visible = false;
            }

            Panel_CartaSupervisionTab2.Visible = true;
            imgbtnAgregarCartaSupervisionTab2.Visible = false;

            btnGuardarFianzaSupervisionTab2.Visible = false;
            btnModificarFianzaSupervisionTab2.Visible = true;
            Up_Tab2.Update();

        }
        protected void grdFianzaSupervisionTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdFianzaSupervisionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_carta_fianza = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_Seguimiento_Carta_Fianza_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaCartasFianzaSupervisionTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grdFianzaSupervisionTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDiasRestante = (Label)e.Row.FindControl("lblDiasRestante");
                if (Convert.ToInt32(lblDiasRestante.Text) < 1)
                {
                    lblDiasRestante.ForeColor = Color.Red;
                }

                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocCartaSupervisionTab2");

                GeneraIcoFile(imb.ToolTip, imb);


                CheckBox chkVer = (CheckBox)e.Row.FindControl("chkFlagVerificoSupervision");
                if (chkVer.Text == "1")
                {
                    chkVer.Checked = true;
                }
                chkVer.Text = "";

                ImageButton imgDocAccionSupervisionGriTab2 = (ImageButton)e.Row.FindControl("imgDocAccionSupervisionGriTab2");
                if (imgDocAccionSupervisionGriTab2.ToolTip.Length > 0)
                {
                    GeneraIcoFile(imgDocAccionSupervisionGriTab2.ToolTip, imgDocAccionSupervisionGriTab2);
                }
                else { imgDocAccionSupervisionGriTab2.Visible = false; }

                Label lblIdAccion = (Label)e.Row.FindControl("lblIdTipoAccion");
                if (lblIdAccion.Text.Equals("2") || lblIdAccion.Text.Equals("3") || lblIdAccion.Text.Equals("4"))
                {
                    lblDiasRestante.Text = "";
                }


            }
        }
        protected void imgDocCartaSupervisionTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdFianzaSupervisionTab2.Rows[row.RowIndex].FindControl("imgDocCartaSupervisionTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void ddlTipoValorizacionSupervisionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void lnkbtnComprobanteSupervisionTab2_Click(object sender, EventArgs e)
        {

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnComprobanteSupervisionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdValorizacionSupervisionTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            double TotalAvance;
            TotalAvance = 0;
            double Totalmonto = 0;
            double Totaldirecto = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //total = Convert.ToDouble(txtAcumuladorTranferenciaTab0.Text) + Convert.ToDouble(lblMonto.Text);
                //txtAcumuladorTranferenciaTab0.Text = total.ToString("N");

                Label lblFisico = (Label)e.Row.FindControl("lblFisicoReal");
                ImageButton imgbtnDoc = (ImageButton)e.Row.FindControl("imgDocAvanceSupervisionTab2");
                Label lblMonto = (Label)e.Row.FindControl("lblMonto");
                Label lblAdelantoDirecto = (Label)e.Row.FindControl("lblAdelantoDirecto");

                TotalAvance = Convert.ToDouble(lblAvanceTotalSupervisionTab2.Text) + Convert.ToDouble(lblFisico.Text);
                Totalmonto = Convert.ToDouble(lblTotalValorizacionSupervisionTab2.Text) + Convert.ToDouble(lblMonto.Text);
                Totaldirecto = Convert.ToDouble(lblAmortizacionSupervisionTotal.Text) + Convert.ToDouble(lblAdelantoDirecto.Text);

                lblAvanceTotalSupervisionTab2.Text = TotalAvance.ToString("N");
                lblTotalValorizacionSupervisionTab2.Text = Totalmonto.ToString("N");
                lblAmortizacionSupervisionTotal.Text = Totaldirecto.ToString("N");

                GeneraIcoFile(imgbtnDoc.ToolTip, imgbtnDoc);
            }
        }
        protected void grdValorizacionSupervisionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlMesSupervisionTab2.Visible = false;
            txtMesSupervisionTab2.Visible = true;

            lblIdValorizacionSupervisionTab2.Text = grdValorizacionSupervisionTab2.SelectedDataKey.Value.ToString();
            GridViewRow row = grdValorizacionSupervisionTab2.SelectedRow;
            txtMesSupervisionTab2.Text = ((Label)row.FindControl("lblMesSupervision")).Text;
            ddlTipoValorizacionSupervisionTab2.Text = ((Label)row.FindControl("lblIDTipoValorizacion")).Text;
            txtFechaPresentacionSupervisionTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            //txtfechaTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtMontoValorizacionSupervisionTab2.Text = ((Label)row.FindControl("lblMonto")).Text;
            txtFechaLimiteSupervisionTab2.Text = ((Label)row.FindControl("lblFechaPago")).Text;

            txtAmortizacionDirectaSupervisionTab2.Text = ((Label)row.FindControl("lblAdelantoDirecto")).Text;
            txtAvanceRealSupervisionTab2.Text = ((Label)row.FindControl("lblFisicoReal")).Text;

            ddlEstadoSituacionalSupervisionTab2.SelectedValue = ((Label)row.FindControl("lblIDEstadoSitucion")).Text;
            txtObservacionSupervisionTab2.Text = ((Label)row.FindControl("lblObservacionSupervision")).Text;

            //txtNroAdicionalTab2.Text = ((Label)row.FindControl("lblAdicional")).Text;
            lnkbtnComprobanteSupervisionTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceSupervisionTab2")).ToolTip;

            lblNomActualizaValorizacionSupervisionTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(lnkbtnComprobanteSupervisionTab2.Text, imgbtnComprobanteSupervisionTab2);


            btnGuardarValorizacionSupervisorTab2.Visible = false;
            btnModificarValorizacionSupervisorTab2.Visible = true;
            Panel_AgregarValorizacionSupervisionTab2.Visible = true;
            imgbtnAgregarValorizacionSupervisionTab2.Visible = false;

            Up_Tab2.Update();
        }
        protected void grdValorizacionSupervisionTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdValorizacionSupervisionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_tipoValorizacion = "0";
                _BEEjecucion.monto = "0";
                _BEEjecucion.fisicoReal = "0";
                _BEEjecucion.montoAdelantoDirecto = "0";

                _BEEjecucion.estadoSituacional = "0";
                _BEEjecucion.observacion = "";
                _BEEjecucion.urlDoc = "";

                _BEEjecucion.Date_fechaInforme = DateTime.Now;
                _BEEjecucion.Date_fechaPago = DateTime.Now;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_InformeAvanceSupervisionPorContrata(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaValorizacionSupervisionTab2();



                    //txtMontoAdicionalTab2.Text = "";
                    //txtfechaAdicionalTab2.Text = "";

                    Panel_AgregarValorizacionSupervisionTab2.Visible = false;
                    imgbtnAgregarValorizacionSupervisionTab2.Visible = true;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void imgDocAvanceSupervisionTab2_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdValorizacionSupervisionTab2.Rows[row.RowIndex].FindControl("imgDocAvanceSupervisionTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocDiagnosticoTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdInformeDiagnosticoTab2.Rows[row.RowIndex].FindControl("imgDocDiagnosticoTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtnAgregarDiagnosticoTab2_Click(object sender, ImageClickEventArgs e)
        {
            imgbtnAgregarDiagnosticoTab2.Visible = false;
            Panel_AgregarDiagnosticoTab2.Visible = true;
            btnGuardarDiagnosticoTab2.Visible = true;
            btnModificarDiagnosticoTab2.Visible = false;
        }
        protected void lnkbtnDiagnosticoTab2_Click(object sender, EventArgs e)
        {
            //LinkButton boton;
            //boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnDiagnosticoTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected Boolean ValidarInformeDiagnosticoTab2()
        {
            Boolean result;
            result = true;

            if (txtFechaPresentacionDiagnosticoTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de presentación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;

        }
        protected void btnGuardarDiagnosticoTab2_Click(object sender, EventArgs e)
        {
            if (ValidarInformeDiagnosticoTab2())
            {
                if (validaArchivo(FileUpload_DiagnosticoTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.id_tipodocumento = "1";

                    _BEEjecucion.Date_fechaInicio = VerificaFecha("");
                    _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionDiagnosticoTab2.Text);
                    _BEEjecucion.estadoSituacional = txtDetalleSituacionalDiagnosticoTab2.Text;
                    _BEEjecucion.observacion = txtObservacionDiagnosticoTab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUpload_DiagnosticoTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_MON_InformeSupervision(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaInformes("1", grdInformeDiagnosticoTab2);


                        Panel_AgregarDiagnosticoTab2.Visible = false;
                        imgbtnAgregarDiagnosticoTab2.Visible = true;

                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }
        }
        protected void btnCancelarDiagnosticoTab2_Click(object sender, EventArgs e)
        {
            imgbtnAgregarDiagnosticoTab2.Visible = true;
            Panel_AgregarDiagnosticoTab2.Visible = false;
        }
        protected void btnModificarDiagnosticoTab2_Click(object sender, EventArgs e)
        {
            if (ValidarInformeDiagnosticoTab2())
            {
                if (validaArchivo(FileUpload_DiagnosticoTab2))
                {
                    _BEEjecucion.id_item = Convert.ToInt32(lblID_DiagnosticoTab2.Text);
                    _BEEjecucion.Tipo = 1;

                    _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionDiagnosticoTab2.Text);
                    _BEEjecucion.estadoSituacional = txtDetalleSituacionalDiagnosticoTab2.Text;
                    _BEEjecucion.observacion = txtObservacionDiagnosticoTab2.Text;

                    if (FileUpload_DiagnosticoTab2.PostedFile.ContentLength > 0)
                    {
                        _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUpload_DiagnosticoTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDoc = lnkbtnDiagnosticoTab2.Text;
                    }

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.UD_MON_InformeSupervision(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se modificó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaInformes("1", grdInformeDiagnosticoTab2);

                        Panel_AgregarDiagnosticoTab2.Visible = false;
                        imgbtnAgregarDiagnosticoTab2.Visible = true;

                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }
        }

        protected void grdInformeDiagnosticoTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocDiagnosticoTab2");
                GeneraIcoFile(imb.ToolTip, imb);
            }

        }
        protected void grdInformeDiagnosticoTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblID_DiagnosticoTab2.Text = grdInformeDiagnosticoTab2.SelectedDataKey.Value.ToString();

            GridViewRow row = grdInformeDiagnosticoTab2.SelectedRow;


            txtFechaPresentacionDiagnosticoTab2.Text = ((Label)row.FindControl("lblFechaPresentacion")).Text;
            txtDetalleSituacionalDiagnosticoTab2.Text = ((Label)row.FindControl("lblDetalleEstadoSituacional")).Text;
            txtObservacionDiagnosticoTab2.Text = ((Label)row.FindControl("lblObservacion")).Text;

            lnkbtnDiagnosticoTab2.Text = ((ImageButton)row.FindControl("imgDocDiagnosticoTab2")).ToolTip;
            GeneraIcoFile(lnkbtnDiagnosticoTab2.Text, imgbtnDiagnosticoTab2);

            lblNomActualizaDiagnosticoTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            Panel_AgregarDiagnosticoTab2.Visible = true;
            imgbtnAgregarDiagnosticoTab2.Visible = false;
            btnModificarDiagnosticoTab2.Visible = true;
            btnGuardarDiagnosticoTab2.Visible = false;

            Up_Tab2.Update();
        }

        protected void grdInformeDiagnosticoTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdInformeDiagnosticoTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;

                _BEEjecucion.Date_fechaInforme = VerificaFecha("");
                _BEEjecucion.estadoSituacional = "";
                _BEEjecucion.observacion = "";

                _BEEjecucion.urlDoc = "";

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_InformeSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    CargaInformes("1", grdInformeDiagnosticoTab2);

                    Panel_AgregarDiagnosticoTab2.Visible = false;
                    imgbtnAgregarDiagnosticoTab2.Visible = true;

                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }


        protected void imgDocFinalTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdInformeFinalTab2.Rows[row.RowIndex].FindControl("imgDocFinalTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtnAgregarInformeFinalTab2_Click(object sender, ImageClickEventArgs e)
        {

            imgbtnAgregarInformeFinalTab2.Visible = false;
            Panel_InformeFinalTab2.Visible = true;

            btnGuardarinformeFinalTab2.Visible = true;
            btnModificarInformeFinalTab2.Visible = false;

            //Up_Tab2.Update();
        }
        protected void btnCancelarInformeFinalTab2_Click(object sender, EventArgs e)
        {

            imgbtnAgregarInformeFinalTab2.Visible = true;
            Panel_InformeFinalTab2.Visible = false;

            //Up_Tab2.Update();
        }

        protected Boolean ValidarInformeFinalTab2()
        {
            Boolean result;
            result = true;

            if (txtFechaPresentacionFinalTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de presentación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;

        }
        protected void btnGuardarinformeFinalTab2_Click(object sender, EventArgs e)
        {
            if (ValidarInformeFinalTab2())
            {
                if (validaArchivo(FileUpload_InformeFinalTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.id_tipodocumento = "3";

                    _BEEjecucion.Date_fechaInicio = VerificaFecha("");
                    _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionFinalTab2.Text);
                    _BEEjecucion.estadoSituacional = txtDetalleEstadoSituacionalFinalTab2.Text;
                    _BEEjecucion.observacion = txtObservacionFinalTab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUpload_InformeFinalTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_MON_InformeSupervision(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaInformes("3", grdInformeFinalTab2);

                        imgbtnAgregarInformeFinalTab2.Visible = true;
                        Panel_InformeFinalTab2.Visible = false;

                        Up_Tab2.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }

        protected void btnModificarInformeFinalTab2_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_InformeFinalTab2))
            {
                _BEEjecucion.id_item = Convert.ToInt32(lblID_InformeFinal.Text);
                _BEEjecucion.Tipo = 1;

                _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionFinalTab2.Text);
                _BEEjecucion.estadoSituacional = txtDetalleEstadoSituacionalFinalTab2.Text;
                _BEEjecucion.observacion = txtObservacionFinalTab2.Text;

                if (FileUpload_InformeFinalTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUpload_InformeFinalTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = lnkbtnInformeFinalTab2.Text;
                }

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_InformeSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaInformes("3", grdInformeFinalTab2);

                    imgbtnAgregarInformeFinalTab2.Visible = true;
                    Panel_InformeFinalTab2.Visible = false;

                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }

        }

        protected void lnkbtnInformeFinalTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnInformeFinalTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdInformeFinalTab2_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblID_InformeFinal.Text = grdInformeFinalTab2.SelectedDataKey.Value.ToString();

            GridViewRow row = grdInformeFinalTab2.SelectedRow;


            txtFechaPresentacionFinalTab2.Text = ((Label)row.FindControl("lblFechaPresentacion")).Text;
            txtDetalleEstadoSituacionalFinalTab2.Text = ((Label)row.FindControl("lblDetalleEstadoSituacional")).Text;
            txtObservacionFinalTab2.Text = ((Label)row.FindControl("lblObservacion")).Text;

            lnkbtnInformeFinalTab2.Text = ((ImageButton)row.FindControl("imgDocFinalTab2")).ToolTip;
            GeneraIcoFile(lnkbtnInformeFinalTab2.Text, imgbtnInformeFinalTab2);

            lblNomActualizaInformeFinalTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;


            imgbtnAgregarInformeFinalTab2.Visible = false;
            Panel_InformeFinalTab2.Visible = true;
            btnGuardarinformeFinalTab2.Visible = false;
            btnModificarInformeFinalTab2.Visible = true;

            Up_Tab2.Update();
        }
        protected void grdInformeFinalTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdInformeFinalTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;

                _BEEjecucion.Date_fechaInforme = VerificaFecha("");
                _BEEjecucion.estadoSituacional = "";
                _BEEjecucion.observacion = "";

                _BEEjecucion.urlDoc = "";

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_InformeSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    CargaInformes("3", grdInformeFinalTab2);

                    imgbtnAgregarInformeFinalTab2.Visible = true;
                    Panel_InformeFinalTab2.Visible = false;

                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdInformeFinalTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocFinalTab2");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void grdInformeMensualTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocMensualTab2");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        protected void grdInformeMensualTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblID_InformeMensualTab2.Text = grdInformeMensualTab2.SelectedDataKey.Value.ToString();

            GridViewRow row = grdInformeMensualTab2.SelectedRow;

            ddlFechaInformeMensualTab2.SelectedValue = ((Label)row.FindControl("lblFechaInformeMensual")).Text;
            ddlFechaInformeMensualTab2.Enabled = false;
            txtFechaPresentacionMensualTab2.Text = ((Label)row.FindControl("lblFechaPresentacion")).Text;
            txtDetalleEstadoSituacionalMensualTab2.Text = ((Label)row.FindControl("lblDetalleEstadoSituacional")).Text;
            txtObservacionMensualTab2.Text = ((Label)row.FindControl("lblObservacion")).Text;

            lnkbtnInformeMensualTab2.Text = ((ImageButton)row.FindControl("imgDocMensualTab2")).ToolTip;
            GeneraIcoFile(lnkbtnInformeMensualTab2.Text, imgbtnInformeMensualTab2);

            lblNomActualizaInformeMensualTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;


            imgbtnAgregarInformeMensualTab2.Visible = false;
            Panel_InformeMensualTab2.Visible = true;
            btnGuardarInformeMensualTab2.Visible = false;
            btnModificarInformeMensualTab2.Visible = true;

            Up_Tab2.Update();
        }


        protected void imgbtnAgregarInformeMensualTab2_Click(object sender, ImageClickEventArgs e)
        {
            Panel_InformeMensualTab2.Visible = true;
            imgbtnAgregarInformeMensualTab2.Visible = false;

            ddlFechaInformeMensualTab2.Enabled = true;
            btnGuardarInformeMensualTab2.Visible = true;
            btnModificarInformeMensualTab2.Visible = false;
        }
        protected void btnCancelarInformeMensualTab2_Click(object sender, EventArgs e)
        {
            Panel_InformeMensualTab2.Visible = false;
            imgbtnAgregarInformeMensualTab2.Visible = true;
        }
        protected void btnModificarInformeMensualTab2_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_InformeFinalTab2))
            {
                _BEEjecucion.id_item = Convert.ToInt32(lblID_InformeMensualTab2.Text);
                _BEEjecucion.Tipo = 1;

                _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionMensualTab2.Text);
                _BEEjecucion.estadoSituacional = txtDetalleEstadoSituacionalMensualTab2.Text;
                _BEEjecucion.observacion = txtObservacionMensualTab2.Text;

                if (FileUpload_InformeMensualTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUpload_InformeMensualTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = lnkbtnInformeMensualTab2.Text;
                }

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_InformeSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaInformes("2", grdInformeMensualTab2);

                    imgbtnAgregarInformeMensualTab2.Visible = true;
                    Panel_InformeMensualTab2.Visible = false;

                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected Boolean ValidarInformeMensualTab2()
        {
            Boolean result;
            result = true;

            if (ddlFechaInformeMensualTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingresar fecha de informe mensual.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaPresentacionMensualTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de presentación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;

        }
        protected void btnGuardarInformeMensualTab2_Click(object sender, EventArgs e)
        {
            if (ValidarInformeMensualTab2())
            {
                if (validaArchivo(FileUpload_InformeFinalTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.id_tipodocumento = "2";

                    _BEEjecucion.Date_fechaInicio = VerificaFecha(ddlFechaInformeMensualTab2.SelectedValue);
                    _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaPresentacionMensualTab2.Text);
                    _BEEjecucion.estadoSituacional = txtDetalleEstadoSituacionalMensualTab2.Text;
                    _BEEjecucion.observacion = txtObservacionMensualTab2.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUpload_InformeMensualTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_MON_InformeSupervision(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaInformes("2", grdInformeMensualTab2);

                        imgbtnAgregarInformeMensualTab2.Visible = true;
                        Panel_InformeMensualTab2.Visible = false;

                        Up_Tab2.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        protected void lnkbtnInformeMensualTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnInformeMensualTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void cargaFechaInformeSupervisicionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListProgramacion = new List<BE_MON_Ejecucion>();

            ListProgramacion = _objBLEjecucion.F_spMON_ProgramacionInforme(_BEEjecucion);

            ddlFechaInformeMensualTab2.DataSource = ListProgramacion;
            ddlFechaInformeMensualTab2.DataTextField = "nombre";
            ddlFechaInformeMensualTab2.DataValueField = "Date_fechaInicio";
            ddlFechaInformeMensualTab2.DataBind();

            ddlFechaInformeMensualTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void imgDocMensualTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdInformeMensualTab2.Rows[row.RowIndex].FindControl("imgDocMensualTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdInformeMensualTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdInformeMensualTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;

                _BEEjecucion.Date_fechaInforme = VerificaFecha("");
                _BEEjecucion.estadoSituacional = "";
                _BEEjecucion.observacion = "";

                _BEEjecucion.urlDoc = "";

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_InformeSupervision(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaInformes("2", grdInformeMensualTab2);

                    imgbtnAgregarInformeMensualTab2.Visible = true;
                    Panel_InformeMensualTab2.Visible = false;

                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        #endregion

        #region Tab3

        protected void CargaTab3()
        {
            cargaTipoReceptora();

            CargaFormatoTabCierre();
            CargaLiquidacion();

            //txtTotalTransferencia.Text = txtAcumuladorTranferenciaTab0.Text;


        }


        protected void CargaLiquidacion()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BELiquidacion.tipoFinanciamiento = 3;

            dt = _objBLLiquidacion.sp_MON_Liquidacion(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                // RECEPCION DE OBRA
                txtFechaCulminacionObraTab3.Text = dt.Rows[0]["fechaCulminacionObraCuadernoObra"].ToString();
                lnkbtnCulminacionTab3.Text = dt.Rows[0]["urlCuadernoObra"].ToString();
                txtFechaComunicacionTab3.Text = dt.Rows[0]["fechaComunicacionEntidad"].ToString();
                lnkbtnComunicacionTab3.Text = dt.Rows[0]["urlDocumentoComunicacionEntidad"].ToString();
                txtFechaDesignacionTab3.Text = dt.Rows[0]["fechaDesignacionComite"].ToString();
                lnkbtnDesignacionTab3.Text = dt.Rows[0]["urlDocumentoDesignacionComite"].ToString();
                string flagObservacion = dt.Rows[0]["flagObservacionRecepcion"].ToString();
                if (flagObservacion.Length > 0)
                {
                    rbObservacionTab3.SelectedValue = flagObservacion;
                }
                if (rbObservacionTab3.SelectedValue == "1")
                {
                    Panel_ObservacionesTab3.Visible = true;
                }
                txtFechaPliegoObservacionTab3.Text = dt.Rows[0]["fechaObservacionRecepcion"].ToString();
                lnkbtnPliegoObservacionTab3.Text = dt.Rows[0]["urlActaObservacionRecepcion"].ToString();
                txtFechaRecepcionFinalTab3.Text = dt.Rows[0]["fechaActaRecepcionFinal"].ToString();
                lnkbtnRecepcionFinalTab3.Text = dt.Rows[0]["urlActaRecepcionFinal"].ToString();

                GeneraIcoFile(lnkbtnCulminacionTab3.Text, imgbtnCulminacionTab3);
                GeneraIcoFile(lnkbtnComunicacionTab3.Text, imgbtnComunicacionTab3);
                GeneraIcoFile(lnkbtnDesignacionTab3.Text, imgbtnDesignacionTab3);
                GeneraIcoFile(lnkbtnPliegoObservacionTab3.Text, imgbtnPliegoObservacionTab3);
                GeneraIcoFile(lnkbtnRecepcionFinalTab3.Text, imgbtnRecepcionFinalTab3);

                lblNomActualizaRecepcionTab3.Text = "Actualizó: " + dt.Rows[0]["usuarioRecepcion"].ToString() + " - " + dt.Rows[0]["fecha_updateRecepcion"].ToString();

                //LIQUIDACION
                txtFechaPresentacionTab3.Text = dt.Rows[0]["fechaPresentacionLiquidacion"].ToString();
                txtFechaAprobacionTab3.Text = dt.Rows[0]["fechaAprobacionLiquidacion"].ToString();
                txtResolucionTab3.Text = dt.Rows[0]["resolucion"].ToString();
                txtFechaResolucionTab3.Text = dt.Rows[0]["fecha"].ToString();
                txtMontoLiquidacionTab3.Text = dt.Rows[0]["montoLiquidacion"].ToString();
                LnkbtnLiquidacionTab3.Text = dt.Rows[0]["urlLiquidacion"].ToString();
                GeneraIcoFile(LnkbtnLiquidacionTab3.Text, imgbtnLiquidacionTab3);
                txtSaldoEntidadTab3.Text = dt.Rows[0]["saldoFavorEntidad"].ToString();
                txtSaldoContratistaTab3.Text = dt.Rows[0]["saldoFavorEmpresa"].ToString();

                txtFechaPresentacionSupervisionTab2.Text = dt.Rows[0]["fechaPresentacionLiqSupervision"].ToString();
                txtFechaAprobacionLiqSupervisionTab3.Text = dt.Rows[0]["fechaAprobacionLiqSupervision"].ToString();
                txtResolucionLiqSupervisionTab3.Text = dt.Rows[0]["resolucionSupervision"].ToString();
                txtFechaResolucionLiqSupervisionTab3.Text = dt.Rows[0]["fechaLiqSupervision"].ToString();
                txtMontoLiqSupervisionTab3.Text = dt.Rows[0]["montoLiqSupervision"].ToString();
                lnkbtnLiqSupervisionTab3.Text = dt.Rows[0]["urlLiquidacionSupervision"].ToString();
                GeneraIcoFile(lnkbtnLiqSupervisionTab3.Text, imgbtnLiqSupervisionTab3);
                txtSaldoEntidadLiqSupervisionTab3.Text = dt.Rows[0]["saldoFavorEntidadSupervision"].ToString();
                txtSaldoSupervisorLiqSupervisionTab3.Text = dt.Rows[0]["saldoFavorEmpresaSupervision"].ToString();

                lblNomActualizaLiquidacionTab3.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                //SOSTENIBILIDAD
                ddlTipoReceptoraTab3.SelectedValue = dt.Rows[0]["id_tipoReceptora"].ToString();
                txtUnidadReceptoraTab3.Text = dt.Rows[0]["UnidadReceptora"].ToString();
                ddlflagCapacitadTab3.SelectedValue = dt.Rows[0]["flagCapacidadAdministrar"].ToString();
                ddlOperatividadTab3.SelectedValue = dt.Rows[0]["iFlagOperativo"].ToString();
                txtFechaActaTransferenciaTab3.Text = dt.Rows[0]["fechaFinalEntregaObraOperacion"].ToString();
                LnkbtnSostenibilidadTab3.Text = dt.Rows[0]["urlDocSostenibilidad"].ToString();
                GeneraIcoFile(LnkbtnSostenibilidadTab3.Text, imgbtnSostenibilidadTab3);
                txtComentarioTab3.Text = dt.Rows[0]["comentarioSostenibilidad"].ToString();


                // CIERRE

                ddlFormatoCierreTabCierre.SelectedValue = dt.Rows[0]["iTipoFormatoCierre"].ToString();
                txtFechaFormato14TabCierre.Text = dt.Rows[0]["fechaFormatoSNIP14"].ToString();
                lnkbtnFormatoCierreTabCierre.Text = dt.Rows[0]["urlFormatoCierre"].ToString();
                GeneraIcoFile(lnkbtnFormatoCierreTabCierre.Text, imgbtnFormatoCierreTabCierre);
                lblNomActualizaCierreTab3.Text = "Actualizó: " + dt.Rows[0]["usuarioCierre"].ToString() + " - " + dt.Rows[0]["fechaUpdateCierre"].ToString();
            }

        }

        protected void cargaTipoReceptora()
        {
            ddlTipoReceptoraTab3.DataSource = _objBLLiquidacion.F_spMON_TipoReceptora();
            ddlTipoReceptoraTab3.DataTextField = "nombre";
            ddlTipoReceptoraTab3.DataValueField = "valor";
            ddlTipoReceptoraTab3.DataBind();

            ddlTipoReceptoraTab3.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
              
        protected void LnkbtnLiquidacion_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnLiquidacionTab3_OnClik(object sender, EventArgs e)
        {

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }



        protected void imgbtnLiqSupervisionTab3_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnLiqSupervisionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnLiqSupervisionTab3.Text);
                Response.End();
            }
        }

        protected void lnkbtnCulminacionTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnCulminacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnCulminacionTab3.Text);
                Response.End();
            }
        }
        protected void lnkbtnComunicacionTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnComunicacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnComunicacionTab3.Text);
                Response.End();
            }
        }
        protected void lnkbtnDesignacionTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnDesignacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnDesignacionTab3.Text);
                Response.End();
            }
        }
        protected void lnkbtnRecepcionFinalTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnRecepcionFinalTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnRecepcionFinalTab3.Text);
                Response.End();
            }
        }

        protected Boolean ValidarRecepcionTab3()
        {
            Boolean result;
            result = true;

            if (rbObservacionTab3.SelectedValue == null | rbObservacionTab3.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar si hubo observación o no.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaRecepcionFinalTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaRecepcionFinalTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de acta de recepción final.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaRecepcionFinalTab3.Text)>DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de acta de recepción final no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaTerminoReal.Text.Length > 0 && Convert.ToDateTime(txtFechaRecepcionFinalTab3.Text) < Convert.ToDateTime(txtFechaTerminoReal.Text))
                {
                    string script = "<script>alert('La fecha de acta de recepción final debe ser mayor o igual a la fecha de termino real.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaResolucionTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaRecepcionFinalTab3.Text) > Convert.ToDateTime(txtFechaResolucionTab3.Text))
                {
                    string script = "<script>alert('La fecha de acta de recepción final debe ser menor o igual a la fecha de resolución de liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

                
            return result;

        }
        protected void btnGuardarRecepcionTab3_Click(object sender, EventArgs e)
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BELiquidacion.tipoFinanciamiento = 3;

            if (validaArchivo(FileUpload_CulminacionTab3))
            {
                if (validaArchivo(FileUpload_ComunicacionTab3))
                {
                    if (validaArchivo(FileUpload_DesignacionTab3))
                    {
                        if (ValidarRecepcionTab3())
                        {
                            if (validaArchivo(FileUpload_PliegoObservacionTab3))
                            {
                                if (validaArchivo(FileUpload_RecepcionFinalTab3))
                                {
                                    _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                                    _BELiquidacion.tipoFinanciamiento = 3;

                                    _BELiquidacion.Date_fechaCumplimiento = VerificaFecha(txtFechaCulminacionObraTab3.Text);
                                    _BELiquidacion.Date_fechaDocumentacion = VerificaFecha(txtFechaComunicacionTab3.Text);
                                    _BELiquidacion.Date_fechaSuscripcion = VerificaFecha(txtFechaDesignacionTab3.Text);
                                    _BELiquidacion.Date_fechaFinal = VerificaFecha(txtFechaRecepcionFinalTab3.Text);
                                    _BELiquidacion.flagCapacidad = rbObservacionTab3.SelectedValue;
                                    _BELiquidacion.Date_fechaRecepcion = VerificaFecha(txtFechaPliegoObservacionTab3.Text);

                                    if (FileUpload_CulminacionTab3.HasFile)
                                    {
                                        _BELiquidacion.urlDocCuadernoObra = _Metodo.uploadfile(FileUpload_CulminacionTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlDocCuadernoObra = lnkbtnCulminacionTab3.Text; }

                                    if (FileUpload_ComunicacionTab3.HasFile)
                                    {
                                        _BELiquidacion.urlDocComunicacion = _Metodo.uploadfile(FileUpload_ComunicacionTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlDocComunicacion = lnkbtnComunicacionTab3.Text; }

                                    if (FileUpload_DesignacionTab3.HasFile)
                                    {
                                        _BELiquidacion.urlDocDesignacion = _Metodo.uploadfile(FileUpload_DesignacionTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlDocDesignacion = lnkbtnDesignacionTab3.Text; }

                                    if (FileUpload_PliegoObservacionTab3.HasFile)
                                    {
                                        _BELiquidacion.urlDocDevolucion = _Metodo.uploadfile(FileUpload_PliegoObservacionTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlDocDevolucion = lnkbtnPliegoObservacionTab3.Text; }

                                    if (FileUpload_RecepcionFinalTab3.HasFile)
                                    {
                                        _BELiquidacion.urlActaCierre = _Metodo.uploadfile(FileUpload_RecepcionFinalTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlActaCierre = lnkbtnRecepcionFinalTab3.Text; }

                                    _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                                    int val = _objBLLiquidacion.IU_MON_RecepcionObra_PorContrata(_BELiquidacion);


                                    if (val == 1)
                                    {
                                        string script = "<script>alert('Se registró correctamente.');</script>";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                        CargaLiquidacion();

                                    }
                                    else
                                    {
                                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                    }


                                    Up_Tab3.Update();

                                }
                            }
                        }
                    }
                }
            }

        }
        protected void lnkbtnPliegoObservacionTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnPliegoObservacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnPliegoObservacionTab3.Text);
                Response.End();
            }
        }


        protected void btnGuardarSostenibilidadTab3_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadSostenibilidadTab3))
            {
                string msjVal = ValidarSostenibilidadTab3();
                if (msjVal.Length > 0)
                {
                    string script = "<script>alert('" + msjVal + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BELiquidacion.tipoFinanciamiento = 3;

                    _BELiquidacion.tipoReceptora = Convert.ToInt32(ddlTipoReceptoraTab3.SelectedValue);
                    _BELiquidacion.unidadReceptora = txtUnidadReceptoraTab3.Text;
                    _BELiquidacion.flagCapacidad = ddlflagCapacitadTab3.SelectedValue;
                    _BELiquidacion.flagOperatividad = ddlOperatividadTab3.SelectedValue;

                    _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaActaTransferenciaTab3.Text);
                    if (FileUploadSostenibilidadTab3.HasFile)
                    {
                        _BELiquidacion.urlDocSostenibilidad = _Metodo.uploadfile(FileUploadSostenibilidadTab3);
                    }
                    else
                    { _BELiquidacion.urlDocSostenibilidad = LnkbtnSostenibilidadTab3.Text; }
                    _BELiquidacion.comentarioCapacidad = txtComentarioTab3.Text;

                    _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLLiquidacion.IU_MON_SostenibilidadPorContrata(_BELiquidacion);


                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaLiquidacion();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                    Up_Tab3.Update();
                }
            }
        }

        protected String ValidarSostenibilidadTab3()
        {
            string msj = "";


            if (ddlTipoReceptoraTab3.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de receptora.\\n";
            }

            if (txtUnidadReceptoraTab3.Text.Count()<2)
            {
                msj = msj + "*Ingresar nombre de entidad receptora.\\n";
            }
            if (ddlflagCapacitadTab3.SelectedValue == "")
            {
                msj = msj + "*Responder si la entidad receptora cuenta con capacidad para administrar el proyecto.\\n";
            }

            if (ddlOperatividadTab3.SelectedValue == "")
            {
                msj = msj + "*Responder si se encuentra operativa.\\n";
            }
            if (txtFechaActaTransferenciaTab3.Text == "")
            {
                msj = msj + "*Ingresar fecha acta transferencia fisica de la obra.\\n";
            }

            return msj;

        }
        protected Boolean ValidarLiquidacionTab3()
        {
            Boolean result;
            result = true;

            if (txtFechaResolucionTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaResolucionTab3.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de resolución de liquidación no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaResolucionTab3.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de resolución de liquidación no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaRecepcionFinalTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaResolucionTab3.Text) < Convert.ToDateTime(txtFechaRecepcionFinalTab3.Text))
                {
                    string script = "<script>alert('La fecha de resolución de liquidación debe ser mayor o igual a la fecha de acta de recepción final.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            //if (txtMontoLiquidacionTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar monto de liquidación de obra.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //if (txtSaldoEntidadTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar saldo a favor de la entidad en la liquidación de obra.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //if (txtSaldoContratistaTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar saldo a favor del consorcio o contratista en la liquidación de obra.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            if (txtResolucionLiqSupervisionTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtResolucionLiqSupervisionTab3.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de resolución de liquidación no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtResolucionLiqSupervisionTab3.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de resolución de liquidación no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

            }
            //if (txtMontoLiqSupervisionTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar monto de liquidación de supervision.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //if (txtSaldoEntidadLiqSupervisionTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar saldo a favor de la entidad en la liquidación de la supervision.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //if (txtSaldoSupervisorLiqSupervisionTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar saldo a favor del supervisor.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            return result;

        }
        protected void btnGuardarLiquidacionTab3_Click(object sender, EventArgs e)
        {
            if (ValidarLiquidacionTab3())
            {
                if (validaArchivo(FileUploadLiquidacionTab3))
                {
                    if (validaArchivo(FileUpload_LiqSupervisionTab3))
                    {
                        _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BELiquidacion.tipoFinanciamiento = 3;

                        _BELiquidacion.Date_fechaPresentacion = VerificaFecha(txtFechaPresentacionTab3.Text);
                        _BELiquidacion.Date_fechaAprobacion = VerificaFecha(txtFechaAprobacionTab3.Text);
                        _BELiquidacion.resolucion = txtResolucionTab3.Text;
                        _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaResolucionTab3.Text);
                        _BELiquidacion.monto = txtMontoLiquidacionTab3.Text;
                        _BELiquidacion.saldoEntidad = txtSaldoEntidadTab3.Text;
                        _BELiquidacion.saldoEmpresa = txtSaldoContratistaTab3.Text;

                        _BELiquidacion.Date_fechaPresentacionSupervision = VerificaFecha(txtFechaPresentacionSupervisionTab2.Text);
                        _BELiquidacion.Date_fechaAprobacionSupervision = VerificaFecha(txtFechaAprobacionLiqSupervisionTab3.Text);
                        _BELiquidacion.ResolucionSupervision = txtResolucionLiqSupervisionTab3.Text;
                        _BELiquidacion.Date_fechaLiqSupervision = VerificaFecha(txtFechaResolucionLiqSupervisionTab3.Text);
                        _BELiquidacion.MontoLiqSupervision = txtMontoLiqSupervisionTab3.Text;
                        _BELiquidacion.saldoEntidadSupervision = txtSaldoEntidadLiqSupervisionTab3.Text;
                        _BELiquidacion.saldoEmpresaSupervision = txtSaldoSupervisorLiqSupervisionTab3.Text;

                        if (FileUploadLiquidacionTab3.HasFile)
                        {
                            _BELiquidacion.urlLiquidacion = _Metodo.uploadfile(FileUploadLiquidacionTab3);
                        }
                        else
                        { _BELiquidacion.urlLiquidacion = LnkbtnLiquidacionTab3.Text; }

                        if (FileUpload_LiqSupervisionTab3.HasFile)
                        {
                            _BELiquidacion.UrlLiquidacionSupervision = _Metodo.uploadfile(FileUpload_LiqSupervisionTab3);
                        }
                        else
                        { _BELiquidacion.UrlLiquidacionSupervision = lnkbtnLiqSupervisionTab3.Text; }

                        _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        int val = _objBLLiquidacion.IU_MON_LiquidacionPorContrata(_BELiquidacion);

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                            CargaLiquidacion();
                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        }

                        Up_Tab3.Update();
                    }
                }
            }
        }
        #endregion

        #region Tab4
        protected void CargaTab4()
        {
            //CargaTipoCertificacion(ddlEiaTab4);
            //CargaTipoCertificacion(ddlLegalTab4);
            //CargaTipoCertificacion(ddlFuenteTab4);
            //CargaTipoCertificacion(ddlResidualesTab4);
            //CargaTipoCertificacion(ddlInfobrasTab4);
            //CargaTipoCertificacion(ddlPruebasTab4);

            cargaCertificacionTab4();
            cargaParalizacionTab4();
            cargaPlazoTAb4();
            cargaPersupuestoTab4();
            cargaDeductivoTab4();
            cargaAdendasTab4();


            //cargaEstadoPruebaTab4();
            //cargaResultadoFinalTab4();
            //cargarCertificacion();

        }

        protected void CargaTipoCertificacion(DropDownList ddl)
        {
            ddl.DataSource = _objBLEjecucion.F_spMON_TipoCertificacion();
            ddl.DataValueField = "valor";
            ddl.DataTextField = "nombre";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));


        }

        protected void btnGuardarCertificacion_OnClick(object sender, EventArgs e)
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            //_BEEjecucion.Eia = ddlEiaTab4.SelectedValue;
            //_BEEjecucion.Fuente = ddlFuenteTab4.SelectedValue;
            //_BEEjecucion.Legal = ddlLegalTab4.SelectedValue;
            //_BEEjecucion.Residuales = ddlResidualesTab4.SelectedValue;
            //_BEEjecucion.Infobras = ddlInfobrasTab4.SelectedValue;
            //_BEEjecucion.pruebas = ddlPruebasTab4.SelectedValue;
            //_BEEjecucion.CodigoInfobra = txtCodigoInfobras.Text;

            int val = _objBLEjecucion.spi_MON_CertificacionAutorizacion(_BEEjecucion);

            if (val == 1)
            {
                string script = "<script>alert('Registro correcto.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                cargarCertificacion();
                Up_Tab3.Update();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void cargarCertificacion()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            dt = _objBLEjecucion.spMON_CertificacionAutorizacion(_BEEjecucion);
            if (dt.Rows.Count > 0)
            {

                //ddlEiaTab4.SelectedValue = dt.Rows[0]["eia"].ToString();
                //ddlLegalTab4.SelectedValue = dt.Rows[0]["legal"].ToString();
                //ddlFuenteTab4.SelectedValue = dt.Rows[0]["fuente"].ToString();
                //ddlResidualesTab4.SelectedValue = dt.Rows[0]["residuales"].ToString();
                //ddlInfobrasTab4.SelectedValue = dt.Rows[0]["infobras"].ToString();
                //ddlPruebasTab4.SelectedValue = dt.Rows[0]["pruebas"].ToString();
                //txtCodigoInfobras.Text = dt.Rows[0]["codigoInfobras"].ToString();
                //lblNomUsuarioCertificacion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                //if (txtCodigoInfobras.Text == "")
                //{
                //    lblmsjInfoobras.Visible = true;

                //}
                //else { lblmsjInfoobras.Visible = false; }
            }



        }
        protected void cargaPlazoTAb4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 1;

            lblTotalDias.Text = "0";
            grdPlazoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPlazoTab4.DataBind();

        }

        protected void grdPlazoTab4_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPlaTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(lblTotalDias.Text) + Convert.ToInt32(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                lblTotalDias.Text = TotalDias.ToString();
            }


        }

        protected void cargaPersupuestoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 2;

            DataTable dt = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPresupuestoTab4.DataSource = dt;
            grdPresupuestoTab4.DataBind();

            string MontoTotal = (dt.Compute("Sum(montoPresup)", "").ToString());

            if (dt.Rows.Count > 0)
            {
                lblPresupuestoAdicional.Text = Convert.ToDecimal(MontoTotal).ToString("N");
                hfTotalAdicional.Value = MontoTotal;
            }
            else
            {
                lblPresupuestoAdicional.Text = "0.00";
            }
        }

        protected void btnModificarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPlazoTab4())
            {
                if (validaArchivo(FileUploadPlaTab4))
                {
                    _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Ampliacion_Plazos.Text);
                    //_BEFinanciamiento.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 1;
                    _BEAmpliacion.Date_fechaCuadernoObra = VerificaFecha(txtFechaCuadernoObraPlaTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                    _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioPlaTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinPlaTab4.Text);
                    _BEAmpliacion.Date_fechaSolicitudContratista = VerificaFecha(txtFechaSolicitudContratistaPlaTab4.Text);
                    _BEAmpliacion.Date_fechaPresentacionInforme = VerificaFecha(txtFechaPresentacionInformePlaTab4.Text);
                    _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);

                    _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;

                    if (FileUploadPlaTab4.PostedFile.ContentLength > 0)
                    {
                        _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                    }
                    else
                    {
                        _BEAmpliacion.urlDoc = LnkbtnPlaTab4.Text;
                    }

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int resul;
                    resul = _objBLAmpliacion.U_MON_AmpliacionPlazoPorContrata(_BEAmpliacion);
                    if (resul == 1)
                    {
                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 5;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        //DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion + ampliacion;

                        if (lblCOD_SUBSECTOR.Text == "3")
                        {
                            GeneraProgramacionPNSR(iPlazo, dtFechaInicioProgramacion);
                        }
                        else
                        {
                            GeneraProgramacionPNSU(iPlazo, dtFechaInicioProgramacion);
                        }


                        string script = "<script>alert('Modificación correcta.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        txtAmpPlazoPlaTab4.Text = "";
                        txtResolAproPlaTab4.Text = "";
                        txtFechaResolPlaTab4.Text = "";
                        txtConceptoPlaTab4.Text = "";
                        txtObservacionPlaTab4.Text = "";
                        txtFechaInicioPlaTab4.Text = "";
                        txtFechaFinPlaTab4.Text = "";
                        LnkbtnPlaTab4.Text = "";
                        imgbtnPlaTab4.Visible = false;
                        imgAgregaPlazoTab4.Visible = true;

                        lblTotalDias.Text = "0";
                        cargaPlazoTAb4();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                    Panel_PlazoTab4.Visible = false;
                    ActualizaFechasTab1();
                    btnModificarPlazoTab4.Visible = true;
                    Up_Tab4.Update();
                }
            }
        }

        protected void btnModificarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPresupuestoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Adicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;
                _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                _BEAmpliacion.observacion = txtObsPreTab4.Text;



                if (FileUploadPreTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPreTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtMontoPreTab4.Text = "";
                    txtVincuPreTab4.Text = "";
                    txtResolAproPreTab4.Text = "";
                    txtFechaPreTab4.Text = "";
                    txtIncidenciaPreTab4.Text = "";
                    LnkbtnPreTab4.Text = "";
                    txtConceptoPreTab4.Text = "";
                    txtObsPreTab4.Text = "";
                    imgbtnPreTab4.Visible = false;
                    imbtnAgregarPresupuestoTab4.Visible = true;

                    cargaPersupuestoTab4();

                    // Actualizar Avance Fisico
                    cargaProgramacionTab2();
                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PresupuestoTab4.Visible = false;
                cargaPersupuestoTab4();
                btnModificarPresupuestoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void btnModificarDeductivoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarDeductivoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Deductivo.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                _BEAmpliacion.vinculacion = rbtnVinculacionDedTab4.SelectedValue;


                _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                _BEAmpliacion.observacion = txtObsDedTab4.Text;

                if (FileUploadDedTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnDedTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Se modificó  correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtMontoDedTab4.Text = "";

                    txtResolAproDedTab4.Text = "";
                    txtFechaDedTab4.Text = "";
                    txtIncidenciaDedTab4.Text = "";

                    txtConceptoDedTab4.Text = "";
                    txtObsDedTab4.Text = "";
                    LnkbtnDedTab4.Text = "";
                    imgbtnDedTab4.Visible = false;
                    imgbtnDeductivoTab4.Visible = true;
                    cargaDeductivoTab4();

                    // Actualizar Avance Fisico
                    cargaProgramacionTab2();
                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_deductivoTab4.Visible = false;
                cargaDeductivoTab4();
                btnModificarDeductivoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        //protected void cargaEstadoPruebaTab4()
        //{
        //    ddlEstadoTab4.DataSource = _objBLCalidad.F_spMON_TipoEstadoPrueba();
        //    ddlEstadoTab4.DataValueField = "valor";
        //    ddlEstadoTab4.DataTextField = "nombre";
        //    ddlEstadoTab4.DataBind();

        //    ddlEstadoTab4.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}

        //protected void cargaResultadoFinalTab4()
        //{
        //    ddlResultadoFinalTab4.DataSource = _objBLCalidad.F_spMON_TipoResultadoFinal();
        //    ddlResultadoFinalTab4.DataValueField = "valor";
        //    ddlResultadoFinalTab4.DataTextField = "nombre";
        //    ddlResultadoFinalTab4.DataBind();

        //    ddlResultadoFinalTab4.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}

        protected void grdPresupuestoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Double TotalDias;
            //TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPreTab4");
                //Label dias = (Label)e.Row.FindControl("lblPresupuesto");

                //TotalDias = Convert.ToDouble(lblPresupuestoAdicional.Text) + Convert.ToDouble(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                //lblPresupuestoAdicional.Text = TotalDias.ToString("N");
            }


        }
        protected void cargaDeductivoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 3;

            DataTable dt = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdDeductivoTab4.DataSource = dt;
            grdDeductivoTab4.DataBind();

            string MontoTotal = (dt.Compute("Sum(montoPresup)", "").ToString());

            if (dt.Rows.Count > 0)
            {
                lblDeductivoTotal.Text = Convert.ToDecimal(MontoTotal).ToString("N");
                hfTotalDeductivo.Value = MontoTotal;
            }
            else
            {
                lblDeductivoTotal.Text = "0.00";
            }

        }

        protected void cargaAdendasTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 4;

            grdAdendasTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdAdendasTab4.DataBind();
        }
        protected void grdDeductivoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Double TotalDias;
            //TotalDias = 0;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocDedTab4");
                //Label dias = (Label)e.Row.FindControl("lbldeductivo");

                //TotalDias = Convert.ToDouble(lblDeductivoTotal.Text) + Convert.ToDouble(dias.Text);
                GeneraIcoFile(imb.ToolTip, imb);
                //lblDeductivoTotal.Text = TotalDias.ToString("N");
            }



        }

        protected void imgAgregaPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            btnGuardarPlazoTab4.Visible = true;
            btnModificarPlazoTab4.Visible = false;
            lblNomUsuarioAmpDias.Text = "";
            txtAmpPlazoPlaTab4.Enabled = true;
            Up_Tab4.Update();
        }

        protected void btnCancelarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = false;
            imgAgregaPlazoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPlazoTab4()
        {
            Boolean result;
            result = true;

            if (txtAmpPlazoPlaTab4.Text == "")
            {
                string script = "<script>alert('Ingrese dias de Ampliación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtResolAproPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaResolPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPlazoTab4())
            {

                if (validaArchivo(FileUploadPlaTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 1;
                    _BEAmpliacion.Date_fechaCuadernoObra = VerificaFecha(txtFechaCuadernoObraPlaTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                    _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioPlaTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinPlaTab4.Text);
                    _BEAmpliacion.Date_fechaSolicitudContratista = VerificaFecha(txtFechaSolicitudContratistaPlaTab4.Text);
                    _BEAmpliacion.Date_fechaPresentacionInforme = VerificaFecha(txtFechaPresentacionInformePlaTab4.Text);
                    _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);

                    _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLAmpliacion.I_MON_AmpliacionPlazoPorContrata(_BEAmpliacion);

                    if (val == 1)
                    {
                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 5;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion - (dtFechaInicio - dtFechaInicioProgramacion).Days + ampliacion;

                        if (lblCOD_SUBSECTOR.Text == "3")
                        {
                            GeneraProgramacionPNSR(iPlazo, dtFechaInicio);
                        }
                        else
                        {
                            GeneraProgramacionPNSU(iPlazo, dtFechaInicio);
                        }

                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        lblTotalDias.Text = "0";
                        cargaPlazoTAb4();
                        ActualizaFechasTab1();
                        txtAmpPlazoPlaTab4.Text = "";
                        txtResolAproPlaTab4.Text = "";
                        txtFechaResolPlaTab4.Text = "";
                        txtConceptoPlaTab4.Text = "";
                        txtObservacionPlaTab4.Text = "";
                        txtFechaInicioPlaTab4.Text = "";
                        txtFechaFinPlaTab4.Text = "";

                        Panel_PlazoTab4.Visible = false;
                        imgAgregaPlazoTab4.Visible = true;

                        Up_Tab4.Update();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();


                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPlaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPlazoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPlazoTab4.Rows[row.RowIndex].FindControl("imgDocPlaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imbtnAgregarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            lblNomUsuarioPresupuesto.Text = "";
            btnGuardarPresupuestoTab4.Visible = true;
            btnModificarPresupuestoTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void btnCancelarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPresupuestoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPresupuestoTab4())
            {

                if (validaArchivo(FileUploadPreTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 2;

                    _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;

                    _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                    _BEAmpliacion.observacion = txtObsPreTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha("");
                    _BEAmpliacion.Date_FechaFin = VerificaFecha("");


                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaPersupuestoTab4();

                        txtMontoPreTab4.Text = "";
                        txtVincuPreTab4.Text = "";
                        txtResolAproPreTab4.Text = "";
                        txtFechaPreTab4.Text = "";
                        txtIncidenciaPreTab4.Text = "";

                        txtConceptoPreTab4.Text = "";
                        txtObsPreTab4.Text = "";

                        Panel_PresupuestoTab4.Visible = false;
                        imbtnAgregarPresupuestoTab4.Visible = true;

                        Up_Tab4.Update();

                        // Actualizar Avance Fisico
                        cargaProgramacionTab2();
                        CargaAvanceFisico();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPreTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPresupuestoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPresupuestoTab4.Rows[row.RowIndex].FindControl("imgDocPreTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            btnGuardarDeductivoTab4.Visible = true;
            btnModificarDeductivoTab4.Visible = false;
            lblNomUsuarioDeductivo.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarDeductivoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (rbtnVinculacionDedTab4.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Vinculación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarDeductivoTab4())
            {

                if (validaArchivo(FileUploadDedTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 3;

                    _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                    //_BEAmpliacion.vinculacion = txtVincuDedTab4.Text;

                    _BEAmpliacion.vinculacion = rbtnVinculacionDedTab4.SelectedValue;
                    _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                    _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                    _BEAmpliacion.observacion = txtObsDedTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha("");
                    _BEAmpliacion.Date_FechaFin = VerificaFecha("");

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaDeductivoTab4();

                        txtMontoDedTab4.Text = "";
                        //txtVincuDedTab4.Text = "";
                        txtResolAproDedTab4.Text = "";
                        txtFechaDedTab4.Text = "";
                        txtIncidenciaDedTab4.Text = "";

                        txtConceptoDedTab4.Text = "";
                        txtObsDedTab4.Text = "";

                        Panel_deductivoTab4.Visible = false;
                        imgbtnDeductivoTab4.Visible = true;

                        Up_Tab4.Update();

                        // Actualizar Avance Fisico
                        cargaProgramacionTab2();
                        CargaAvanceFisico();
                        
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocDedTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdDeductivoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdDeductivoTab4.Rows[row.RowIndex].FindControl("imgDocDedTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnAgregarFianzasTab4_OnClick(object sender, EventArgs e)
        {
            Panel_fianzas.Visible = true;
            imgbtnAgregarFianzasTab4.Visible = false;
            btnGuardarFianzasTab2.Visible = true;
            lblNomUsuarioCarta.Text = "";
            btnModificarFianzasTab2.Visible = false;
            imgbtnCartaTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnAccionTab2.Text = "";

            lblTitDocAccion.Text = "";
            FileUploadAccionTab2.Visible = false;
            imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnAccionTab2.Text = "";
            

            Up_Tab2.Update();
        }

        protected void imgbtnPlaTabb4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPlaTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnPreTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPreTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnDedTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnDedTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdPresupuestoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPresupuestoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaPersupuestoTab4();
                    Up_Tab4.Update();

                    // Actualizar Avance Fisico
                    cargaProgramacionTab2();
                    CargaAvanceFisico();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdDeductivoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdDeductivoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDeductivoTab4();
                    Up_Tab4.Update();

                    // Actualizar Avance Fisico
                    cargaProgramacionTab2();
                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPlazoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPlazoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Ampliacion_Plazos.Text = id_ampliacion.ToString();
            GridViewRow row = grdPlazoTab4.SelectedRow;

            txtFechaCuadernoObraPlaTab4.Text = ((Label)row.FindControl("lblFechaCuaderno")).Text;
            txtAmpPlazoPlaTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            //txtAmpPlazoPlaTab4.Enabled = false;
            txtResolAproPlaTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolPlaTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoPlaTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionPlaTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            LnkbtnPlaTab4.Text = ((ImageButton)row.FindControl("imgDocPlaTab4")).ToolTip;
            lblNomUsuarioAmpDias.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicioPlaTab4.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFinPlaTab4.Text = ((Label)row.FindControl("lblfechaFin")).Text;
            txtFechaSolicitudContratistaPlaTab4.Text = ((Label)row.FindControl("lblFechaSolicitud")).Text;
            txtFechaPresentacionInformePlaTab4.Text = ((Label)row.FindControl("lblFechaPresentacion")).Text;

            GeneraIcoFile(LnkbtnPlaTab4.Text, imgbtnPlaTab4);

            btnGuardarPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            Panel_PlazoTab4.Visible = true;
            Up_Tab4.Update();


        }

        protected void grdPresupuestoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPresupuestoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Adicional.Text = id_ampliacion.ToString();
            GridViewRow row = grdPresupuestoTab4.SelectedRow;

            txtMontoPreTab4.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtVincuPreTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproPreTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaPreTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaPreTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;
            txtConceptoPreTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsPreTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioPresupuesto.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            LnkbtnPreTab4.Text = ((ImageButton)row.FindControl("imgDocPreTab4")).ToolTip;

            GeneraIcoFile(LnkbtnPreTab4.Text, imgbtnPreTab4);


            btnGuardarPresupuestoTab4.Visible = false;
            btnModificarPresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            Panel_PresupuestoTab4.Visible = true;

            Up_Tab4.Update();


        }

        protected void grdDeductivoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdDeductivoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Deductivo.Text = id_ampliacion.ToString();
            GridViewRow row = grdDeductivoTab4.SelectedRow;

            txtMontoDedTab4.Text = Server.HtmlDecode(row.Cells[2].Text);
            rbtnVinculacionDedTab4.SelectedValue = ((Label)row.FindControl("lblVinculacion")).Text;

            txtResolAproDedTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaDedTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaDedTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;

            txtConceptoDedTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsDedTab4.Text = ((Label)row.FindControl("lblObservacionesDe")).Text;

            lblNomUsuarioDeductivo.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            LnkbtnDedTab4.Text = ((ImageButton)row.FindControl("imgDocDedTab4")).ToolTip;

            GeneraIcoFile(LnkbtnDedTab4.Text, imgbtnDedTab4);


            btnGuardarDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            Panel_deductivoTab4.Visible = true;

            Up_Tab4.Update();


        }

        protected void grdAdendasTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAdendasTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                _BEAmpliacion.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEAmpliacion.fechaAdenda = "01/01/1990";
                _BEAmpliacion.fechaResolucion = "01/01/1990";
                _BEAmpliacion.concepto = "";
                _BEAmpliacion.resolAprob = "";
                _BEAmpliacion.observacion = "";
                _BEAmpliacion.urlDoc = "";

                int val = _objBLAmpliacion.UD_MON_Adendas(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaAdendasTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdAdendasTab4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdAdendasTab4.SelectedDataKey.Value.ToString();
            lblIDAdendasTab4.Text = id_ampliacion.ToString();
            GridViewRow row = grdAdendasTab4.SelectedRow;

            txtFechaAdendaTab4.Text = ((Label)row.FindControl("lblFechaAdenda")).Text;

            txtResolucionAdendaTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaResolucionAdendaTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;

            txtConceptoAdeTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionAdendaTab4.Text = ((Label)row.FindControl("lblObservacionesAde")).Text;

            lblNomActualizaAdendaTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            lnkbtnDocAdendaTab4.Text = ((ImageButton)row.FindControl("imgDocAdeTab4")).ToolTip;

            GeneraIcoFile(lnkbtnDocAdendaTab4.Text, imgbtnDocAdendaTab4);

            btnGuardarAdendaTab4.Visible = false;
            btnModificarAdendaTab4.Visible = true;
            imgbtnAdendaTab4.Visible = false;
            Panel_AdendaTab4.Visible = true;

            Up_Tab4.Update();
        }
        protected void grdAdendasTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAdeTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void imgbtnAdendaTab4_Click(object sender, ImageClickEventArgs e)
        {
            Panel_AdendaTab4.Visible = true;
            imgbtnAdendaTab4.Visible = false;
            btnGuardarAdendaTab4.Visible = true;
            btnModificarAdendaTab4.Visible = false;
        }
        protected void lnkbtnDocAdendaTab4_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = lnkbtnDocAdendaTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected Boolean ValidarAdendaTab4()
        {
            Boolean result;
            result = true;

            if (txtConceptoAdeTab4.Text == "")
            {
                string script = "<script>alert('Ingresar concepto de adenda.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtFechaAdendaTab4.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de adenda.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtResolucionAdendaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese resolución de adenda.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }
        protected void btnGuardarAdendaTab4_Click(object sender, EventArgs e)
        {
            if (ValidarAdendaTab4())
            {

                if (validaArchivo(FileUpload_ArchivoAdendaTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 4; //ADENDAS

                    _BEAmpliacion.concepto = txtConceptoAdeTab4.Text;
                    _BEAmpliacion.Date_fechaAdenda = VerificaFecha(txtFechaAdendaTab4.Text);

                    _BEAmpliacion.resolAprob = txtResolucionAdendaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionAdendaTab4.Text);

                    _BEAmpliacion.observacion = txtObservacionAdendaTab4.Text;

                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ArchivoAdendaTab4);

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int val = _objBLAmpliacion.I_MON_Adendas(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaAdendasTab4();

                        Panel_AdendaTab4.Visible = false;
                        imgbtnAdendaTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }
        protected void btnModificarAdendaTab4_Click(object sender, EventArgs e)
        {
            if (ValidarAdendaTab4())
            {

                if (validaArchivo(FileUpload_ArchivoAdendaTab4))
                {

                    _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblIDAdendasTab4.Text);

                    _BEAmpliacion.concepto = txtConceptoAdeTab4.Text;
                    _BEAmpliacion.Date_fechaAdenda = VerificaFecha(txtFechaAdendaTab4.Text);

                    _BEAmpliacion.resolAprob = txtResolucionAdendaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionAdendaTab4.Text);

                    _BEAmpliacion.observacion = txtObservacionAdendaTab4.Text;


                    if (FileUpload_ArchivoAdendaTab4.PostedFile.ContentLength > 0)
                    {
                        _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ArchivoAdendaTab4);
                    }
                    else
                    {
                        _BEAmpliacion.urlDoc = lnkbtnDocAdendaTab4.Text;
                    }

                    _BEAmpliacion.tipo = 1;
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int val = _objBLAmpliacion.UD_MON_Adendas(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se modificó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaAdendasTab4();

                        //txtMontoDedTab4.Text = "";
                        ////txtVincuDedTab4.Text = "";
                        //txtResolAproDedTab4.Text = "";
                        //txtFechaDedTab4.Text = "";
                        //txtIncidenciaDedTab4.Text = "";

                        //txtConceptoDedTab4.Text = "";
                        //txtObsDedTab4.Text = "";

                        Panel_AdendaTab4.Visible = false;
                        imgbtnAdendaTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }
        protected void btnCancelarAdendaTab4_Click(object sender, EventArgs e)
        {
            Panel_AdendaTab4.Visible = false;
            imgbtnAdendaTab4.Visible = true;
        }
        protected void imgDocAdeTab4_Click1(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //int id_ampliacion = Convert.ToInt32(grdAdendasTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdAdendasTab4.Rows[row.RowIndex].FindControl("imgDocAdeTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void cargaCertificacionTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;

            grdCertificacionTab4.DataSource = _objBLAmpliacion.F_MON_CertificacionAutorizacion_PorContrata(_BEAmpliacion);
            grdCertificacionTab4.DataBind();
        }
        protected void imgbtnAgregarCertificacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            Panel_CertificacionTab4.Visible = true;
            imgbtnAgregarCertificacionTab4.Visible = false;

            btnGuardarCertificacionTab4.Visible = true;
            btnModificarCertificacionTab4.Visible = false;
        }

        protected void btnCancelarCertificacionTab4_OnClick(object sender, EventArgs e)
        {
            Panel_CertificacionTab4.Visible = false;
            imgbtnAgregarCertificacionTab4.Visible = true;
            Up_Tab4.Update();
        }
        protected void btnModificarCertificacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarCertificacionTab4())
            {
                if (validaArchivo(FileUpload_CertificacionTab4))
                {
                    _BEAmpliacion.id_tabla = Convert.ToInt32(lblID_CertificacionTab4.Text);
                    _BEAmpliacion.tipo = 1;

                    _BEAmpliacion.concepto = txtTipoCertificacionTab4.Text;
                    _BEAmpliacion.entidad = txtEntidadTab4.Text;
                    _BEAmpliacion.Date_fechaPresentacionInforme = VerificaFecha(txtFechaAprobacionEmisionTab4.Text);

                    if (FileUpload_CertificacionTab4.PostedFile.ContentLength > 0)
                    {
                        _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_CertificacionTab4);
                    }
                    else
                    {
                        _BEAmpliacion.urlDoc = lnkbtnCertificacionTab4.Text;
                    }
                    //_BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_CertificacionTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLAmpliacion.UD_MON_DetalleCertificacionAutorizacionPorContrata(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se modificó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaCertificacionTab4();

                        Panel_CertificacionTab4.Visible = false;
                        imgbtnAgregarCertificacionTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }

        protected Boolean ValidarCertificacionTab4()
        {
            Boolean result;
            result = true;

            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}



            return result;

        }
        protected void btnGuardarCertificacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarCertificacionTab4())
            {

                if (validaArchivo(FileUpload_CertificacionTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;

                    _BEAmpliacion.concepto = txtTipoCertificacionTab4.Text;
                    _BEAmpliacion.entidad = txtEntidadTab4.Text;
                    _BEAmpliacion.Date_fechaPresentacionInforme = VerificaFecha(txtFechaAprobacionEmisionTab4.Text);
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_CertificacionTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLAmpliacion.I_MON_DetalleCertificacionAutorizacionPorContrata(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaCertificacionTab4();

                        Panel_CertificacionTab4.Visible = false;
                        imgbtnAgregarCertificacionTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }
        protected void lnkbtnCertificacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnCertificacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgDocCertificacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //int id_ampliacion = Convert.ToInt32(grdAdendasTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdCertificacionTab4.Rows[row.RowIndex].FindControl("imgDocCertificacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdCertificacionTab4_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblID_CertificacionTab4.Text = grdCertificacionTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdCertificacionTab4.SelectedRow;

            txtTipoCertificacionTab4.Text = ((Label)row.FindControl("lblTipoTab4")).Text;
            txtEntidadTab4.Text = ((Label)row.FindControl("lblEntidadTab4")).Text;
            txtFechaAprobacionEmisionTab4.Text = ((Label)row.FindControl("lblFechaAprobacionTab4")).Text;

            lblNomActualizaCertificacionTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            lnkbtnCertificacionTab4.Text = ((ImageButton)row.FindControl("imgDocCertificacionTab4")).ToolTip;

            GeneraIcoFile(lnkbtnDocAdendaTab4.Text, imgbtnCertificacionTab4);

            btnGuardarCertificacionTab4.Visible = false;
            btnModificarCertificacionTab4.Visible = true;

            imgbtnAgregarCertificacionTab4.Visible = false;
            Panel_CertificacionTab4.Visible = true;

            Up_Tab4.Update();
        }
        protected void grdCertificacionTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdCertificacionTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_tabla = Convert.ToInt32(objTemp.ToString());
                _BEAmpliacion.tipo = 2;

                _BEAmpliacion.concepto = "";
                _BEAmpliacion.entidad = "";
                _BEAmpliacion.Date_fechaPresentacionInforme = VerificaFecha("");
                _BEAmpliacion.urlDoc = "";
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.UD_MON_DetalleCertificacionAutorizacionPorContrata(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaCertificacionTab4();

                    Panel_CertificacionTab4.Visible = false;
                    imgbtnAgregarCertificacionTab4.Visible = true;

                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdCertificacionTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocCertificacionTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void cargaParalizacionTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 6;

            grdParalizacionTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdParalizacionTab4.DataBind();

        }


        protected void grdParalizacionTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocParalizacionTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(txtTotalParalizacionTab4.Text) + Convert.ToInt32(dias.Text);
                txtTotalParalizacionTab4.Text = TotalDias.ToString();

                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void grdParalizacionTab4_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_ParalizacionTab4.Text = grdParalizacionTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdParalizacionTab4.SelectedRow;

            txtDiasParalizadosTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            //txtDiasParalizadosTab4.Enabled = false;
            txtResoluciónParalizacionTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolucionParalizacionTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoParalizacionTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionParalizacionTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            lnkbtnParalizacionTab4.Text = ((ImageButton)row.FindControl("imgDocParalizacionTab4")).ToolTip;
            lblNomActualizaParalizacionTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicioParalizacionTab4.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFinParalizacionTab4.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(lnkbtnParalizacionTab4.Text, imgbtnParalizacionTab4);

            btnGuardarParalizacionTab4.Visible = false;
            btnModificarParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;
            Panel_ParalizacionTab4.Visible = true;

            Up_Tab4.Update();
        }

        protected void grdParalizacionTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void lnkbtnParalizacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnParalizacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected Boolean ValidarParalizacionTab4()
        {
            Boolean result;
            result = true;

            //if (txtDiasParalizadosTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese dias paralizados.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtFechaInicioParalizacionTab4.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaInicioParalizacionTab4.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaInicioTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text) <= Convert.ToDateTime(txtFechaInicioTab2.Text))
            {
                string script = "<script>alert('La fecha de inicio de paralización debe ser mayor a la fecha de inicio de la obra.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaFinParalizacionTab4.Text == "")
            {
                string script = "<script>alert('Ingrese fecha fin de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaFinParalizacionTab4.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha fin de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) < Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text))
            {
                string script = "<script>alert('La fecha fin debe ser mayor o igual a la fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaTerminoReal.Text.Length > 0 && Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) >= Convert.ToDateTime(txtFechaTerminoReal.Text))
            {
                string script = "<script>alert('La fecha de fin de paralización debe ser menor a la fecha de termino real de la obra.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
           
            return result;
        }

        protected void btnGuardarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {

                if (validaArchivo(FileUpload_ParalizacionTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 6;

                    //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                    _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ParalizacionTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                    int dias = (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days + 1;
                    _BEAmpliacion.ampliacion = dias.ToString();

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 5;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion - (dtFechaInicio - dtFechaInicioProgramacion).Days + ampliacion;

                        if (lblCOD_SUBSECTOR.Text == "3")
                        {
                            GeneraProgramacionPNSR(iPlazo, dtFechaInicio);
                        }
                        else
                        {
                            GeneraProgramacionPNSU(iPlazo, dtFechaInicio);
                        }


                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        txtTotalParalizacionTab4.Text = "0";
                        cargaParalizacionTab4();
                        //ActualizaFechasTab1();
                        //txtAmpPlazoPlaTab4.Text = "";
                        //txtResolAproPlaTab4.Text = "";
                        //txtFechaResolPlaTab4.Text = "";
                        //txtConceptoPlaTab4.Text = "";
                        //txtObservacionPlaTab4.Text = "";
                        //txtFechaInicio.Text = "";
                        //txtFechaFin.Text = "";

                        Panel_ParalizacionTab4.Visible = false;
                        imgbtnAgregarParalizacionTAb4.Visible = true;

                        Up_Tab4.Update();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }

        protected void btnModificarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {
                if (validaArchivo(FileUpload_ParalizacionTab4))
                {
                    _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_ParalizacionTab4.Text);
                    //_BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    //_BEAmpliacion.tipoFinanciamiento = 3;
                    //_BEAmpliacion.id_tipo_ampliacion = 6;

                    //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                    _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                    int dias = (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days + 1;
                    _BEAmpliacion.ampliacion = dias.ToString();

                    if (FileUpload_ParalizacionTab4.PostedFile.ContentLength > 0)
                    {
                        _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ParalizacionTab4);
                    }
                    else
                    {
                        _BEAmpliacion.urlDoc = lnkbtnParalizacionTab4.Text;
                    }

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int resul;
                    resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);

                    //int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (resul == 1)
                    {

                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 5;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        //DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion + ampliacion;

                        if (lblCOD_SUBSECTOR.Text == "3")
                        {
                            GeneraProgramacionPNSR(iPlazo, dtFechaInicioProgramacion);
                        }
                        else
                        {
                            GeneraProgramacionPNSU(iPlazo, dtFechaInicioProgramacion);
                        }

                        string script = "<script>alert('Registro Correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        Panel_ParalizacionTab4.Visible = false;
                        imgbtnAgregarParalizacionTAb4.Visible = true;

                        txtTotalParalizacionTab4.Text = "0";
                        cargaParalizacionTab4();
                        ActualizaFechasTab1();

                        Up_Tab4.Update();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }
        protected void btnCancelarParalizacionTab4_Click(object sender, EventArgs e)
        {
            Panel_ParalizacionTab4.Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = true;

            Up_Tab4.Update();
        }

        protected void imgbtnAgregarParalizacionTAb4_Click(object sender, ImageClickEventArgs e)
        {

            Panel_ParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;
            //lblNomUsuarioPresupuesto.Text = "";
            txtDiasParalizadosTab4.Enabled = true;
            btnGuardarParalizacionTab4.Visible = true;
            btnModificarParalizacionTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void imgDocParalizacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdParalizacionTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdParalizacionTab4.Rows[row.RowIndex].FindControl("imgDocParalizacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        #endregion

        #region Tab6

        protected void CargaTab6()
        {
            CargaPanelTab6();
            CargaTipoDocumentoMonitorTab6();
        }

        protected void CargaTipoDocumentoMonitorTab6()
        {
            ddlTipoArchivoTab6.DataSource = _objBLBandeja.F_spMON_TipoDocumentoMonitor();
            ddlTipoArchivoTab6.DataValueField = "valor";
            ddlTipoArchivoTab6.DataTextField = "nombre";
            ddlTipoArchivoTab6.DataBind();

            ddlTipoArchivoTab6.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void CargaPanelTab6()
        {
            _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdPanelTab6.DataSource = _objBLBandeja.F_spMON_DocumentoMonitor(_BEBandeja);
            grdPanelTab6.DataBind();
        }

        protected void imgbtnAgregarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnGuardarPanelTab6.Visible = true;
            btnModificarPanelTab6.Visible = false;
            lblNomUsuarioDocumento.Text = "";

            txtNombreArchivoTab6.Text = "";
            txtDescripcionTab6.Text = "";
            txtFechaTab6.Text = "";
            LnkbtnDocTab6.Text = "";
            ddlTipoArchivoTab6.SelectedValue = "";
            imgbtnDocTab6.ImageUrl = "~/img/blanco.png";

            Up_Tab6.Update();
        }

        protected void btnCancelarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = false;
            imgbtnAgregarPanelTab6.Visible = true;
            Up_Tab6.Update();
        }

        protected void imgDocTab6_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdPanelTab6.Rows[row.RowIndex].FindControl("imgDocTab6");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void btnGuardarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {

                if (validaArchivo(FileUpload_Tab6))
                {
                    _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;
                    _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLBandeja.spi_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Registro correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;

                        txtNombreArchivoTab6.Text = "";
                        txtDescripcionTab6.Text = "";
                        ddlTipoArchivoTab6.SelectedValue = "";
                        txtFechaTab6.Text = "";
                        imgbtnDocTab6.ImageUrl = "~/img/blanco.png";
                        Up_Tab6.Update();


                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }
        protected void grdPanelTab6_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab6");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }


        protected void grdPanelTab6_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPanelTab6.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.Id_documentoMonitor = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.tipo = "2";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);

                int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaPanelTab6();
                    Up_Tab6.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPanelTab6_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_documento_monitor.Text = grdPanelTab6.SelectedDataKey.Value.ToString();

            GridViewRow row = grdPanelTab6.SelectedRow;

            txtNombreArchivoTab6.Text = ((Label)row.FindControl("lblNombre")).Text;
            txtDescripcionTab6.Text = ((Label)row.FindControl("lblDescripcion")).Text;

            ddlTipoArchivoTab6.SelectedValue = ((Label)row.FindControl("lblIDTipo")).Text;

            txtFechaTab6.Text = ((Label)row.FindControl("lblFecha")).Text;
            LnkbtnDocTab6.Text = ((ImageButton)row.FindControl("imgDocTab6")).ToolTip;

            lblNomUsuarioDocumento.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnDocTab6.Text, imgbtnDocTab6);

            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnModificarPanelTab6.Visible = true;
            btnGuardarPanelTab6.Visible = false;
            Up_Tab6.Update();
        }

        protected Boolean validarPanelTab6()
        {
            Boolean result;
            result = true;

            if (txtNombreArchivoTab6.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (ddlTipoArchivoTab6.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaTab6.Text == "")
            {
                string script = "<script>alert('Ingresar fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (FileUpload_Tab6.HasFile == false && LnkbtnDocTab6.Text == "")
            {
                string script = "<script>alert('Adjuntar archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (ddlTipoArchivoTab6.SelectedValue.Equals("1"))
                {
                    if (validaArchivoFotos(FileUpload_Tab6) == false)
                    {
                        result = false;
                        return result;
                    }
                }
            }


            return result;


        }

        protected void btnModificarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    _BEBandeja.Id_documentoMonitor = Convert.ToInt32(lblId_documento_monitor.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;

                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEBandeja.tipo = "1";

                    if (FileUpload_Tab6.HasFile)
                    {
                        _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    }
                    else
                    {
                        _BEBandeja.UrlDoc = LnkbtnDocTab6.Text;
                    }


                    int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;
                        Up_Tab6.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }



        }

        protected void LnkbtnDocTab6_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkbtnDocTab6.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        #endregion

        //#region Tab7

        //protected void CargaTab7()
        //{
        //    cargaConclusionTab7();

        //}

        //protected void cargaConclusionTab7()
        //{
        //    _BEConclusion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEConclusion.tipoFinanciamiento = 3;

        //    dt = _objBLConclusion.spMON_Conclusion(_BEConclusion);

        //    if (dt.Rows.Count > 0)
        //    {
        //        txtConclusionTab7.Text = dt.Rows[0]["conclusiones"].ToString();
        //        txtRecomendacionTab7.Text = dt.Rows[0]["observaciones"].ToString();
        //        txtFechaREgistroTab7.Text = dt.Rows[0]["fecha_registro"].ToString();
        //        lnkbtnFileTab7.Text = dt.Rows[0]["urlDoc"].ToString();

        //        GeneraIcoFile(lnkbtnFileTab7.Text, imgbtnFileTab7);


        //    }

        //}

        //protected void lnkbtnFileTab7_OnClik(object sender, EventArgs e)
        //{
        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
        //    FileInfo toDownload = new FileInfo(pat + lnkbtnFileTab7.Text);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + lnkbtnFileTab7.Text);
        //        Response.End();
        //    }

        //}

        //protected void imgbtnFileTab7_OnClik(object sender, EventArgs e)
        //{
        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
        //    FileInfo toDownload = new FileInfo(pat + lnkbtnFileTab7.Text);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + lnkbtnFileTab7.Text);
        //        Response.End();
        //    }

        //}
        //protected void btnGuardarTab7_OnClick(object sender, EventArgs e)
        //{
        //    if (validaArchivo(FileUploadTab7))
        //    {
        //        _BEConclusion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEConclusion.tipoFinanciamiento = 3;

        //        _BEConclusion.conclusion = txtConclusionTab7.Text;
        //        _BEConclusion.observacion = txtRecomendacionTab7.Text;
        //        _BEConclusion.urlDoc = _Metodo.uploadfile(FileUploadTab7);
        //        _BEConclusion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLConclusion.spi_MON_Conclusion(_BEConclusion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se registró correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            cargaConclusionTab7();

        //            Up_Tab4.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }
        //    }
        //}

        //#endregion


        //#region Tab9
        //protected void CargaTab9()
        //{
        //    DataTable dtDatos = new DataTable();
        //    dtDatos = objBLProyecto.spSOL_ObtieneProyecto(Convert.ToInt32(LblID_SOLICITUD.Text));

        //    if (dtDatos.Rows.Count > 0)
        //    {
        //        txtUnidadFormuladoraTab9.Text = dtDatos.Rows[0]["pliegoUF"].ToString();
        //        txtNivelViabilidadTab9.Text = dtDatos.Rows[0]["situacion"].ToString();
        //        txtUltioEstudioTab9.Text = dtDatos.Rows[0]["ultimoEstudio"].ToString();
        //        txtEstadoEstudioTab9.Text = dtDatos.Rows[0]["estadoUltimoEstudio"].ToString();
        //        txtFechaViabilidadTab9.Text = dtDatos.Rows[0]["aprobacion_viabilidad"].ToString();
        //        txtPoblacionTab9.Text = dtDatos.Rows[0]["poblacion_snip"].ToString();
        //        txtUltimoMontoTab9.Text = dtDatos.Rows[0]["ultimoMonto"].ToString();
        //        txtEstadoCheclistTab9.Text = dtDatos.Rows[0]["EstadoChecklist"].ToString();
        //        txtRevisorCheclit.Text = dtDatos.Rows[0]["UsuarioUpdateChecklist"].ToString() + " - " + dtDatos.Rows[0]["FechaUpdateChecklist"].ToString();

        //        txtResponsableETTab9.Text = dtDatos.Rows[0]["resp_elaboracion"].ToString();
        //        txtElaboracionPresupuestoTab9.Text = dtDatos.Rows[0]["fecha_pres_obra"].ToString();

        //        txtMontoF15Tab9.Text = dtDatos.Rows[0]["MontoF15"].ToString();
        //        txtMontoF16Tab9.Text = dtDatos.Rows[0]["MontoF16"].ToString();
        //    }
        //}

        //#endregion

        protected void lnkbtnFichaRecepcionTab2_OnClick(object sender, EventArgs e)
        {
            //LinkButton boton;
            //boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //string urlDocumento = lnkbtnFichaRecepcionTab2.Text;

            //FileInfo toDownload = new FileInfo(pat + urlDocumento);
            //if (toDownload.Exists)
            //{
            //    Response.Clear();
            //    Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            //    Response.AddHeader("Content-Length", toDownload.Length.ToString());
            //    Response.ContentType = "application/octet-stream";
            //    Response.WriteFile(pat + urlDocumento);
            //    Response.End();
            //}
        }
        protected void imgbtnFichaRecepcionTab2_OnClick(object sender, ImageClickEventArgs e)
        {
            //ImageButton boton;
            //boton = (ImageButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //string urlDocumento = lnkbtnFichaRecepcionTab2.Text;

            //FileInfo toDownload = new FileInfo(pat + urlDocumento);
            //if (toDownload.Exists)
            //{
            //    Response.Clear();
            //    Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            //    Response.AddHeader("Content-Length", toDownload.Length.ToString());
            //    Response.ContentType = "application/octet-stream";
            //    Response.WriteFile(pat + urlDocumento);
            //    Response.End();
            //}
        }
      
        protected void btnDetalleDevengadoTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMDevengadoMensualizadoTab0(Convert.ToInt32(id));
            UpdatePanel3.Update();
            MPE_DetalleSOSEMTab0.Show();
        }
        protected void btnFuenteTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMFuenteFinanciamientoTab0(Convert.ToInt32(id));
            Up_FuenteFinanciamientoTAb0.Update();
            MPE_FuenteFinanciamientoTab0.Show();
        }
        protected Boolean ValidarEstadoSituacional()
        {
            Boolean result;
            result = true;

            if (ddlEstadoTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Estado de Ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            //if (ddlEstadoTab2.SelectedValue == "5" || ddlEstadoTab2.SelectedValue == "6" || ddlEstadoTab2.SelectedValue == "11")
            //{
            if (ddlSubEstadoTab2.SelectedValue == "" && ddlSubEstadoTab2.Visible == true)
            {
                string script = "<script>alert('Ingrese Sub - Estado de Ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            //}

            if (ddlEstadoTab2.SelectedValue == "5")
            {
                if (ddlSubEstado2Tab2.SelectedValue == "")
                {
                    string script = "<script>alert('Ingrese Sub - Estado 2 de Ejecución.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }

            if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlEstadoTab2.SelectedValue), 3)))
            {
                string script = "<script>alert('No puede volver el proyecto a Actos Previos');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            //_BEEjecucion.coordinadorMVCS = txtCordinadorTab2.Text;
            _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
            _BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;
            _BEEjecucion.id_tipoEstadoParalizado = ddlSubEstado2Tab2.SelectedValue;

            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.IU_spiu_MON_EstadoSituacionalNE(_BEEjecucion);
 
            //if (lblIdEstado.Text.Trim() != "") {
            //    if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlEstadoTab2.SelectedValue), 3)))
            //    {
            //        string script = "<script>alert('No puede volver el proyecto a Actos Previos');</script>";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //        result = false;
            //        return result;

            //    }
            //}
           
 
            return result;
        }

        //protected void cargarConexionesPNSR()
        //{
        //    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    dt = _objBLEjecucion.spMON_MetasPNSR(_BEEjecucion);

        //    if (dt.Rows.Count > 0)
        //    {
        //        txtConexionAgua.Text = dt.Rows[0]["nroAgua"].ToString();
        //        txtConexionUbs.Text = dt.Rows[0]["nroAlcantarilladoUBS"].ToString();
        //        lblNomActualizaConexionesTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdateMetas"].ToString();
        //    }
        //}

        //protected void btnGuardarConexionTab2_Click(object sender, EventArgs e)
        //{
        //    if (ValidarMetas())
        //    {
        //        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEEjecucion.nroAgua = Convert.ToInt32(txtConexionAgua.Text);
        //        _BEEjecucion.nroAlcantarillado = Convert.ToInt32(txtConexionUbs.Text);
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLEjecucion.U_MON_MetasPNSR(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Registro Correcto.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //            //CargaFinanTransferencia();

        //            cargarConexionesPNSR();
        //            Up_Tab2.Update();
        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }
        //}


        //protected bool ValidarMetas()
        //{
        //    bool result = true;

        //    if (txtConexionAgua.Text.Equals(""))
        //    {
        //        string script = "<script>alert('Ingresar nro. de conexiones de agua.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        result = false;
        //        return result;
        //    }

        //    if (txtConexionUbs.Text.Equals(""))
        //    {
        //        string script = "<script>alert('Ingresar nro. de conexiones UBS.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        result = false;
        //        return result;
        //    }

        //    return result;
        //    return result;
        //}



        protected void TabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            ViewState["ActiveTabIndex"] = TabContainer.ActiveTabIndex;
        }

        protected void imgbtnEditarUE_Click(object sender, ImageClickEventArgs e)
        {
            string script = "<script>$('#modalEditarUE').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void btnModificarUE_Click(object sender, EventArgs e)
        {
            ObjBEProyecto.id = Convert.ToInt32(LblID_PROYECTO.Text);
            if (LblID_SOLICITUD.Text == "")
            {
                ObjBEProyecto.Id_Solicitudes = 0;
            }
            else
            {
                ObjBEProyecto.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
            }
            ObjBEProyecto.Entidad_ejec = txtUnidadEjecutora.Text;

            int val = objBLProyecto.U_MON_UnidadEjecutora(ObjBEProyecto);

            if (val == 1)
            {
                string script = "<script>$('#modalEditarUE').modal('hide');alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                }
                UPTabContainerDetalles.Update();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void grdUbicacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdUbicacion.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.flagActivo = 0;
                _BEBandeja.CCPP = ((Label)CurrentRow.FindControl("lblUbigeo")).Text + ((Label)CurrentRow.FindControl("lblUbigeoCCPP")).Text;
                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarUbicacion_Click(object sender, ImageClickEventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = true;
            //btnModificarUbicacionMetasTab0.Visible = false;
            Up_UbigeoTab0.Update();
        }

        protected Boolean ValidarUbigeoTab0()
        {
            Boolean result;
            result = true;

            if (ddlDepartamentoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Departamento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlProvinciaTab0.SelectedValue == "")
            {

                string script = "<script>alert('Ingrese Provincia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;


            }

            if (ddlDistritoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Distrito.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;
        }

        protected void btnGuardarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            if (ValidarUbigeoTab0())
            {
                _BEBandeja.flagActivo = 1;
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                if (ddlCCPPTab0.SelectedValue.Equals(""))
                {
                    _BEBandeja.CCPP = ddlDepartamentoTab0.SelectedValue + ddlProvinciaTab0.SelectedValue + ddlDistritoTab0.SelectedValue;
                }
                else
                {
                    _BEBandeja.CCPP = ddlCCPPTab0.SelectedValue;
                }

                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void btnCancelarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = false;
        }

        protected void ddlDepartamentoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlProvinciaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlDistritoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = _objBLBandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("- SELECCIONAR -", ""));

        }

        protected void CargaUbigeosProyectos(int idProyecto)
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = _objBLBandeja.F_spMON_Ubigeo(idProyecto);
            grdUbicacion.DataSource = ListUbigeos;
            grdUbicacion.DataBind();



        }

        protected void imgbtnEditarUbigeo_Click(object sender, ImageClickEventArgs e)
        {
            CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));

            cargaUbigeo(ddlDepartamentoTab0, "1", null, null, null);
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);

            Up_UbigeoTab0.Update();
            //string script = "<script>ShowPopupRegistroInformeVisita();</script>";
            string script = "<script>$('#modalUbigeo').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void BtnRefrescar_Click(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);
            DataTable dt = (new BLProyecto().spu_MON_UltimaActualizacion(idProy));
            if (dt.Rows.Count > 0)
            {
                LblUltimaModificacion.Text = Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortTimeString();
                LblUsuario.Text = dt.Rows[0]["Usuario"].ToString();
            }
            else
            {
                LblUltimaModificacion.Text = "";
                LblUsuario.Text = "";
            }
        }

        protected void ddlSubEstado2Tab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadoParalizado(ddlSubEstado2Tab2.SelectedValue); //TIPO PROBLEMATICA
            Up_TipoProblematicaTab2.Update();
        }
        protected void CargaTipoSubEstadoParalizado(string pTipoEstadoParalizado)
        {
            chbProblematicaTab2.Items.Clear();

            if (ddlSubEstadoTab2.SelectedValue.Equals("82"))//PERMANENTE
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                if (!(pTipoEstadoParalizado == ""))
                {
                    list = _objBLEjecucion.F_spMON_TipoSubEstadoParalizado(Convert.ToInt32(pTipoEstadoParalizado));
                }
                else
                {
                    list = null;
                }

                chbProblematicaTab2.DataSource = list;
                chbProblematicaTab2.DataTextField = "nombre";
                chbProblematicaTab2.DataValueField = "valor";
                chbProblematicaTab2.DataBind();
            }
        }
        protected void ddlSubEstadoTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            trSuspensionTab2.Visible = false;
            trReinicioTab2.Visible = false;
            spSubestado2.Visible = false;
            ddlSubEstado2Tab2.Visible = false;

            if (ddlSubEstadoTab2.SelectedValue.Equals("87")) // SUSPENSION DE PLAZO
            {
                trSuspensionTab2.Visible = true;
                trReinicioTab2.Visible = true;
                Up_FechasSuspensionTab2.Update();

                spSubestado2.Visible = true;
                ddlSubEstado2Tab2.Visible = true;
                //Up_SubEstado2NombreTab2.Update();
                //Up_SubEstado2DetalleTab2.Update();
                CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
            }
            else if (ddlSubEstadoTab2.SelectedValue.Equals("80")) //ATRAZADO
            {
                spSubestado2.Visible = true;
                ddlSubEstado2Tab2.Visible = true;
                //Up_SubEstado2NombreTab2.Update();
                //Up_SubEstado2DetalleTab2.Update();
                CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
            }
            Up_FechasSuspensionTab2.Update();
        }

        protected void CargaTipoSubEstado2(string pTipoSubEstado)
        {
            //ddlTipoSubEstado2InformeTab2.DataSource = null;
            //ddlTipoSubEstado2InformeTab2.DataBind();
            ddlSubEstado2Tab2.Items.Clear();

            List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

            if (!(pTipoSubEstado == ""))
            {
                list = _objBLEjecucion.F_spMON_TipoSubEstado2(Convert.ToInt32(pTipoSubEstado));
            }
            else
            {
                list = null;
            }

            ddlSubEstado2Tab2.DataSource = list;
            ddlSubEstado2Tab2.DataTextField = "nombre";
            ddlSubEstado2Tab2.DataValueField = "valor";
            ddlSubEstado2Tab2.DataBind();

            ddlSubEstado2Tab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void grdAvanceFisicoTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAvanceFisicoTab2.PageIndex = e.NewPageIndex;
            CargaAvanceFisico();
        }

        protected void grdAvanceFisicoAdicionalTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAvanceFisicoAdicionalTab2.PageIndex = e.NewPageIndex;
            CargaAvanceFisico();
        }

     

        protected void grdEvaluacionTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdEvaluacionTab2.PageIndex = e.NewPageIndex;
            cargarEvaluacionTab2();
        }

        protected void grdProgramacionTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProgramacionTab2.PageIndex = e.NewPageIndex;
            cargaProgramacionTab2();
        }

        protected void grdFianzasTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdFianzasTab2.PageIndex = e.NewPageIndex;
            cargaCartasFianzasTab2();
        }

        protected void grdAvanceFisicoTab2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell cell1 = new TableHeaderCell();
                cell1.Text = "Información General";
                if (lblID_ACCESO.Text.Equals("0"))
                {
                    cell1.ColumnSpan = 4;
                }
                else
                {
                    cell1.ColumnSpan = 5;
                }

                cell1.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell1);

                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Amortización";
                HeaderCell.ColumnSpan = 2;
                HeaderCell.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(HeaderCell);

                TableCell cell2 = new TableCell();
                cell2.Text = "";
                cell2.ColumnSpan = 2;
                //cell3.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell2);


                TableCell cell3 = new TableCell();
                cell3.Text = "Avance % Fisico";
                cell3.ColumnSpan = 2;
                cell3.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell3);

                TableCell cell4 = new TableCell();
                cell4.Text = "Avance % Financiero";
                cell4.ColumnSpan = 2;
                cell4.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell4);

                HeaderGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);

            }
        }

        protected void grdAvanceFisicoAdicionalTab2_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell cell1 = new TableHeaderCell();
                cell1.Text = "Información General";
                if (lblID_ACCESO.Text.Equals("0"))
                {
                    cell1.ColumnSpan = 5;
                }
                else
                {
                    cell1.ColumnSpan = 6;
                }

                cell1.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell1);

                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Amortización";
                HeaderCell.ColumnSpan = 2;
                HeaderCell.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(HeaderCell);

                TableCell cell2 = new TableCell();
                cell2.Text = "";
                cell2.ColumnSpan = 2;
                //cell3.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell2);


                TableCell cell3 = new TableCell();
                cell3.Text = "Avance % Fisico";
                cell3.ColumnSpan = 2;
                cell3.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell3);

                TableCell cell4 = new TableCell();
                cell4.Text = "Avance % Financiero";
                cell4.ColumnSpan = 2;
                cell4.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell4);

                HeaderGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);

            }
        }

        protected void btnGuardarCierreTab3_Click(object sender, EventArgs e)
        {
            string msj = ValidarCierre();
            if (msj.Length > 0)
            {
                string script = "<script>alert('" + msj + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BELiquidacion.tipoFinanciamiento = 3;

                _BELiquidacion.idTipoFormato = Convert.ToInt32(ddlFormatoCierreTabCierre.SelectedValue);
                _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaFormato14TabCierre.Text);
                _BELiquidacion.urlDocumento = lnkbtnFormatoCierreTabCierre.Text;

                if (FileUploadFormatoCierreTabCierre.HasFile)
                {
                    _BELiquidacion.urlDocumento = _Metodo.uploadfile(FileUploadFormatoCierreTabCierre);
                }
                else
                { _BELiquidacion.urlDocumento = lnkbtnFormatoCierreTabCierre.Text; }


                _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLLiquidacion.IU_spiu_MON_CierreProyecto_NE(_BELiquidacion);


                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaLiquidacion();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }


                Up_TabCierre.Update();
                Up_Tab3.Update();
            }
        }

        protected string ValidarCierre()
        {
            string msj = "";


            if (ddlFormatoCierreTabCierre.SelectedValue == "")
            {
                msj = msj + "*Ingrese tipo de formato de cierre.\\n";
            }

            if (txtFechaFormato14TabCierre.Text == "")
            {
                msj = msj + "*Ingrese fecha de registro de formato.\\n";
            }
            else
            {
                if (_Metodo.ValidaFecha(txtFechaFormato14TabCierre.Text) == false)
                {
                    msj = msj + "*Formato no valido de Fecha de registro de formato.";
                }
                else if (Convert.ToDateTime(txtFechaFormato14TabCierre.Text) > DateTime.Now.Date)
                {
                    msj = msj + "*La fecha de registro de formato no puede ser mayor a la fecha de hoy.\\n";
                }
            }

            return msj;
        }

        protected void imgbtnFormatoCierreTabCierre_Click(object sender, ImageClickEventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFormatoCierreTabCierre.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void CargaFormatoTabCierre()
        {
            DataSet ds = _objBLFinanciamiento.spMON_rParametro(55);

            ddlFormatoCierreTabCierre.DataSource = ds.Tables[0];
            ddlFormatoCierreTabCierre.DataTextField = "nombre";
            ddlFormatoCierreTabCierre.DataValueField = "valor";
            ddlFormatoCierreTabCierre.DataBind();

            ddlFormatoCierreTabCierre.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void lnkbtnFormatoCierreTabCierre_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFormatoCierreTabCierre.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnSostenibilidadTab3_Click(object sender, ImageClickEventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkbtnSostenibilidadTab3.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnSostenibilidadTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkbtnSostenibilidadTab3.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnBuscarRUCContratistaTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsultorTab1, txtNombConsultorTab1, txtRepresentanteLegalConsultorTab1, divMsjBusquedaRucContratistaTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }


        protected void lnkbtnBuscarRUCConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsorcioTab1, txtNomconsorcioTab1, txtRepresentanteTab1, divMsjBusquedaRucConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void lnkbtnBuscarRUCMiembroConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucGrupTab1, txtContratistaGrupTab1, txtRepresentanGrupTAb1, divMsjBusquedaRucMiembroConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void btnFinalizarTab3_Click(object sender, EventArgs e)
        {
            if (lblIdEstadoRegistro.Text.Equals("2")) //ACTIVO
            {
                _BEEjecucion.id_estadoProyecto = 3;
            }
            else
            {
                _BEEjecucion.id_estadoProyecto = 2;
            }
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_EstadoProyecto(_BEEjecucion);

            if (val == 1)
            {
                //CargaLiquidacion();

                string script = "<script>alert('Se Actualizó Correctamente'); window.location.reload()</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void lnkbtnSinRuc_Click(object sender, EventArgs e)
        {
            txtNomconsorcioTab1.Enabled = true;
            txtRepresentanteTab1.Enabled = true;
        }

        protected void btnVigenciaVencida_Click(object sender, EventArgs e)
        {
            string script = "<script>$('#modalVigencia').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            dt = _objBLFinanciamiento.spMON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text));

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["flagActivoReporte"].ToString() == "1")
                {
                    btnRegistrarVencimiento.Enabled = true;
                }

                if (dt.Rows[0]["fechaUpdateVencimiento"].ToString().Length > 0)
                {
                    ddlFlagVigencia.SelectedValue = dt.Rows[0]["flagVigenciaVencida"].ToString();
                    ddlTipoAccionVigencia.SelectedValue = dt.Rows[0]["idTipoAccionVencimiento"].ToString();

                    if (ddlFlagVigencia.SelectedValue.Equals("1"))
                    {
                        divAccionVigencia.Visible = true;
                    }
                    else
                    {
                        divAccionVigencia.Visible = false;
                        ddlTipoAccionVigencia.SelectedValue = "";
                    }

                    if (ddlTipoAccionVigencia.SelectedValue == "")
                    {
                        divAccionVigencia.Visible = false;
                    }

                    lblNomActualizaVencimiento.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdateVencimiento"].ToString();
                }
            }

            Up_PanelVigencia.Update();
        }

        protected String ValidarVigenciaVencia()
        {
            string msj = "";


            if (ddlFlagVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar etapa con vigencia vencida.\\n";
            }

            if (ddlFlagVigencia.SelectedValue.Equals("1") && ddlTipoAccionVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de acción.\\n";
            }


            return msj;

        }
        protected void btnRegistrarVencimiento_Click(object sender, EventArgs e)
        {
            string msjVal = ValidarVigenciaVencia();

            if (msjVal.Length > 0)
            {
                string script = "<script>alert('" + msjVal + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                int pTipoAccion = 0;
                if (ddlTipoAccionVigencia.SelectedValue != "")
                {
                    pTipoAccion = Convert.ToInt32(ddlTipoAccionVigencia.SelectedValue);
                }

                DataSet ds = new DataSet();
                ds = _objBLFinanciamiento.paSSP_spiu_MON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(LblID_USUARIO.Text), Convert.ToInt32(ddlFlagVigencia.SelectedValue), pTipoAccion);

                if (ds.Tables[0].Rows[0]["vCod"].ToString() == "200")
                {
                    string script = "<script>$('#modalVigencia').modal('hide');alert('Se registró la vigencia de vencimiento. Se actualizará la página');window.location.reload()</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Hubo un error, volver a intentar.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void ddlFlagVigencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFlagVigencia.SelectedValue.Equals("1"))
            {
                divAccionVigencia.Visible = true;
            }
            else
            {
                divAccionVigencia.Visible = false;
                ddlTipoAccionVigencia.SelectedValue = "";
            }

            Up_PanelVigencia.Update();
        }

        protected void TabActivoProceso()
        {
            if (grSeguimientoSeace.Rows.Count > 0)
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }
            else if (grdSeguimientoProcesoTab1.Rows.Count > 0 || txtValorReferencialTab1.Text.Length > 0)
            {
                PanelSeaceAuto.Visible = false;
                PanelSeaceManual.Visible = true;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";
            }
            else
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }

            //Desahabilitar para PNSR. AMAZONIA , PIASAR y PROCOES
            //Por ahora se desactiva todo 2020.11.13
            //if (lblCOD_SUBSECTOR.Text.Equals("3") && (lblID_SUBPROGRAMA.Text.Equals("4") || lblID_SUBPROGRAMA.Text.Equals("19") || lblID_SUBPROGRAMA.Text.Equals("2")))
            //{
                lnkbtnSeaceAutoPanel.Visible = false;
                lnkbtnSeaceManualPanel.Visible = false;
                PanelSeaceAuto.Visible = false;
                PanelSeaceManual.Visible = true;
            //}
        }
        protected void CargaCodigoSeace()
        {
            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            grSeguimientoSeace.DataSource = _objBLFinanciamiento.spMON_CodigoSeace(pIdProyecto, Convert.ToInt32(ddlProcesoTab1.SelectedValue));
            grSeguimientoSeace.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void CargaProcesosRelSEACE()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSEACEObra.DataSource = _objBLFinanciamiento.spMON_SEACE_Obra(snip, Convert.ToInt32(LblID_PROYECTO.Text));
            grdSEACEObra.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }
        protected void grdSEACEObra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSEACEObra.PageIndex = e.NewPageIndex;
            CargaProcesosRelSEACE();
        }
        protected void BtnRegistrarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            if (TxtCodigoSeace.Text.Length == 0)
            {
                string script = "<script>alert('Inresar código de SEACE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script3", script, false);
            }
            else
            {
                CodigosSeace _BE = new CodigosSeace();
                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(TxtCodigoSeace.Text);
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se registro correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnCancelarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            Panel_RegistroSeace.Visible = false;
            imgbtnAgregarSeguimientoSeace.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtnAgregarSeguimientoSeace_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RegistroSeace.Visible = true;
            imgbtnAgregarSeguimientoSeace.Visible = false;
            Up_Tab1.Update();
        }

        protected void grSeguimientoSeace_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grSeguimientoSeace.DataKeys[CurrentRow.RowIndex].Value as object;

                CodigosSeace _BE = new CodigosSeace();

                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(objTemp.ToString());
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grSeguimientoSeace_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblEstadoItem = (Label)e.Row.FindControl("lblEstadoItem");
                Label lblEstado = (Label)e.Row.FindControl("lblEstado");
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");

                string caseSwitch = lblEstadoItem.Text;

                lblEstado.Text = lblEstadoItem.Text;
                lblEstado.ToolTip = lblEstadoItem.Text;

                switch (caseSwitch)
                {
                    case "APELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO POR RECORTE PRESUPUESTAL":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "DESIERTO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUBSCRIPCION DEL CONTRATO POR DECISION DE LA ENTIDAD":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUSCRIBIO EL CONTRATO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NULO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "SUSPENDIDO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "ADJUDICADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "ADJUDICADO A PRORRATA":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONSENTIDO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONTRATADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    default:
                        lblEstado.CssClass = "label label-primary";
                        break;
                }

                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");

                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;

                }
            }
        }

        protected void grdSEACEObra_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");
                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");
                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;
                }
            }
        }
        protected void lnkbtnSeaceAutoPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = true;
            PanelSeaceManual.Visible = false;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";

            Up_Tab1.Update();
        }

        protected void lnkbtnSeaceManualPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";

            Up_Tab1.Update();
        }

        protected void ddlTipoAccion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoAccion.SelectedValue.ToString().Equals("2") )
            {
                lblTitDocAccion.Text = "Fecha de Renovación";
                txtFechaRenovacion.Visible = true;
                txtFechaRenovacion.Text = "";
                FileUploadAccionTab2.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else if (ddlTipoAccion.SelectedValue.ToString().Equals("3") || ddlTipoAccion.SelectedValue.ToString().Equals("4"))
            {
                lblTitDocAccion.Text = "Doc. " + ddlTipoAccion.SelectedItem.Text;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                FileUploadAccionTab2.Visible = true;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else
            {
                lblTitDocAccion.Text = "";
                FileUploadAccionTab2.Visible = false;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }

            Up_Tab2.Update();
        }

        protected void lnkbtnAccionTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnAccionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocAccionGriTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdFianzasTab2.Rows[row.RowIndex].FindControl("imgDocAccionGriTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void ddlTipoAccionSupervisionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoAccionSupervisionTab2.SelectedValue.ToString().Equals("2"))
            {
                lblTitDocAccionSupervisionTab2.Text = "Fecha de Renovación";
                txtFechaRenovacionSupervisionTab2.Visible = true;
                txtFechaRenovacionSupervisionTab2.Text = "";
                FileUploadAccionSupervisionTab2.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else if (ddlTipoAccionSupervisionTab2.SelectedValue.ToString().Equals("3") || ddlTipoAccionSupervisionTab2.SelectedValue.ToString().Equals("4"))
            {
                lblTitDocAccionSupervisionTab2.Text = "Doc. " + ddlTipoAccion.SelectedItem.Text;
                txtFechaRenovacionSupervisionTab2.Text = "";
                txtFechaRenovacionSupervisionTab2.Visible = false;
                FileUploadAccionSupervisionTab2.Visible = true;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else
            {
                lblTitDocAccionSupervisionTab2.Text = "";
                FileUploadAccionSupervisionTab2.Visible = false;
                txtFechaRenovacionSupervisionTab2.Text = "";
                txtFechaRenovacionSupervisionTab2.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }

            Up_Tab2.Update();
        }


        protected void lnkbtnAccionSupervisionTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnAccionSupervisionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocAccionSupervisionGriTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdFianzaSupervisionTab2.Rows[row.RowIndex].FindControl("imgDocAccionSupervisionGriTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnDocInspectorTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnDocInspectorTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnDocResidenteTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnDocResidenteTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocResidenteGri_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grd_HistorialResidenteTab2.Rows[row.RowIndex].FindControl("imgDocResidenteGri");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocInspectorGri_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grd_HistorialInspectorTab2.Rows[row.RowIndex].FindControl("imgDocInspectorGri");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocSupervisionGri_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdHistorialJefeSupervisionTab2.Rows[row.RowIndex].FindControl("imgDocSupervisionGri");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdHistorialJefeSupervisionTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocSupervisionGri");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void grd_HistorialInspectorTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocInspectorGri");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void grd_HistorialResidenteTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocResidenteGri");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void lnkbtnDocSupervisionTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnDocSupervisionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected Boolean verificarAdelanto()
        {
            string result = "";

           
            if (txtFechaAdelantoTab2.Text == "")
            {
                result = result + "*Ingresar Fecha de adelanto.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaAdelantoTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha de adelanto.\\n";
            }
            else
            {
                if (Convert.ToDateTime(txtFechaAdelantoTab2.Text) > DateTime.Today)
                {
                    result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

                }
            }

            if (ddlTipoAdelantoTab2.SelectedValue == "")
            {
                result = result + "*Seleccionar el tipo de adelanto.\\n";
            }

            if (txtMontoAdelantoTab2.Text == "")
            {
                result = result + "*Ingresar monto de adelanto.\\n";
            }


            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void grdAdelantoObraTab2_SelectedIndexChanged1(object sender, EventArgs e)
        {
            lblIdAdelantoTab2.Text = grdAdelantoObraTab2.SelectedDataKey.Value.ToString();

            GridViewRow row = grdAdelantoObraTab2.SelectedRow;

            txtFechaAdelantoTab2.Text = ((Label)row.FindControl("lblFechaAdelanto")).Text;
            txtNroAdelantoTab2.Text = ((Label)row.FindControl("lblNroAdelanto")).Text;
            txtMontoAdelantoTab2.Text = ((Label)row.FindControl("lblMonto")).Text;
            ddlTipoAdelantoTab2.SelectedValue = ((Label)row.FindControl("lblIdTipoAdelanto")).Text;

            lblNomUsuarioAdelantoTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;


            Panel_RegistroAdelantoTab2.Visible = true;
            imgbtnAgregarAdelantoTab2.Visible = false;
            btnGuardarAdelantoTab2.Visible = false;
            btnModificarAdelantoTab2.Visible = true;

            Up_Tab2.Update();
        }

        protected void grdAdelantoObraTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAdelantoObraTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.Date_fecha = Convert.ToDateTime("01/01/1990");
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                dt = new DataTable();
                dt = _objBLEjecucion.spud_MON_Adelanto(_BEEjecucion);

                if (dt.Rows[0]["vCod"].ToString().Equals("200"))
                {
                    string script = "<script>alert('Se eliminó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroAdelantoTab2.Visible = false;
                    imgbtnAgregarAdelantoTab2.Visible = true;
                    CargaAdelanto();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarAdelantoTab2_Click(object sender, ImageClickEventArgs e)
        {
            ddlTipoAdelantoTab2.SelectedValue = "";
            lblNomUsuarioAdelantoTab2.Text = "";
            txtNroAdelantoTab2.Text = "";
            txtMontoAdelantoTab2.Text = "";
            txtFechaAdelantoTab2.Text = "";
            Panel_RegistroAdelantoTab2.Visible = true;
            imgbtnAgregarAdelantoTab2.Visible = false;
            btnGuardarAdelantoTab2.Visible = true;
            btnModificarAdelantoTab2.Visible = false;

            Up_Tab2.Update();
        }

        protected void btnGuardarAdelantoTab2_Click(object sender, EventArgs e)
        {
            if (verificarAdelanto())
            {
                _BEEjecucion = new BE_MON_Ejecucion();

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.flag = 1;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaAdelantoTab2.Text);
                _BEEjecucion.nroAdelanto = txtNroAdelantoTab2.Text;
                _BEEjecucion.monto = txtMontoAdelantoTab2.Text;
                _BEEjecucion.idTipoAdelanto = ddlTipoAdelantoTab2.SelectedIndex;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                dt = new DataTable();
                dt = _objBLEjecucion.spi_MON_Adelanto(_BEEjecucion);

                if (dt.Rows[0]["vCod"].ToString().Equals("200"))
                {
                    string script = "<script>alert('Se registró correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroAdelantoTab2.Visible = false;
                    imgbtnAgregarAdelantoTab2.Visible = true;
                    CargaAdelanto();
                    Up_Tab2.Update();
                }
                else if (dt.Rows[0]["vCod"].ToString().Equals("0"))
                {
                    string script = "<script>alert('"+ dt.Rows[0]["vDesc"].ToString() + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }


        }

        protected void btnCancelarAdelantoTab2_Click(object sender, EventArgs e)
        {
            Panel_RegistroAdelantoTab2.Visible = false;
            imgbtnAgregarAdelantoTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void btnModificarAdelantoTab2_Click(object sender, EventArgs e)
        {
            if (verificarAdelanto())
            {
                _BEEjecucion = new BE_MON_Ejecucion();

                _BEEjecucion.id_item = Convert.ToInt32(lblIdAdelantoTab2.Text);
                _BEEjecucion.Tipo = 1;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaAdelantoTab2.Text);
                _BEEjecucion.nroAdelanto = txtNroAdelantoTab2.Text;
                _BEEjecucion.monto = txtMontoAdelantoTab2.Text;
                _BEEjecucion.idTipoAdelanto = ddlTipoAdelantoTab2.SelectedIndex;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                dt = new DataTable();
                dt = _objBLEjecucion.spud_MON_Adelanto(_BEEjecucion);

                if (dt.Rows[0]["vCod"].ToString().Equals("200"))
                {
                    string script = "<script>alert('Se actualizó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroAdelantoTab2.Visible = false;
                    imgbtnAgregarAdelantoTab2.Visible = true;
                    CargaAdelanto();
                    Up_Tab2.Update();
                }
                else if (dt.Rows[0]["vCod"].ToString().Equals("0"))
                {
                    string script = "<script>alert('" + dt.Rows[0]["vDesc"].ToString() + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        public void CargaAdelanto() {
            BE_MON_Ejecucion _BE = new BE_MON_Ejecucion();
            _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BE.flag = 1;

            dt = _objBLEjecucion.spMON_Adelanto(_BE);

            grdAdelantoObraTab2.DataSource = dt;
            grdAdelantoObraTab2.DataBind();


            lblTotalMontoAdelantoDirecto.Text = (dt.AsEnumerable()
                                                    .Where(y => y.Field<int>("idTipoAdelanto") == 1)
                                                    .Sum(x => x.Field<double>("monto")))
                                                    .ToString();

            lblTotalMontoAdelantoMaterial.Text = (dt.AsEnumerable()
                                            .Where(y => y.Field<int>("idTipoAdelanto") == 2)
                                            .Sum(x => x.Field<double>("monto")))
                                            .ToString();
  
        }

    
    }
}