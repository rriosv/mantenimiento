﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Ayuda_MemoriaNE : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        DataTable dt = new DataTable();

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["idtipo"] != null)
                {
                    string pGetIdProyecto = (Request.QueryString["id"].ToString());
                    string pGetIdRevision = (Request.QueryString["idtipo"].ToString());

                    if (pGetIdProyecto.Length > 0 && pGetIdRevision.Length > 0)
                    {
                        lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";

                        if (Request.Browser.IsMobileDevice && imgbtnExportar.Visible == true)
                        {
                            imgbtnImprimir.Visible = false;
                            imgbtnExportar.Visible = false;
                            imgbtnExportarMobile.Visible = true;
                        }

                        CargaInformacionGeneral();
                    }
                    else
                    {
                        lblTitle.Text = "NOTA: NO EXISTE INFORMACIÓN PARA MOSTRAR.";

                        string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                    try
                    {
                        string t = Request.QueryString["t"].ToString();

                        if (t == "p")
                        {
                            imgbtnImprimir.Visible = false;
                            imgbtnExportar.Visible = false;
                        }
                    }
                    catch
                    { }

                }
                else
                {
                    lblTitle.Text = "NOTA: PARAMETROS INCORRECTOS.";

                    string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void CargaInformacionGeneral()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;
            _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());


            dt = _objBLLiquidacion.spMON_Ayuda_MemoriaNE(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                int idPrograma = Convert.ToInt32(dt.Rows[0]["id_tipoPrograma"]);


                lblSnip.Text = dt.Rows[0]["cod_snip"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblMontoViable.Text = dt.Rows[0]["MONTO_SNIP"].ToString();

                lblEjecutora.Text = dt.Rows[0]["UnidadEjecutora"].ToString();
                lblBeneficiario.Text = dt.Rows[0]["poblacionSNIP"].ToString();




                //lblMonto1_PMIB.Text = dt.Rows[0]["montoInversion"].ToString();
                lblPorcentajeMontoMVCS_PMIB.Text = Convert.ToDecimal(dt.Rows[0]["montoAprobado"]).ToString("N2");
                lblPorcentajeMontoMUNIC_PMIB.Text = Convert.ToDecimal(dt.Rows[0]["monto_financiamientoPrograma"]).ToString("N2");
                lblPorcentajeMontoBENEF_PMIB.Text = Convert.ToDecimal(dt.Rows[0]["monto_TransferirNE"]).ToString("N2");

                lblMontoViable.Text = dt.Rows[0]["montoViable"].ToString();
                lblMontoFaseInversion.Text = dt.Rows[0]["montoinversion"].ToString();
                //lblMontoConvenio.Text = dt.Rows[0]["MONTO_CONVENIO"].ToString();


                BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
                DataTable ListTransferencia = _objBLTransferencia.spMON_GastosSAV_NE(Convert.ToInt32(Request.QueryString["id"].ToString()));
                grdDevengado.DataSource = ListTransferencia;
                grdDevengado.DataBind();


                lblFechaInicio1.Text = dt.Rows[0]["FECHAINICIO"].ToString();
                lblPlazoEjecucion1.Text = dt.Rows[0]["PLAZOEJECUCION"].ToString();
                lblFechaTerminoContractual.Text = dt.Rows[0]["FECHA_FIN"].ToString();

                //lblFechaTerminoReal.Text = dt.Rows[0]["FECHA_FIN"].ToString();

                lblResdente.Text = dt.Rows[0]["Residente"].ToString();
                //lblEmpresaSupervisora.Text = dt.Rows[0]["Residente"].ToString();

                lblSupervisor.Text = dt.Rows[0]["Supervisor"].ToString();


                lblPorcentajeAvance.Text = dt.Rows[0]["FISICOREAL"].ToString();
                lblAvanceProgramado.Text = dt.Rows[0]["FisicoProgramado"].ToString();
              
                lblAvanceFinanciero.Text = dt.Rows[0]["AvanceFinanciero"].ToString();
                lblAvanceFinanciero.Text = Convert.ToDouble(lblAvanceFinanciero.Text).ToString("N") + "% (Fuente - SSI)";

                lblEstadoSituacional.Text = dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();

                if (_BELiquidacion.tipoFinanciamiento == 3)
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                    List<BE_MON_Ejecucion> _ListAvance = _objBLEjecucion.F_spMON_EvaluacionRecomendacion(_BEEjecucion);

                    _ListAvance = (from cust in _ListAvance
                                   where cust.flagAyuda == "1"
                                   orderby cust.Date_fechaVisita descending
                                   select cust).ToList();
                    if (_ListAvance.Count > 0)
                    {
                        StringBuilder strDetalleSituacional = new StringBuilder();
                        StringBuilder strAcciones = new StringBuilder();
                        strDetalleSituacional.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");
                        strAcciones.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAcciones'>");
                        foreach (BE_MON_Ejecucion item in _ListAvance)
                        {

                            //armamos las viviendas
                            string TextViviendas = "";
                            if (item.Concluidas != null)
                                TextViviendas += " Concluidas: " + item.Concluidas.ToString() + ",";
                            if (item.EnEjecucion != null)
                                TextViviendas += " En ejecución: " + item.EnEjecucion.ToString() + ",";
                            if (item.PorIniciar != null)
                                TextViviendas += " Por iniciar: " + item.PorIniciar.ToString() + ",";
                            if (item.Paralizadas != null)
                                TextViviendas += " Paralizadas: " + item.Paralizadas.ToString() + ",";

                            if (TextViviendas.Length > 1)
                                TextViviendas = TextViviendas.Substring(0, TextViviendas.Length - 1) + ". ";

                            if (item.Evaluacion.Length > 0 | TextViviendas.Length > 0)
                            {
                                strDetalleSituacional.Append("<tr>");
                                strDetalleSituacional.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 15px;'><li>{0}: {1}</li></ul></td>", item.Date_fechaVisita.ToString("dd/MM/yyyy"), TextViviendas + item.Evaluacion.ToString()));
                                strDetalleSituacional.Append("</tr>");
                            }

                            if (item.Recomendacion.Length > 0)
                            {
                                strAcciones.Append("<tr>");
                                strAcciones.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 15px;'><li>{0}: {1}</li></ul></td>", item.Date_fechaVisita.ToString("dd/MM/yyyy"), item.Recomendacion.ToString()));
                                strAcciones.Append("</tr>");
                            }
                        }

                        strDetalleSituacional.Append("</table>");
                        lblDetalleSituacional.Text = strDetalleSituacional.ToString();
                        strAcciones.Append("</table>");
                        lblAcciones.Text = strAcciones.ToString();
                    }
                }
                else
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                    _BEEjecucion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());

                    DataTable dtAvanceFisico = new DataTable();
                    dtAvanceFisico = _objBLEjecucion.spMON_AvanceFisico(_BEEjecucion);

                    DataRow[] result = dtAvanceFisico.Select("flagAyuda = '1'");
                    if (result.Count() > 0)
                    {
                        StringBuilder strDetalleSituacional = new StringBuilder();
                        strDetalleSituacional.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");

                        foreach (DataRow row in result)
                        {
                            if (row["situacion"].ToString().Length > 0)
                            {
                                strDetalleSituacional.Append("<tr>");
                                strDetalleSituacional.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 15px;'><li>{0}</li></ul></td>", row["situacion"].ToString()));
                                strDetalleSituacional.Append("</tr>");
                            }
                        }

                        strDetalleSituacional.Append("</table>");
                        lblDetalleSituacional.Text = strDetalleSituacional.ToString();
                    }

                }


                int id_solicitudes = Convert.ToInt32(dt.Rows[0]["id_solicitudes"].ToString());

                //MUESTRA METAS

                int id_tipoPrograma = Convert.ToInt32(dt.Rows[0]["id_tipoPrograma"].ToString());
                Metas(id_solicitudes, id_tipoPrograma);

                //FORMATO DEL PNVR
                if (idPrograma == 5)
                {
                    lblhabitantes.Text = " FAMILIAS";

                    LblTextoCodigoSnip.Text = "N° DE CONVENIO";
                    lblSnip.Text = dt.Rows[0]["convenioContrato"].ToString();
                    LblTextoMontoTransferirNE.Text = "MONTO APORTE DE BENEFICIARIO:";
                    lblPorcentajeMontoBENEF_PMIB.Text = (Convert.ToDecimal(dt.Rows[0]["montoAprobado"]) - Convert.ToDecimal(dt.Rows[0]["monto_financiamientoPrograma"])).ToString("N2");
                    LblTextUnidadEjecutora.Text = "MODALIDAD DE EJECUCIÓN";
                    lblEjecutora.Text = "NÚCLEOS EJECUTORES";
                    TrCostoProyecto.Visible = false;
                    TrDesembolsos.Visible = false;
                    lblMetas.Text = dt.Rows[0]["poblacionSNIP"].ToString() + " VIVIENDAS";
                    lblAvanceFinanciero.Text = dt.Rows[0]["AvanceFinacieroReal"].ToString() + " %";

                    LblTextoAvanceFisicoAcumulado.Text = "Avance Físico Ejecutado Acumulado";
                    LblTextoAvanceFinancieroAcumulado.Text = "Avance Financiero Ejecutado Acumulado";

                }

            }
            else
            {
                lblTitle.Text = "NOTA: NO EXISTE INFORMACIÓN PARA MOSTRAR.";

                string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        }
                    

        private void Metas(int id, int iTipoPrograma)
        {
            int pIdProyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            DataSet ds = objBLProyecto.paSSP_SOL_rMetasAyudaMemoria(pIdProyecto, 2, 1); // 1: Calidad; 2:Monitoreo

            DataTable dtSistema = ds.Tables[0];
            DataTable dtMetas = ds.Tables[1];

            string htmlProgTotal = "";

            if (dtMetas.Rows.Count > 0)
            {
                foreach (DataRow rowSistema in dtSistema.Rows)
                {
                    string htmlProg = "";
                    foreach (DataRow rowP in dtMetas.Rows)
                    {
                        if (rowP["idSistema"].ToString().Equals(rowSistema["idSistema"].ToString()))
                        {
                            htmlProg = htmlProg +
                                       "<tr>" +
                                            "<td> <ul style = 'margin:0px;-moz-padding-start: 25px;' ><li>" + rowP["Componente"].ToString() + "</li></ul></td>" +
                                       "</tr>";
                        }
                    }

                    if (htmlProg.Length > 0)
                    {
                        htmlProgTotal = htmlProgTotal +
                                    "<table class='table'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        htmlProg +
                                    "</table>";
                    }


                }

                StringBuilder strMetas = new StringBuilder();
                strMetas.Append(htmlProgTotal);
                lblMetas.Text = strMetas.ToString();
            }
            else if (id > 0 && (iTipoPrograma == 1 || iTipoPrograma == 3 || iTipoPrograma == 7))
            {
                DataTable _obj;
                BEVarPnsu _objVarPnsu = new BEVarPnsu();
                _objVarPnsu.Id_Solicitudes = id;
                _obj = objBLProyecto.spSOL_Seguimiento_Variable_PNSU(_objVarPnsu);
                if (_obj.Rows.Count > 0)
                {
                    StringBuilder strMetasPNSU = new StringBuilder();
                    strMetasPNSU.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");

                    //AGUAR POTABLE
                    if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) == 0)
                    {
                    }
                    else
                    {
                        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Agua Potable</td></tr>");

                        if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) != 0)
                        {

                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Captación.</li></ul></td>", _obj.Rows[0]["a_captacion_nuc"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_conduccion_cacd"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_conduccion_nuc"].ToString());

                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de conducción.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_conduccion_cacd"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_conduccion_nuc"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua potable.</li></ul></td>", _obj.Rows[0]["a_trataPotable_num"].ToString(), _obj.Rows[0]["a_trataPotable_cap"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_impulsion_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_impulsion_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de impulsion.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Estación de bombeo.</li></ul></td>", _obj.Rows[0]["a_estacion_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Reservorio(s).</li></ul></td>", _obj.Rows[0]["a_reservorio_nur"].ToString(), _obj.Rows[0]["a_reservorio_carm"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_aduccion_nua"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_aduccion_caad"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de linea(s) de aducción.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["a_redes_nur"].ToString()) * Convert.ToDouble(_obj.Rows[0]["a_redes_card"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de  Redes de distribución.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucn"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucr"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Piletas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucp"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }



                    }

                    //ALCANTARILLADO
                    if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_planta_capl"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString()) == 0.00)
                    {
                    }
                    else
                    {
                        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Alcantarillado</td></tr>");

                        if (Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_efluente_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de linea(s) efluente de agua residual tratada.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua(s) residual.</li></ul></td>", _obj.Rows[0]["s_planta_nup"].ToString(), _obj.Rows[0]["s_planta_capl"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_agua_nua"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de emisor(s).</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_impulsion_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea(s) de impulsión.</li></ul></td>", dTotalMetas.ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Camara de Bombeo.</li></ul></td>", _obj.Rows[0]["s_camara_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["s_colectores_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de Redes de alcantarillado sanitario.</li></ul></td>", dTotalMetas.ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucn"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucr"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) básica de saneamiento.</li></ul></td>", _obj.Rows[0]["s_conexion_nuclo"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }
                    }

                    //DRENAJE PLUVIAL
                    if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) == 0 &&
                            Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString()) == 0.00 &&
                            Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString()) == 0.00)
                    {
                    }
                    else
                    {

                        strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Drenaje Pluvial</td></tr>");

                        if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_tuberia_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de tuberia de conexión.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_cuneta_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de cunetas.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de boca de tormenta.</li></ul></td>", _obj.Rows[0]["d_tormenta_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de conexión.</li></ul></td>", _obj.Rows[0]["d_conexion_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) != 0)
                        {
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de inspección.</li></ul></td>", _obj.Rows[0]["d_inspeccion_num"].ToString()));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_secundario_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de colector(s) secundario.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }

                        if (Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) != 0)
                        {
                            double dTotalMetas = Convert.ToDouble(_obj.Rows[0]["d_principal_num"].ToString()) * Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString());
                            strMetasPNSU.Append("<tr>");
                            strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de colector(s) principal.</li></ul></td>", dTotalMetas.ToString("N2")));
                            strMetasPNSU.Append("</tr>");
                        }
                    }

                    strMetasPNSU.Append("</table>");
                    lblMetas.Text = strMetasPNSU.ToString();


                }
            }
        }
  
        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();

            string pCodigo = lblSnip.Text.Replace("/", "_");
            string strnom = "AyudaMemoria_Codigo_" + pCodigo + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }
    }
}