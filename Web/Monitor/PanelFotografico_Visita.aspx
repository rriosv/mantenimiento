﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PanelFotografico_Visita.aspx.cs" Inherits="Web.Monitor.PanelFotografico_Visita" Culture="es-PE" UICulture="es-PE" Title="SSP" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/print.css" type="text/css" media="print" />
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        .dxgvFooter {
            white-space: nowrap;
            font-size: 10pt;
            font-family: Calibri;
            text-align: center;
        }

         .table2 {
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #a7a7a7 !important;
        }


        .table-bordered {
            border: 1px solid #a7a7a7;
        }

            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid #ddd;
            }

            .table-bordered > thead > tr > th,
            .table-bordered > thead > tr > td {
                border-bottom-width: 2px;
            }

            
        .alert-warning {
            color: rgb(169, 68, 66);
            background-color: rgb(242, 222, 222);
            border-color: rgb(235, 204, 209);
            padding: 15px 15px 20px;
            font-family: helvetica;
            border: 1px solid transparent;
            border-radius: 4px;
            width:30%;
            margin:auto;
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <center>
        <div id="div_Ficha" style="width: 800px; height: auto;">
   <center>
             
             <table style="margin:auto;margin-top:5px;margin-bottom:6px; font-family:Calibri" cellpadding="0" cellspacing="0" >
                
                 <tr>
                     <td style="text-align:center">
                         <asp:Label runat="server" Text="AÑO DE LA LUCHA CONTRA LA CORRUPCIÓN E IMPUNIDAD" Font-Size="11px" Font-Bold="true"></asp:Label><br />
                         <span style="font-family: Arial; font-size: 16pt;margin-top:20px"><b></b></span>
                         <div style="text-align:left">
                             <table style="text-align:left; margin-left:0" runat="server" visible="false">
                                 <tr><td><asp:Label runat="server" Text="Fecha :" ></asp:Label> </td>
                                     <td>
                                        <asp:Label runat="server"  Text="" ID="lblFechaAyuda" Font-Size="12px" ></asp:Label>
                                     </td></tr>
                             </table>
                         </div>
                     </td>
                 </tr>
                
             </table>
                 </center>

            <div style="width: 100%; left: 0">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13.5px; width: 100%; text-align:left" cellpadding="5">
                    <tr>
                        <td colspan="3" style="text-align: center">
                            <u>
                                <asp:Label ID="lblTituloInforme" runat="server" Text="PANEL FOTOGRAFICO" Font-Bold="true"></asp:Label>
                              
                            </u>
                        </td>
                    </tr>
                    <tr>
                        <td >Nombre del Proyecto</td>
                        <td>:</td>
                        <td style="width:75%">
                            <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label><br />
                            
                        </td>
                    </tr>
                    <tr>
                        <td>Código del Proyecto </td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                   
                    <tr>
                        <td>Fecha </td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblFechaInforme" runat="server" Text=""></asp:Label>
                            
                        </td>
                    </tr>
                   
                </table>
            </div>
             
            <div style="width: 100%; font-family:Calibri; font-size:13px; text-align:center"  >

              
                    <asp:Label runat="server" ID="lblFotos"></asp:Label>
            
            </div>

           
         

             <div id="icon_word">
                    <center>
                        <table>
                            <tr>
                            <td>
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                                
                            </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                        </table>
                    </center>

                </div>

            <br />
            <br />
             </div>
     </center>
    
    </form>
</body>
</html>
