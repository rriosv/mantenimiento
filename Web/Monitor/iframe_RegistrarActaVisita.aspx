﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iframe_RegistrarActaVisita.aspx.cs" Inherits="Web.Monitor.iframe_RegistrarActaVisita"  Culture="es-PE" UICulture="es-PE" MaintainScrollPositionOnPostback="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link href="../css/Monitoreo.css" rel="stylesheet" type="text/css" />
    <script src="../js/pace.min.js"></script>
    <title></title>
    <link href="../css/pace.flash.css" rel="stylesheet" />
    <style type="text/css">
        body {
            font-family:Calibri;
        }
       
    </style>

    <asp:PlaceHolder runat="server">
        <%:System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
                 
        <div>
            <asp:Label ID="LblID_USUARIO" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="LblID_PROYECTO" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lblCOD_SUBSECTOR" runat="server" Text="" Visible="false"></asp:Label>

              <asp:UpdatePanel runat="server" ID="Up_RegistroActaVisita" UpdateMode="Conditional">
                <ContentTemplate>
                   
                        <table style="font-size: 10pt;">
                     
                            <tr>
                                <td><b>Fecha Acta de Visita: </b>
                                    <asp:TextBox ID="txtFechaActavisita" runat="server" Width="80" placeholder="dd/mm/yyyy"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender261" runat="server" TargetControlID="txtFechaActavisita"
                                        FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                    <asp:CalendarExtender ID="CalendarExtender30" runat="server" TargetControlID="txtFechaActavisita" 
                                        Format="dd/MM/yyyy" >
                                    </asp:CalendarExtender>


                                </td>
                            </tr>

                            <tr runat="server" id="trCumplimientoInicio" visible="false">
                                <td>
                                    <b>Cumplimiento de los 5 requisitos necesarios para el inicio de obra: </b>
                                    <asp:CheckBoxList runat="server" ID="chbcumplimiento" RepeatDirection="Horizontal" CellPadding="8">
                                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>

                            <tr runat="server" id="trFuncionamiento1">
                                <td style="height: 20px"></td>
                            </tr>
                            <tr runat="server" id="trFuncionamiento2">
                                <td align="left" style="font-weight: bold">FUNCIONAMIENTO :</td>
                            </tr>
                            <tr runat="server" id="trFuncionamiento3">
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>
                            <tr runat="server" id="trFuncionamiento4">
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold; text-align: right">Funcionamiento :</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" RepeatDirection="Vertical" ID="chblTipoFuncionamientoActaVisita">
                                                    <asp:ListItem Text="Los sistemas se encuentran operativos y en funcionamiento" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Las obras no se encuentran operativas" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Los sistemas se encuentran operando con deficiencias" Value="3"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">Comentario :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtComentarioFuncionamientoActaVisita" runat="server" TextMode="MultiLine" Width="600px" Height="100px"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: bold; text-align: right">¿Inaugurada?:</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="chblFlagInaugurado">
                                                    <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr runat="server" id="trComponentePNSU1">
                                <td style="height: 10px"></td>
                            </tr>
                            <tr runat="server" id="trComponentePNSU2">
                                <td align="left" style="font-weight: bold">COMPONENTES DEL PROYECTO:</td>
                            </tr>
                            <tr runat="server" id="trComponentePNSU3">
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>
                            <tr runat="server" id="trComponentePNSU4">
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <asp:GridView runat="server" ID="grdSistemaDrenajePluvial" EmptyDataText="No hay información registrada"
                                                    ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                    CellPadding="2" CellSpacing="2" Width="750px" DataKeyNames="id_item"
                                                    OnSelectedIndexChanged="grdSistemaDrenajePluvial_SelectedIndexChanged" OnRowCommand="grdSistemaDrenajePluvial_RowCommand">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image" SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>

                                                        <asp:TemplateField HeaderText="Tipo" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTipoComponente" Text='<%# Eval("tipoEstado") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Componentes del Proyecto" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombreSistemaDrenaje" Text='<%# Eval("nombre") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Comentario" Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComentario" Text='<%# Eval("observacion") %>' runat="server" CssClass="tablaGrilla"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Eli." Visible="true">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                <asp:Label ID="lblId_tipoSistemaDrenaje" Text='<%# Eval("id_tabla") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="GridHeader2" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle Height="25px" Font-Size="13px" />
                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                </asp:GridView>
                                            </td>
                                            <td valign="top">
                                                <asp:ImageButton ID="imgbtnAgregarSistemaDrenaje" runat="server" ImageUrl="~/img/add.png"
                                                    onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                    Height="30px" AlternateText="Agregar" ToolTip="Agregar nuevo registro." OnClick="imgbtnAgregarSistemaDrenaje_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <asp:Panel ID="Panel_RegistroSistemaDrenajePluvial" Visible="false" runat="server">
                                                    <div style="padding: 5px 10px 5px 10px;" class="CuadrosEmergentes">
                                                        <table width="75%" class="tablaRegistro">
                                                            <tr>
                                                                <td colspan="4" align="center">
                                                                    <b>SELECCIONAR LOS COMPONENTES DEL PROYECTO VISITADO</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 10px"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Sistema :
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:DropDownList runat="server" ID="ddlTipoSistemaDrenaje"></asp:DropDownList>
                                                                    <asp:Label ID="lblIdEvaluacionRecomendacionPOPUP" runat="server" Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblIdSistemaDrenaje" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td align="right">Comentario :
                                                                </td>
                                                                <td align="left" colspan="3">

                                                                    <asp:TextBox ID="txtComentarioSistemaDrenaje" runat="server" Width="400px" TextMode="MultiLine" Height="60px"> </asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" align="left" style="padding-left: 30px;">
                                                                    <asp:Label ID="lblNomSistemaDrenaje" runat="server" Font-Size="10px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Button ID="btnGuardarSistemaDrenaje" runat="server" Text="Guardar" OnClick="btnGuardarSistemaDrenaje_Click" />
                                                                    <asp:Button ID="btnModificarSistemaDrenaje" runat="server" Text="Modificar" OnClick="btnModificarSistemaDrenaje_Click" />
                                                                    <asp:Button ID="btnCancelarSistemaDrenaje" runat="server" Text="Cancelar" OnClick="btnCancelarSistemaDrenaje_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </asp:Panel>

                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>

                           <%-- <tr runat="server" id="trComponentePMIB1" visible="false">
                                <td style="height: 10px"></td>
                            </tr>
                            <tr runat="server" id="trComponentePMIB2" visible="false">
                                <td align="left" style="font-weight: bold">OTROS ASUNTOS TECNICOS Y ADMINISTRATIVOS:</td>
                            </tr>
                            <tr runat="server" id="trComponentePMIB3" visible="false">
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>

                            <tr runat="server" id="trComponentePMIB4" visible="false">
                                <td align="left" style="border: 1px solid #c8c3c3;">
                                    <table width="98%">
                                        <tr style="border-bottom: 1px solid #c8c3c3">
                                            <td><b>Asunto</b></td>
                                            <td><b>SI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO</b></td>
                                            <td style="text-align: center"><b>Comentario</b></td>
                                        </tr>
                                        <tr>
                                            <td>1. Oficina y almacén de obra</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario1" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>2. Disponibilidad de materiales en condiciones adecuadas</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList2" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario2" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>3. Equipos en las cantidades y estado operativo requeridos</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList3" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario3" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>4. Cartel de obra instalado de acuerdo a modelo PMIB - MVCS</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList4" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario4" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>5. Presencia del Residente de Obra</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList5" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario5" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>6. Presencia del Supervisor de Obra</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList6" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario6" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>7. Copia del Expediente Técnico para uso en obra</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList7" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario7" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>8. Cuaderno de obra actualizado</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList8" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario8" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>9. Calendario de obra actualizado a la fecha de inicio</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList9" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario9" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>10. Empleo de implemento de seguridad para los trabajadores</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList10" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario10" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>11. Elementos de seguridad para la población (cercos, señales, etc.)</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList11" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario11" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>12. Cumplimientos de adecuados procesos constructivos</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList12" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario12" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>13. Cumplimientos del expediente técnico de obra</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList13" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario13" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>14. Presenta pruebas y controles de calidad reglamentarios</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList14" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario14" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>15. Cumpliemiento de procesos reglamentarios en casos adicionales o ampliaciones de plazo</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList15" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario15" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>16. Apreciación objetiva de la calidad de las obras ya realizadas</td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="RadioButtonList16" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtComentario16" Text="" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr runat="server" id="trDocumentoPMIB1" visible="false">
                                <td style="height: 10px"></td>
                            </tr>
                            <tr runat="server" id="trDocumentoPMIB2" visible="false">
                                <td align="left" style="font-weight: bold">CUMPLIMIENTO DE ENTREGA DE DOCUMENTOS:</td>
                            </tr>
                            <tr runat="server" id="trDocumentoPMIB3" visible="false">
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>
                            <tr runat="server" id="trDocumentoPMIB4" visible="false">
                                <td style="text-align: left">Relación de documento que se solicitaron y que no han sido entregados o no pudieron verificarse :
                                     <CKEditor:CKEditorControl ID="txtEntregaDocumentoActaVisita" runat="server" Width="98%" Height="200px" MaxLength="2000"
                                         ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                </td>
                            </tr>--%>

                            <tr>
                                <td style="height: 10px"></td>
                            </tr>

                            <tr>
                                <td align="left" style="font-weight: bold">OBSERVACIONES :</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <%--<asp:TextBox ID="txtObservacionesActaVisita" runat="server" TextMode="MultiLine" Width="98%" Height="200px"></asp:TextBox>--%>
                                    <CKEditor:CKEditorControl ID="txtObservacionesActaVisita" runat="server" Width="98%" Height="200px" MaxLength="2000"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-weight: bold">RECOMENDACIONES :</td>
                            </tr>
                            <tr>
                                <td>
                                    <%--<asp:TextBox ID="txtRecomendacionesActaVisita" runat="server" TextMode="MultiLine" Width="98%" Height="200px"></asp:TextBox>--%>
                                    <CKEditor:CKEditorControl ID="txtRecomendacionesActaVisita" runat="server" Width="98%" Height="200px" MaxLength="8"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNomUsuarioActaVisita" runat="server" Font-Size="10px"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btnGuardarRecomendacionActaVisita" runat="server" Text="Guardar" OnClick="btnGuardarRecomendacionActaVisita_Click" />
                                    <asp:Button ID="btnGenerarActaVisitaPopup" runat="server" Text="Generar Acta Visita" OnClick="btnGenerarActaVisitaPopup_Click" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                  
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>

    <script type="text/javascript">
        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }
    </script>
</body>
</html>