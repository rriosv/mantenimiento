﻿using Business;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Asignacion : System.Web.UI.Page
    {
        private BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        private BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();
        private DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargaUbigeo(ddlDepa, "1", null, null, null);
                ddlDepa.SelectedValue = "15"; //LIMA
                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                ddlprov.SelectedValue = "01"; //LIMA
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
                cargaSector();
                if (Session["CodSubsector"].ToString() == "1" || Session["CodSubsector"].ToString() == "2")
                {
                    ddlSubsector.Enabled = false;
                    ddlSubsector.SelectedValue = Session["CodSubsector"].ToString();
                }
                //cargaTecnicos();
                cargaTipo();

                cargaFuncion(ddlTipoAgente1, 1);
                cargaFuncion(ddlTipoAgente2, 1);
                cargaFuncion(ddlTipoAgente3, 1);
                cargaFuncion(ddlTipoAgente4, 1);
                cargaFuncion(ddlTipoAgente5, 1);

                cargaTecnico(ddlTecnico, "1", ddlSubsector.SelectedValue);

                //cargaNoAsignados(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtanno.Text, ddlModalidadFinanciamiento.SelectedValue);
                //cargaAsignados(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtanno.Text, ddlModalidadFinanciamiento.SelectedValue);
            }

            if (RbFiltro.SelectedValue == "COD")
            {
                TxtNombreProyecto.Visible = false;
                TxtSnipFiltro.Visible = true;
            }
            else if (RbFiltro.SelectedValue == "PRY")
            {
                TxtNombreProyecto.Visible = true;
                TxtSnipFiltro.Visible = false;
            }

            //string scriptSelect = "<script>if(typeof(applySelect2)=='function'){applySelect2('.ddl-filter');}</script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", scriptSelect, false);
        }

        //protected void cargaTecnicos()
        //{
        //    //cargaTecnico(ddlTecnico);
        //    //cargaTecnico(ddlTecObl);
        //    //cargaTecnico(ddlTecOpc1);
        //    //cargaTecnico(ddlTecOpc2);
        //    //cargaTecnico(ddlTecOpc3);
        //    //cargaTecnico(ddlTecOpc4);

        //    cargaTecnico(ddlNombreAgente1);
        //    cargaTecnico(ddlNombreAgente2);
        //    cargaTecnico(ddlNombreAgente3);
        //    cargaTecnico(ddlNombreAgente4);
        //    cargaTecnico(ddlNombreAgente5);

        //    Up_Agente.Update();
        //}

        protected void grdAsignados_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAsignados.PageIndex = e.NewPageIndex;
            CargarGrilla(2);

            Up_grdAsignado.Update();
        }

        protected void gvPryNoAsignado_DataBound(object sender, EventArgs e)
        {
            int currentPage = gvPryNoAsignado.PageIndex + 1;
            string paginado = " (Pagina " + currentPage.ToString() +
                " de " + gvPryNoAsignado.PageCount.ToString() + ")";
            lblPaginado.Text = paginado;
        }

        protected void grdAsignados_OnDataBound(object sender, EventArgs e)
        {
            int currentPage = grdAsignados.PageIndex + 1;
            string paginado = " (Pagina " + currentPage.ToString() +
                    " de " + grdAsignados.PageCount.ToString() + ")";
            lblPaginadoAsignado.Text = paginado;
        }

        protected void gvPryNoAsignado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPryNoAsignado.PageIndex = e.NewPageIndex;
            CargarGrilla(2);

            Up_grdNoAsignado.Update();
        }

        protected void CargarGrilla(int estadoProyecto)
        {
            if (RbFiltro.SelectedValue == "COD")
            {
                _BE_Bandeja.snip = TxtSnipFiltro.Value;
                _BE_Bandeja.nombProyecto = "";
            }
            else if (RbFiltro.SelectedValue == "PRY")
            {
                _BE_Bandeja.snip = "";
                _BE_Bandeja.nombProyecto = TxtNombreProyecto.Text;
            }

            _BE_Bandeja.depa = ddlDepa.SelectedValue;
            _BE_Bandeja.prov = ddlprov.SelectedValue;
            _BE_Bandeja.dist = ddldist.SelectedValue;
            _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            _BE_Bandeja.Anio = txtanno.Text;
            _BE_Bandeja.estadoProyecto = estadoProyecto;
            _BE_Bandeja.modalidadFinanciamiento = ddlModalidadFinanciamiento.SelectedValue;
            _BE_Bandeja.Subsector = ddlSubPrograma.SelectedValue;

            dt = objBLbandeja.spMON_BandejaCoordinador(_BE_Bandeja);

            if (estadoProyecto == 1)
            {
                gvPryNoAsignado.DataSource = dt;
                gvPryNoAsignado.DataBind();
                LabelTotalNoAsignado.Text = "Total Sin Asignar: " + dt.Rows.Count.ToString();
            }
            else if (estadoProyecto == 2)
            {
                grdAsignados.DataSource = dt;
                grdAsignados.DataBind();
                LabeltotalAsignado.Text = "Total Asignados: " + dt.Rows.Count.ToString();
            }
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void cargaSector()
        {
            List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();
            list = objBLbandeja.F_spMON_TipoPrograma();

            ddlSubsector.DataSource = list;
            ddlSubsector.DataTextField = "nombre";
            ddlSubsector.DataValueField = "valor";
            ddlSubsector.DataBind();
            ddlSubsector.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlPrograma1.DataSource = list;
            ddlPrograma1.DataTextField = "nombre";
            ddlPrograma1.DataValueField = "valor";
            ddlPrograma1.DataBind();
            ddlPrograma1.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlPrograma2.DataSource = list;
            ddlPrograma2.DataTextField = "nombre";
            ddlPrograma2.DataValueField = "valor";
            ddlPrograma2.DataBind();
            ddlPrograma2.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlPrograma3.DataSource = list;
            ddlPrograma3.DataTextField = "nombre";
            ddlPrograma3.DataValueField = "valor";
            ddlPrograma3.DataBind();
            ddlPrograma3.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlPrograma4.DataSource = list;
            ddlPrograma4.DataTextField = "nombre";
            ddlPrograma4.DataValueField = "valor";
            ddlPrograma4.DataBind();
            ddlPrograma4.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlPrograma5.DataSource = list;
            ddlPrograma5.DataTextField = "nombre";
            ddlPrograma5.DataValueField = "valor";
            ddlPrograma5.DataBind();
            ddlPrograma5.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void cargaTipo()
        {
            ddlTipo.DataSource = objBLbandeja.F_spMON_TipoFinanciamiento();
            ddlTipo.DataTextField = "nombre";
            ddlTipo.DataValueField = "valor";
            ddlTipo.DataBind();

            ddlTipo.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void cargaTecnico(DropDownList ddl, string tipoAgente, string id_programa)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarTecnicoxTipo(id_programa, tipoAgente);
            ddl.DataTextField = "tecnico";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void cargaFuncion(DropDownList ddl, int tipoAgente)
        {
            ddl.DataSource = objBLbandeja.F_spMON_TipoAgente(tipoAgente);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void btnAsignar_OnClick(object sender, EventArgs e)
        {
            ddlTipoAgente1.SelectedValue = "1";
            ddlPrograma1.SelectedValue = ddlSubsector.SelectedValue;
            cargaTecnico(ddlNombreAgente1, ddlTipoAgente1.SelectedValue, ddlPrograma1.SelectedValue);
            int count = 0;

            foreach (GridViewRow grvrom in gvPryNoAsignado.Rows)
            {
                CheckBox chkbNoAsignados = (CheckBox)grvrom.FindControl("chkbNoAsignados");

                if (chkbNoAsignados.Checked == true)
                {
                    count = count + 1;
                }
            }

            Up_Filtro.Update();

            if (count == 0)
            {
                string script = "<script>alert('Debe seleccionar un proyecto para asignar sus agentes.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script789", script, false);
                return;
            }
            else
            {
                lblCantAgente.Text = "Usted marco " + count.ToString() + " proyecto(s) para seleccionar sus agentes";

               
                Up_Agente.Update();
              
                string script = "<script>$('#modalTecnico').modal('show');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script919", script, false);
            }

           
        }

        protected void ddlDepa_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

            Up_Filtro.Update();
            //cargaBandeja();
        }

        protected void ddlprov_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

            Up_Filtro.Update();
            //cargaBandeja();
        }

        //protected void ddldist_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    cargaBandeja();
        //}
        protected void btnNoAsignar_OnClick(object sender, EventArgs e)
        {
            foreach (GridViewRow grvrom in grdAsignados.Rows)
            {
                CheckBox chkbAsignados = (CheckBox)grvrom.FindControl("chkbAsignados");
                if (chkbAsignados.Checked == true)
                {
                    _BE_Bandeja.id_proyecto = Convert.ToInt32(grdAsignados.DataKeys[grvrom.RowIndex].Values[0]);
                    _BE_Bandeja.tipo = "2";
                    _BE_Bandeja.id_tecnico = 9999999;
                    _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                    int valor = objBLbandeja.spi_MON_AsignarProyecto(_BE_Bandeja);
                }
            }

            CargarGrilla(1);
            CargarGrilla(2);

            //  cargaTecnicos();
            Up_Filtro.Update();

            Up_grdNoAsignado.Update();
            Up_grdAsignado.Update();
        }

        protected Boolean validaAsignar()
        {
            Boolean result;
            result = true;

            //if (ddlTecObl.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione Técnico Obligatorio.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;
        }

        protected void btnBuscar_OnClick(object sender, EventArgs e)
        {
            if (txtanno.Text != "")
            {
                if (!(Convert.ToInt32(txtanno.Text) >= 1900 && Convert.ToInt32(txtanno.Text) <= DateTime.Now.Year))
                {
                    string script = "<script>alert('Rango de Año es de 1900 hasta la actualidad');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return;
                }
            }
            cargaBandeja();
        }

        protected void cargaBandeja()
        {
            CargarGrilla(1);
            CargarGrilla(2);

            Up_grdNoAsignado.Update();
            Up_grdAsignado.Update();
        }

        protected void btnLimpiar_OnClick(object sender, EventArgs e)
        {
            TxtSnipFiltro.Value = "";
            TxtNombreProyecto.Text = "";
            ddlDepa.SelectedValue = "";
            ddlprov.SelectedValue = "";
            ddldist.SelectedValue = "";
            txtanno.Text = "";
            ddlTecnico.SelectedValue = "";
            ddlTipo.SelectedValue = "";
            ddlModalidadFinanciamiento.SelectedValue = "";
            Up_Filtro.Update();

            gvPryNoAsignado.DataSource = null;
            gvPryNoAsignado.DataBind();
            LabelTotalNoAsignado.Text = "";

            grdAsignados.DataSource = null;
            grdAsignados.DataBind();
            LabeltotalAsignado.Text = "";

            Up_grdNoAsignado.Update();
            Up_grdAsignado.Update();
        }

        protected void ddlSubsector_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlTecnico, "1", ddlSubsector.SelectedValue);
            Up_Filtro.Update();
            Up_grdNoAsignado.Update();
            //cargaBandeja();

            //CARGAR LOS SUBPROGRAMAS
            cargaSubPrograma();

            //if (ddlSubsector.SelectedValue == "3" || ddlSubsector.SelectedValue == "10" || ddlSubsector.SelectedValue == "1" || ddlSubsector.SelectedValue == "7") //PNSR - DGPPCS - PNSU - PASLC
            //{
            //    tdSub1.Visible = true;
            //    tdSub2.Visible = true;
            //}
            //else
            //{
            //    tdSub1.Visible = false;
            //    tdSub2.Visible = false;
            //}
        }

        protected void cargaSubPrograma()
        {
            int iPrograma = 0;

            if (ddlSubsector.SelectedValue != "")
            {
                iPrograma = Convert.ToInt32(ddlSubsector.SelectedValue);
            }
            ddlSubPrograma.DataSource = objBLbandeja.F_spMON_TipoSubPrograma(iPrograma); //SOLO PNSR Y PNSU TIENE SUB PROGRAMAS
            ddlSubPrograma.DataTextField = "nombre";
            ddlSubPrograma.DataValueField = "valor";
            ddlSubPrograma.DataBind();

            ddlSubPrograma.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        //protected void ddlTecnico_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    cargaBandeja();
        //}

        //protected void ddlTipo_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    cargaBandeja();
        //}

        private void EnviarCorreo()
        {
            BLUtil _objBLUtil = new BLUtil();
            string script;
            try
            {
                string cuerpo = "";

                MailMessage mail = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                /**********************CORREO DE COORDINADOR A TECNICO**************************/

                _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente1.SelectedValue);
                _BE_Bandeja.tipo = "1";
                mail.To.Add(objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja).ElementAt(0).correo.ToString());

                if (ddlTipoAgente2.SelectedValue != "" && ddlNombreAgente2.SelectedValue != "")
                {
                    _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente2.SelectedValue);
                    _BE_Bandeja.tipo = "1";
                    mail.To.Add(objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja).ElementAt(0).correo.ToString());
                }

                if (ddlTipoAgente3.SelectedValue != "" && ddlNombreAgente3.SelectedValue != "")
                {
                    _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente3.SelectedValue);
                    _BE_Bandeja.tipo = "1";
                    mail.To.Add(objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja).ElementAt(0).correo.ToString());
                }

                if (ddlTipoAgente4.SelectedValue != "" && ddlNombreAgente4.SelectedValue != "")
                {
                    _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente4.SelectedValue);
                    _BE_Bandeja.tipo = "1";
                    mail.To.Add(objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja).ElementAt(0).correo.ToString());
                }

                if (ddlTipoAgente5.SelectedValue != "" && ddlNombreAgente5.SelectedValue != "")
                {
                    _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente5.SelectedValue);
                    _BE_Bandeja.tipo = "1";
                    mail.To.Add(objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja).ElementAt(0).correo.ToString());
                }

                //_BE_Bandeja.tipo = "2";
                //_BE_Bandeja.sector = ddlSubsector.SelectedValue;
                // DataTable dtCordi = new DataTable();
                //dtCordi=objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja);

                //foreach (BE_MON_BANDEJA dr in objBLbandeja.F_spMON_ObtenerCorreo(_BE_Bandeja))
                //{
                //    mail.CC.Add(dr.correo.ToString());

                //}

                cuerpo = cuerpo + "Ing. Se le deriva el siguiente proyecto(s) para su monitoreo: </br></br>";

                mail.Subject = "ASIGNACIÓN PARA MONITOREO DE PROYECTO(S) - SSP";

                mail.CC.Add(new MailAddress(_objBLUtil.spSOL_Informacion(10, Convert.ToInt32(Session["IdUsuario"]), null, 0).Rows[0]["correo"].ToString()));
                /*************ASIGNACION DE PROYECTOS**********/

                cuerpo = cuerpo + "<table border=1 cellspacing=1 cellpadding=1 style='font-size:14px'>";

                cuerpo = cuerpo + "<tr><td align=center><strong>SNIP</strong></td><td align=center><strong>NOMBRE PROYECTO</strong></td><td align=center><strong>TIPO</strong></td></tr>";

                DataTable dtproy;
                dtproy = (DataTable)Session["dt"];

                if (dtproy.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtproy.Rows)
                    {
                        cuerpo = cuerpo + "<tr>";
                        cuerpo = cuerpo + "<td>" + dr["cod_snip"].ToString() + "</td>";
                        cuerpo = cuerpo + "<td>" + dr["nom_proyecto"].ToString() + "</td>";
                        cuerpo = cuerpo + "<td>" + dr["tipoFinanciamiento"].ToString() + "</td>";

                        cuerpo = cuerpo + "</tr>";
                    }
                }

                cuerpo = cuerpo + "</table></br></br></br>";

                cuerpo = cuerpo + "<Table style='font-size:12px; font-style:italic'><tr><td >Sistema de Seguimiento de Proyectos - SSP</td></tr><tr><td>Oficina General de Estadística e Informática - OGEI</td></tr></Table>";

                /*********************************************/
                mail.Body = cuerpo;
                mail.IsBodyHtml = true;
                smtp.Send(mail);
            }
            catch
            {
                script = "<script>alert('Error no se envio el correo verificar');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                //return false;
            }
        }

        protected void btnRegistrarAgente_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("cod_snip");
            dt.Columns.Add("nom_proyecto");
            dt.Columns.Add("tipoFinanciamiento");

            foreach (GridViewRow grvrom in gvPryNoAsignado.Rows)
            {
                CheckBox chkbNoAsignados = (CheckBox)grvrom.FindControl("chkbNoAsignados");

                if (chkbNoAsignados.Checked == true)
                {
                    Label lblsnip = (Label)grvrom.FindControl("cod_snip");
                    Label nom_proyecto = (Label)grvrom.FindControl("nom_proyecto");
                    Label tipoFinanciamiento = (Label)grvrom.FindControl("lblFinanciamiento");

                    dt.Rows.Add(lblsnip.Text, nom_proyecto.Text, tipoFinanciamiento.Text);
                    Session["dt"] = dt;

                    _BE_Bandeja.id_proyecto = Convert.ToInt32(gvPryNoAsignado.DataKeys[grvrom.RowIndex].Values[0]);
                    _BE_Bandeja.tipo = "1";
                    _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente1.SelectedValue);
                    _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                    int valor = objBLbandeja.spi_MON_AsignarProyecto(_BE_Bandeja);

                    if (ddlTipoAgente2.SelectedValue != "" && ddlNombreAgente2.SelectedValue != "")
                    {
                        _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente2.SelectedValue);
                        valor = objBLbandeja.spi_MON_AsignarProyecto(_BE_Bandeja);
                    }

                    if (ddlTipoAgente3.SelectedValue != "" && ddlNombreAgente3.SelectedValue != "")
                    {
                        _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente3.SelectedValue);
                        valor = objBLbandeja.spi_MON_AsignarProyecto(_BE_Bandeja);
                    }

                    if (ddlTipoAgente4.SelectedValue != "" && ddlNombreAgente4.SelectedValue != "")
                    {
                        _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente4.SelectedValue);
                        valor = objBLbandeja.spi_MON_AsignarProyecto(_BE_Bandeja);
                    }

                    if (ddlTipoAgente5.SelectedValue != "" && ddlNombreAgente5.SelectedValue != "")
                    {
                        _BE_Bandeja.id_tecnico = Convert.ToInt32(ddlNombreAgente5.SelectedValue);
                        valor = objBLbandeja.spi_MON_AsignarProyecto(_BE_Bandeja);
                    }
                }
            }

            CargarGrilla(1);
            CargarGrilla(2);

            EnviarCorreo();
            //cargaTecnicos();
            Up_Filtro.Update();

            
            Up_grdNoAsignado.Update();
            Up_grdAsignado.Update();

            string script = "<script>$('#modalTecnico').modal('hide');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script99", script, false);
        }

        protected void ddlTipoAgente1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente1, ddlTipoAgente1.SelectedValue, ddlPrograma1.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlTipoAgente2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente2, ddlTipoAgente2.SelectedValue, ddlPrograma2.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlTipoAgente3_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente3, ddlTipoAgente3.SelectedValue, ddlPrograma3.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlTipoAgente4_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente4, ddlTipoAgente4.SelectedValue, ddlPrograma4.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlTipoAgente5_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente5, ddlTipoAgente5.SelectedValue, ddlPrograma5.SelectedValue);
            Up_Agente.Update();
        }

        //protected void ddlModalidadFinanciamiento_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    cargaBandeja();
        //}

        protected void ddlPrograma5_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente5, ddlTipoAgente5.SelectedValue, ddlPrograma5.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlPrograma3_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente3, ddlTipoAgente3.SelectedValue, ddlPrograma3.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlPrograma2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente2, ddlTipoAgente2.SelectedValue, ddlPrograma2.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlPrograma1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente1, ddlTipoAgente1.SelectedValue, ddlPrograma1.SelectedValue);
            Up_Agente.Update();
        }

        protected void ddlPrograma4_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaTecnico(ddlNombreAgente4, ddlTipoAgente4.SelectedValue, ddlPrograma4.SelectedValue);
            Up_Agente.Update();
        }

        protected void RbFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtNombreProyecto.Text = "";
            TxtSnipFiltro.Value = "";
            if (RbFiltro.SelectedValue == "COD")
            {
                TxtNombreProyecto.Visible = false;
                TxtSnipFiltro.Visible = true;
            }
            else if (RbFiltro.SelectedValue == "PRY")
            {
                TxtNombreProyecto.Visible = true;
                TxtSnipFiltro.Visible = false;
            }
        }
    }
}