﻿using System;
using System.Collections.Generic;
using Business;
using Entity;

namespace Web.Monitor
{
    public partial class ConsultaPersonalNE : System.Web.UI.Page
    {
        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text.Replace(" ", "");

            if (txtSearch.Text.Length < 8)
            {
                //int decimalLength = Convert.ToInt32(txtSearch.Text).ToString("D").Length + 8;

                if (txtSearch.Text.Length < 1)
                {
                    txtSearch.Text = "0";
                }

                string fmt = "00000000.##";
                string formatString = " {0,15:" + fmt + "}";
                txtSearch.Text = Convert.ToInt32(txtSearch.Text).ToString(fmt);
            }

            List<BE_MON_Ejecucion> ListaAgenteAsignados = new List<BE_MON_Ejecucion>();
            _BEEjecucion.DNI = txtSearch.Text;

            ListaAgenteAsignados = _objBLEjecucion.F_MON_PersonalNE(_BEEjecucion);

            grdxPersonalNE.DataSource = ListaAgenteAsignados;
            grdxPersonalNE.DataBind();
        }
    }
}