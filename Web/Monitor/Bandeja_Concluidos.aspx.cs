﻿using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace Web.Monitor
{
    public partial class Bandeja_Concluidos : System.Web.UI.Page
    {
        private Hashtable _sheduleData;

        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();

        BEProyecto _BEProyecto = new BEProyecto();
        BLProyecto objBLProyecto = new BLProyecto();

        BLUtil _Metodo = new BLUtil();
        DataTable dt;


        BLAccionesDiarias objAccionesDiarias = new BLAccionesDiarias();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                cargarCalendario("156");
                _sheduleData = GetSchedule();
                UP_calendar.Update();

                cargaUbigeo(ddlDepa, "1", null, null, null);
                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
                cargaSector();

                if (Session["CodSubsector"].ToString() == "1" || Session["CodSubsector"].ToString() == "2")
                {
                    ddlSubsector.Enabled = false;
                    ddlSubsector.SelectedValue = Session["CodSubsector"].ToString();

                }



                cargaBandejaConcluidos(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlGestion.SelectedValue);
            }

            _sheduleData = GetSchedule();

        }

        #region FILTRO

        protected void cargaSector()
        {
            ddlSubsector.DataSource = objBLbandeja.F_spMON_TipoPrograma();
            ddlSubsector.DataTextField = "nombre";
            ddlSubsector.DataValueField = "valor";
            ddlSubsector.DataBind();

            ddlSubsector.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void ddlDepa_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

            Up_Filtro.Update();
            cargaBandeja();
        }

        protected void ddlprov_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

            Up_Filtro.Update();
            cargaBandeja();
        }

        protected void ddldist_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaBandeja();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {


            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void ddlSubsector_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // cargaTecnico(ddlTecnico);
            cargaBandejaConcluidos(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlGestion.SelectedValue);

            Up_grdBandeja.Update();
        }

        protected void btnBuscar_OnClick(object sender, EventArgs e)
        {
            cargaBandeja();

        }

        protected void cargaBandeja()
        {
            cargaBandejaConcluidos(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlGestion.SelectedValue);

            Up_grdBandeja.Update();
        }



        protected void ddlGestion_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaBandeja();
        }
        protected void ddlTecnico_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaBandeja();
        }


        protected void ChkboxConcluido_OnCheckedChanged(object sender, EventArgs e)
        {

        }
        protected void btnLimpiar_OnClick(object sender, EventArgs e)
        {
            txtSnipFiltro.Text = "";
            txtProyectoFiltro.Text = "";
            ddlDepa.SelectedValue = "";
            ddlprov.SelectedValue = "";
            ddldist.SelectedValue = "";
            if (ddlSubsector.Enabled == true)
            { ddlSubsector.SelectedValue = ""; }

            ddlGestion.SelectedValue = "";
            ChkboxConcluido.Checked = false;
            Up_Filtro.Update();

            cargaBandejaConcluidos(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlGestion.SelectedValue);
            Up_grdBandeja.Update();
        }

        protected void ddlInauguración_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaBandeja();
        }
        #endregion

        protected void cargaBandejaConcluidos(string snip, string proyecto, string depa, string prov, string dist, string sector, string Anio)
        {
            _BE_Bandeja.snip = snip;
            _BE_Bandeja.nombProyecto = proyecto;
            _BE_Bandeja.depa = depa;
            _BE_Bandeja.prov = prov;
            _BE_Bandeja.dist = dist;
            _BE_Bandeja.sector = sector;
            _BE_Bandeja.Anio = Anio;

            _BE_Bandeja.tipo = ChkboxConcluido.Checked == true ? "1" : "0";

            // _BE_Bandeja.tipoFinanciamientoFiltro = tipo;
            List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();
            ListBandeja = objBLbandeja.F_spMON_BandejaConcluidos(_BE_Bandeja);

            if (ddlInauguración.SelectedValue == "1")
            {
                DateTime fechaHoy = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));

                ListBandeja = (from cust in ListBandeja
                               where cust.dtFechaInauguracion <= fechaHoy
                               select cust).ToList();
            }

            if (ddlInauguración.SelectedValue == "2")
            {
                DateTime fechaHoy = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));

                ListBandeja = (from cust in ListBandeja
                               where cust.dtFechaInauguracion > fechaHoy
                               select cust).ToList();
            }
            grdBandeja.DataSource = ListBandeja;
            grdBandeja.DataBind();
            LabeltotalAsignado.Text = "Total de Proyectos: " + ListBandeja.Count.ToString();
        }

        protected void grdBandeja_OnDataBound(object sender, EventArgs e)
        {

            int currentPage = grdBandeja.PageIndex + 1;
            string paginado = " (Pagina " + currentPage.ToString() +
                    " de " + grdBandeja.PageCount.ToString() + ")";
            lblPaginadoAsignado.Text = paginado;


        }


        protected void grdBandeja_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdBandeja.PageIndex = e.NewPageIndex;
            cargaBandejaConcluidos(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlGestion.SelectedValue);
            Up_grdBandeja.Update();

        }


        protected void cCalChanged(object sender, EventArgs e)
        {


        }

        private Hashtable GetSchedule()
        {
            Hashtable schedule = new Hashtable();

            for (int i = 0; i <= lb_Acciones.Items.Count - 1; i++)
            {
                string value = lb_Acciones.Items[i].Text;
                schedule[value] = lb_Acciones.Items[i].Text;
            }


            return schedule;

        }
        protected void DayRender(object sender, DayRenderEventArgs e)
        {
            if (_sheduleData[e.Day.Date.ToShortDateString()] != null)
            {
                Literal lit = new Literal();
                lit.Text = "</br>";
                e.Cell.Controls.Add(lit);


                //HtmlImage im = new HtmlImage();
                //im.Src = "note1.png";
                //im.Alt = "Cordinación";
                //e.Cell.Controls.Add(im);

                Label lbl = new Label();
                string fecha = (string)_sheduleData[e.Day.Date.ToShortDateString()];
                DataTable dt;
                dt = objBLbandeja.spMON_InauguracionCalendario(fecha);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        lbl.Text = lbl.Text + "</br>" + dr["dato"].ToString();
                    }

                }
                //lbl.Text = "</br>1)12:00-16:00 - SNIP 456238";
                lbl.ForeColor = System.Drawing.Color.Blue;
                e.Cell.Controls.Add(lbl);

                //e.Cell.Controls.Add(lit);

                //Label lbl2 = new Label();
                // lbl.Text = (string)_sheduleData[e.Day.Date.ToShortDateString()];
                //lbl2.Text = "2)19:00-16:00 - SNIP 456238 ";
                //lbl2.ForeColor = System.Drawing.Color.Blue;
                //e.Cell.Controls.Add(lbl2);

            }


        }

        private void cargarCalendario(string SNIP)
        {

            dt = objBLbandeja.spMON_InauguracionAll();
            lb_Acciones.DataSource = dt;
            lb_Acciones.DataValueField = "id_inauguracion";
            lb_Acciones.DataTextField = "fechaInauguracion";
            lb_Acciones.DataBind();

            //lb_Acciones.Items.Insert(0, new ListItem("06/01/2014", "1"));
            //lb_Acciones.Items.Insert(0, new ListItem("08/01/2014", "2"));
        }

        protected void imgbtnRequisito_OnClick(object sender, EventArgs e)
        {

            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            //  string id = grvproy.DataKeys[row.RowIndex].Values[0].ToString();
            lblIdProyecto.Text = grdBandeja.DataKeys[row.RowIndex].Values[0].ToString();
            Label nombreProyecto = (Label)grdBandeja.Rows[row.RowIndex].FindControl("nom_proyecto");
            Label unidad_ejecutora = (Label)grdBandeja.Rows[row.RowIndex].FindControl("unidad_ejecutora");
            Label cod_snip = (Label)grdBandeja.Rows[row.RowIndex].FindControl("cod_snip");
            Label nom_depa = (Label)grdBandeja.Rows[row.RowIndex].FindControl("nom_depa");
            Label lblprov = (Label)grdBandeja.Rows[row.RowIndex].FindControl("lblprov");
            Label lbldist = (Label)grdBandeja.Rows[row.RowIndex].FindControl("lbldist");
            Label lblID_Sol = (Label)grdBandeja.Rows[row.RowIndex].FindControl("lblID_Solicitud");


            lblNombreProyecto.Text = nombreProyecto.Text;
            lblsnip.Text = cod_snip.Text;
            lblejecutora.Text = unidad_ejecutora.Text;

            DataTable dtUE = new DataTable();
            dtUE = objBLProyecto.spSOL_obtieneDirectorioUE(lblejecutora.Text, Convert.ToInt32(lblID_Sol.Text));

            if (dtUE.Rows.Count > 0)
            {
                Panel_UE_Informacion.Visible = true;
                lblCargoUE.Text = dtUE.Rows[0]["cargo"].ToString();
                lblEncargadoUE.Text = dtUE.Rows[0]["encargado_UE"].ToString();
                lblDireccionUE.Text = dtUE.Rows[0]["direccion"].ToString();
                lblCodigoPostal.Text = dtUE.Rows[0]["ddn"].ToString();
                lbltelefonoUE.Text = dtUE.Rows[0]["telefono"].ToString();
                lblFaxUE.Text = dtUE.Rows[0]["fax"].ToString();
                lblCorreoUE.Text = dtUE.Rows[0]["correo"].ToString();

            }

            cargaTipoInagura();


            _sheduleData = GetSchedule();

            Calendar1.FirstDayOfWeek = FirstDayOfWeek.Sunday;
            Calendar1.NextPrevFormat = NextPrevFormat.FullMonth;
            Calendar1.TitleFormat = TitleFormat.MonthYear;
            Calendar1.ShowGridLines = true;
            //     Calendar1.DayStyle.HorizontalAlign = HorizontalAlign.Left;
            Calendar1.DayStyle.VerticalAlign = VerticalAlign.Top;
            Calendar1.DayStyle.Height = new Unit(55);
            Calendar1.DayStyle.Width = new Unit(120);
            Calendar1.OtherMonthDayStyle.BackColor = System.Drawing.Color.Cornsilk;
            // Calendar1.DayRender += new DayRenderEventHandler(this.DayRender);

            UP_calendar.Update();

            MPE_Requisitos.Show();

            //string script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            // Up_Fondo.Update();

            cargaProyinauguracion(lblIdProyecto.Text);

        }

        protected void btnReporteResumen_OnClick(object sender, EventArgs e)
        {

            _BE_Bandeja.snip = txtSnipFiltro.Text;
            _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            _BE_Bandeja.depa = ddlDepa.SelectedValue;
            _BE_Bandeja.prov = ddlprov.SelectedValue;
            _BE_Bandeja.dist = ddldist.SelectedValue;
            _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            _BE_Bandeja.Anio = ddlGestion.SelectedValue;
            _BE_Bandeja.tipo = ChkboxConcluido.Checked == true ? "1" : "0";

            // _BE_Bandeja.tipoFinanciamientoFiltro = tipo;
            List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();
            ListBandeja = objBLbandeja.F_spMON_Reporte_BandejaConcluidos(_BE_Bandeja);

            if (ddlInauguración.SelectedValue == "1")
            {
                DateTime fechaHoy = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));

                ListBandeja = (from cust in ListBandeja
                               where cust.dtFechaInauguracion <= fechaHoy
                               select cust).ToList();
            }

            if (ddlInauguración.SelectedValue == "2")
            {
                DateTime fechaHoy = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));

                ListBandeja = (from cust in ListBandeja
                               where cust.dtFechaInauguracion > fechaHoy
                               select cust).ToList();
            }

            GridView grd = new GridView();

            grd.Columns.Add(new BoundField { DataField = "id_proyecto" });
            grd.Columns.Add(new BoundField { DataField = "sector" });
            grd.Columns.Add(new BoundField { DataField = "Anio" });
            grd.Columns.Add(new BoundField { DataField = "snip" });
            grd.Columns.Add(new BoundField { DataField = "nombProyecto" });
            grd.Columns.Add(new BoundField { DataField = "monto_inversion" });
            grd.Columns.Add(new BoundField { DataField = "monto_transferencia" });
            grd.Columns.Add(new BoundField { DataField = "unidadEjecutora" });
            grd.Columns.Add(new BoundField { DataField = "nroDS" });
            grd.Columns.Add(new BoundField { DataField = "poblacionSnip" });
            grd.Columns.Add(new BoundField { DataField = "fisicoEjecutado" });
            grd.Columns.Add(new BoundField { DataField = "id_TipoEstadoEjecucion" });
            grd.Columns.Add(new BoundField { DataField = "tipoEstadoEjecucion" });
            grd.Columns.Add(new BoundField { DataField = "tecnico" });
            grd.Columns.Add(new BoundField { DataField = "fecha" });
            grd.Columns.Add(new BoundField { DataField = "horaInicio" });
            grd.Columns.Add(new BoundField { DataField = "representanteMVCS" });

            grd.AutoGenerateColumns = false;
            grd.DataSource = ListBandeja;
            grd.DataBind();
            Exportar_Concluidos(grd);

        }


        protected void cargaTipoInagura()
        {
            ddlTipoInauguracion.DataSource = objBLbandeja.F_spMON_TipoInaugura();
            ddlTipoInauguracion.DataTextField = "nombre";
            ddlTipoInauguracion.DataValueField = "valor";
            ddlTipoInauguracion.DataBind();

            ddlTipoInauguracion.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void cargaProyinauguracion(string idProyecto)
        {
            _BE_Bandeja.id_proyecto = Convert.ToInt32(idProyecto);

            dt = objBLbandeja.spMON_InauguracionProyecto(_BE_Bandeja);

            if (dt.Rows.Count > 0)
            {
                txtRepresentanteMVCS.Text = dt.Rows[0]["representanteMVCS"].ToString();
                ddlTipoInauguracion.SelectedValue = dt.Rows[0]["id_tipoInaugura"].ToString();
                txtFechaInauguracion.Text = dt.Rows[0]["fechaInauguracion"].ToString();
                txtHoraInicio.Text = dt.Rows[0]["horaInicio"].ToString();
                txtHoraTermino.Text = dt.Rows[0]["horaFin"].ToString();
                lblUser.Text = dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

            }
            else
            {
                ddlTipoInauguracion.SelectedValue = "";
                txtRepresentanteMVCS.Text = "";
                txtFechaInauguracion.Text = "";
                txtHoraInicio.Text = "";
                txtHoraTermino.Text = "";
                lblUser.Text = "";

            }
        }

        protected void btnGuardar_OnClick(object sender, EventArgs e)
        {
            if (validarGuardar())
            {
                string usr = (Session["IdUsuario"].ToString());
                int val = objBLbandeja.spi_MON_InauguracionProyecto(Convert.ToInt32(lblIdProyecto.Text), Convert.ToInt32(ddlTipoInauguracion.SelectedValue), Convert.ToDateTime(txtFechaInauguracion.Text), txtHoraInicio.Text, txtHoraTermino.Text, txtRepresentanteMVCS.Text, usr);

                if (val == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaBandejaConcluidos(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlGestion.SelectedValue);
                    Up_grdBandeja.Update();

                    cargarCalendario("156");
                    _sheduleData = GetSchedule();
                    UP_calendar.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }


            }



        }


        protected Boolean validarGuardar()
        {
            Boolean result;
            result = true;

            if (ddlTipoInauguracion.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar Inaugurado por.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaInauguracion.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de Inauguración.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaInauguracion.Text) == false)
            {
                string script = "<script>alert('Ingresar una fecha con el formato:dd/mm/yyyy.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtHoraInicio.Text == "")
            //{
            //    string script = "<script>alert('Ingresar Hora de Inicio.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if ((txtHoraInicio.Text).Length != 5)
            {
                string script = "<script>alert('Ingresar Hora de Inicio con el formato:hh:mm');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if ((txtHoraTermino.Text).Length != 5)
            {
                string script = "<script>alert('Ingresar Hora de Termino con el formato:hh:mm.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }

        protected void Exportar_Concluidos(GridView grd)
        {
            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Bandeja_Inauguración.xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                //grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                for (int i = 1; i < 17; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                }


                grd.HeaderRow.Cells[0].Visible = false;

                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "GESTION";
                grd.HeaderRow.Cells[3].Text = "SNIP";
                grd.HeaderRow.Cells[4].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[4].Width = 600;
                grd.HeaderRow.Cells[5].Text = "COSTO FASE DE INVERSION (S/.)";
                grd.HeaderRow.Cells[5].Width = 120;
                grd.HeaderRow.Cells[6].Text = "MONTO TRANSFERIDOS (S/.)";
                grd.HeaderRow.Cells[6].Width = 120;
                grd.HeaderRow.Cells[7].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[7].Width = 400;
                grd.HeaderRow.Cells[8].Text = "N° D.S./R.M.";
                grd.HeaderRow.Cells[8].Width = 120;
                grd.HeaderRow.Cells[9].Text = "POB. SNIP";
                grd.HeaderRow.Cells[10].Text = "AVANCE FISICO OBRA (%)";
                grd.HeaderRow.Cells[10].Width = 100;
                grd.HeaderRow.Cells[11].Visible = false;
                grd.HeaderRow.Cells[12].Text = "ESTADO SITUACIONAL";
                grd.HeaderRow.Cells[12].Width = 100;
                grd.HeaderRow.Cells[13].Text = "MONITOR(A)";
                grd.HeaderRow.Cells[13].Width = 200;
                grd.HeaderRow.Cells[14].Text = "FECHA INAUGURACION";
                grd.HeaderRow.Cells[15].Text = "HORA";

                grd.HeaderRow.Cells[16].Text = "REPRESENTANTE MVCS";



                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];

                    row.Cells[0].Visible = false;
                    row.Cells[11].Visible = false;

                    for (int j = 0; j < 17; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].VerticalAlign = VerticalAlign.Middle;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }



                }


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "BANDEJA DE PROYECTOS CONCLUIDOS Y PROXIMOS A INAUGURAR - SSP";
                myTable.ColumnSpan = 15;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                // Define estilo para formato texto y numérico
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);
                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);

                // Agrega título en primera celda
                //string Titulo = "     SISTEMA INTEGRADO DE GESTION Y EVALUACIÓN DE PROYECTOS - OGEI ";
                //HttpContext.Current.Response.Write(Titulo);
                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();

            }
        }


        protected void imgbtnRecepcion_OnClick(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdBandeja.Rows[row.RowIndex].FindControl("imgbtnRecepcion");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void imgbtnFichaProyecto_OnClick(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grdBandeja.DataKeys[row.RowIndex].Values[0].ToString();

            //  ImageButton url = (ImageButton)grdBandeja.Rows[row.RowIndex].FindControl("imgbtnFichaProyecto");

            Label lblIDFinanciamiento = (Label)grdBandeja.Rows[row.RowIndex].FindControl("lblIdTipoFinanciamiento");

            Label lblCodSubsector = (Label)grdBandeja.Rows[row.RowIndex].FindControl("des_subsector");


            string script = "<script>window.open('Ficha_Proyecto.aspx?id=" + id + "&idtipo=" + lblIDFinanciamiento.Text + "&cod_subsector=" + lblCodSubsector.Text + "&t=r' ,'_blank') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);



        }
    }
}