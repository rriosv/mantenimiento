﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business;
using Entity;
using DevExpress.XtraCharts;
using System.Globalization;

namespace Web.Monitor
{
    public partial class iframe_GraficoSOSEM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string snip = "";
            try
            {
                snip = (Request.QueryString["snip"].ToString());
            }
            catch
            {
                snip = "0";
            }

            if (!IsPostBack)
            {
                CargaGraficoSOSEMTab0(snip);
            }

        }

        protected void CargaGraficoSOSEMTab0(string vString)
        {
            BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
            BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

            int snip = Convert.ToInt32(vString);
            
            List<BE_MON_SOSEM> ListSosem = new List<BE_MON_SOSEM>();

            ListSosem = _objBLFinanciamiento.F_spMON_SosemDevengadoMensualizado(snip, 0);

            if (ListSosem.Count > 0)
            {
                Series SerieD = WebChartControl1.Series[0];
                Series SeriePIM = WebChartControl1.Series[1];

                for (int i = 0; i < ListSosem.Count; i++)
                {

                    SeriesPoint punto = new SeriesPoint(Convert.ToInt32(ListSosem.ElementAt(i).anio), Convert.ToDecimal(ListSosem.ElementAt(i).devAcumulado).ToString("N2", CultureInfo.CurrentUICulture));
                    SerieD.Points.Add(punto);
                    SeriesPoint puntoPIM = new SeriesPoint(Convert.ToInt32(ListSosem.ElementAt(i).anio), Convert.ToDecimal(ListSosem.ElementAt(i).pimAcumulado).ToString("N2", CultureInfo.CurrentUICulture));
                    SeriePIM.Points.Add(puntoPIM);
                }
            }
            else
            {
                WebChartControl1.Visible = false;
            }

        }
    }
}