﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Net;
using System.Data;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class PresupuestoPNVR : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();
        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();
        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                Response.Redirect("~/login.aspx?ms=1");
            }

            Grabar_Log();
            if (!IsPostBack)
            {
                lblId_Proyecto.Text = Request.QueryString["id"].ToString();
                lblId_Usuario.Text = (Session["IdUsuario"]).ToString();
                cargaItem();

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(lblId_Proyecto.Text); ;
                DataTable dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    lblNomProyecto.Text = dt.Rows[0]["nom_proyecto"].ToString();

                    string depa = dt.Rows[0]["depa"].ToString();
                    string prov = dt.Rows[0]["prov"].ToString();
                    string dist = dt.Rows[0]["dist"].ToString();

                    string flag = (dt.Rows[0]["flagFinalizoPresupuesto"].ToString());

                    chbFinalizacion.SelectedValue = flag;

                    if (flag == "1")
                    {
                        btnGuardarPresupuesto.Enabled = false;
                    }

                }

                _BEProceso.id_proyecto = Convert.ToInt32(lblId_Proyecto.Text);
                _BEProceso.id_usuario = Convert.ToInt32(lblId_Usuario.Text);

                int idacceso = _objBLProceso.spMON_Acceso(_BEProceso);

                if (idacceso == 0)
                {
                    btnGuardarPresupuesto.Visible = false;
                }
            }
        }
        protected void cargaItem()
        {
            List<BE_MON_Financiamiento> ListItem = new List<BE_MON_Financiamiento>();
            ListItem = _objBLFinanciamiento.F_spMON_PresupuestoPNVR(Convert.ToInt32(lblId_Proyecto.Text));

            if (ListItem.ElementAt(0).tipo == 2)
            {
                //int i = 0;
                foreach (BE_MON_Financiamiento ItemRubro in ListItem)
                {

                    //Label lblIDPartida = (Label)FindControl("lblIdPartida" + i.ToString());
                    ////Label lblNro = (Label)FindControl("lblN" + i.ToString());
                    //Label lblNombre = (Label)FindControl("lblPartida" + i.ToString());
                    //TextBox txtMonto = (TextBox)FindControl("lblMonto" + i.ToString());

                    Label lblIDPartida;
                    Label lblNombre;
                    TextBox txtMonto;


                    lblIDPartida = (Label)FindControl("lblIdPartida" + ItemRubro.numero);
                    lblNombre = (Label)FindControl("lblPartida" + ItemRubro.numero);
                    txtMonto = (TextBox)FindControl("lblMonto" + ItemRubro.numero);


                    lblIDPartida.Text = ItemRubro.numero.ToString();
                    //lblNro.Text = ItemRubro.Id;
                    lblNombre.Text = ItemRubro.observacion;
                    txtMonto.Text = Convert.ToDouble(ItemRubro.monto).ToString("N2");
                    txtMonto.Style.Add("text-align", "right");

                    if (ItemRubro.usuario.Length > 0)
                    {
                        lblNomActualiza.Text = "Actualizó: " + ItemRubro.usuario + " - " + ItemRubro.strFecha_update;
                    }

                    //i = i + 1;



                }

                tblSierra.Visible = true;
            }

            if (ListItem.ElementAt(0).tipo == 3)
            {
                foreach (BE_MON_Financiamiento ItemRubro in ListItem)
                {
                    Label lblIDPartida = (Label)FindControl("lblIdPartidaSelva" + ItemRubro.numero.ToString());
                    //Label lblNro = (Label)FindControl("lblN" + i.ToString());
                    Label lblNombre = (Label)FindControl("lblPartidaSelva" + ItemRubro.numero.ToString());
                    TextBox txtMonto = (TextBox)FindControl("txtMontoSelva" + ItemRubro.numero.ToString());

                    lblIDPartida.Text = ItemRubro.numero.ToString();
                    //lblNro.Text = ItemRubro.Id;
                    lblNombre.Text = ItemRubro.observacion;
                    txtMonto.Text = Convert.ToDouble(ItemRubro.monto).ToString("N2");
                    txtMonto.Style.Add("text-align", "right");
                    if (ItemRubro.usuario.Length > 0)
                    {
                        lblNomActualiza.Text = "Actualizó: " + ItemRubro.usuario + " - " + ItemRubro.strFecha_update;
                    }

                }

                tblSelva.Visible = true;
            }

        }

        protected void lblMonto_TextChanged(object sender, EventArgs e)
        {
            double suma;
            suma = 0;
            for (int i = 1; i <= 14; i++)
            {

                TextBox txtMonto = (TextBox)FindControl("lblMonto" + i.ToString());

                if (txtMonto.Text == "")
                {
                    txtMonto.Text = "0.00";
                }
                txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString("N2");
                suma = suma + Convert.ToDouble(txtMonto.Text);

            }

            // AUMENTADO EL úlTIMO ITEM
            if (lblMonto19.Text == "")
            {
                lblMonto19.Text = "0.00";
            }
            lblMonto19.Text = Convert.ToDouble(lblMonto19.Text).ToString("N2");
            suma = suma + Convert.ToDouble(lblMonto19.Text);

            for (int i = 25; i <= 28; i++)
            {

                TextBox txtMonto = (TextBox)FindControl("lblMonto" + i.ToString());

                if (txtMonto.Text == "")
                {
                    txtMonto.Text = "0.00";
                }
                txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString("N2");
                suma = suma + Convert.ToDouble(txtMonto.Text);

            }

            lblMonto15.Text = suma.ToString("N2");

            lblMonto17.Text = (Convert.ToDouble(lblMonto15.Text) + Convert.ToDouble(lblMonto16.Text)).ToString("N2");
        }
        protected void lblMonto015_TextChanged(object sender, EventArgs e)
        {
            lblMonto17.Text = (Convert.ToDouble(lblMonto15.Text) + Convert.ToDouble(lblMonto16.Text)).ToString("N2");
        }

        protected Boolean ValidarPresupuesto()
        {
            Boolean result;
            result = true;

            if (tblSierra.Visible)
            {

                String[] ArrayPartidasSelva = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "19", "25", "26", "27", "28", "15", "16", "17", "18" };

                foreach (String ItemPartida in ArrayPartidasSelva)
                {

                    Label lblNombre = (Label)FindControl("lblPartida" + ItemPartida.ToString());
                    TextBox txtMonto = (TextBox)FindControl("lblMonto" + ItemPartida.ToString());

                    if (txtMonto.Text == "")
                    {
                        string script = "<script>alert('Ingresar monto de" + lblNombre.Text + " .');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                    else
                    {
                        txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString("N2");
                    }
                }

                return result;




            }
            if (tblSelva.Visible)
            {
                int[] ArrayPartidasSelva = new int[] { 1, 2, 20, 4, 21, 22, 5, 23, 24, 11, 13, 14, 15, 16, 17, 18, 25, 26 };

                foreach (int ItemPartida in ArrayPartidasSelva)
                {

                    Label lblNombre = (Label)FindControl("lblIdPartidaSelva" + ItemPartida.ToString());
                    TextBox txtMonto = (TextBox)FindControl("txtMontoSelva" + ItemPartida.ToString());

                    if (txtMonto.Text == "")
                    {
                        string script = "<script>alert('Ingresar monto de" + lblNombre.Text + " .');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                    else
                    {
                        txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString("N2");
                    }
                }

                return result;
            }

            return result;
        }

        protected void btnGuardarPresupuesto_Click(object sender, EventArgs e)
        {
            if (ValidarPresupuesto())
            {
                int val = 0;

                if (tblSierra.Visible)
                {
                    /*
                    for (int i = 0; i <= 18; i++)
                    {
                        Label lblIDPartida = (Label)FindControl("lblIdPartida" + i.ToString());
                        //Label lblNro = (Label)FindControl("lblN" + i.ToString());
                        TextBox txtMonto = (TextBox)FindControl("lblMonto" + i.ToString());

                        _BEFinanciamiento.id_proyecto = Convert.ToInt32(lblId_Proyecto.Text);
                        _BEFinanciamiento.Id = lblIDPartida.Text;
                        txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString();
                        _BEFinanciamiento.monto = txtMonto.Text;
                        _BEFinanciamiento.id_usuario = Convert.ToInt32(lblId_Usuario.Text);

                        val = _objBLFinanciamiento.IU_spiu_MON_PresupuestoPNVR(_BEFinanciamiento);

                        if (val == 0)
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        }
                    }
                    */

                    String[] ArrayPartidasSelva = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "19", "25", "26", "27", "28", "15", "16", "17", "18" };


                    foreach (String ItemPartida in ArrayPartidasSelva)
                    {
                        Label lblIDPartida = (Label)FindControl("lblIdPartida" + ItemPartida.ToString());
                        TextBox txtMonto = (TextBox)FindControl("lblMonto" + ItemPartida.ToString());

                        _BEFinanciamiento.id_proyecto = Convert.ToInt32(lblId_Proyecto.Text);
                        _BEFinanciamiento.Id = lblIDPartida.Text;
                        txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString();
                        _BEFinanciamiento.monto = txtMonto.Text;
                        _BEFinanciamiento.id_usuario = Convert.ToInt32(lblId_Usuario.Text);

                        val = _objBLFinanciamiento.IU_spiu_MON_PresupuestoPNVR(_BEFinanciamiento);

                        if (val == 0)
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        }
                    }
                }

                if (tblSelva.Visible)
                {
                    int[] ArrayPartidasSelva = new int[] { 1, 2, 20, 4, 21, 22, 5, 23, 24, 11, 13, 14, 15, 16, 17, 18, 25, 26 };

                    foreach (int ItemPartida in ArrayPartidasSelva)
                    {
                        Label lblIDPartida = (Label)FindControl("lblIdPartidaSelva" + ItemPartida.ToString());
                        //Label lblNro = (Label)FindControl("lblN" + i.ToString());
                        TextBox txtMonto = (TextBox)FindControl("txtMontoSelva" + ItemPartida.ToString());

                        _BEFinanciamiento.id_proyecto = Convert.ToInt32(lblId_Proyecto.Text);
                        _BEFinanciamiento.Id = lblIDPartida.Text;
                        txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString();
                        _BEFinanciamiento.monto = txtMonto.Text;
                        _BEFinanciamiento.id_usuario = Convert.ToInt32(lblId_Usuario.Text);

                        val = _objBLFinanciamiento.IU_spiu_MON_PresupuestoPNVR(_BEFinanciamiento);

                        if (val == 0)
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        }
                    }
                }

                if (chbFinalizacion.SelectedValue == "1")
                {
                    _BEFinanciamiento.flag = 1;
                }
                else
                {
                    _BEFinanciamiento.flag = 0;
                }

                val = _objBLFinanciamiento.U_spu_MON_FlagPresupuestoPNVR(_BEFinanciamiento);

                if (val == 0)
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaItem();

                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(lblId_Proyecto.Text); ;
                    DataTable dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        chbFinalizacion.SelectedValue = (dt.Rows[0]["flagFinalizoPresupuesto"].ToString());

                        if (chbFinalizacion.SelectedValue == "1")
                        {
                            btnGuardarPresupuesto.Enabled = false;
                        }
                    }
                }

            }
        }

        protected void txtMontoSelva_TextChanged(object sender, EventArgs e)
        {
            double suma;
            suma = 0;

            int[] ArrayPartidasSelva = new int[] { 1, 2, 20, 4, 21, 22, 5, 23, 24, 11, 13, 14, 25, 26 };

            foreach (int ItemPartida in ArrayPartidasSelva)
            {
                TextBox txtMonto = (TextBox)FindControl("txtMontoSelva" + ItemPartida.ToString());

                if (txtMonto.Text == "")
                {
                    txtMonto.Text = "0.00";
                }

                txtMonto.Text = Convert.ToDouble(txtMonto.Text).ToString("N2");

                suma = suma + Convert.ToDouble(txtMonto.Text);
            }

            txtMontoSelva15.Text = suma.ToString("N2");
            txtMontoSelva17.Text = (Convert.ToDouble(txtMontoSelva15.Text) + Convert.ToDouble(txtMontoSelva16.Text)).ToString("N2");
        }

        protected void txtMontoSelva16_TextChanged(object sender, EventArgs e)
        {
            txtMontoSelva17.Text = (Convert.ToDouble(txtMontoSelva15.Text) + Convert.ToDouble(txtMontoSelva16.Text)).ToString("N2");
        }


    }
}