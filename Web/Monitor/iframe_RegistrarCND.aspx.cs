﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace Web.Monitor
{
    public partial class iframe_RegistrarCND : System.Web.UI.Page
    {
        BL_MON_Ejecucion _obj = new BL_MON_Ejecucion();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                int idProy = Convert.ToInt32(Request.QueryString["id"]);

                CargarCompromiso(idProy);
            }

            //Habilitación y desactivación de radiobutton
            string script2 = "";
            if (rbEnergia.SelectedValue.Equals("1"))
            {
                script2 = script2 +"$('#txtEnergiasRenovables').prop( 'disabled', false );";
            }
            else
            {
                script2 = script2 + "$('#txtEnergiasRenovables').prop( 'disabled', true );";
            }

            if (rbCarga.SelectedValue.Equals("1"))
            {
                script2 = script2 + "$('#txtCarga').prop( 'disabled', false );";
            }
            else
            {
                script2 = script2 + "$('#txtCarga').prop( 'disabled', true );";
            }

            if (rbCapacidad.SelectedValue.Equals("1"))
            {
                script2 = script2 + "$('#txtCapacidad').prop( 'disabled', false );";
            }
            else
            {
                script2 = script2 + "$('#txtCapacidad').prop( 'disabled', true );";
            }
            script2 = "<script>" + script2 + "</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script2, false);
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            string msjVal = Validar();
            if (msjVal.Length > 0)
            {
                string script3 = "<script>alert('" + msjVal + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script4", script3, false);
            }
            else
            {
                int idProy = Convert.ToInt32(Request.QueryString["id"]);

                string _flagEnergia = "";
                string _flagNuevoPtar = "";
                string _flagDigestores = "";

                float _undEnergia = 0;
                float _undNuevoPtarCarga = 0;
                float _undDigestoresCapacidad = 0;

                _flagEnergia = rbEnergia.SelectedValue;
                _flagNuevoPtar = rbCarga.SelectedValue;
                _flagDigestores = rbCapacidad.SelectedValue;

                _undEnergia = float.Parse(txtEnergiasRenovables.Text);
                _undNuevoPtarCarga = float.Parse(txtCarga.Text);
                _undDigestoresCapacidad = float.Parse(txtCapacidad.Text);

                //int val = _obj.IU_MON_CompromisoGEI(idProy, _flagEnergia, _undEnergia, _flagNuevoPtar, _undNuevoPtarCarga, _flagDigestores, _undDigestoresCapacidad, Convert.ToInt32(Session["IdUsuario"].ToString()));
                int val = _obj.IU_MON_CompromisoGEI(idProy, _flagEnergia, _undEnergia, _flagNuevoPtar, _undNuevoPtarCarga, _flagDigestores, _undDigestoresCapacidad, 481);


                if (val == 1)
                {
                    string script = "<script>alert('Se registro correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargarCompromiso(idProy);

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void CargarCompromiso(int _idProyecto)
        {
            dt = _obj.spMON_CompromisoGEI(_idProyecto);

            string script = "";


            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["flagEnergia"].ToString().Equals("1") || dt.Rows[0]["flagEnergia"].ToString().Equals("0"))
                {
                    rbEnergia.SelectedValue = dt.Rows[0]["flagEnergia"].ToString();

                    if (dt.Rows[0]["flagEnergia"].ToString().Equals("1"))
                    {
                        txtEnergiasRenovables.Text = dt.Rows[0]["undEnergia"].ToString();
                        //txtEnergiasRenovables.Enabled = true;
                        script = "$('#txtEnergiasRenovables').prop( 'disabled', false );";
                    }
                    else
                    {
                        script = "$('#txtEnergiasRenovables').prop( 'disabled', true );";
                    }

                }

                if (dt.Rows[0]["flagNuevoPtar"].ToString().Equals("1") || dt.Rows[0]["flagNuevoPtar"].ToString().Equals("0"))
                {
                    rbCarga.SelectedValue = dt.Rows[0]["flagNuevoPtar"].ToString();

                    if (dt.Rows[0]["flagNuevoPtar"].ToString().Equals("1"))
                    {
                        txtCarga.Text = dt.Rows[0]["undNuevoPtarCarga"].ToString();
                        //txtCarga.Enabled = true;

                        script = script + "$('#txtCarga').prop( 'disabled', false );";
                    }
                    else
                    {
                        script = script + "$('#txtCarga').prop( 'disabled', true );";
                    }
                }
                if (dt.Rows[0]["flagDigestores"].ToString().Equals("1") || dt.Rows[0]["flagDigestores"].ToString().Equals("0"))
                {
                    rbCapacidad.SelectedValue = dt.Rows[0]["flagDigestores"].ToString();

                    if (dt.Rows[0]["flagDigestores"].ToString().Equals("1"))
                    {
                        txtCapacidad.Text = dt.Rows[0]["undDigestoresCapacidad"].ToString();
                        txtCapacidad.Enabled = true;

                        script = script + "$('#txtCapacidad').prop( 'disabled', false );";
                    }
                    else
                    {
                        script = script + "$('#txtCapacidad').prop( 'disabled', true );";
                    }
                }

               

                lblNomActualizo.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
            }
            else
            {
                script = "$('#txtEnergiasRenovables').prop( 'disabled', true );";
                script = script + "$('#txtCarga').prop( 'disabled', true );";
                script = script + "$('#txtCapacidad').prop( 'disabled', true );";
            }

            if (script.Length > 0)
            {
                script = "<script>" + script + "</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }

        protected void rbEnergia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbEnergia.SelectedValue.Equals("1"))
            {
                string script = "<script>$('#txtEnergiasRenovables').prop( 'disabled', false );</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                txtEnergiasRenovables.Text = "0";
                string script = "<script>$('#txtEnergiasRenovables').prop( 'disabled', true );</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }

        protected void rbCarga_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbCarga.SelectedValue.Equals("1"))
            {
                string script = "<script>$('#txtCarga').prop( 'disabled', false );</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                txtCarga.Text = "0";
                string script = "<script>$('#txtCarga').prop( 'disabled', true );</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }

        protected void rbCapacidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbCapacidad.SelectedValue.Equals("1"))
            {
                string script = "<script>$('#txtCapacidad').prop( 'disabled', false );</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                txtCapacidad.Text = "0";
                string script = "<script>$('#txtCapacidad').prop( 'disabled', true );</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }

        protected string Validar()
        {
            string msj = "";

            if (rbEnergia.SelectedValue == "")
            {
                msj = msj + "*Responder ¿El proyecto considera el uso de energias renovables o generación de energía?.\\n";
            }
            if (txtEnergiasRenovables.Text == "")
            {
                msj = msj + "*Ingresar cantidad de Energias Renovables (kWH).\\n";
            }
            if (rbCarga.SelectedValue == "")
            {
                msj = msj + "*Responder ¿El proyecto considera la construcción de una nueva PTAR?.\\n";
            }
            if (txtCarga.Text == "")
            {
                msj = msj + "*Ingresar cantidad de Carga (kg DBO/día).\\n";
            }
            if (rbCapacidad.SelectedValue == "")
            {
                msj = msj + "*Responder ¿El proyecto considera digestores de lodos en la PTAR con quema de metano?.\\n";
            }
            if (txtCapacidad.Text == "")
            {
                msj = msj + "*Ingresar Capacidad (m3 de lodo a tratar/día)\\n";
            }
            return msj;
        }
    }
}