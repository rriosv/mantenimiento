﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorreoTecnico.aspx.cs" Inherits="Web.Monitor.CorreoTecnico" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Correo</title>
    <link href="../css/global.css"rel="stylesheet" type="text/css" />
         <script src="../js/global.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10%" align="right">
               PARA:
            </td>
            <td width="90%" align="left" style="padding-left:5px">
               <asp:TextBox ID="txtpara" runat="server" Width="490px"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td width="10%" align="right">
               CC:
            </td>
            <td width="90%" align="left" style="padding-left:5px">
               <asp:TextBox ID="txtCC" runat="server" Width="490px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:5px">
            </td>
        </tr>
        <tr>
            <td align="right">
            ASUNTO:
            </td>
            <td align="left" style="padding-left:5px">
                <asp:TextBox ID="txtasunto" runat="server" Width="490px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px">
            </td>
        </tr>
        <tr>
            <td colspan="2">
              <CKEditor:CKEditorControl ID="ckeditor" Width="580px" runat="server" Height ="260px" 
                         ToolbarFull="Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl&#13;&#10;/&#13;&#10;Styles|Format|Font|FontSize&#13;&#10;TextColor|BGColor" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;

            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button id="btnenviar" runat="server" Text="Enviar" CssClass="boton" OnClick="btnenviar_Click"></asp:Button>
                &nbsp;
                <asp:Button id="btncancelar" runat="server" Text="Cancelar" CssClass="boton" OnClick="btncancelar_Click"></asp:Button>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
