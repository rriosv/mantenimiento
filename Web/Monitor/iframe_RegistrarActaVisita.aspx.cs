﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;


namespace Web.Monitor
{
    public partial class iframe_RegistrarActaVisita : System.Web.UI.Page
    {
        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BLUtil _Metodo = new BLUtil();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = "";

                string idProyecto = "";
                try
                {
                    id = Request.QueryString["idE"].ToString();
                    lblIdEvaluacionRecomendacionPOPUP.Text = id.ToString();
                    idProyecto = Request.QueryString["idPo"].ToString();

                }
                catch
                {

                }
                LblID_PROYECTO.Text = idProyecto;
                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();
                lblCOD_SUBSECTOR.Text = Request.QueryString["idSector"].ToString().Trim();

                cargaInicial();
                CargaActaVisitaDatosGenerales(id);
            }
        }

        protected void cargaInicial()
        {
            //lblIdEvaluacionRecomendacionPOPUP.Text = lblID_evaluacionTab2.Text;
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionPOPUP.Text);
            grdSistemaDrenajePluvial.DataSource = _objBLEjecucion.F_MON_SistemaDrenajeDetalle(_BEEjecucion);
            grdSistemaDrenajePluvial.DataBind();

            int pIdSector = 1;
            if (lblCOD_SUBSECTOR.Text.Equals("2")) 
            { pIdSector = 2; }
            ddlTipoSistemaDrenaje.DataSource = _objBLEjecucion.F_MON_TipoSistemaDrenaje(pIdSector);
            ddlTipoSistemaDrenaje.DataTextField = "nombre";
            ddlTipoSistemaDrenaje.DataValueField = "valor";
            ddlTipoSistemaDrenaje.DataBind();
            ddlTipoSistemaDrenaje.Items.Insert(0, new ListItem("-Seleccione-", ""));

            CargaActaVisitaDatosGenerales(lblIdEvaluacionRecomendacionPOPUP.Text);
            //CargarAsuntosTecnicos(lblIdEvaluacionRecomendacionPOPUP.Text);

            if (lblCOD_SUBSECTOR.Text.Equals("2")) // PMIB
            {
                //trComponentePNSU1.Visible = false;
                //trComponentePNSU2.Visible = false;
                //trComponentePNSU3.Visible = false;
                //trComponentePNSU4.Visible = false;

                //trComponentePMIB1.Visible = true;
                //trComponentePMIB2.Visible = true;
                //trComponentePMIB3.Visible = true;
                //trComponentePMIB4.Visible = true;

                //trDocumentoPMIB1.Visible = true;
                //trDocumentoPMIB2.Visible = true;
                //trDocumentoPMIB3.Visible = true;
                //trDocumentoPMIB4.Visible = true;

                trFuncionamiento1.Visible = false;
                trFuncionamiento2.Visible = false;
                trFuncionamiento3.Visible = false;
                trFuncionamiento4.Visible = false;

                //trCumplimientoInicio.Visible = true;
            }
            else
            {
                trCumplimientoInicio.Visible = false;
            }
            

            Up_RegistroActaVisita.Update();
        }
        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected Boolean ValidarRegistroActaVisita()
        {
            string result = "";

            if (txtFechaActavisita.Text == "")
            {
                result = result + "*Ingresar fecha de acta de visita.\\n";
            }

            if (_Metodo.ValidaFecha(txtFechaActavisita.Text) == false)
            {
                result = result + "*Formato no valido de fecha de acta de visita.\\n";
            }

            if (trCumplimientoInicio.Visible == true)
            {
                if (chbcumplimiento.SelectedValue == "")
                {
                    result = result + "*Seleccionar cumplimiento de los 5 requisitos necesarios para el inicio de obra.\\n";
                }
            }

            if (trFuncionamiento4.Visible == true)
            {
                if (chblTipoFuncionamientoActaVisita.SelectedValue == "")
                {
                    result = result + "*Seleccionar un item de funcionamiento.\\n";
                }

                if (txtComentarioFuncionamientoActaVisita.Text == "")
                {
                    result = result + "*Ingresar comentario de funcionamiento.\\n";
                }

            }
            if (grdSistemaDrenajePluvial.Rows.Count == 0 && lblCOD_SUBSECTOR.Text.Equals("1"))
            {
                result = result + "*Registrar componentes del proyecto.\\n";
            }

            if (txtObservacionesActaVisita.Text.Trim() == "")
            {
                result = result + "*Ingresar observaciones.\\n";
            }

            if (txtRecomendacionesActaVisita.Text.ToString().Trim() == "")
            {
                result = result + "*Ingresar recomendaciones.'\\n"; 
            }

            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                return true;
            }

        }


        protected void btnGuardarRecomendacionActaVisita_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroActaVisita())
            {
                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionPOPUP.Text);

                _BEEjecucion.Date_fechaVisita = VerificaFecha(txtFechaActavisita.Text);
                if (chblTipoFuncionamientoActaVisita.SelectedValue == "")
                {
                    _BEEjecucion.id_item = 0;
                }
                else
                {
                    _BEEjecucion.id_item = Convert.ToInt32(chblTipoFuncionamientoActaVisita.SelectedValue);
                }
                _BEEjecucion.Evaluacion = txtComentarioFuncionamientoActaVisita.Text;
                if (chblFlagInaugurado.SelectedValue == "")
                {
                    _BEEjecucion.flag = 2;
                }
                else
                {
                    _BEEjecucion.flag = Convert.ToInt32(chblFlagInaugurado.SelectedValue);
                }

                if (trCumplimientoInicio.Visible == true)
                {
                    _BEEjecucion.flagVerificado = chbcumplimiento.SelectedValue;
                }
                else
                {
                    _BEEjecucion.flagVerificado = "";
                }
                //_BEEjecucion.comentario = txtEntregaDocumentoActaVisita.Text;
                _BEEjecucion.comentario = "";
                _BEEjecucion.observacion = txtObservacionesActaVisita.Text;
                _BEEjecucion.Recomendacion = txtRecomendacionesActaVisita.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.U_MON_EvaluacionRecomendacionActaVisita(_BEEjecucion);
               
                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //CargarAsuntosTecnicos(lblIdEvaluacionRecomendacionPOPUP.Text);
                    CargaActaVisitaDatosGenerales(lblIdEvaluacionRecomendacionPOPUP.Text);
                    Up_RegistroActaVisita.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void CargaActaVisitaDatosGenerales(string idEvaluacionRecomendacion)
        {

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(idEvaluacionRecomendacion);
            dt = _objBLEjecucion.spMON_EvaluacionRecomendacionActaVisita(_BEEjecucion);

            txtFechaActavisita.Text = dt.Rows[0]["fechaActaVisita"].ToString();
            txtObservacionesActaVisita.Text = dt.Rows[0]["observacionActaVisita"].ToString();
            txtRecomendacionesActaVisita.Text = dt.Rows[0]["recomendacionActaVisita"].ToString();
            //txtEntregaDocumentoActaVisita.Text = dt.Rows[0]["cumplimientoDocumentoActaVisita"].ToString();


            string iTipoFuncionamiento = dt.Rows[0]["id_tipoFuncionamiento"].ToString();
            if (iTipoFuncionamiento.Length > 0)
            {
                chblTipoFuncionamientoActaVisita.SelectedValue = iTipoFuncionamiento;
            }
            else
            {
                chblTipoFuncionamientoActaVisita.ClearSelection();
            }

            if (dt.Rows[0]["idEstado"].ToString().Equals("41") && dt.Rows[0]["idSubEstado"].ToString().Equals("71")) //Por Iniciar
            {
                trCumplimientoInicio.Visible = true;
                chbcumplimiento.SelectedValue = dt.Rows[0]["flagRequisitoInicio"].ToString();
            }

            txtComentarioFuncionamientoActaVisita.Text = dt.Rows[0]["comentarioFuncionamiento"].ToString();

            string flagInaugurado = dt.Rows[0]["flagInaugurado"].ToString();
            if (flagInaugurado.Length > 0)
            {
                chblFlagInaugurado.SelectedValue = flagInaugurado;
            }
            else
            {
                chblFlagInaugurado.ClearSelection();
            }

            lblNomUsuarioActaVisita.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdataActaVisita"].ToString();

        }

        protected void CargarAsuntosTecnicos(string idEvaluacionRecomendacion)
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(idEvaluacionRecomendacion);
            dt = _objBLEjecucion.spMON_RevisionItemAdministrativoVisita(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= 16; i++)
                {
                    bool encontro = false;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (i == Convert.ToInt32((row["id_itemAdministrativoVisita"].ToString())))
                        {
                            TextBox txtCome = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtComentario" + row["id_itemAdministrativoVisita"].ToString());
                            RadioButtonList result = (RadioButtonList)FindControl("ctl00$ContentPlaceHolder1$RadioButtonList" + row["id_itemAdministrativoVisita"].ToString());

                            txtCome.Text = row["comentario"].ToString();
                            if (row["result"].ToString().Equals("0"))
                            {
                                result.ClearSelection();
                            }
                            else
                            {
                                result.SelectedValue = row["result"].ToString();
                            }

                            encontro = true;
                        }
                    }

                    if (encontro == false)
                    {
                        TextBox txtCome = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtComentario" + i.ToString());
                        RadioButtonList result = (RadioButtonList)FindControl("ctl00$ContentPlaceHolder1$RadioButtonList" + i.ToString());
                        txtCome.Text = "";
                        result.ClearSelection();
                    }

                }
            }
        }

        protected void btnGenerarActaVisitaPopup_Click(object sender, EventArgs e)
        {
            string id = lblIdEvaluacionRecomendacionPOPUP.Text;
            string script = "<script>window.open('Acta_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void imgbtnAgregarSistemaDrenaje_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RegistroSistemaDrenajePluvial.Visible = true;
            imgbtnAgregarSistemaDrenaje.Visible = false;
            btnGuardarSistemaDrenaje.Visible = true;
            btnModificarSistemaDrenaje.Visible = false;

            txtComentarioSistemaDrenaje.Text = "";
            ddlTipoSistemaDrenaje.SelectedValue = "";
            //txtComentarioTab8.Text = "";
            //imgbtnDocTab8.ImageUrl = "~/img/blanco.png";
            //lnkbtnDocTab8.Text = "";

            Up_RegistroActaVisita.Update();
        }

        protected void btnGuardarSistemaDrenaje_Click(object sender, EventArgs e)
        {
            // Información general
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionPOPUP.Text);
            _BEEjecucion.Date_fechaVisita = VerificaFecha(txtFechaActavisita.Text);
            if (chblTipoFuncionamientoActaVisita.SelectedValue == "")
            {
                _BEEjecucion.id_item = 0;
            }
            else
            {
                _BEEjecucion.id_item = Convert.ToInt32(chblTipoFuncionamientoActaVisita.SelectedValue);
            }
            _BEEjecucion.Evaluacion = txtComentarioFuncionamientoActaVisita.Text;
            if (chblFlagInaugurado.SelectedValue == "")
            {
                _BEEjecucion.flag = 2;
            }
            else
            {
                _BEEjecucion.flag = Convert.ToInt32(chblFlagInaugurado.SelectedValue);
            }
            if (trCumplimientoInicio.Visible == true)
            {
                _BEEjecucion.flagVerificado = chbcumplimiento.SelectedValue;
            }
            else
            {
                _BEEjecucion.flagVerificado = "";
            }
            _BEEjecucion.comentario = "";
            //_BEEjecucion.comentario = txtEntregaDocumentoActaVisita.Text;
            _BEEjecucion.observacion = txtObservacionesActaVisita.Text;
            _BEEjecucion.Recomendacion = txtRecomendacionesActaVisita.Text;
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int valGeneral = _objBLEjecucion.U_MON_EvaluacionRecomendacionActaVisita(_BEEjecucion);
            //////////////////

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionPOPUP.Text);
            _BEEjecucion.id_item = Convert.ToInt32(ddlTipoSistemaDrenaje.SelectedValue);
            _BEEjecucion.observacion = txtComentarioSistemaDrenaje.Text;
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.I_MON_SistemaDrenajeDetalle(_BEEjecucion);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                cargarSistemasDrenajes(lblIdEvaluacionRecomendacionPOPUP.Text);

                Panel_RegistroSistemaDrenajePluvial.Visible = false;
                imgbtnAgregarSistemaDrenaje.Visible = true;

                Up_RegistroActaVisita.Update();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void cargarSistemasDrenajes(string idEvaluacionRecomendacion)
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(idEvaluacionRecomendacion);
            grdSistemaDrenajePluvial.DataSource = _objBLEjecucion.F_MON_SistemaDrenajeDetalle(_BEEjecucion);
            grdSistemaDrenajePluvial.DataBind();
        }

        protected void btnModificarSistemaDrenaje_Click(object sender, EventArgs e)
        {
            _BEEjecucion.id_tabla = Convert.ToInt32(lblIdSistemaDrenaje.Text);
            _BEEjecucion.Tipo = 1;
            _BEEjecucion.id_item = Convert.ToInt32(ddlTipoSistemaDrenaje.SelectedValue);
            _BEEjecucion.observacion = txtComentarioSistemaDrenaje.Text;
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.UD_MON_SistemaDrenajeDetalle(_BEEjecucion);

            if (val == 1)
            {
                string script = "<script>alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                cargarSistemasDrenajes(lblIdEvaluacionRecomendacionPOPUP.Text);

                Panel_RegistroSistemaDrenajePluvial.Visible = false;
                imgbtnAgregarSistemaDrenaje.Visible = true;

                Up_RegistroActaVisita.Update();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void btnCancelarSistemaDrenaje_Click(object sender, EventArgs e)
        {
            Panel_RegistroSistemaDrenajePluvial.Visible = false;
            imgbtnAgregarSistemaDrenaje.Visible = true;

            Up_RegistroActaVisita.Update();

        }

        protected void grdSistemaDrenajePluvial_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grdSistemaDrenajePluvial.SelectedDataKey.Value.ToString();
            lblIdSistemaDrenaje.Text = id.ToString();

            GridViewRow row = grdSistemaDrenajePluvial.SelectedRow;

            ddlTipoSistemaDrenaje.SelectedValue = ((Label)row.FindControl("lblId_tipoSistemaDrenaje")).Text;
            txtComentarioSistemaDrenaje.Text = ((Label)row.FindControl("lblComentario")).Text;

            lblNomSistemaDrenaje.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnModificarSistemaDrenaje.Visible = true;
            btnGuardarSistemaDrenaje.Visible = false;
            Panel_RegistroSistemaDrenajePluvial.Visible = true;
            imgbtnAgregarSistemaDrenaje.Visible = false;

            Up_RegistroActaVisita.Update();
        }

        protected void grdSistemaDrenajePluvial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSistemaDrenajePluvial.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_tabla = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_item = Convert.ToInt32(0);
                _BEEjecucion.observacion = txtComentarioSistemaDrenaje.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_SistemaDrenajeDetalle(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargarSistemasDrenajes(lblIdEvaluacionRecomendacionPOPUP.Text);

                    Panel_RegistroSistemaDrenajePluvial.Visible = false;
                    imgbtnAgregarSistemaDrenaje.Visible = true;

                    Up_RegistroActaVisita.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
    }
}