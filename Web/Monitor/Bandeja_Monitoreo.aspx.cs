﻿using Business;
using Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Bandeja_Monitoreo : System.Web.UI.Page
    {
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();
               
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
              
                string usuario = Session["IdUsuario"].ToString();

                _BE_Bandeja.id_usuario = Convert.ToInt32(usuario);
                _BE_Bandeja.tipo = "1";

                BE_MON_BANDEJA _ENTBandeja = new BE_MON_BANDEJA();
                _ENTBandeja = objBLbandeja.F_spMON_ObtieneInfo(_BE_Bandeja);

                int codSubsector;
                codSubsector = Convert.ToInt32(Session["CodSubsector"].ToString());
                if (codSubsector > 0)
                {
                    cargaSector(ddlSubsector);
                    if (ddlSubsector.SelectedValue == "2" || ddlSubsector.SelectedValue == "3")
                    {
                        ddlSubsector.Enabled = false;
                    }
                    ddlSubsector.SelectedValue = Session["CodSubsector"].ToString();

                }

                cargaTecnico(ddlTecnico);

                if (_ENTBandeja != null)
                {
                    if (_ENTBandeja.nivel == "T")
                    {

                        ddlTecnico.SelectedValue = _ENTBandeja.id_tecnico.ToString();

                        ddlTecnico.Enabled = false;
                    }
                }

                Session["flagMonitor"] = "1";
                cargaUbigeo(ddlDepa, "1", null, null, null);
                ddlDepa.SelectedValue="15";
                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                ddlprov.SelectedValue = "01";
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
                cargaSector(ddlSubsector);

              
                cargaTipo();
                //cargaEstrategia();
                cargaEstado();
                //CargaModalidad();
                cargaBandejaMonitor("1","10");
                //HabilitaOpcionMail();

            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            string script = "<script>$('[data-toggle=\"tooltip\"]').tooltip();</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script88", script, false);
        }
        #region FILTRO

        protected void cargaSector(DropDownList ddl)
        {
            List<BE_MON_BANDEJA> ListSector = new List<BE_MON_BANDEJA>();
            ListSector = objBLbandeja.F_spMON_TipoPrograma();
            ddl.DataSource = ListSector;
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("--TODOS--", ""));
        }
        protected void cargaTipo()
        {
            ddlTipo.DataSource = objBLbandeja.F_spMON_TipoFinanciamiento();
            ddlTipo.DataTextField = "nombre";
            ddlTipo.DataValueField = "valor";
            ddlTipo.DataBind();

            ddlTipo.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        protected void cargaEstado()
        {
            ddlEstado.DataSource = objBLbandeja.F_spMON_FiltroEstadoEjecucion();
            ddlEstado.DataTextField = "nombre";
            ddlEstado.DataValueField = "valor";
            ddlEstado.DataBind();

            ddlEstado.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
    
        protected void cargaTecnico(DropDownList ddl)
        {
            List<BE_MON_BANDEJA> List = new List<BE_MON_BANDEJA>();
            List = objBLbandeja.F_spMON_ListarTecnico(ddlSubsector.SelectedValue);

            List = List.OrderBy(x => x.tecnico).ToList();

            ddl.DataSource = List;
            ddl.DataTextField = "tecnico";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        //protected void CargaModalidad()
        //{
        //    ddlModalidad.Items.Clear();

        //    ddlModalidad.Items.Insert(0, new ListItem("Cooperación Internacional", "2"));
        //    ddlModalidad.Items.Insert(0, new ListItem("Núcleo Ejecutor", "4"));
        //    ddlModalidad.Items.Insert(0, new ListItem("Por Contrata", "3"));
        //    ddlModalidad.Items.Insert(0, new ListItem("Transferencia", "1"));
        //    ddlModalidad.Items.Insert(0, new ListItem("--TODOS--", ""));

        //}
        protected void ddlDepa_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlDepa.SelectedValue != "")
            //{
            //    tdProvincia.Visible = true;
            //    tdProvincia2.Visible = true;

                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
            //}
            //else
            //{
            //    tdProvincia.Visible = false;
            //    tdProvincia2.Visible = false;
            //    tdDistrito.Visible = false;
            //    tdDistrito2.Visible = false;
            //}
            Up_Filtro.Update();
            //cargaBandeja();
        }
        protected void ddlprov_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlprov.SelectedValue != "")
            {
                //tdDistrito.Visible = true;
                //tdDistrito2.Visible = true;

                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
            }
            //else
            //{
            //    tdDistrito.Visible = false;
            //    tdDistrito2.Visible = false;
            //}
            Up_Filtro.Update();
            //cargaBandeja();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
 
        protected void btnBuscar_OnClick(object sender, EventArgs e)
        {
            if (txtanno.Text != "")
            {
                if (!(Convert.ToInt32(txtanno.Text) >= 1900 && Convert.ToInt32(txtanno.Text) <= DateTime.Now.Year))
                {
                    string script = "<script>alert('Rango de Año es de 1900 hasta la actualidad');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return;
                }
            }
            cargaBandeja("1","10");

        }
        protected void cargaBandeja(string starIndex, string maxRows)
        {

            cargaBandejaMonitor(starIndex, maxRows);

            Up_grdBandeja.Update();
        }
   
        protected void btnLimpiar_OnClick(object sender, EventArgs e)
        {
            txtanno.Text = "";
            txtSnipFiltro.Text = "";
            txtProyectoFiltro.Text = "";
            ddlDepa.SelectedValue = "";
            ddlprov.SelectedValue = "";
            ddldist.SelectedValue = "";
            if (ddlSubsector.Enabled == true)
            { ddlSubsector.SelectedValue = ""; }

            if (ddlTecnico.Enabled == true)
            { ddlTecnico.SelectedValue = ""; }

            ddlTipo.SelectedValue = "";
            ddlModalidad.SelectedValue = "";
            ddlEstado.SelectedValue = "";
            ddlEstrategia.SelectedValue = "0";
            Up_Filtro.Update();

            //cargaBandejaMonitor(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue,txtanno.Text);
            //Up_grdBandeja.Update();
        }
      
        #endregion
 
        protected void cargaBandejaMonitor(string starIndex, string maxRows )
        {
            DataTable dtTotal = new DataTable();
            ConsultaMonitoreo pConsulta = new ConsultaMonitoreo();

            pConsulta.snip = txtSnipFiltro.Text;
            pConsulta.depa = ddlDepa.SelectedValue;
            pConsulta.prov = ddlprov.SelectedValue;
            pConsulta.dist = ddldist.SelectedValue;
            pConsulta.tecnico = ddlTecnico.SelectedValue;
            pConsulta.unidad = ddlSubsector.SelectedValue;
            pConsulta.etapaProyecto = ddlTipo.SelectedValue;
            if (ddlEstado.SelectedValue != "")
            {
                pConsulta.nomEstado = ddlEstado.SelectedItem.Text;
            }
            pConsulta.nroPagina = starIndex;
            pConsulta.tamanioPagina = maxRows;


            DataSet dsResult = objBLbandeja.pa_rBandejaMonitoreo(pConsulta);
            int total = Convert.ToInt32(  dsResult.Tables[0].Rows[0][0].ToString());
            grdBandeja.VirtualItemCount = total;
            grdBandeja.DataSource = dsResult.Tables[1];
            grdBandeja.DataBind();
            //LabeltotalAsignado.Text = "Total Asignados: " + ListBandejaMonitor.Count;
        }
 
        protected void grdBandeja_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // CONTROLA PROYECTO ANTIGUO CON REGISTRO FINALIZADO
                Label lblDetalle = (Label)e.Row.FindControl("lblDetalle");
                //dynamic jDyn = JsonConvert.DeserializeObject(lblDetalle.Text);
                Newtonsoft.Json.Linq.JObject jDyn = new Newtonsoft.Json.Linq.JObject();
                //jDyn = JsonConvert.DeserializeObject(lblDetalle.Text);
                DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(lblDetalle.Text);
                DataTable dt = dataSet.Tables["DataDetalleMonitoreo"];
                //lblDetalle.Text = "";
                string html = "";

                Label lblFechaUpdate = (Label)e.Row.FindControl("lblFechaUpdate");
                HtmlGenericControl divFechaUpdate = (HtmlGenericControl)e.Row.FindControl("divFechaUpdate");

                if (lblFechaUpdate.Text == "")
                {
                    lblFechaUpdate.Text = "10/03/2021";
                }

                int dias = (DateTime.Now - Convert.ToDateTime(lblFechaUpdate.Text)).Days;

                if (dias < 15)
                {
                    html = html + "<div class=\"bg-success mt-2\" >" +
                                "Actualizado" +
                                "</div>";
                }
                else if (dias < 60)
                {
                    html = html + "<div class=\"bg-warning mt-2\" >" +
                                  "Desactualizado"  +
                                  "</div>" +
                                  " <small>"+ dias.ToString() + " Días"+
                                    "</ small > ";
                }
                else 
                {
                    html = html + "<div class=\"bg-danger mt-2\" >" +
                                  "Desactualizado" +
                                  "</div>"+
                                       " <small>" + dias.ToString() + " Días" +
                                    "</ small > ";
                }
                
                divFechaUpdate.InnerHtml = html; ;

                foreach (DataRow row in dt.Rows)
                {
                    string vIdEtapa = row["iTipoFinanciamientoGrilla"].ToString();
                    string vIdProyecto = row["xidIdProyecto"].ToString();
                    string vEstado = row["vEstadoEjecucion"].ToString();
                    string vAvance = row["deAvanceFisicoReal"].ToString();

                    switch (vIdEtapa)
                    {
                        case "1":
                            //LinkButton imb1 = (LinkButton)e.Row.FindControl("imgbtnPreInversion");
                            //imb1.Text = "<i class='fa fa-file-text-o'></i>";
                            html = "";
                            HtmlGenericControl divP = (HtmlGenericControl)e.Row.FindControl("divPerfil");

                            html = html + "<a href=" + "Registro_PreInversion?id=" + vIdProyecto + " target='_blank'  class=\"btn btn-outline-primary btn-sm\" data-toggle=\"tooltip\" data-html=\"true\" title=\"" +
                                "<div class='container'>" +
                                "<div class='row'><div class='col-4'>Etapa: </div><div class='col-8 text-left'> PREINVERSIÓN " + "</div></div>" +
                                "<div class='row'><div class='col-4'>Estado: </div><div class='col-8 text-left'> " + vEstado + "</div></div>" +
                                "<div class='row'><div class='col-4'>Avance: </div><div class='col-8 text-left'> " + vAvance + "%</div></div>" +
                                //"<div class='row'><div class='col-8 text-left'><strong>" + "<a class='btn btn-xs' href=" + "Registro_EjecucionContrata?id=" + vIdProyecto + " target='_blank'> Ver Más... </a>" + "</strong>" + "</div></div>" +
                                "</div>\">P" +
                                "</a>";

                            divP.InnerHtml = html; ;
                            // code block
                            break;
                        case "2":
                            //LinkButton imb2 = (LinkButton)e.Row.FindControl("imgbtnExpediente");
                            //imb2.Text = "<i class='fa fa-folder'></i>";
                            html = "";
                            HtmlGenericControl divE = (HtmlGenericControl)e.Row.FindControl("divExpediente");

                            html = html + "<a href=" + "Registro_ExpedienteTecnico?id=" + vIdProyecto + " target='_blank'  class=\"btn btn-outline-primary btn-sm\" data-toggle=\"tooltip\" data-html=\"true\" title=\"" +
                                "<div class='container'>" +
                                "<div class='row'><div class='col-4'>Etapa: </div><div class='col-8 text-left'> EXP. TÉCNICO " + "</div></div>" +
                                "<div class='row'><div class='col-4'>Estado: </div><div class='col-8 text-left'> " + vEstado + "</div></div>" +
                                "<div class='row'><div class='col-4'>Avance: </div><div class='col-8 text-left'> " + vAvance + "%</div></div>" +
                                //"<div class='row'><div class='col-8 text-left'><strong>" + "<a class='btn btn-xs' href=" + "Registro_EjecucionContrata?id=" + vIdProyecto + " target='_blank'> Ver Más... </a>" + "</strong>" + "</div></div>" +
                                "</div>\">E" +
                                "</a>";

                            divE.InnerHtml = html; ;
                            break;
                        case "3":
                            //LinkButton imb3 = (LinkButton)e.Row.FindControl("imgbtnObra");
                            //imb3.Text = "<i class='fa fa-building'></i>";
                            HtmlGenericControl divO = (HtmlGenericControl)e.Row.FindControl("divObra");
                            html = "";
                            html = html + "<a href=" + "Registro_EjecucionContrata?id=" + vIdProyecto + " target='_blank'  class=\"btn btn-outline-primary btn-sm\" data-toggle=\"tooltip\" data-html=\"true\" title=\"" +
                               "<div class='container'>" +
                               "<div class='row'><div class='col-4'>Etapa: </div><div class='col-8 text-left'> OBRA " + "</div></div>" +
                               "<div class='row'><div class='col-4'>Estado: </div><div class='col-8 text-left'> " + vEstado + "</div></div>" +
                               "<div class='row'><div class='col-4'>Avance: </div><div class='col-8 text-left'> " + vAvance + "%</div></div>" +
                               //"<div class='row'><div class='col-8 text-left'><strong>" + "<a class='btn btn-xs' href=" + "Registro_EjecucionContrata?id=" + vIdProyecto + " target='_blank'> Ver Más... </a>" + "</strong>" + "</div></div>" +
                               "</div>\">O" +
                               "</a>";
                            divO.InnerHtml = html; ;
                            break;
                        case "4":
                            //LinkButton imb4 = (LinkButton)e.Row.FindControl("imgbtnMantenimiento");
                            //imb4.Text = "<i class='fa fa-binoculars'></i>";
                            html = "";
                            HtmlGenericControl divM = (HtmlGenericControl)e.Row.FindControl("divMantenimiento");

                            html = html + "<a href=" + "Registro_Mantenimiento?id=" + vIdProyecto + " target='_blank'  class=\"btn btn-outline-primary btn-sm\" data-toggle=\"tooltip\" data-html=\"true\" title=\"" +
                                "<div class='container'>" +
                                "<div class='row'><div class='col-4'>Etapa: </div><div class='col-8 text-left'> MANTENIMIENTO " + "</div></div>" +
                                "<div class='row'><div class='col-4'>Estado: </div><div class='col-8 text-left'> " + vEstado  + "</div></div>" +
                                "<div class='row'><div class='col-4'>Avance: </div><div class='col-8 text-left'> " + vAvance + "%</div></div>" +
                               // "<div class='row'><div class='col-8 text-left'><strong>" + "<a class='btn btn-xs' href=" + "Registro_EjecucionContrata?id=" + vIdProyecto + " target='_blank'> Ver Más... </a>" + "</strong>" + "</div></div>" +
                                "</div>\">M" +
                                "</a>";

                            divM.InnerHtml = html; ;

                            break;
                        default:
                            break;
                    }


                }
               
            }
        }

        protected void grdBandeja_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdBandeja.PageIndex = e.NewPageIndex;
            cargaBandeja((e.NewPageIndex+1).ToString(),"10");
        }
      
        protected void btnReporteResumen_OnClick(object sender, EventArgs e)
        {
            //if (ddlTipoReporte.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccionar tipo de registro.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //}
            //else
            //{
            //    if (ddlTipoReporte.SelectedValue == "1")
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.Anio = txtanno.Text;
            //        if (chbFlagCMD.Checked == true)
            //        {
            //            _BE_Bandeja.flagCMD = "1";
            //        }
            //        else
            //        {
            //            _BE_Bandeja.flagCMD = "";
            //        }

            //        DataTable obj = objBLbandeja.spMON_ReporteGeneralPMIB(_BE_Bandeja);
            //        //obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_XLS(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "2") // REPORTE GENERAL PNSU
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.sector = Session["CodSubsector"].ToString();
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;

            //        DataTable obj = objBLbandeja.spMON_ReporteGeneral(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_ReporteGeneralPNSU(grd2);
     
            //    }

            //    if (ddlTipoReporte.SelectedValue == "3") //Detalle PMIB
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.Anio = txtanno.Text;
            //        if (chbFlagCMD.Checked == true)
            //        {
            //            _BE_Bandeja.flagCMD = "1";
            //        }
            //        else
            //        {
            //            _BE_Bandeja.flagCMD = "";
            //        }

            //        DataTable obj = objBLbandeja.spMON_ReporteDetallePMIB(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_DetallePMIB(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "4") // PNSU DETALLE
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        if (chbFlagCMD.Checked == true)
            //        {
            //            _BE_Bandeja.flagCMD = "1";
            //        }
            //        else
            //        {
            //            _BE_Bandeja.flagCMD = "";
            //        }
            //        DataTable obj = objBLbandeja.spMON_ReporteSuperGeneralPNSU(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");


            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_DetallePNSU(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "5")  // METAS FISICAS PMIB
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        //_BE_Bandeja.sector = ddlSubsector.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.Anio = txtanno.Text;
            //        if (chbFlagCMD.Checked == true)
            //        {
            //            _BE_Bandeja.flagCMD = "1";
            //        }
            //        else
            //        {
            //            _BE_Bandeja.flagCMD = "";
            //        }

            //        DataTable obj = objBLbandeja.spMON_ReporteMetasPMIB(_BE_Bandeja);
            //        // obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_MetasPMIB(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "6")  // OBRAS DESACTUALIZADAS
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            //        _BE_Bandeja.tecnico = ddlTecnico.SelectedValue;

            //        List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();
            //        ListBandeja = objBLbandeja.F_spMON_ReporteObrasDesactualizadas(_BE_Bandeja);
            //        // obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.Columns.Add(new BoundField { DataField = "sector" });
            //        grd2.Columns.Add(new BoundField { DataField = "snip" });
            //        grd2.Columns.Add(new BoundField { DataField = "depa" });
            //        grd2.Columns.Add(new BoundField { DataField = "nombProyecto" });
            //        grd2.Columns.Add(new BoundField { DataField = "Estado" });
            //        grd2.Columns.Add(new BoundField { DataField = "fecha" });
            //        grd2.Columns.Add(new BoundField { DataField = "fisicoEjecutado" });
            //        grd2.Columns.Add(new BoundField { DataField = "tecnico" });

            //        grd2.AutoGenerateColumns = false;
            //        grd2.DataSource = ListBandeja;
            //        grd2.DataBind();

            //        Exportar_ObrasDesactualizadas(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "7")  //REPORTE NE
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;


            //        DataTable obj = objBLbandeja.spMON_ReporteNE(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_NE(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "8") // Seguimiento PNSU
            //    {
            //        DataTable obj = objBLbandeja.spMON_ReporteMonitoreoPNSUparaPNSR();
            //        obj = selectDisctinct(obj, "id_proyecto");


            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_ReportePNSUparaPNSR(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "9") //PROYECTOS PNSR
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.sector = "3";
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.Anio = txtanno.Text;
            //        if (chbFlagCMD.Checked == true)
            //        {
            //            _BE_Bandeja.flagCMD = "1";
            //        }
            //        else
            //        {
            //            _BE_Bandeja.flagCMD = "";
            //        }

            //        DataTable obj = objBLbandeja.spMON_ReporteDetallePMIB(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_DetallePMIB(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "10")  //Detalle Desembolso NE
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;


            //        DataTable obj = objBLbandeja.spMON_ReporteDetalleDevengadoNE(_BE_Bandeja);
            //        //obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_DetalleDevengadoNE(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "11")  //Valor Financiado NE
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;


            //        DataTable obj = objBLbandeja.spMON_ReporteValorFinanciadoNE(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_ValorFinanciadoNE(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "12")  //Seguimiento de NE
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;


            //        DataTable obj = objBLbandeja.spMON_ReporteSeguimientoNE(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_SeguimientoNE(grd2);

            //    }

            //    if (ddlTipoReporte.SelectedValue == "13")  //REPORTE PARALIZADOS
            //    {
            //        _BE_Bandeja.snip = txtSnipFiltro.Text;
            //        _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            //        _BE_Bandeja.depa = ddlDepa.SelectedValue;
            //        _BE_Bandeja.prov = ddlprov.SelectedValue;
            //        _BE_Bandeja.dist = ddldist.SelectedValue;
            //        _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
            //        _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
            //        _BE_Bandeja.sector = ddlSubsector.SelectedValue;
            //        _BE_Bandeja.Anio = txtanno.Text;
            //        if (chbFlagCMD.Checked == true)
            //        {
            //            _BE_Bandeja.flagCMD = "1";
            //        }
            //        else
            //        {
            //            _BE_Bandeja.flagCMD = "";
            //        }

            //        DataTable obj = objBLbandeja.spMON_ReporteParalizados(_BE_Bandeja);
            //        obj = selectDisctinct(obj, "id_proyecto");
            //        GridView grd2 = new GridView();
            //        grd2.DataSource = obj;
            //        grd2.DataBind();

            //        Exportar_Paralizados(grd2);

            //    }

            //}

            //List<BE_MON_BANDEJA> ListBandejaMonitor = new List<BE_MON_BANDEJA>();

            //ListBandejaMonitor = (List<BE_MON_BANDEJA>)Session["Monitoreo"];

            //grdBandeja.DataSource = ListBandejaMonitor;
            //grdBandeja.DataBind();
            //LabeltotalAsignado.Text = "Total Asignados: " + ListBandejaMonitor.Count;
        }
        public static DataTable selectDisctinct(DataTable dt, string columnName)
        {
            try
            {
                if (columnName == null || columnName.Length == 0)
                    throw new ArgumentNullException(columnName, "El parámetro no puede ser nulo");
                DataTable distintos = dt.DefaultView.ToTable(true, columnName);
                DataTable aux = new DataTable();
                foreach (DataColumn dc in dt.Columns)
                    aux.Columns.Add(new DataColumn(dc.Caption, dc.DataType));
                foreach (DataRow dr in distintos.Rows)
                {
                    aux.ImportRow(dt.Select(columnName + " = '" + dr[0] + "'")[0]);
                }
                return aux;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        protected void Exportar_XLS(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Resumen_Monitor_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 1; i < 22; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                }
                for (int i = 22; i < 40; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }



                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                //grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[1].Text = "Subsector";
                grd.HeaderRow.Cells[2].Text = "SNIP";
                grd.HeaderRow.Cells[3].Text = "GESTION";
                grd.HeaderRow.Cells[4].Text = "VRAEM";
                grd.HeaderRow.Cells[5].Text = "FRONTERIZO";
                grd.HeaderRow.Cells[6].Text = "EMERGENCIA TERREMOTO 2007";
                grd.HeaderRow.Cells[6].Width = 100;
                //grd.HeaderRow.Cells[6].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[7].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "PROVINCIA";
                grd.HeaderRow.Cells[8].Width = 150;
                grd.HeaderRow.Cells[9].Text = "DISTRITO";
                grd.HeaderRow.Cells[9].Width = 150;
                grd.HeaderRow.Cells[10].Text = "TIPO FINANCIAMIENTO";
                grd.HeaderRow.Cells[10].Width = 120;
                grd.HeaderRow.Cells[11].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[11].Width = 500;
                grd.HeaderRow.Cells[12].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[13].Text = "MONTO DEL REGISTRO FASE DE INVERSIÓN S/.";
                grd.HeaderRow.Cells[13].Width = 100;
                grd.HeaderRow.Cells[14].Text = "MONTO ACTUALIZO PIP";
                grd.HeaderRow.Cells[14].Width = 100;
                grd.HeaderRow.Cells[15].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[16].Text = "NRO CONVENIO";
                grd.HeaderRow.Cells[17].Text = "FECHA CONVENIO";
                grd.HeaderRow.Cells[18].Text = "AÑO Q SE FINANCIO X PRIMERA VEZ";
                grd.HeaderRow.Cells[18].Width = 100;
                grd.HeaderRow.Cells[19].Text = "N° D.S.";
                grd.HeaderRow.Cells[20].Text = "FECHA D.S.";

                grd.HeaderRow.Cells[21].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[22].Text = "FECHA INICO";
                ////grd.HeaderRow.Cells[19].Width = 150;
                grd.HeaderRow.Cells[23].Text = "PLAZO DE EJECUCION (DIAS)";
                grd.HeaderRow.Cells[23].Width = 80;
                grd.HeaderRow.Cells[24].Text = "TERMINO CONTRACTUAL";
                grd.HeaderRow.Cells[25].Text = "AVANCE FISICO REAL SUSTENTADO";
                grd.HeaderRow.Cells[26].Text = "FECHA VISITA";
                grd.HeaderRow.Cells[27].Text = "AVANCE FISICO PROGRAMADO";
                grd.HeaderRow.Cells[28].Text = "AVANCE FISICO ACUMULADO DE ACUERDO A MONITOREO";
                grd.HeaderRow.Cells[28].Width = 100;
                grd.HeaderRow.Cells[29].Text = "ESTADO DE EJECUCION";
                grd.HeaderRow.Cells[30].Text = "SITUACION DE OBRA";
                grd.HeaderRow.Cells[30].Width = 300;


                grd.HeaderRow.Cells[31].Text = "FECHA VALORIZACION";
                grd.HeaderRow.Cells[32].Text = "MONTO TRANSFERIDO";
                grd.HeaderRow.Cells[33].Text = "% DE TRANSFERENCIA";
                grd.HeaderRow.Cells[34].Text = "MODALIDAD DE EJECUCION";
                grd.HeaderRow.Cells[35].Text = "SISTEMA DE CONTRATACION";
                grd.HeaderRow.Cells[36].Text = "SOSTENIBILIDAD";
                //grd.HeaderRow.Cells[22].Width = 160;
                grd.HeaderRow.Cells[37].Text = "RECOMENDACIONES DE VISITA";

                grd.HeaderRow.Cells[38].Text = "MONITOR";
                grd.HeaderRow.Cells[39].Text = "AMBITO";
                //grd.HeaderRow.Cells[26].Text = "DISTRITO FRONTERIZO";
                //grd.HeaderRow.Cells[27].Text = "ZONA HUALLAGA";
                //grd.HeaderRow.Cells[28].Text = "ZONA HELADA";
                //grd.HeaderRow.Cells[29].Text = "CUENCA RAMIS";
                //grd.HeaderRow.Cells[30].Text = "LISTA";
                //grd.HeaderRow.Cells[31].Text = "SUB-LISTA";
                //grd.HeaderRow.Cells[32].Text = "REVISOR";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    row.Cells[0].Visible = false;

                    for (int j = 0; j < 40; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "RESUMEN DE PROYECTOS FINANCIADOS";
                myTable.ColumnSpan = 28;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                
                //myTable = new TableCell();
                //myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                //myTable.ColumnSpan = 4;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "AMBITO DE INTERVENCION";
                //myTable.ColumnSpan = 6;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                
                //myTable = new TableCell();
                //myTable.Text = "INFORMACIÓN ADICIONAL";
                //myTable.ColumnSpan = 2;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }

        protected void ddlSubsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTecnico.Enabled == true)
            {
                cargaTecnico(ddlTecnico);
            }
            Up_Filtro.Update();
        }
    }

}