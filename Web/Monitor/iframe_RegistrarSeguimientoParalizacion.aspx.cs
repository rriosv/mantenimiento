﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entity;
using Business;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Globalization;
using System.Threading;
using System.Web.Services;
using System.Web.Script.Services;
using WebSCA.googleApi;
using Web.componente;
using System.Collections.Generic;

namespace Web.Monitor
{
    public partial class iframe_RegistrarSeguimientoParalizacion : System.Web.UI.Page
    {
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BLUtil _Metodo = new BLUtil();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {




            if (!IsPostBack)
            {
                lblIdEvaluacionRecomendacionParalizacion.Text = Request.QueryString["idE"].ToString();
                lblSNIP.Text = Request.QueryString["SNIP"].ToString();
                LblID_PROYECTO.Text = Request.QueryString["idPo"].ToString();


                CargarInformacionGeneralParalizacion();
                CargarSeguimientoParalizacion();
             
                Panel_RegistroSeguimientoParalizacion.Visible = false;
                imgbtnAgregarSeguimientoParalizacion.Visible = true;

                Up_SeguimientoParalizacion.Update();

                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();

            }


 

        }


        protected void CargarInformacionGeneralParalizacion()
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
            dt = _objBLEjecucion.spMON_EvaluacionRecomendacionActaVisita(_BEEjecucion);

            txtCoordinadorMVCSParalizado.Text = dt.Rows[0]["coordinadorMVCS"].ToString();
            txtFechaParalizacion.Text = dt.Rows[0]["fechaParalizacion"].ToString();
            txtFechaVisitaDiagnosticoParalizacion.Text = dt.Rows[0]["fechaVisitaDiagnostico"].ToString();
            string IdTipoParalizacion = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
            if (IdTipoParalizacion.Equals("82")) // PERMANENTE
            {
                //txtFechaReinicio.Enabled = false;
                lblFechaFinanciamientoExp.Visible = true;
                txtFechaFinanciamientoExp.Visible = true;
            }

            txtFechaReinicio.Text = dt.Rows[0]["fechaReinicio"].ToString();
            txtFechaFinanciamientoExp.Text = dt.Rows[0]["fechaFinanciamientoExpTecnico"].ToString();
            txtProblematica.Text = dt.Rows[0]["problematica"].ToString();
            txtDetalleProblematicaParalizacion.Text = dt.Rows[0]["problematicaParalizacion"].ToString();
            txtDiagnostico.Text = dt.Rows[0]["diagnosticoParalizacion"].ToString();
            lblNombreActualizacionParalizacion.Text = "Actualizó: " + dt.Rows[0]["usuarioParalizacion"].ToString() + " - " + dt.Rows[0]["fechaUpdateParalizacion"].ToString();
        }

        protected void btnGuardarParalizacion_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroParalizada())
            {
                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
                _BEEjecucion.coordinadorMVCS = txtCoordinadorMVCSParalizado.Text;
                _BEEjecucion.Date_fechaParalizacion = _Metodo.VerificaFecha(txtFechaParalizacion.Text);
                _BEEjecucion.Date_fechaDiagnostico = _Metodo.VerificaFecha(txtFechaVisitaDiagnosticoParalizacion.Text);
                _BEEjecucion.Date_fechaReinicio = _Metodo.VerificaFecha(txtFechaReinicio.Text);
                _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinanciamientoExp.Text);
                _BEEjecucion.Evaluacion = txtDetalleProblematicaParalizacion.Text;
                _BEEjecucion.diagnostico = txtDiagnostico.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.U_MON_EvaluacionRecomendacionParalizacion(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se guardó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargarInformacionGeneralParalizacion();

                    if (ddlTipoMonitoreoSeguimiento.SelectedValue == "3")
                    {
                        lblNombreActaParalizada.Text = "ACTA DE REUNIÓN DE SEGUIMIENTO - OBRAS PARALIZADAS";

                        Panel_InformacionComplementariaParalizada.Visible = true;
                    }
                    else
                    {
                        if (ddlTipoMonitoreoSeguimiento.SelectedValue == "2")
                        {
                            lblNombreActaParalizada.Text = "ACTA DE VISITA DE OBRA PARALIZADA";
                            Panel_InformacionComplementariaParalizada.Visible = true;
                        }
                    }

                    Up_SeguimientoParalizacion.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected Boolean ValidarRegistroParalizada()
        {
            Boolean result;
            result = true;

            if (txtFechaParalizacion.Text.Length == 0)
            {
                string script = "<script>alert('Ingresar fecha de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaVisitaDiagnosticoParalizacion.Text.Length == 0)
            {
                string script = "<script>alert('Ingresar fecha visita de diagnostico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaVisitaDiagnosticoParalizacion.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaVisitaDiagnosticoParalizacion.Text) == false)
                {
                    string script = "<script>alert('Formato invalido de fecha visita de diagnostico. El formato debe ser dd/mm/yyyy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }


                if (_Metodo.VerificaFecha(txtFechaVisitaDiagnosticoParalizacion.Text) < _Metodo.VerificaFecha(txtFechaParalizacion.Text))
                {
                    string script = "<script>alert('La fecha de visita de diagnostico debe ser mayor y/o igual a la fecha de paralización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaReinicio.Text.Length > 0)
            {

                if (_Metodo.ValidaFecha(txtFechaReinicio.Text) == false)
                {
                    string script = "<script>alert('Formato invalido de fecha de reinicio. El formato debe ser dd/mm/yyyy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (_Metodo.VerificaFecha(txtFechaReinicio.Text) <= _Metodo.VerificaFecha(txtFechaParalizacion.Text))
                {
                    string script = "<script>alert('La fecha de reinicio debe ser mayor a la fecha de paralización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                if (txtFechaVisitaDiagnosticoParalizacion.Text.Length > 0)
                {
                    if (_Metodo.VerificaFecha(txtFechaReinicio.Text) <= _Metodo.VerificaFecha(txtFechaVisitaDiagnosticoParalizacion.Text))
                    {
                        string script = "<script>alert('La fecha de reinicio debe ser mayor a la fecha de visita de diagnostico.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }

            }

            if (txtFechaFinanciamientoExp.Text.Length > 0 && txtFechaFinanciamientoExp.Visible == true)
            {
                if (_Metodo.ValidaFecha(txtFechaFinanciamientoExp.Text) == false)
                {
                    string script = "<script>alert('Formato invalido de fecha de financiamiento de Exp. Técnico. El formato debe ser dd/mm/yyyy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (_Metodo.VerificaFecha(txtFechaFinanciamientoExp.Text) <= _Metodo.VerificaFecha(txtFechaParalizacion.Text))
                {
                    string script = "<script>alert('La fecha de finciamiento del Exp. Técnico debe ser mayor a la fecha de paralización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            return result;
        }

        protected void CargarSeguimientoParalizacion()
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
            dt = _objBLEjecucion.spMON_SeguimientoParalizacion(_BEEjecucion);
            grdSeguimientoParalizacion.DataSource = dt;
            grdSeguimientoParalizacion.DataBind();

        }

        protected Boolean validaArchivo(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hata 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }
        protected void imgbtnAgregarSeguimientoParalizacion_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RegistroSeguimientoParalizacion.Visible = true;
            imgbtnAgregarSeguimientoParalizacion.Visible = false;

            btnGuardarSeguimientoParalizacion.Visible = true;
            btnModificarSeguimientoParalizacion.Visible = false;

            ddlTipoMonitoreoSeguimiento.SelectedValue = "";
            txtFechaSeguimiento.Text = "";
            txtSituacionActualSeguimiento.Text = "";
            txtAccionesRealizadasSeguimiento.Text = "";
            txtAccionesPorRealizar.Text = "";

            lnkbtnInformeSeguimiento.Text = "";
            imgbtnInformeSeguimiento.ImageUrl = "~/img/blanco.png";

            lnkbtnOficioSeguimiento.Text = "";
            imgbtnOficioSeguimiento.ImageUrl = "~/img/blanco.png";
            lnkbtnActaSeguimiento.Text = "";
            imgbtnActaSeguimiento.ImageUrl = "~/img/blanco.png";

            lblNombreActualizaSeguimientoParalizada.Text = "";

            Panel_InformacionComplementariaParalizada.Visible = false;

            Up_SeguimientoParalizacion.Update();
        }

        protected void grdSeguimientoParalizacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoParalizacion.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.id_tipomonitoreo = "0";
                _BEEjecucion.Date_fecha = _Metodo.VerificaFecha(txtFechaSeguimiento.Text);
                _BEEjecucion.situacion = txtSituacionActualSeguimiento.Text;
                _BEEjecucion.accionesRealizadas = txtAccionesRealizadasSeguimiento.Text;
                _BEEjecucion.accionesPorRealizar = txtAccionesPorRealizar.Text;
                _BEEjecucion.UrlInforme = "";
                _BEEjecucion.UrlOficio = "";
                _BEEjecucion.UrlActa = "";
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Tipo = 2; // Eliminar

                int val = _objBLEjecucion.UD_MON_SeguimientoParalizacion(_BEEjecucion);


                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    CargarSeguimientoParalizacion();
                    Up_SeguimientoParalizacion.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void btnCancelarSeguimientoParalizacion_Click(object sender, EventArgs e)
        {
            Panel_RegistroSeguimientoParalizacion.Visible = false;
            imgbtnAgregarSeguimientoParalizacion.Visible = true;
            Up_SeguimientoParalizacion.Update();
        }

        protected Boolean ValidarRegistroSeguimientoParalizacion()
        {
            Boolean result;
            result = true;

            if (txtFechaSeguimiento.Text.Length == 0)
            {
                string script = "<script>alert('Ingresar fecha de seguimiento de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtFechaSeguimiento.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaSeguimiento.Text) == false)
                {
                    string script = "<script>alert('Formato invalido de fecha de seguimiento. El formato debe ser dd/mm/yyyy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }



            if (ddlTipoMonitoreoSeguimiento.SelectedValue == "")
            {

                string script = "<script>alert('Seleccionar tipo de monitoreo de seguimiento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }

        protected void btnModificarSeguimientoParalizacion_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroSeguimientoParalizacion())
            {
                if (validaArchivo(FileUploadActaSeguimiento))
                {
                    if (validaArchivo(FileUploadInformeSeguimiento))
                    {
                        if (validaArchivo(FileUploadOficioSeguimiento))
                        {
                            _BEEjecucion.id_item = Convert.ToInt32(lblIdSeguimientoParalizacion.Text);
                            _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreoSeguimiento.SelectedValue;
                            _BEEjecucion.Date_fecha = _Metodo.VerificaFecha(txtFechaSeguimiento.Text);
                            _BEEjecucion.situacion = txtSituacionActualSeguimiento.Text;
                            _BEEjecucion.accionesRealizadas = txtAccionesRealizadasSeguimiento.Text;
                            _BEEjecucion.accionesPorRealizar = txtAccionesPorRealizar.Text;
                            _BEEjecucion.UrlInforme = (FileUploadInformeSeguimiento.PostedFile.ContentLength > 0) ? _Metodo.uploadfile(FileUploadInformeSeguimiento) : lnkbtnInformeSeguimiento.Text;
                            _BEEjecucion.UrlOficio = (FileUploadOficioSeguimiento.PostedFile.ContentLength > 0) ? _Metodo.uploadfile(FileUploadOficioSeguimiento) : lnkbtnOficioSeguimiento.Text;
                            _BEEjecucion.UrlActa = (FileUploadActaSeguimiento.PostedFile.ContentLength > 0) ? _Metodo.uploadfile(FileUploadActaSeguimiento) : lnkbtnActaSeguimiento.Text;
                            _BEEjecucion.Tipo = 1; // Modificar
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLEjecucion.UD_MON_SeguimientoParalizacion(_BEEjecucion);

                            if (val == 1)
                            {
                                string script = "<script>alert('Se Actualizo correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                CargarSeguimientoParalizacion();
                                Up_SeguimientoParalizacion.Update();
                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }
                        }
                    }
                }
            }
        }
        protected void btnGuardarSeguimientoParalizacion_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroSeguimientoParalizacion())
            {
                if (validaArchivo(FileUploadActaSeguimiento))
                {
                    if (validaArchivo(FileUploadInformeSeguimiento))
                    {
                        if (validaArchivo(FileUploadOficioSeguimiento))
                        {
                            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
                            _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreoSeguimiento.SelectedValue;
                            _BEEjecucion.Date_fecha = _Metodo.VerificaFecha(txtFechaSeguimiento.Text);
                            _BEEjecucion.situacion = txtSituacionActualSeguimiento.Text;
                            _BEEjecucion.accionesRealizadas = txtAccionesRealizadasSeguimiento.Text;
                            _BEEjecucion.accionesPorRealizar = txtAccionesPorRealizar.Text;
                            _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUploadActaSeguimiento);
                            _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUploadInformeSeguimiento);
                            _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUploadOficioSeguimiento);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLEjecucion.I_MON_SeguimientoParalizacion(_BEEjecucion);

                            if (val > 0)
                            {
                                lblIdSeguimientoParalizacion.Text = val.ToString();
                                lblIdFicha.Text = "0";

                                CargarSeguimientoParalizacion();

                                if (ddlTipoMonitoreoSeguimiento.SelectedValue == "3")
                                {
                                    lblMsjRegistroActa.Text = "¿Desea continuar con el registro del <b>ACTA DE REUNIÓN DE SEGUIMIENTO - OBRAS PARALIZADAS (Asistencia Técnica)</b>?";
                                    MPE_PREGUNTA.Show();
                                }
                                else
                                {
                                    if (ddlTipoMonitoreoSeguimiento.SelectedValue == "2")
                                    {
                                        lblMsjRegistroActa.Text = "¿Desea continuar con el registro del <b>ACTA DE VISITA DE OBRA PARALIZADA</b>?";
                                        MPE_PREGUNTA.Show();
                                    }
                                    else
                                    {

                                        string script = "<script>alert('Se registró correctamente el seguimiento de la paralización de obra. ');</script>";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                    }
                                }

                                Panel_RegistroSeguimientoParalizacion.Visible = false;
                                imgbtnAgregarSeguimientoParalizacion.Visible = true;
                                Up_SeguimientoParalizacion.Update();

                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }
                        }
                    }
                }
            }
        }

        protected void lnkbtnInformeSeguimiento_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnInformeSeguimiento.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnOficioSeguimiento_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnOficioSeguimiento.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocInformeSeguimiento_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //int id_ampliacion = Convert.ToInt32(grdSeguimientoParalizacion.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdSeguimientoParalizacion.Rows[row.RowIndex].FindControl("imgDocInformeSeguimiento");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocOficioSeguimiento_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //int id_ampliacion = Convert.ToInt32(grdSeguimientoParalizacion.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdSeguimientoParalizacion.Rows[row.RowIndex].FindControl("imgDocOficioSeguimiento");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void grdSeguimientoParalizacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDocInformeSeguimiento = (ImageButton)e.Row.FindControl("imgDocInformeSeguimiento");
                GeneraIcoFile(imgDocInformeSeguimiento.ToolTip, imgDocInformeSeguimiento);

                ImageButton imgDocOficioSeguimiento = (ImageButton)e.Row.FindControl("imgDocOficioSeguimiento");
                GeneraIcoFile(imgDocOficioSeguimiento.ToolTip, imgDocOficioSeguimiento);

                ImageButton imgDocActaInformeSeguimiento = (ImageButton)e.Row.FindControl("imgDocActaInformeSeguimiento");
                GeneraIcoFile(imgDocActaInformeSeguimiento.ToolTip, imgDocActaInformeSeguimiento);

                string lblIdTipoMonitoreo = ((Label)e.Row.FindControl("lblIdTipoMonitoreo")).Text;

                if (lblIdTipoMonitoreo == "3")
                {
                    Label lblNombreActa = ((Label)e.Row.FindControl("lblNombreActa"));
                    lblNombreActa.Text = "ASISTENCIA TÉCNICA";
                }
                else
                {
                    if (lblIdTipoMonitoreo == "2")
                    {
                        Label lblNombreActa = ((Label)e.Row.FindControl("lblNombreActa"));
                        lblNombreActa.Text = "ACTA DE VISITA";
                    }
                    else
                    {
                        LinkButton lnkbtnVerActa = ((LinkButton)e.Row.FindControl("lnkbtnVerActa"));
                        lnkbtnVerActa.Visible = false;

                    }
                }

            }
        }

        protected void grdSeguimientoParalizacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblIdSeguimientoParalizacion.Text = grdSeguimientoParalizacion.SelectedDataKey.Value.ToString();

            GridViewRow row = grdSeguimientoParalizacion.SelectedRow;
            lblIdFicha.Text = ((Label)row.FindControl("lblIdFicha")).Text;

            ddlTipoMonitoreoSeguimiento.SelectedValue = ((Label)row.FindControl("lblIdTipoMonitoreo")).Text;
            txtFechaSeguimiento.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtSituacionActualSeguimiento.Text = ((Label)row.FindControl("lblSituacionActual")).Text;
            txtAccionesRealizadasSeguimiento.Text = ((Label)row.FindControl("lblAccionesRealizadas")).Text;
            txtAccionesPorRealizar.Text = ((Label)row.FindControl("lblAccionesPorRealizar")).Text;

            lnkbtnInformeSeguimiento.Text = ((ImageButton)row.FindControl("imgDocInformeSeguimiento")).ToolTip;
            GeneraIcoFile(lnkbtnInformeSeguimiento.Text, imgbtnInformeSeguimiento);

            lnkbtnOficioSeguimiento.Text = ((ImageButton)row.FindControl("imgDocOficioSeguimiento")).ToolTip;
            GeneraIcoFile(lnkbtnOficioSeguimiento.Text, imgbtnOficioSeguimiento);

            lnkbtnActaSeguimiento.Text = ((ImageButton)row.FindControl("imgDocActaInformeSeguimiento")).ToolTip;
            GeneraIcoFile(lnkbtnActaSeguimiento.Text, imgbtnActaSeguimiento);

            lblNombreActualizaSeguimientoParalizada.Text = "Actualizó: " + ((Label)row.FindControl("lblUsuario")).Text + " - " + ((Label)row.FindControl("lblFechaUpdate")).Text;

            Panel_RegistroSeguimientoParalizacion.Visible = true;
            imgbtnAgregarSeguimientoParalizacion.Visible = false;

            btnGuardarSeguimientoParalizacion.Visible = false;
            btnModificarSeguimientoParalizacion.Visible = true;

            if (ddlTipoMonitoreoSeguimiento.SelectedValue == "3")
            {
                lblNombreActaParalizada.Text = "ACTA DE REUNIÓN DE SEGUIMIENTO - OBRAS PARALIZADAS";

                Panel_InformacionComplementariaParalizada.Visible = true;
            }
            else
            {
                if (ddlTipoMonitoreoSeguimiento.SelectedValue == "2")
                {
                    lblNombreActaParalizada.Text = "ACTA DE VISITA DE OBRA PARALIZADA";
                    Panel_InformacionComplementariaParalizada.Visible = true;
                }
                else
                {
                    Panel_InformacionComplementariaParalizada.Visible = false;
                }
            }


            Up_SeguimientoParalizacion.Update();

        }

        protected void GeneraIcoFile(string lnk, ImageButton imgbtn)
        {
            if (lnk != "")
            {

                string ext = lnk.Substring(lnk.Length - 3, 3);

                ext = ext.ToLower();

                if (ext == "pdf")
                {
                    imgbtn.ImageUrl = "~/img/pdf.gif";
                }

                if (ext == "xls" || ext == "lsx")
                {
                    imgbtn.ImageUrl = "~/img/xls.gif";

                }

                if (ext == "doc" || ext == "ocx")
                {
                    imgbtn.ImageUrl = "~/img/doc.gif";

                }

                if (ext == "png" || ext == "PNG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }
                if (ext == "jpg" || ext == "JPG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "peg" || ext == "PEG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "bmp" || ext == "BMP")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                imgbtn.ToolTip = lnk;
            }
            else
            {
                imgbtn.ToolTip = "";
                imgbtn.ImageUrl = "~/img/blanco.png";
            }

        }


        protected void lnkbtnRegistrarActaParalizada_Click(object sender, EventArgs e)
        {
            //ifrmInformeVisita.Attributes["src"] = "iframe_RegistrarInformeVisita.aspx?idE=" + lblID_evaluacionTab2.Text + "&idPo=" + LblID_PROYECTO.Text;
            string url = "";
            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
            dt = _objBLEjecucion.spMON_SeguimientoParalizacion(_BEEjecucion);

            DataRow[] result = dt.Select("id_seguimientoParalizacion = '" + lblIdSeguimientoParalizacion.Text + "'");
            lblIdFicha.Text = result[0]["id_ficha"].ToString();

            if (ddlTipoMonitoreoSeguimiento.SelectedValue == "3")
            {
                lblTituloActa.Text = "REGISTRO DE REUNIÓN DE SEGUIMIENTO (ASISTENCIA TECNICA)";

                string pNombreProyecto = Request.QueryString["pNombreProyecto"].ToString();
                url = UrlFrontEnd + "Ficha/Ficha05/" + Session["clave"].ToString() + "?pIdProyecto=0&pIdSolicitud=0&pIdRegistro=" + lblIdFicha.Text + "&pSnip=" + lblSNIP.Text + "&pIdFicha=5&pIdSeguimiento=" + lblIdSeguimientoParalizacion.Text + "&pNombreProyecto=" + pNombreProyecto;
                //ifrmRegistroActa.Attributes["src"] = url;

            }
            if (ddlTipoMonitoreoSeguimiento.SelectedValue == "2")
            {
                lblTituloActa.Text = "REGISTRO DE VISITA DE OBRA PARALIZADA";
                //ifrmRegistrarActa.Attributes["src"] = UrlFrontEnd + "Ficha/Ficha03?pIdProyecto=0&pIdSolicitud=0&pIdRegistro=0&pSnip=0&pIdFicha=3&pIdSeguimiento=" +lblIdSeguimientoParalizacion.Text;
                url = UrlFrontEnd + "Ficha/Ficha03/" + Session["clave"].ToString() + "?pIdProyecto=0&pIdSolicitud=0&pIdRegistro=" + lblIdFicha.Text + "&pSnip=" + lblSNIP.Text + "&pIdFicha=3&pIdSeguimiento=" + lblIdSeguimientoParalizacion.Text;
                //ifrmRegistroActa.Attributes["src"] = url;
            }

            Up_tituloRegistroActa.Update();
            Up_RegistroActa.Update();

            // string script = "<script>window.open('" + url + "&t=r','_blank','width=1500,height=900,scrollbars=yes') </script>";
            string script = "<script>windowOpener('" + url + "&t=r')</script>";


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //MPE_ACTA.Show();
            //Up_RegistroActaMPE.Update();

            //string script = "<script>ShowPopupRegistroActaVisita();</script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void lnkbtnVerActa_Click(object sender, EventArgs e)
        {
            LinkButton boton = default(LinkButton);
            GridViewRow row = default(GridViewRow);
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string idSeguimiento = grdSeguimientoParalizacion.DataKeys[row.RowIndex]["id_seguimientoParalizacion"].ToString();
            Label lblIdTipoMonitoreo = (Label)grdSeguimientoParalizacion.Rows[row.RowIndex].FindControl("lblIdTipoMonitoreo");
            Label lblIdFichaPoppup = (Label)grdSeguimientoParalizacion.Rows[row.RowIndex].FindControl("lblIdFicha");

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
            dt = _objBLEjecucion.spMON_SeguimientoParalizacion(_BEEjecucion);

            DataRow[] result = dt.Select("id_seguimientoParalizacion = " + idSeguimiento + "");
            lblIdFichaPoppup.Text = result[0]["id_ficha"].ToString();

            string url = "";
            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();

            if (lblIdTipoMonitoreo.Text == "3")
            {
                string pNombreProyecto = Request.QueryString["pNombreProyecto"].ToString();
                url = UrlFrontEnd + "Ficha/Ficha05/" + Session["clave"].ToString() + "?pIdProyecto=" + LblID_PROYECTO.Text + "&pIdSolicitud=0&pIdRegistro=" + lblIdFichaPoppup.Text + "&pSnip=" + lblSNIP.Text + "&pIdFicha=5&pIdSeguimiento=" + idSeguimiento + "&print=1" + "&pNombreProyecto=" + pNombreProyecto;

                string script = "<script>window.open('" + url + "&t=r','_blank','width=1500,height=900,scrollbars=yes') </script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (lblIdTipoMonitoreo.Text == "2")
            {

                url = UrlFrontEnd + "Ficha/Ficha03/" + Session["clave"].ToString() + "?pIdProyecto=" + LblID_PROYECTO.Text + "&pIdSolicitud=0&pIdRegistro=" + lblIdFichaPoppup.Text + "&pSnip=" + lblSNIP.Text + "&pIdFicha=3&pIdSeguimiento=" + idSeguimiento + "&print=1";

                string script = "<script>window.open('" + url + "&t=r','_blank','width=1500,height=900,scrollbars=yes') </script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        }

        protected void btnSIContinuar_Click(object sender, EventArgs e)
        {
            string url = "";
            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
            dt = _objBLEjecucion.spMON_SeguimientoParalizacion(_BEEjecucion);

            DataRow[] result = dt.Select("id_seguimientoParalizacion = '" + lblIdSeguimientoParalizacion.Text + "'");
            lblIdFicha.Text = result[0]["id_ficha"].ToString();


            if (ddlTipoMonitoreoSeguimiento.SelectedValue == "3")
            {
                lblTituloActa.Text = "REGISTRO DE REUNIÓN DE SEGUIMIENTO (ASISTENCIA TECNICA)";
                string pNombreProyecto = Request.QueryString["pNombreProyecto"].ToString();
                url = UrlFrontEnd + "Ficha/Ficha05/" + Session["clave"].ToString() + "?pIdProyecto=0&pIdSolicitud=0&pIdRegistro=" + lblIdFicha.Text + "&pSnip=" + lblSNIP.Text + "&pIdFicha=5&pIdSeguimiento=" + lblIdSeguimientoParalizacion.Text + "&pNombreProyecto=" + pNombreProyecto;
            }
            if (ddlTipoMonitoreoSeguimiento.SelectedValue == "2")
            {
                lblTituloActa.Text = "REGISTRO DE VISITA DE OBRA PARALIZADA";
                url = UrlFrontEnd + "Ficha/Ficha03/" + Session["clave"].ToString() + "?pIdProyecto=0&pIdSolicitud=0&pIdRegistro=" + lblIdFicha.Text + "&pSnip=" + lblSNIP.Text + "&pIdFicha=3&pIdSeguimiento=" + lblIdSeguimientoParalizacion.Text;
            }

            Up_tituloRegistroActa.Update();
            Up_RegistroActa.Update();

            MPE_PREGUNTA.Hide();

            // string script = "<script>window.open('" + url + "&t=r','_blank','width=1500,height=900,scrollbars=yes') </script>";
            string script = "<script>windowOpener('" + url + "&t=r')</script>";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void btnNOContinuar_Click(object sender, EventArgs e)
        {
            MPE_PREGUNTA.Hide();

        }

        protected void imgbtnAgregarCoodinador_Click(object sender, ImageClickEventArgs e)
        {
            Panel_Coordinador.Visible = true;
            btnGuardar_Coordinador.Visible = true;
            btnModificar_Coordinador.Visible = false;

            dt = CargaEmpleadoParalizacion(5);
            grd_HistorialCoordinador.DataSource = dt;
            grd_HistorialCoordinador.DataBind();

            Up_SeguimientoParalizacion.Update();
        }


        protected DataTable CargaEmpleadoParalizacion(int tipo)
        {
            DataTable dt;
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoParalizacion(_BEEjecucion);
            return dt;
        }

        protected void grd_HistorialCoordinador_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminarCoordinador")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialCoordinador.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;//PARA LA ELIMINACION
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;
                int val = _objBLEjecucion.spud_MON_EmpleadoParalizacion(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;

                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoParalizacion(5);
                    if (dt.Rows.Count > 0)
                    {
                        txtCoordinadorMVCSParalizado.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCoordinadorMVCSParalizado.Text = "";
                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();

                    Panel_Coordinador.Visible = false;
                    Up_SeguimientoParalizacion.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grd_HistorialCoordinador_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialCoordinador.SelectedDataKey.Value.ToString();
            lblIdCoordinadorM.Text = id;
            GridViewRow row = grd_HistorialCoordinador.SelectedRow;

            txtCoordinador.Text = ((Label)row.FindControl("lblnombreS")).Text;
            txtFechaC.Text = ((Label)row.FindControl("lblFechaSS")).Text;

            btnGuardar_Coordinador.Visible = false;
            btnModificar_Coordinador.Visible = true;
            Panel_Coordinador.Visible = true;
            Up_SeguimientoParalizacion.Update();
        }

        protected void btnGuardar_Coordinador_Click(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de coordinador MVCS.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIdEvaluacionRecomendacionParalizacion.Text);
                _BEEjecucion.tipoempleado = 5; // COORDINDOR MVCS PARALIZADO

                _BEEjecucion.empleadoObra = txtCoordinador.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                _BEEjecucion.Date_fecha = _Metodo.VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spi_MON_EmpleadoParalizacion(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró coordinador MVCS correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //txtCordinadorTab2.Text = txtCoordinador.Text ;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoParalizacion(5);
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    txtCoordinadorMVCSParalizado.Text = dt.Rows[0]["nombre"].ToString();

                    Panel_Coordinador.Visible = false;
                    Up_SeguimientoParalizacion.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void btnModificar_Coordinador_Click(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIdCoordinadorM.Text);
                _BEEjecucion.Tipo = 1;//valor q indica la modificacion
                                      // _BEEjecucion.empleadoObra = txtu.Text;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = _Metodo.VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spud_MON_EmpleadoParalizacion(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el coordinador correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoParalizacion(5);
                    if (dt.Rows.Count > 0)
                    {
                        txtCoordinadorMVCSParalizado.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCoordinadorMVCSParalizado.Text = "";

                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    Panel_Coordinador.Visible = false;
                    btnModificar_Coordinador.Visible = false;
                    Up_SeguimientoParalizacion.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void btnCancelar_Coordinador_Click(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = false;
            Up_SeguimientoParalizacion.Update();
        }

        protected void lnkbtnActaSeguimiento_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnActaSeguimiento.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocActaInformeSeguimiento_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            //int id_ampliacion = Convert.ToInt32(grdSeguimientoParalizacion.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdSeguimientoParalizacion.Rows[row.RowIndex].FindControl("imgDocActaInformeSeguimiento");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string getToken()
        {
            VGoogleDriveApi vGAA = new VGoogleDriveApi();
            return vGAA.getAccessToken().ToString();
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string ListarAcciones(int id)
        {
            BL_MON_Ejecucion _objBLEjecucion2 = new BL_MON_Ejecucion();
            DataSet ds = _objBLEjecucion2.spMON_EvaluacionRecomendacionAccionesSeleccionar(id);
            string vResultado = "";
            vResultado = "{" + UtilsJSON.serializarJsonResponse(UtilsJSON.agregarJsonMensajeDS(ds)) + "}";
            return vResultado;

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GuardarAcciones(List<BE_MON_EvaluacionRecomendacionAcciones> acciones)
        {

            //armamos el datatable para enviar las acciones
            DataTable DtAcciones = new DataTable();
            DtAcciones.Columns.Add("id_evaluacionRecomendacion", typeof(int));
            DtAcciones.Columns.Add("Tipo", typeof(int));
            DtAcciones.Columns.Add("IdAccion", typeof(int));
            DtAcciones.Columns.Add("Fecha", typeof(DateTime));
            DtAcciones.Columns.Add("Documento", typeof(string));
            DtAcciones.Columns.Add("NombreDocumento", typeof(string));
            DtAcciones.Columns.Add("Monto", typeof(decimal));
            DtAcciones.Columns.Add("Activo", typeof(bool));


            foreach (BE_MON_EvaluacionRecomendacionAcciones OBJERA in acciones)
            { 
                DataRow DR = DtAcciones.NewRow();
                DR["id_evaluacionRecomendacion"] = OBJERA.id_evaluacionRecomendacion;
                DR["Tipo"] = OBJERA.Tipo;
                DR["IdAccion"] = OBJERA.IdAccion;
                if (OBJERA.Fecha != null)
                    DR["Fecha"] = OBJERA.Fecha;
                DR["Documento"] = OBJERA.Documento;
                DR["NombreDocumento"] = OBJERA.NombreDocumento;
                DR["Monto"] = DBNull.Value;
                DR["Activo"] = OBJERA.Activo;
                DtAcciones.Rows.Add(DR);

            }

            BL_MON_Ejecucion _objBLEjecucion2 = new BL_MON_Ejecucion();
            DataSet ds = _objBLEjecucion2.U_MON_GuardarRecomendacionAcciones(DtAcciones);
            string vResultado = "";
            vResultado = "{" + UtilsJSON.serializarJsonResponse(UtilsJSON.agregarJsonMensajeDS(ds)) + "}";
            return vResultado;

        }

    }
}