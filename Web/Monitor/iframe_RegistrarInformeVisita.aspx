﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iframe_RegistrarInformeVisita.aspx.cs" Inherits="Web.Monitor.iframe_RegistrarInformeVisita"  Culture="es-PE" UICulture="es-PE" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link href="../css/Monitoreo.css" rel="stylesheet" type="text/css" />
     <script src="../js/pace.min.js"></script>
    <title></title>
    <link href="../css/pace.flash.css" rel="stylesheet" />
    <style type="text/css">
        body {
            font-family:Calibri;
        }
    </style>

    <asp:PlaceHolder runat="server">
        <%:System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>

        <center>
            <br />
            <asp:Label ID="LblID_USUARIO" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="LblID_PROYECTO" runat="server" Text="" Visible="false"></asp:Label>

            <asp:UpdatePanel runat="server" ID="Up_RegistroInforme" UpdateMode="Conditional">
                <ContentTemplate>
                   
                        <table style="font-size: 10pt; width: 91%">
                            <tr>
                                <td align="left" style="font-weight: bold">ENCABEZADO DE INFORME:</td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td>Número de Informe :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtNumeroInforme" runat="server" Width="400" placeholder="Informe N°xxx-2017-VIVIENDA/VMCS/PNSU....."></asp:TextBox>
                                                <asp:Label ID="lblIdRecomendacionInformeMonitoreoPOPU" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Dirigido a :</td>
                                            <td>
                                                <asp:TextBox ID="txtDestinatario" runat="server" Width="400"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>Cargo de Destinatario :</td>
                                            <td>
                                                <asp:TextBox ID="txtCargoDestinatario" runat="server" Width="350"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Asunto :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtAsuntoInforme" runat="server" Width="400"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Referencia :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtReferenciaInforme" runat="server" Width="400"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Fecha: </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtFechaInforme" runat="server" Width="80" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender259" runat="server" TargetControlID="txtFechaInforme"
                                                    FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                <asp:CalendarExtender ID="CalendarExtender28" runat="server" TargetControlID="txtFechaInforme"
                                                    Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                                <b>Cabecera de Informe:</b><br />
                                                (Deberá ingresar el nombre y código del proyecto visitado.) </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <%--<asp:TextBox ID="txtCabeceraInforme" runat="server" Width="98%" Height="150px" TextMode="MultiLine" 
                                                    placeholder="Deberá ingresar el nombre de proyecto visitado y código. "></asp:TextBox>--%>
                                              
                                                <CKEditor:CKEditorControl ID="txtCabeceraInforme" runat="server" Width="100%" Height="200px"
                                                    ToolbarBasic="NewPage|Preview&#13;&#10;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"
                                                    
                                                    ></CKEditor:CKEditorControl>&nbsp;
       
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>

                            <tr>
                                <td style="height: 10px"></td>
                            </tr>

                            <tr>
                                <td align="left" style="font-weight: bold">ANTECEDENTES :</td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>

                            <tr>
                                <td align="left">

                                    <CKEditor:CKEditorControl ID="txtAntecedentesInforme" runat="server" Width="98%" Height="250px" MaxLength="2000"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
                                </td>
                            </tr>


                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <%-- <tr>
                                <td align="left" style="font-weight: bold">METAS DEL PROYECTO :</td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>--%>

                            <tr>
                                <td align="left">
                                    <asp:Label ID="txtMetasInforme" runat="server" TextMode="MultiLine" Width="740" Height="250px" Visible="false"></asp:Label>
                                    <%--       <CKEditor:CKEditorControl ID="txtMetasInforme" runat="server" Width="98%" Height="250px" 
                         ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
                                    --%>
                                </td>
                            </tr>

                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-weight: bold">ASPECTO FINANCIERO :</td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>

                            <tr>
                                <td align="left">
                                    <%--<asp:TextBox ID="txtAspectoFinancieroInforme" runat="server" TextMode="MultiLine" Width="740" Height="250px"></asp:TextBox>--%>
                                    <CKEditor:CKEditorControl ID="txtAspectoFinancieroInforme" runat="server" Width="98%" Height="250px"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                </td>
                            </tr>

                            <tr>
                                <td style="height: 10px"></td>
                            </tr>

                            <tr>
                                <td align="left" style="font-weight: bold">ANALISIS :</td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="linea.png" width="100%" height="10px" /></td>
                            </tr>

                            <tr>
                                <td align="left">
                                    <%--<asp:TextBox ID="txtAnalisisInforme" runat="server" TextMode="MultiLine" Width="740px" Height="250px"></asp:TextBox>--%>
                                    <CKEditor:CKEditorControl ID="txtAnalisisInforme" runat="server" Width="98%" Height="250px"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>


                            <tr>
                                <td align="left" style="font-weight: bold">OBSERVACIONES :</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <%--<asp:TextBox ID="txtConclusionesInforme" runat="server" TextMode="MultiLine" Width="740px" Height="250px"></asp:TextBox>--%>
                                    <CKEditor:CKEditorControl ID="txtConclusionesInforme" runat="server" Width="98%" Height="250px"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-weight: bold">RECOMENDACIONES :</td>
                            </tr>
                            <tr>
                                <td>
                                    <%--<asp:TextBox ID="txtRecomendacionesInforme" runat="server" TextMode="MultiLine" Width="740px" Height="250px"></asp:TextBox>--%>
                                    <CKEditor:CKEditorControl ID="txtRecomendacionesInforme" runat="server" Width="98%" Height="250px"
                                        ToolbarFull="NewPage|Preview&#13;&#10;Cut|Copy|Paste&#13;&#10;Undo|Redo|-|SelectAll|RemoveFormat&#13;&#10;Bold|Italic|Underline|Strike|-|Subscript|Superscript&#13;&#10;NumberedList|BulletedList|-|Outdent|Indent&#13;&#10;JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock&#13;&#10;BidiLtr|BidiRtl" FontNames="Arial/Arial, Helvetica, sans-serif;&#13;&#10;Calibri/Calibri, Arial, Helvetica, sans-serif;&#13;&#10;Comic Sans MS/Comic Sans MS, cursive;&#13;&#10;Courier New/Courier New, Courier, monospace;&#13;&#10;Georgia/Georgia, serif;&#13;&#10;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;&#13;&#10;Tahoma/Tahoma, Geneva, sans-serif;&#13;&#10;Times New Roman/Times New Roman, Times, serif;&#13;&#10;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;&#13;&#10;Verdana/Verdana, Geneva, sans-serif"></CKEditor:CKEditorControl>&nbsp;
       
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNomUsuarioInforme" runat="server" Font-Size="10px"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btnGuardarInformeVisita" runat="server" Text="Guardar" OnClick="btnGuardarInformeVisita_Click" />
                                    <asp:Button ID="btnGenerarInformePOPUP" runat="server" Text="Generar Informe" OnClick="btnGenerarInformePOPUP_Click" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />

        </center>
    </form>
</body>
</html>
