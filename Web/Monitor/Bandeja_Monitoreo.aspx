﻿<%@ Page Title="Bandeja de Monitoreo" Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="Bandeja_Monitoreo.aspx.cs" Inherits="Web.Monitor.Bandeja_Monitoreo" Culture="es-PE" UICulture="es-PE"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="Server">
   
    <style type="text/css">

        .grid-monitor {
            font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size: 13px;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            border: none !important;
            border-top-color: initial !important;
            border-top-style: none !important;
            border-top-width: initial !important;
            border-right-color: initial !important;
            border-right-style: none !important;
            border-right-width: initial !important;
            border-bottom-color: initial !important;
            border-bottom-style: none !important;
            border-bottom-width: initial !important;
            border-left-color: initial !important;
            border-left-style: none !important;
            border-left-width: initial !important;
            border-image-source: initial !important;
            border-image-slice: initial !important;
            border-image-width: initial !important;
            border-image-outset: initial !important;
            border-image-repeat: initial !important;
        }
    </style>
    
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" >
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.svg" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; ">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

    <center>
        <div class="Grid">
            <br />
            <p>
                <label class="titlePage">INVERSIONES
                </label>
            </p>
      
            <asp:UpdatePanel runat="server" ID="Up_Filtro" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Criterios de Búsqueda</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <%--<button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i>
                                </button>--%>
                            </div>
                        </div>
                        <div class="card-body p-0" style="display: block;">
                            <br />
                            <div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="formHeader">CÓDIGO</label>
                                            <asp:TextBox ID="txtSnipFiltro" runat="server" CssClass="form-control" ></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1001" runat="server" TargetControlID="txtSnipFiltro" FilterType="Numbers" Enabled="True" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="formHeader">NOMBRE DE INVERSIÓN</label>
                                             <asp:TextBox ID="txtProyectoFiltro" runat="server" CssClass="form-control" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="formHeader">DISTRITO</label>
                                             <asp:DropDownList ID="ddlDepa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepa_OnSelectedIndexChanged" Visible="false">
                                            </asp:DropDownList>
                                             <asp:DropDownList ID="ddlprov" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlprov_OnSelectedIndexChanged" Visible="false">
                                            </asp:DropDownList>
                                             <asp:DropDownList ID="ddldist" runat="server" CssClass="custom-select">
                                            </asp:DropDownList>
                         
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="formHeader">RESPONSABLE</label>
                                              <asp:DropDownList ID="ddlTecnico" runat="server" CssClass="custom-select">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="formHeader">ETAPA</label>
                                              <asp:DropDownList ID="ddlTipo" runat="server" CssClass="custom-select">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="formHeader">ESTADO</label>
                                             <asp:DropDownList ID="ddlEstado" runat="server" CssClass="custom-select">
                                            </asp:DropDownList>

                                            <asp:TextBox ID="txtanno" runat="server" Width="50px" onkeypress="return esInteger(event)" placeholder="yyyy" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="ddlSubsector" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubsector_SelectedIndexChanged" Visible="false">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlModalidad" runat="server" Visible="false">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlEstrategia" runat="server" Width="200px" Visible="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1 offset-sm-5">
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnBuscar" runat="server" OnClick="btnBuscar_OnClick" CssClass="btn btn-primary">
                                        <i class="fa fa-filter"></i> BUSCAR
                                            </asp:LinkButton>

                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnLimpiar3" runat="server" OnClick="btnLimpiar_OnClick" CssClass="btn btn-default">
                            <i class="fa fa-eraser" id="BtnLimpiar2"></i> LIMPIAR
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="text-right" style="width: 98%">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="btnReporteResumen" Visible="false" runat="server" OnClick="btnReporteResumen_OnClick" CssClass="btn btn-verde"><i class="fa fa-file-excel-o"></i> Exportar</asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnReporteResumen" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <asp:UpdatePanel ID="Up_grdBandeja" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="table table-responsive">
                    <asp:GridView
                        ID="grdBandeja"
                        runat="server"
                        Width="98%"
                        AllowPaging="True"
                        AllowCustomPaging="true"
                        DataKeyNames="xidIdProyecto"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered grid-monitor"
                        EmptyDataText="No hay registros."
                        PageSize="10"
                        OnPageIndexChanging="grdBandeja_PageIndexChanging"
                        OnRowDataBound="grdBandeja_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="INFORMACIÓN GENERAL">
                                <ItemTemplate>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-2 text-left d-inline">
                                                <strong>CUI:</strong>
                                                <div class="btn btn-xs btn-outline-primary btn-flat" title="Ver ficha MEF" onclick="<%# "loadModalIframe('MEF',"+Eval("iSNIP")+")" %>"><%# Eval("iCodUnificado") %></div>
                                            </div>
                                            <div class="col-4 text-left d-inline">
                                                <i class="fas fa-map-marker-alt"></i><%# Eval("vUbicacion") %>
                                            </div>
                                            <div class="col-6 text-left">
                                                <strong>UEI:</strong> <%# Eval("vUnidadEjecutora") %>
                                            </div>
                                        </div>
                                        <div class="row  pt-1">
                                            <div class="col-12 text-left">
                                                <strong>NOMBRE DE INVERSIÓN:</strong>  <%# Eval("vNombreProyecto") %>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label ID="des_subsector" Text='<%# Eval("vPrograma") %>' runat="Server" Visible="false" />
                                    <asp:Label ID="id_sector" runat="server" Text='<%#Eval("xidIdPrograma") %>' Visible="False" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SNIP" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="cod_snip" Text='<%# Convert.ToInt32(Eval("iSNIP"))==0 ? "NO": Eval("iSNIP") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CODIGO" Visible="false">
                                <ItemTemplate>
                                    <a style="text-decoration: underline; cursor: pointer; font-weight: bold; color: #a60000"
                                        href="<%# Eval("iSNIP", "http://ofi2.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo={0}") %>"
                                        target="_blank" title="Hacer clic para ver el proyecto en el MEF">
                                        <%# (Convert.ToInt32(Eval("iSNIP")) == 0 ? "NO" : Eval("iSNIP"))%>
                                    </a>
                                </ItemTemplate>
                                <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="UBICACION" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="nomUbigeo" Text='<%# Eval("vUbicacion")%>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOMBRE DE PROYECTO" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="nom_proyecto" Text='<%# Eval("vNombreProyecto") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <%--   <asp:TemplateField HeaderText="UNIDAD EJECUTORA">
                                        <ItemTemplate>
                                            <asp:Label ID="entidad_ejecutora" Text='<%# Eval("vUnidadEjecutora") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle  />
                                    </asp:TemplateField>--%>

                            <%--<asp:TemplateField HeaderText="TÉCNICO(S)">
                                        <ItemTemplate>
                                            <asp:Panel runat="server" ID="PanelTecnico">
                                                <asp:Label ID="lbltecnico" Text='<%# Eval("tecnicofiltro") %>' runat="Server" />
                                            </asp:Panel>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle  />
                                    </asp:TemplateField>--%>

                            <%--  <asp:TemplateField HeaderText="MODALIDAD FINANCIAM.">
                            <ItemTemplate>
                                <asp:Label ID="lblModalidadFinanciamiento" Text='<%# Eval("vSubModalidadFinanciamiento") %>' runat="Server" />
                            </ItemTemplate>
                            <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                            <HeaderStyle />
                        </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="MONTO VIABLE" Visible="true">
                                <ItemTemplate>
                                    <i class="fas fa-sack"></i>
                                    <asp:Label ID="monto_MVCS" Text='<%# "S/"+ Convert.ToDouble(Eval("dMontoViable")).ToString("N") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <%--    <asp:TemplateField HeaderText="" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIDFinanciamiento" Text='<%# Eval("tipoFinanciamiento") %>' runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle  />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="tipo" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFinanciamiento" Text="" runat="Server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle  />
                                    </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="ESTADO SITUACIONAL">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstadoEjecuccion" Text='<%# Eval("vTipoEstadoEjecucion") %>' runat="Server" />

                                    <div runat="server" id="divFechaUpdate">
                                    </div>

                                    <asp:Label ID="lblFechaUpdate" Text='<%# Eval("vFechaUpdate") %>' runat="Server" Visible="false" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PRE INVER." Visible="false">
                                <ItemTemplate>
                                    <%--                                            <asp:ImageButton ID="imgbtnPreInversion" AlternateText="Pre Inv." ImageUrl='<%# (Eval("tipoFinanciamiento").ToString()=="1" ? "~/img/pre.png" : "~/img/sin_pre.png")%>' runat="server" OnClick="imgbtnPreInversion_OnClick" />--%>
                                    <%--  <asp:LinkButton ID="imgbtnPreInversion" AlternateText="Pre Inv." runat="server" OnClick="imgbtnPreInversion_OnClick">
                                                <%# (Eval("xidIdTipoFinanciamiento").ToString()=="1" ? "<i class='fa fa-file-text-o amarillo'></i>" : "<i class='fa fa-file-text-o o1'></i>")%>
                                </asp:LinkButton>--%>
                                </ItemTemplate>
                                <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="GridHeaderMonitor" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="EXP. TÉCNICO" ItemStyle-HorizontalAlign="Center" Visible="false">
                                <ItemTemplate>
                                    <%--                                            <asp:ImageButton ID="imgbtnExpediente" runat="server" ImageUrl='<%# (Eval("tipoFinanciamiento").ToString()=="2" ? "~/img/expediente.gif" : "~/img/sin_expediente.gif")%>' OnClick="imgbtnExpediente_OnClick" />--%>
                                    <%--<asp:LinkButton ID="imgbtnExpediente" runat="server" OnClick="imgbtnExpediente_OnClick">
                                                <%# (Eval("xidIdTipoFinanciamiento").ToString()=="2" ? "<i class='fa fa-folder amarillo'></i>" : "<i class='fa fa-folder o1'></i>")%>
                                </asp:LinkButton>--%>
                                </ItemTemplate>
                                <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="GridHeaderMonitor" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OBRA" Visible="false">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblIDObra" Text='<%# Eval("Obra") %>' runat="Server" Visible="false" />--%>
                                    <%-- <asp:LinkButton ID="imgbtnObra" runat="server" AlternateText="Obra" OnClick="imgbtnObra_OnClick">
                                              <i class='fa fa-building o1'></i>
                                </asp:LinkButton>--%>
                                </ItemTemplate>
                                <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="GridHeaderMonitor" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AVANCE">
                                <ItemTemplate>
                                    <div>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-green" role="progressbar" aria-volumenow='<%# Eval("deAvanceFisicoReal") %>' aria-volumemin="0" aria-volumemax="100" style='<%# "width:"+ Eval("deAvanceFisicoReal")+"%" %>'>
                                            </div>
                                        </div>
                                        <small><%# Eval("deAvanceFisicoReal") %>% 
                                        </small>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="100px" VerticalAlign="Middle" HorizontalAlign="Center" />

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ETAPAS">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblIDMantimiento" Text='<%# Eval("Obra") %>' runat="Server"  Visible="false" />--%>
                                    <%--  <asp:LinkButton ID="imgbtnMantenimiento" runat="server" Visible="false" AlternateText="Obra" OnClick="imgbtnObra_OnClick">
                                                <%# (Eval("flagMantenimiento").ToString()=="1" ? "<i class='fa fa-binoculars amarillo'></i>" : "<i class='fa fa-binoculars o1'></i>")%>
                                </asp:LinkButton>--%>
                                    <div class="text-left">
                                        <div id="divPerfil" runat="server" class="d-inline"></div>
                                        <div id="divExpediente" runat="server" class="d-inline"></div>
                                        <div id="divObra" runat="server" class="d-inline"></div>
                                        <div id="divMantenimiento" runat="server" class="d-inline"></div>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="120px" VerticalAlign="Middle" HorizontalAlign="Center" />

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FICHA">
                                <ItemTemplate>
                                    <%--<asp:LinkButton ID="imgbtnAyudaMemoria" runat="server" OnClick="imgbtnAyudaMemoria_OnClick" Visible="false" >
                                                <i class="fa fa-question-circle ayuda"></i>
                                </asp:LinkButton>--%>
                                    <asp:Label runat="server" ID="lblDetalle" Visible="false" Text='<%# Eval("vDetalleEtapa") %>'></asp:Label>

                                    <div id="divFicha" class="btn btn-sm btn-info" onclick="<%# "loadModalIframe('Ficha',"+Eval("xidIdProyecto")+")" %>">Ver</div>


                                </ItemTemplate>
                                <ItemStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="8" FirstPageText="Primero" LastPageText="Último" />
                        <PagerStyle HorizontalAlign="Center" CssClass="pagination-lg" />
                        <FooterStyle HorizontalAlign="Center" />
                        <HeaderStyle Height="15px" BackColor="#2D7BBD" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle VerticalAlign="Middle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="grdBandeja" />
            </Triggers>
        </asp:UpdatePanel>
    </center>


     <!-- Modal Ficha-->
    <div class="modal fade" id="modalIframe" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                   <%-- <h5 class="modal-title" id="exampleModalLabel">REGISTRO DE INVERSIÓN</h5>--%>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe id="IfrmGeneral" runat="server" width="100%" height="650px"></iframe>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceFooter" ID="idFooter" runat="server">
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function loadModalIframe(pTipo, pId) {
            //let pIdProyecto = p;
            let src = "";
            switch (pTipo) {

              
                case "Ficha":
                    $('#txtTituloFrame').text('SEGUIMIENTO PCM')
                    $('#modalContent').attr('style', 'width:960px');
                    src = "Ficha_Ejecutiva?id=" + pId + '&idtipo=0&t=r';
                    break;
                case "MEF":
                    $('#txtTituloFrame').text('SEGUIMIENTO PCM')
                    $('#modalContent').attr('style', 'width:960px');
                    
                    src = "https://ofi5.mef.gob.pe/invierte/formato/verProyectoCU/" + pId;
                    break;
                       
            }
            $("#ContentPlaceHolder1_IfrmGeneral").attr("src", src);
            $('#modalIframe').modal('show');
        };

    </script>
</asp:Content>

