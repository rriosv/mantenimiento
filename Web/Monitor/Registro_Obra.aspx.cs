﻿using AjaxControlToolkit;
using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Registro_Obra : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();

        BL_MON_Riesgo _objBLRiesgo = new BL_MON_Riesgo();
        BE_MON_Riesgo _BERiesgo = new BE_MON_Riesgo();

        BLUtil _Metodo = new BLUtil();
        DataTable dt = new DataTable();

        decimal sumFooterValue = 0;

        protected string SplitConvFile(string convenio)
        {

            string textToReturn = string.Empty;
            textToReturn = Regex.Split(convenio, "____")[0]; ;

            //do split etc and assign formatted string to textToReturn
            return textToReturn;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);

            if (!IsPostBack)
            {
                if (Session["LoginUsuario"] == null)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                    Response.Redirect("~/login?ms=1");
                }

                BtnRefrescar_Click(null, null);

                if (!(ViewState["ActiveTabIndex"] == null))
                {
                    TabContainer.ActiveTabIndex = (int)ViewState["ActiveTabIndex"];
                }

                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();
                lblID_PERFIL_USUARIO.Text = Session["PerfilUsuario"].ToString();
                lblCOD_SUBSECTOR.Text = Session["CodSubsector"].ToString();

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int idacceso = _objBLProceso.spMON_Acceso(_BEProceso);
                lblID_ACCESO.Text = idacceso.ToString();

                if (idacceso == 0)
                {
                    OcultarRegistro();
                }


                //SUPERVISORES EXTERNOS
                if (Convert.ToInt32(lblID_PERFIL_USUARIO.Text) == 82 && Session["NombreOficina"].ToString().Equals("EXTERNO"))
                {
                    TabPanelFinanciamiento.Visible = false;
                    TabPanelFinanciamiento.TabIndex = 999;
                    TabPanelProcesoSeleccion.Visible = false;
                    TabPanelProcesoSeleccion.TabIndex = 999;
                   
                    TabPanelEstadoEjecuccion.TabIndex = 0;
                    TabPanelAdicional.TabIndex = 1;
                    TabPanelLiquidacion.TabIndex = 2;
                    TabPanelFotografico.TabIndex = 3;

                    trInformeMonitoreo.Visible = false;

                    //btnImportarCuaderno.Visible = true;


                    trGEI.Visible = false;

                }

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {

                    lblNombProy.Text = dt.Rows[0]["nom_proyecto"].ToString();

                    string depa = dt.Rows[0]["depa"].ToString();
                    string prov = dt.Rows[0]["prov"].ToString();
                    string dist = dt.Rows[0]["dist"].ToString();
                    lblSNIP.Text = dt.Rows[0]["cod_snip"].ToString();

                    if (lblSNIP.Text == "0")
                    {
                        trVigencia1.Visible = false;
                        trVigencia2.Visible = false;
                        trVigencia3.Visible = false;
                    }

                    ifrGraficoSosem.Attributes["src"] = "iframe_GraficoSOSEM?snip=" + lblSNIP.Text;
        

                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    LblID_SOLICITUD.Text = dt.Rows[0]["id_solicitudes"].ToString();

                    lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                    lblCOD_SUBSECTOR.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                    lblTipoFinanciamiento.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();
                                       

                    lblIdEstadoRegistro.Text = dt.Rows[0]["idEstadoProyecto"].ToString();
                    if (lblIdEstadoRegistro.Text.Equals("3"))
                    {
                        lblEstadoRegistro.Text = "FINALIZADO";
                        btnFinalizarTab3.Text = "Activar";
                    }
                    else
                    {
                        lblEstadoRegistro.Text = "ACTIVADO";
                        btnFinalizarTab3.Text = "Finalizar";
                    }

                }
                CargaTab0(Convert.ToInt32(LblID_PROYECTO.Text));
                CargaFinanciamientoTab0(Convert.ToInt32(LblID_PROYECTO.Text));

                CargaTab1();
                CargaTab4();
                CargaTab2();
                CargaTab3();

                CargaTab6();
                //CargaTab7();
                //CargaTab8();
                //CargaTab9();

                if (lblCOD_SUBSECTOR.Text == "2")
                {
                    trBeneficiariosTab2.Visible = true;
                }

                //CargarMetas(Convert.ToInt32(LblID_PROYECTO.Text));

                //Acceso a monitor para eliminar registros de visitas
                if (LblID_USUARIO.Text.Equals("481"))
                {
                    grdEvaluacionTab2.Columns[21].Visible = true;
                }

                // COORDINADORES PMIB , PNSU, PNSR, PNVR, PASLC
                if (lblIdEstadoRegistro.Text.Equals("3") && (lblID_PERFIL_USUARIO.Text.Equals("23") || lblID_PERFIL_USUARIO.Text.Equals("24") || lblID_PERFIL_USUARIO.Text.Equals("71") || lblID_PERFIL_USUARIO.Text.Equals("74") || lblID_PERFIL_USUARIO.Text.Equals("76") || LblID_USUARIO.Text.Equals("481")))
                {
                    btnFinalizarTab3.Visible = true;
                }
                else if (lblIdEstadoRegistro.Text.Equals("3"))
                {
                    btnFinalizarTab3.Visible = false;
                }

            }

            //Agregar menu lateral por etapa
            BL_MON_Menu oMenu = new BL_MON_Menu();

            List<BE_MON_Menu> ListMenu = new List<BE_MON_Menu>();
            ListMenu = oMenu.paSSP_MON_rEtapas(idProy, 1);
            int currentIndex = 0;
            btnNuevoProy.Visible = false;

            for (int i = 0; i < ListMenu.Count; i++)
            {
                BE_MON_Menu menu = ListMenu[i];
                HtmlGenericControl li = new HtmlGenericControl("li");
                if (menu.idProyecto == idProy)
                {
                    li.Attributes.Add("class", "active");

                    //Colocando tipos de financiamiento
                    DataSet dsTipo = new DataSet();
                    dsTipo = _objBLFinanciamiento.paSSP_MON_rTipoFinanciamientos(menu.idTipoFinanciamiento);
                    DataTable dtTipoFinanc = new DataTable();
                    dtTipoFinanc = dsTipo.Tables[0];

                    foreach (DataRow row in dtTipoFinanc.Rows)
                    {
                        slTipoFinanc.Items.Add(new ListItem(row["vNombre"].ToString(), row["idTIpoFinanciamiento"].ToString()));
                    }
                    currentIndex = i;
                }

                menuObra.Controls.Add(li);

                HtmlGenericControl anchor = new HtmlGenericControl("a");

                string aspxPage = "";
                switch (menu.idTipoFinanciamiento)
                {
                    case 1:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_PreInversion.aspx";
                        }
                        else
                        {
                            aspxPage = "Registro_PreInversion_PorTransferencia.aspx";
                        }
                        break;
                    case 2:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_ExpedienteTecnico.aspx";
                        }
                        else
                        {
                            aspxPage = "Registro_ExpedienteTecnico_PorTransferencia.aspx";
                        }
                        break;
                    case 3:
                        if (menu.idModalidadFinanciamiento == 3)
                        {
                            aspxPage = "Registro_EjecucionContrata.aspx";
                        }
                        else
                        {
                            if (menu.idModalidadFinanciamiento == 4)
                            { aspxPage = "Registro_NE.aspx"; }
                            else
                            {
                                if (menu.idModalidadFinanciamiento == 2)
                                {
                                    aspxPage = "Registro_Jica.aspx";
                                }
                                else { aspxPage = "Registro_Obra.aspx"; }
                            }
                        }
                        break;
                    default:
                        aspxPage = "Registro_Obra.aspx";
                        break;
                }

                //anchor.Attributes.Add("href", "/Monitor/"+ aspxPage + "?id=" + menu.idProyecto);
                anchor.Attributes.Add("href", "" + aspxPage + "?id=" + menu.idProyecto + "&token=" + Request.QueryString["token"]);
                anchor.InnerText = menu.tipoFinanciamiento;
                li.Controls.Add(anchor);

                if (menu.flagUltimo == 1 && menu.idProyecto == idProy)
                {
                    btnNuevoProy.Disabled = false;
                    btnNuevoProy.Visible = true;
                }
            }

        }

        protected void OcultarRegistro()
        {
            btnFinalizarTab3.Visible = false;

            btnAgregarContrapartidasTab0.Visible = false;
            //btnAgregarTransferenciaTab0.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;

            imgbtnAvanceFisicoTab2.Visible = false;
            imgbtnPlaTab4.Visible = false;
            imgbtnPreTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = false;
            btnGuardarTab3.Visible = false;

            btnGuardarSostenibilidadTab3.Visible = false;

            btnGuardarTab1.Visible = false;
            btnCambiarModalidadTab1.Visible = false;
            btnGuardarDirectoTab1.Visible = false;
            btnGuardarInfoTab2.Visible = false;

            grdContrapartidasTab0.Columns[0].Visible = false;
            grdContrapartidasTab0.Columns[8].Visible = false;

            grdSeguimientoProcesoTab1.Columns[0].Visible = false;
            grdSeguimientoProcesoTab1.Columns[11].Visible = false;
            grdConsultoresTab1.Columns[0].Visible = false;
            grdConsultoresTab1.Columns[5].Visible = false;
            grdSubcontratosTab1.Columns[0].Visible = false;
            grdSubcontratosTab1.Columns[13].Visible = false;
            imgbtnAgregarSubcontratoTab1.Visible = false;

            imgbtnAgregarPersonalClaveTab1.Visible = false;
            grdPersonalClaveTab1.Columns[0].Visible = false;
            grdPersonalClaveTab1.Columns[8].Visible = false;

            imgbtn_residenteObraTab2.Visible = false;
            imgbtn_SupervisorDesignado.Visible = false;
            imgbtn_CoordinadorTab2.Visible = false;

            ImgbtnActualizarInspector.Visible = false;
            grdFianzasTab2.Columns[0].Visible = false;
            grdFianzasTab2.Columns[16].Visible = false;
            imgbtnAgregarFianzasTab4.Visible = false;
            grdAvanceFisicoTab2.Columns[0].Visible = false;
            grdAvanceFisicoTab2.Columns[16].Visible = false;

            imgbtnAvanceFisicoTab2.Visible = false;

            grdAvanceFisicoAdicionalTab2.Columns[0].Visible = false;
            grdAvanceFisicoAdicionalTab2.Columns[16].Visible = false;

            imgbtnAvanceFisicoAdicionalTab2.Visible = false;

            grdEvaluacionTab2.Columns[0].Visible = false;
            grdEvaluacionTab2.Columns[21].Visible = false;
            imgbtnAgregarEvaluacionTab2.Visible = false;

            grdParalizacionTab4.Columns[0].Visible = false;
            grdParalizacionTab4.Columns[11].Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = false;

            grdPlazoTab4.Columns[0].Visible = false;
            grdPlazoTab4.Columns[11].Visible = false;
            imgAgregaPlazoTab4.Visible = false;
            grdPresupuestoTab4.Columns[0].Visible = false;
            grdPresupuestoTab4.Columns[12].Visible = false;
            grdDeductivoTab4.Columns[0].Visible = false;
            grdDeductivoTab4.Columns[12].Visible = false;

            grdPanelTab6.Columns[0].Visible = false;
            grdPanelTab6.Columns[8].Visible = false;
            imgbtnAgregarPanelTab6.Visible = false;


            //btnGuardarMetasPMIB.Visible = false;
            UPTabContainerDetalles.Update();

            imgbtnEditarUbigeo.Visible = false;
            imgbtnEditarUE.Visible = false;
            imgbtnEditarMontosMVCS.Visible = false;
                      
            lnkbtnBuscarRUCConsorcioTab1.Visible = false;
            lnkbtnBuscarRUCContratistaTab1.Visible = false;
            lnkbtnBuscarRUCMiembroConsorcioTab1.Visible = false;

            BtnGuardarIncorporacionRecursos.Visible = false;
            btnRegistrarVencimiento.Visible = false;
            btnVigenciaVencida.Text = "VER";
        }
        protected void CargaFinanciamientoTab0(int idProy)
        {

            //Colocando resumen de financiamientp etapa - https://trello.com/c/uqVY2SZM/41-obras
            DataSet ds = new DataSet();
            ds = _objBLFinanciamiento.paSSP_MON_rFinanciamiento(idProy);
            DataTable dtSumatorias = new DataTable();
            dtSumatorias = ds.Tables[0];

            if (dtSumatorias.Rows.Count > 0)
            {
                lblMontoComprometido.Text = (Convert.ToDouble(dtSumatorias.Rows[0]["SumMontoComprometido"])).ToString("N");
                lblMontoTransferido.Text = (Convert.ToDouble(dtSumatorias.Rows[0]["SumMontoTransferido"])).ToString("N");
            }
            else
            {
                lblMontoComprometido.Text = "0.00";
                lblMontoTransferido.Text = "0.00";
            }

            DataTable dtCofinanciamiento = new DataTable();
            dtCofinanciamiento = ds.Tables[1];

            if (dtCofinanciamiento.Rows.Count > 0)
            {
                lblMontoConfinFirm.Text = (Convert.ToDouble(dtCofinanciamiento.Rows[0]["SumCofinanciamiento"])).ToString("N");
            }
            else
            {
                lblMontoConfinFirm.Text = "0.00";
            }

            DataTable dtMontosTotalesDisp = new DataTable();
            dtMontosTotalesDisp = ds.Tables[2];
            lblMontoTotalDispDS.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[0]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispDU.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[2]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispRM.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[1]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispLey.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[3]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispDL.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[4]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalDispRD.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[5]["SumMontoTransferidoXDisp"])).ToString("N");

            DataTable dtMontosTotalesFuente = new DataTable();
            dtMontosTotalesDisp = ds.Tables[3];
            lblMontoTotalFuenteRO.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[0]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalFuenteRD.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[3]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalFuenteROOC.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[2]["SumMontoTransferidoXDisp"])).ToString("N");
            lblMontoTotalFuenteDYT.Text = "S/ " + (Convert.ToDouble(dtMontosTotalesDisp.Rows[1]["SumMontoTransferidoXDisp"])).ToString("N");

            //Colocando grilla de financiamiento - https://trello.com/c/uqVY2SZM/41-obras
            DataTable dtFinanciamiento = new DataTable();
            dtFinanciamiento = ds.Tables[4];

            if (dtFinanciamiento.Rows.Count > 0)
                grd_ResumConvenioTab0.DataSource = dtFinanciamiento;
            else
                grd_ResumConvenioTab0.DataSource = new DataTable();

            grd_ResumConvenioTab0.DataBind();
        }

        protected bool isNull(string val)
        {
            return val != "";
        }


        protected void grd_ResumConvenioTab0_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell cell = new TableHeaderCell();
                cell.ColumnSpan = 7;
                HeaderGridRow.Cells.Add(cell);

                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Monto Transferido";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(HeaderCell);

                //HeaderCell = new TableCell();
                //HeaderCell.Text = "Monto Comprometido MVCS";
                //HeaderCell.ColumnSpan = 3;
                //HeaderCell.CssClass = "GridHeader2";
                //HeaderGridRow.Cells.Add(HeaderCell);
                HeaderGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {

                //e.Row.Cells.RemoveAt(0);
                //e.Row.Cells.RemoveAt(0);

                //e.Row.Cells.RemoveAt(0);
                //e.Row.Cells.RemoveAt(0);
                //e.Row.Cells[6].ColumnSpan = 2;
                //e.Row.Cells[6].Text= "S/ "+ShowGrandTotalComp();

                //e.Row.Cells[7].ColumnSpan = 2;
                //e.Row.Cells[7].Text = "S/ " + ShowGrandTotalTransf();

            }
        }

        protected void grd_ResumConvenioTab0_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[9].Text = "S/ " + ShowGrandTotalComp();
                //e.Row.Cells[12].Text = "S/ " + ShowGrandTotalTransf();
            }
        }

        public string GetFormatAmount(object num)
        {
            return (Convert.ToDouble(num)).ToString("N");
        }

        public string ShowSubTotalComp(object totalCost)
        {
            sumFooterValue += Convert.ToInt64(totalCost);
            return (Convert.ToDouble(totalCost)).ToString("N");
        }

        public string ShowGrandTotalComp()
        {
            return (Convert.ToDouble(sumFooterValue)).ToString("N");
        }


        [WebMethod]
        public static string GuardarEtapa(string idProyecto, string idUsuario, string idTipoFinanciamiento, string vEtapa, string idEtapa)
        {

            BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
            DataSet ds = new DataSet();
            if (idEtapa == "") idEtapa = "0";
            ds = _objBLFinanciamiento.paSSP_MON_gPROYECTO(Int32.Parse(idProyecto), Int32.Parse(idUsuario), Int32.Parse(idTipoFinanciamiento), Int32.Parse(vEtapa), Int32.Parse(idEtapa));
            DataTable dtResultado = new DataTable();
            dtResultado = ds.Tables[1];
            return dtResultado.Rows[0]["vCod"].ToString();
        }

        protected Boolean validaArchivoFotos(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".bmp" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".jpg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar solo fotos en formato: BMP, JPG, JPEG o PNG.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 31912320)
                {
                    script = "<script>alert('Archivo solo hata 30 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaArchivo(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 31912320)
                {
                    script = "<script>alert('Archivo solo hata 30 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        public bool ValidaDecimal(string inValue)
        {
            bool bValid;
            try
            {
                decimal myDT = Convert.ToDecimal(inValue);
                bValid = true;

            }
            catch (Exception e)
            {
                bValid = false;
            }


            return bValid;
        }

        protected void GeneraIcoFile(string lnk, ImageButton imgbtn)
        {
            if (lnk != "")
            {

                string ext = lnk.Substring(lnk.Length - 3, 3);

                ext = ext.ToLower();

                if (ext == "pdf")
                {
                    imgbtn.ImageUrl = "~/img/pdf.gif";
                }

                if (ext == "xls" || ext == "lsx")
                {
                    imgbtn.ImageUrl = "~/img/xls.gif";

                }

                if (ext == "doc" || ext == "ocx")
                {
                    imgbtn.ImageUrl = "~/img/doc.gif";

                }

                if (ext == "png" || ext == "PNG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }
                if (ext == "jpg" || ext == "JPG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "peg" || ext == "PEG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "bmp" || ext == "BMP")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                imgbtn.ToolTip = lnk;
            }
            else
            {
                imgbtn.ToolTip = "";
                imgbtn.ImageUrl = "~/img/blanco.png";
            }

        }

        #region Tab0

        protected void CargaTab0(int idproy)
        {
            CargaTipoAporteTab0();
            //CargaTipoFinanTransferenciaTab0();
            //CargaTipoTransferenciaTab0();
            //CargaTipoDocSustento();
            //CargaTipoFaseProceso();

            //Modulo de contrapartida antiguo
            //CargaContrapartidaTab0();

            //Antiguo modulo de transferencias
            //CargaFinanTransferencia(); 

            CargaSOSEMEjecutorasTab0();

        }

        protected void CargaContrapartidaTab0()
        {
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            _BEFinanciamiento.tipoFinanciamiento = 3;

            List<BE_MON_Financiamiento> listContrapartida = new List<BE_MON_Financiamiento>();
            listContrapartida = _objBLFinanciamiento.F_spMON_FinanContrapartida(_BEFinanciamiento);
            grdContrapartidasTab0.DataSource = listContrapartida;
            grdContrapartidasTab0.DataBind();

            txtAcumuladogrdContraTab0.Text = ((from prod in listContrapartida
                                               select Convert.ToDecimal(prod.monto)).Sum()).ToString("N2");



            //totalMontoGrdContrapartida();


            double val1 = 0;
            if (txtAcumuladogrdContraTab0.Text != "")
            { val1 = Convert.ToDouble(txtAcumuladogrdContraTab0.Text); }

            double val2 = 0;
            //if (txtMontoSNIPTab0.Text != "")
            //{ val2 = Convert.ToDouble(txtMontoSNIPTab0.Text); }

            //@TODO TOTAL INVERSION COMENTADO
            //txtTotalInversionTab0.Text = (val1 + val2).ToString("N");

        }

        //CAMBIOS

      


        protected void CargaSOSEMEjecutorasTab0()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSOSEMEjecutoras.DataSource = _objBLFinanciamiento.F_spMON_SosemEjecutora(snip);
            grdSOSEMEjecutoras.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void CargaSOSEMDevengadoMensualizadoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdDetalleSOSEMTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemDevengadoMensualizado(snip, idEjecutora);
            grdDetalleSOSEMTab0.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }


        protected void CargaSOSEMFuenteFinanciamientoTab0(int idEjecutora)
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdFuenteFinanciamientoTab0.DataSource = _objBLFinanciamiento.F_spMON_SosemFuenteFinanciamiento(snip, idEjecutora);
            grdFuenteFinanciamientoTab0.DataBind();

            //totalMontoGrdTransferenciaTab0();

        }

        protected void CargaTipoAporteTab0()
        {
            ddlTipoAporteTab0.DataSource = _objBLFinanciamiento.F_spMON_TipoAporte();
            ddlTipoAporteTab0.DataTextField = "nombre";
            ddlTipoAporteTab0.DataValueField = "valor";
            ddlTipoAporteTab0.DataBind();

            ddlTipoAporteTab0.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void btnAgregarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            btnModificarContrapartidasTab0.Visible = false;
            btnGuardarContrapartidasTab0.Visible = true;
            Panel_ContrapartidaTab0.Visible = true;
            btnAgregarContrapartidasTab0.Visible = false;
            lblNomUsuarioContrapartida.Text = "";
            Up_Tab0.Update();

        }

        protected void btnGuardarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarContrapartidaTab0())
            {

                _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEFinanciamiento.tipoFinanciamiento = 3;
                _BEFinanciamiento.entidad_cooperante = txtEntidadCooperanteTab0.Text;
                _BEFinanciamiento.documento = txtDocumentoTab0.Text;
                _BEFinanciamiento.monto = txtMontoTab0.Text;
                _BEFinanciamiento.id_tipo_aporte = Convert.ToInt32(ddlTipoAporteTab0.SelectedValue.ToString());
                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEFinanciamiento.MontoObra = txtMontoObraTab0.Text;
                _BEFinanciamiento.MontoSupervision = txtMontoSupervisionTab0.Text;
                int val = _objBLFinanciamiento.spi_MON_FinanContrapartida(_BEFinanciamiento);


                if (val == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaContrapartidaTab0();

                    txtEntidadCooperanteTab0.Text = "";
                    txtDocumentoTab0.Text = "";
                    txtMontoTab0.Text = "";
                    ddlTipoAporteTab0.SelectedValue = "";
                    txtMontoObraTab0.Text = "";
                    txtMontoSupervisionTab0.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                Panel_ContrapartidaTab0.Visible = false;
                btnAgregarContrapartidasTab0.Visible = true;
                Up_Tab0.Update();
            }
        }

        protected void btnCancelarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            Panel_ContrapartidaTab0.Visible = false;
            btnAgregarContrapartidasTab0.Visible = true;
            Up_Tab0.Update();

        }


        protected void btnFileTransferencia_OnClick(object sender, EventArgs e)
        {
            string file;
            //  file = FileUpload1.PostedFile.FileName;

        }

        protected Boolean ValidarContrapartidaTab0()
        {
            Boolean result;
            result = true;

            if (txtEntidadCooperanteTab0.Text == "")
            {
                string script = "<script>alert('Ingrese contrapartida.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtDocumentoTab0.Text == "")
            //{
            //    string script = "<script>alert('Ingrese documento.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (ddlTipoAporteTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de aporte.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;

        }


        protected void LnkbtnCartaTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnCartaTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnAvanceTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnAvanceTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnAvanceAdicionalTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void LnkbtnSubContrato_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSubContrato.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnSeguimientoTAb1_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSeguimientoTAb1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdContrapartidasTab0_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_financiamiento_contrapartida;
            id_financiamiento_contrapartida = grdContrapartidasTab0.SelectedDataKey.Value.ToString();
            lblIDTipoContrapartida.Text = id_financiamiento_contrapartida.ToString();
            GridViewRow row = grdContrapartidasTab0.SelectedRow;
            txtEntidadCooperanteTab0.Text = ((Label)row.FindControl("lblEntidad")).Text;
            txtDocumentoTab0.Text = ((Label)row.FindControl("lblDocumento")).Text;
            txtMontoTab0.Text = Server.HtmlDecode(row.Cells[7].Text);
            ddlTipoAporteTab0.SelectedValue = ((Label)row.FindControl("lblId_TipoAporte")).Text;
            ddlTipoAporteTab0.SelectedValue = ((Label)row.FindControl("lblId_TipoAporte")).Text;
            txtMontoObraTab0.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtMontoSupervisionTab0.Text = Server.HtmlDecode(row.Cells[4].Text);


            lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnModificarContrapartidasTab0.Visible = true;
            btnGuardarContrapartidasTab0.Visible = false;
            Panel_ContrapartidaTab0.Visible = true;
            btnAgregarContrapartidasTab0.Visible = false;
            Up_Tab0.Update();


        }

        protected void grdSeguimientoProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdSeguimientoProcesoTab1.SelectedDataKey.Value.ToString();
            lblRegistroSeguimientoP.Text = id_seguimiento.ToString();
            GridViewRow row = grdSeguimientoProcesoTab1.SelectedRow;
            TxtConvocatoriaTab1.Text = ((Label)row.FindControl("lblConvocatoria")).Text;
            txtFechaPubliTab1.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoAdjudicacionTab1.Text = ((Label)row.FindControl("lblTipoAdjudicacion")).Text;
            ddlResultadoTab1.SelectedValue = ((Label)row.FindControl("lblIDResultado")).Text;
            txtObservacion.Text = ((Label)row.FindControl("lblobservacion")).Text;
            LnkbtnSeguimientoTAb1.Text = ((ImageButton)row.FindControl("imgDocTab1")).ToolTip;
            lblNomUsuarioSeguimientoProceso.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnSeguimientoTAb1.Text, imgbtnSeguimientoTAb1);



            cargaSeguimientoProcesoTab1();
            Up_Tab1.Update();
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            btnModificarSeguimientoProcesoTab1.Visible = true;
            btnGuardarSeguimientoProcesoTab1.Visible = false;

        }

        protected void grdSubcontratosTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_subContratos;
            id_subContratos = grdSubcontratosTab1.SelectedDataKey.Value.ToString();
            lblId_SubContratos.Text = id_subContratos.ToString();
            GridViewRow row = grdSubcontratosTab1.SelectedRow;

            ddlRubroTab1.Text = ((Label)row.FindControl("lblId_Rubros")).Text;
            txtnombreTab1.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtMontoTab1.Text = Convert.ToDouble(((Label)row.FindControl("lblmonto")).Text).ToString("N");
            txtPlazoTab1.Text = ((Label)row.FindControl("lblPlazo")).Text;
            ddlModalidaSubcontratoTab1.Text = ((Label)row.FindControl("lblId_Modalidad")).Text;
            LnkbtnSubContrato.Text = ((ImageButton)row.FindControl("imgDocSubContratoTab1")).ToolTip;

            GeneraIcoFile(LnkbtnSubContrato.Text, imgbtnSubContrato);


            Panel_SubcontratosTab1.Visible = true;
            btnGuardarSubcontratosTab1.Visible = false;
            btnModificarSubcontratosTab1.Visible = true;
            imgbtnAgregarSubcontratoTab1.Visible = false;

        }

        protected void grdConsultoresTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_grupo_consorcio;
            id_grupo_consorcio = grdConsultoresTab1.SelectedDataKey.Value.ToString();
            lblId_Registro_Contratista.Text = id_grupo_consorcio.ToString();
            GridViewRow row = grdConsultoresTab1.SelectedRow;
            txtContratistaGrupTab1.Text = ((Label)row.FindControl("lblNombConsultor")).Text;
            txtRucGrupTab1.Text = ((Label)row.FindControl("lblRuc")).Text;
            txtRepresentanGrupTAb1.Text = ((Label)row.FindControl("lblRepresentante")).Text;
            Panel_AgregarConsultorTab1.Visible = true;
            btn_guardarConsultor.Visible = false;
            btn_modificarConsultor.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
        }

        protected void grdFianzasTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_carta_fianza;
            id_carta_fianza = grdFianzasTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_Carta_Fianza.Text = id_carta_fianza.ToString();
            GridViewRow row = grdFianzasTab2.SelectedRow;
            txtEntidadFinancieraTab2.Text = ((Label)row.FindControl("lblEntidadFinanciera")).Text;
            txtNroCartaF.Text = ((Label)row.FindControl("lblNroCarta")).Text;
            ddlTipoTab2.Text = ((Label)row.FindControl("lblIdTipo")).Text;
            txtFechaInicioVigenciaTab2.Text = ((Label)row.FindControl("lblFechaInicio")).Text;

            txtFechaFinVigenciaTab2.Text = ((Label)row.FindControl("lblFechaFin")).Text;
            txtmontoFianzaTab2.Text = ((Label)row.FindControl("lblmontoTab2")).Text;
            if (txtmontoFianzaTab2.Text != "")
            {
                txtmontoFianzaTab2.Text = Convert.ToDouble(txtmontoFianzaTab2.Text).ToString("N");
            }
            ddlTipoAccion.SelectedValue = ((Label)row.FindControl("lblIdTipoAccion")).Text;
            //txtmontoFianzaTab2.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtPorcentajeTab2.Text = ((Label)row.FindControl("lblPorcentaje")).Text;
            chkVerificado.Checked = ((CheckBox)row.FindControl("chb_flagVerifico")).Checked;
            txtFechaRenovacion.Text = ((Label)row.FindControl("lblRenovacion")).Text; 
            lnkbtnAccionTab2.Text = ((ImageButton)row.FindControl("imgDocAccionGriTab2")).ToolTip;
            GeneraIcoFile(lnkbtnAccionTab2.Text, imgbtnAccionTab2);
            LnkbtnCartaTab2.Text = ((ImageButton)row.FindControl("imgDocCartaTab2")).ToolTip;

            lblNomUsuarioCarta.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnCartaTab2.Text, imgbtnCartaTab2);

            if (ddlTipoAccion.SelectedValue.ToString().Equals("2"))
            {
                lblTitDocAccion.Text = "Fecha de Renovación";
                txtFechaRenovacion.Visible = true;
                FileUploadAccionTab2.Visible = false;
            }
            else if (ddlTipoAccion.SelectedValue.ToString().Equals("3") || ddlTipoAccion.SelectedValue.ToString().Equals("4"))
            {
                lblTitDocAccion.Text = "Doc. " + ddlTipoAccion.SelectedItem.Text;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                FileUploadAccionTab2.Visible = true;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else
            {
                lblTitDocAccion.Text = "";
                FileUploadAccionTab2.Visible = false;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }

            Panel_fianzas.Visible = true;
            imgbtnAgregarFianzasTab4.Visible = false;

            btnGuardarFianzasTab2.Visible = false;
            btnModificarFianzasTab2.Visible = true;
            Up_Tab2.Update();

        }


        protected void btnModificarContrapartidasTab0_OnClick(object sender, EventArgs e)
        {
            if (ValidarContrapartidaTab0())
            {

                _BEFinanciamiento.id_financiamientoContrapartida = Convert.ToInt32(lblIDTipoContrapartida.Text);
                _BEFinanciamiento.tipo = 1;
                _BEFinanciamiento.entidad_cooperante = txtEntidadCooperanteTab0.Text;
                _BEFinanciamiento.documento = txtDocumentoTab0.Text;
                _BEFinanciamiento.monto = txtMontoTab0.Text;
                _BEFinanciamiento.id_tipo_aporte = Convert.ToInt32(ddlTipoAporteTab0.SelectedValue.ToString());
                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEFinanciamiento.MontoObra = txtMontoObraTab0.Text;
                _BEFinanciamiento.MontoSupervision = txtMontoSupervisionTab0.Text;


                int val = _objBLFinanciamiento.spud_MON_FinanContrapartida(_BEFinanciamiento);

                if (val == 1)
                {
                    string script = "<script>alert('Actualización Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaContrapartidaTab0();

                    txtEntidadCooperanteTab0.Text = "";
                    txtDocumentoTab0.Text = "";
                    txtMontoTab0.Text = "";
                    ddlTipoAporteTab0.SelectedValue = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                Panel_ContrapartidaTab0.Visible = false;
                btnAgregarContrapartidasTab0.Visible = true;
                Up_Tab0.Update();
            }
        }

        protected void grdContrapartidasTab0_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdContrapartidasTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEFinanciamiento.id_financiamientoContrapartida = Convert.ToInt32(objTemp.ToString());
                _BEFinanciamiento.tipo = 2;
                _BEFinanciamiento.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLFinanciamiento.spud_MON_FinanContrapartida(_BEFinanciamiento);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaContrapartidaTab0();
                    Up_Tab0.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdAvanceFisicoTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_AvanceFisico_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdAvanceFisicoAdicionalTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoAdicionalTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_AvanceFisico_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdFianzasTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdFianzasTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_carta_fianza = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_Seguimiento_Carta_Fianza_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaCartasFianzasTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        #endregion

        #region Tab1

        protected void CargaTab1()
        {

            CargarIncorporacionRegistro();

            if (!IsPostBack)
            {
                // Seguimiento manual del proceso de selección
                CargaddlTipoAdjudicacionTab1(ddlAdjudicacionTab1);
                CargaddlTipoAdjudicacionTab1(ddlTipoAdjudicacionTab1);
                cargaDDlTipoContratacionTab1();
                CargaDllModalidadTab1();
                CargaDllModalidadTab1(ddlModalidaSubcontratoTab1);

                CargaListaRubro();
                CargaDllTipoResultado();
                CargaDatosTab1();
                if (Panel_ProcesoIndirectoTab1.Visible == true)
                {
                    TabActivoProceso();
                }
            }

            // Seguimiento Automatico
                      

           
        }

        protected void cargaConsultoresTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdConsultoresTab1.DataSource = _objBLProceso.F_spMON_GrupoConsorcio(_BEProceso);
            grdConsultoresTab1.DataBind();


        }

        protected void CargaDatosTab1()
        {

            lblTitSeace.Text = "SEGUIMIENTO "+ddlProcesoTab1.SelectedItem.Text+" (Obligatorio)";
            lnkbtnSeaceAutoPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " POR SEACE";
            lnkbtnSeaceManualPanel.Text = "SEGUIMIENTO " + ddlProcesoTab1.SelectedItem.Text + " DE FORMA MANUAL";

            lblTitPersonalTab1.Text = "PERSONAL CLAVE " + ddlProcesoTab1.SelectedItem.Text;

            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);
            string modal;
            if (dt.Rows.Count > 0)
            {
                modal = dt.Rows[0]["id_tipoModalidad"].ToString();
                if (modal != "")
                {
                    ddlModalidadTab1.SelectedValue = modal;
                                       
                }
                else
                {
                    ddlModalidadTab1.SelectedValue = "";

                }

                if (ddlModalidadTab1.SelectedValue == "1") // DIRECTA
                {
                    lblNomActualizaProcesoDirecta.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    Panel_ProcesoDirectoTab1.Visible = true;
                    btnGuardarDirectoTab1.Visible = true;
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    //cargaSeguimientoProcesoTab1();
                    if (lblID_ACCESO.Text == "1")
                    {
                        btnCambiarModalidadTab1.Visible = true;
                    }
                    // CargaDatosTab1();
                    cargaSubcontratosTab1();
                    //CargaResponsableTab1();
                    Panel_OtrasModalidadesTab1.Visible = false;
                }

                if (ddlModalidadTab1.SelectedValue == "2")  // INDIRECTA
                {

                    lblNomActualizaProcesoDirecta.Text = "";
                    ddlAdjudicacionTab1.SelectedValue = dt.Rows[0]["id_tipoAdjudicacion"].ToString();
                    txtNroLicTab1.Text = dt.Rows[0]["NroLic"].ToString();
                    TxtAnio2Tab1.Text = dt.Rows[0]["anio"].ToString();
                    txtDetalleTab1.Text = dt.Rows[0]["detalle"].ToString();
                    txtProTab1.Text = dt.Rows[0]["buenaPro"].ToString();
                    txtProConsentidaTab1.Text = dt.Rows[0]["buenaProConsentida"].ToString();
                    txtValorReferencialTab1.Text = dt.Rows[0]["valorReferencial"].ToString();
                    ddlTipoContratacionTab1.SelectedValue = dt.Rows[0]["id_tipoContratacion"].ToString();
                    txtMontoContratadoTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                    txtNroContrato.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                    txtFechaFirmaContratoTab1.Text = dt.Rows[0]["fechaFirmaContrato"].ToString();
                    lblNomActualizaProcesoSeleccion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    // lblNomUsuarioOtraModalidad.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    string valCheck = dt.Rows[0]["id_tipoEmpresa"].ToString();

                    rbConsultorTab1.Checked = false;
                    rbConsorcioTab1.Checked = false;
                    rbConsorcioTab1.Enabled = true;
                    rbConsultorTab1.Enabled = true;

                    if (valCheck != "")
                    {
                        if (Convert.ToInt32(valCheck) == 1)
                        {
                            rbConsultorTab1.Checked = true;
                            rbConsorcioTab1.Checked = false;
                            //  rbConsorcioTab1.Enabled = false;

                            lblNomConsultorTab1.Font.Bold = true;
                            lblNomConsorcioTab1.Font.Bold = false;

                            Panel_ConsultorTab1.Visible = true;
                            Panel_ConsorcioTab1.Visible = false;


                            Up_rbProcesoTab1.Update();

                            txtNombConsultorTab1.Text = dt.Rows[0]["nombreContratista"].ToString();
                            txtRucConsultorTab1.Text = dt.Rows[0]["rucContratista"].ToString();
                            lnkbtnContratoContratistaTab1.Text = dt.Rows[0]["urlContrato"].ToString();
                            txtRepresentanteLegalConsultorTab1.Text = dt.Rows[0]["representanteContratista"].ToString();
                            GeneraIcoFile(lnkbtnContratoContratistaTab1.Text, imgbtnContratoContratistaTab1);

                        }
                        if (Convert.ToInt32(valCheck) == 2)
                        {
                            rbConsorcioTab1.Checked = true;
                            rbConsultorTab1.Checked = false;
                            //  rbConsultorTab1.Enabled = false;

                            lblNomConsorcioTab1.Font.Bold = true;
                            lblNomConsultorTab1.Font.Bold = false;

                            Panel_ConsorcioTab1.Visible = true;
                            Panel_ConsultorTab1.Visible = false;

                            cargaConsultoresTab1();

                            Up_rbProcesoTab1.Update();


                            lnkbtnContratoTab1.Text = dt.Rows[0]["urlContrato"].ToString();

                            txtNomconsorcioTab1.Text = dt.Rows[0]["nombreConsorcio"].ToString();
                            txtRucConsorcioTab1.Text = dt.Rows[0]["rucConsorcio"].ToString();
                            txtTelefonoConsorcioTab1.Text = dt.Rows[0]["telefonoConsorcio"].ToString();
                            txtRepresentanteTab1.Text = dt.Rows[0]["representateConsorcio"].ToString();

                            GeneraIcoFile(lnkbtnContratoTab1.Text, imgbtnContratoTab1);

                        }
                    }
                    else
                    {
                        rbConsorcioTab1.Checked = false;
                        rbConsultorTab1.Checked = false;
                        rbConsultorTab1.Enabled = true;
                        rbConsorcioTab1.Enabled = true;
                        Panel_ConsultorTab1.Visible = false;
                        Panel_ConsorcioTab1.Visible = false;

                        txtNombConsultorTab1.Text = "";
                        txtRucConsultorTab1.Text = "";

                        txtNomconsorcioTab1.Text = "";
                        txtRucConsorcioTab1.Text = "";
                        txtTelefonoConsorcioTab1.Text = "";
                        txtRepresentanteTab1.Text = "";

                        lblNomConsultorTab1.Font.Bold = false;
                        lblNomConsorcioTab1.Font.Bold = false;

                        cargaConsultoresTab1();
                        Up_rbProcesoTab1.Update();

                        imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                        lnkbtnContratoContratistaTab1.Text = "";
                        imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                        lnkbtnContratoTab1.Text = "";
                    }



                    Panel_ProcesoIndirectoTab1.Visible = true;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    cargaSeguimientoProcesoTab1();
                    cargaPersonalClaveTab1();
                    // CargaDatosTab1();
                    if (lblID_ACCESO.Text == "1")
                    {
                        btnCambiarModalidadTab1.Visible = true;
                    }

                    Panel_OtrasModalidadesTab1.Visible = false;
                }

                if (ddlModalidadTab1.SelectedValue == "3")
                {
                    lblNomActualizaProcesoDirecta.Text = "";
                    lblNomUsuarioOtraModalidad.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                    txtMontoContratadoOtrasModalidadTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                    txtNroContratoOtrasModalidadTab1.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                    txtEntidadEjecutoraTab1.Text = dt.Rows[0]["entidadEjecutora"].ToString();
                    txtCoordinadorEntidadEjecutoraTab1.Text = dt.Rows[0]["coordinadorEJ"].ToString();
                    txtTelefonoCoordEJTab1.Text = dt.Rows[0]["telefonoCoordEJ"].ToString();
                    txtCorreoCoordEJTab1.Text = dt.Rows[0]["correoCoordEJ"].ToString();
                    txtfechaConvenioTab1.Text = dt.Rows[0]["fechaConvenio"].ToString();
                    txtConvenioContratoTab1.Text = dt.Rows[0]["convenioContrato"].ToString();
                    LnkbtnOtraModalidad.Text = dt.Rows[0]["urlContrato"].ToString();

                    GeneraIcoFile(LnkbtnOtraModalidad.Text, ImgbtnOtraModalidad);

                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    // cargaSeguimientoProcesoTab1();
                    // CargaDatosTab1();
                    if (lblID_ACCESO.Text == "1")
                    {
                        btnCambiarModalidadTab1.Visible = true;
                    }

                    Panel_OtrasModalidadesTab1.Visible = true;
                }
            }
            else
            {
                ddlAdjudicacionTab1.SelectedValue = "";
                txtNroLicTab1.Text = "";
                TxtAnio2Tab1.Text = "";
                txtDetalleTab1.Text = "";
                txtProTab1.Text = "";
                txtProConsentidaTab1.Text = "";
                txtValorReferencialTab1.Text = "";
                ddlTipoContratacionTab1.SelectedValue = "";
                txtMontoContratadoTab1.Text = "";
                txtNroContrato.Text = "";
                txtNomconsorcioTab1.Text = "";
                txtRucConsorcioTab1.Text = "";
                txtTelefonoConsorcioTab1.Text = "";
                txtRepresentanteTab1.Text = "";

                rbConsorcioTab1.Checked = false;
                rbConsultorTab1.Checked = false;
                rbConsultorTab1.Enabled = true;
                rbConsorcioTab1.Enabled = true;
                Panel_ConsultorTab1.Visible = false;
                Panel_ConsorcioTab1.Visible = false;
                lblNomActualizaProcesoSeleccion.Text = "";
                lblNomActualizaProcesoDirecta.Text = "";

                imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoContratistaTab1.Text = "";
                imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoTab1.Text = "";

                //CAMPOS DE OTRA MODALIDAD
                txtMontoContratadoOtrasModalidadTab1.Text = "";
                txtNroContratoOtrasModalidadTab1.Text = "";

                txtEntidadEjecutoraTab1.Text = "";
                txtCoordinadorEntidadEjecutoraTab1.Text = "";
                txtTelefonoCoordEJTab1.Text = "";
                txtCorreoCoordEJTab1.Text = "";
                txtfechaConvenioTab1.Text = "";
                txtConvenioContratoTab1.Text = "";
                lblNomUsuarioOtraModalidad.Text = "";
                LnkbtnOtraModalidad.Text = "";
                ImgbtnOtraModalidad.ImageUrl = "~/img/blanco.png";
            }

            cargaConsultoresTab1();


            // INFORMACION AUTOMATICA SEACE
            CargaCodigoSeace();
            CargaProcesosRelSEACE();

        }


        protected void CargaDllModalidadTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoModalidad_Contrato();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaDllTipoResultado()
        {
            ddlResultadoTab1.DataSource = _objBLEjecucion.F_spMON_TipoResultado();
            ddlResultadoTab1.DataTextField = "nombre";
            ddlResultadoTab1.DataValueField = "valor";
            ddlResultadoTab1.DataBind();

            ddlResultadoTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaDllModalidadTab1()
        {
            ddlModalidadTab1.Items.Clear();

            ddlModalidadTab1.Items.Insert(0, new ListItem("Directa", "1"));
            ddlModalidadTab1.Items.Insert(0, new ListItem("Indirecta", "2"));
            ddlModalidadTab1.Items.Insert(0, new ListItem("Otra Modalidad", "3"));
            //ddlModalidadTab1.DataSource = _objBLProceso.F_spMON_TipoModalidad();
            //ddlModalidadTab1.DataTextField = "nombre";
            //ddlModalidadTab1.DataValueField = "valor";
            //ddlModalidadTab1.DataBind();

            ddlModalidadTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaListaRubro()
        {
            ddlRubroTab1.DataSource = _objBLProceso.F_spMON_ListaRubro();
            ddlRubroTab1.DataTextField = "nombre";
            ddlRubroTab1.DataValueField = "valor";
            ddlRubroTab1.DataBind();

            ddlRubroTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void cargaDDlTipoContratacionTab1()
        {
            ddlTipoContratacionTab1.DataSource = _objBLProceso.F_spMON_TipoContratacion();
            ddlTipoContratacionTab1.DataTextField = "nombre";
            ddlTipoContratacionTab1.DataValueField = "valor";
            ddlTipoContratacionTab1.DataBind();

            ddlTipoContratacionTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void ddlProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlProcesoTab1.SelectedValue == "1")
            {
                cabecerahito.Visible = true;
                cuerpohito.Visible = true;
            }
            else
            {
                cabecerahito.Visible = false;
                cuerpohito.Visible = false;
            }


            if (ddlProcesoTab1.SelectedValue != "")
            {
                ddlModalidadTab1.SelectedValue = "";
                CargaDatosTab1();

                if (ddlModalidadTab1.SelectedValue != "")
                {
                }
                else
                {
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    btnCambiarModalidadTab1.Visible = false;
                    Panel_OtrasModalidadesTab1.Visible = false;
                }
            }

            if (Convert.ToInt32(lblCOD_SUBSECTOR.Text) == 1)
            {
                Panel_ProcesoDirectoTab1.Visible = false;
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;
            }

            if (Panel_ProcesoIndirectoTab1.Visible == true)
            {
                TabActivoProceso();
            }

            Up_Tab1.Update();


        }

        protected void btnCambiarModalidadTab1_OnClick(object sender, EventArgs e)
        {
            string valorModalidad = ddlModalidadTab1.SelectedValue;
            int val;
            if (valorModalidad == "1")
            {
                ddlModalidadTab1.SelectedValue = "2";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);

                Panel_ProcesoIndirectoTab1.Visible = true;
                Panel_ProcesoDirectoTab1.Visible = false;
                btnGuardarDirectoTab1.Visible = false;
                cargaSeguimientoProcesoTab1();
                cargaPersonalClaveTab1();
                CargaDatosTab1();
                btnCambiarModalidadTab1.Visible = true;


            }

            if (valorModalidad == "2")
            {
                ddlModalidadTab1.SelectedValue = "3";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);


                btnGuardarDirectoTab1.Visible = true;

                //cargaSeguimientoProcesoTab1();
                btnCambiarModalidadTab1.Visible = true;

                Panel_ProcesoIndirectoTab1.Visible = false;
                Panel_ProcesoDirectoTab1.Visible = false;
                btnGuardarDirectoTab1.Visible = false;
                Panel_OtrasModalidadesTab1.Visible = true;

                CargaDatosTab1();
                //cargaSubcontratosTab1();
                //CargaResponsableTab1();


            }

            if (valorModalidad == "3")
            {
                ddlModalidadTab1.SelectedValue = "1";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);

                Panel_ProcesoDirectoTab1.Visible = true;
                btnGuardarDirectoTab1.Visible = true;
                Panel_ProcesoIndirectoTab1.Visible = false;
                //cargaSeguimientoProcesoTab1();
                btnCambiarModalidadTab1.Visible = true;
                CargaDatosTab1();
                cargaSubcontratosTab1();
                //CargaResponsableTab1();
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;
                Up_Tab1.Update();
            }

        }

        protected void btnGuardarDirectoTab1_OnClick(object sender, EventArgs e)
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
            //_BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue);
            //_BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

            //_BEProceso.norLic = Convert.ToInt32(txtNroLicTab1.Text);
            //_BEProceso.anio = TxtAnio2Tab1.Text;
            //_BEProceso.detalle = txtDetalleTab1.Text;
            _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
            _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
            _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
            //_BEProceso.valorReferencial = txtValorReferencialTab1.Text;

            //_BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
            //_BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
            //if (rbConsultorTab1.Checked == true)
            //{
            //    _BEProceso.id_tipoEmpresa = 1;
            //    _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
            //    _BEProceso.rucContratista = txtRucConsultorTab1.Text;

            //}

            //if (rbConsorcioTab1.Checked == true)
            //{
            //    _BEProceso.id_tipoEmpresa = 2;
            //    _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
            //    _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
            //    _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
            //    _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

            //    if (FileUploadAdjContratoTab1.HasFile)
            //    {
            //        _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
            //    }
            //    else
            //    { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


            //}

            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
            _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);
            int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);//2


            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                CargaDatosTab1();

                Up_Tab1.Update();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void btnGuardarOtraModalidadTab1_OnClick(object sender, EventArgs e)
        {

            if (_Metodo.ValidaFecha(txtfechaConvenioTab1.Text) == false)
            {
                string script = "<script>alert('Ingresar una fecha de convenio valida.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                //_BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue);
                //_BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

                //_BEProceso.norLic = Convert.ToInt32(txtNroLicTab1.Text);
                //_BEProceso.anio = TxtAnio2Tab1.Text;
                //_BEProceso.detalle = txtDetalleTab1.Text;
                _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                //_BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                _BEProceso.montoConsorcio = txtMontoContratadoOtrasModalidadTab1.Text;
                _BEProceso.NroContratoConsorcio = (txtNroContratoOtrasModalidadTab1.Text);

                _BEProceso.EntidadEjecutora = txtEntidadEjecutoraTab1.Text;
                _BEProceso.CoordinadorEJ = txtCoordinadorEntidadEjecutoraTab1.Text;
                _BEProceso.TelefonoCoordEJ = txtTelefonoCoordEJTab1.Text;
                _BEProceso.CorreoCoordEJ = txtCorreoCoordEJTab1.Text;
                _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);
                _BEProceso.ConvenioContrato = txtConvenioContratoTab1.Text;
                _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
                if (FileUploadOtraModalidad.HasFile)
                {
                    _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadOtraModalidad);
                }
                else
                { _BEProceso.urlContrato = LnkbtnOtraModalidad.Text; }

                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);//2


                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosTab1();

                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }



            }
        }

        protected void ImgbtnOtraModalidad_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnOtraModalidad.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnOtraModalidad.Text);
                Response.End();
            }
        }

        protected void lnkbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoContratistaTab1_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoContratistaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoContratistaTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void ddlModalidadTab1_OnSelectIndexChanged(object sender, EventArgs e)
        {

            if (ddlProcesoTab1.SelectedValue != "")
            {
                CargaDatosTab1();

                if (ddlModalidadTab1.SelectedValue != "")
                {
                    if (ddlModalidadTab1.SelectedValue == "1")
                    {
                        Panel_ProcesoDirectoTab1.Visible = true;
                        btnGuardarDirectoTab1.Visible = true;
                        Panel_ProcesoIndirectoTab1.Visible = false;

                        //cargaSeguimientoProcesoTab1();

                        cargaSubcontratosTab1();
                        //CargaResponsableTab1();

                    }

                    if (ddlModalidadTab1.SelectedValue == "2")
                    {
                        Panel_ProcesoIndirectoTab1.Visible = true;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        cargaSeguimientoProcesoTab1();
                        cargaPersonalClaveTab1();
                        // CargaDatosTab1();

                    }

                    if (ddlModalidadTab1.SelectedValue == "3")
                    {
                        Panel_ProcesoIndirectoTab1.Visible = false;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        Panel_OtrasModalidadesTab1.Visible = true;
                        //cargaSeguimientoProcesoTab1();
                        // CargaDatosTab1();

                    }

                    Up_Tab1.Update();
                }
                else
                {
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    Panel_OtrasModalidadesTab1.Visible = false;


                }
            }

            if (Convert.ToInt32(lblCOD_SUBSECTOR.Text) == 1)
            {
                Panel_ProcesoDirectoTab1.Visible = false;
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;

            }


            Up_Tab1.Update();
        }

        protected void cargaSeguimientoProcesoTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;


            grdSeguimientoProcesoTab1.DataSource = _objBLProceso.F_spMON_SeguimientoProcesoSeleccion(_BEProceso);
            grdSeguimientoProcesoTab1.DataBind();

            CargarHitos();
        }

        protected void cargaPersonalClaveTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;


            grdPersonalClaveTab1.DataSource = _objBLProceso.spMON_PersonalClave(_BEProceso);
            grdPersonalClaveTab1.DataBind();
        }

        protected void grdSeguimientoProcesoTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab1");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void CargaddlTipoAdjudicacionTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoAdjudicacion();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected Boolean ValidarSeguimientoTab1()
        {
            Boolean result;
            result = true;

            if (TxtConvocatoriaTab1.Text == "")
            {
                string script = "<script>alert('Ingrese convocatoria.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaPubliTab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de publicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaPubliTab1.Text) == false)
            {
                string script = "<script>alert('Formato de fecha de publicación no valido.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaPubliTab1.Text) > DateTime.Now.Date)
            {
                string script = "<script>alert('La fecha de publicación debe ser menor a la fecha de hoy.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtProTab1.Text.Length > 0 && Convert.ToDateTime(txtFechaPubliTab1.Text) > Convert.ToDateTime(txtProTab1.Text))
            {
                string script = "<script>alert('La fecha de publicación debe ser menor o igual a la fecha de buena pro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoAdjudicacionTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de adjudicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlResultadoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione resultado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }

        protected void btnGuardarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSeguimientoTab1())
            {
                if (validaArchivo(FileUploadSeguimientoTAb1))
                {
                    if (validaArchivo(FileUploadContratoContratistaTab1))
                    {

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                        _BEProceso.Date_fechaPublicacion = Convert.ToDateTime(txtFechaPubliTab1.Text);
                        _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                        _BEProceso.resultado = ddlResultadoTab1.SelectedValue;
                        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                        _BEProceso.observacion = txtObservacion.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                        int val = _objBLProceso.spi_MON_SeguimientoProcesoSeleccion(_BEProceso);

                        if (ddlProcesoTab1.SelectedValue == "1")
                        {
                            GuardarHitos();
                        }

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            TxtConvocatoriaTab1.Text = "";
                            txtFechaPubliTab1.Text = "";
                            ddlTipoAdjudicacionTab1.SelectedValue = "";
                            ddlResultadoTab1.SelectedValue = "";
                            txtObservacion.Text = "";

                            Panel_AgregarSeguimientoTab1.Visible = false;
                            imgbtn_AgregarSeguimientoTab1.Visible = true;
                            cargaSeguimientoProcesoTab1();
                            Up_Tab1.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }

                    }
                }
            }

        }

        private void GuardarHitos()
        {
            //guardar hitos
            string mensaje = "";
            BE_HitosProyecto _BE_HitosProyecto = new BE_HitosProyecto();
            if (TxtFechaRegistroParticipantes.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaRegistroParticipantes.Text))
                {
                    _BE_HitosProyecto.dFechaRegistroParticipantes = Convert.ToDateTime(TxtFechaRegistroParticipantes.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de registro de participantes no válida\\n";
                }
            }

            if (TxtFechaFormulacion.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaFormulacion.Text))
                {
                    _BE_HitosProyecto.dFechaFormulacion = Convert.ToDateTime(TxtFechaFormulacion.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de Formulación de consultas no válida\\n";
                }
            }

            if (TxtFechaIntegracionBases.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaIntegracionBases.Text))
                {
                    _BE_HitosProyecto.dFechaIntegracionBases = Convert.ToDateTime(TxtFechaIntegracionBases.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de integración de bases no válida\\n";
                }
            }

            if (TxtFechaPreparacionOfertas.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaPreparacionOfertas.Text))
                {
                    _BE_HitosProyecto.dFechaPreparacionOfertas = Convert.ToDateTime(TxtFechaPreparacionOfertas.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de integración de bases no válida\\n";
                }
            }

            if (TxtFechaEvaluacionOferta.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaEvaluacionOferta.Text))
                {
                    _BE_HitosProyecto.dFechaEvaluacionOferta = Convert.ToDateTime(TxtFechaEvaluacionOferta.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de evaluación de ofertas no válida\\n";
                }
            }

            if (TxtFechaPresentacionDocumentoContrato.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaPresentacionDocumentoContrato.Text))
                {
                    _BE_HitosProyecto.dFechaPresentacionDocumentoContrato = Convert.ToDateTime(TxtFechaPresentacionDocumentoContrato.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de presentación de documentos para contrato no válida\\n";
                }
            }

            if (TxtFechaActoPosteriorFirmaContrato.Text != "")
            {
                if (_Metodo.IsDate(TxtFechaActoPosteriorFirmaContrato.Text))
                {
                    _BE_HitosProyecto.dFechaActoPosteriorFirmaContrato = Convert.ToDateTime(TxtFechaActoPosteriorFirmaContrato.Text);
                }
                else
                {
                    mensaje = mensaje + "*Fecha de documentación previa al inicio de obra no válida\\n";
                }
            }

            if (mensaje != "")
            {
                string script = "<script>alert('" + mensaje + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Scriptmensaje", script, false);
            }
            else
            {
                _BE_HitosProyecto.iId = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE_HitosProyecto.EtapaRegistro = 2;
                _BE_HitosProyecto.IdUsuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val2 = _objBLProceso.paSSP_HitosProyecto(_BE_HitosProyecto); //1
                //string script = "<script>alert('Se guardo correctamente');</script>";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Scriptmensaje", script, false);
            }

        }

        //protected void imgDocResponsableTab1_OnClick(object sender, EventArgs e)
        //{
        //    ImageButton boton;
        //    GridViewRow row;
        //    boton = (ImageButton)sender;
        //    row = (GridViewRow)boton.NamingContainer;
        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


        //    ImageButton url = (ImageButton)grdResponsableTab1.Rows[row.RowIndex].FindControl("imgDocResponsableTab1");
        //    string urlDocumento = url.ToolTip;

        //    FileInfo toDownload = new FileInfo(pat + urlDocumento);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + urlDocumento);
        //        Response.End();
        //    }
        //}

        protected void imgDocTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdSeguimientoProcesoTab1.Rows[row.RowIndex].FindControl("imgDocTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgDocSubContratoTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdSubcontratosTab1.Rows[row.RowIndex].FindControl("imgDocSubContratoTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void rbConsultorTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsultorTab1.Font.Bold = true;
            lblNomConsorcioTab1.Font.Bold = false;
            Up_lblNomConsultorTab1.Update();
            Up_lblNomConsorcioTab1.Update();

            rbConsorcioTab1.Checked = false;
            Up_rbConsorcioTab1.Update();
            Panel_ConsultorTab1.Visible = true;
            Panel_ConsorcioTab1.Visible = false;
            Up_rbProcesoTab1.Update();
        }

        protected void rbConsorcioTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsorcioTab1.Font.Bold = true;
            lblNomConsultorTab1.Font.Bold = false;
            Up_lblNomConsorcioTab1.Update();
            Up_lblNomConsultorTab1.Update();

            rbConsultorTab1.Checked = false;
            Up_rbConsultorTab1.Update();
            Panel_ConsorcioTab1.Visible = true;
            Panel_ConsultorTab1.Visible = false;
            Up_rbProcesoTab1.Update();

        }

        protected void imgbtn_AgregarSeguimientoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            lblNomUsuarioSeguimientoProceso.Text = "";
            btnGuardarSeguimientoProcesoTab1.Visible = true;
            btnModificarSeguimientoProcesoTab1.Visible = false;
            Up_Tab1.Update();
        }

        protected void btnCancelaSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtn_AgregarConsultorTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = true;
            btn_modificarConsultor.Visible = false;
            Up_Tab1.Update();
        }

        protected void btn_cancelarConsultor_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected Boolean ValidarResultadoTab1()
        {
            Boolean result;
            result = true;

            //if (ddlAdjudicacionTab1.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione tipo de Adjudicación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtNroLicTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese N° de Licitación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (TxtAnio2Tab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese año.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtDetalleTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese detalle.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtProTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Buena Pro.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (!txtProTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }
            }

            if (!txtProConsentidaTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtProConsentidaTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Buena Pro Consentida.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtProConsentidaTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de Buena Pro Consentida no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }
            }

            if (!txtFechaFirmaContratoTab1.Text.Equals(""))
            {
                if (_Metodo.ValidaFecha(txtFechaFirmaContratoTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de firma de contrato.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {

                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > DateTime.Today)
                    {
                        string script = "<script>alert('La Fecha de firma de contrato ingresada es mayor a la fecha de hoy.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }


                if (!txtProTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato no puede ser menor a la Fecha de Buena Pro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;

                    }
                }

                if (!txtProConsentidaTab1.Text.Equals(""))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) < Convert.ToDateTime(txtProConsentidaTab1.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato no puede ser menor a la Fecha de Buena Pro Consentida.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length > 0 && ddlProcesoTab1.SelectedValue.Equals("1"))
                {
                    if (Convert.ToDateTime(txtFechaFirmaContratoTab1.Text) > Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text))
                    {
                        string script = "<script>alert('La Fecha de firma de contrato debe ser menor a la fecha de entrega de terreno.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }

            if (rbConsultorTab1.Checked == true)
            {
                if (txtNombConsultorTab1.Text == "")
                {
                    txtNombConsultorTab1.Focus();
                    string script = "<script>alert('Ingrese nombre del contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (txtRucConsultorTab1.Text == "")
                {
                    txtRucConsultorTab1.Focus();
                    string script = "<script>alert('Ingrese RUC de contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }

            if (rbConsorcioTab1.Checked == true)
            {
                if (txtNomconsorcioTab1.Text == "")
                {
                    txtNomconsorcioTab1.Focus();

                    string script = "<script>alert('Ingrese nombre del consorcio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }

            return result;
        }

        protected void btnGuardarTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (validaArchivo(FileUploadAdjContratoTab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                    if (ddlAdjudicacionTab1.SelectedValue == "")
                    {
                        _BEProceso.id_tipoAdjudicacion = 0;
                    }
                    else
                    { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }

                    _BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

                    if (txtNroLicTab1.Text == "")
                    { txtNroLicTab1.Text = "0"; }
                    _BEProceso.norLic = (txtNroLicTab1.Text);

                    _BEProceso.anio = TxtAnio2Tab1.Text;
                    _BEProceso.detalle = txtDetalleTab1.Text;
                    _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                    _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                    if (txtValorReferencialTab1.Text == "")
                    { txtValorReferencialTab1.Text = "0"; }
                    _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                    if (txtMontoContratadoTab1.Text == "")
                    { txtMontoContratadoTab1.Text = "0"; }

                    _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                    _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
                    if (rbConsultorTab1.Checked == true)
                    {
                        _BEProceso.id_tipoEmpresa = 1;
                        _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                        _BEProceso.rucContratista = txtRucConsultorTab1.Text;
                        _BEProceso.representanteContratista = txtRepresentanteLegalConsultorTab1.Text;

                        if (FileUploadContratoContratistaTab1.HasFile)
                        {
                            _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                        }
                        else
                        { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }
                    }

                    if (rbConsorcioTab1.Checked == true)
                    {
                        _BEProceso.id_tipoEmpresa = 2;
                        _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                        _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                        _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                        _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                        if (FileUploadAdjContratoTab1.HasFile)
                        {
                            _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                        }
                        else
                        { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                    }

                    _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);




                    int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso); //1

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab1();

                        Up_Tab1.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                }
            }

            var s = TabContainer;
        }

        protected Boolean ValidarGrupoConsorcioTab1()
        {
            Boolean result;
            result = true;

            if (txtContratistaGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese nombre de contratista.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtRucGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese N° de RUC.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtRepresentanGrupTAb1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Representante Legal.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            return result;
        }

        protected void btn_guardarConsultor_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (ValidarGrupoConsorcioTab1())
                {
                    if (validaArchivo(FileUploadAdjContratoTab1))
                    {
                        //REGISTRAR PROCESO DE SELECCION INDIRECTA

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                        if (ddlAdjudicacionTab1.SelectedValue == "")
                        {
                            _BEProceso.id_tipoAdjudicacion = 0;
                        }
                        else
                        { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }

                        _BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

                        if (txtNroLicTab1.Text == "")
                        { txtNroLicTab1.Text = "0"; }
                        _BEProceso.norLic = (txtNroLicTab1.Text);

                        _BEProceso.anio = TxtAnio2Tab1.Text;
                        _BEProceso.detalle = txtDetalleTab1.Text;
                        _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                        _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                        if (txtValorReferencialTab1.Text == "")
                        { txtValorReferencialTab1.Text = "0"; }
                        _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                        if (txtMontoContratadoTab1.Text == "")
                        { txtMontoContratadoTab1.Text = "0"; }

                        _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                        _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
                        if (rbConsultorTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 1;
                            _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                            _BEProceso.rucContratista = txtRucConsultorTab1.Text;

                            if (FileUploadContratoContratistaTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }
                        }

                        if (rbConsorcioTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 2;
                            _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                            _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                            _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                            _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                            if (FileUploadAdjContratoTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                        }

                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);
                        _BEProceso.Date_fechaContrato = VerificaFecha(txtFechaFirmaContratoTab1.Text);
                        int valTab1 = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso); //1


                        //REGISTRA CONTRATISTA
                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                        _BEProceso.rucContratista = txtRucGrupTab1.Text;
                        _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        int val = _objBLProceso.spi_MON_GrupoConsorcio(_BEProceso);

                        if (val == 1 && valTab1 == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            cargaConsultoresTab1();

                            txtContratistaGrupTab1.Text = "";
                            txtRucGrupTab1.Text = "";
                            txtRepresentanGrupTAb1.Text = "";

                            Panel_AgregarConsultorTab1.Visible = false;
                            imgbtn_AgregarConsultorTab1.Visible = true;

                            Up_Tab1.Update();

                            CargaDatosTab1();
                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }

                }
            }

        }

        protected Boolean ValidarSubContratoTab1()
        {
            Boolean result;
            result = true;

            if (ddlRubroTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Rubro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtnombreTab1.Text == "")
            {
                string script = "<script>alert('Ingrese Nombre');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese monto.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtPlazoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese Plazo (Días).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlModalidaSubcontratoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese modalidad.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }

        protected void btnGuardarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSubContratoTab1())
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.nombre = txtnombreTab1.Text;
                _BEProceso.id_rubro = Convert.ToInt32(ddlRubroTab1.SelectedValue);
                _BEProceso.plazo = Convert.ToInt32(txtPlazoTab1.Text);
                _BEProceso.monto = txtMontoTab1.Text;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidaSubcontratoTab1.SelectedValue);
                _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spi_MON_SubContrato(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSubcontratosTab1();

                    // ddlModalidadTab1.SelectedValue = "";
                    txtnombreTab1.Text = "";
                    ddlRubroTab1.SelectedValue = "";
                    txtPlazoTab1.Text = "";
                    txtMontoTab1.Text = "";
                    ddlModalidaSubcontratoTab1.SelectedValue = "";

                    Panel_SubcontratosTab1.Visible = false;
                    imgbtnAgregarSubcontratoTab1.Visible = true;

                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }

        protected void imgbtnAgregarSubcontratoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_SubcontratosTab1.Visible = true;
            imgbtnAgregarSubcontratoTab1.Visible = false;
            btnGuardarSubcontratosTab1.Visible = true;
            btnModificarSubcontratosTab1.Visible = false;
            Up_Tab1.Update();
        }

        protected void btnCancelarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {
            Panel_SubcontratosTab1.Visible = false;
            imgbtnAgregarSubcontratoTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void grdSubcontratosTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocSubContratoTab1");

                GeneraIcoFile(imb.ToolTip, imb);


            }
        }

        protected void cargaSubcontratosTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdSubcontratosTab1.DataSource = _objBLProceso.F_spMON_SubContrato(_BEProceso);
            grdSubcontratosTab1.DataBind();


        }

        //protected void CargaResponsableTab1()
        //{
        //    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

        //    //grdResponsableTab1.DataSource = _objBLProceso.spMON_ResponsableObra(_BEProceso);
        //    //grdResponsableTab1.DataBind();


        //}

        //protected void imgbtnAgregarResponsableTab1_OnClick(object sender, EventArgs e)
        //{
        //    Panel_ResponsableTab1.Visible = true;
        //    imgbtnAgregarResponsableTab1.Visible = false;
        //    btnGuardarResponsableObraTab1.Visible = true;
        //    btnModificarResponsableObraTab1.Visible = false;
        //    Up_Tab1.Update();
        //}

        //protected void btnCancelarResponsableTab1_OnClick(object sender, EventArgs e)
        //{
        //    Panel_ResponsableTab1.Visible = false;
        //    imgbtnAgregarResponsableTab1.Visible = true;
        //    Up_Tab1.Update();
        //}

        //protected Boolean ValidarResponsableTab1()
        //{
        //    Boolean result;
        //    result = true;

        //    if (txtCargoTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese cargo.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    if (txtNombreResponsableTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese Nombre');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }


        //    if (txtResolucionTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese N° de Resolución.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    if (txtCipTab1.Text == "")
        //    {
        //        string script = "<script>alert('Ingrese CIP.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }

        //    if (ddlModalidadResponsableTab1.SelectedValue == "")
        //    {
        //        string script = "<script>alert('Ingrese Modalidad.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }


        //    return result;
        //}

        //protected void btnGuardarResponsableObraTab1_OnClick(object sender, EventArgs e)
        //{
        //    if (ValidarResponsableTab1())
        //    {
        //        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

        //        _BEProceso.nombre = txtNombreResponsableTab1.Text;
        //        _BEProceso.cargo = txtCargoTab1.Text;
        //        _BEProceso.nroResolucion = (txtResolucionTab1.Text);
        //        _BEProceso.cip = txtCipTab1.Text;
        //        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadResponsableTab1.SelectedValue);
        //        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResponsableTab1);
        //        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLProceso.spi_MON_ResponsableObra(_BEProceso);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se registró correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //CargaResponsableTab1();

        //            txtnombreTab1.Text = "";
        //            txtCargoTab1.Text = "";
        //            txtResolucionTab1.Text = "";
        //            txtCipTab1.Text = "";
        //            ddlModalidadResponsableTab1.SelectedValue = "";

        //            Panel_ResponsableTab1.Visible = false;
        //            imgbtnAgregarResponsableTab1.Visible = true;

        //            Up_Tab1.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }


        //}
        protected void grdResponsableTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocResponsableTab1");

                GeneraIcoFile(imb.ToolTip, imb);



            }
        }

        protected void btnModificarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSeguimientoTab1())
            {


                _BEProceso.id_seguimiento = Convert.ToInt32(lblRegistroSeguimientoP.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                _BEProceso.Date_fechaPublicacion = Convert.ToDateTime(txtFechaPubliTab1.Text);
                _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                _BEProceso.resultado = ddlResultadoTab1.SelectedValue;

                if (FileUploadSeguimientoTAb1.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSeguimientoTAb1.Text;
                }


                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);


                _BEProceso.observacion = txtObservacion.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                if (ddlProcesoTab1.SelectedValue == "1")
                {
                    GuardarHitos();
                }


                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Procesos_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();


                    TxtConvocatoriaTab1.Text = "";
                    txtFechaPubliTab1.Text = "";
                    ddlTipoAdjudicacionTab1.SelectedValue = "";
                    ddlResultadoTab1.SelectedValue = "";
                    txtObservacion.Text = "";
                    LnkbtnSeguimientoTAb1.Text = "";
                    imgbtnSeguimientoTAb1.Visible = false;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSeguimientoTab1.Visible = false;
                btnModificarSeguimientoProcesoTab1.Visible = true;
                cargaSeguimientoProcesoTab1();
                Up_Tab1.Update();
                imgbtn_AgregarSeguimientoTab1.Visible = true;

            }



        }


        protected void btnModificarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSubContratoTab1())
            {


                _BEProceso.id_subContratos = Convert.ToInt32(lblId_SubContratos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.id_rubro = Convert.ToInt32(ddlRubroTab1.SelectedValue);
                _BEProceso.nombre = txtnombreTab1.Text;
                _BEProceso.monto = txtMontoTab1.Text;
                _BEProceso.plazo = Convert.ToInt32(txtPlazoTab1.Text);
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidaSubcontratoTab1.SelectedValue);

                if (FileUploadSubContrato.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSubContrato.Text;
                }





                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_SubContrato_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();


                    ddlRubroTab1.SelectedValue = "";
                    txtnombreTab1.Text = "";
                    txtMontoTab1.Text = "";
                    txtPlazoTab1.Text = "";
                    ddlModalidaSubcontratoTab1.SelectedValue = "";
                    LnkbtnSubContrato.Text = "";
                    imgbtnSubContrato.Visible = false;
                    imgbtnAgregarSubcontratoTab1.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_SubcontratosTab1.Visible = false;
                cargaSubcontratosTab1();
                btnModificarSubcontratosTab1.Visible = true;
                Up_Tab1.Update();

            }


        }

        //protected void btnModificarResponsableObraTab1_OnClick(object sender, EventArgs e)
        //{

        //    if (ValidarResponsableTab1())
        //    {


        //        _BEProceso.id_responsable = Convert.ToInt32(lblId_Responsable_Obra.Text);
        //        //_BEFinanciamiento.tipoFinanciamiento = 3;
        //        _BEProceso.cargo = txtCargoTab1.Text;
        //        _BEProceso.nroResolucion = (txtResolucionTab1.Text);
        //        _BEProceso.nombre = txtNombreResponsableTab1.Text;
        //        _BEProceso.cip = txtCipTab1.Text;
        //        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadResponsableTab1.SelectedValue);

        //        if (FileUploadResponsableTab1.PostedFile.ContentLength > 0)
        //        {
        //            _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResponsableTab1);
        //        }
        //        else
        //        {
        //            _BEProceso.urlDoc = LnkbtnResponsableObraTab1c.Text;
        //        }

        //        //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResponsableTab1);

        //        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        int resul;
        //        resul = _objBLProceso.spud_MON_Seguimiento_Responsable_Editar(_BEProceso);
        //        if (resul == 1)
        //        {
        //            string script = "<script>alert('Registro Correcto.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //            CargaFinanTransferencia();


        //            txtCargoTab1.Text = "";
        //            txtResolucionTab1.Text = "";
        //            txtNombreResponsableTab1.Text = "";
        //            txtCipTab1.Text = "";
        //            ddlModalidadResponsableTab1.SelectedValue = "";
        //            LnkbtnResponsableObraTab1c.Text = "";
        //            imgbtnResponsableObraTab1.Visible = false;




        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //        Panel_ResponsableTab1.Visible = false;
        //        //CargaResponsableTab1();
        //        btnModificarResponsableObraTab1.Visible = true;
        //        imgbtnAgregarResponsableTab1.Visible = true;
        //        Up_Tab1.Update();

        //    }
        //}


        protected void btn_modificarConsultor_OnClick(object sender, EventArgs e)
        {

            if (ValidarGrupoConsorcioTab1())
            {

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(lblId_Registro_Contratista.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                _BEProceso.rucContratista = txtRucGrupTab1.Text;
                _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Consorcio_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();

                    txtContratistaGrupTab1.Text = "";
                    txtRucGrupTab1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarConsultorTab1.Visible = false;
                cargaConsultoresTab1();
                btn_modificarConsultor.Visible = true;
                imgbtn_AgregarConsultorTab1.Visible = true;
                Up_Tab1.Update();


            }
        }

        protected void grdSeguimientoProcesoTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoProcesoTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_seguimiento = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Procesos_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSeguimientoProcesoTab1();
                    Up_Tab1.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdSubcontratosTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSubcontratosTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_subContratos = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_SubContrato_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSubcontratosTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        //protected void grdResponsableTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grdResponsableTab1.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEProceso.id_responsable = Convert.ToInt32(objTemp.ToString());
        //        //_BEFinaTra.tipo = 2;
        //        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLProceso.spud_MON_Seguimiento_Responsable_Eliminar(_BEProceso);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //CargaResponsableTab1();
        //            Up_Tab1.Update();
        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        protected void grdConsultoresTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdConsultoresTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Consorcio_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaConsultoresTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        #endregion

        #region Tab2
        protected void CargaTab2()
        {
            CargaTipoCartaFianza();
            CargaTipoAccion();
            dt = CargaEmpleadoObraTab2(2);
            grd_HistorialInspectorTab2.DataSource = dt;
            grd_HistorialInspectorTab2.DataBind();

            dt = CargaEmpleadoObraTab2(1);
            grd_HistorialResidenteTab2.DataSource = dt;
            grd_HistorialResidenteTab2.DataBind();

            dt = CargaEmpleadoObraTab2(3);
            grd_HistorialSupervisorTab2.DataSource = dt;
            grd_HistorialSupervisorTab2.DataBind();

            dt = CargaEmpleadoObraTab2(4);
            grd_HistorialCoordinador.DataSource = dt;
            grd_HistorialCoordinador.DataBind();

            CargaDatosTab2();

            cargaCartasFianzasTab2();

            if (!this.IsPostBack)
            {
                grdAvanceFisicoTab2.PageIndex = Int32.MaxValue;
            }
            CargaAvanceFisico();
            CargaTipoMonitoreo(); //Combo Detalle
            CargaTipoDocumentos();

            CargaTipoEstadoSituacionalTab2();
            CargaTipoValorizacion();

            if (lblCOD_SUBSECTOR.Text == "1") //PNSU
            {
                trGEI.Visible = true;
            }

            if (!this.IsPostBack)
            {
                grdEvaluacionTab2.PageIndex = Int32.MaxValue;

            }
            cargarEvaluacionTab2();
            CargaAdelanto();

        }

        protected void CargaTipoCartaFianza()
        {
            ddlTipoTab2.DataSource = _objBLEjecucion.F_spMON_TipoCartaFianza();
            ddlTipoTab2.DataTextField = "nombre";
            ddlTipoTab2.DataValueField = "valor";
            ddlTipoTab2.DataBind();

            ddlTipoTab2.Items.Insert(0, new ListItem("SELECCIONAR", ""));

        }

        protected void CargaTipoAccion()
        {
            List<BE_MON_BANDEJA> ListComboTipo = new List<BE_MON_BANDEJA>();

            ListComboTipo = _objBLEjecucion.F_spMON_TipoAccionCarta();
            ddlTipoAccion.DataSource = ListComboTipo;
            ddlTipoAccion.DataTextField = "nombre";
            ddlTipoAccion.DataValueField = "valor";
            ddlTipoAccion.DataBind();

            ddlTipoAccion.Items.Insert(0, new ListItem("SELECCIONAR", ""));
        }
        protected void CargaTipoValorizacion()
        {
            ddlValorizacionTab2.DataSource = _objBLEjecucion.F_spMON_TipoValorizacion();
            ddlValorizacionTab2.DataTextField = "nombre";
            ddlValorizacionTab2.DataValueField = "valor";
            ddlValorizacionTab2.DataBind();


            //ddlValorizacionTab2.Items.Insert(0, new ListItem("Valorización Normal", "1"));
            //ddlValorizacionTab2.Items.Insert(0, new ListItem("Valorización por Mayores Gastos", "3"));
            //ddlValorizacionTab2.Items.Insert(0, new ListItem("Valorizacion por Intereses", "4"));

            ddlValorizacionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoMonitoreo()
        {
            ddlTipoMonitoreo.DataSource = _objBLEjecucion.F_spMON_ListarTipoMonitoreo();
            ddlTipoMonitoreo.DataTextField = "nombre";
            ddlTipoMonitoreo.DataValueField = "valor";
            ddlTipoMonitoreo.DataBind();

            ddlTipoMonitoreo.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoEstadosDetalle()
        {
            ddlTipoEstadoInformeTab2.ClearSelection();

            ddlTipoEstadoInformeTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(3, 1);
            ddlTipoEstadoInformeTab2.DataTextField = "nombre";
            ddlTipoEstadoInformeTab2.DataValueField = "valor";
            ddlTipoEstadoInformeTab2.DataBind();

            ddlTipoEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void CargaTipoSubEstadosDetalle(string TipoEstadoEjecucion)
        {
            ddlTipoSubEstadoInformeTab2.ClearSelection();

            if (!TipoEstadoEjecucion.Equals(""))
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                list = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(TipoEstadoEjecucion));

                ddlTipoSubEstadoInformeTab2.DataSource = list;
                ddlTipoSubEstadoInformeTab2.DataTextField = "nombre";
                ddlTipoSubEstadoInformeTab2.DataValueField = "valor";
                ddlTipoSubEstadoInformeTab2.DataBind();

                //ddlTipoSubEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

                if (list.Count == 0)
                {
                    lblNombreSubEstado.Visible = false;
                    ddlTipoSubEstadoInformeTab2.Visible = false;
                }
                else
                {
                    lblNombreSubEstado.Visible = true;
                    ddlTipoSubEstadoInformeTab2.Visible = true;
                }
            }

            ddlTipoSubEstadoInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void CargaTipoSubEstado2(string pTipoSubEstado)
        {
            //ddlTipoSubEstado2InformeTab2.DataSource = null;
            //ddlTipoSubEstado2InformeTab2.DataBind();
            ddlTipoSubEstado2InformeTab2.Items.Clear();

            List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

            if (!(pTipoSubEstado == ""))
            {
                list = _objBLEjecucion.F_spMON_TipoSubEstado2(Convert.ToInt32(pTipoSubEstado));
            }
            else
            {
                list = null;
            }

            ddlTipoSubEstado2InformeTab2.DataSource = list;
            ddlTipoSubEstado2InformeTab2.DataTextField = "nombre";
            ddlTipoSubEstado2InformeTab2.DataValueField = "valor";
            ddlTipoSubEstado2InformeTab2.DataBind();

            ddlTipoSubEstado2InformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }


        protected void CargaTipoSubEstadoParalizado(string pTipoEstadoParalizado)
        {
            chbProblematicaTab2.Items.Clear();

            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("82"))//PERMANENTE
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                if (!(pTipoEstadoParalizado == ""))
                {
                    list = _objBLEjecucion.F_spMON_TipoSubEstadoParalizado(Convert.ToInt32(pTipoEstadoParalizado));
                }
                else
                {
                    list = null;
                }
                //ddlTipoProblematicaTab2.DataSource = list;
                //ddlTipoProblematicaTab2.DataTextField = "nombre";
                //ddlTipoProblematicaTab2.DataValueField = "valor";
                //ddlTipoProblematicaTab2.DataBind();

                chbProblematicaTab2.DataSource = list;
                chbProblematicaTab2.DataTextField = "nombre";
                chbProblematicaTab2.DataValueField = "valor";
                chbProblematicaTab2.DataBind();
            }

            //ddlTipoProblematicaTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void CargaTipoDocumentos()
        {
            ddlTipoDocumento.DataSource = _objBLEjecucion.F_spMON_ListarTipoDocumentoEvaluacionRecomendacion();
            ddlTipoDocumento.DataTextField = "nombre";
            ddlTipoDocumento.DataValueField = "valor";
            ddlTipoDocumento.DataBind();

            ddlTipoDocumento.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected DataTable CargaEmpleadoObraTab2(int tipo)
        {
            DataTable dt;
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoObra(_BEEjecucion);
            return dt;

            //  grd.DataBind();
        }

        protected void grdAvanceFisicoTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_Avance.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoTab2.SelectedRow;
            ddlValorizacionTab2.Text = ((Label)row.FindControl("lblid_tipoValorizacion")).Text;
            txtfechaTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtMontoTab2.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtFisiReal.Text = ((Label)row.FindControl("lblFisicoReal")).Text;
            lblFisRealPreAcumuladoTab2.Text = (Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) - Convert.ToDouble(txtFisiReal.Text)).ToString();

            txtFisiProgTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            lblFisProgPreAcumuladoTab2.Text = (Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text) - Convert.ToDouble(txtFisiProgTab2.Text)).ToString();

            txtFinanRealTab2.Text = ((Label)row.FindControl("lblFinancieroReal")).Text;
            lblFinancieroRealPreAcumuladoTab2.Text = (Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text) - Convert.ToDouble(txtFinanRealTab2.Text)).ToString();

            txtFinanProgTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            lblFinancieroProgPreAcumuladoTab2.Text = (Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text) - Convert.ToDouble(txtFinanProgTab2.Text)).ToString();

            txtObservaciontab2.Text = ((Label)row.FindControl("lblObservacion")).Text;
            ddlEstadoSituacionalTab2.Text = ((Label)row.FindControl("lblid_tipoEstadoSituacional")).Text;
            txtNroAdicionalTab2.Text = ((Label)row.FindControl("lblAdicional")).Text;
            LnkbtnAvanceTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;

            lblNomUsuarioAvance.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnAvanceTab2.Text, imgbtnAvanceTab2);


            btnGuardarAvanceTab2.Visible = false;
            btnModificarAvanceTab2.Visible = true;
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;

            Up_Tab2.Update();

        }


        protected void grdAvanceFisicoAdicionalTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoAdicionalTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_AvanceAdicional.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoAdicionalTab2.SelectedRow;
            ddlValorizacionAdicionalTab2.Text = ((Label)row.FindControl("lblid_tipoValorizacion")).Text;
            txtfechaAdicionalTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtMontoAdicionalTab2.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtFisiRealAdicional.Text = ((Label)row.FindControl("lblFisicoReal")).Text;
            txtFisiProgAdicionalTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            txtFinanRealAdicionalTab2.Text = ((Label)row.FindControl("lblFinancieroReal")).Text;
            txtFinanProgAdicionalTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            txtObservacionAdicionaltab2.Text = ((Label)row.FindControl("lblObservacion")).Text;
            ddlEstadoSituacionalAdicionalTab2.Text = ((Label)row.FindControl("lblid_tipoEstadoSituacional")).Text;
            txtNroAdicionalAdicionalTab2.Text = ((Label)row.FindControl("lblAdicional")).Text;
            LnkbtnAvanceAdicionalTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceAdicionalTab2")).ToolTip;

            lblNomUsuarioAvanceAdicional.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnAvanceAdicionalTab2.Text, imgbtnAvanceAdicionalTab2);

            btnGuardarAvanceAdicionalTab2.Visible = false;
            btnModificarAvanceAdicionalTab2.Visible = true;
            Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoAdicionalTab2.Visible = false;

            Up_Tab2.Update();

        }

        protected void cargaCartasFianzasTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdFianzasTab2.DataSource = _objBLEjecucion.spMON_CartaFianza(_BEEjecucion);
            grdFianzasTab2.DataBind();
        }

        protected void grdFianzasTab2_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDiasRestante = (Label)e.Row.FindControl("lblDiasRestante");
                if (Convert.ToInt32(lblDiasRestante.Text) < 1)
                {
                    lblDiasRestante.ForeColor = Color.Red;
                }

                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocCartaTab2");
                GeneraIcoFile(imb.ToolTip, imb);


                CheckBox chkVer = (CheckBox)e.Row.FindControl("chb_flagVerifico");
                if (chkVer.Text == "1")
                {
                    chkVer.Checked = true;
                }
                chkVer.Text = "";

                ImageButton imgDocAccionGriTab2 = (ImageButton)e.Row.FindControl("imgDocAccionGriTab2");
                if (imgDocAccionGriTab2.ToolTip.Length > 0)
                {
                    GeneraIcoFile(imgDocAccionGriTab2.ToolTip, imgDocAccionGriTab2);
                }
                else { imgDocAccionGriTab2.Visible = false; }

                Label lblIdAccion = (Label)e.Row.FindControl("lblIdTipoAccion");
                if (lblIdAccion.Text.Equals("2") || lblIdAccion.Text.Equals("3") || lblIdAccion.Text.Equals("4"))
                {
                    lblDiasRestante.Text = "";
                }

                if (lblID_ACCESO.Text.Equals("0"))
                {
                    chkVer.Enabled = false;
                    //chkRenuev.Enabled = false;
                }

            }
        }

        protected void imgDocCartaTab2_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdFianzasTab2.Rows[row.RowIndex].FindControl("imgDocCartaTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void CargaAvanceFisico()
        {
            DataTable dtValorizacion = new DataTable();

            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            dtValorizacion = _objBLEjecucion.spMON_AvanceFisico(_BEEjecucion);
            grdAvanceFisicoTab2.DataSource = dtValorizacion;
            grdAvanceFisicoTab2.DataBind();

            DataTable dtAdicional = new DataTable();
            dtAdicional = _objBLEjecucion.spMON_AvanceFisicoAdicional(_BEEjecucion);
            if (dtAdicional.Rows.Count > 0)
            {
                grdAvanceFisicoAdicionalTab2.DataSource = dtAdicional;
                grdAvanceFisicoAdicionalTab2.DataBind();

                trTab2Adicional1.Visible = true;
                trTab2Adicional2.Visible = true;
                trTab2Adicional3.Visible = true;
                trTab2Adicional4.Visible = true;
            }

            txtMontoTotalTab2.Text = (dtValorizacion.AsEnumerable().Sum(x => x.Field<Decimal>("monto"))).ToString("N");
            lblFisicoRealAcumuladoTab2.Text = (dtValorizacion.AsEnumerable().Sum(x => x.Field<Decimal>("fisicoReal"))).ToString("N");
            lblFisicoProgAcumuladoTab2.Text = (dtValorizacion.AsEnumerable().Sum(x => x.Field<Decimal>("fisicoProgramado"))).ToString("N");
            lblFinancieroRealAcumuladoTab2.Text = (dtValorizacion.AsEnumerable().Sum(x => x.Field<Decimal>("financieroReal"))).ToString("N");
            lblFinancieroProgAcumuladoTab2.Text = (dtValorizacion.AsEnumerable().Sum(x => x.Field<Decimal>("financieroProgramado"))).ToString("N");

            txtMontoTotalAdicionalTab2.Text = (dtAdicional.AsEnumerable().Sum(x => x.Field<Decimal>("monto"))).ToString("N");
            txtFisicoRealAdicionalTab2.Text = (dtAdicional.AsEnumerable().Sum(x => x.Field<Decimal>("fisicoReal"))).ToString("N");
            txtFisicoProgAdicionalTab2.Text = (dtAdicional.AsEnumerable().Sum(x => x.Field<Decimal>("fisicoProgramado"))).ToString("N");
            txtFinancieroRealAdicionalTab2.Text = (dtAdicional.AsEnumerable().Sum(x => x.Field<Decimal>("financieroReal"))).ToString("N");
            txtFinancieroProgAdicionalTab2.Text = (dtAdicional.AsEnumerable().Sum(x => x.Field<Decimal>("financieroProgramado"))).ToString("N");

            DataTable dtValorizacion2 = BLUtil.ordenarDt(dtValorizacion, "fecha_update", "DESC");

            if (dtValorizacion2.Rows.Count > 0)
            {
                String s = dtValorizacion2.Rows[0]["usuario"].ToString();
                lblFechaUltimaActualizacionValorizacionTab2.Text = "(Actualizó: " + dtValorizacion2.Rows[0]["usuario"].ToString() + " - " + dtValorizacion2.Rows[0]["fecha_update"].ToString() + ")";
            }

        }

        protected void imgDocAvanceTab2_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocAvanceTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        //AQUI
        protected void chb_flagVerifico_Check(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var val2 = grdFianzasTab2.DataKeys[row.RowIndex].Value;
            _BEEjecucion.id_tabla = Convert.ToInt32(val2.ToString());
           
            CheckBox chb_flagVerifico = (CheckBox)grdFianzasTab2.Rows[row.RowIndex].FindControl("chb_flagVerifico");
            if (chb_flagVerifico.Checked == true)
            {
                _BEEjecucion.flagVerificado = "1";
            }

            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_CartaFianza_FlagVerifica(_BEEjecucion);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                cargaCartasFianzasTab2();

                Up_Tab2.Update();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }
           
        protected void imgbtnAvanceFisicoTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;
            lblNomUsuarioAvance.Text = "";
            btnGuardarAvanceTab2.Visible = true;
            btnModificarAvanceTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void imgbtnAvanceFisicoAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoAdicionalTab2.Visible = false;
            lblNomUsuarioAvanceAdicional.Text = "";
            btnGuardarAvanceAdicionalTab2.Visible = true;
            btnModificarAvanceAdicionalTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = false;
            imgbtnAvanceFisicoTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceAdicionalTab2.Visible = false;
            imgbtnAvanceFisicoAdicionalTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void imgbtn_residenteObraTab2_OnClick(object sender, EventArgs e)
        {
            Panel_HistorialResidenteTab2.Visible = true;
            btnGuardarResidenteTab2.Visible = true;
            btnModificarResidenteTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void imgbtn_SupervisorTab2_OnClick(object sender, EventArgs e)
        {
            Panel_HistorialSupervisorTab2.Visible = true;
            btnGuardarSupervisorTab2.Visible = true;
            btnModificarSupervisorTab2.Visible = false;

            Up_Tab2.Update();
        }
        protected void imgbtn_CoordinadorTab2_OnClick(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = true;
            btnGuardar_Coordinador.Visible = true;
            btnModificar_Coordinador.Visible = false;
            Up_Tab2.Update();
        }

        protected void imgbtnMostrarFecha_OnClick(object sender, EventArgs e)
        {
            //Panel_HistorialSupervisorTab2.Visible = true;
            //btnGuardarSupervisorTab2.Visible = true;
            //btnModificarSupervisorTab2.Visible = false;

            //Up_Tab2.Update();

            lblFecha.Visible = true;
            txtFechaTerminoTab2.Visible = true;
        }

        protected void btnCancelarResidenteTab2_Onclick(object sender, EventArgs e)
        {
            Panel_HistorialResidenteTab2.Visible = false;

            Up_Tab2.Update();
        }
        protected void btnCancelarCoordinador_Onclick(object sender, EventArgs e)
        {
            Panel_Coordinador.Visible = false;

            Up_Tab2.Update();
        }
        protected void btnCancelarSupervisorTab2_Onclick(object sender, EventArgs e)
        {
            Panel_HistorialSupervisorTab2.Visible = false;

            Up_Tab2.Update();
        }

        protected void ImgbtnActualizarInspector_OnClick(object sender, EventArgs e)
        {
            Panel_HistorialInspectorTab2.Visible = true;
            btnGrabarInspectorTab2.Visible = true;
            btnModificarInspectorTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void btnCancelarInspectorTab2_Onclick(object sender, EventArgs e)
        {
            Panel_HistorialInspectorTab2.Visible = false;

            Up_Tab2.Update();
        }

        //protected void CargaTipoEstadoTab2()
        //{
        //    ddlEstadoTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(3);
        //    ddlEstadoTab2.DataTextField = "nombre";
        //    ddlEstadoTab2.DataValueField = "valor";
        //    ddlEstadoTab2.DataBind();

        //    ddlEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}

        protected void CargaTipoEstadoSituacionalTab2()
        {
            ddlEstadoSituacionalTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoSituacional();
            ddlEstadoSituacionalTab2.DataTextField = "nombre";
            ddlEstadoSituacionalTab2.DataValueField = "valor";
            ddlEstadoSituacionalTab2.DataBind();

            ddlEstadoSituacionalTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlEstadoSituacionalAdicionalTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoSituacional();
            ddlEstadoSituacionalAdicionalTab2.DataTextField = "nombre";
            ddlEstadoSituacionalAdicionalTab2.DataValueField = "valor";
            ddlEstadoSituacionalAdicionalTab2.DataBind();

            ddlEstadoSituacionalAdicionalTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        //protected void CargaSubTipoAdjudicacionTab2(string tipo)
        //{
        //    if (tipo.Equals(""))
        //    {
        //        ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        //    }
        //    else
        //    {
        //        ddlSubEstadoTab2.DataSource = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(tipo));
        //        ddlSubEstadoTab2.DataTextField = "nombre";
        //        ddlSubEstadoTab2.DataValueField = "valor";
        //        ddlSubEstadoTab2.DataBind();

        //        ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        //    }
        //}

        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected Boolean ValidarEjecucionTab2()
        {
            Boolean result;
            result = true;

            if (txtPlazoEjecucionTab2.Text == "")
            {
                string script = "<script>alert('Ingrese plazo de ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaEntregaTerrenoTab2.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaEntregaTerrenoTab2.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de entrega de terreno no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de entrega de terreno no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaFirmaContratoTab1.Text.Length > 0 && Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text) < Convert.ToDateTime(txtFechaFirmaContratoTab1.Text))
                {
                    string script = "<script>alert('La fecha de entrega de terreno debe ser mayor o igual a la fecha de firma de contrato.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaInicioTab2.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaInicioTab2.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de inicio de terreno no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaInicioTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de inicio no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaInicioTab2.Text) < Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text))
                {
                    string script = "<script>alert('La fecha de inicio debe ser mayor o igual a la fecha de entrega de terreno.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaTerminoTab2.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaTerminoTab2.Text) == false)
                {
                    string script = "<script>alert('Formato de fecha de termino real no valido.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaTerminoTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de termino real no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaInicioTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaTerminoTab2.Text) <= Convert.ToDateTime(txtFechaInicioTab2.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser mayor a la fecha de inicio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaRecepcionTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaTerminoTab2.Text) > Convert.ToDateTime(txtFechaRecepcionTab2.Text))
                {
                    string script = "<script>alert('La fecha de termino real debe ser menor o igual a la fecha de recepción.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaRecepcionTab2.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaRecepcionTab2.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de acta de recepción final.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (Convert.ToDateTime(txtFechaRecepcionTab2.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La fecha de acta de recepción final no puede ser mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaRecepcionTab2.Text) > Convert.ToDateTime(txtFechaTab3.Text))
                {
                    string script = "<script>alert('La fecha de acta de recepción final debe ser menor o igual a la fecha de resolución de liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }
            return result;
        }

        protected void CargaDatosTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                //txtCordinadorTab2.Text = dt.Rows[0]["coordinadorMVCS"].ToString();

                lblNombreEstadoGeneral.Text = dt.Rows[0]["vEstadoEjecucion"].ToString().ToUpper();

                lblIdEstado.Text = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();
                lblIdSubEstado.Text = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
                lblIdSubEstado2.Text = dt.Rows[0]["id_TipoEstadoParalizado"].ToString();

                //ddlEstadoTab2.SelectedValue = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();

                //if (ddlEstadoTab2.SelectedValue == "5" || ddlEstadoTab2.SelectedValue == "6" || ddlEstadoTab2.SelectedValue == "11")
                //{
                //CargaSubTipoAdjudicacionTab2(ddlEstadoTab2.SelectedValue);
                //if (dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString() != "")
                //{
                //    ddlSubEstadoTab2.SelectedValue = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
                //}
                //}


                txtFechaInicioTab2.Text = dt.Rows[0]["fechaInicio"].ToString();
                txtPlazoEjecucionTab2.Text = dt.Rows[0]["plazoEjecucion"].ToString();
                txtFechaInicioContractualTab2.Text = dt.Rows[0]["fechaInicioContractual"].ToString();
                txtFechaInicioReal.Text = dt.Rows[0]["fechaInicioReal"].ToString();

                txtFechaFinContractual.Text = dt.Rows[0]["fechaFinContractual"].ToString();
                txtFechaTerminoReal.Text = dt.Rows[0]["fechaFinReal"].ToString();
                txtFechaTerminoTab2.Text = dt.Rows[0]["fechaFin"].ToString();
                txtFechaTerminoProbableTab2.Text = dt.Rows[0]["fechaFinProbable"].ToString();

                //if (txtFechaTerminoTab2.Text.Length > 0)
                //{
                //    lblFecha.Visible = true;
                //    txtFechaTerminoTab2.Visible = true;
                //    //imgbtnMostrarFecha.Visible = false;

                //}

                txtFechaEntregaTerrenoTab2.Text = dt.Rows[0]["FechaEntregaTerreno"].ToString();
                txtFechaRecepcionTab2.Text = dt.Rows[0]["FechaRecepcion"].ToString();
                txtCordinadorTab2.Text = dt.Rows[0]["coordinadorMVCS"].ToString();
                TxtSuperTab2.Text = dt.Rows[0]["SupervisorDesignado"].ToString();
                txtResidenteTab2.Text = dt.Rows[0]["ResidenteObra"].ToString();
                txtInspectorTab2.Text = dt.Rows[0]["Inspector"].ToString();
                txtCoordinadorUETab2.Text = dt.Rows[0]["coordinadorUE"].ToString();
                txtTelefonoUETab2.Text = dt.Rows[0]["telefonoUE"].ToString();
                txtCorreoUETab2.Text = dt.Rows[0]["correoUE"].ToString();
                txtTelefonoCoordinadorUETab2.Text = dt.Rows[0]["telefonocoordinador"].ToString();
                txtCorreoCoordinadorUETab2.Text = dt.Rows[0]["correocoordinador"].ToString();

                // txtFechaTR.Text = dt.Rows[0]["fechafinalR"].ToString();
                //txtMontoAdelantoDirecTab2.Text = dt.Rows[0]["MontoAdelantoDirecto"].ToString();
                //txtMontoAdelantoMatTab2.Text = dt.Rows[0]["MontoAdelantoMaterial"].ToString();
                //txtFechaAdelantoDirecTab2.Text = dt.Rows[0]["fechaAdelantoDirecto"].ToString();
                //txtFechaAdelantoMatTab2.Text = dt.Rows[0]["fechaAdelantoMaterial"].ToString();

                lnkbtnFichaRecepcionTab2.Text = dt.Rows[0]["urlFichaRecepcion"].ToString();
                GeneraIcoFile(lnkbtnFichaRecepcionTab2.Text, imgbtnFichaRecepcionTab2);

                lblNombActuaTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                if (dt.Rows[0]["usuarioUpdateEstado"].ToString().Length > 0)
                {
                    lblFechaEstadoTab2.Text = "(Actualizó: " + dt.Rows[0]["usuarioUpdateEstado"].ToString() + " - " + dt.Rows[0]["fechaUpdateEstado"].ToString() + ")";
                }
                ActualizaFechasTab1();
                txtPoblacionBeneficiariaTab2.Text = dt.Rows[0]["poblacionDirecta"].ToString();
            }
            else
            {
                lblNombreEstadoGeneral.Text = "Actos Previos - Por Convoca (Falta registrar estado)";
            }
        }

        protected void ActualizaFechasTab1()
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);

                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);

                int diasParalizados;
                diasParalizados = Convert.ToInt32(txtTotalParalizacionTab4.Text);
                fecha = fecha.AddDays(diasParalizados);
                txtFechaTerminoRealFinal.Text = (fecha.ToString()).Substring(0, 10);

            }
        }
        protected void FechaTermino_OnTextChanged(object sender, EventArgs e)
        {
            if (txtFechaInicioTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);
                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }

            if (txtFechaInicioTab2.Text == "")
            {
                txtFechaFinContractual.Text = "";
                txtFechaTerminoReal.Text = "";

            }
        }
        protected void btnGuardarInfoTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarEjecucionTab2())
            {
                if (validaArchivo(FileUploadFichaRecepcionTab2))
                {

                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;

                    _BEEjecucion.coordinadorMVCS = txtCordinadorTab2.Text;
                    //_BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                    //_BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;


                    _BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaInicioTab2.Text);
                    _BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                    _BEEjecucion.Date_fechaInicioContractual = VerificaFecha(txtFechaInicioContractualTab2.Text);
                    _BEEjecucion.Date_fechaInicioReal = VerificaFecha(txtFechaInicioReal.Text);
                    _BEEjecucion.Date_fechaFinContractual = VerificaFecha(txtFechaFinContractual.Text);
                    _BEEjecucion.Date_fechaFinReal = VerificaFecha(txtFechaTerminoReal.Text);
                    _BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaTerminoTab2.Text);
                    _BEEjecucion.Date_fechaFinProbable = VerificaFecha(txtFechaTerminoProbableTab2.Text);

                    _BEEjecucion.Date_fechaEntregaTerreno = VerificaFecha(txtFechaEntregaTerrenoTab2.Text);
                    _BEEjecucion.Date_fechaRecepcion = VerificaFecha(txtFechaRecepcionTab2.Text);
                    if (txtPoblacionBeneficiariaTab2.Text == "")
                    {
                        txtPoblacionBeneficiariaTab2.Text = "0";
                    }
                    _BEEjecucion.poblacion = Convert.ToInt32(txtPoblacionBeneficiariaTab2.Text);
                    _BEEjecucion.supervisorDesignado = TxtSuperTab2.Text;
                    _BEEjecucion.residenteObra = txtResidenteTab2.Text;
                    _BEEjecucion.inspector = txtInspectorTab2.Text;
                    _BEEjecucion.coordinadorUE = txtCoordinadorUETab2.Text;
                    _BEEjecucion.telefonoUE = txtTelefonoUETab2.Text;
                    _BEEjecucion.correoUE = txtCorreoUETab2.Text;
                    _BEEjecucion.telefonocoordinador = txtTelefonoCoordinadorUETab2.Text;
                    _BEEjecucion.correocoordinador = txtCorreoCoordinadorUETab2.Text;

                    //_BEEjecucion.montoAdelantoDirecto = txtMontoAdelantoDirecTab2.Text;
                    //_BEEjecucion.montoAdelantoMaterial = txtMontoAdelantoMatTab2.Text;
                    //_BEEjecucion.Date_fechaAdelantoDirecto = VerificaFecha(txtFechaAdelantoDirecTab2.Text);
                    //_BEEjecucion.Date_fechaAdelantoMaterial = VerificaFecha(txtFechaAdelantoMatTab2.Text);

                    if (FileUploadFichaRecepcionTab2.HasFile)
                    {
                        _BEEjecucion.urlFichaRecepcion = _Metodo.uploadfile(FileUploadFichaRecepcionTab2);
                    }
                    _BEEjecucion.urlFichaRecepcion = FileUploadFichaRecepcionTab2.HasFile ? _Metodo.uploadfile(FileUploadFichaRecepcionTab2) : lnkbtnFichaRecepcionTab2.Text;

                    // _BEEjecucion.urlFichaRecepcion = _Metodo.uploadfile(FileUploadFichaRecepcionTab2);
                    //_BEEjecucion.tipoPrioridad = ddlTipoPrioridadTab2.SelectedValue;
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.spi_MON_EstadoEjecuccionTransferencia(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente el estado de ejecución.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab2();

                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }

        }

        protected void grdAvanceFisicoTab2_onRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);


            }
        }

        protected void grdAvanceFisicoAdicionalTab2_onRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceAdicionalTab2");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected Boolean ValidarAvanceTab2()
        {
            Boolean result;
            result = true;



            if (ddlValorizacionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo Valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToInt32(ddlValorizacionTab2.SelectedValue) > 1)
            {
                if (txtNroAdicionalTab2.Text == "" || txtNroAdicionalTab2.Text == "0")
                {
                    string script = "<script>alert('Ingrese N° Adicional de Valorización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtfechaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtfechaTab2.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {

                if (Convert.ToDateTime(txtfechaTab2.Text) > DateTime.Today)
                {
                    string script = "<script>alert('La fecha de valorización ingresada es mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

            }

            if (txtMontoTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ValidaDecimal(txtMontoTab2.Text) == false)
            {
                string script = "<script>alert('Ingrese monto(S/.) valido, por ejemplo: 10000.02 ');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiReal.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (Convert.ToDouble(txtFisiReal.Text) > 100)
                {
                    string script = "<script>alert('El avance físico real no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }


            if (txtFisiProgTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (Convert.ToDouble(txtFisiProgTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance físico programado no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFinanRealTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (Convert.ToDouble(txtFinanRealTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance financiero real no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFinanProgTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {
                if (Convert.ToDouble(txtFinanProgTab2.Text) > 100)
                {
                    string script = "<script>alert('El avance financiero programado no puede ser mayor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (Convert.ToDouble(txtFisiReal.Text) > 0 && Convert.ToDouble(txtFinanRealTab2.Text) == 0)
            {
                string script = "<script>alert('Debe ingresar el avance financiero real cuando el avance físico real es mayor a 0.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToDouble(txtFisiProgTab2.Text) > 0 && Convert.ToDouble(txtFinanProgTab2.Text) == 0)
            {
                string script = "<script>alert('Debe ingresar el avance financiero programado cuando el avance físico programado es mayor a 0.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlEstadoSituacionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese estado situacional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (btnGuardarAvanceTab2.Visible == true && ddlValorizacionTab2.SelectedValue != "2") //No corresponde en Val. Adicionales la validación
            {
                if (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico real acumulado es: " + (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico programado acumulado es: " + (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero real acumulado es: " + (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero programado acumulado es: " + (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }

            if (btnModificarAvanceTab2.Visible == true)
            {
                if (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisRealPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico real acumulado es: " + (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisRealPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisProgPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance físico programado acumulado es: " + (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisProgPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero real acumulado es: " + (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgPreAcumuladoTab2.Text) > 101)
                {
                    string script = "<script>alert('El avance financiero programado acumulado es: " + (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }

            return result;
        }

        protected Boolean ValidarAvanceAdicionalTab2()
        {
            Boolean result;
            result = true;



            if (ddlValorizacionAdicionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo Valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToInt32(ddlValorizacionAdicionalTab2.SelectedValue) > 1)
            {
                if (txtNroAdicionalAdicionalTab2.Text == "" || txtNroAdicionalAdicionalTab2.Text == "0")
                {
                    string script = "<script>alert('Ingrese N° Adicional de Valorización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtfechaAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (_Metodo.ValidaFecha(txtfechaAdicionalTab2.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            else
            {

                if (Convert.ToDateTime(txtfechaAdicionalTab2.Text) > DateTime.Today)
                {
                    string script = "<script>alert('La fecha de valorización ingresada es mayor a la fecha de hoy.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

            }

            if (txtMontoAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ValidaDecimal(txtMontoAdicionalTab2.Text) == false)
            {
                string script = "<script>alert('Ingrese monto(S/.) valido, por ejemplo: 10000.02 ');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiRealAdicional.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiProgAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFinanRealAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFinanProgAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (ddlEstadoSituacionalAdicionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese estado situacional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }

        protected void btnGuardarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;

                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaTab2.Text);
                    _BEEjecucion.monto = txtMontoTab2.Text;
                    _BEEjecucion.fisicoReal = txtFisiReal.Text;
                    _BEEjecucion.fisicoProgramado = txtFisiProgTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalTab2.SelectedValue);
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEEjecucion.observacion = txtObservaciontab2.Text;
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionTab2.SelectedValue;
                    _BEEjecucion.NroAdicional = txtNroAdicionalTab2.Text;

                    int val = _objBLEjecucion.spi_MON_AvanceFisico(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoTab2.Text = "";
                        txtfechaTab2.Text = "";
                        txtFisiReal.Text = "";
                        txtFisiProgTab2.Text = "";
                        txtFinanRealTab2.Text = "";
                        txtFinanProgTab2.Text = "";
                        ddlEstadoSituacionalTab2.SelectedValue = "";
                        txtObservaciontab2.Text = "";
                        ddlValorizacionTab2.SelectedValue = "";
                        txtNroAdicionalTab2.Text = "";

                        Panel_AgregarAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }


        protected void btnGuardarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceAdicionalTab2())
            {
                if (validaArchivo(FileUploadAvanceAdicionalTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;

                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaAdicionalTab2.Text);
                    _BEEjecucion.monto = txtMontoAdicionalTab2.Text;
                    _BEEjecucion.fisicoReal = txtFisiRealAdicional.Text;
                    _BEEjecucion.fisicoProgramado = txtFisiProgAdicionalTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealAdicionalTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgAdicionalTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalAdicionalTab2.SelectedValue);
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEEjecucion.observacion = txtObservacionAdicionaltab2.Text;
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionAdicionalTab2.SelectedValue;
                    _BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;

                    int val = _objBLEjecucion.spi_MON_AvanceFisico(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoAdicionalTab2.Text = "";
                        txtfechaAdicionalTab2.Text = "";
                        txtFisiRealAdicional.Text = "";
                        txtFisiProgAdicionalTab2.Text = "";
                        txtFinanRealAdicionalTab2.Text = "";
                        txtFinanProgAdicionalTab2.Text = "";
                        ddlEstadoSituacionalAdicionalTab2.SelectedValue = "";
                        txtObservacionAdicionaltab2.Text = "";
                        ddlValorizacionAdicionalTab2.SelectedValue = "";
                        txtNroAdicionalAdicionalTab2.Text = "";

                        Panel_AgregarAvanceAdicionalTab2.Visible = false;
                        imgbtnAvanceFisicoAdicionalTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        protected Boolean ValidarFianzaTab2()
        {
            Boolean result;
            result = true;

            if (txtEntidadFinancieraTab2.Text == "")
            {
                string script = "<script>alert('Ingrese Entidad Financiera.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtNroCartaF.Text == "")
            {
                string script = "<script>alert('Ingrese N° Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (ddlTipoTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFechaInicioVigenciaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio de vigencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFechaFinVigenciaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de fin de vigencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtmontoFianzaTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtPorcentajeTab2.Text == "")
            {
                string script = "<script>alert('Ingrese % de cubrimiento de Carta Fianza.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtPorcentajeTab2.Text != "")
            {
                if (ddlTipoTab2.SelectedValue == "1")
                {
                    if (Convert.ToDouble(txtPorcentajeTab2.Text) > 10.00)
                    {
                        string script = "<script>alert('El % de cubrimiento debe ser menor a 10%');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }

                if (ddlTipoTab2.SelectedValue == "2")
                {
                    if (Convert.ToDouble(txtPorcentajeTab2.Text) > 20.00)
                    {
                        string script = "<script>alert('El % de cubrimiento debe ser menor a 20%');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }

                if (ddlTipoTab2.SelectedValue == "3")
                {
                    if (Convert.ToDouble(txtPorcentajeTab2.Text) > 40.00)
                    {
                        string script = "<script>alert('El % de cubrimiento debe ser menor a 40%');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }

            }

            if (ddlTipoAccion.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de acción.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else if (ddlTipoAccion.SelectedValue.Equals("2") && txtFechaRenovacion.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de renovación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }

        protected void btnCancelarFianzasTab2_OnClick(object sender, EventArgs e)
        {
            Panel_fianzas.Visible = false;
            imgbtnAgregarFianzasTab4.Visible = true;
            Up_Tab2.Update();
        }

        protected void ddlValorizacionTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlValorizacionTab2.SelectedValue == "1" || ddlValorizacionTab2.SelectedValue == "")
            {
                lblAdicional.Visible = false;
                txtNroAdicionalTab2.Visible = false;

            }
            else
            {
                lblAdicional.Visible = true;
                txtNroAdicionalTab2.Visible = true;
            }

            Up_lblAdicionalTab2.Update();
            Up_txtAdicionalTab2.Update();
        }

        protected void btnGuardarFianzasTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarFianzaTab2())
            {
                if (validaArchivo(FileUploadCartaTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                    _BEEjecucion.entidadFinanciera = txtEntidadFinancieraTab2.Text;
                    _BEEjecucion.nroCarta = txtNroCartaF.Text;
                    _BEEjecucion.tipoCarta = Convert.ToInt32(ddlTipoTab2.SelectedValue);

                    _BEEjecucion.Date_fechaInicio = Convert.ToDateTime(txtFechaInicioVigenciaTab2.Text);
                    _BEEjecucion.Date_fechaFin = Convert.ToDateTime(txtFechaFinVigenciaTab2.Text);
                    _BEEjecucion.Date_fechaRenovacion = Convert.ToDateTime(VerificaFecha(txtFechaRenovacion.Text));
                    _BEEjecucion.monto = txtmontoFianzaTab2.Text;


                    _BEEjecucion.porcentaje = txtPorcentajeTab2.Text;
                    if (chkVerificado.Checked == true)
                    {
                        _BEEjecucion.flagVerificado = "1";
                    }

                    _BEEjecucion.idTipoAccion = ddlTipoAccion.SelectedValue;

                    _BEEjecucion.urlDocAccion = _Metodo.uploadfile(FileUploadAccionTab2);
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.spi_MON_CartaFianza(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaCartasFianzasTab2();

                        txtEntidadFinancieraTab2.Text = "";
                        txtNroCartaF.Text = "";
                        ddlTipoTab2.SelectedValue = "";
                        txtFechaInicioVigenciaTab2.Text = "";
                        txtFechaFinVigenciaTab2.Text = "";
                        txtmontoFianzaTab2.Text = "";
                        txtPorcentajeTab2.Text = "";

                        Panel_fianzas.Visible = false;
                        imgbtnAgregarFianzasTab4.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        //protected void totalMontoGrdTab2(int tipo, Label txt)
        //{
        //    _BEFinanciamiento.tipoGrd = tipo;
        //    _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    txt.Text = _objBLFinanciamiento.F_spMON_MontoTotalGrd(_BEFinanciamiento);
        //}

        protected void btnGuardarResidenteTab2_OnClick(object sender, EventArgs e)
        {
            if (txtResidenteObraTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de residente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIPR.Text.Trim() == "" && txtCapR.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                //if (txtCIPR.Text == "")
                //{
                //    string script = "<script>alert('Ingresar CIP.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                //}
                //else
                //{
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 1;

                _BEEjecucion.empleadoObra = txtResidenteObraTab2.Text;
                _BEEjecucion.CIP = txtCIPR.Text;
                _BEEjecucion.telefono = txtTelefonoR.Text;
                _BEEjecucion.correo = txtCorreoR.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCapR.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaR.Text);

                int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtResidenteTab2.Text = txtResidenteObraTab2.Text;
                    txtResidenteObraTab2.Text = "";
                    txtTelefonoR.Text = "";
                    txtCorreoR.Text = "";
                    txtCIPR.Text = "";
                    txtCapR.Text = "";
                    txtFechaR.Text = "";


                    //RESIDENTE
                    dt = CargaEmpleadoObraTab2(1);
                    grd_HistorialResidenteTab2.DataSource = dt;
                    grd_HistorialResidenteTab2.DataBind();

                    Panel_HistorialResidenteTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
            //}





        }
        protected void btnGuardarCoordinador_OnClick(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de coordinador MVCS.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 4;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró coordinador MVCS correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //txtCordinadorTab2.Text = txtCoordinador.Text ;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoObraTab2(4);
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    Panel_Coordinador.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }


        protected void btnGuardarSupervisorTab2_OnClick(object sender, EventArgs e)
        {
            if (txtSupervisorS.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIPS.Text.Trim() == "" && txtCapS.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 3;

                _BEEjecucion.empleadoObra = txtSupervisorS.Text;
                _BEEjecucion.CIP = txtCIPS.Text;
                _BEEjecucion.telefono = txtTelefonoS.Text;
                _BEEjecucion.correo = txtCorreoS.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCapS.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaS.Text);
                int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró supervisor correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtSupervisorS.Text = "";
                    txtTelefonoS.Text = "";
                    txtCIPS.Text = "";
                    txtCorreoS.Text = "";
                    txtCapS.Text = "";
                    txtFechaS.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);

                    //SUPERVISOR
                    dt = CargaEmpleadoObraTab2(3);
                    grd_HistorialSupervisorTab2.DataSource = dt;
                    grd_HistorialSupervisorTab2.DataBind();

                    Panel_HistorialSupervisorTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }





        }

        protected void btnGrabarInspectorTab2_OnClick(object sender, EventArgs e)
        {
            if (txtInspector.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de inspector.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIP.Text.Trim() == "" && txtCap.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                //if (txtCIP.Text == "")
                //{
                //    string script = "<script>alert('Ingresar CIP.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                //}
                //else
                //{
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoempleado = 2;

                _BEEjecucion.empleadoObra = txtInspector.Text;
                _BEEjecucion.telefono = txtTelefono.Text;
                _BEEjecucion.correo = txtCorreo.Text;
                _BEEjecucion.CIP = txtCIP.Text;
                _BEEjecucion.cap = txtCap.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaP.Text);

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtInspectorTab2.Text = txtInspector.Text;
                    txtInspector.Text = "";
                    txtTelefono.Text = "";
                    txtCorreo.Text = "";
                    txtCIP.Text = "";
                    txtCap.Text = "";
                    txtFechaP.Text = "";

                    //INSPECTOR
                    dt = CargaEmpleadoObraTab2(2);
                    grd_HistorialInspectorTab2.DataSource = dt;
                    grd_HistorialInspectorTab2.DataBind();

                    Panel_HistorialInspectorTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnModificarAvanceTab2_OnClick(object sender, EventArgs e)
        {

            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {

                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_Avance.Text);
                    //_BEFinanciamiento.tipoFinanciamiento = 3;
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionTab2.SelectedValue;
                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaTab2.Text);
                    _BEEjecucion.monto = txtMontoTab2.Text;
                    _BEEjecucion.fisicoReal = txtFisiReal.Text;
                    _BEEjecucion.fisicoProgramado = txtFisiProgTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgTab2.Text;
                    _BEEjecucion.observacion = txtObservaciontab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalTab2.SelectedValue);

                    if (FileUploadAvanceTab2.PostedFile.ContentLength > 0)
                    {
                        _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDoc = LnkbtnAvanceTab2.Text;
                    }


                    //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    _BEEjecucion.NroAdicional = txtNroAdicionalTab2.Text;
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int resul;
                    resul = _objBLEjecucion.spud_MON_AvanceFisico_Editar(_BEEjecucion);
                    if (resul == 1)
                    {
                        string script = "<script>alert('Registro Correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        ddlValorizacionTab2.SelectedValue = "";
                        txtfechaTab2.Text = "";
                        txtMontoTab2.Text = "";
                        txtFisiReal.Text = "";
                        txtFisiProgTab2.Text = "";
                        txtFinanRealTab2.Text = "";
                        txtFinanProgTab2.Text = "";
                        txtObservaciontab2.Text = "";
                        ddlEstadoSituacionalTab2.SelectedValue = "";
                        txtNroAdicionalTab2.Text = "";
                        LnkbtnAvanceTab2.Text = "";
                        imgbtnAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;

                        CargaAvanceFisico();



                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                    Panel_AgregarAvanceTab2.Visible = false;
                    CargaAvanceFisico();
                    btnModificarAvanceTab2.Visible = true;
                    Up_Tab2.Update();
                }

            }
        }

        protected void btnModificarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {

            if (ValidarAvanceAdicionalTab2())
            {


                _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_AvanceAdicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEEjecucion.id_tipoValorizacion = ddlValorizacionAdicionalTab2.SelectedValue;
                _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaAdicionalTab2.Text);
                _BEEjecucion.monto = txtMontoAdicionalTab2.Text;
                _BEEjecucion.fisicoReal = txtFisiRealAdicional.Text;
                _BEEjecucion.fisicoProgramado = txtFisiProgAdicionalTab2.Text;
                _BEEjecucion.financieroReal = txtFinanRealAdicionalTab2.Text;
                _BEEjecucion.financieroProgramado = txtFinanProgAdicionalTab2.Text;
                _BEEjecucion.observacion = txtObservacionAdicionaltab2.Text;
                _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalAdicionalTab2.SelectedValue);

                if (FileUploadAvanceAdicionalTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = LnkbtnAvanceAdicionalTab2.Text;
                }


                //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                _BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLEjecucion.spud_MON_AvanceFisico_Editar(_BEEjecucion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // ddlValorizacionTab2.SelectedValue = "";
                    txtfechaAdicionalTab2.Text = "";
                    txtMontoAdicionalTab2.Text = "";
                    txtFisiRealAdicional.Text = "";
                    txtFisiProgAdicionalTab2.Text = "";
                    txtFinanRealAdicionalTab2.Text = "";
                    txtFinanProgAdicionalTab2.Text = "";
                    txtObservacionAdicionaltab2.Text = "";
                    ddlEstadoSituacionalAdicionalTab2.SelectedValue = "";
                    txtNroAdicionalAdicionalTab2.Text = "";
                    LnkbtnAvanceAdicionalTab2.Text = "";
                    imgbtnAvanceAdicionalTab2.Visible = false;
                    imgbtnAvanceFisicoAdicionalTab2.Visible = true;

                    CargaAvanceFisico();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarAvanceAdicionalTab2.Visible = false;
                //CargaAvanceFisico();
                btnModificarAvanceAdicionalTab2.Visible = true;
                Up_Tab2.Update();


            }
        }

        protected void btnModificarFianzasTab2_OnClick(object sender, EventArgs e)
        {

            if (ValidarFianzaTab2())
            {
                _BEEjecucion.id_carta_fianza = Convert.ToInt32(lblId_Registro_Carta_Fianza.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEEjecucion.entidadFinanciera = txtEntidadFinancieraTab2.Text;
                _BEEjecucion.nroCarta = txtNroCartaF.Text;
                _BEEjecucion.tipoCarta = Convert.ToInt32(ddlTipoTab2.SelectedValue);
                _BEEjecucion.Date_fechaInicio = Convert.ToDateTime(txtFechaInicioVigenciaTab2.Text);
                _BEEjecucion.Date_fechaFin = Convert.ToDateTime(txtFechaFinVigenciaTab2.Text);
                _BEEjecucion.Date_fechaRenovacion = Convert.ToDateTime(VerificaFecha(txtFechaRenovacion.Text));
                _BEEjecucion.monto = txtmontoFianzaTab2.Text;
                _BEEjecucion.porcentaje = txtPorcentajeTab2.Text;

                _BEEjecucion.idTipoAccion = ddlTipoAccion.SelectedValue;
                if (chkVerificado.Checked == true)
                {
                    _BEEjecucion.flagVerificado = "1";
                }

                if (FileUploadCartaTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = LnkbtnCartaTab2.Text;
                }

                if (FileUploadAccionTab2.Visible == true && FileUploadAccionTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDocAccion = _Metodo.uploadfile(FileUploadAccionTab2);
                }
                else
                {
                    _BEEjecucion.urlDocAccion = lnkbtnAccionTab2.Text;
                }
                //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCartaTab2);


                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLEjecucion.spud_MON_Seguimiento_Carta_Fianza_Editar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaCartasFianzasTab2();

                    txtEntidadFinancieraTab2.Text = "";
                    txtNroCartaF.Text = "";
                    ddlTipoTab2.SelectedValue = "";
                    txtFechaInicioVigenciaTab2.Text = "";
                    txtFechaFinVigenciaTab2.Text = "";
                    txtmontoFianzaTab2.Text = "";
                    txtPorcentajeTab2.Text = "";
                    LnkbtnCartaTab2.Text = "";
                    imgbtnCartaTab2.Visible = false;
                    imgbtnAgregarFianzasTab4.Visible = true;
                    Panel_fianzas.Visible = false;
                    chkVerificado.Checked = false;
                    //chkRenovado.Checked = false;
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_fianzas.Visible = false;
                    cargaCartasFianzasTab2();
                    //btnModificarFianzasTab2.Visible = true;

                    Up_Tab2.Update();

                }
            }
        }

        protected void imgbtnAgregarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            //Nuevo registro
            lblID_evaluacionTab2.Text = "0";

            string flagContinuarRegistro = "1";
            string msjContinuarRegistro = "";

            if (lblIdEstado.Text.Equals("5")) //PARALIZADO  VERIFICAR REGISTRO DE FECHA DE REINICIO
            {

                dt = new DataTable();
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);


                if (dt.Rows.Count > 0)
                {
                    string IdEstado = (dt.Rows[dt.Rows.Count - 1]["id_TipoEstadoEjecuccion"]).ToString();

                    if (lblIdSubEstado.Text.Equals("82")) //PERMANENTE
                    {
                        string fechaFinanciamiento = (dt.Rows[dt.Rows.Count - 1]["fechaFinanciamientoExpTecnico"]).ToString();
                        string fechaReinicio = (dt.Rows[dt.Rows.Count - 1]["fechaReinicio"]).ToString();
                        if (IdEstado.Equals("5") && fechaFinanciamiento.Length <= 1 && fechaReinicio.Length <= 1)
                        {
                            flagContinuarRegistro = "0";
                            msjContinuarRegistro = "Para continuar con el registro del Avance, debe registrar la fecha de financiamiento del Expediente de Saldo de Obra y/o fecha de reinicio de la Obra.";
                        }
                    }
                    else // TEMPORAL
                    {
                        string fechaReinicio = (dt.Rows[dt.Rows.Count - 1]["fechaReinicio"]).ToString();
                        if (IdEstado.Equals("5") && fechaReinicio.Length <= 1)
                        {
                            flagContinuarRegistro = "0";
                            msjContinuarRegistro = "Para continuar con el registro del Avance, debe registrar la fecha de reinicio de la Obra.";
                        }
                    }
                }

            }

            //if (lblIdSubEstado.Text.Equals("87")) //SUSPENSION  VERIFICAR REGISTRO DE FECHA DE REINICIO
            //{
            //    dt = new DataTable();
            //    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            //    dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);

            //    if (dt.Rows.Count > 0)
            //    {
            //        string vIdSubEstado = (dt.Rows[dt.Rows.Count - 1]["id_tipoSubEstadoEjecucion"]).ToString();
            //        string fechaReinicio = (dt.Rows[dt.Rows.Count - 1]["fechaReinicio"]).ToString();
            //        if (vIdSubEstado.Equals("87") && fechaReinicio.Length <= 1)
            //        {
            //            flagContinuarRegistro = "0";
            //            msjContinuarRegistro = "Para continuar con el registro del Avance, debe registrar la fecha de reinicio de la Obra.";
            //        }
            //    }
            //}
            if (flagContinuarRegistro.Equals("0"))
            {
                string script = "<script>alert('" + msjContinuarRegistro + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                Panel_ObservacionTab2.Visible = true;
                imgbtnAgregarEvaluacionTab2.Visible = false;
                lblNomUsuarioObservaciones.Text = "";
                btnGuardarEvaluacionTab2.Visible = true;
                btnModificarEvaluacionTab2.Visible = false;

                txtFechaVisitaTab2.Text = "";
                txtEvaluacionTab2.Text = "";
                txtRecomendacionTab2.Text = "";
                txtProgramadoTab2.Text = "";
                txtEjecutadoTab2.Text = "";
                ddlTipoMonitoreo.SelectedValue = "";
                txtNroTramie.Text = "";
                txtTipoDocumento.Text = "";
                ddlTipoDocumento.SelectedValue = "";
                txtAsunto.Text = "";
                txtProgramadoTab2.Text = "0";
                txtEjecutadoTab2.Text = "0";
                txtFinanEjecutadoTab2.Text = "0";
                txtFinanProgramadoTab2.Text = "0";

                lblMsjCambioEstado.Text = "";
                lblNomUsuarioFlagAyudaTab4.Text = "";

                imgbtnActaTab4.ImageUrl = "~/img/blanco.png";
                imgbtnInformeTab4.ImageUrl = "~/img/blanco.png";
                imgbtnOficioTab4.ImageUrl = "~/img/blanco.png";

                LnkbtnInformeTab4.Text = "";
                LnkbtnOficioTab4.Text = "";
                LnkbtnActaTab4.Text = "";

                chbAyudaMemoriaTab4.Checked = true;
                chbAyudaMemoriaTab4.Enabled = true;

                ddlTipoEstadoInformeTab2.Enabled = true;
                ddlTipoSubEstadoInformeTab2.Enabled = true;
                ddlTipoSubEstado2InformeTab2.Enabled = true;

                CargaTipoEstadosDetalle();
                CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);

                Panel_InformacionComplementaria.Visible = false;
                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
                Up_FechasSuspensionTab2.Update();

                chbProblematicaTab2.Items.Clear();
                txtFechaInicioSuspensionTab2.Text = "";
                txtFechaFinalReinicioTab2.Text = "";

                if (lblIdEstado.Text.Equals("5") && lblCOD_SUBSECTOR.Text.Equals("1"))
                {
                    if (!(LblID_USUARIO.Text.Equals("792") || LblID_USUARIO.Text.Equals("481")))
                    {

                        lblMsjCambioEstado.Text = "<div>" +
                                "<p class='bg-danger paddding-lg text-danger'><b>NOTA:</b> </br></br>";

                        lblMsjCambioEstado.Text = lblMsjCambioEstado.Text + "Para registrar el cambio de estado de una OBRA PARALIZADA a otro ESTADO, se debe enviar el informe de derivación a la Coordinación de Estudios y Proyectos." +
                            "</br> </br> Posteriormente la persona autorizada de la Coordinación de Asistencia Técnica realizará el cambio de estado." +
                            "</p></div>";
                        Up_MsjEstado.Update();

                        ddlTipoEstadoInformeTab2.Enabled = false;
                        ddlTipoSubEstadoInformeTab2.Enabled = false;
                        ddlTipoSubEstado2InformeTab2.Enabled = false;

                        ddlTipoSubEstado2InformeTab2.Visible = true;
                        lblNombreSubEstado2.Visible = true;
                        Up_SubEstado2NombreTab2.Update();
                        Up_SubEstado2DetalleTab2.Update();

                        ddlTipoEstadoInformeTab2.SelectedValue = lblIdEstado.Text; //ESTADO
                        CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);
                        ddlTipoSubEstadoInformeTab2.SelectedValue = lblIdSubEstado.Text; // SUBESTADO
                        ddlTipoSubEstado2InformeTab2.SelectedValue = lblIdSubEstado2.Text; // SUBESTADO2

                    }

                }

                Up_Tab2.Update();
            }
        }
        protected void grd_HistorialCoordinador_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialCoordinador.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;//PARA LA ELIMINACION
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;

                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    dt = CargaEmpleadoObraTab2(4);
                    if (dt.Rows.Count > 0)
                    {
                        txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCordinadorTab2.Text = "";
                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();

                    Panel_Coordinador.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grd_HistorialSupervisorTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialSupervisorTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = DateTime.Now;
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtSupervisorS.Text = "";
                    txtTelefonoS.Text = "";
                    txtCIPS.Text = "";
                    txtCorreoS.Text = "";
                    dt = CargaEmpleadoObraTab2(3);
                    if (dt.Rows.Count > 0)
                    {

                        TxtSuperTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        TxtSuperTab2.Text = "";

                    }


                    Panel_HistorialSupervisorTab2.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialCoordinador_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialCoordinador.SelectedDataKey.Value.ToString();
            lblIdCoordinadorM.Text = id;
            GridViewRow row = grd_HistorialCoordinador.SelectedRow;

            txtCoordinador.Text = ((Label)row.FindControl("lblnombreS")).Text;
            txtFechaC.Text = ((Label)row.FindControl("lblFechaSS")).Text;

            btnGuardar_Coordinador.Visible = false;
            btnModificar_Coordinador.Visible = true;
            Panel_Coordinador.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }

        protected void grd_HistorialSupervisorTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialSupervisorTab2.SelectedDataKey.Value.ToString();
            lblIDSupervisor.Text = id;
            GridViewRow row = grd_HistorialSupervisorTab2.SelectedRow;

            txtSupervisorS.Text = ((Label)row.FindControl("lblnombreS")).Text;
            txtCIPS.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtTelefonoS.Text = ((Label)row.FindControl("lblTelefonoS")).Text;
            txtCorreoS.Text = ((Label)row.FindControl("lblCorreoS")).Text;
            txtCapS.Text = ((Label)row.FindControl("lblCapS")).Text;
            txtFechaS.Text = ((Label)row.FindControl("lblFechaSS")).Text;

            btnGuardarSupervisorTab2.Visible = false;
            btnModificarSupervisorTab2.Visible = true;
            Panel_HistorialSupervisorTab2.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }

        protected void btnModificarSupervisorTab2_OnClick(object sender, EventArgs e)
        {
            if (txtSupervisorS.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIPS.Text.Trim() == "" && txtCapS.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIDSupervisor.Text);
                _BEEjecucion.Tipo = 1;
                // _BEEjecucion.empleadoObra = txtu.Text;
                _BEEjecucion.empleadoObra = txtSupervisorS.Text;
                _BEEjecucion.CIP = txtCIPS.Text;
                _BEEjecucion.telefono = txtTelefonoS.Text;
                _BEEjecucion.correo = txtCorreoS.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCapS.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaS.Text);

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el supervisor correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtSupervisorS.Text = "";
                    txtTelefonoS.Text = "";
                    txtCIPS.Text = "";
                    txtCorreoS.Text = "";
                    txtCapS.Text = "";
                    txtFechaS.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(3);
                    if (dt.Rows.Count > 0)
                    {

                        TxtSuperTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        TxtSuperTab2.Text = "";

                    }

                    Panel_HistorialSupervisorTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void btnModificarCoordinador_OnClick(object sender, EventArgs e)
        {
            if (txtCoordinador.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIdCoordinadorM.Text);
                _BEEjecucion.Tipo = 1;//valor q indica la modificacion
                                      // _BEEjecucion.empleadoObra = txtu.Text;

                _BEEjecucion.empleadoObra = txtCoordinador.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el coordinador correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtCoordinador.Text = "";
                    txtFechaC.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(4);
                    if (dt.Rows.Count > 0)
                    {
                        txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
                    }
                    else
                    {
                        txtCordinadorTab2.Text = "";

                    }
                    grd_HistorialCoordinador.DataSource = dt;
                    grd_HistorialCoordinador.DataBind();
                    Panel_Coordinador.Visible = false;
                    btnModificar_Coordinador.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialResidenteTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialResidenteTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = VerificaFecha("");
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta de residente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtResidenteObraTab2.Text = "";
                    txtTelefonoR.Text = "";
                    txtCIPR.Text = "";
                    txtCorreoR.Text = "";
                    dt = CargaEmpleadoObraTab2(1);
                    if (dt.Rows.Count > 0)
                    {

                        txtResidenteTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtResidenteTab2.Text = "";

                    }


                    Panel_HistorialResidenteTab2.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialResidenteTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialResidenteTab2.SelectedDataKey.Value.ToString();
            lblId_Residente.Text = id;
            GridViewRow row = grd_HistorialResidenteTab2.SelectedRow;

            txtResidenteObraTab2.Text = ((Label)row.FindControl("lblResidente")).Text;
            txtCIPR.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtTelefonoR.Text = ((Label)row.FindControl("lblTelefonoR")).Text;
            txtCorreoR.Text = ((Label)row.FindControl("lblCorreoR")).Text;
            txtCapR.Text = ((Label)row.FindControl("lblcapR")).Text;
            txtFechaR.Text = ((Label)row.FindControl("lblFechaRR")).Text;

            btnGuardarResidenteTab2.Visible = false;
            btnModificarResidenteTab2.Visible = true;
            Panel_HistorialResidenteTab2.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }

        protected void btnModificarResidenteTab2_OnClick(object sender, EventArgs e)
        {
            if (txtResidenteObraTab2.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de residente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIPR.Text.Trim() == "" && txtCapR.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblId_Residente.Text);
                _BEEjecucion.Tipo = 1;
                // _BEEjecucion.empleadoObra = txtu.Text;
                _BEEjecucion.empleadoObra = txtResidenteObraTab2.Text;
                _BEEjecucion.CIP = txtCIPR.Text;
                _BEEjecucion.telefono = txtTelefonoR.Text;
                _BEEjecucion.correo = txtCorreoR.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCapR.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaR.Text);

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);


                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el residente correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtResidenteObraTab2.Text = "";
                    txtTelefonoR.Text = "";
                    txtCIPR.Text = "";
                    txtCorreoR.Text = "";
                    txtCapR.Text = "";
                    txtFechaR.Text = "";


                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(1);
                    grd_HistorialResidenteTab2.DataSource = dt;
                    grd_HistorialResidenteTab2.DataBind();

                    if (dt.Rows.Count > 0)
                    {

                        txtResidenteTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtResidenteTab2.Text = "";

                    }


                    Panel_HistorialResidenteTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialInspectorTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grd_HistorialInspectorTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = VerificaFecha("");
                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta de Inspector.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtInspector.Text = "";
                    txtTelefono.Text = "";
                    txtCIP.Text = "";
                    txtCorreo.Text = "";
                    dt = CargaEmpleadoObraTab2(2);
                    if (dt.Rows.Count > 0)
                    {

                        txtInspectorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtInspectorTab2.Text = "";

                    }


                    Panel_HistorialInspectorTab2.Visible = false;
                    Up_Tab2.Update();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grd_HistorialInspectorTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id;
            id = grd_HistorialInspectorTab2.SelectedDataKey.Value.ToString();
            lblID_Inspector.Text = id;
            GridViewRow row = grd_HistorialInspectorTab2.SelectedRow;

            txtInspector.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtCIP.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtTelefono.Text = ((Label)row.FindControl("lblTelefono")).Text;
            txtCorreo.Text = ((Label)row.FindControl("lblCorreo")).Text;
            txtCap.Text = ((Label)row.FindControl("lblCap")).Text;
            txtFechaP.Text = ((Label)row.FindControl("lblFechaII")).Text;

            btnGrabarInspectorTab2.Visible = false;
            btnModificarInspectorTab2.Visible = true;
            Panel_HistorialInspectorTab2.Visible = true;
            Up_Tab2.Update();
            //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            //btnModificarContrapartidasTab0.Visible = true;
            //btnGuardarContrapartidasTab0.Visible = false;
            //Panel_ContrapartidaTab0.Visible = true;
            //btnAgregarContrapartidasTab0.Visible = false;
            //Up_Tab0.Update();


        }

        protected void btnModificarInspectorTab2_OnClick(object sender, EventArgs e)
        {
            if (txtInspector.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de Inspector.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else if (txtCIP.Text.Trim() == "" && txtCap.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar CIP o CAP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblID_Inspector.Text);
                _BEEjecucion.Tipo = 1;
                // _BEEjecucion.empleadoObra = txtu.Text;
                _BEEjecucion.empleadoObra = txtInspector.Text;
                _BEEjecucion.CIP = txtCIP.Text;
                _BEEjecucion.telefono = txtTelefono.Text;
                _BEEjecucion.correo = txtCorreo.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.cap = txtCap.Text;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaP.Text);

                int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el Inspector correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //TxtSuperTab2.Text = txtSupervisorS.Text;
                    txtInspector.Text = "";
                    txtTelefono.Text = "";
                    txtCIP.Text = "";
                    txtCorreo.Text = "";
                    txtCap.Text = "";
                    txtFechaP.Text = "";

                    //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
                    dt = CargaEmpleadoObraTab2(2);
                    if (dt.Rows.Count > 0)
                    {

                        txtInspectorTab2.Text = dt.Rows[0]["nombre"].ToString();

                    }
                    else
                    {
                        txtInspectorTab2.Text = "";

                    }

                    Panel_HistorialInspectorTab2.Visible = false;
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void cargarEvaluacionTab2()
        {
            dt = new DataTable();
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);


            if (dt.Rows.Count > 0)
            {
                lblAvanceEjecutadoInfomeTab2.Text = (dt.Rows[dt.Rows.Count - 1]["fisicoEjecutado"]).ToString();
                lblAvanceProgramadoInfomeTab2.Text = (dt.Rows[dt.Rows.Count - 1]["fisicoProgramado"]).ToString();
                //string IdEstado = (dt.Rows[dt.Rows.Count - 1]["id_TipoEstadoEjecuccion"]).ToString();
                //string fecha = (dt.Rows[dt.Rows.Count - 1]["fechaReinicio"]).ToString();

                //if (IdEstado.Equals("5") && fecha.Length<=1)
                //{
                //    lblFlagContinuarRegistroAvance.Text = "0";
                //}
            }

            grdEvaluacionTab2.DataSource = dt;
            grdEvaluacionTab2.DataBind();

        }

        protected void btnGuardarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_OficioTab4))
            {
                if (validaArchivo(FileUpload_InformeTab4))
                {
                    if (validaArchivo(FileUpload_ActaTab4))
                    {
                        if (ValidarEvaluacionRecomendacionTab2())
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            _BEEjecucion.Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text);

                            _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue);
                            _BEEjecucion.id_tipoSubEstadoEjecucion = ddlTipoSubEstadoInformeTab2.SelectedValue;
                            _BEEjecucion.id_tipoEstadoParalizado = ddlTipoSubEstado2InformeTab2.SelectedValue.Equals("") ? "0" : ddlTipoSubEstado2InformeTab2.SelectedValue;
                            //_BEEjecucion.id_tipoSubEstadoParalizado = ddlTipoProblematicaTab2.SelectedValue.Equals("") ? "0" : ddlTipoProblematicaTab2.SelectedValue;

                            string vDetalleProblemas = "";
                            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("82"))//Paralizados permanentes
                            {
                                //List<ListItem> selected = new List<ListItem>();
                                foreach (ListItem item in chbProblematicaTab2.Items)
                                {
                                    //string myJson = "{'Username': 'myusername','Password':'pass'}";
                                    vDetalleProblemas = vDetalleProblemas + "{\"idSubEstado\":\"" + ddlTipoSubEstadoInformeTab2.SelectedValue + "\", \"idProblematica\":\"" + item.Value;
                                    if (item.Selected)
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"1\"},";
                                    }
                                    else
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"0\"},";
                                    }
                                }

                                vDetalleProblemas = vDetalleProblemas.Substring(0, vDetalleProblemas.Length - 1);
                                vDetalleProblemas = "[" + vDetalleProblemas + "]";
                            }
                            else
                            {
                                if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("87"))//Suspension
                                {
                                    _BEEjecucion.Date_fechaInicio = _Metodo.VerificaFecha(txtFechaInicioSuspensionTab2.Text);
                                    _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinalReinicioTab2.Text);
                                }
                            }
                            _BEEjecucion.detalleProblematica = vDetalleProblemas;

                            _BEEjecucion.Evaluacion = txtEvaluacionTab2.Text;
                            _BEEjecucion.Recomendacion = txtRecomendacionTab2.Text;
                            _BEEjecucion.FisicoEjec = txtEjecutadoTab2.Text;
                            _BEEjecucion.FisicoProg = txtProgramadoTab2.Text;

                            _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab4);
                            _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab4);
                            _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab4);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                            _BEEjecucion.id_tipoSubMonitoreo = ddlTipoSubMonitoreo.SelectedValue.Equals("") ? "0" : ddlTipoSubMonitoreo.SelectedValue;

                            //trActaVisita.Visible = ddlTipoSubMonitoreo.SelectedValue.Equals("1") ? true : false;

                            //if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("5")) // PARALIZADA
                            //{
                            //    trActaVisita.Visible = false;
                            //}

                            _BEEjecucion.nrotramite = txtNroTramie.Text;
                            //_BEEjecucion.tipodocumento = txtTipoDocumento.Text;
                            _BEEjecucion.id_tipodocumento = ddlTipoDocumento.SelectedValue;
                            _BEEjecucion.asunto = txtAsunto.Text;
                            _BEEjecucion.financieroReal = txtFinanEjecutadoTab2.Text;
                            _BEEjecucion.financieroProgramado = txtFinanProgramadoTab2.Text;

                            if (chbAyudaMemoriaTab4.Checked == true)
                            {
                                _BEEjecucion.flagAyuda = "1";
                            }
                            else
                            {
                                _BEEjecucion.flagAyuda = "0";
                            }

                            int val;
                            val = _objBLEjecucion.spi_MON_EvaluacionRecomendacion(_BEEjecucion); // Devuelve id_evaluacion_recomendacion
                            lblID_evaluacionTab2.Text = val.ToString();

                            if (val > 0)
                            {
                                string script = "<script>alert('Se registró Correctamente. Puede continuar con el registro de la información complementaria (Acta de Visita e Informe) según corresponda.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                cargarEvaluacionTab2();
                                btnGuardarEvaluacionTab2.Visible = false;
                                Panel_InformacionComplementaria.Visible = true;
                                CargaDatosTab2();
                                Up_Tab2.Update();
                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                            }

                        }
                    }
                }
            }
        }

        //protected Boolean ValidarEvaluacionRecomendacionTab2()
        //{
        //    Boolean result;
        //    result = true;

        //    if (ddlTipoMonitoreo.SelectedValue == "")
        //    {
        //        string script = "<script>alert('Ingrese tipo de monitoreo.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    if (ddlTipoSubMonitoreo.SelectedValue.Equals("") && ddlTipoSubMonitoreo.Visible == true)
        //    {
        //        string script = "<script>alert('Seleccionar Sub tipo de monitoreo.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    if (txtFechaVisitaTab2.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar Fecha de Visita.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    if (_Metodo.ValidaFecha(txtFechaVisitaTab2.Text) == false)
        //    {
        //        string script = "<script>alert('Formato no valido de Fecha de Visita.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }
        //    else
        //    {

        //        if (Convert.ToDateTime(txtFechaVisitaTab2.Text) > DateTime.Today)
        //        {
        //            string script = "<script>alert('La fecha ingresada es mayor a la fecha de hoy.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }
        //    }

        //    if (ddlTipoEstadoInformeTab2.SelectedValue.Equals(""))
        //    {
        //        string script = "<script>alert('Seleccionar estado.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") && ddlTipoSubEstadoInformeTab2.Visible == true)
        //    {
        //        string script = "<script>alert('Seleccionar Sub Estado.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("87") && txtFechaInicioSuspensionTab2.Text.Length < 10)
        //    {
        //        string script = "<script>alert('Ingresar fecha de suspensión.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    if (txtFechaInicioSuspensionTab2.Text.Length > 9)
        //    {
        //        if (_Metodo.ValidaFecha(txtFechaInicioSuspensionTab2.Text) == false)
        //        {
        //            string script = "<script>alert('Formato no valido de fecha de suspensión.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }
        //        else
        //        {

        //            if (Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text) > DateTime.Today)
        //            {
        //                string script = "<script>alert('La fecha de suspensión ingresada es mayor a la fecha de hoy.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                result = false;
        //                return result;
        //            }
        //        }
        //    }

        //    if (txtFechaFinalReinicioTab2.Text.Length > 9)
        //    {
        //        if (_Metodo.ValidaFecha(txtFechaFinalReinicioTab2.Text) == false)
        //        {
        //            string script = "<script>alert('Formato no valido de fecha de reinicio.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }
        //        else
        //        {

        //            if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) > DateTime.Today)
        //            {
        //                string script = "<script>alert('La fecha de reinicio ingresada es mayor a la fecha de hoy.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                result = false;
        //                return result;
        //            }

        //            if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) < Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text))
        //            {
        //                string script = "<script>alert('La fecha de reinicio debe ser mayor a la fecha de suspensión.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                result = false;
        //                return result;
        //            }
        //        }
        //    }

        //    if (ddlTipoSubEstado2InformeTab2.Visible && ddlTipoSubEstado2InformeTab2.SelectedValue.Equals(""))
        //    {
        //        string script = "<script>alert('Seleccionar Sub Estado 2.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }


        //    if (ddlTipoSubEstado2InformeTab2.SelectedValue.Equals("82")) // PALIZADO PERMANENTE
        //    {
        //        int contadorProblemas = 0;
        //        foreach (ListItem item in chbProblematicaTab2.Items)
        //        {
        //            if (item.Selected)
        //            {
        //                contadorProblemas = contadorProblemas + 1;
        //            }
        //        }

        //        if (contadorProblemas > 0)
        //        {
        //            string script = "<script>alert('Seleccionar problematica.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        }

        //        result = false;
        //        return result;
        //    }

        //    // Actos Previos - En Ejecución - Paralizada
        //    if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("41") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("4") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("5"))
        //    {
        //        if (txtFechaTerminoTab2.Text.Length > 0)
        //        {
        //            string script = "<script>alert('No puede registrar la fecha de termino real en el estado  : " + ddlTipoEstadoInformeTab2.SelectedItem.Text + "');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }

        //    }

        //    // Concluido - En Ejecución - Paralizada
        //    if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("6") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("4") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("5"))
        //    {
        //        if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
        //        {
        //            string script = "<script>alert('Para registrar el estado "+ ddlTipoEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }
        //    }

        //    // Concluido
        //    if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("6"))
        //    {
        //        if (txtFechaTerminoTab2.Text.Length == 0)
        //        {
        //            string script = "<script>alert('Para registrar el estado Concluido debe registrar primero la fecha de termino real.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }

        //        //En Liquidación, Liquidada, Recepcionada
        //        if ((ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("75") || ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("77") || ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("78")) && txtFechaRecepcionTab2.Text.Equals(""))
        //        {
        //            string script = "<script>alert('Para registrar el estado Concluido y subestado "+ ddlTipoSubEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de recepción.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }

        //    }

        //    if (txtEjecutadoTab2.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar avance físico ejecutado.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }
        //    else
        //    {
        //        if (Convert.ToDouble(txtEjecutadoTab2.Text) > 100)
        //        {
        //            string script = "<script>alert('El avance físico ejecutado no puede ser mayor a 100%.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }
        //    }

        //    if (txtProgramadoTab2.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar avance físico programado.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;

        //    }
        //    else
        //    {
        //        if (Convert.ToDouble(txtProgramadoTab2.Text) > 100)
        //        {
        //            string script = "<script>alert('El avance físico programado no puede ser mayor a 100%.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }
        //    }

        //    //CONSISTENCIA DE AVANCES REGISTRADOS
        //    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    List<BE_MON_Ejecucion> ListAvance = new List<BE_MON_Ejecucion>();
        //    ListAvance = _objBLEjecucion.F_spMON_EvaluacionRecomendacion(_BEEjecucion);

        //    if (btnGuardarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 0)
        //    {
        //        string script = "<script>alert('Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }
        //    else if (btnModificarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 1)
        //    {
        //        string script = "<script>alert('Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        result = false;
        //        return result;
        //    }

        //    BE_MON_Ejecucion v1 = new BE_MON_Ejecucion { Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text), Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text) };
        //    BE_MON_Ejecucion itemMayor = null;
        //    BE_MON_Ejecucion itemMenor = null;
        //    //Item mayor a la fecha ingresada
        //    if (ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
        //    {
        //        itemMayor = ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).First();

        //        if (Convert.ToDouble(itemMayor.FisicoEjec) < Convert.ToDouble(txtEjecutadoTab2.Text))
        //        {
        //            string script = "<script>alert('El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.FisicoEjec + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }

        //        //if (Convert.ToDouble(itemMayor.financieroReal) < Convert.ToDouble(txtFinanEjecutadoTab2.Text))
        //        //{
        //        //    string script = "<script>alert('El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.financieroReal + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".');</script>";
        //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        //    result = false;
        //        //    return result;
        //        //}
        //    }
        //    //Item menor a la fecha ingresada
        //    if (ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
        //    {
        //        itemMenor = ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Last();

        //        if (Convert.ToDouble(itemMenor.FisicoEjec) > Convert.ToDouble(txtEjecutadoTab2.Text))
        //        {
        //            string script = "<script>alert('El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.FisicoEjec + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            result = false;
        //            return result;
        //        }

        //        //if (Convert.ToDouble(itemMenor.financieroReal) > Convert.ToDouble(txtFinanEjecutadoTab2.Text))
        //        //{
        //        //    string script = "<script>alert('El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.financieroReal + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".');</script>";
        //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        //    result = false;
        //        //    return result;
        //        //}
        //    }


        //    if (lblIdEstado.Text.Trim() != "")
        //    {
        //        if (ddlTipoEstadoInformeTab2.Enabled == true) //habilitado para registrar
        //        {
        //            if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue), 3)))
        //            {
        //                string script = "<script>alert('No puede volver el proyecto a Actos Previos');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                result = false;
        //                return result;

        //            }
        //        }
        //    }


        //    return result;
        //}

        protected Boolean ValidarEvaluacionRecomendacionTab2()
        {
            string result;
            result = "";

            if (ddlTipoMonitoreo.SelectedValue == "")
            {
                result = result + "*Ingrese tipo de monitoreo.\\n";
            }

            if (ddlTipoSubMonitoreo.SelectedValue.Equals("") && ddlTipoSubMonitoreo.Visible == true)
            {
                result = result + "*Seleccionar Sub tipo de monitoreo.\\n";
            }

            if (txtFechaVisitaTab2.Text == "")
            {
                result = result + "*Ingresar Fecha de Visita.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaVisitaTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha de Visita.\\n";
            }
            else
            {
                if (Convert.ToDateTime(txtFechaVisitaTab2.Text) > DateTime.Today)
                {
                    result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

                }
            }
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals(""))
            {
                result = result + "*Seleccionar estado.\\n";
            }
            else if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("") && ddlTipoSubEstadoInformeTab2.Visible == true)
            {
                result = result + "*Seleccionar Sub Estado.\\n";
            }

            //Suspensión del plazo de ejecución
            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("87") && txtFechaInicioSuspensionTab2.Text.Length < 10)
            {
                result = result + "*Ingresar fecha de suspensión.\\n";
            }

            if (txtFechaInicioSuspensionTab2.Text.Length > 9)
            {
                if (_Metodo.ValidaFecha(txtFechaInicioSuspensionTab2.Text) == false)
                {
                    result = result + "*Formato no valido de fecha de suspensión.\\n";
                }
                else
                {
                    if (Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text) > DateTime.Today)
                    {
                        result = result + "*La fecha de suspensión ingresada es mayor a la fecha de hoy.\\n";
                    }
                }
            }

            if (txtFechaFinalReinicioTab2.Text.Length > 9)
            {
                if (_Metodo.ValidaFecha(txtFechaFinalReinicioTab2.Text) == false)
                {
                    result = result + "*Formato no valido de fecha de reinicio.\\n";
                }
                else
                {
                    if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) > DateTime.Today)
                    {
                        result = result + "*La fecha de reinicio ingresada es mayor a la fecha de hoy.\\n";
                    }

                    if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) < Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text))
                    {
                        result = result + "*La fecha de reinicio debe ser mayor a la fecha de suspensión.\\n";
                    }
                }
            }

            if (ddlTipoSubEstado2InformeTab2.Visible && ddlTipoSubEstado2InformeTab2.SelectedValue.Equals(""))
            {
                result = result + "*Seleccionar Sub Estado 2.\\n";
            }

            if (ddlTipoSubEstado2InformeTab2.SelectedValue.Equals("82")) // PALIZADO PERMANENTE
            {
                int contadorProblemas = 0;
                foreach (ListItem item in chbProblematicaTab2.Items)
                {
                    if (item.Selected)
                    {
                        contadorProblemas = contadorProblemas + 1;
                    }
                }

                if (contadorProblemas > 0)
                {
                    result = result + "*Seleccionar problematica.\\n";
                }

            }

            //Actos Previos - Por Convocar
            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("70") && grd_ResumConvenioTab0.Rows.Count == 0 && LblID_SOLICITUD.Text != "0" && TxtFechaIncorporacionRecurso.Text.Length<1)
            {
                result = result + "*Debe registrar primero la pestaña de financiamiento antes del registro de estado.\\n";
            }



            //ACctos Previos - Proceso de Seleccion - Indirecta
            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("72") && ddlProcesoTab1.SelectedValue == "1" && ddlModalidadTab1.SelectedValue == "2")
            {
                if (grdSeguimientoProcesoTab1.Rows.Count == 0)
                {
                    result = result + "*Debe registrar primero el seguimiento al proceso de selección.\\n";
                }
            }
            //ACctos Previos - Proceso de Seleccion - Por Iniciar
            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("71") && ddlProcesoTab1.SelectedValue == "1" && ddlModalidadTab1.SelectedValue == "2")
            {
                if (grdSeguimientoProcesoTab1.Rows.Count == 0)
                {
                    result = result + "*Debe registrar primero el seguimiento al proceso de selección.\\n";
                }

                if (txtProTab1.Text.Equals("") || txtProConsentidaTab1.Text.Equals("") || txtFechaFirmaContratoTab1.Text.Equals(""))
                {
                    result = result + "*Debe registrar primero los campos del resultado al proceso de selección.\\n";
                }
            }


            // Actos Previos - En Ejecución - Paralizada
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("41") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("4") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("5"))
            {
                if (txtFechaTerminoTab2.Text.Length > 0)
                {
                    result = result + "*No puede registrar la fecha de termino real en el estado  : " + ddlTipoEstadoInformeTab2.SelectedItem.Text + ".\\n";
                }

            }

            // En Ejecución
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("4"))
            {
                if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlTipoEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.\\n";
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length == 0) //FECHA DE ENTREGA DE TERRENO
                {
                    result = result + "*Para registrar el estado " + ddlTipoEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de entrega de terrno.\\n";
                }
            }

            // Concluido - Paralizada
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("6") || ddlTipoEstadoInformeTab2.SelectedValue.Equals("5"))
            {
                if (txtFechaInicioTab2.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlTipoEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio.\\n";
                }
            }

            // Concluido
            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("6"))
            {
                if (txtFechaTerminoTab2.Text.Length == 0)
                {
                    result = result + "*Para registrar el estado Concluido debe registrar primero la fecha de termino real.\\n";
                }

                //En Liquidación, Liquidada, Recepcionada
                if ((ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("75") || ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("77") || ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("78")) && txtFechaRecepcionTab2.Text.Equals(""))
                {
                    result = result + "*Para registrar el estado Concluido y subestado " + ddlTipoSubEstadoInformeTab2.SelectedItem.Text + " debe registrar primero la fecha de recepción.\\n";
                }

            }

            if (txtEjecutadoTab2.Text == "")
            {
                result = result + "*Ingresar avance físico ejecutado.\\n";
            }
            else if (Convert.ToDouble(txtEjecutadoTab2.Text) > 100)
            {
                result = result + "*El avance físico ejecutado no puede ser mayor a 100%.\\n";

            }


            if (txtProgramadoTab2.Text == "")
            {
                result = result + "*Ingresar avance físico programado.\\n";
            }
            else
            {
                if (Convert.ToDouble(txtProgramadoTab2.Text) > 100)
                {
                    result = result + "*El avance físico programado no puede ser mayor a 100%.\\n";
                }
            }

            //CONSISTENCIA DE AVANCES REGISTRADOS
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            List<BE_MON_Ejecucion> ListAvance = new List<BE_MON_Ejecucion>();
            ListAvance = _objBLEjecucion.F_spMON_EvaluacionRecomendacion(_BEEjecucion);

            if (btnGuardarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 0)
            {
                result = result + "*Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.\\n";
            }
            else if (btnModificarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 1)
            {
                result = result + "*Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.\\n";
            }

            if (txtFechaVisitaTab2.Text.Length > 0)
            {
                BE_MON_Ejecucion v1 = new BE_MON_Ejecucion { Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text), Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text) };
                BE_MON_Ejecucion itemMayor = null;
                BE_MON_Ejecucion itemMenor = null;
                //Item mayor a la fecha ingresada
                if (ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
                {
                    itemMayor = ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).First();

                    if (Convert.ToDouble(itemMayor.FisicoEjec) < Convert.ToDouble(txtEjecutadoTab2.Text))
                    {
                        result = result + "*El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.FisicoEjec + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".\\n";
                    }

                    //if (Convert.ToDouble(itemMayor.financieroReal) < Convert.ToDouble(txtFinanEjecutadoTab2.Text))
                    //{
                    //    string script = "<script>alert('El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.financieroReal + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".');</script>";
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //    result = false;
                    //    return result;
                    //}
                }
                //Item menor a la fecha ingresada
                if (ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
                {
                    itemMenor = ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Last();

                    if (Convert.ToDouble(itemMenor.FisicoEjec) > Convert.ToDouble(txtEjecutadoTab2.Text))
                    {
                        result = result + "*El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.FisicoEjec + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".\\n";
                    }

                    //if (Convert.ToDouble(itemMenor.financieroReal) > Convert.ToDouble(txtFinanEjecutadoTab2.Text))
                    //{
                    //    string script = "<script>alert('El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.financieroReal + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".');</script>";
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //    result = false;
                    //    return result;
                    //}
                }
            }

            if (lblIdEstado.Text.Trim() != "")
            {
                if (ddlTipoEstadoInformeTab2.Enabled == true) //habilitado para registrar
                {
                    if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue), 3)))
                    {
                        result = result + "*No puede volver el proyecto a Actos Previos.\\n";
                    }
                }
            }

            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                return false;
            }
            else
            {
                return true;
            }
        }
        protected void btnModificarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_OficioTab4))
            {
                if (validaArchivo(FileUpload_InformeTab4))
                {
                    if (validaArchivo(FileUpload_ActaTab4))
                    {

                        if (ValidarEvaluacionRecomendacionTab2())
                        {
                            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            _BEEjecucion.Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text);

                            _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlTipoEstadoInformeTab2.SelectedValue);
                            _BEEjecucion.id_tipoSubEstadoEjecucion = ddlTipoSubEstadoInformeTab2.SelectedValue;
                            _BEEjecucion.id_tipoEstadoParalizado = ddlTipoSubEstado2InformeTab2.SelectedValue.Equals("") ? "0" : ddlTipoSubEstado2InformeTab2.SelectedValue;
                            //_BEEjecucion.id_tipoSubEstadoParalizado = ddlTipoProblematicaTab2.SelectedValue.Equals("") ? "0" : ddlTipoProblematicaTab2.SelectedValue;

                            string vDetalleProblemas = "";
                            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("82"))//Paralizados permanentes
                            {
                                //List<ListItem> selected = new List<ListItem>();
                                foreach (ListItem item in chbProblematicaTab2.Items)
                                {
                                    //string myJson = "{'Username': 'myusername','Password':'pass'}";
                                    vDetalleProblemas = vDetalleProblemas + "{\"idSubEstado\":\"" + ddlTipoSubEstadoInformeTab2.SelectedValue + "\", \"idProblematica\":\"" + item.Value;
                                    if (item.Selected)
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"1\"},";
                                    }
                                    else
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"0\"},";
                                    }
                                }

                                vDetalleProblemas = vDetalleProblemas.Substring(0, vDetalleProblemas.Length - 1);

                                vDetalleProblemas = "[" + vDetalleProblemas + "]";
                            }
                            else
                            {
                                if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("87"))//Suspension
                                {
                                    _BEEjecucion.Date_fechaInicio = _Metodo.VerificaFecha(txtFechaInicioSuspensionTab2.Text);
                                    _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinalReinicioTab2.Text);
                                }
                            }

                            _BEEjecucion.detalleProblematica = vDetalleProblemas;

                            _BEEjecucion.Evaluacion = txtEvaluacionTab2.Text;
                            _BEEjecucion.Recomendacion = txtRecomendacionTab2.Text;
                            _BEEjecucion.FisicoEjec = txtEjecutadoTab2.Text;
                            _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                            _BEEjecucion.id_tipoSubMonitoreo = ddlTipoSubMonitoreo.SelectedValue.Equals("") ? "0" : ddlTipoSubMonitoreo.SelectedValue;
                            _BEEjecucion.FisicoProg = txtProgramadoTab2.Text;
                            _BEEjecucion.nrotramite = txtNroTramie.Text;
                            _BEEjecucion.id_tipodocumento = ddlTipoDocumento.SelectedValue;
                            _BEEjecucion.asunto = txtAsunto.Text;
                            _BEEjecucion.Tipo = 1;
                            _BEEjecucion.financieroProgramado = txtFinanProgramadoTab2.Text;
                            _BEEjecucion.financieroReal = txtFinanEjecutadoTab2.Text;

                            if (chbAyudaMemoriaTab4.Checked == true)
                            {
                                _BEEjecucion.flagAyuda = "1";
                            }
                            else
                            {
                                _BEEjecucion.flagAyuda = "0";
                            }

                            if (FileUpload_ActaTab4.HasFile)
                            {
                                _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlActa = LnkbtnActaTab4.Text;
                            }

                            if (FileUpload_InformeTab4.HasFile)
                            {
                                _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlInforme = LnkbtnInformeTab4.Text;
                            }


                            if (FileUpload_OficioTab4.HasFile)
                            {
                                _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlOficio = LnkbtnOficioTab4.Text;
                            }

                            int val;
                            try
                            {
                                val = _objBLEjecucion.spud_MON_EvaluacionRecomendacion(_BEEjecucion);
                            }
                            catch (Exception ex)
                            {
                                val = 0;
                            }

                            if (val == 1)
                            {
                                string script = "<script>alert('Se Actualizó Correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                cargarEvaluacionTab2();

                                txtFechaVisitaTab2.Text = "";
                                txtEvaluacionTab2.Text = "";
                                txtRecomendacionTab2.Text = "";
                                txtProgramadoTab2.Text = "";
                                txtEjecutadoTab2.Text = "";
                                ddlTipoMonitoreo.SelectedValue = "";
                                txtNroTramie.Text = "";
                                txtTipoDocumento.Text = "";
                                ddlTipoDocumento.SelectedValue = "";
                                txtAsunto.Text = "";
                                chbAyudaMemoriaTab4.Checked = false;
                                Panel_ObservacionTab2.Visible = false;
                                imgbtnAgregarEvaluacionTab2.Visible = true;

                                CargaDatosTab2();
                                Up_Tab2.Update();


                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }

                        }
                    }
                }
            }

        }
        protected void btnCancelarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            Panel_ObservacionTab2.Visible = false;
            imgbtnAgregarEvaluacionTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void grdEvaluacionTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_evaluacionRecomendacion;
            id_evaluacionRecomendacion = grdEvaluacionTab2.SelectedDataKey.Value.ToString();
            lblID_evaluacionTab2.Text = id_evaluacionRecomendacion.ToString();

            GridViewRow row = grdEvaluacionTab2.SelectedRow;
            string xidTipoEstado = ((Label)row.FindControl("lblId_TipoEstadoEjecuccion")).Text;
            string xidTipoSubEstado = ((Label)row.FindControl("lblId_tipoSubEstadoEjecucion")).Text;

            if (xidTipoSubEstado.Equals("83")) //Paralizados Temporales
            {
                string script = "<script>alert('No se puede editar esta información.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                txtFechaVisitaTab2.Text = ((Label)row.FindControl("lblFechaVisita")).Text;
                txtEvaluacionTab2.Text = ((Label)row.FindControl("lblEvaluacion")).Text;

                txtRecomendacionTab2.Text = ((Label)row.FindControl("lblRecomendacion")).Text;

                txtEjecutadoTab2.Text = ((Label)row.FindControl("lblFisicoEjec")).Text;
                txtProgramadoTab2.Text = ((Label)row.FindControl("lblFisicoProg")).Text;

                txtFinanProgramadoTab2.Text = ((Label)row.FindControl("lblFinancieroProg")).Text;
                txtFinanEjecutadoTab2.Text = ((Label)row.FindControl("lblFinancieroEjec")).Text;

                ddlTipoMonitoreo.Text = ((Label)row.FindControl("lblid_tipoMonitor")).Text;
                CargaSubTipoMonitoreo(ddlTipoMonitoreo.SelectedValue);
                ddlTipoSubMonitoreo.SelectedValue = ((Label)row.FindControl("lblId_TipoSubMonitoreo")).Text;

                if (ddlTipoSubMonitoreo.SelectedValue.Equals("1"))
                {
                    trActaVisita.Visible = true;
                }
                else
                {
                    trActaVisita.Visible = false;
                }


                txtNroTramie.Text = ((Label)row.FindControl("lblNroTramite")).Text;
                ddlTipoDocumento.Text = ((Label)row.FindControl("lblid_tipoDocumento")).Text;
                txtAsunto.Text = ((Label)row.FindControl("lblAsunto")).Text;

                chbAyudaMemoriaTab4.Checked = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;

                LnkbtnActaTab4.Text = ((ImageButton)row.FindControl("imgDocActaTab4")).ToolTip;
                LnkbtnInformeTab4.Text = ((ImageButton)row.FindControl("imgDocInformeTab4")).ToolTip;
                LnkbtnOficioTab4.Text = ((ImageButton)row.FindControl("imgDocOficioTab4")).ToolTip;

                lblNomUsuarioFlagAyudaTab4.Text = "(Actualizó: " + ((Label)row.FindControl("lblId_UsuarioFlag")).Text + " - " + ((Label)row.FindControl("lblFecha_UpdateFlag")).Text + ")";
                lblNomUsuarioObservaciones.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

                GeneraIcoFile(LnkbtnActaTab4.Text, imgbtnActaTab4);
                GeneraIcoFile(LnkbtnInformeTab4.Text, imgbtnInformeTab4);
                GeneraIcoFile(LnkbtnOficioTab4.Text, imgbtnOficioTab4);

                btnModificarEvaluacionTab2.Visible = true;
                btnGuardarEvaluacionTab2.Visible = false;
                Panel_ObservacionTab2.Visible = true;
                imgbtnAgregarEvaluacionTab2.Visible = false;
                chbAyudaMemoriaTab4.Enabled = false;

                CargaTipoEstadosDetalle();

                ddlTipoEstadoInformeTab2.SelectedValue = xidTipoEstado;
                CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);
                ddlTipoSubEstadoInformeTab2.SelectedValue = xidTipoSubEstado;

                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
                Up_FechasSuspensionTab2.Update();

                if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("5"))
                {
                    CargaTipoSubEstado2(ddlTipoSubEstadoInformeTab2.SelectedValue);
                    ddlTipoSubEstado2InformeTab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                    ddlTipoSubEstado2InformeTab2.Visible = true;
                    lblNombreSubEstado2.Visible = true;
                    Up_SubEstado2NombreTab2.Update();
                    Up_SubEstado2DetalleTab2.Update();

                    CargaTipoSubEstadoParalizado(ddlTipoSubEstado2InformeTab2.SelectedValue); //TIPO PROBLEMATICA

                    try
                    {
                        string idDetalleProblematica = ((Label)row.FindControl("lblId_DetalleProblematica")).Text;
                        string[] itemProblematica = idDetalleProblematica.Split(',');
                        foreach (string item in itemProblematica)
                        {
                            foreach (ListItem itemChkb in chbProblematicaTab2.Items)
                            {
                                if (itemChkb.Value == item)
                                {
                                    itemChkb.Selected = true;
                                }
                            }
                        }
                    }
                    catch
                    {
                        // CargaTipoSubEstadoParalizado(ddlTipoSubEstado2InformeTab2.SelectedValue); //Volver a cargar PROBLEMATICA
                    }
                    lblNombreProblematicaTab2.Visible = true;
                    //ddlTipoProblematicaTab2.Visible = true;
                    chbProblematicaTab2.Visible = true;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();
                    //Up_chbTipoProblematicaTab2.Update();

                    trActaVisita.Visible = false; // Ocultar acta de visita para paralizadas.
                }
                else
                {
                    //ddlTipoSubEstado2InformeTab2.SelectedValue = "";
                    ddlTipoSubEstado2InformeTab2.Visible = false;
                    lblNombreSubEstado2.Visible = false;
                    Up_SubEstado2NombreTab2.Update();
                    Up_SubEstado2DetalleTab2.Update();

                    lblNombreProblematicaTab2.Visible = false;
                    chbProblematicaTab2.Items.Clear();
                    chbProblematicaTab2.Visible = false;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();

                    if (xidTipoSubEstado.Equals("87")) // Suspension de plazo
                    {
                        trSuspensionTab2.Visible = true;
                        trReinicioTab2.Visible = true;

                        txtFechaInicioSuspensionTab2.Text = ((Label)row.FindControl("lblFechaSuspensionGrilla")).Text;
                        txtFechaFinalReinicioTab2.Text = ((Label)row.FindControl("lblFechaReinicioGrilla")).Text;
                        Up_FechasSuspensionTab2.Update();

                        //Cargar SubEstado2
                        CargaTipoSubEstado2(ddlTipoSubEstadoInformeTab2.SelectedValue);
                        ddlTipoSubEstado2InformeTab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                        ddlTipoSubEstado2InformeTab2.Visible = true;
                        lblNombreSubEstado2.Visible = true;
                        Up_SubEstado2NombreTab2.Update();
                        Up_SubEstado2DetalleTab2.Update();
                    }

                    if (xidTipoSubEstado.Equals("80")) // Atrasado
                    {
                        //Cargar SubEstado2
                        CargaTipoSubEstado2(ddlTipoSubEstadoInformeTab2.SelectedValue);
                        ddlTipoSubEstado2InformeTab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                        ddlTipoSubEstado2InformeTab2.Visible = true;
                        lblNombreSubEstado2.Visible = true;
                        Up_SubEstado2NombreTab2.Update();
                        Up_SubEstado2DetalleTab2.Update();
                    }
                }
                Panel_InformacionComplementaria.Visible = true;

                //VALIDACION PARA EVITAR CAMBIO DE ESTADOS
                string vfecha = ((Label)row.FindControl("lblFecha_Update")).Text;
                if (vfecha.Length > 10 && !xidTipoEstado.Equals(""))
                {
                    if (Convert.ToDateTime(vfecha.Substring(0, 10)).ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        ddlTipoEstadoInformeTab2.Enabled = true;
                        ddlTipoSubEstadoInformeTab2.Enabled = true;
                        ddlTipoSubEstado2InformeTab2.Enabled = true;
                    }
                    else
                    {
                        if (!(LblID_USUARIO.Text.Equals("792") || LblID_USUARIO.Text.Equals("481"))) //CESAR MEDINA Y USUARIO MONITOR PUEDEN MODIFIAR LOS ESTADOS
                        {
                            ddlTipoEstadoInformeTab2.Enabled = false;
                            ddlTipoSubEstadoInformeTab2.Enabled = false;
                            ddlTipoSubEstado2InformeTab2.Enabled = false;
                        }
                        if (ddlTipoSubEstado2InformeTab2.Visible && ddlTipoSubEstado2InformeTab2.SelectedValue.Equals(""))
                        {
                            ddlTipoSubEstado2InformeTab2.Enabled = true;
                        }
                        if (lblCOD_SUBSECTOR.Text.Equals("3"))
                        {
                            ddlTipoSubEstadoInformeTab2.Enabled = true;
                            ddlTipoSubEstado2InformeTab2.Enabled = true;
                        }
                    }
                }
            }
            Up_Tab2.Update();
        }

        protected void CargaSubTipoMonitoreo(string vTipoMonitoreo)
        {
            ddlTipoSubMonitoreo.ClearSelection();

            if (!vTipoMonitoreo.Equals(""))
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                list = _objBLEjecucion.F_spMON_ListarSubTipoMonitoreo(Convert.ToInt32(vTipoMonitoreo));

                ddlTipoSubMonitoreo.DataSource = list;
                ddlTipoSubMonitoreo.DataTextField = "nombre";
                ddlTipoSubMonitoreo.DataValueField = "valor";
                ddlTipoSubMonitoreo.DataBind();

                if (list.Count == 0)
                {
                    lblTipoSubNombre.Visible = false;
                    ddlTipoSubMonitoreo.Visible = false;
                }
                else
                {
                    lblTipoSubNombre.Visible = true;
                    ddlTipoSubMonitoreo.Visible = true;
                }
            }

            ddlTipoSubMonitoreo.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }
        protected void grdEvaluacionTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdEvaluacionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fechaVisita = Convert.ToDateTime("09/09/2012");

                int val = _objBLEjecucion.spud_MON_EvaluacionRecomendacion(_BEEjecucion);

                if (val == 1)
                {
                    CargaDatosTab2();
                    cargarEvaluacionTab2();
                    Up_Tab2.Update();

                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grdEvaluacionTab2_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imbActa = (ImageButton)e.Row.FindControl("imgDocActaTab4");
                GeneraIcoFile(imbActa.ToolTip, imbActa);

                ImageButton imbInforme = (ImageButton)e.Row.FindControl("imgDocInformeTab4");
                GeneraIcoFile(imbInforme.ToolTip, imbInforme);

                ImageButton imbOficio = (ImageButton)e.Row.FindControl("imgDocOficioTab4");
                GeneraIcoFile(imbOficio.ToolTip, imbOficio);

                CheckBox chbFlagAyuda = (CheckBox)e.Row.FindControl("chbFlagAyuda");
                if (lblID_ACCESO.Text.Equals("0"))
                {
                    chbFlagAyuda.Enabled = false;
                }

                if (chbFlagAyuda.Text == "1")
                {
                    chbFlagAyuda.Checked = true;
                    chbFlagAyuda.Text = "";
                }
                else
                {
                    chbFlagAyuda.Checked = false;
                    chbFlagAyuda.Text = "";
                }

                // VISITA DE MONITOREO
                Label lblId_TipoSubMonitoreo = (Label)e.Row.FindControl("lblId_TipoSubMonitoreo");
                if (lblId_TipoSubMonitoreo.Text.Equals("1"))
                {
                    LinkButton lnkbtnDescargarActaMonitoreoGrilla = (LinkButton)e.Row.FindControl("lnkbtnDescargarActaMonitoreoGrilla");
                    lnkbtnDescargarActaMonitoreoGrilla.Visible = true;
                }

                //PARALIZADO
                Label lblId_TipoEstadoEjecuccion = (Label)e.Row.FindControl("lblId_TipoEstadoEjecuccion");
                if (lblId_TipoEstadoEjecuccion.Text.Equals("5"))
                {
                    //OCULTAR ACTA DE VISITA PARA PARALIZADOS
                    LinkButton lnkbtnDescargarActaMonitoreoGrilla = (LinkButton)e.Row.FindControl("lnkbtnDescargarActaMonitoreoGrilla");
                    lnkbtnDescargarActaMonitoreoGrilla.Visible = false;
                }

                //
                Label lblFlagInformeTipeado = (Label)e.Row.FindControl("lblFlagInformeTipeado");
                if (lblFlagInformeTipeado.Text.Equals("0"))
                {
                    LinkButton lnkbtnDescargarInformeMonitoreoGrilla = (LinkButton)e.Row.FindControl("lnkbtnDescargarInformeMonitoreoGrilla");
                    lnkbtnDescargarInformeMonitoreoGrilla.Visible = false;
                }

            }
        }

        protected void imgDocActaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocActaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocInformeTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocInformeTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocOficioTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocOficioTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnActaTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab4.Text);
                Response.End();
            }
        }
        protected void LnkbtnInformeTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnInformeTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnInformeTab4.Text);
                Response.End();
            }
        }
        protected void LnkbtnOficioTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnOficioTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnOficioTab4.Text);
                Response.End();
            }
        }
        //protected void btnComponentesPNSU_OnClick(object sender, EventArgs e)
        //{
        //    VarPnsu();
        //    cargaComponentesPNSU();
        //    Up_Componente.Update();
        //    MPE_PNSU.Show();

        //}

        //private void VarPnsu()
        //{

        //    DataTable _obj;
        //    _BEEjecucion.snip = lblSNIP.Text;
        //    _BEEjecucion.tipoFinanciamiento = 3;

        //    _obj = _objBLEjecucion.spMON_ObtenerVariablesPNSU(_BEEjecucion);

        //    if (_obj.Rows.Count > 0)
        //    {
        //        txtA_captacion_nuc.Text = _obj.Rows[0]["a_captacion_nuc"].ToString();//
        //        txtA_captacion_cacl.Text = _obj.Rows[0]["a_captacion_cacl"].ToString();//

        //        txtA_trataPotable_num.Text = _obj.Rows[0]["a_trataPotable_num"].ToString();//
        //        txtA_trataPotable_cap.Text = _obj.Rows[0]["a_trataPotable_cap"].ToString();//

        //        txtA_conduccion_nuc.Text = _obj.Rows[0]["a_conduccion_nuc"].ToString();
        //        txtA_conduccion_cacd.Text = _obj.Rows[0]["a_conduccion_cacd"].ToString();

        //        txtA_impulsion_num.Text = _obj.Rows[0]["a_impulsion_num"].ToString();
        //        txtA_impulsion_cap.Text = _obj.Rows[0]["a_impulsion_cap"].ToString();

        //        txtA_estacion_num.Text = _obj.Rows[0]["a_estacion_num"].ToString();

        //        txtA_reservorio_nur.Text = _obj.Rows[0]["a_reservorio_nur"].ToString();
        //        txtA_reservorio_carm.Text = _obj.Rows[0]["a_reservorio_carm"].ToString();

        //        txtA_aduccion_nua.Text = _obj.Rows[0]["a_aduccion_nua"].ToString();
        //        txtA_aduccion_caad.Text = _obj.Rows[0]["a_aduccion_caad"].ToString();

        //        txtA_redes_nur.Text = _obj.Rows[0]["a_redes_nur"].ToString();
        //        txtA_redes_card.Text = _obj.Rows[0]["a_redes_card"].ToString();

        //        txtA_conexion_nucn.Text = _obj.Rows[0]["a_conexion_nucn"].ToString();
        //        txtA_conexion_nucr.Text = _obj.Rows[0]["a_conexion_nucr"].ToString();
        //        txtA_conexion_nucp.Text = _obj.Rows[0]["a_conexion_nucp"].ToString();

        //        txtS_colector_num.Text = _obj.Rows[0]["s_colectores_num"].ToString();
        //        txtS_colector_cap.Text = _obj.Rows[0]["s_colectores_cap"].ToString();


        //        txtS_camara_num.Text = _obj.Rows[0]["s_camara_num"].ToString();

        //        txtS_planta_nup.Text = _obj.Rows[0]["s_planta_nup"].ToString();
        //        txtS_planta_capl.Text = _obj.Rows[0]["s_planta_capl"].ToString();

        //        txtS_agua_nua.Text = _obj.Rows[0]["s_agua_nua"].ToString();
        //        txtS_agua_caad.Text = _obj.Rows[0]["s_agua_caad"].ToString();

        //        txtS_conexion_nucn.Text = _obj.Rows[0]["s_conexion_nucn"].ToString();
        //        txtS_conexion_nucr.Text = _obj.Rows[0]["s_conexion_nucr"].ToString();
        //        txtS_conexion_nuclo.Text = _obj.Rows[0]["s_conexion_nuclo"].ToString();

        //        txtS_efluente_num.Text = _obj.Rows[0]["s_efluente_num"].ToString();
        //        txtS_efluente_cap.Text = _obj.Rows[0]["s_efluente_cap"].ToString();

        //        txtS_impulsion_num.Text = _obj.Rows[0]["s_impulsion_num"].ToString();
        //        txtS_impulsion_cap.Text = _obj.Rows[0]["s_impulsion_cap"].ToString();

        //        txtD_tuberia_num.Text = _obj.Rows[0]["d_tuberia_num"].ToString();
        //        txtD_tuberia_cap.Text = _obj.Rows[0]["d_tuberia_cap"].ToString();

        //        txtD_cuneta_num.Text = _obj.Rows[0]["d_cuneta_num"].ToString();
        //        txtD_cuneta_cap.Text = _obj.Rows[0]["d_cuneta_cap"].ToString();

        //        txtD_tormenta_num.Text = _obj.Rows[0]["d_tormenta_num"].ToString();
        //        txtD_conexion_num.Text = _obj.Rows[0]["d_conexion_num"].ToString();
        //        txtD_inspeccion_num.Text = _obj.Rows[0]["d_inspeccion_num"].ToString();

        //        txtD_secundario_num.Text = _obj.Rows[0]["d_secundario_num"].ToString();
        //        txtD_secundario_cap.Text = _obj.Rows[0]["d_secundario_cap"].ToString();

        //        txtD_principal_num.Text = _obj.Rows[0]["d_principal_num"].ToString();
        //        txtD_principal_cap.Text = _obj.Rows[0]["d_principal_cap"].ToString();

        //    }

        //}

        //protected void LlenarPNSU()
        //{
        //    txtA_captacion.Text = "0";
        //    txtA_conduccion.Text = "0";
        //    txtA_estacion.Text = "0";
        //    txtA_impulsion.Text = "0";
        //    txtA_trata.Text = "0";
        //    txtA_reservorio.Text = "0";
        //    txtA_aduccion.Text = "0";
        //    txtA_redes.Text = "0";
        //    txtA_conexion_nueva.Text = "0";
        //    txtA_conexion_piletas.Text = "0";
        //    txtA_conexion_rehab.Text = "0";

        //    txtS_efluente.Text = "0";
        //    txtS_camara.Text = "0";
        //    txtS_emisor.Text = "0";
        //    txtS_Impulsion.Text = "0";
        //    txtS_planta.Text = "0";
        //    txtS_redes.Text = "0";
        //    txtS_conexion_nueva.Text = "0";
        //    txtS_conexion_rehab.Text = "0";
        //    txtS_conexion_unid.Text = "0";
        //    txtS_Impulsion.Text = "0";
        //    txtS_redes.Text = "0";

        //    txtD_tuberia.Text = "0";
        //    txtD_cuneta.Text = "0";
        //    txtD_tormenta.Text = "0";
        //    txtD_conexion.Text = "0";
        //    txtD_inspeccion.Text = "0";
        //    txtD_secundario.Text = "0";
        //    txtD_principal.Text = "0";

        //    txtA_captacion_p.Text = "0";
        //    txtA_conduccion_p.Text = "0";
        //    txtA_estacion_p.Text = "0";
        //    txtA_impulsion_p.Text = "0";
        //    txtA_trata_p.Text = "0";
        //    txtA_reservorio_p.Text = "0";
        //    txtA_aduccion_p.Text = "0";
        //    txtA_redes_p.Text = "0";
        //    txtA_conexion_nueva_p.Text = "0";
        //    txtA_conexion_piletas_p.Text = "0";
        //    txtA_conexion_rehab_p.Text = "0";

        //    txtS_efluente_p.Text = "0";
        //    txtS_camara_p.Text = "0";
        //    txtS_emisor_p.Text = "0";
        //    txtS_Impulsion_p.Text = "0";
        //    txtS_planta_p.Text = "0";
        //    txtS_redes_p.Text = "0";
        //    txtS_conexion_nueva_p.Text = "0";
        //    txtS_conexion_rehab_p.Text = "0";
        //    txtS_conexion_unid_p.Text = "0";
        //    txtS_Impulsion_p.Text = "0";
        //    txtS_redes_p.Text = "0";

        //    txtD_tuberia_p.Text = "0";
        //    txtD_cuneta_p.Text = "0";
        //    txtD_tormenta_p.Text = "0";
        //    txtD_conexion_p.Text = "0";
        //    txtD_inspeccion_p.Text = "0";
        //    txtD_secundario_p.Text = "0";
        //    txtD_principal_p.Text = "0";
        //}

        //protected void cargaComponentesPNSU()
        //{
        //    _BEVarPNSU.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEVarPNSU.TipoFinanciamiento = 3;

        //    dt = _objBLEjecucion.spMON_ComponentesPNSU(_BEVarPNSU);


        //    if (dt.Rows.Count > 0)
        //    {
        //        txtA_captacion.Text = dt.Rows[0]["a_captacion"].ToString();//
        //        txtA_conduccion.Text = dt.Rows[0]["a_conduccion"].ToString();//
        //        txtA_trata.Text = dt.Rows[0]["a_trata"].ToString();//
        //        txtA_reservorio.Text = dt.Rows[0]["a_reservorio"].ToString();//
        //        txtA_aduccion.Text = dt.Rows[0]["a_Aduccion"].ToString();
        //        txtA_redes.Text = dt.Rows[0]["a_redes"].ToString();
        //        txtA_conexion_nueva.Text = dt.Rows[0]["a_conexion_nueva"].ToString();
        //        txtA_conexion_piletas.Text = dt.Rows[0]["a_conexion_piletas"].ToString();
        //        txtA_conexion_rehab.Text = dt.Rows[0]["a_conexion_rehab"].ToString();
        //        txtA_impulsion.Text = dt.Rows[0]["a_impulsion"].ToString();
        //        txtA_estacion.Text = dt.Rows[0]["a_estacion"].ToString();

        //        txtS_efluente.Text = dt.Rows[0]["s_efluente"].ToString();
        //        txtS_camara.Text = dt.Rows[0]["s_camara"].ToString();
        //        txtS_emisor.Text = dt.Rows[0]["s_emisor"].ToString();
        //        txtS_planta.Text = dt.Rows[0]["s_planta"].ToString();
        //        txtS_conexion_nueva.Text = dt.Rows[0]["s_conexion_nueva"].ToString();
        //        txtS_conexion_rehab.Text = dt.Rows[0]["s_conexion_rehab"].ToString();
        //        txtS_conexion_unid.Text = dt.Rows[0]["s_conexion_unid"].ToString();
        //        txtS_Impulsion.Text = dt.Rows[0]["s_impulsion"].ToString();
        //        txtS_redes.Text = dt.Rows[0]["s_redes"].ToString();

        //        txtD_tuberia.Text = dt.Rows[0]["d_tuberia"].ToString();
        //        txtD_cuneta.Text = dt.Rows[0]["d_cuneta"].ToString();
        //        txtD_tormenta.Text = dt.Rows[0]["d_tormenta"].ToString();
        //        txtD_conexion.Text = dt.Rows[0]["d_conexion"].ToString();
        //        txtD_inspeccion.Text = dt.Rows[0]["d_inspeccion"].ToString();
        //        txtD_secundario.Text = dt.Rows[0]["d_secundarios"].ToString();
        //        txtD_principal.Text = dt.Rows[0]["d_principal"].ToString();

        //        txtA_captacion_p.Text = dt.Rows[0]["a_captacion_p"].ToString();//
        //        txtA_conduccion_p.Text = dt.Rows[0]["a_conduccion_p"].ToString();//
        //        txtA_trata_p.Text = dt.Rows[0]["a_planta_p"].ToString();//
        //        txtA_reservorio_p.Text = dt.Rows[0]["a_reservorio_p"].ToString();//
        //        txtA_aduccion_p.Text = dt.Rows[0]["a_aduccion_p"].ToString();
        //        txtA_redes_p.Text = dt.Rows[0]["a_redes_p"].ToString();
        //        txtA_conexion_nueva_p.Text = dt.Rows[0]["a_conexion_nueva_p"].ToString();
        //        txtA_conexion_piletas_p.Text = dt.Rows[0]["a_conexion_piletas_p"].ToString();
        //        txtA_conexion_rehab_p.Text = dt.Rows[0]["a_conexion_rehab_p"].ToString();
        //        txtA_impulsion_p.Text = dt.Rows[0]["a_impulsion_p"].ToString();
        //        txtA_estacion_p.Text = dt.Rows[0]["a_estacion_p"].ToString();

        //        txtS_efluente_p.Text = dt.Rows[0]["s_linea_p"].ToString();
        //        txtS_camara_p.Text = dt.Rows[0]["s_camara_p"].ToString();
        //        txtS_emisor_p.Text = dt.Rows[0]["s_emisor_p"].ToString();
        //        txtS_planta_p.Text = dt.Rows[0]["s_planta_p"].ToString();
        //        txtS_conexion_nueva_p.Text = dt.Rows[0]["s_conexion_nueva_p"].ToString();
        //        txtS_conexion_rehab_p.Text = dt.Rows[0]["s_conexion_rehab_p"].ToString();
        //        txtS_conexion_unid_p.Text = dt.Rows[0]["s_conexion_unid_p"].ToString();
        //        txtS_Impulsion_p.Text = dt.Rows[0]["s_impulsion_p"].ToString();
        //        txtS_redes_p.Text = dt.Rows[0]["s_redes_p"].ToString();

        //        txtD_tuberia_p.Text = dt.Rows[0]["d_tuberia_p"].ToString();
        //        txtD_cuneta_p.Text = dt.Rows[0]["d_cuneta_p"].ToString();
        //        txtD_tormenta_p.Text = dt.Rows[0]["d_boca_p"].ToString();
        //        txtD_conexion_p.Text = dt.Rows[0]["d_conexion_p"].ToString();
        //        txtD_inspeccion_p.Text = dt.Rows[0]["d_inpeccion_p"].ToString();
        //        txtD_secundario_p.Text = dt.Rows[0]["d_secundarios_p"].ToString();
        //        txtD_principal_p.Text = dt.Rows[0]["d_principal_p"].ToString();

        //        lblUsuarioComponentesPNSU.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
        //        UpdatePanel2.Update();
        //    }
        //    else
        //    {
        //        lblUsuarioComponentesPNSU.Text = ".";
        //        UpdatePanel2.Update();
        //        LlenarPNSU();
        //    }


        //}


        //protected void btnGuardarComponentesPNSU_OnClick(object sender, EventArgs e)
        //{
        //    _BEVarPNSU.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //    _BEVarPNSU.TipoFinanciamiento = 3;
        //    _BEVarPNSU.A_captacion = Convert.ToInt32(txtA_captacion.Text);
        //    _BEVarPNSU.A_conduccion = Convert.ToInt32(txtA_conduccion.Text);
        //    _BEVarPNSU.A_trata = Convert.ToInt32(txtA_trata.Text);
        //    _BEVarPNSU.A_reservorio = Convert.ToInt32(txtA_reservorio.Text);
        //    _BEVarPNSU.A_Aduccion = Convert.ToInt32(txtA_aduccion.Text);
        //    _BEVarPNSU.A_redes = Convert.ToInt32(txtA_redes.Text);
        //    _BEVarPNSU.A_conexion_nueva = Convert.ToInt32(txtA_conexion_nueva.Text);
        //    _BEVarPNSU.A_conexion_piletas = Convert.ToInt32(txtA_conexion_piletas.Text);
        //    _BEVarPNSU.A_conexion_rehab = Convert.ToInt32(txtA_conexion_rehab.Text);
        //    _BEVarPNSU.A_impulsion = Convert.ToInt32(txtA_impulsion.Text);
        //    _BEVarPNSU.A_estacion = Convert.ToInt32(txtA_estacion.Text);

        //    _BEVarPNSU.S_efluente = Convert.ToInt32(txtS_efluente.Text);
        //    _BEVarPNSU.S_camara = Convert.ToInt32(txtS_camara.Text);
        //    _BEVarPNSU.S_emisor = Convert.ToInt32(txtS_emisor.Text);
        //    _BEVarPNSU.S_planta = Convert.ToInt32(txtS_planta.Text);
        //    _BEVarPNSU.S_conexion_nueva = Convert.ToInt32(txtS_conexion_nueva.Text);
        //    _BEVarPNSU.S_conexion_rehab = Convert.ToInt32(txtS_conexion_rehab.Text);
        //    _BEVarPNSU.S_conexion_unid = Convert.ToInt32(txtS_conexion_unid.Text);
        //    _BEVarPNSU.S_redes = Convert.ToInt32(txtS_redes.Text);
        //    _BEVarPNSU.S_impulsion = Convert.ToInt32(txtS_Impulsion.Text);

        //    _BEVarPNSU.D_tuberia = Convert.ToInt32(txtD_tuberia.Text);
        //    _BEVarPNSU.D_cuneta = Convert.ToInt32(txtD_cuneta.Text);
        //    _BEVarPNSU.D_tormenta = Convert.ToInt32(txtD_tormenta.Text);
        //    _BEVarPNSU.D_conexion = Convert.ToInt32(txtD_conexion.Text);
        //    _BEVarPNSU.D_inspeccion = Convert.ToInt32(txtD_inspeccion.Text);
        //    _BEVarPNSU.D_secundario = Convert.ToInt32(txtD_secundario.Text);
        //    _BEVarPNSU.D_principal = Convert.ToInt32(txtD_principal.Text);

        //    _BEVarPNSU.A_captacion_p = Convert.ToInt32(txtA_captacion_p.Text);
        //    _BEVarPNSU.A_conduccion_p = Convert.ToInt32(txtA_conduccion_p.Text);
        //    _BEVarPNSU.A_trata_p = Convert.ToInt32(txtA_trata_p.Text);
        //    _BEVarPNSU.A_reservorio_p = Convert.ToInt32(txtA_reservorio_p.Text);
        //    _BEVarPNSU.A_Aduccion_p = Convert.ToInt32(txtA_aduccion_p.Text);
        //    _BEVarPNSU.A_redes_p = Convert.ToInt32(txtA_redes_p.Text);
        //    _BEVarPNSU.A_conexion_nueva_p = Convert.ToInt32(txtA_conexion_nueva_p.Text);
        //    _BEVarPNSU.A_conexion_piletas_p = Convert.ToInt32(txtA_conexion_piletas_p.Text);
        //    _BEVarPNSU.A_conexion_rehab_p = Convert.ToInt32(txtA_conexion_rehab_p.Text);
        //    _BEVarPNSU.A_impulsion_p = Convert.ToInt32(txtA_impulsion_p.Text);
        //    _BEVarPNSU.A_estacion_p = Convert.ToInt32(txtA_estacion_p.Text);

        //    _BEVarPNSU.S_efluente_p = Convert.ToInt32(txtS_efluente_p.Text);
        //    _BEVarPNSU.S_camara_p = Convert.ToInt32(txtS_camara_p.Text);
        //    _BEVarPNSU.S_emisor_p = Convert.ToInt32(txtS_emisor_p.Text);
        //    _BEVarPNSU.S_planta_p = Convert.ToInt32(txtS_planta_p.Text);
        //    _BEVarPNSU.S_conexion_nueva_p = Convert.ToInt32(txtS_conexion_nueva_p.Text);
        //    _BEVarPNSU.S_conexion_rehab_p = Convert.ToInt32(txtS_conexion_rehab_p.Text);
        //    _BEVarPNSU.S_conexion_unid_p = Convert.ToInt32(txtS_conexion_unid_p.Text);
        //    _BEVarPNSU.S_redes_p = Convert.ToInt32(txtS_redes_p.Text);
        //    _BEVarPNSU.S_impulsion_p = Convert.ToInt32(txtS_Impulsion_p.Text);

        //    _BEVarPNSU.D_tuberia_p = Convert.ToInt32(txtD_tuberia_p.Text);
        //    _BEVarPNSU.D_cuneta_p = Convert.ToInt32(txtD_cuneta_p.Text);
        //    _BEVarPNSU.D_tormenta_p = Convert.ToInt32(txtD_tormenta_p.Text);
        //    _BEVarPNSU.D_conexion_p = Convert.ToInt32(txtD_conexion_p.Text);
        //    _BEVarPNSU.D_inspeccion_p = Convert.ToInt32(txtD_inspeccion_p.Text);
        //    _BEVarPNSU.D_secundario_p = Convert.ToInt32(txtD_secundario_p.Text);
        //    _BEVarPNSU.D_principal_p = Convert.ToInt32(txtD_principal_p.Text);

        //    _BEVarPNSU.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //    int val = _objBLEjecucion.spi_MON_ComponentesPNSU(_BEVarPNSU);

        //    if (val == 1)
        //    {
        //        string script = "<script>alert('Se registró Correctamente.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        VarPnsu();
        //        cargaComponentesPNSU();
        //        Up_Componente.Update();

        //    }
        //    else
        //    {
        //        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    }

        //}


        #endregion

        #region Tab3

        protected void CargaTab3()
        {
            cargaTipoReceptora();
            CargaLiquidacion();

            //txtTotalTransferencia.Text = txtAcumuladorTranferenciaTab0.Text;


        }

        protected Boolean validaLiquidacionTab3()
        {
            if (txtFechaTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de Fecha de Liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    return false;
                }

                if (Convert.ToDateTime(txtFechaTab3.Text) > DateTime.Now.Date)
                {
                    string script = "<script>alert('La Fecha de Liquidación no debe ser mayor a la actual.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

                if (txtFechaRecepcionTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaTab3.Text) < Convert.ToDateTime(txtFechaRecepcionTab2.Text))
                {
                    string script = "<script>alert('La fecha de liquidación debe ser mayor  a la fecha de recepción.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return false;
                }

            }

            return true;
        }
        protected void CargaLiquidacion()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BELiquidacion.tipoFinanciamiento = 3;

            dt = _objBLLiquidacion.sp_MON_Liquidacion(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                txtLiquidacionTab3.Text = dt.Rows[0]["resolucion"].ToString();
                txtFechaTab3.Text = dt.Rows[0]["fecha"].ToString();
                txtMontoTab3.Text = dt.Rows[0]["montoLiquidacion"].ToString();
                txtSaldoTab3.Text = dt.Rows[0]["saldoDevolver"].ToString();
                LnkbtnLiquidacionTab3.Text = dt.Rows[0]["urlLiquidacion"].ToString();
                LnkbtnActaTab3.Text = dt.Rows[0]["urlActaCierre"].ToString();

                ddlflagCapacitadTab3.SelectedValue = dt.Rows[0]["flagCapacidadAdministrar"].ToString();

                //string flagIntra = dt.Rows[0]["flagConexionIntradomiciliaria"].ToString();
                //if (flagIntra == "1") { rbIntradomiciliariaTab3.SelectedValue = "1"; }
                //if (flagIntra == "0") { rbIntradomiciliariaTab3.SelectedValue = "0"; }

                //string flagedu = dt.Rows[0]["flagEducacionSanitaria"].ToString();
                //if (flagedu == "1") { rbEducacionTab3.SelectedValue = "1"; }
                //if (flagedu == "0") { rbEducacionTab3.SelectedValue = "0"; }



                // txtAdicionalTab3.Text = dt.Rows[0]["montoAdicional"].ToString();
                //txtOtrosMontosTab3.Text = dt.Rows[0]["otrosMontos"].ToString();
                LnkbtnDevolucionTab3.Text = dt.Rows[0]["urlDocDevolucion"].ToString();

                txtResolucionLiqSupervision.Text = dt.Rows[0]["resolucionSupervision"].ToString();
                txtFechaLiqSupervision.Text = dt.Rows[0]["fechaLiqSupervision"].ToString();
                txtMontoLiqSupervision.Text = dt.Rows[0]["montoLiqSupervision"].ToString();
                txtResolucionLiqObraComplem.Text = dt.Rows[0]["resolucionObraComplementaria"].ToString();
                txtFechaLiqObraComplem.Text = dt.Rows[0]["fechaLiqObraComplem"].ToString();
                txtMontoLiqObraComplem.Text = dt.Rows[0]["montoLiqObraComplementaria"].ToString();

                lnkbtnLiqObraComplemTab3.Text = dt.Rows[0]["urlLiquidacionObraComplem"].ToString();
                lnkbtnLiqSupervisionTab3.Text = dt.Rows[0]["urlLiquidacionSupervision"].ToString();


                lblNomLiquidacion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                double val1 = 0;
                if (txtMontoTab3.Text != "")
                { val1 = Convert.ToDouble(txtMontoTab3.Text); }

                double val2 = 0;
                if (txtMontoLiqSupervision.Text != "")
                { val2 = Convert.ToDouble(txtMontoLiqSupervision.Text); }

                double val3 = 0;
                if (txtSaldoTab3.Text != "")
                { val3 = Convert.ToDouble(txtSaldoTab3.Text); }

                double val4 = 0;
                if (txtMontoLiqObraComplem.Text != "")
                { val4 = Convert.ToDouble(txtMontoLiqObraComplem.Text); }

                txtTotalTab3.Text = (val1 + val2 + val3 + val4).ToString("N");

                GeneraIcoFile(lnkbtnLiqObraComplemTab3.Text, imgbtnLiqObraComplemTab3);
                GeneraIcoFile(lnkbtnLiqSupervisionTab3.Text, imgbtnLiqSupervisionTab3);
                GeneraIcoFile(LnkbtnDevolucionTab3.Text, imgbtnDevolucionTab3);
                GeneraIcoFile(LnkbtnLiquidacionTab3.Text, imgbtnLiquidacionTab3);
                GeneraIcoFile(LnkbtnActaTab3.Text, imgbtnActaTab3);

                //SOSTENIBILIDAD
                ddlTipoReceptoraTab3.SelectedValue = dt.Rows[0]["id_tipoReceptora"].ToString();
                txtUnidadReceptoraTab3.Text = dt.Rows[0]["UnidadReceptora"].ToString();
                ddlflagCapacitadTab3.SelectedValue = dt.Rows[0]["flagCapacidadAdministrar"].ToString();
                ddlOperatividadTab3.SelectedValue = dt.Rows[0]["iFlagOperativo"].ToString();
                txtFechaActaTransferenciaTab3.Text = dt.Rows[0]["fechaFinalEntregaObraOperacion"].ToString();
                LnkbtnSostenibilidadTab3.Text = dt.Rows[0]["urlDocSostenibilidad"].ToString();
                GeneraIcoFile(LnkbtnSostenibilidadTab3.Text, imgbtnSostenibilidadTab3);
                txtComentarioTab3.Text = dt.Rows[0]["comentarioSostenibilidad"].ToString();


            }

        }

        protected void cargaTipoReceptora()
        {
            ddlTipoReceptoraTab3.DataSource = _objBLLiquidacion.F_spMON_TipoReceptora();
            ddlTipoReceptoraTab3.DataTextField = "nombre";
            ddlTipoReceptoraTab3.DataValueField = "valor";
            ddlTipoReceptoraTab3.DataBind();

            ddlTipoReceptoraTab3.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }



        protected void btnGuardarTab3_OnClick(object sender, EventArgs e)
        {
            if (validaLiquidacionTab3())
            {
                if (validaArchivo(FileUploadLiqSupervisionTab3))
                {
                    if (validaArchivo(FileUploadLiqObraComplemTab3))
                    {
                        if (validaArchivo(FileUploadLiquidacionTab3))
                        {
                            if (validaArchivo(FileUploadActaTab3))
                            {
                                if (validaArchivo(FileUploadSostenibilidadTab3))
                                {

                                    _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                                    _BELiquidacion.tipoFinanciamiento = 3;

                                    _BELiquidacion.resolucion = txtLiquidacionTab3.Text;
                                    _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaTab3.Text);
                                    _BELiquidacion.monto = txtMontoTab3.Text;
                                    _BELiquidacion.saldoDevolver = txtSaldoTab3.Text;
                                    //_BELiquidacion.flagCapacidad = ddlflagCapacitadTab3.SelectedValue;
                                    //_BELiquidacion.comentarioCapacidad = txtComentarioTab3.Text;
                                    //_BELiquidacion.unidadReceptora = txtUnidadReceptoraTab3.Text;

                                    _BELiquidacion.ResolucionSupervision = txtResolucionLiqSupervision.Text;
                                    _BELiquidacion.Date_fechaLiqSupervision = VerificaFecha(txtFechaLiqSupervision.Text);
                                    _BELiquidacion.MontoLiqSupervision = txtMontoLiqSupervision.Text;

                                    _BELiquidacion.ResolucionObraComplementaria = txtResolucionLiqObraComplem.Text;
                                    _BELiquidacion.Date_fechaLiqObraComplem = VerificaFecha(txtFechaLiqObraComplem.Text);
                                    _BELiquidacion.MontoLiqObraComplementaria = txtMontoLiqObraComplem.Text;


                                    if (FileUploadLiqSupervisionTab3.HasFile)
                                    {
                                        _BELiquidacion.UrlLiquidacionSupervision = _Metodo.uploadfile(FileUploadLiqSupervisionTab3);
                                    }
                                    else
                                    { _BELiquidacion.UrlLiquidacionSupervision = lnkbtnLiqSupervisionTab3.Text; }

                                    if (FileUploadLiqObraComplemTab3.HasFile)
                                    {
                                        _BELiquidacion.UrlLiquidacionObraComplem = _Metodo.uploadfile(FileUploadLiqObraComplemTab3);
                                    }
                                    else
                                    { _BELiquidacion.UrlLiquidacionObraComplem = lnkbtnLiqObraComplemTab3.Text; }


                                    if (FileUploadLiquidacionTab3.HasFile)
                                    {
                                        _BELiquidacion.urlLiquidacion = _Metodo.uploadfile(FileUploadLiquidacionTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlLiquidacion = LnkbtnLiquidacionTab3.Text; }

                                    if (FileUploadActaTab3.HasFile)
                                    {
                                        _BELiquidacion.urlActaCierre = _Metodo.uploadfile(FileUploadActaTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlActaCierre = LnkbtnActaTab3.Text; }

                                    if (FileUploadDevolucionTab3.HasFile)
                                    {
                                        _BELiquidacion.urlDocDevolucion = _Metodo.uploadfile(FileUploadDevolucionTab3);
                                    }
                                    else
                                    { _BELiquidacion.urlDocDevolucion = LnkbtnDevolucionTab3.Text; }


                                    _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                                    int val = _objBLLiquidacion.spi_MON_Liquidacion(_BELiquidacion);


                                    if (val == 1)
                                    {
                                        string script = "<script>alert('Se registró correctamente.');</script>";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                        CargaLiquidacion();

                                    }
                                    else
                                    {
                                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                    }


                                    Up_Tab3.Update();

                                }
                            }

                        }

                    }
                }
            }
        }

        protected void LnkbtnLiquidacion_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnLiquidacionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnSostenibilidadTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnSostenibilidadTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnSostenibilidadTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnSostenibilidadTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
                Response.End();
            }

        }

        //protected void imgbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        //{


        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
        //    FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
        //        Response.End();
        //    }

        //}

        protected void txtMontoTab3_OnClick(object sender, EventArgs e)
        {
            double val1 = 0;
            if (txtMontoTab3.Text != "")
            { val1 = Convert.ToDouble(txtMontoTab3.Text); }
            txtMontoTab3.Text = val1.ToString("N");

            double val2 = 0;
            if (txtMontoLiqSupervision.Text != "")
            { val2 = Convert.ToDouble(txtMontoLiqSupervision.Text); }
            txtMontoLiqSupervision.Text = val2.ToString("N");

            double val3 = 0;
            if (txtSaldoTab3.Text != "")
            { val3 = Convert.ToDouble(txtSaldoTab3.Text); }
            txtSaldoTab3.Text = val3.ToString("N");

            double val4 = 0;
            if (txtMontoLiqObraComplem.Text != "")
            { val4 = Convert.ToDouble(txtMontoLiqObraComplem.Text); }
            txtMontoLiqObraComplem.Text = val4.ToString("N");

            txtTotalTab3.Text = (val1 + val2 + val3 + val4).ToString("N");

            Up_Tab3.Update();
        }




        protected void imgbtnLiqObraComplemTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnLiqObraComplemTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnLiqObraComplemTab3.Text);
                Response.End();
            }

        }


        protected void imgbtnLiqSupervisionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnLiqSupervisionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnLiqSupervisionTab3.Text);
                Response.End();
            }

        }

        #endregion

        #region Tab4
        protected void CargaTab4()
        {
            //cargaParalizacionTab4();
            if (!this.IsPostBack)
            {
                grdPlazoTab4.PageIndex = Int32.MaxValue;

            }
            cargaPlazoTAb4();
            cargaPersupuestoTab4();
            cargaDeductivoTab4();
        }

        protected void cargaPlazoTAb4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 1;

            DataTable dtPlazo = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPlazoTab4.DataSource = dtPlazo;
            grdPlazoTab4.DataBind();

            lblTotalDias.Text = (dtPlazo.AsEnumerable().Sum(x => x.Field<Int32>("ampliacionPlazo"))).ToString();


        }

        protected void grdPlazoTab4_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPlaTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void cargaPersupuestoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 2;

            DataTable dtAmpliacion = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPresupuestoTab4.DataSource = dtAmpliacion;
            grdPresupuestoTab4.DataBind();

            lblPresupuestoAdicional.Text = (dtAmpliacion.AsEnumerable().Sum(x => x.Field<Decimal>("montoPresup"))).ToString("N");

        }

        protected void btnModificarPlazoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPlazoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Ampliacion_Plazos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;


                _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);



                if (FileUploadPlaTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPlaTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtAmpPlazoPlaTab4.Text = "";
                    txtResolAproPlaTab4.Text = "";
                    txtFechaResolPlaTab4.Text = "";
                    txtConceptoPlaTab4.Text = "";
                    txtObservacionPlaTab4.Text = "";
                    txtFechaInicio.Text = "";
                    txtFechaFin.Text = "";
                    LnkbtnPlaTab4.Text = "";
                    imgbtnPlaTab4.Visible = false;
                    imgAgregaPlazoTab4.Visible = true;

                    lblTotalDias.Text = "0";
                    //    cargaPlazoTAb4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PlazoTab4.Visible = false;
                cargaPlazoTAb4();
                ActualizaFechasTab1();
                btnModificarPlazoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void btnModificarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPresupuestoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Adicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;
                _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                _BEAmpliacion.observacion = txtObsPreTab4.Text;



                if (FileUploadPreTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPreTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);



                    txtMontoPreTab4.Text = "";
                    txtVincuPreTab4.Text = "";
                    txtResolAproPreTab4.Text = "";
                    txtFechaPreTab4.Text = "";
                    txtIncidenciaPreTab4.Text = "";
                    LnkbtnPreTab4.Text = "";
                    txtConceptoPreTab4.Text = "";
                    txtObsPreTab4.Text = "";
                    imgbtnPreTab4.Visible = false;
                    imbtnAgregarPresupuestoTab4.Visible = true;

                    cargaPersupuestoTab4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PresupuestoTab4.Visible = false;
                cargaPersupuestoTab4();
                btnModificarPresupuestoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void btnModificarDeductivoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarDeductivoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Deductivo.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                _BEAmpliacion.observacion = txtObsDedTab4.Text;

                if (FileUploadDedTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnDedTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);




                    txtMontoDedTab4.Text = "";
                    txtVincuDedTab4.Text = "";
                    txtResolAproDedTab4.Text = "";
                    txtFechaDedTab4.Text = "";
                    txtIncidenciaDedTab4.Text = "";

                    txtConceptoDedTab4.Text = "";
                    txtObsDedTab4.Text = "";
                    LnkbtnDedTab4.Text = "";
                    imgbtnDedTab4.Visible = false;
                    imgbtnDeductivoTab4.Visible = true;
                    cargaDeductivoTab4();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_deductivoTab4.Visible = false;
                cargaDeductivoTab4();
                btnModificarDeductivoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }



        protected void grdPresupuestoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Double TotalDias;
            //TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPreTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }


        }
        protected void cargaDeductivoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 3;

            DataTable dtDeductivo = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdDeductivoTab4.DataSource = dtDeductivo;
            grdDeductivoTab4.DataBind();


            lblDeductivoTotal.Text = (dtDeductivo.AsEnumerable().Sum(x => x.Field<Decimal>("montoPresup"))).ToString("N");


        }
        protected void grdDeductivoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocDedTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void imgAgregaPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            btnGuardarPlazoTab4.Visible = true;
            btnModificarPlazoTab4.Visible = false;
            lblNomUsuarioAmpDias.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = false;
            imgAgregaPlazoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPlazoTab4()
        {
            Boolean result;
            result = true;

            if (txtAmpPlazoPlaTab4.Text == "")
            {
                string script = "<script>alert('Ingrese dias de Ampliación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtResolAproPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaResolPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPlazoTab4())
            {

                if (validaArchivo(FileUploadPlaTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 1;

                    _BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        lblTotalDias.Text = "0";
                        cargaPlazoTAb4();
                        ActualizaFechasTab1();
                        txtAmpPlazoPlaTab4.Text = "";
                        txtResolAproPlaTab4.Text = "";
                        txtFechaResolPlaTab4.Text = "";
                        txtConceptoPlaTab4.Text = "";
                        txtObservacionPlaTab4.Text = "";
                        txtFechaInicio.Text = "";
                        txtFechaFin.Text = "";

                        Panel_PlazoTab4.Visible = false;
                        imgAgregaPlazoTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPlaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPlazoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPlazoTab4.Rows[row.RowIndex].FindControl("imgDocPlaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imbtnAgregarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            lblNomUsuarioPresupuesto.Text = "";
            btnGuardarPresupuestoTab4.Visible = true;
            btnModificarPresupuestoTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void btnCancelarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPresupuestoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPresupuestoTab4())
            {

                if (validaArchivo(FileUploadPreTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 2;

                    _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;

                    _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                    _BEAmpliacion.observacion = txtObsPreTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);


                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaPersupuestoTab4();

                        txtMontoPreTab4.Text = "";
                        txtVincuPreTab4.Text = "";
                        txtResolAproPreTab4.Text = "";
                        txtFechaPreTab4.Text = "";
                        txtIncidenciaPreTab4.Text = "";

                        txtConceptoPreTab4.Text = "";
                        txtObsPreTab4.Text = "";

                        Panel_PresupuestoTab4.Visible = false;
                        imbtnAgregarPresupuestoTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPreTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPresupuestoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPresupuestoTab4.Rows[row.RowIndex].FindControl("imgDocPreTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            btnGuardarDeductivoTab4.Visible = true;
            btnModificarDeductivoTab4.Visible = false;
            lblNomUsuarioDeductivo.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarDeductivoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarDeductivoTab4())
            {

                if (validaArchivo(FileUploadDedTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 3;

                    _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                    _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                    _BEAmpliacion.observacion = txtObsDedTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaDeductivoTab4();

                        txtMontoDedTab4.Text = "";
                        txtVincuDedTab4.Text = "";
                        txtResolAproDedTab4.Text = "";
                        txtFechaDedTab4.Text = "";
                        txtIncidenciaDedTab4.Text = "";

                        txtConceptoDedTab4.Text = "";
                        txtObsDedTab4.Text = "";

                        Panel_deductivoTab4.Visible = false;
                        imgbtnDeductivoTab4.Visible = true;

                        Up_Tab4.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocDedTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdDeductivoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdDeductivoTab4.Rows[row.RowIndex].FindControl("imgDocDedTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }




        protected void imgbtnAgregarFianzasTab4_OnClick(object sender, EventArgs e)
        {
            Panel_fianzas.Visible = true;
            imgbtnAgregarFianzasTab4.Visible = false;
            btnGuardarFianzasTab2.Visible = true;
            lblNomUsuarioCarta.Text = "";
            btnModificarFianzasTab2.Visible = false;
            imgbtnCartaTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnAccionTab2.Text = "";

            lblTitDocAccion.Text = "";
            FileUploadAccionTab2.Visible = false;
            imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
            lnkbtnAccionTab2.Text = "";

            Up_Tab4.Update();
        }



        protected void imgbtnPlaTabb4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPlaTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnPreTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPreTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void LnkbtnDedTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnDedTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }



        protected void grdPlazoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPlazoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    lblTotalDias.Text = "0";
                    cargaPlazoTAb4();
                    ActualizaFechasTab1();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPresupuestoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPresupuestoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaPersupuestoTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdDeductivoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdDeductivoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaDeductivoTab4();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPlazoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPlazoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Ampliacion_Plazos.Text = id_ampliacion.ToString();
            GridViewRow row = grdPlazoTab4.SelectedRow;

            txtAmpPlazoPlaTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            txtResolAproPlaTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolPlaTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoPlaTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionPlaTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            LnkbtnPlaTab4.Text = ((ImageButton)row.FindControl("imgDocPlaTab4")).ToolTip;
            lblNomUsuarioAmpDias.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicio.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFin.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(LnkbtnPlaTab4.Text, imgbtnPlaTab4);

            btnGuardarPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            Panel_PlazoTab4.Visible = true;
            Up_Tab4.Update();


        }

        protected void grdPresupuestoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPresupuestoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Adicional.Text = id_ampliacion.ToString();
            GridViewRow row = grdPresupuestoTab4.SelectedRow;

            txtMontoPreTab4.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtVincuPreTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproPreTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaPreTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaPreTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;
            txtConceptoPreTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsPreTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioPresupuesto.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            LnkbtnPreTab4.Text = ((ImageButton)row.FindControl("imgDocPreTab4")).ToolTip;

            GeneraIcoFile(LnkbtnPreTab4.Text, imgbtnPreTab4);


            btnGuardarPresupuestoTab4.Visible = false;
            btnModificarPresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            Panel_PresupuestoTab4.Visible = true;

            Up_Tab4.Update();


        }

        protected void grdDeductivoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdDeductivoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Deductivo.Text = id_ampliacion.ToString();
            GridViewRow row = grdDeductivoTab4.SelectedRow;

            txtMontoDedTab4.Text = Server.HtmlDecode(row.Cells[2].Text);
            txtVincuDedTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproDedTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaDedTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaDedTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;

            txtConceptoDedTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsDedTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioDeductivo.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            LnkbtnDedTab4.Text = ((ImageButton)row.FindControl("imgDocDedTab4")).ToolTip;

            GeneraIcoFile(LnkbtnDedTab4.Text, imgbtnDedTab4);


            btnGuardarDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            Panel_deductivoTab4.Visible = true;

            Up_Tab4.Update();


        }

        protected void cargaParalizacionTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 6;

            grdParalizacionTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdParalizacionTab4.DataBind();

        }


        protected void cargaAccionTab4()
        {
            //jc
            _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_ParalizacionTab4.Text);
            grdAccionesTab4.DataSource = _objBLAmpliacion.spMON_T_Detalle_Paralizacion(_BEAmpliacion);
            grdAccionesTab4.DataBind();


        }



        protected void grdParalizacionTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocParalizacionTab4");
                ImageButton imbInf = (ImageButton)e.Row.FindControl("imgInfParalizacionTab4");
                ImageButton imbOfic = (ImageButton)e.Row.FindControl("imgOficParalizacionTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                Label lblDetalle = (Label)e.Row.FindControl("lblObservacionesTab4");

                Label lblid_ampliacion = (Label)e.Row.FindControl("lblid_ampliacion");

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblid_ampliacion.Text);
                DataTable dt = _objBLAmpliacion.spMON_T_Detalle_Paralizacion(_BEAmpliacion);

                string tabla = "";
                if (dt.Rows.Count > 0)
                {
                    string html = "<table style='border:1px solid #28375B;border-spacing: 2px;border-color: grey;'>";

                    foreach (DataRow dr in dt.Rows)
                    {
                        string imgDA = "";
                        if (dr["flagAyuda"].ToString() == "1")
                        {
                            imgDA = "<IMG SRC='../img/vb.png'>";
                        }
                        else
                        {
                            imgDA = "";


                        }

                        html += "<tr> <td style='border:1px solid #28375B;border-spacing: 2px;border-color: grey;' > " + dr["fecha"].ToString() + " </td><td style='border:1px solid #28375B;border-spacing: 2px;border-color: grey;'> " + dr["accion"].ToString() + " </td><td  style='border:1px solid #28375B;border-spacing: 2px;border-color: grey;'> " + imgDA + " </td></tr>";

                    }

                    html += "</table>";
                    tabla = html;
                }


                lblDetalle.Text = tabla;

                TotalDias = Convert.ToInt32(txtTotalParalizacionTab4.Text) + Convert.ToInt32(dias.Text);
                txtTotalParalizacionTab4.Text = TotalDias.ToString();

                GeneraIcoFile(imb.ToolTip, imb);
                GeneraIcoFile(imbInf.ToolTip, imbInf);
                GeneraIcoFile(imbOfic.ToolTip, imbOfic);
            }
        }

        protected void grdParalizacionTab4_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_ParalizacionTab4.Text = grdParalizacionTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdParalizacionTab4.SelectedRow;

            txtDiasParalizadosTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;

            txtResoluciónParalizacionTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolucionParalizacionTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoParalizacionTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionParalizacionTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            lnkbtnActaParalizacionTab4.Text = ((ImageButton)row.FindControl("imgDocParalizacionTab4")).ToolTip;
            lnkbtnInformeParalizacionTab4.Text = ((ImageButton)row.FindControl("imgInfParalizacionTab4")).ToolTip;
            lnkbtnoficioParalizacionTab4.Text = ((ImageButton)row.FindControl("imgOficParalizacionTab4")).ToolTip;

            lblNomActualizaParalizacionTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicioParalizacionTab4.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFinParalizacionTab4.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(lnkbtnActaParalizacionTab4.Text, imgbtnActaParalizacionTab4);
            GeneraIcoFile(lnkbtnInformeParalizacionTab4.Text, imgbtnInformeParalizacionTab4);
            GeneraIcoFile(lnkbtnoficioParalizacionTab4.Text, imgbtnOficioParalizacionTab4);

            btnGuardarParalizacionTab4.Visible = false;
            btnModificarParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;

            Panel_ParalizacionTab4.Visible = true;

            trDiasParalizados.Visible = true;

            Up_Tab4.Update();
        }

        protected void grdParalizacionTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdParalizacionTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaParalizacionTab4();
                    ActualizaFechasTab1();
                    Up_Tab2.Update();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }



        protected Boolean ValidarParalizacionTab4()
        {
            Boolean result;
            result = true;

            //if (txtDiasParalizadosTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese dias paralizados.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtFechaInicioParalizacionTab4.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaInicioParalizacionTab4.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }



            if (txtFechaFinParalizacionTab4.Text == "")
            {
                string script = "<script>alert('Ingrese fecha fin de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaFinParalizacionTab4.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha fin de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            //if (txtFechaResolPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {

                if (validaArchivo(FileUpload_Acta_ParalizacionTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 6;

                    //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                    _BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text = ((Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days).ToString();
                    _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_Acta_ParalizacionTab4);
                    _BEAmpliacion.urlInforme = _Metodo.uploadfile(FileUpload_Informe_ParalizacionTab4);
                    _BEAmpliacion.urlOficio = _Metodo.uploadfile(FileUpload_Oficio_ParalizacionTab4);

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);
                    _BEAmpliacion.urlInforme = _Metodo.uploadfile(FileUpload_Informe_ParalizacionTab4);
                    _BEAmpliacion.urlOficio = _Metodo.uploadfile(FileUpload_Oficio_ParalizacionTab4);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        txtTotalParalizacionTab4.Text = "0";
                        cargaParalizacionTab4();
                        //ActualizaFechasTab1();
                        //txtAmpPlazoPlaTab4.Text = "";
                        //txtResolAproPlaTab4.Text = "";
                        //txtFechaResolPlaTab4.Text = "";
                        //txtConceptoPlaTab4.Text = "";
                        //txtObservacionPlaTab4.Text = "";
                        //txtFechaInicio.Text = "";
                        //txtFechaFin.Text = "";

                        Panel_ParalizacionTab4.Visible = false;
                        imgbtnAgregarParalizacionTAb4.Visible = true;

                        Up_Tab4.Update();

                        //cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }

        protected void btnModificarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_Acta_ParalizacionTab4))
            {
                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_ParalizacionTab4.Text);
                //_BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                //_BEAmpliacion.tipoFinanciamiento = 3;
                //_BEAmpliacion.id_tipo_ampliacion = 6;

                //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                _BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text = ((Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days).ToString();
                _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                if (FileUpload_Acta_ParalizacionTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_Acta_ParalizacionTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = lnkbtnActaParalizacionTab4.Text;
                }

                //oficio
                if (FileUpload_Informe_ParalizacionTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlInforme = _Metodo.uploadfile(FileUpload_Informe_ParalizacionTab4);
                }
                else
                {
                    _BEAmpliacion.urlInforme = lnkbtnInformeParalizacionTab4.Text;
                }

                //informe
                if (FileUpload_Oficio_ParalizacionTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlOficio = _Metodo.uploadfile(FileUpload_Oficio_ParalizacionTab4);
                }
                else
                {
                    _BEAmpliacion.urlOficio = lnkbtnoficioParalizacionTab4.Text;
                }




                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);

                //int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_ParalizacionTab4.Visible = false;
                    imgbtnAgregarParalizacionTAb4.Visible = true;

                    txtTotalParalizacionTab4.Text = "0";
                    cargaParalizacionTab4();
                    ActualizaFechasTab1();

                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void btnCancelarParalizacionTab4_Click(object sender, EventArgs e)
        {
            Panel_ParalizacionTab4.Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = true;

            Up_Tab4.Update();
        }

        protected void imgbtnAgregarParalizacionTAb4_Click(object sender, ImageClickEventArgs e)
        {

            Panel_ParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;
            //lblNomUsuarioPresupuesto.Text = "";
            txtDiasParalizadosTab4.Text = "";
            txtResoluciónParalizacionTab4.Text = "";
            txtFechaFinParalizacionTab4.Text = "";
            txtFechaInicioParalizacionTab4.Text = "";
            txtFechaResolucionParalizacionTab4.Text = "";
            txtFechaInicioParalizacionTab4.Text = "";
            txtConceptoParalizacionTab4.Text = "";

            lnkbtnActaParalizacionTab4.Text = "";
            lnkbtnInformeParalizacionTab4.Text = "";
            lnkbtnoficioParalizacionTab4.Text = "";
            imgbtnActaParalizacionTab4.ImageUrl = "~/img/blanco.png";
            imgbtnInformeParalizacionTab4.ImageUrl = "~/img/blanco.png";
            imgbtnOficioParalizacionTab4.ImageUrl = "~/img/blanco.png";
            btnGuardarParalizacionTab4.Visible = true;
            btnModificarParalizacionTab4.Visible = false;
            Up_Tab4.Update();

            DateTime fecha1 = DateTime.Now;
            DateTime fecha2 = Convert.ToDateTime("01/02/2016");


            //limpiar el detalle
            DataTable dtAcciones = new DataTable();

            trDiasParalizados.Visible = false;

            //jc
            //grdAccionesTab4.DataSource = dtAcciones;
            //grdAccionesTab4.DataBind();
        }

        protected void imgDocParalizacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdParalizacionTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdParalizacionTab4.Rows[row.RowIndex].FindControl("imgDocParalizacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        #endregion

        #region Tab6

        protected void CargaTab6()
        {
            CargaPanelTab6();
            CargaTipoDocumentoMonitorTab6();
        }

        protected void CargaTipoDocumentoMonitorTab6()
        {
            ddlTipoArchivoTab6.DataSource = _objBLBandeja.F_spMON_TipoDocumentoMonitor();
            ddlTipoArchivoTab6.DataValueField = "valor";
            ddlTipoArchivoTab6.DataTextField = "nombre";
            ddlTipoArchivoTab6.DataBind();

            ddlTipoArchivoTab6.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void CargaPanelTab6()
        {
            _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdPanelTab6.DataSource = _objBLBandeja.F_spMON_DocumentoMonitor(_BEBandeja);
            grdPanelTab6.DataBind();
        }

        protected void imgbtnAgregarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnGuardarPanelTab6.Visible = true;
            btnModificarPanelTab6.Visible = false;
            lblNomUsuarioDocumento.Text = "";

            txtNombreArchivoTab6.Text = "";
            txtDescripcionTab6.Text = "";
            txtFechaTab6.Text = "";
            LnkbtnDocTab6.Text = "";
            ddlTipoArchivoTab6.SelectedValue = "";
            imgbtnDocTab6.ImageUrl = "~/img/blanco.png";

            Up_Tab6.Update();
        }

        protected void btnCancelarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = false;
            imgbtnAgregarPanelTab6.Visible = true;
            Up_Tab6.Update();
        }

        protected void imgDocTab6_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdPanelTab6.Rows[row.RowIndex].FindControl("imgDocTab6");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void btnGuardarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    string vUrlArchivoCargar = _Metodo.uploadfile(FileUpload_Tab6);

                    if (vUrlArchivoCargar.Length > 0)
                    {
                        _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                        _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;
                        _BEBandeja.UrlDoc = vUrlArchivoCargar;
                        _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                        _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                        _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        int val = _objBLBandeja.spi_MON_DocumentoMonitor(_BEBandeja);

                        if (val == 1)
                        {
                            string script = "<script>alert('Registro correcto.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                            CargaPanelTab6();
                            Panel_FotograficoTab6.Visible = false;
                            imgbtnAgregarPanelTab6.Visible = true;

                            txtNombreArchivoTab6.Text = "";
                            txtDescripcionTab6.Text = "";
                            ddlTipoArchivoTab6.SelectedValue = "";
                            txtFechaTab6.Text = "";
                            imgbtnDocTab6.ImageUrl = "~/img/blanco.png";
                            Up_Tab6.Update();
                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }
                    else
                    {
                        string script = "<script>alert('Error: No se pudo registrar el archivo.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        protected void grdPanelTab6_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab6");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }


        protected void grdPanelTab6_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPanelTab6.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.Id_documentoMonitor = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.tipo = "2";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);

                int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaPanelTab6();
                    Up_Tab6.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPanelTab6_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_documento_monitor.Text = grdPanelTab6.SelectedDataKey.Value.ToString();

            GridViewRow row = grdPanelTab6.SelectedRow;

            txtNombreArchivoTab6.Text = ((Label)row.FindControl("lblNombre")).Text;
            txtDescripcionTab6.Text = ((Label)row.FindControl("lblDescripcion")).Text;

            ddlTipoArchivoTab6.SelectedValue = ((Label)row.FindControl("lblIDTipo")).Text;

            txtFechaTab6.Text = ((Label)row.FindControl("lblFecha")).Text;
            LnkbtnDocTab6.Text = ((ImageButton)row.FindControl("imgDocTab6")).ToolTip;

            lblNomUsuarioDocumento.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnDocTab6.Text, imgbtnDocTab6);

            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnModificarPanelTab6.Visible = true;
            btnGuardarPanelTab6.Visible = false;
            Up_Tab6.Update();
        }

        protected Boolean validarPanelTab6()
        {
            Boolean result;
            result = true;

            if (txtNombreArchivoTab6.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaTab6.Text == "")
            {
                string script = "<script>alert('Ingresar fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (FileUpload_Tab6.HasFile == false && LnkbtnDocTab6.Text == "")
            {
                string script = "<script>alert('Adjuntar archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (ddlTipoArchivoTab6.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (ddlTipoArchivoTab6.SelectedValue.Equals("1"))
                {
                    if (validaArchivoFotos(FileUpload_Tab6) == false)
                    {
                        result = false;
                        return result;
                    }
                }
            }


            return result;


        }

        protected void btnModificarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    string vUrlArchivoCargar = _Metodo.uploadfile(FileUpload_Tab6);
                    if (FileUpload_Tab6.HasFile == false)
                    {
                        vUrlArchivoCargar = LnkbtnDocTab6.Text;
                    }

                    if (vUrlArchivoCargar.Length > 0)
                    {
                        _BEBandeja.Id_documentoMonitor = Convert.ToInt32(lblId_documento_monitor.Text);
                        _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                        _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;

                        _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                        _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                        _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        _BEBandeja.tipo = "1";

                        if (FileUpload_Tab6.HasFile)
                        {
                            _BEBandeja.UrlDoc = vUrlArchivoCargar;
                        }
                        else
                        {
                            _BEBandeja.UrlDoc = LnkbtnDocTab6.Text;
                        }


                        int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                        if (val == 1)
                        {
                            string script = "<script>alert('Se actualizó correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                            CargaPanelTab6();
                            Panel_FotograficoTab6.Visible = false;
                            imgbtnAgregarPanelTab6.Visible = true;
                            Up_Tab6.Update();
                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }
                    else
                    {
                        string script = "<script>alert('Error: No se pudo registrar el archivo.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }



        }

        protected void LnkbtnDocTab6_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkbtnDocTab6.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        #endregion

        protected void lnkbtnFichaRecepcionTab2_OnClick(object sender, EventArgs e)
        {
            //LinkButton boton;
            //boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFichaRecepcionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtnFichaRecepcionTab2_OnClick(object sender, ImageClickEventArgs e)
        {
            //ImageButton boton;
            //boton = (ImageButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFichaRecepcionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void chbFlagAyuda_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var valor = grdEvaluacionTab2.DataKeys[row.RowIndex].Value;
            //_BEEjecucion.id_tabla = Convert.ToInt32(valor.ToString());
            string FlagAyudaLectura = ((Label)row.FindControl("lblAyudaObtiene")).Text;
            Boolean FlagAyuda = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;
            Boolean result;
            if (FlagAyudaLectura == "1")
            {
                result = true;
            }
            else
            {
                result = false;
            }

            if (FlagAyuda == result)
            {
            }
            else
            {
                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(valor.ToString());
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                if (FlagAyuda == true)
                {
                    _BEEjecucion.flagAyuda = "1";
                }
                else
                {
                    _BEEjecucion.flagAyuda = "0";
                }

                int val;
                try
                {
                    val = _objBLEjecucion.spu_MON_EvaluacionRecomendacionByFlag(_BEEjecucion);
                }
                catch (Exception ex)
                {
                    val = 0;
                }

                if (val == 1)
                {
                    string script = "<script>alert('Se modificó el flag de Ayuda Memoria correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_ObservacionTab2.Visible = false;
                    imgbtnAgregarEvaluacionTab2.Visible = true;
                    cargarEvaluacionTab2();

                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }

        }
        protected void btnDetalleDevengadoTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMDevengadoMensualizadoTab0(Convert.ToInt32(id));
            UpdatePanel3.Update();
            MPE_DetalleSOSEMTab0.Show();
        }
        protected void btnFuenteTab0_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            var id = grdSOSEMEjecutoras.DataKeys[row.RowIndex].Value;

            CargaSOSEMFuenteFinanciamientoTab0(Convert.ToInt32(id));
            Up_FuenteFinanciamientoTAb0.Update();
            MPE_FuenteFinanciamientoTab0.Show();
        }
        protected void imgDocAvanceAdicionalTab2_Click1(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoAdicionalTab2.Rows[row.RowIndex].FindControl("imgDocAvanceAdicionalTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtnEditarUE_Click(object sender, ImageClickEventArgs e)
        {
            //MPE_UE.Show();
            string script = "<script>$('#modalEditarUE').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }
        protected void btnModificarUE_Click(object sender, EventArgs e)
        {
            ObjBEProyecto.id = Convert.ToInt32(LblID_PROYECTO.Text);
            if (LblID_SOLICITUD.Text == "")
            {
                ObjBEProyecto.Id_Solicitudes = 0;
            }
            else
            {
                ObjBEProyecto.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
            }
            ObjBEProyecto.Entidad_ejec = txtUnidadEjecutora.Text;

            int val = objBLProyecto.U_MON_UnidadEjecutora(ObjBEProyecto);

            if (val == 1)
            {
                string script = "<script>$('#modalEditarUE').modal('hide');alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                }
                UPTabContainerDetalles.Update();



            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }
        protected void imgbtnEditarMontosMVCS_Click(object sender, ImageClickEventArgs e)
        {
            //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
            //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
            //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");

            //txtMontoObraMVCS.Text = lblFinanObra.Text;
            //txtMontoSupervisionMVCS.Text = lblFinanSuper.Text;

            txtMontoTotalMVCS.Text = (Convert.ToDouble(txtMontoObraMVCS.Text) + Convert.ToDouble(txtMontoSupervisionMVCS.Text)).ToString("N");

            Up_MontosMVCS.Update();
            MPE_MontosMVCS.Show();
        }
        protected void btnModificarMontosMVCS_Click(object sender, EventArgs e)
        {
            ObjBEProyecto.id = Convert.ToInt32(LblID_PROYECTO.Text);
            ObjBEProyecto.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
            ObjBEProyecto.Monto_Obra = Convert.ToDouble(txtMontoObraMVCS.Text);
            ObjBEProyecto.Monto_Supervision = Convert.ToDouble(txtMontoSupervisionMVCS.Text);

            int val = objBLProyecto.U_MON_MontosMVCS(ObjBEProyecto);

            if (val == 1)
            {
                string script = "<script>alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    //string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    //txtUnidadEjecutora.Text = UE;

                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                }
                UPTabContainerDetalles.Update();
                MPE_MontosMVCS.Hide();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }
        protected void txtMontoObraMVCS_TextChanged(object sender, EventArgs e)
        {
            if (txtMontoObraMVCS.Text == "")
            {
                txtMontoObraMVCS.Text = "0";
            }

            if (txtMontoSupervisionMVCS.Text == "")
            {
                txtMontoSupervisionMVCS.Text = "0";
            }

            txtMontoTotalMVCS.Text = (Convert.ToDouble(txtMontoObraMVCS.Text) + Convert.ToDouble(txtMontoSupervisionMVCS.Text)).ToString("N");

            Up_MontosMVCS.Update();

        }
        protected void lnkbtnActaParalizacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnActaParalizacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void lnkbtnInformeParalizacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnInformeParalizacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void lnkbtnoficioParalizacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnoficioParalizacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void btnGuardarAccionesTab4_Click(object sender, EventArgs e)
        {

            //jc
            _BEAmpliacion.id_ampliaciones = Convert.ToInt32(lblId_ParalizacionTab4.Text);
            _BEAmpliacion.fecha = Convert.ToDateTime(txtFechaAccionTab4.Text);
            _BEAmpliacion.accion = Convert.ToString(txtAccionTab4.Text);
            _BEAmpliacion.usr_registro = Convert.ToInt32(LblID_USUARIO.Text);
            _BEAmpliacion.usr_update = Convert.ToInt32(LblID_USUARIO.Text);


            int val;

            try
            {
                val = _objBLAmpliacion.spi_Detalle_Paralizacion(_BEAmpliacion);
            }
            catch (Exception ex)
            {
                val = 0;
            }

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                //jc
                cargaAccionTab4();
                Pn_AccionFormulario.Visible = false;
                cargaParalizacionTab4();
                Up_Tab2.Update();


            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }
        protected void btnCancelarAccionesTab4_Click(object sender, EventArgs e)
        {

            //jc
            Pn_AccionFormulario.Visible = false;
            Up_Tab4.Update();

        }
        protected void imgInfParalizacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdParalizacionTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdParalizacionTab4.Rows[row.RowIndex].FindControl("imgInfParalizacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgOficParalizacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdParalizacionTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdParalizacionTab4.Rows[row.RowIndex].FindControl("imgOficParalizacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgAccion_Click(object sender, ImageClickEventArgs e)
        {

            //Boton de agregar Accion

            ImageButton boton = default(ImageButton);
            GridViewRow row = default(GridViewRow);
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            lblId_ParalizacionTab4.Text = grdParalizacionTab4.DataKeys[row.RowIndex]["id_ampliacion"].ToString();

            btnGuardarAccionesTab4.Visible = true;
            btnModificarParalizacionTab4.Visible = false;


            //grdAccionesTab4.DataSource = null;
            //grdAccionesTab4.DataBind();
            PN_LISTA_ACCION.Visible = false;
            Pn_AccionFormulario.Visible = true;
            MPE_DETALLEACCION.Show();
        }
        protected void chb_flagAyuda_Check(object sender, EventArgs e)
        {
            //jc
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var cod_detalle_paralizacion = grdAccionesTab4.DataKeys[row.RowIndex].Value;
            CheckBox chb_flagAyuda = (CheckBox)grdAccionesTab4.Rows[row.RowIndex].FindControl("chb_flagAyuda");
            if (chb_flagAyuda.Checked == true)
            {
                _BEAmpliacion.flag_ayuda = 1;
            }
            else
            {
                _BEAmpliacion.flag_ayuda = 0;

            }

            _BEAmpliacion.id_detalleParalizacion = Convert.ToInt32(cod_detalle_paralizacion);
            _BEAmpliacion.usr_update = Convert.ToInt32(LblID_USUARIO.Text);


            int val = _objBLAmpliacion.spu_MON_Detalle_Paralizacion(_BEAmpliacion);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                cargaAccionTab4();

                Up_Tab4.Update();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }


        }
        protected void grdAccionesTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkAyuda = (CheckBox)e.Row.FindControl("chb_flagAyuda");
                if (chkAyuda.Text == "1")
                {
                    chkAyuda.Checked = true;
                }
                chkAyuda.Text = "";
            }


        }
        protected void imgVerAccion_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton boton = default(ImageButton);
            GridViewRow row = default(GridViewRow);
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            lblId_ParalizacionTab4.Text = grdParalizacionTab4.DataKeys[row.RowIndex]["id_ampliacion"].ToString();

            cargaAccionTab4();
            PN_LISTA_ACCION.Visible = true;
            Pn_AccionFormulario.Visible = false;
            UP_RegistroAcciones.Update();

            MPE_DETALLEACCION.Show();


        }
        protected void grdAccionesTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //jc
            if (e.CommandName == "EliminarAccion")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAccionesTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_detalleParalizacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.usr_registro = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.spu_MON_Elim_Detalle_Paralizacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaParalizacionTab4();
                    cargaAccionTab4();
                    Up_Tab2.Update();




                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grdAccionesTab4_SelectedIndexChanged(object sender, EventArgs e)
        {
            //jc
            lblIdAcciones.Text = grdAccionesTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdAccionesTab4.SelectedRow;

            txtFechaAccionTab4.Text = ((Label)row.FindControl("lblfechaFin")).Text;
            txtAccionTab4.Text = ((Label)row.FindControl("lblAcciones")).Text;
            lblNomUsuarioAcciones.Text = "Actualizó: " + ((Label)row.FindControl("lblNomUsuarioAccion")).Text + " - " + ((Label)row.FindControl("lblFecha_UpdateAccion")).Text;

            Pn_AccionFormulario.Visible = true;
            btnGuardarAccionesTab4.Visible = false;
            btnModificarAccionesTab4.Visible = true;

            //UPAcciones.Update();


        }
        protected void btnModificarAccionesTab4_Click(object sender, EventArgs e)
        {
            //jc
            _BEAmpliacion.id_detalleParalizacion = Convert.ToInt32(lblIdAcciones.Text);
            _BEAmpliacion.fecha = Convert.ToDateTime(txtFechaAccionTab4.Text);
            _BEAmpliacion.accion = Convert.ToString(txtAccionTab4.Text);
            _BEAmpliacion.usr_registro = Convert.ToInt32(LblID_USUARIO.Text);
            _BEAmpliacion.usr_update = Convert.ToInt32(LblID_USUARIO.Text);


            int val;

            try
            {
                val = _objBLAmpliacion.spud_MON_Detalle_Paralizacion(_BEAmpliacion);
            }
            catch (Exception ex)
            {
                val = 0;
            }

            if (val == 1)
            {
                string script = "<script>alert('Se Actualizo correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                cargaParalizacionTab4();
                cargaAccionTab4();
                Up_Tab2.Update();
                Pn_AccionFormulario.Visible = false;


            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }
        protected void btnAgregarDetalleAccionTab4_Click(object sender, ImageClickEventArgs e)
        {
            UP_RegistroAcciones.Update();
            lblIdAcciones.Text = "";
            txtFechaAccionTab4.Text = "";
            txtAccionTab4.Text = "";
            btnGuardarAccionesTab4.Visible = true;
            btnModificarAccionesTab4.Visible = false;
            Pn_AccionFormulario.Visible = true;
            lblNomUsuarioAcciones.Text = "";
        }
        protected void ImgCerrarAccion_Click(object sender, ImageClickEventArgs e)
        {
            Up_Tab4.Update();
            cargaParalizacionTab4();
            MPE_DETALLEACCION.Hide();
        }
        protected void ImgCerrarAccion_Click1(object sender, ImageClickEventArgs e)
        {

        }
        //protected void grdSEACEBienesServicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdSEACEBienesServicios.PageIndex = e.NewPageIndex;
        //    CargaOBRAEjecutorasTab0SEACEBienesServicios();
        //}
        //protected void grdSEACEConsultoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdSEACEConsultoria.PageIndex = e.NewPageIndex;
        //    CargaOBRAEjecutorasTab0SEACEConsultoria();
        //}
       
        //protected void imgseaceobra_Click(object sender, ImageClickEventArgs e)
        //{
        //    ImageButton boton = default(ImageButton);
        //    GridViewRow row = default(GridViewRow);
        //    boton = (ImageButton)sender;
        //    row = (GridViewRow)boton.NamingContainer;

        //    HiddenField hfiIdentificar = (HiddenField)row.FindControl("hfIDENTIFICADOR");
        //    string x;
        //    x = hfiIdentificar.Value;
        //}
        //protected void imgseaceConsultoria_Click(object sender, ImageClickEventArgs e)
        //{

        //}
        protected void ddlTipoEstadoInformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);

            if (ddlTipoEstadoInformeTab2.SelectedValue.Equals("5")) // PARALIZADO
            {

                if (!(LblID_USUARIO.Text.Equals("792") || LblID_USUARIO.Text.Equals("481")) && lblCOD_SUBSECTOR.Text.Equals("1"))
                {
                    lblMsjCambioEstado.Text = "<div>" +
                       "<p class='bg-danger paddding-lg text-danger'><b>NOTA:</b> </br></br>";

                    lblMsjCambioEstado.Text = lblMsjCambioEstado.Text + "Para registrar el cambio de estado de una OBRA al ESTADO PARALIZADO, se debe enviar el informe de derivación a la Coordinación de Asistencia Técnica y luego" +
                        " registrar el informe en el SSP. </br> </br>" +
                        "Posteriormente la persona autorizada de la Coordinación de Asistencia Técnica realizará el cambio de estado a PARALIZADO." +
                        "</p></div>";
                    Up_MsjEstado.Update();
                    ddlTipoEstadoInformeTab2.SelectedValue = "";
                    CargaTipoSubEstadosDetalle(ddlTipoEstadoInformeTab2.SelectedValue);
                }
                else
                {
                    ddlTipoSubEstadoInformeTab2.SelectedValue = "82";
                    lblNombreSubEstado2.Visible = true;
                    ddlTipoSubEstado2InformeTab2.Visible = true;
                    Up_SubEstado2NombreTab2.Update();
                    Up_SubEstado2DetalleTab2.Update();
                    CargaTipoSubEstado2(ddlTipoSubEstadoInformeTab2.SelectedValue);

                    lblNombreProblematicaTab2.Visible = true;
                    //ddlTipoProblematicaTab2.Visible = true;
                    chbProblematicaTab2.Visible = true;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();

                    CargaTipoSubEstadoParalizado(ddlTipoSubEstado2InformeTab2.SelectedValue);
                    Up_SubEstado2DetalleTab2.Update();
                }
            }
            else
            {
                lblMsjCambioEstado.Text = "";
                Up_MsjEstado.Update();

                lblNombreSubEstado2.Visible = false;
                ddlTipoSubEstado2InformeTab2.SelectedValue = "";
                ddlTipoSubEstado2InformeTab2.Visible = false;
                Up_SubEstado2NombreTab2.Update();
                Up_SubEstado2DetalleTab2.Update();

                lblNombreProblematicaTab2.Visible = false;
                //ddlTipoProblematicaTab2.Visible = false;
                chbProblematicaTab2.Visible = false;
                Up_NombreProblematicaTab2.Update();
                Up_TipoProblematicaTab2.Update();
            }

            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("87")) // SUSPENSION DE PLAZO
            {
                trSuspensionTab2.Visible = true;
                trReinicioTab2.Visible = true;
            }
            else
            {
                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
            }
            Up_FechasSuspensionTab2.Update();

            Up_SubEstadoNombreTab2.Update();
            Up_SubEstadoDetalleTab2.Update();
        }

        protected void ddlTipoSubEstadoInformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            trSuspensionTab2.Visible = false;
            trReinicioTab2.Visible = false;

            if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("87")) // SUSPENSION DE PLAZO
            {
                trSuspensionTab2.Visible = true;
                trReinicioTab2.Visible = true;

                lblNombreSubEstado2.Visible = true;
                ddlTipoSubEstado2InformeTab2.Visible = true;
                Up_SubEstado2NombreTab2.Update();
                Up_SubEstado2DetalleTab2.Update();
                CargaTipoSubEstado2(ddlTipoSubEstadoInformeTab2.SelectedValue);
            }
            else if (ddlTipoSubEstadoInformeTab2.SelectedValue.Equals("80")) //ATRAZADO
            {
                lblNombreSubEstado2.Visible = true;
                ddlTipoSubEstado2InformeTab2.Visible = true;
                Up_SubEstado2NombreTab2.Update();
                Up_SubEstado2DetalleTab2.Update();
                CargaTipoSubEstado2(ddlTipoSubEstadoInformeTab2.SelectedValue);
            }
            else
            {
                lblNombreSubEstado2.Visible = false;
                ddlTipoSubEstado2InformeTab2.Visible = false;
                Up_SubEstado2NombreTab2.Update();
                Up_SubEstado2DetalleTab2.Update();
            }

            Up_FechasSuspensionTab2.Update();
        }
        protected void lnkbtnRegistrarActaMonitoreoGrilla_Click(object sender, EventArgs e)
        {

            if (lblID_evaluacionTab2.Text.Length == 0)
            {
                string script = "<script>alert('Debe registrar primero el avance para luego continuar con el Acta de Visita.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                ifrmActaVisita.Attributes["src"] = "iframe_RegistrarActaVisita.aspx?idE=" + lblID_evaluacionTab2.Text + "&idPo=" + LblID_PROYECTO.Text + "&idSector=" + lblCOD_SUBSECTOR.Text;
                Up_RegistroActaVisitaBoostrap.Update();

                //string script = "<script>ShowPopupRegistroActaVisita();</script>";
                string script = "<script>$('#modalActaVisita').modal('show');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

        }
        protected void lnkbtnDescargarActaMonitoreoGrilla_Click(object sender, EventArgs e)
        {
            LinkButton boton = default(LinkButton);
            GridViewRow row = default(GridViewRow);
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grdEvaluacionTab2.DataKeys[row.RowIndex].Value.ToString();

            string script = "<script>window.open('Acta_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        }
        protected void CargarAsuntosTecnicos(string idEvaluacionRecomendacion)
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(idEvaluacionRecomendacion);
            dt = _objBLEjecucion.spMON_RevisionItemAdministrativoVisita(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= 16; i++)
                {
                    bool encontro = false;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (i == Convert.ToInt32((row["id_itemAdministrativoVisita"].ToString())))
                        {
                            TextBox txtCome = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtComentario" + row["id_itemAdministrativoVisita"].ToString());
                            RadioButtonList result = (RadioButtonList)FindControl("ctl00$ContentPlaceHolder1$RadioButtonList" + row["id_itemAdministrativoVisita"].ToString());

                            txtCome.Text = row["comentario"].ToString();
                            if (row["result"].ToString().Equals("0"))
                            {
                                result.ClearSelection();
                            }
                            else
                            {
                                result.SelectedValue = row["result"].ToString();
                            }

                            encontro = true;
                        }
                    }

                    if (encontro == false)
                    {
                        TextBox txtCome = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtComentario" + i.ToString());
                        RadioButtonList result = (RadioButtonList)FindControl("ctl00$ContentPlaceHolder1$RadioButtonList" + i.ToString());
                        txtCome.Text = "";
                        result.ClearSelection();
                    }

                }
            }
        }
        protected void lnkbtnRegistrarInformeMonitoreoGrilla_Click(object sender, EventArgs e)
        {

            if (lblID_evaluacionTab2.Text.Length == 0)
            {
                string script = "<script>alert('Debe registrar primero el avance para luego continuar con el Informe.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                ifrmInformeVisita.Attributes["src"] = "iframe_RegistrarInformeVisita.aspx?idE=" + lblID_evaluacionTab2.Text + "&idPo=" + LblID_PROYECTO.Text;
                Up_RegistroInforme.Update();

                //string script = "<script>ShowPopupRegistroInformeVisita();</script>";
                string script = "<script>$('#modalInformeVisita').modal('show');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }
        protected void lnkbtnDescargarInformeMonitoreoGrilla_Click(object sender, EventArgs e)
        {
            LinkButton boton = default(LinkButton);
            GridViewRow row = default(GridViewRow);
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grdEvaluacionTab2.DataKeys[row.RowIndex].Value.ToString();
            //lblIdRecomendacionInformeMonitoreoPOPU.Text = id;

            string script = "<script>window.open('Informe_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }
        protected void imgbtnFotosTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton = default(ImageButton);
            GridViewRow row = default(GridViewRow);
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grdEvaluacionTab2.DataKeys[row.RowIndex]["id_evaluacionRecomendacion"].ToString();
            hfIdEvaluacionRecomentacion.Value = id;
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(id);

            //Falta habilitar Panel Movil
            DataTable dt = _objBLEjecucion.spMON_DetalleFotoVisitaMovil(_BEEjecucion);
            if (dt.Rows.Count > 0)
            {
                MPE_ElegirFoto.Show();
                grdFotoMovil.DataSource = dt;
                grdFotoMovil.DataBind();
                UpdatePanelFotoMovil.Update();
            }
            else
            {
                string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

            //string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }
        protected void cargaDetalleFotosVisita()
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIDEvaluacionRecomendacionFotoTab2.Text);

            grdAvanceFoto.DataSource = _objBLEjecucion.spMON_DetalleFotoVisita(_BEEjecucion);
            grdAvanceFoto.DataBind();

        }

        protected void cargaDetalleFotosVisitaMovil(int pIdEvaluacionRecomendacion)
        {
            _BEEjecucion.Id_ejecucionRecomendacion = pIdEvaluacionRecomendacion;

            DataTable dt = _objBLEjecucion.spMON_DetalleFotoVisitaMovil(_BEEjecucion);
            grdMovilRegistro.DataSource = dt;
            grdMovilRegistro.DataBind();
        }

      

        protected void grdAvanceFoto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //jc
            if (e.CommandName == "EliminarAvanceFoto")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFoto.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.comentario = "";
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.urlDoc = "";
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_DetalleFotoVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDetalleFotosVisita();
                    Up_Tab2.Update();
                    MPE_AVANCE_FOTO.Show();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdAvanceFoto_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblIdDetalleFoto.Text = grdAvanceFoto.SelectedDataKey.Value.ToString();
            GridViewRow row = grdAvanceFoto.SelectedRow;

            txtDescripcionFoto.Text = ((Label)row.FindControl("lblDescripcion")).Text;
            lnkCargarAvanceFoto.Text = ((ImageButton)row.FindControl("imgAvanceFoto")).ToolTip;
            GeneraIcoFile(lnkCargarAvanceFoto.Text, imgBtnCargarAvanceFoto);
            lblNomUsuarioAvanceFoto.Text = "Actualizó: " + ((Label)row.FindControl("lblUsuario")).Text + " - " + ((Label)row.FindControl("lblFechaUpdate")).Text;

            Up_AVANCEFOTO.Update();

            btnModificarAvanceFoto.Visible = true;
            PN_AvanceFotoFormulario.Visible = true;
            btnGuardarAvanceFoto.Visible = false;
            MPE_AVANCE_FOTO.Show();

        }
        protected void btnAgregarAvanceFoto_Click(object sender, ImageClickEventArgs e)
        {
            Up_AVANCEFOTO.Update();
            btnModificarAvanceFoto.Visible = false;
            PN_ListaAvanceFoto.Visible = true;
            PN_AvanceFotoFormulario.Visible = true;
            btnGuardarAvanceFoto.Visible = true;
            lnkCargarAvanceFoto.Text = "";
            //txtFechaAvanceFoto.Text = "";
            imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
            lblNomUsuarioAvanceFoto.Text = "";
        }
        protected void btnGuardarAvanceFoto_Click(object sender, EventArgs e)
        {
            if (txtDescripcionFoto.Text.Length == 0)
            {
                string script = "<script>alert('Ingresar una breve descripción.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                if (validaArchivoFotos(FileUploadCargarAvanceFoto))
                {
                    _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIDEvaluacionRecomendacionFotoTab2.Text);
                    _BEEjecucion.comentario = txtDescripcionFoto.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCargarAvanceFoto);

                    //AsyncFileUpload AsyncFileUploadPDF = (AsyncFileUpload)Session["FotoCargaAvance"];
                    //_BEEjecucion.urlDoc = _Metodo.uploadAsyncFileMonitoreo(AsyncFileUploadPDF);

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_MON_DetalleFotoVisita(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        PN_AvanceFotoFormulario.Visible = false;
                        cargaDetalleFotosVisita();

                        MPE_AVANCE_FOTO.Show();
                        txtDescripcionFoto.Text = "";
                        lnkCargarAvanceFoto.Text = "";
                        imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
                        PN_AvanceFotoFormulario.Visible = false;
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        protected void btnModificarAvanceFoto_Click(object sender, EventArgs e)
        {

            if (validaArchivoFotos(FileUploadCargarAvanceFoto))
            {

                _BEEjecucion.id_item = Convert.ToInt32(lblIdDetalleFoto.Text);

                if (FileUploadCargarAvanceFoto.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCargarAvanceFoto);
                }
                else
                {
                    _BEEjecucion.urlDoc = lnkCargarAvanceFoto.Text;
                }

                _BEEjecucion.comentario = txtDescripcionFoto.Text;
                _BEEjecucion.Tipo = 1;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLEjecucion.UD_MON_DetalleFotoVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se Actualizo correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaDetalleFotosVisita();
                    MPE_AVANCE_FOTO.Show();
                    txtDescripcionFoto.Text = "";
                    lnkCargarAvanceFoto.Text = "";
                    imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
                    PN_AvanceFotoFormulario.Visible = false;


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void btnCancelarAvanceFoto_Click(object sender, EventArgs e)
        {
            Up_AVANCEFOTO.Update();
            PN_AvanceFotoFormulario.Visible = false;

        }

        protected void lnkCargarAvanceFoto_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkCargarAvanceFoto.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgAvanceFoto_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdAvanceFoto.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdAvanceFoto.Rows[row.RowIndex].FindControl("imgAvanceFoto");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdAvanceFoto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgAvanceFoto");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void btnGenerarPanelFotografico_Click(object sender, EventArgs e)
        {
            string id = lblIDEvaluacionRecomendacionFotoTab2.Text;
            string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }
        protected void lnkbtnRegistrarPanelFotosGrilla_Click(object sender, EventArgs e)
        {
            if (lblID_evaluacionTab2.Text.Length == 0)
            {
                string script = "<script>alert('Debe registrar primero el avance para luego continuar con el registro del panel fotográfico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                //lblIdRecomendacionInformeMonitoreoPOPU.Text = lblID_evaluacionTab2.Text;
                //ImageButton boton = default(ImageButton);
                //GridViewRow row = default(GridViewRow);
                //boton = (ImageButton)sender;
                //row = (GridViewRow)boton.NamingContainer;
                lblIDEvaluacionRecomendacionFotoTab2.Text = lblID_evaluacionTab2.Text;


                cargaDetalleFotosVisita();
                //PN_ListaAvanceFoto.Visible = true;
                PN_AvanceFotoFormulario.Visible = false;
                Up_AVANCEFOTO.Update();

                cargaDetalleFotosVisitaMovil(Convert.ToInt32(lblID_evaluacionTab2.Text));
               
                Up_MovilRegistro.Update();

                MPE_AVANCE_FOTO.Show();
            }
        }
        protected void TabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            ViewState["ActiveTabIndex"] = TabContainer.ActiveTabIndex;

            //if (TabContainer.ActiveTabIndex == 1)
            //{
            //    TabActivoProceso();
            //}
        }

        protected void ddlTipoMonitoreo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaSubTipoMonitoreo(ddlTipoMonitoreo.SelectedValue);

            Up_SubMonitoreoNombreTab2.Update();
            Up_SubMonitoreoDetalleTab2.Update();
        }

        protected void grdEvaluacionTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdEvaluacionTab2.PageIndex = e.NewPageIndex;
            cargarEvaluacionTab2();
        }

        protected void grdAvanceFisicoTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAvanceFisicoTab2.PageIndex = e.NewPageIndex;
            CargaAvanceFisico();
        }

        //protected void lnkbtnDetalleParalizado_Click(object sender, EventArgs e)
        //{

        //    LinkButton boton = default(LinkButton);
        //    GridViewRow row = default(GridViewRow);
        //    boton = (LinkButton)sender;
        //    row = (GridViewRow)boton.NamingContainer;
        //    string idEvaluacion = grdEvaluacionTab2.DataKeys[row.RowIndex]["id_evaluacionRecomendacion"].ToString();

        //    ifrmRegistroSeguimientoParalizacion.Attributes["src"] = "iframe_RegistrarSeguimientoParalizacion.aspx?idE=" + idEvaluacion + "&idPo=" + LblID_PROYECTO.Text + "&SNIP=" + lblSNIP.Text;
        //    Up_SeguimientoParalizacion.Update();

        //    MPE_DetalleParalizacion.Show();

        //    Panel_ObservacionTab2.Visible = false;
        //    imgbtnAgregarEvaluacionTab2.Visible = true;
        //    Up_Tab2.Update();


        //}
          

        protected void grdUbicacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdUbicacion.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.flagActivo = 0;
                _BEBandeja.CCPP = ((Label)CurrentRow.FindControl("lblUbigeo")).Text + ((Label)CurrentRow.FindControl("lblUbigeoCCPP")).Text;
                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarUbicacion_Click(object sender, ImageClickEventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = true;
            //btnModificarUbicacionMetasTab0.Visible = false;
            Up_UbigeoTab0.Update();
        }

        protected Boolean ValidarUbigeoTab0()
        {
            Boolean result;
            result = true;

            if (ddlDepartamentoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Departamento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlProvinciaTab0.SelectedValue == "")
            {

                string script = "<script>alert('Ingrese Provincia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;


            }

            if (ddlDistritoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Distrito.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;
        }

        protected void btnGuardarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            if (ValidarUbigeoTab0())
            {
                _BEBandeja.flagActivo = 1;
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                if (ddlCCPPTab0.SelectedValue.Equals(""))
                {
                    _BEBandeja.CCPP = ddlDepartamentoTab0.SelectedValue + ddlProvinciaTab0.SelectedValue + ddlDistritoTab0.SelectedValue;
                }
                else
                {
                    _BEBandeja.CCPP = ddlCCPPTab0.SelectedValue;
                }

                _BEBandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    //Actualizar nombres de ubigeo en la principal
                    ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                    dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                    if (dt.Rows.Count > 0)
                    {
                        string depa = dt.Rows[0]["depa"].ToString();
                        string prov = dt.Rows[0]["prov"].ToString();
                        string dist = dt.Rows[0]["dist"].ToString();
                        lblUBICACION.Text = depa + " - " + prov + " - " + dist;
                        UPTabContainerDetalles.Update();
                    }

                    CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));
                    Panel_UbicacionMetasTab0.Visible = false;
                    Up_UbigeoTab0.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void btnCancelarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = false;
        }

        protected void ddlDepartamentoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlProvinciaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void ddlDistritoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);
            Up_UbigeoTab0.Update();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = _objBLBandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("- SELECCIONAR -", ""));

        }

        protected void CargaUbigeosProyectos(int idProyecto)
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = _objBLBandeja.F_spMON_Ubigeo(idProyecto);
            grdUbicacion.DataSource = ListUbigeos;
            grdUbicacion.DataBind();



        }

        protected void imgbtnEditarUbigeo_Click(object sender, ImageClickEventArgs e)
        {
            CargaUbigeosProyectos(Convert.ToInt32(LblID_PROYECTO.Text));

            cargaUbigeo(ddlDepartamentoTab0, "1", null, null, null);
            cargaUbigeo(ddlProvinciaTab0, "2", ddlDepartamentoTab0.SelectedValue, null, null);
            cargaUbigeo(ddlDistritoTab0, "3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, null);
            cargaUbigeo(ddlCCPPTab0, "4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue);

            Up_UbigeoTab0.Update();
            //string script = "<script>ShowPopupRegistroInformeVisita();</script>";
            string script = "<script>$('#modalUbigeo').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void ddlTipoSubEstado2InformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadoParalizado(ddlTipoSubEstado2InformeTab2.SelectedValue); //TIPO PROBLEMATICA
            //ddlTipoSubEstado2InformeTab2.Visible = true;
            Up_TipoProblematicaTab2.Update();
            //Up_chbTipoProblematicaTab2.Update();
        }

        protected void BtnRefrescar_Click(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);
            DataTable dt = (new BLProyecto().spu_MON_UltimaActualizacion(idProy));
            if (dt.Rows.Count > 0)
            {
                LblUltimaModificacion.Text = Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortTimeString();
                LblUsuario.Text = dt.Rows[0]["Usuario"].ToString();
            }
            else
            {
                LblUltimaModificacion.Text = "";
                LblUsuario.Text = "";
            }

        }

        protected void btnVerPanelFotografico_Click(object sender, EventArgs e)
        {
            MPE_ElegirFoto.Hide();

            string script = "<script>('#modalElegirFoto').modal('hide');window.open('PanelFotografico_Visita.aspx?idE=" + hfIdEvaluacionRecomentacion.Value + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void btnVerFotosMovil_Click(object sender, EventArgs e)
        {
            MPE_ElegirFoto.Hide();

            string script = "<script>$('#modalElegirFoto').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //Panel_Movil.Visible = true;
            //btnVerPanelFotografico.Visible = false;
            //btnVerFotosMovil.Visible = false;
            //UpdatePanelFotoMovil.Update();
        }


        protected void grdAvanceFisicoAdicionalTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAvanceFisicoAdicionalTab2.PageIndex = e.NewPageIndex;
            CargaAvanceFisico();
        }

        protected void grdPlazoTab4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPlazoTab4.PageIndex = e.NewPageIndex;
            cargaPlazoTAb4();
        }

        protected void grdAvanceFisicoTab2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableHeaderCell cell1 = new TableHeaderCell();
                cell1.Text = "Información General";
                if (lblID_ACCESO.Text.Equals("0"))
                {
                    cell1.ColumnSpan = 5;
                }
                else
                {
                    cell1.ColumnSpan = 6;
                }

                cell1.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell1);

                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Avance % Fisico";
                HeaderCell.ColumnSpan = 2;
                HeaderCell.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(HeaderCell);

                TableCell cell3 = new TableCell();
                cell3.Text = "Avance % Financiero";
                cell3.ColumnSpan = 2;
                cell3.CssClass = "GridHeader2";
                HeaderGridRow.Cells.Add(cell3);

                HeaderGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);

            }
        }

        protected void grdPersonalClaveTab1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocCV");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void grdPersonalClaveTab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblId_ParalizacionTab4.Text = grdPersonalClaveTab1.SelectedDataKey.Value.ToString();
            GridViewRow row = grdPersonalClaveTab1.SelectedRow;

            lblIdPersonalClaveTab1.Text = ((Label)row.FindControl("lblIdPersonalClave")).Text;

            txtNombrePersonalClaveTab1.Text = ((Label)row.FindControl("lblNombre")).Text;
            txtDNIPersonalClaveTab1.Text = ((Label)row.FindControl("lblDNI")).Text;
            txtCIPPersonalClaveTab1.Text = ((Label)row.FindControl("lblCIP")).Text;
            txtEspecialidadPersonalClaveTab1.Text = ((Label)row.FindControl("lblEspecialidad")).Text;
            txtTelefonoPersonalClaveTab1.Text = ((Label)row.FindControl("lblTelefono")).Text;
            txtCorreoPersonalClaveTab1.Text = ((Label)row.FindControl("lblCorreo")).Text;
            lnkbtnCVPersonalClaveTab1.Text = ((ImageButton)row.FindControl("imgDocCV")).ToolTip;
            GeneraIcoFile(lnkbtnCVPersonalClaveTab1.Text, imgbtnCVPersonalClaveTab1);

            lblNomUsuarioPersonalClaveTab1.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            imgbtnAgregarPersonalClaveTab1.Visible = false;
            Panel_PersonalClaveTab1.Visible = true;
            btnGuardarPersonalClaveTab1.Visible = false;
            btnModificarPersonalClaveTab1.Visible = true;

            Up_Tab1.Update();
        }

        protected void grdPersonalClaveTab1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPersonalClaveTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id = Convert.ToInt32(objTemp.ToString());
                _BEProceso.id_tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.UD_MON_PersonalClave(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaPersonalClaveTab1();
                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void imgbtnAgregarPersonalClaveTab1_Click(object sender, ImageClickEventArgs e)
        {
            imgbtnAgregarPersonalClaveTab1.Visible = false;
            Panel_PersonalClaveTab1.Visible = true;
            lblNomUsuarioPersonalClaveTab1.Text = "";
            btnGuardarPersonalClaveTab1.Visible = true;
            btnModificarPersonalClaveTab1.Visible = false;

            txtNombrePersonalClaveTab1.Text = "";
            txtDNIPersonalClaveTab1.Text = "";
            txtCIPPersonalClaveTab1.Text = "";
            txtEspecialidadPersonalClaveTab1.Text = "";
            txtTelefonoPersonalClaveTab1.Text = "";
            txtCorreoPersonalClaveTab1.Text = "";
            lnkbtnCVPersonalClaveTab1.Text = "";


            Up_Tab1.Update();
        }

        protected void btnGuardarPersonalClaveTab1_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadCVPersonalClaveTab1))
            {

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.nombre = txtNombrePersonalClaveTab1.Text;
                _BEProceso.dni = txtDNIPersonalClaveTab1.Text;
                _BEProceso.cip = txtCIPPersonalClaveTab1.Text;
                _BEProceso.especialidad = txtEspecialidadPersonalClaveTab1.Text;
                _BEProceso.telefono = txtTelefonoPersonalClaveTab1.Text;
                _BEProceso.correo = txtCorreoPersonalClaveTab1.Text;
                _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadCVPersonalClaveTab1);

                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLProceso.I_MON_PersonalClave(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    imgbtnAgregarPersonalClaveTab1.Visible = true;
                    Panel_PersonalClaveTab1.Visible = false;

                    cargaPersonalClaveTab1();
                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }

        protected void btnModificarPersonalClaveTab1_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadCVPersonalClaveTab1))
            {

                _BEProceso.id = Convert.ToInt32(lblIdPersonalClaveTab1.Text);

                _BEProceso.nombre = txtNombrePersonalClaveTab1.Text;
                _BEProceso.dni = txtDNIPersonalClaveTab1.Text;
                _BEProceso.cip = txtCIPPersonalClaveTab1.Text;
                _BEProceso.especialidad = txtEspecialidadPersonalClaveTab1.Text;
                _BEProceso.telefono = txtTelefonoPersonalClaveTab1.Text;
                _BEProceso.correo = txtCorreoPersonalClaveTab1.Text;

                if (FileUploadCVPersonalClaveTab1.HasFile)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadCVPersonalClaveTab1);
                }
                else
                { _BEProceso.urlDoc = lnkbtnCVPersonalClaveTab1.Text; }


                _BEProceso.id_tipo = 1;

                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLProceso.UD_MON_PersonalClave(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    imgbtnAgregarPersonalClaveTab1.Visible = true;
                    Panel_PersonalClaveTab1.Visible = false;

                    cargaPersonalClaveTab1();
                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnCancelarPersonalClaveTab1_Click(object sender, EventArgs e)
        {
            imgbtnAgregarPersonalClaveTab1.Visible = true;
            Panel_PersonalClaveTab1.Visible = false;

            Up_Tab1.Update();
        }

        protected void lnkbtnCVPersonalClaveTab1_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = lnkbtnCVPersonalClaveTab1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocCV_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdPersonalClaveTab1.Rows[row.RowIndex].FindControl("imgDocCV");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void btnGuardarSostenibilidadTab3_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadSostenibilidadTab3))
            {
                string msjVal = ValidarSostenibilidadTab3();
                if (msjVal.Length > 0)
                {
                    string script = "<script>alert('" + msjVal + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BELiquidacion.tipoFinanciamiento = 3;

                    _BELiquidacion.tipoReceptora = Convert.ToInt32(ddlTipoReceptoraTab3.SelectedValue);
                    _BELiquidacion.unidadReceptora = txtUnidadReceptoraTab3.Text;
                    _BELiquidacion.flagCapacidad = ddlflagCapacitadTab3.SelectedValue;
                    _BELiquidacion.flagOperatividad = ddlOperatividadTab3.SelectedValue;

                    _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaActaTransferenciaTab3.Text);
                    if (FileUploadSostenibilidadTab3.HasFile)
                    {
                        _BELiquidacion.urlDocSostenibilidad = _Metodo.uploadfile(FileUploadSostenibilidadTab3);
                    }
                    else
                    { _BELiquidacion.urlDocSostenibilidad = LnkbtnSostenibilidadTab3.Text; }
                    _BELiquidacion.comentarioCapacidad = txtComentarioTab3.Text;

                    _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLLiquidacion.IU_MON_SostenibilidadPorContrata(_BELiquidacion);


                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaLiquidacion();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                    Up_Tab3.Update();
                }
            }
        }

        protected String ValidarSostenibilidadTab3()
        {
            string msj = "";


            if (ddlTipoReceptoraTab3.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de receptora.\\n";
            }

            if (txtUnidadReceptoraTab3.Text.Count() < 2)
            {
                msj = msj + "*Ingresar nombre de entidad receptora.\\n";
            }
            if (ddlflagCapacitadTab3.SelectedValue == "")
            {
                msj = msj + "*Responder si la entidad receptora cuenta con capacidad para administrar el proyecto.\\n";
            }

            if (ddlOperatividadTab3.SelectedValue == "")
            {
                msj = msj + "*Responder si se encuentra operativa.\\n";
            }
            if (txtFechaActaTransferenciaTab3.Text == "")
            {
                msj = msj + "*Ingresar fecha acta transferencia fisica de la obra.\\n";
            }

            return msj;

        }

        protected void imgbtnSostenibilidadTab3_Click(object sender, ImageClickEventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkbtnSostenibilidadTab3.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnSostenibilidadTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkbtnSostenibilidadTab3.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void lnkbtnBuscarRUCContratistaTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsultorTab1, txtNombConsultorTab1, txtRepresentanteLegalConsultorTab1, divMsjBusquedaRucContratistaTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }


        protected void lnkbtnBuscarRUCConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucConsorcioTab1, txtNomconsorcioTab1, txtRepresentanteTab1, divMsjBusquedaRucConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void lnkbtnBuscarRUCMiembroConsorcioTab1_Click(object sender, EventArgs e)
        {
            _Metodo.buscarRucSunat(txtRucGrupTab1, txtContratistaGrupTab1, txtRepresentanGrupTAb1, divMsjBusquedaRucMiembroConsorcioTab1, LblID_USUARIO.Text);
            Up_rbProcesoTab1.Update();
        }

        protected void btnFinalizarTab3_Click(object sender, EventArgs e)
        {
            if (lblIdEstadoRegistro.Text.Equals("2")) //ACTIVO
            {
                _BEEjecucion.id_estadoProyecto = 3;
            }
            else
            {
                _BEEjecucion.id_estadoProyecto = 2;
            }
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_EstadoProyecto(_BEEjecucion);

            if (val == 1)
            {
                //CargaLiquidacion();

                string script = "<script>alert('Se Actualizó Correctamente'); window.location.reload()</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void lnkbtnSinRuc_Click(object sender, EventArgs e)
        {
            txtNomconsorcioTab1.Enabled = true;
            txtRepresentanteTab1.Enabled = true;
        }

        public void CargarHitos()
        {
            DataTable dthitos = _objBLProceso.spMON_HitosProyecto(Convert.ToInt32(LblID_PROYECTO.Text), 2);
            if (dthitos.Rows.Count > 0)
            {
                DataRow DR = dthitos.Rows[0];
                if (DR["dFechaRegistroParticipantes"] != DBNull.Value)
                    TxtFechaRegistroParticipantes.Text = _Metodo.formatoparafecha(DR["dFechaRegistroParticipantes"].ToString());

                if (DR["dFechaFormulacion"] != DBNull.Value)
                    TxtFechaFormulacion.Text = _Metodo.formatoparafecha(DR["dFechaFormulacion"].ToString());

                if (DR["dFechaIntegracionBases"] != DBNull.Value)
                    TxtFechaIntegracionBases.Text = _Metodo.formatoparafecha(DR["dFechaIntegracionBases"].ToString());

                if (DR["dFechaPreparacionOfertas"] != DBNull.Value)
                    TxtFechaPreparacionOfertas.Text = _Metodo.formatoparafecha(DR["dFechaPreparacionOfertas"].ToString());

                if (DR["dFechaEvaluacionOferta"] != DBNull.Value)
                    TxtFechaEvaluacionOferta.Text = _Metodo.formatoparafecha(DR["dFechaEvaluacionOferta"].ToString());

                if (DR["dFechaPresentacionDocumentoContrato"] != DBNull.Value)
                    TxtFechaPresentacionDocumentoContrato.Text = _Metodo.formatoparafecha(DR["dFechaPresentacionDocumentoContrato"].ToString());

                if (DR["dFechaActoPosteriorFirmaContrato"] != DBNull.Value)
                    TxtFechaActoPosteriorFirmaContrato.Text = _Metodo.formatoparafecha(DR["dFechaActoPosteriorFirmaContrato"].ToString());


            }
        }

        protected void btnVigenciaVencida_Click(object sender, EventArgs e)
        {
            string script = "<script>$('#modalVigencia').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            dt = _objBLFinanciamiento.spMON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text));

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["flagActivoReporte"].ToString() == "1")
                {
                    btnRegistrarVencimiento.Enabled = true;
                }

                if (dt.Rows[0]["fechaUpdateVencimiento"].ToString().Length > 0)
                {
                    ddlFlagVigencia.SelectedValue = dt.Rows[0]["flagVigenciaVencida"].ToString();
                    ddlTipoAccionVigencia.SelectedValue = dt.Rows[0]["idTipoAccionVencimiento"].ToString();

                    if (ddlFlagVigencia.SelectedValue.Equals("1"))
                    {
                        divAccionVigencia.Visible = true;
                    }
                    else
                    {
                        divAccionVigencia.Visible = false;
                        ddlTipoAccionVigencia.SelectedValue = "";
                    }

                    if (ddlTipoAccionVigencia.SelectedValue == "")
                    {
                        divAccionVigencia.Visible = false;
                    }

                    lblNomActualizaVencimiento.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdateVencimiento"].ToString();
                }
            }

            Up_PanelVigencia.Update();
        }

        protected String ValidarVigenciaVencia()
        {
            string msj = "";


            if (ddlFlagVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar etapa con vigencia vencida.\\n";
            }

            if (ddlFlagVigencia.SelectedValue.Equals("1") && ddlTipoAccionVigencia.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de acción.\\n";
            }


            return msj;

        }
        protected void btnRegistrarVencimiento_Click(object sender, EventArgs e)
        {
            string msjVal = ValidarVigenciaVencia();

            if (msjVal.Length > 0)
            {
                string script = "<script>alert('" + msjVal + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                int pTipoAccion = 0;
                if (ddlTipoAccionVigencia.SelectedValue != "")
                {
                    pTipoAccion = Convert.ToInt32(ddlTipoAccionVigencia.SelectedValue);
                }

                DataSet ds = new DataSet();
                ds = _objBLFinanciamiento.paSSP_spiu_MON_VigenciaVencida(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(LblID_USUARIO.Text), Convert.ToInt32(ddlFlagVigencia.SelectedValue), pTipoAccion);

                if (ds.Tables[0].Rows[0]["vCod"].ToString() == "200")
                {
                    string script = "<script>$('#modalVigencia').modal('hide');alert('Se registró la vigencia de vencimiento. Se actualizará la página');window.location.reload()</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Hubo un error, volver a intentar.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void ddlFlagVigencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFlagVigencia.SelectedValue.Equals("1"))
            {
                divAccionVigencia.Visible = true;
            }
            else
            {
                divAccionVigencia.Visible = false;
                ddlTipoAccionVigencia.SelectedValue = "";
            }

            Up_PanelVigencia.Update();
        }


        protected void CargarIncorporacionRegistro()
        {
            DataSet DSIR = _objBLFinanciamiento.spd_MON_ObtenerIncorporacionRegistro(Convert.ToInt32(LblID_PROYECTO.Text));
            if (DSIR.Tables[0].Rows.Count > 0)
            {
                ChkIncorporaRecursos.Checked = Convert.ToBoolean(DSIR.Tables[0].Rows[0]["Incorporado"]);
                if (DSIR.Tables[0].Rows[0]["FechaIncorporacion"] != DBNull.Value)
                    TxtFechaIncorporacionRecurso.Text = Convert.ToDateTime(DSIR.Tables[0].Rows[0]["FechaIncorporacion"]).ToString("dd/MM/yyyy");
                if (DSIR.Tables[0].Rows[0]["MontoIncorporado"] != DBNull.Value)
                    TxtMontoIncorporado.Text = DSIR.Tables[0].Rows[0]["MontoIncorporado"].ToString();
                if (DSIR.Tables[0].Rows[0]["ActaResolucionAlcaldia"] != DBNull.Value)
                    LbIncorporacionRecursos.Text = DSIR.Tables[0].Rows[0]["ActaResolucionAlcaldia"].ToString();

                ChkRequiereActualizacionET.Checked = Convert.ToBoolean(DSIR.Tables[0].Rows[0]["RequiereActualizacionETCOVID19"]);
                ChkCuentaPartidaCOVID19.Checked = Convert.ToBoolean(DSIR.Tables[0].Rows[0]["ETCuentaPartidaCOVID19"]);
                ChkTieneResolucionAprobacionET.Checked = Convert.ToBoolean(DSIR.Tables[0].Rows[0]["TieneResolucionAprobacionET"]);


                GeneraIcoFile(LbIncorporacionRecursos.Text, IbIncorporacionRecursos);

            }
        }

        protected void IbIncorporacionRecursos_Click(object sender, ImageClickEventArgs e)
        {
            LbIncorporacionRecursos_Click(null, null);
        }

        protected void LbIncorporacionRecursos_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LbIncorporacionRecursos.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LbIncorporacionRecursos.Text);
                Response.End();
            }
        }

        protected void BtnGuardarIncorporacionRecursos_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaArchivo(FuRecursoTransferido))
                {
                    //validar
                    Boolean paso = true;
                    string mensaje = "";
                    string archivo = "";
                    int incorporado = 0;
                    int RequiereActualizacionET = (ChkRequiereActualizacionET.Checked ? 1 : 0);
                    int CuentaPartidaCOVID19 = (ChkCuentaPartidaCOVID19.Checked ? 1 : 0);
                    int TieneResolucionAprobacionET = (ChkTieneResolucionAprobacionET.Checked ? 1 : 0);



                    DateTime? fecha = null;
                    decimal? monto = null;
                    if (ChkIncorporaRecursos.Checked)
                    {
                        incorporado = 1;

                        if (TxtFechaIncorporacionRecurso.Text.ToString() == "")
                        {
                            paso = false;
                            mensaje += "*falta ingresar fecha de incorporación de recursos\\n";
                        }
                        else
                        {
                            fecha = Convert.ToDateTime(TxtFechaIncorporacionRecurso.Text);
                        }

                        if (TxtMontoIncorporado.Text.ToString() == "")
                        {
                            paso = false;
                            mensaje += "*falta ingresar monto de incorporación de recursos\\n";
                        }
                        else
                        {
                            monto = Convert.ToDecimal(TxtMontoIncorporado.Text);
                        }
                    }

                    if (paso)
                    {
                        if (FuRecursoTransferido.HasFile)
                        {
                            archivo = _Metodo.uploadfile(FuRecursoTransferido);
                        }
                        else
                        {
                            archivo = LbIncorporacionRecursos.Text;
                        }

                        _objBLFinanciamiento.spd_MON_IncorporacionRegistro(Convert.ToInt32(LblID_PROYECTO.Text), incorporado, fecha, monto, archivo, Convert.ToInt32(LblID_USUARIO.Text),
                               RequiereActualizacionET, CuentaPartidaCOVID19, TieneResolucionAprobacionET);
                        LbIncorporacionRecursos.Text = archivo;
                        GeneraIcoFile(LbIncorporacionRecursos.Text, IbIncorporacionRecursos);

                        string script = "<script>alert('Se guardo correctamente la incorporación de registros');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script5", script, false);
                    }
                    else
                    {
                        string script = "<script>alert('" + mensaje + "');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script555", script, false);
                    }

                }
            }
            catch (Exception ex)
            {
                string script = "<script>alert('Ocurrio un error al guardar la incorporación de registros');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script3", script, false);
            }
        }

        protected void TabActivoProceso()
        {
            if (grSeguimientoSeace.Rows.Count > 0)
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }
            else if (grdSeguimientoProcesoTab1.Rows.Count > 0 || txtValorReferencialTab1.Text.Length > 0)
            {
                PanelSeaceAuto.Visible = false;
                PanelSeaceManual.Visible = true;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";
            }
            else
            {
                PanelSeaceAuto.Visible = true;
                PanelSeaceManual.Visible = false;
                lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
                lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";
            }

            //Por ahora se desactiva todo 2020.11.13
            lnkbtnSeaceAutoPanel.Visible = false;
            lnkbtnSeaceManualPanel.Visible = false;
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
        }
        protected void CargaCodigoSeace()
        {
            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            grSeguimientoSeace.DataSource = _objBLFinanciamiento.spMON_CodigoSeace(pIdProyecto, Convert.ToInt32(ddlProcesoTab1.SelectedValue));
            grSeguimientoSeace.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }

        protected void CargaProcesosRelSEACE()
        {
            int snip = Convert.ToInt32(lblSNIP.Text);

            grdSEACEObra.DataSource = _objBLFinanciamiento.spMON_SEACE_Obra(snip, Convert.ToInt32(LblID_PROYECTO.Text));
            grdSEACEObra.DataBind();
            //totalMontoGrdTransferenciaTab0();
        }
        protected void grdSEACEObra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSEACEObra.PageIndex = e.NewPageIndex;
            CargaProcesosRelSEACE();
        }
        protected void BtnRegistrarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            if (TxtCodigoSeace.Text.Length == 0)
            {
                string script = "<script>alert('Inresar código de SEACE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script3", script, false);
            }
            else
            {
                CodigosSeace _BE = new CodigosSeace();
                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(TxtCodigoSeace.Text);
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;
                
                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se registro correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void btnCancelarSeguimientoSeace_Click(object sender, EventArgs e)
        {
            Panel_RegistroSeace.Visible = false;
            imgbtnAgregarSeguimientoSeace.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtnAgregarSeguimientoSeace_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RegistroSeace.Visible = true;
            imgbtnAgregarSeguimientoSeace.Visible = false;
            Up_Tab1.Update();
        }

        protected void grSeguimientoSeace_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grSeguimientoSeace.DataKeys[CurrentRow.RowIndex].Value as object;

                CodigosSeace _BE = new CodigosSeace();
                
                _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BE.Codigo = Convert.ToInt32(objTemp.ToString());
                _BE.idEtapa = Convert.ToInt32(ddlProcesoTab1.SelectedValue);
                _BE.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BE.Activo = true;

                int val = _objBLBandeja.spiu_MON_RegistrarCodigosSeace(_BE);

                if (val == 1)
                {
                    string script = "<script>alert('Se eliminó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    TxtCodigoSeace.Text = "";
                    Panel_RegistroSeace.Visible = false;
                    imgbtnAgregarSeguimientoSeace.Visible = true;

                    CargaCodigoSeace();
                    CargaProcesosRelSEACE();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grSeguimientoSeace_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                Label lblEstadoItem = (Label)e.Row.FindControl("lblEstadoItem");
                Label lblEstado = (Label)e.Row.FindControl("lblEstado");
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");

                string caseSwitch = lblEstadoItem.Text;

                lblEstado.Text = lblEstadoItem.Text;
                lblEstado.ToolTip = lblEstadoItem.Text;

                switch (caseSwitch)
                {
                    case "APELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "CANCELADO POR RECORTE PRESUPUESTAL":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "DESIERTO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUBSCRIPCION DEL CONTRATO POR DECISION DE LA ENTIDAD":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NO SUSCRIBIO EL CONTRATO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "NULO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "SUSPENDIDO":
                        lblEstado.CssClass = "label label-danger";
                        break;
                    case "ADJUDICADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "ADJUDICADO A PRORRATA":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONSENTIDO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    case "CONTRATADO":
                        lblEstado.CssClass = "label label-success";
                        break;
                    default:
                        lblEstado.CssClass = "label label-primary";
                        break;
                }

                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");

                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;

                }
            }
        }

        protected void grdSEACEObra_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNroItem = (Label)e.Row.FindControl("lblNroItem");
                if (lblNroItem.Text != "1")
                {
                    Label lblMsjNroItem = (Label)e.Row.FindControl("lblMsjNroItem");
                    lblMsjNroItem.Text = "(" + lblNroItem.Text + " items)";
                    lblMsjNroItem.Visible = true;
                }
            }
        }
        protected void lnkbtnSeaceAutoPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = true;
            PanelSeaceManual.Visible = false;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabActivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabDesactivado";

            Up_Tab1.Update();
        }

        protected void lnkbtnSeaceManualPanel_Click(object sender, EventArgs e)
        {
            PanelSeaceAuto.Visible = false;
            PanelSeaceManual.Visible = true;
            lnkbtnSeaceAutoPanel.CssClass = "btn btnTabDesactivado";
            lnkbtnSeaceManualPanel.CssClass = "btn btnTabActivado";

            Up_Tab1.Update();
        }

        protected void ddlTipoAccion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoAccion.SelectedValue.ToString().Equals("2"))
            {
                lblTitDocAccion.Text = "Fecha de Renovación";
                txtFechaRenovacion.Visible = true;
                txtFechaRenovacion.Text = "";
                FileUploadAccionTab2.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else if (ddlTipoAccion.SelectedValue.ToString().Equals("3") || ddlTipoAccion.SelectedValue.ToString().Equals("4"))
            {
                lblTitDocAccion.Text = "Doc. " + ddlTipoAccion.SelectedItem.Text;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                FileUploadAccionTab2.Visible = true;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }
            else
            {
                lblTitDocAccion.Text = "";
                FileUploadAccionTab2.Visible = false;
                txtFechaRenovacion.Text = "";
                txtFechaRenovacion.Visible = false;
                //imgbtnAccionTab2.ImageUrl = "~/img/blanco.png";
                //lnkbtnAccionTab2.Text = "";
            }

            Up_Tab2.Update();
        }

        protected void lnkbtnAccionTab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnAccionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocAccionGriTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdFianzasTab2.Rows[row.RowIndex].FindControl("imgDocAccionGriTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected Boolean verificarAdelanto()
        {
            string result = "";


            if (txtFechaAdelantoTab2.Text == "")
            {
                result = result + "*Ingresar Fecha de adelanto.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaAdelantoTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha de adelanto.\\n";
            }
            else
            {
                if (Convert.ToDateTime(txtFechaAdelantoTab2.Text) > DateTime.Today)
                {
                    result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

                }
            }

            if (ddlTipoAdelantoTab2.SelectedValue == "")
            {
                result = result + "*Seleccionar el tipo de adelanto.\\n";
            }

            if (txtMontoAdelantoTab2.Text == "")
            {
                result = result + "*Ingresar monto de adelanto.\\n";
            }


            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void grdAdelantoObraTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblIdAdelantoTab2.Text = grdAdelantoObraTab2.SelectedDataKey.Value.ToString();

            GridViewRow row = grdAdelantoObraTab2.SelectedRow;

            txtFechaAdelantoTab2.Text = ((Label)row.FindControl("lblFechaAdelanto")).Text;
            txtNroAdelantoTab2.Text = ((Label)row.FindControl("lblNroAdelanto")).Text;
            txtMontoAdelantoTab2.Text = ((Label)row.FindControl("lblMonto")).Text;
            ddlTipoAdelantoTab2.SelectedValue = ((Label)row.FindControl("lblIdTipoAdelanto")).Text;

            lblNomUsuarioAdelantoTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;


            Panel_RegistroAdelantoTab2.Visible = true;
            imgbtnAgregarAdelantoTab2.Visible = false;
            btnGuardarAdelantoTab2.Visible = false;
            btnModificarAdelantoTab2.Visible = true;

            Up_Tab2.Update();
        }

        protected void grdAdelantoObraTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAdelantoObraTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.Date_fecha = Convert.ToDateTime("01/01/1990");
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                dt = new DataTable();
                dt = _objBLEjecucion.spud_MON_Adelanto(_BEEjecucion);

                if (dt.Rows[0]["vCod"].ToString().Equals("200"))
                {
                    string script = "<script>alert('Se eliminó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroAdelantoTab2.Visible = false;
                    imgbtnAgregarAdelantoTab2.Visible = true;
                    CargaAdelanto();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

            }
        }

        protected void imgbtnAgregarAdelantoTab2_Click(object sender, ImageClickEventArgs e)
        {
            ddlTipoAdelantoTab2.SelectedValue = "";
            lblNomUsuarioAdelantoTab2.Text = "";
            txtNroAdelantoTab2.Text = "";
            txtMontoAdelantoTab2.Text = "";
            txtFechaAdelantoTab2.Text = "";
            Panel_RegistroAdelantoTab2.Visible = true;
            imgbtnAgregarAdelantoTab2.Visible = false;
            btnGuardarAdelantoTab2.Visible = true;
            btnModificarAdelantoTab2.Visible = false;

            Up_Tab2.Update();
        }

        protected void btnGuardarAdelantoTab2_Click(object sender, EventArgs e)
        {
            if (verificarAdelanto())
            {
                _BEEjecucion = new BE_MON_Ejecucion();

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.flag = 1;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaAdelantoTab2.Text);
                _BEEjecucion.nroAdelanto = txtNroAdelantoTab2.Text;
                _BEEjecucion.monto = txtMontoAdelantoTab2.Text;
                _BEEjecucion.idTipoAdelanto = ddlTipoAdelantoTab2.SelectedIndex;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                dt = new DataTable();
                dt = _objBLEjecucion.spi_MON_Adelanto(_BEEjecucion);

                if (dt.Rows[0]["vCod"].ToString().Equals("200"))
                {
                    string script = "<script>alert('Se registró correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroAdelantoTab2.Visible = false;
                    imgbtnAgregarAdelantoTab2.Visible = true;
                    CargaAdelanto();
                    Up_Tab2.Update();
                }
                else if (dt.Rows[0]["vCod"].ToString().Equals("0"))
                {
                    string script = "<script>alert('" + dt.Rows[0]["vDesc"].ToString() + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void btnModificarAdelantoTab2_Click(object sender, EventArgs e)
        {
            if (verificarAdelanto())
            {
                _BEEjecucion = new BE_MON_Ejecucion();

                _BEEjecucion.id_item = Convert.ToInt32(lblIdAdelantoTab2.Text);
                _BEEjecucion.Tipo = 1;
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaAdelantoTab2.Text);
                _BEEjecucion.nroAdelanto = txtNroAdelantoTab2.Text;
                _BEEjecucion.monto = txtMontoAdelantoTab2.Text;
                _BEEjecucion.idTipoAdelanto = ddlTipoAdelantoTab2.SelectedIndex;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                dt = new DataTable();
                dt = _objBLEjecucion.spud_MON_Adelanto(_BEEjecucion);

                if (dt.Rows[0]["vCod"].ToString().Equals("200"))
                {
                    string script = "<script>alert('Se actualizó correctamente'); </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistroAdelantoTab2.Visible = false;
                    imgbtnAgregarAdelantoTab2.Visible = true;
                    CargaAdelanto();
                    Up_Tab2.Update();
                }
                else if (dt.Rows[0]["vCod"].ToString().Equals("0"))
                {
                    string script = "<script>alert('" + dt.Rows[0]["vDesc"].ToString() + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void btnCancelarAdelantoTab2_Click(object sender, EventArgs e)
        {
            Panel_RegistroAdelantoTab2.Visible = false;
            imgbtnAgregarAdelantoTab2.Visible = true;
            Up_Tab2.Update();
        }

        public void CargaAdelanto()
        {
            BE_MON_Ejecucion _BE = new BE_MON_Ejecucion();
            _BE.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BE.flag = 1;

            dt = _objBLEjecucion.spMON_Adelanto(_BE);

            grdAdelantoObraTab2.DataSource = dt;
            grdAdelantoObraTab2.DataBind();

        }

      
    }
}
