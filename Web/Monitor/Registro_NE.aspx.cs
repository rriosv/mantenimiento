﻿using DevExpress.Web;
using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class Registro_NE : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();
         
        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();

        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();
        BL_MON_Riesgo _objBLRiesgo = new BL_MON_Riesgo();
        BE_MON_Riesgo _BERiesgo = new BE_MON_Riesgo();

        DataTable dt = new DataTable();
        BLUtil _Metodo = new BLUtil();

        protected void Page_Load(object sender, EventArgs e)
        {
            cargaPadronMiembrosNE();
            int idProy = Convert.ToInt32(Request.QueryString["id"]);

            if (!IsPostBack)
            {
                if (Session["LoginUsuario"] == null)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                    Response.Redirect("~/login.aspx?ms=1");
                }
                BtnRefrescar_Click(null, null);


                LblID_PROYECTO.Text = Request.QueryString["id"].ToString();
                LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();
                lblID_PERFIL_USUARIO.Text = Session["PerfilUsuario"].ToString();
                lblCOD_SUBSECTOR.Text = Session["CodSubsector"].ToString();

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int idacceso = _objBLProceso.spMON_Acceso(_BEProceso);
                lblID_ACCESO.Text = idacceso.ToString();

                //grdAvanceFisicoTab2.Columns[6].Visible = false;

                if (idacceso == 0)
                {
                    OcultarRegistro();
                }

                CargaTab0();
                CargaTab1();
                CargaTab4();
                CargaTab2();
                CargaTab3();

                CargaTab6();
                //CargaTab7();
                CargaTab9();

                if (lblCOD_SUBSECTOR.Text.Equals("1") || lblCOD_SUBSECTOR.Text.Equals("3") || lblCOD_SUBSECTOR.Text.Equals("7") || lblCOD_SUBSECTOR.Text.Equals("2"))
                {
                    CargarMetas(Convert.ToInt32(LblID_PROYECTO.Text));
                }

                //Acceso a monitor para eliminar registros de visitas
                if (LblID_USUARIO.Text.Equals("481"))
                {
                    grdEvaluacionTab2.Columns[22].Visible = true;
                }

                // COORDINADORES PMIB , PNSU, PNSR, PNVR, PASLC
                if (lblIdEstadoRegistro.Text.Equals("3") && (lblID_PERFIL_USUARIO.Text.Equals("23") || lblID_PERFIL_USUARIO.Text.Equals("24") || lblID_PERFIL_USUARIO.Text.Equals("71") || lblID_PERFIL_USUARIO.Text.Equals("74") || lblID_PERFIL_USUARIO.Text.Equals("76") || LblID_USUARIO.Text.Equals("481")))
                {
                    btnFinalizarTab3.Visible = true;
                }
                else if (lblIdEstadoRegistro.Text.Equals("3"))
                {
                    btnFinalizarTab3.Visible = false;
                }


                if (lblCOD_SUBSECTOR.Text.Equals("5"))
                {
                    trViviendaMonitoreoTab4.Visible = true;
                }

            }

            CargaPlantillaFichaRiesgoTab9(0);

         
        }

        protected void OcultarRegistro()
        {
            Panel_MiembrosNE.Visible = false;
            //btnFlagFinalizarTab3.Visible = false;
                     
            btnGuardarTab0.Visible = false;
            btnGuardarProcesoIndirectaNE.Visible = false;
            btnGuardarConvenioNE.Visible = false;
            btnGuardarCuentaBancariaTab1.Visible = false;
            btnGuardarSesionesTab1.Visible = false;
            btnGenerarProgramacionTab2.Visible = false;
            imgbtnAgregaMiembroNucleoTab1.Visible = false;

            grdMiembroNucleoTAb1.Columns[10].Visible = false;

            imgbtnAgregarAgenteTab2.Visible = false;

            imgbtn_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = false;

            imgbtnAvanceFisicoTab2.Visible = false;
            imgbtnPlaTab4.Visible = false;
            imgbtnPreTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = false;
            //btnGuardarTab7.Visible = false;
            btnGuardarTab3.Visible = false;
            //btnGuardarLiquidacionPNVRTab3.Visible = false;

            btnGuardarTab1.Visible = false;
            btnCambiarModalidadTab1.Visible = false;
            btnGuardarDirectoTab1.Visible = false;
            btnGuardarInfoTab2.Visible = false;

            grdSeguimientoProcesoTab1.Columns[0].Visible = false;
            grdSeguimientoProcesoTab1.Columns[11].Visible = false;
            grdConsultoresTab1.Columns[0].Visible = false;
            grdConsultoresTab1.Columns[5].Visible = false;
            grdSubcontratosTab1.Columns[0].Visible = false;
            grdSubcontratosTab1.Columns[13].Visible = false;
            imgbtnAgregarSubcontratoTab1.Visible = false;

            grdAvanceFisicoTab2.Columns[0].Visible = false;
            grdAvanceFisicoTab2.Columns[18].Visible = false;

            imgbtnAvanceFisicoTab2.Visible = false;

            grdAvanceFisicoAdicionalTab2.Columns[0].Visible = false;
            grdAvanceFisicoAdicionalTab2.Columns[16].Visible = false;

            imgbtnAvanceFisicoAdicionalTab2.Visible = false;

            grdEvaluacionTab2.Columns[0].Visible = false;
            grdEvaluacionTab2.Columns[22].Visible = false;
            imgbtnAgregarEvaluacionTab2.Visible = false;


            grdPlazoTab4.Columns[0].Visible = false;
            grdPlazoTab4.Columns[11].Visible = false;
            imgAgregaPlazoTab4.Visible = false;
            grdPresupuestoTab4.Columns[0].Visible = false;
            grdPresupuestoTab4.Columns[12].Visible = false;
            grdDeductivoTab4.Columns[0].Visible = false;
            grdDeductivoTab4.Columns[12].Visible = false;

            grdPanelTab6.Columns[0].Visible = false;
            grdPanelTab6.Columns[8].Visible = false;
            imgbtnAgregarPanelTab6.Visible = false;

            //grdHistorialTab8.Columns[0].Visible = false;
            //grdHistorialTab8.Columns[7].Visible = false;
            //imgbtnAgregarHistorialTab8.Visible = false;

            //btnFlagDesFinalizarTab3.Visible = false;

            btnPadronBeneficiario.Visible = false;
            



            grdUbicacionMetaTab0.Columns[0].Visible = false;
            grdUbicacionMetaTab0.Columns[8].Visible = false;
            imgbtnAgregarUbicacionMetasTab0.Visible = false;
            grdTamboProyectoTab0.Columns[2].Visible = false;
            imgbtnAgregarTamboTab0.Visible = false;
            btnGuardarAdicionalTab0.Visible = false;

            btnActualizaSolicitudTab1.Visible = false;

            grdParalizacionTab4.Columns[0].Visible = false;
            grdParalizacionTab4.Columns[11].Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = false;

            grdAgenteTab2.Columns[0].Visible = false;
            grdAgenteTab2.Columns[10].Visible = false;

            grdProgramacionTab2.Columns[0].Visible = false;


            grdRendicionCuentaNETab1.Columns[0].Visible = false;
            grdRendicionCuentaNETab1.Columns[6].Visible = false;

            imgbtnAgregarRendicionCuentaTab1.Visible = false;
            grdDesembolsosTab2.Columns[0].Visible = false;
            grdDesembolsosTab2.Columns[7].Visible = false;
            btnGuardarCulminacionProyectoTab2.Visible = false;
            imgbtnAgregarDesembolsosTab2.Visible = false;

            btnActualizarSaldoTab2.Visible = false;
            btnGuardarCierreTab3.Visible = false;


            grdViviendasLiquidadas.Columns[0].Visible = false;

            //btnGuardarConexionTab2.Visible = false;

            trCoordenadaTabMeta.Visible = false;
            lblMsjMetaTabH.Visible = false;

            #region Riesgo Tab9
            imgbtnAgregaRiesgosTab9.Visible = false;
            //grdRiesgosTab9.Columns[0].Visible = false;
            grdRiesgosTab9.Columns[5].Visible = false;
            grdRiesgosTab9.Columns[6].Visible = false;
            #endregion

            btnFinalizarTab3.Visible = false;
            UPTabContainerDetalles.Update();
        }

        protected void CargarMetas(int idProy)
        {

            DataSet dsMet = new DataSet();
            dsMet = _objBLFinanciamiento.paSSP_MON_rMetas(idProy, 2, 0);
            DataTable dtSistema = dsMet.Tables[0];
            DataTable dtProgamado = dsMet.Tables[1];
            DataTable dtEjecutado = dsMet.Tables[2];

            string htmlProgTotal = "";
            string htmlEjecTotal = "";
            foreach (DataRow rowSistema in dtSistema.Rows)
            {
                string htmlProg = "";
                foreach (DataRow rowP in dtProgamado.Rows)
                {
                    if (rowP["idSistema"].ToString().Equals(rowSistema["idSistema"].ToString()))
                    {
                        if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
                        {
                            htmlProg = htmlProg +
                                         "<tr>" +
                                              "<td>" + rowP["COMPONENTE"].ToString() + "</td>" +
                                              "<td>" + rowP["Cantidad"].ToString() + "</td>" +
                                              "<td>" + rowP["Costo"].ToString() + "</td>" +
                                         "</tr>";
                        }
                        else //SANEAMIENTO
                        {
                            htmlProg = htmlProg +
                                       "<tr>" +
                                            "<td>" + rowP["COMPONENTE"].ToString() + "</td>" +
                                            "<td>" + rowP["TIPOLOGIA1"].ToString() + "</td>" +
                                            "<td>" + rowP["TIPOLOGIA2"].ToString() + "</td>" +
                                            "<td>" + rowP["NATURALEZA"].ToString() + "</td>" +
                                            "<td>" + rowP["Cantidad"].ToString() + "</td>" +
                                            "<td>" + rowP["Capacidad"].ToString() + "</td>" +
                                            "<td>" + rowP["Costo"].ToString() + "</td>" +
                                       "</tr>";
                        }
                    }
                }

                if (htmlProg.Length > 0)
                {
                    if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
                    {
                        htmlProgTotal = htmlProgTotal +
                                    "<table class='table table-bordered'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        "<tr>" +
                                        "<th>Componente</th>" +
                                        "<th>Cantidad</th>" +
                                        "<th>Costo S/</th>" +
                                        "</tr>" +
                                        htmlProg +
                                    "</table>";
                    }
                    else
                    {
                        htmlProgTotal = htmlProgTotal +
                                    "<table class='table table-bordered'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        "<tr>" +
                                        "<th>Componente</th>" +
                                        "<th>Tipología 1</th>" +
                                        "<th>Tipología 2</th>" +
                                        "<th>Naturaleza de <br> Intervención</th>" +
                                        "<th>Cantidad</th>" +
                                        "<th>Capacidad</th>" +
                                        "<th>Costo S/</th>" +
                                        "</tr>" +
                                        htmlProg +
                                    "</table>";
                    }
                }

                string htmlEjec = "";
                foreach (DataRow rowP in dtEjecutado.Rows)
                {
                    if (rowP["idSistema"].ToString().Equals(rowSistema["idSistema"].ToString()))
                    {
                        if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
                        {
                            htmlEjec = htmlEjec +
                                   "<tr>" +
                                        "<td>" + rowP["COMPONENTE"].ToString() + "</td>" +
                                        "<td>" + rowP["Cantidad"].ToString() + "</td>" +
                                        "<td>" + rowP["Costo"].ToString() + "</td>" +
                                   "</tr>";
                        }
                        else
                        {
                            htmlEjec = htmlEjec +
                                   "<tr>" +
                                        "<td>" + rowP["COMPONENTE"].ToString() + "</td>" +
                                        "<td>" + rowP["TIPOLOGIA1"].ToString() + "</td>" +
                                        "<td>" + rowP["TIPOLOGIA2"].ToString() + "</td>" +
                                        "<td>" + rowP["NATURALEZA"].ToString() + "</td>" +
                                        "<td>" + rowP["Cantidad"].ToString() + "</td>" +
                                        "<td>" + rowP["Capacidad"].ToString() + "</td>" +
                                        "<td>" + rowP["Costo"].ToString() + "</td>" +
                                   "</tr>";
                        }
                    }
                }

                if (htmlEjec.Length > 0)
                {
                    if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
                    {
                        htmlEjecTotal = htmlEjecTotal +
                                   "<table class='table table-bordered'> " +
                                       "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                       "<tr>" +
                                       "<th>Componente</th>" +
                                       //"<th>Tipología 1</th>" +
                                       //"<th>Tipología 2</th>" +
                                       //"<th>Naturaleza de <br> Intervención</th>" +
                                       "<th>Cantidad</th>" +
                                       //"<th>Capacidad</th>" +
                                       "<th>Costo S/</th>" +
                                       "</tr>" +
                                       htmlEjec +
                                   "</table>";
                    }
                    else
                    {
                        htmlEjecTotal = htmlEjecTotal +
                                    "<table class='table table-bordered'> " +
                                        "<tr><th colspan='7'>" + rowSistema["vDescripcion"].ToString() + "</th></tr>" +
                                        "<tr>" +
                                        "<th>Componente</th>" +
                                        "<th>Tipología 1</th>" +
                                        "<th>Tipología 2</th>" +
                                        "<th>Naturaleza de <br> Intervención</th>" +
                                        "<th>Cantidad</th>" +
                                        "<th>Capacidad</th>" +
                                        "<th>Costo S/</th>" +
                                        "</tr>" +
                                        htmlEjec +
                                    "</table>";
                    }
                }
            }

            if (lblCOD_SUBSECTOR.Text.Equals("2")) //PMIB
            {
                divMetaProg.Style.Add("width", "85%");
                divMetaEjec.Style.Add("width", "85%");
            }

            if (htmlProgTotal.Equals(""))
            {
                htmlProgTotal = "<table class='table table-bordered'> " +
                                       "<tr><td>No hay registro</td></tr>" +
                                   "</table>";
            }

            if (htmlEjecTotal.Equals(""))
            {
                htmlEjecTotal = "<table class='table table-bordered'> " +
                                       "<tr><td>No hay registro</td></tr>" +
                                   "</table>";
            }

            divMetaProg.InnerHtml = htmlProgTotal;
            divMetaEjec.InnerHtml = htmlEjecTotal;

        }

        protected Boolean validaArchivoFotos(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".bmp" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".jpg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar solo fotos en formato: BMP, JPG, JPEG o PNG.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hasta 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }
            return result;
        }

     
        protected Boolean validaArchivo(FileUpload file)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > 11912320)
                {
                    script = "<script>alert('Archivo solo hasta 10 MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        protected Boolean validaArchivo(FileUpload file, int tamanioMB)
        {
            Boolean result;
            result = true;
            string script = "";
            string ext = Path.GetExtension((file.FileName));

            if ((file.HasFile) == true)
            {
                if (ext.ToLower() != ".pdf" && ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx" && ext.ToLower() != ".doc" && ext.ToLower() != ".docx" && ext.ToLower() != ".bmp" && ext.ToLower() != ".jpg" && ext.ToLower() != ".jpeg" && ext.ToLower() != ".png")
                {
                    script = "<script>alert('Ingresar Otro Tipo de Archivo (PDF, Excel, Word, BMP, JPG, JPEG o PNG)');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }

                if (file.PostedFile.ContentLength > (1048576 * tamanioMB))
                {
                    script = "<script>alert('Archivo solo hasta " + tamanioMB.ToString() + " MB.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    result = false;
                    return result;
                }
            }

            return result;

        }

        public bool ValidaDecimal(string inValue)
        {
            bool bValid;
            try
            {
                decimal myDT = Convert.ToDecimal(inValue);
                bValid = true;

            }
            catch (Exception e)
            {
                bValid = false;
            }


            return bValid;
        }

        protected void GeneraIcoFile(string lnk, ImageButton imgbtn)
        {
            if (lnk != "")
            {

                string ext = lnk.Substring(lnk.Length - 3, 3);
                ext = ext.ToLower();

                if (ext == "pdf")
                {
                    imgbtn.ImageUrl = "~/img/pdf.gif";
                }

                if (ext == "xls" || ext == "lsx")
                {
                    imgbtn.ImageUrl = "~/img/xls.gif";

                }

                if (ext == "doc" || ext == "ocx")
                {
                    imgbtn.ImageUrl = "~/img/doc.gif";

                }

                if (ext == "png" || ext == "PNG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }
                if (ext == "jpg" || ext == "JPG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "peg" || ext == "PEG")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                if (ext == "bmp" || ext == "BMP")
                {
                    imgbtn.ImageUrl = "~/img/jpg.gif";

                }

                imgbtn.ToolTip = lnk;
            }
            else
            {
                imgbtn.ToolTip = "";
                imgbtn.ImageUrl = "~/img/blanco.png";
            }

        }

        #region Tab0
 

        protected void CargaTab0()
        {
            CargaTipologia();
            CargaTipoDenominacionTab0();
            CargaDatosGeneralesTab0();
            cargaGastosSAV();

        }

        protected void cargaGastosSAV()
        {
            DataTable dt = new DataTable();
            dt = _objBLFinanciamiento.spMON_GastosSAV_NE(Convert.ToInt32(LblID_PROYECTO.Text));
            grdGastosSAV.DataSource = dt;
            grdGastosSAV.DataBind();

            var sum = dt.Compute("Sum(MontoGirado)", "");

            if (sum.ToString() != "")
            {
                lblMontoSAVTab0.Text = Convert.ToDouble(sum).ToString("N2");
            }

        }
        protected void CargaTipologia()
        {
            xcblTipologia.DataSource = _objBLBandeja.F_spMON_TipoTipologia();
            xcblTipologia.TextField = "nombre";
            xcblTipologia.ValueField = "valor";
            xcblTipologia.DataBind();

        }

        protected void CargaTipoDenominacionTab0()
        {
            ddlTipoDenominacionFuenteTab0.DataSource = _objBLBandeja.F_spMON_TipoDenominacionFuente();
            ddlTipoDenominacionFuenteTab0.DataValueField = "valor";
            ddlTipoDenominacionFuenteTab0.DataTextField = "nombre";
            ddlTipoDenominacionFuenteTab0.DataBind();

            ddlTipoDenominacionFuenteTab0.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }
        protected void CargaDatosGeneralesTab0()
        {
            ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
            dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
            if (dt.Rows.Count > 0)
            {
                lblNombProy.Text = dt.Rows[0]["nom_proyecto"].ToString();

                string depa = dt.Rows[0]["depa"].ToString();
                string prov = dt.Rows[0]["prov"].ToString();
                string dist = dt.Rows[0]["dist"].ToString();


                CargaCCPPTab0(dt.Rows[0]["cod_depa"].ToString(), dt.Rows[0]["cod_prov"].ToString(), dt.Rows[0]["cod_dist"].ToString());
                lblIdUbigeo.Text = dt.Rows[0]["cod_depa"].ToString() + dt.Rows[0]["cod_prov"].ToString() + dt.Rows[0]["cod_dist"].ToString();
                lblSNIP.Text = dt.Rows[0]["cod_snip"].ToString();
                string UE = dt.Rows[0]["entidad_ejec"].ToString();
                txtUnidadEjecutora.Text = UE;
                lblUE.Text = " / UE: " + UE;
                //lblPrograma.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                LblID_SOLICITUD.Text = dt.Rows[0]["id_solicitudes"].ToString();


                lblUBICACION.Text = depa + " - " + prov + " - " + dist;

                lblCOD_SUBSECTOR.Text = dt.Rows[0]["id_tipoPrograma"].ToString();
                lblTipoFinanciamiento.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();


                lblIdEstadoRegistro.Text = dt.Rows[0]["idEstadoProyecto"].ToString();
                if (lblIdEstadoRegistro.Text.Equals("3"))
                {
                    lblEstadoRegistro.Text = "FINALIZADO";
                    btnFinalizarTab3.Text = "Activar";
                }
                else
                {
                    lblEstadoRegistro.Text = "ACTIVADO";
                    btnFinalizarTab3.Text = "Finalizar";
                }

                if (lblCOD_SUBSECTOR.Text == "3") //PNSR
                {
                    Panel_tblPNSR.Visible = true;
                    //******************** DATOS GENERALES ****************************//
                    lblProgramaTab0.Text = dt.Rows[0]["NombPrograma"].ToString();
                    lblSubProgramaTab0.Text = dt.Rows[0]["NombSubPrograma"].ToString();

                    lblCodInternoTab0.Text = Convert.ToInt32(LblID_PROYECTO.Text).ToString("D10");
                    lblUbigeoTab0.Text = lblUBICACION.Text;
                    string CCPP = dt.Rows[0]["ccpp"].ToString();

                    if (CCPP.Length > 0)
                    {
                        lblCCPP.Text = "(" + CCPP.Substring(0, CCPP.Length - 2) + ")";
                    }
                    else
                    {
                        lblCCPP.Text = "";
                    }
                    lblEtapaTab0.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();
                    lblMontoSnipTab0.Text = Convert.ToDouble(dt.Rows[0]["monto_snip"].ToString()).ToString("N");
                    lblFechaViabilidaTab0.Text = dt.Rows[0]["fecha_viabilidad"].ToString();

                    lblFechaEstadoTab0.Text = dt.Rows[0]["fechaUpdateEstado"].ToString();
                    lblEstadoTab0.Text = dt.Rows[0]["estadoEjecucion"].ToString();
                    lblPoblacionSNIPTab0.Text = dt.Rows[0]["num_hab_snip"].ToString();
                    lblEntidadEjecutoraTab0.Text = dt.Rows[0]["entidad_ejec"].ToString();
                    lblVRAETab0.Text = "VRAE - " + dt.Rows[0]["VRAE"].ToString();
                    lblCuencaTab0.Text = " Cuenca Geografica - ";
                    lblTambosRuralesTab0.Text = " Tambos Rurales - ";
                    lblJuntosTab0.Text = " Juntos - ";
                    lblUnidadFormuladorTab0.Text = dt.Rows[0]["UF"].ToString();
                    if (dt.Rows[0]["montoViable_montoSNIP"].ToString().Length > 0)
                    {
                        lblMontoViableTab0.Text = Convert.ToDouble(dt.Rows[0]["montoViable_montoSNIP"].ToString()).ToString("N");
                    }
                    txtUtmXTab0.Text = dt.Rows[0]["UTMX"].ToString();
                    if (txtUtmXTab0.Text.Length > 0)
                    {
                        txtUtmXTab0.Text = Convert.ToDouble(txtUtmXTab0.Text).ToString();
                    }
                    txtUtmYTab0.Text = dt.Rows[0]["UTMY"].ToString();
                    if (txtUtmYTab0.Text.Length > 0)
                    {
                        txtUtmYTab0.Text = Convert.ToDouble(txtUtmYTab0.Text).ToString();
                    }
                    txtUtmZTab0.Text = dt.Rows[0]["UTMZ"].ToString();
                    if (txtUtmZTab0.Text.Length > 0)
                    {
                        txtUtmZTab0.Text = Convert.ToDouble(txtUtmZTab0.Text).ToString();
                    }

                    ddlTipoDenominacionFuenteTab0.SelectedValue = dt.Rows[0]["id_tipoDenominacionFuente"].ToString();
                    txtCaudalFuenteTab0.Text = dt.Rows[0]["caudalFuente"].ToString();
                    txtNroColiformeFuenteTab0.Text = dt.Rows[0]["nroColiformesFuente"].ToString();
                    txtResultadoAnalisisTab0.Text = dt.Rows[0]["resultadoAnalisisFuente"].ToString();
                    txtCentroPoblado.Text = dt.Rows[0]["centro_poblado"].ToString();

                    lblNomUsuarioTab0.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();


                    //******************** LISTA CCPPP ****************************//
                    string[] ArrayIdCCPP = (dt.Rows[0]["IdCCPP"].ToString()).Split(',');
                    //QUITAR SELECCION REGIONES
                    ASPxListBox lb = (ASPxListBox)xddeAmbitoTab0.FindControl("xlbAmbitoTab0");
                    lb.UnselectAll();

                    foreach (string ArrayUno in ArrayIdCCPP)
                    {
                        if (ArrayUno != "")
                        {
                            foreach (ListEditItem list in lb.Items)
                            {
                                if (list.Value.ToString() == ArrayUno.ToString())
                                {
                                    list.Selected = true;
                                }
                            }
                        }
                    }
                    //******************** LISTA TIPOLOGIA ****************************//
                    string[] ArrayIdTipologia = (dt.Rows[0]["IdTipologia"].ToString()).Split(',');
                    //QUITAR SELECCION REGIONES
                    xcblTipologia.UnselectAll();

                    foreach (string ArrayUno in ArrayIdTipologia)
                    {
                        if (ArrayUno != "")
                        {
                            foreach (ListEditItem list in xcblTipologia.Items)
                            {
                                if (list.Value.ToString() == ArrayUno.ToString())
                                {
                                    list.Selected = true;
                                }
                            }
                        }
                    }

                    //Muestra Padron Beneficiarios
                    trPadronBeneficiarioPNSR.Visible = true;
                }
                else
                {
                    if (lblCOD_SUBSECTOR.Text == "5") //PNVR
                    {
                        Panel_tblPNVR.Visible = true;

                        lblProgramaITab0.Text = dt.Rows[0]["NombPrograma"].ToString();
                        lblCodInternoITab0.Text = Convert.ToInt32(LblID_PROYECTO.Text).ToString("D10");
                        trAprobacionETTab0.Visible = false;

                        CargaUbicacionMetasTab0();
                        CargaTambosProyetos();

                        trLiquidacionPNVRTab3.Visible = true;
                        btnViviendasTerminadasTab3.Visible = true;
                        //Ocultar gastos acumulados

                        tdG1.Visible = false; tdG2.Visible = false; tdG3.Visible = false; tdG4.Visible = false; tdG5.Visible = false;
                        tdG6.Visible = false; tdG7.Visible = false; tdG8.Visible = false; tdG9.Visible = false; tdG10.Visible = false;
                        tdG11.Visible = false; tdG12.Visible = false; tdG13.Visible = false; tdG14.Visible = false; tdG15.Visible = false;
                        tdG16.Visible = false; tdG17.Visible = false; tdG18.Visible = false; tdG19.Visible = false;

                        imgFormulaAvanceFisico.ImageUrl = "../img/formulaAvanceFisicoNE_PNVR.png";
                    }
                    else
                    {

                        Panel_tblPNSU.Visible = true;
                        //******************** DATOS GENERALES ****************************//
                        lblProgTab0.Text = dt.Rows[0]["NombPrograma"].ToString();

                        lblCodIntTab0.Text = Convert.ToInt32(LblID_PROYECTO.Text).ToString("D10");

                        lblUbigeo6Tab0.Text = lblUBICACION.Text;
                        string CCPP = dt.Rows[0]["ccpp"].ToString();

                        if (CCPP.Length > 0)
                        {
                            lblUbigeoCCPPTab0.Text = "(" + CCPP.Substring(0, CCPP.Length - 2) + ")";
                        }
                        else
                        {
                            lblUbigeoCCPPTab0.Text = "";
                        }
                        lblEtapTab0.Text = dt.Rows[0]["TipoFinanciamiento"].ToString();
                        lblMontoTab0.Text = Convert.ToDouble(dt.Rows[0]["monto_snip"].ToString()).ToString("N");
                        lblFechaViabilidadTab0.Text = dt.Rows[0]["fecha_viabilidad"].ToString();

                        lblFechaEstadoActualTab0.Text = dt.Rows[0]["fechaUpdateEstado"].ToString();
                        lblEstadoActualTab0.Text = dt.Rows[0]["estadoEjecucion"].ToString();

                        lblBeneficiariosTab0.Text = dt.Rows[0]["num_hab_snip"].ToString();
                        lblUnidadEjecutoraTab0.Text = dt.Rows[0]["entidad_ejec"].ToString();
                        lblUnidadFormuladoraTab0.Text = dt.Rows[0]["UF"].ToString();
                    }
                }

            }
        }

        protected void CargaUbicacionMetasTab0()
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = _objBLBandeja.F_spMON_UbigeosMetas(Convert.ToInt32(LblID_PROYECTO.Text));
            grdUbicacionMetaTab0.DataSource = ListUbigeos;
            grdUbicacionMetaTab0.DataBind();

            int resultadoTotal = ListUbigeos.Sum(elemento => elemento.meta);

            txtTotalMetasTab0.Text = resultadoTotal.ToString();

        }
        protected void CargaTambosProyetos()
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();

            grdTamboProyectoTab0.DataSource = _objBLBandeja.F_spMON_TamboProyecto(Convert.ToInt32(LblID_PROYECTO.Text));
            grdTamboProyectoTab0.DataBind();


        }
        protected void CargaCCPPTab0(string depa, string prov, string dist)
        {
            ASPxListBox lb = (ASPxListBox)xddeAmbitoTab0.FindControl("xlbAmbitoTab0");

            lb.DataSource = _objBLBandeja.F_spMON_ListarUbigeo("4", depa, prov, dist);
            lb.TextField = "nombre";
            lb.ValueField = "codigo";
            lb.DataBind();
            xddeAmbitoTab0.DataBind();
        }

        protected void CargaUbigeo(string tipo, string depa, string prov, string dist, DropDownList ddl)
        {
            ddl.DataSource = _objBLBandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void btnFileTransferencia_OnClick(object sender, EventArgs e)
        {
            string file;
            //  file = FileUpload1.PostedFile.FileName;

        }

        protected void LnkbtnAvanceTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnAvanceTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void LnkbtnAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnAvanceAdicionalTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void LnkbtnSubContrato_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSubContrato.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        //protected void imgbtn_AgregarSeguimientoTab0_OnClick(object sender, EventArgs e)
        //{
        //    Panel_AgregarSOSEM.Visible = true;
        //    imgbtn_agregarSOSEM.Visible = false;
        //    Up_Tab0.Update();
        //}


        protected void grdAvanceFisicoTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.D_InformeAvancePNSR(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdAvanceFisicoAdicionalTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFisicoAdicionalTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.spud_MON_AvanceFisico_Eliminar(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected Boolean ValidarDatosGeneralesTab0()
        {
            Boolean result;
            result = true;

            int countCCPP = 0;
            //REGISTRAR CCPP
            ASPxListBox lbCCPP = (ASPxListBox)xddeAmbitoTab0.FindControl("xlbAmbitoTab0");
            foreach (ListEditItem list in lbCCPP.Items)
            {
                if (list.Selected == true)
                {
                    _BEBandeja.flagActivo = 1;
                    countCCPP = countCCPP + 1;
                }

            }
            if (countCCPP == 0)
            {
                string script = "<script>alert('Seleccionar minimo un Centro Poblado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtUtmXTab0.Text == "")
            {
                string script = "<script>alert('Ingresar coordenada UTM X.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtUtmYTab0.Text == "")
            {
                string script = "<script>alert('Ingresar coordenada UTM Y.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtUtmZTab0.Text == "")
            {
                string script = "<script>alert('Ingresar coordenada UTM Z.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlTipoDenominacionFuenteTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de denominación de la fuente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtCaudalFuenteTab0.Text == "")
            {
                string script = "<script>alert('Ingresar caudal.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtNroColiformeFuenteTab0.Text == "")
            {
                string script = "<script>alert('Ingresar Nro. de coliformes');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtResultadoAnalisisTab0.Text == "")
            {
                string script = "<script>alert('Ingresar resultados del análisis físico químico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }
        protected void btnGuardarTab0_Click(object sender, EventArgs e)
        {
            if (ValidarDatosGeneralesTab0())
            {
                string IdProyecto = Request.QueryString["id"].ToString();

                _BEBandeja.id_proyecto = Convert.ToInt32(IdProyecto);
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                //REGISTRAR CCPP
                ASPxListBox lbCCPP = (ASPxListBox)xddeAmbitoTab0.FindControl("xlbAmbitoTab0");
                foreach (ListEditItem list in lbCCPP.Items)
                {
                    if (list.Selected == true)
                    {
                        _BEBandeja.flagActivo = 1;
                    }
                    else
                    { _BEBandeja.flagActivo = 0; }

                    _BEBandeja.id_proyecto = Convert.ToInt32(IdProyecto);
                    _BEBandeja.CCPP = list.Value.ToString();
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _objBLBandeja.IU_spiu_MON_ProyectoUbigeoCCPP(_BEBandeja);
                }

                //REGISTRAR TIPOLOGIA
                foreach (ListEditItem list in xcblTipologia.Items)
                {
                    if (list.Selected == true)
                    {
                        _BEBandeja.flagActivo = 1;
                    }
                    else
                    {
                        _BEBandeja.flagActivo = 0;
                    }

                    _BEBandeja.id_proyecto = Convert.ToInt32(IdProyecto);
                    _BEBandeja.tipoTipologia = list.Value.ToString();
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _objBLBandeja.IU_spiu_MON_ProyectoTipologia(_BEBandeja);
                }


                //DATOS COMPLEMENTARIOS
                _BEBandeja.centro_poblado = Convert.ToString(txtCentroPoblado.Text);
                _BEBandeja.UTMX = Convert.ToDecimal(txtUtmXTab0.Text);
                _BEBandeja.UTMY = Convert.ToDecimal(txtUtmYTab0.Text);
                _BEBandeja.UTMZ = Convert.ToDecimal(txtUtmZTab0.Text);
                _BEBandeja.tipoDenominacion = Convert.ToInt32(ddlTipoDenominacionFuenteTab0.SelectedValue);
                _BEBandeja.caudalFuente = Convert.ToDecimal(txtCaudalFuenteTab0.Text);
                _BEBandeja.nroColiformesFuente = Convert.ToDecimal(txtNroColiformeFuenteTab0.Text);
                _BEBandeja.resultadoAnalisisFuente = txtResultadoAnalisisTab0.Text;

                int val = _objBLBandeja.U_spu_MON_DatosComplementariosPNSR(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente los datos generales y complementarios.')</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosGeneralesTab0();
                    UPTabContainerDetalles.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void imgbtnAgregarUbicacionMetasTab0_Click(object sender, ImageClickEventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = true;
            btnModificarUbicacionMetasTab0.Visible = false;

            imgbtnAgregarUbicacionMetasTab0.Visible = false;

            CargaUbigeo("1", "", "", "", ddlDepartamentoTab0);
            CargaUbigeo("2", ddlDepartamentoTab0.SelectedValue, "", "", ddlProvinciaTab0);
            CargaUbigeo("3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, "", ddlDistritoTab0);
            CargaUbigeo("4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue, ddlCCPPTab0);

        }

        protected void grdUbicacionMetaTab0_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdUbicacionMetaTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.id = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.tipo = "2";
                _BEBandeja.CCPP = "0000000000";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLBandeja.UD_spud_MON_UbigeoMetas(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaUbicacionMetasTab0();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void ddlDepartamentoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("2", ddlDepartamentoTab0.SelectedValue, "", "", ddlProvinciaTab0);
            CargaUbigeo("3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, "", ddlDistritoTab0);
            CargaUbigeo("4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue, ddlCCPPTab0);
        }
        protected void ddlProvinciaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, "", ddlDistritoTab0);
            CargaUbigeo("4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue, ddlCCPPTab0);
        }
        protected void ddlDistritoTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue, ddlCCPPTab0);
        }
        protected void grdUbicacionMetasTab0_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected Boolean ValidarUbigeoMetasTab0()
        {
            Boolean result;
            result = true;


            if (ddlDepartamentoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar departamento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlProvinciaTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar provincia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlDistritoTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar distrito.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlCCPPTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar CCPP.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }



            if (txtMetaTab0.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar meta');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            return result;
        }
        protected void btnCancelarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            Panel_UbicacionMetasTab0.Visible = false;
            imgbtnAgregarUbicacionMetasTab0.Visible = true;
        }

        protected void grdUbicacionMetaTab0_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = grdUbicacionMetaTab0.SelectedRow;
            lblIdUbigeoTab0.Text = grdUbicacionMetaTab0.SelectedDataKey.Value.ToString();

            string ubigeo = ((Label)row.FindControl("lblUbigeo")).Text;
            string ubigeoCCPP = ((Label)row.FindControl("lblUbigeoCCPP")).Text;

            CargaUbigeo("1", "", "", "", ddlDepartamentoTab0);
            ddlDepartamentoTab0.SelectedValue = ubigeo.Substring(0, 2);

            CargaUbigeo("2", ddlDepartamentoTab0.SelectedValue, "", "", ddlProvinciaTab0);
            ddlProvinciaTab0.SelectedValue = ubigeo.Substring(2, 2);

            CargaUbigeo("3", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, "", ddlDistritoTab0);
            ddlDistritoTab0.SelectedValue = ubigeo.Substring(4, 2);

            CargaUbigeo("4", ddlDepartamentoTab0.SelectedValue, ddlProvinciaTab0.SelectedValue, ddlDistritoTab0.SelectedValue, ddlCCPPTab0);

            if (ubigeoCCPP != "")
            {
                ddlCCPPTab0.SelectedValue = ubigeo + ubigeoCCPP;
            }

            //ddlCCPPTab0.SelectedValue = ubigeoCCPP;

            txtMetaTab0.Text = ((Label)row.FindControl("lblMeta")).Text;

            lblNomUsuarioUbigeoTab0.Text = "Actualizó: " + ((Label)row.FindControl("lblNomUsuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            Panel_UbicacionMetasTab0.Visible = true;
            btnGuardarUbicacionMetasTab0.Visible = false;
            btnModificarUbicacionMetasTab0.Visible = true;
            imgbtnAgregarUbicacionMetasTab0.Visible = false;
        }

        protected void btnModificarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            if (ValidarUbigeoMetasTab0())
            {
                _BEBandeja.id = Convert.ToInt32(lblIdUbigeoTab0.Text);
                _BEBandeja.CCPP = ddlCCPPTab0.SelectedValue;
                _BEBandeja.meta = Convert.ToInt32(txtMetaTab0.Text);
                _BEBandeja.tipo = "1";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLBandeja.UD_spud_MON_UbigeoMetas(_BEBandeja);


                if (val == 1)
                {
                    string script = "<script>alert('Se modificó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaUbicacionMetasTab0();

                    Panel_UbicacionMetasTab0.Visible = false;
                    imgbtnAgregarUbicacionMetasTab0.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }
        protected void btnGuardarUbicacionMetasTab0_Click(object sender, EventArgs e)
        {
            if (ValidarUbigeoMetasTab0())
            {
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.CCPP = ddlCCPPTab0.SelectedValue;
                _BEBandeja.meta = Convert.ToInt32(txtMetaTab0.Text);
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLBandeja.I_spi_MON_UbigeoMetas(_BEBandeja);


                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaUbicacionMetasTab0();

                    Panel_UbicacionMetasTab0.Visible = false;
                    imgbtnAgregarUbicacionMetasTab0.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected Boolean ValidarInformacionAdicionalTab0()
        {
            Boolean result;
            result = true;


            if (txtFechaAprobacionExpTab0.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de aprobación de Exp. Técnico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaAprobacionExpTab0.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de aprobación de Exp. Técnico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaConvenioNETab1.Text.Length > 0)
            {
                if (lblCOD_SUBSECTOR.Text != "5")//PNVR NO APLICA
                {
                    if (Convert.ToDateTime(txtFechaAprobacionExpTab0.Text) < Convert.ToDateTime(txtFechaConvenioNETab1.Text))
                    {
                        string script = "<script>alert('La Fecha de aprobación de Exp. Técnico debe ser mayor o igual a la fecha de convenio.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }

            if (txtFechaAperturaTab1.Text.Length>0)
            {
                if (Convert.ToDateTime(txtFechaAprobacionExpTab0.Text) > Convert.ToDateTime(txtFechaAperturaTab1.Text))
                {
                    string script = "<script>alert('La Fecha de aprobación de Exp. Técnico debe ser menor o igual a la fecha de apertura de cuenta bancaria.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtMontoAprobadoTab0.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar monto aprobado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoTransferirNETab0.Text.Trim() == "")
            {
                string script = "<script>alert('Ingresar monto a transferir al NE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtMontoFinanciarTab0.Text.Trim() == "")
            {
                if (lblCOD_SUBSECTOR.Text == "3")
                {
                    string script = "<script>alert('Ingresar monto de financiamiento del MVCS.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
                else
                {
                    txtMontoFinanciarTab0.Text = "0";
                }
            }



            return result;
        }

        protected void CargaAprobacioETTab0()
        {
            List<BE_MON_PROCESO> ListAprobacion = new List<BE_MON_PROCESO>();
            ListAprobacion = _objBLProceso.F_spMON_AprobacionExpediente(Convert.ToInt32(LblID_PROYECTO.Text));

            if (ListAprobacion.Count > 0)
            {

                txtFechaAprobacionExpTab0.Text = ListAprobacion.ElementAt(0).strFechaAprobacionExpediente;
                txtRDAprobacionETTab0.Text = ListAprobacion.ElementAt(0).nroResolucion;
                txtMontoFinanciarTab0.Text = ListAprobacion.ElementAt(0).montoFinanciadoMVCS;
                txtMontoAprobadoTab0.Text = ListAprobacion.ElementAt(0).montoAprobadoExpediente;
                txtMontoTransferirNETab0.Text = ListAprobacion.ElementAt(0).montoTransferencia;
                lnkbtnPresupuestoExpTab0.Text = ListAprobacion.ElementAt(0).urlDoc;

                GeneraIcoFile(lnkbtnPresupuestoExpTab0.Text, imgbtnPresupuestoExpTab0);

                lblNomUsuarioAdicionalTab0.Text = "Actualizó: " + ListAprobacion.ElementAt(0).usuario + " - " + ListAprobacion.ElementAt(0).strFecha_update;
            }
        }
        protected void btnGuardarAdicionalTab0_Click(object sender, EventArgs e)
        {
            if (ValidarInformacionAdicionalTab0())
            {
                if (validaArchivo(FileUploadPresupuestoTecnicoTab0))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEProceso.nroResolucion = txtRDAprobacionETTab0.Text;
                    _BEProceso.montoFinanciadoMVCS = txtMontoFinanciarTab0.Text;
                    _BEProceso.montoAprobadoExpediente = txtMontoAprobadoTab0.Text;
                    _BEProceso.montoTransferencia = txtMontoTransferirNETab0.Text;

                    _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaAprobacionExpTab0.Text);

                    if (FileUploadPresupuestoTecnicoTab0.HasFile)
                    {
                        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadPresupuestoTecnicoTab0);
                    }
                    else
                    { _BEProceso.urlDoc = lnkbtnPresupuestoExpTab0.Text; }

                    int val = _objBLProceso.U_AprobacionExpedienteNE(_BEProceso);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente la información de aprobación del Exp. Técnico.')</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAprobacioETTab0();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }

        }
        protected void lnkbtnPresupuestoExpTab0_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnPresupuestoExpTab0.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }


        protected void grdTamboProyectoTab0_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdTamboProyectoTab0.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.id = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLBandeja.D_spd_MON_TamboProyecto(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaTambosProyetos();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void imgbtnAgregarTamboTab0_Click(object sender, ImageClickEventArgs e)
        {
            Panel_AgregarTamboTab0.Visible = true;
            imgbtnAgregarTamboTab0.Visible = false;

            CargaTambosTab0();

        }
        protected void btnCancelarTamboTab0_Click(object sender, EventArgs e)
        {
            Panel_AgregarTamboTab0.Visible = false;
            imgbtnAgregarTamboTab0.Visible = true;
        }
        protected void btnGuardarTamboTab0_Click(object sender, EventArgs e)
        {
            if (ddlTamboTab0.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar Tambo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEBandeja.id = Convert.ToInt32(ddlTamboTab0.SelectedValue);
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLBandeja.I_spi_MON_TamboProyecto(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente el tambo.')</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_AgregarTamboTab0.Visible = false;
                    imgbtnAgregarTamboTab0.Visible = true;

                    CargaTambosProyetos();
                }
                else
                {
                    if (val == 2)
                    {
                        string script = "<script>alert('Error, el tambo ya se encuentra registradó.')</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        #endregion

        #region Tab1

        protected void CargaTab1()
        {

            //CargaOBRAEjecutorasTab0SEACEObra();
            //CargaOBRAEjecutorasTab0SEACEConsultoria();
            //CargaOBRAEjecutorasTab0SEACEBienesServicios();

            //if (Convert.ToInt32(lblSNIP.Text) > 0 && (grdSEACEObra.Rows.Count > 0) || (grdSEACEConsultoria.Rows.Count > 0) || (grdSEACEBienesServicios.Rows.Count > 0))
            //{
            //    TabPanelProcesoSeleccionSEACE.Enabled = true;
            //    TabPanelProcesoSeleccion.Enabled = false;

            //}
            //else
            //{
            //TabPanelProcesoSeleccionSEACE.Enabled = false;
            //TabPanelProcesoSeleccion.Enabled = true;

            CargaddlTipoAdjudicacionTab1(ddlAdjudicacionTab1);
            CargaddlTipoAdjudicacionTab1(ddlTipoAdjudicacionTab1);
            cargaDDlTipoContratacionTab1();
            CargaDllModalidadTab1();
            // CargaDllModalidadTab1(ddlModalidadResponsableTab1);
            CargaDllModalidadTab1(ddlModalidaSubcontratoTab1);
            //CargaTipoAlcanceConvenio();
            CargaTipoGradoInstruccionTab1();
            CargaTipoDocumentoIdentificacionTab1(ddlTipoDocumentoPATab1);
            CargaTipoDocumentoIdentificacionTab1(ddlTipoDocumentoTab1);
            CargaTipoOcupacion();

            CargaListaRubro();
            CargaDllTipoResultado();

            CargaTipoEstadoAsignacionTab1();
            CargaTipoCargoTab1();

            CargaDatosTab1();

            CargaAprobacioETTab0();

            if (!this.IsPostBack)
            {
                grdRendicionCuentaNETab1.PageIndex = Int32.MaxValue;
            }
            CargaRendicionCuentaNETab1();
           
        }

        protected void cargaConsultoresTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdConsultoresTab1.DataSource = _objBLProceso.F_spMON_GrupoConsorcio(_BEProceso);
            grdConsultoresTab1.DataBind();


        }

        protected void CargaDatosTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);
            string modal;
            if (dt.Rows.Count > 0)
            {
                modal = dt.Rows[0]["id_tipoModalidad"].ToString();
                if (modal != "")
                {
                    ddlModalidadTab1.SelectedValue = modal;
                }
                else
                {
                    ddlModalidadTab1.SelectedValue = "";
                }


                if (ddlModalidadTab1.SelectedValue == "1") // DIRECTA
                {
                    lblNomActualizaProcesoDirecta.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    Panel_ProcesoDirectoTab1.Visible = true;
                    btnGuardarDirectoTab1.Visible = true;
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    //cargaSeguimientoProcesoTab1();
                    btnCambiarModalidadTab1.Visible = true;
                    // CargaDatosTab1();
                    cargaSubcontratosTab1();
                    //CargaResponsableTab1();
                    Panel_OtrasModalidadesTab1.Visible = false;
                    Panel_IndirectaNETab1.Visible = false;
                }

                if (ddlModalidadTab1.SelectedValue == "2")  // INDIRECTA
                {

                    lblNomActualizaProcesoDirecta.Text = "";
                    ddlAdjudicacionTab1.SelectedValue = dt.Rows[0]["id_tipoAdjudicacion"].ToString();
                    txtNroLicTab1.Text = dt.Rows[0]["NroLic"].ToString();
                    TxtAnio2Tab1.Text = dt.Rows[0]["anio"].ToString();
                    txtDetalleTab1.Text = dt.Rows[0]["detalle"].ToString();
                    txtProTab1.Text = dt.Rows[0]["buenaPro"].ToString();
                    txtProConsentidaTab1.Text = dt.Rows[0]["buenaProConsentida"].ToString();
                    txtValorReferencialTab1.Text = dt.Rows[0]["valorReferencial"].ToString();
                    ddlTipoContratacionTab1.SelectedValue = dt.Rows[0]["id_tipoContratacion"].ToString();
                    txtMontoContratadoTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                    txtNroContrato.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                    lblNomActualizaProcesoSeleccion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    // lblNomUsuarioOtraModalidad.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    string valCheck = dt.Rows[0]["id_tipoEmpresa"].ToString();

                    rbConsultorTab1.Checked = false;
                    rbConsorcioTab1.Checked = false;
                    rbConsorcioTab1.Enabled = true;
                    rbConsultorTab1.Enabled = true;

                    if (valCheck != "")
                    {
                        if (Convert.ToInt32(valCheck) == 1)
                        {
                            rbConsultorTab1.Checked = true;
                            rbConsorcioTab1.Checked = false;
                            //  rbConsorcioTab1.Enabled = false;

                            lblNomConsultorTab1.Font.Bold = true;
                            lblNomConsorcioTab1.Font.Bold = false;

                            Panel_ConsultorTab1.Visible = true;
                            Panel_ConsorcioTab1.Visible = false;


                            Up_rbProcesoTab1.Update();

                            txtNombConsultorTab1.Text = dt.Rows[0]["nombreContratista"].ToString();
                            txtRucConsultorTab1.Text = dt.Rows[0]["rucContratista"].ToString();
                            lnkbtnContratoContratistaTab1.Text = dt.Rows[0]["urlContrato"].ToString();

                            GeneraIcoFile(lnkbtnContratoContratistaTab1.Text, imgbtnContratoContratistaTab1);

                        }
                        if (Convert.ToInt32(valCheck) == 2)
                        {
                            rbConsorcioTab1.Checked = true;
                            rbConsultorTab1.Checked = false;
                            //  rbConsultorTab1.Enabled = false;

                            lblNomConsorcioTab1.Font.Bold = true;
                            lblNomConsultorTab1.Font.Bold = false;

                            Panel_ConsorcioTab1.Visible = true;
                            Panel_ConsultorTab1.Visible = false;

                            cargaConsultoresTab1();

                            Up_rbProcesoTab1.Update();


                            lnkbtnContratoTab1.Text = dt.Rows[0]["urlContrato"].ToString();

                            txtNomconsorcioTab1.Text = dt.Rows[0]["nombreConsorcio"].ToString();
                            txtRucConsorcioTab1.Text = dt.Rows[0]["rucConsorcio"].ToString();
                            txtTelefonoConsorcioTab1.Text = dt.Rows[0]["telefonoConsorcio"].ToString();
                            txtRepresentanteTab1.Text = dt.Rows[0]["representateConsorcio"].ToString();

                            GeneraIcoFile(lnkbtnContratoTab1.Text, imgbtnContratoTab1);

                        }
                    }
                    else
                    {
                        rbConsorcioTab1.Checked = false;
                        rbConsultorTab1.Checked = false;
                        rbConsultorTab1.Enabled = true;
                        rbConsorcioTab1.Enabled = true;
                        Panel_ConsultorTab1.Visible = false;
                        Panel_ConsorcioTab1.Visible = false;

                        txtNombConsultorTab1.Text = "";
                        txtRucConsultorTab1.Text = "";

                        txtNomconsorcioTab1.Text = "";
                        txtRucConsorcioTab1.Text = "";
                        txtTelefonoConsorcioTab1.Text = "";
                        txtRepresentanteTab1.Text = "";

                        lblNomConsultorTab1.Font.Bold = false;
                        lblNomConsorcioTab1.Font.Bold = false;

                        cargaConsultoresTab1();
                        Up_rbProcesoTab1.Update();

                        imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                        lnkbtnContratoContratistaTab1.Text = "";
                        imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                        lnkbtnContratoTab1.Text = "";
                    }



                    Panel_ProcesoIndirectoTab1.Visible = true;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    cargaSeguimientoProcesoTab1();
                    // CargaDatosTab1();
                    btnCambiarModalidadTab1.Visible = true;

                    Panel_OtrasModalidadesTab1.Visible = false;
                    Panel_IndirectaNETab1.Visible = false;
                }

                if (ddlModalidadTab1.SelectedValue == "3") // OTRAS MODALIDADES
                {
                    lblNomActualizaProcesoDirecta.Text = "";
                    lblNomUsuarioOtraModalidad.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                    txtMontoContratadoOtrasModalidadTab1.Text = dt.Rows[0]["MontoConsorcio"].ToString();
                    txtNroContratoOtrasModalidadTab1.Text = dt.Rows[0]["NroContratoConsorcio"].ToString();
                    txtEntidadEjecutoraTab1.Text = dt.Rows[0]["entidadEjecutora"].ToString();
                    txtCoordinadorEntidadEjecutoraTab1.Text = dt.Rows[0]["coordinadorEJ"].ToString();
                    txtTelefonoCoordEJTab1.Text = dt.Rows[0]["telefonoCoordEJ"].ToString();
                    txtCorreoCoordEJTab1.Text = dt.Rows[0]["correoCoordEJ"].ToString();
                    txtfechaConvenioTab1.Text = dt.Rows[0]["fechaConvenio"].ToString();
                    txtConvenioContratoTab1.Text = dt.Rows[0]["convenioContrato"].ToString();
                    LnkbtnOtraModalidad.Text = dt.Rows[0]["urlContrato"].ToString();

                    GeneraIcoFile(LnkbtnOtraModalidad.Text, ImgbtnOtraModalidad);

                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    // cargaSeguimientoProcesoTab1();
                    // CargaDatosTab1();
                    btnCambiarModalidadTab1.Visible = true;

                    Panel_OtrasModalidadesTab1.Visible = true;
                    Panel_IndirectaNETab1.Visible = false;
                }

                if (ddlModalidadTab1.SelectedValue == "4") //INDIRECTA NUCLEOS EJECUTORES
                {
                    lblNomActualizaProcesoDirecta.Text = "";
                    lblNomActualizaProcesoIndirectaNE.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                    txtFechaPresentacionTab1.Text = dt.Rows[0]["fechaPresentacionSolicitud"].ToString();
                    txtFechaAprobacionTab1.Text = dt.Rows[0]["fechaAprobacionSolicitud"].ToString();
                    lnkbtnSolicitudTab1.Text = dt.Rows[0]["urlSolicitud"].ToString();
                    GeneraIcoFile(lnkbtnSolicitudTab1.Text, imgbtnSolicitudTab1);

                    if (dt.Rows[0]["flagResultado"].ToString() == "1")
                    {
                        rbAceptadoTab1.Checked = true;
                        rbRechazadoTab1.Checked = false;

                        trAprobacionTab1.Visible = true;
                    }
                    else
                    {
                        if (dt.Rows[0]["flagResultado"].ToString() == "0")
                        {
                            rbAceptadoTab1.Checked = false;
                            rbRechazadoTab1.Checked = true;

                            txtMotivoTab1.Text = dt.Rows[0]["motivo"].ToString();
                            lnkbtnResultadoTab1.Text = dt.Rows[0]["urlMotivo"].ToString();
                            GeneraIcoFile(lnkbtnResultadoTab1.Text, imgbtnResultadoTab1);

                            trMotivoTab1.Visible = true;
                            trArchivoMotivoTab1.Visible = true;
                        }
                        else
                        {
                            rbAceptadoTab1.Checked = false;
                            rbRechazadoTab1.Checked = false;
                        }

                    }
                    lblNomActualizaSolicitudTab1.Text = "Actualizó: " + dt.Rows[0]["usuarioSolicitud"].ToString() + " - " + dt.Rows[0]["fechaUpdateSolicitud"].ToString();

                    txtFechaConvenioNETab1.Text = dt.Rows[0]["fechaConvenio"].ToString();
                    txtNroConvenioNETab1.Text = dt.Rows[0]["convenioContrato"].ToString();
                    //ddlTipoAlcanceNETab1.Text = dt.Rows[0]["id_tipoAlcanceConvenio"].ToString();

                    if (txtNroConvenioNETab1.Text.Length > 10 && (lblCOD_SUBSECTOR.Text == "5" || lblCOD_SUBSECTOR.Text == "1"))
                    {
                        lblNomSNIP.Text = "CONVENIO:";
                        lblSNIP.Text = txtNroConvenioNETab1.Text;
                    }

                    lblNomActualizaConvenioNETab1.Text = "Actualizó: " + dt.Rows[0]["usuarioConvenio"].ToString() + " - " + dt.Rows[0]["fechaUpdateConvenioNE"].ToString();

                    txtFechaActaConstitucionNETab1.Text = dt.Rows[0]["fechaConstitucionNE"].ToString();
                    txtAsistentesHombres.Text = dt.Rows[0]["asistentesHombres"].ToString();
                    txtAsistentesMujeres.Text = dt.Rows[0]["asistentesMujeres"].ToString();

                    lnkbtnActaAsambleaNETab1.Text = dt.Rows[0]["urlActaAsamblea"].ToString();
                    GeneraIcoFile(lnkbtnActaAsambleaNETab1.Text, imgbtnActaAsambleaNETab1);

                    lnkbtnConvenioNETab1.Text = dt.Rows[0]["urlConvenioNE"].ToString();
                    GeneraIcoFile(lnkbtnConvenioNETab1.Text, imgbtnConvenioNETab1);

                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;

                    // btnCambiarModalidadTab1.Visible = true;

                    Panel_OtrasModalidadesTab1.Visible = false;
                    Panel_IndirectaNETab1.Visible = true;

                    CargaUbigeo("1", "", "", "", ddlDepaPATab1);
                    //ddlDepaPATab1.SelectedValue = lblIdUbigeo.Text.Substring(0, 2);
                    //CargaUbigeo("2", ddlDepaPATab1.SelectedValue, "", "", ddlProvPATab1);
                    //ddlProvPATab1.SelectedValue = lblIdUbigeo.Text.Substring(2, 2);
                    //CargaUbigeo("3", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, "", ddlDistPATab1);
                    //ddlDistPATab1.SelectedValue = lblIdUbigeo.Text.Substring(4, 2);
                    //CargaUbigeo("4", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, ddlDistPATab1.SelectedValue, ddlCCPPPATab1);

                    CargaProcesoMiembroNE();

                    CargaUbigeo("1", "", "", "", ddlDepaCuentaTab1);

                    DataTable dtDatoPre = new DataTable();
                    dtDatoPre = _objBLProceso.spMON_DatosPreoperativosNE(_BEProceso);

                    if (dtDatoPre.Rows.Count > 0)
                    {
                        ddlTipoBancoTab1.SelectedValue = dtDatoPre.Rows[0]["id_tipoBanco"].ToString();
                        txtNroCuentaTab1.Text = dtDatoPre.Rows[0]["nroCuenta"].ToString();
                        txtFechaAperturaTab1.Text = dtDatoPre.Rows[0]["fechaApertura"].ToString();
                        txtAgenciaTab1.Text = dtDatoPre.Rows[0]["nombreAgencia"].ToString();
                        lblNomActualizaCuentaBancariaTab1.Text = "Actualizó: " + dtDatoPre.Rows[0]["usuarioCuenta"].ToString() + " - " + dtDatoPre.Rows[0]["fechaUpdateCuenta"].ToString();

                        string ubigeoAgencia = dtDatoPre.Rows[0]["ubigeoAgencia"].ToString();
                        if (ubigeoAgencia.Length == 6)
                        {
                            ddlDepaCuentaTab1.SelectedValue = ubigeoAgencia.Substring(0, 2);
                            CargaUbigeo("2", ddlDepaCuentaTab1.SelectedValue, "", "", ddlProvCuentaTab1);
                            ddlProvCuentaTab1.SelectedValue = ubigeoAgencia.Substring(2, 2);
                            CargaUbigeo("3", ddlDepaCuentaTab1.SelectedValue, ddlProvCuentaTab1.SelectedValue, "", ddlDistCuentatab1);
                            ddlDistCuentatab1.SelectedValue = ubigeoAgencia.Substring(4, 2);
                        }

                        txtFechaOrientacionTab1.Text = dtDatoPre.Rows[0]["fechaSesion"].ToString();
                        txtAsistenteOrientacionHombreTab1.Text = dtDatoPre.Rows[0]["nroAsistentesHombres"].ToString();
                        txtAsistenteOrientacionMujerTab1.Text = dtDatoPre.Rows[0]["nroAsistentesMujeres"].ToString();
                        lnkbtnActaOrientacionTab1.Text = dtDatoPre.Rows[0]["actaSesion"].ToString();
                        GeneraIcoFile(lnkbtnActaOrientacionTab1.Text, imgbtnActaOrientacionTab1);
                        lblNomActualizaSesionTab1.Text = "Actualizó: " + dtDatoPre.Rows[0]["usuarioSesion"].ToString() + " - " + dtDatoPre.Rows[0]["fechaUpdateSesion"].ToString();


                    }

                }

            }
            else
            {
                ddlAdjudicacionTab1.SelectedValue = "";
                txtNroLicTab1.Text = "";
                TxtAnio2Tab1.Text = "";
                txtDetalleTab1.Text = "";
                txtProTab1.Text = "";
                txtProConsentidaTab1.Text = "";
                txtValorReferencialTab1.Text = "";
                ddlTipoContratacionTab1.SelectedValue = "";
                txtMontoContratadoTab1.Text = "";
                txtNroContrato.Text = "";
                txtNomconsorcioTab1.Text = "";
                txtRucConsorcioTab1.Text = "";
                txtTelefonoConsorcioTab1.Text = "";
                txtRepresentanteTab1.Text = "";

                rbConsorcioTab1.Checked = false;
                rbConsultorTab1.Checked = false;
                rbConsultorTab1.Enabled = true;
                rbConsorcioTab1.Enabled = true;
                Panel_ConsultorTab1.Visible = false;
                Panel_ConsorcioTab1.Visible = false;
                lblNomActualizaProcesoSeleccion.Text = "";
                lblNomActualizaProcesoDirecta.Text = "";

                imgbtnContratoTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoContratistaTab1.Text = "";
                imgbtnContratoContratistaTab1.ImageUrl = "~/img/blanco.png";
                lnkbtnContratoTab1.Text = "";

                //CAMPOS DE OTRA MODALIDAD
                txtMontoContratadoOtrasModalidadTab1.Text = "";
                txtNroContratoOtrasModalidadTab1.Text = "";

                txtEntidadEjecutoraTab1.Text = "";
                txtCoordinadorEntidadEjecutoraTab1.Text = "";
                txtTelefonoCoordEJTab1.Text = "";
                txtCorreoCoordEJTab1.Text = "";
                txtfechaConvenioTab1.Text = "";
                txtConvenioContratoTab1.Text = "";
                lblNomUsuarioOtraModalidad.Text = "";
                LnkbtnOtraModalidad.Text = "";
                ImgbtnOtraModalidad.ImageUrl = "~/img/blanco.png";

                //INDIRECTA NE
                txtNroConvenioNETab1.Text = "";
                txtFechaConvenioNETab1.Text = "";
                //ddlTipoAlcanceNETab1.Text = "";
                txtFechaActaConstitucionNETab1.Text = "";
                txtAsistentesHombres.Text = "0";
                txtAsistentesMujeres.Text = "0";
                lnkbtnActaAsambleaNETab1.Text = "";
                imgbtnActaAsambleaNETab1.ImageUrl = "~/img/blanco.png";
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnCambiarModalidadTab1.Visible = false;

            }
            cargaConsultoresTab1();
            Up_Tab1.Update();

        }

        protected void CargaDllModalidadTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoModalidad_Contrato();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaDllTipoResultado()
        {
            ddlResultadoTab1.DataSource = _objBLEjecucion.F_spMON_TipoResultado();
            ddlResultadoTab1.DataTextField = "nombre";
            ddlResultadoTab1.DataValueField = "valor";
            ddlResultadoTab1.DataBind();

            ddlResultadoTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaTambosTab0()
        {
            if (ddlTamboTab0.Items.Count == 0)
            {
                ddlTamboTab0.DataSource = _objBLBandeja.F_spMON_ListarTambos();
                ddlTamboTab0.DataTextField = "nombre";
                ddlTamboTab0.DataValueField = "valor";
                ddlTamboTab0.DataBind();

                ddlTamboTab0.Items.Insert(0, new ListItem("-SELECCIONE-", ""));
            }
            else
            {
                ddlTamboTab0.SelectedValue = "";
            }

        }

        protected void CargaDllModalidadTab1()
        {
            ddlModalidadTab1.DataSource = _objBLProceso.F_spMON_TipoModalidad();
            ddlModalidadTab1.DataTextField = "nombre";
            ddlModalidadTab1.DataValueField = "valor";
            ddlModalidadTab1.DataBind();

            ddlModalidadTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }
        //protected void CargaTipoAlcanceConvenio()
        //{
        //    ddlTipoAlcanceNETab1.DataSource = _objBLProceso.F_spMON_TipoAlcanceconvenio();
        //    ddlTipoAlcanceNETab1.DataTextField = "nombre";
        //    ddlTipoAlcanceNETab1.DataValueField = "valor";
        //    ddlTipoAlcanceNETab1.DataBind();

        //    ddlTipoAlcanceNETab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        //}

        protected void CargaTipoGradoInstruccionTab1()
        {
            ddlTipoGradoInstruccionPATab1.DataSource = _objBLProceso.F_spMON_TipoGradoInstruccion();
            ddlTipoGradoInstruccionPATab1.DataTextField = "nombre";
            ddlTipoGradoInstruccionPATab1.DataValueField = "valor";
            ddlTipoGradoInstruccionPATab1.DataBind();

            ddlTipoGradoInstruccionPATab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaTipoDocumentoIdentificacionTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoDocumentoIdentificacion();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            // ddl.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaTipoOcupacion()
        {
            ddlTipoOcupacionPATab1.DataSource = _objBLProceso.F_spMON_TipoOcupacion();
            ddlTipoOcupacionPATab1.DataTextField = "nombre";
            ddlTipoOcupacionPATab1.DataValueField = "valor";
            ddlTipoOcupacionPATab1.DataBind();

            ddlTipoOcupacionPATab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void LnkbtnSeguimientoTAb1_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnSeguimientoTAb1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void CargaListaRubro()
        {
            ddlRubroTab1.DataSource = _objBLProceso.F_spMON_ListaRubro();
            ddlRubroTab1.DataTextField = "nombre";
            ddlRubroTab1.DataValueField = "valor";
            ddlRubroTab1.DataBind();

            ddlRubroTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void CargaRendicionCuentaNETab1()
        {

            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;


            grdRendicionCuentaNETab1.DataSource = _objBLProceso.F_spMON_RendicionCuentaNE(_BEProceso);
            grdRendicionCuentaNETab1.DataBind();

        }
        protected void cargaDDlTipoContratacionTab1()
        {
            ddlTipoContratacionTab1.DataSource = _objBLProceso.F_spMON_TipoContratacion();
            ddlTipoContratacionTab1.DataTextField = "nombre";
            ddlTipoContratacionTab1.DataValueField = "valor";
            ddlTipoContratacionTab1.DataBind();

            ddlTipoContratacionTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));

        }

        protected void ddlProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcesoTab1.SelectedValue != "")
            {
                ddlModalidadTab1.SelectedValue = "";
                CargaDatosTab1();

                if (ddlModalidadTab1.SelectedValue != "")
                {

                }
                else
                {
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    btnCambiarModalidadTab1.Visible = false;
                    Panel_OtrasModalidadesTab1.Visible = false;
                }


            }


            if (Convert.ToInt32(lblCOD_SUBSECTOR.Text) == 1)
            {
                Panel_ProcesoDirectoTab1.Visible = false;
            }


            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;

            }

            Up_Tab1.Update();


        }

        protected void btnCambiarModalidadTab1_OnClick(object sender, EventArgs e)
        {
            string valorModalidad = ddlModalidadTab1.SelectedValue;
            int val;
            if (valorModalidad == "1")
            {
                ddlModalidadTab1.SelectedValue = "2";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);

                Panel_ProcesoIndirectoTab1.Visible = true;
                Panel_ProcesoDirectoTab1.Visible = false;
                btnGuardarDirectoTab1.Visible = false;
                cargaSeguimientoProcesoTab1();
                CargaDatosTab1();
                btnCambiarModalidadTab1.Visible = true;


            }

            if (valorModalidad == "2")
            {
                ddlModalidadTab1.SelectedValue = "3";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);


                btnGuardarDirectoTab1.Visible = true;

                //cargaSeguimientoProcesoTab1();
                btnCambiarModalidadTab1.Visible = true;

                Panel_ProcesoIndirectoTab1.Visible = false;
                Panel_ProcesoDirectoTab1.Visible = false;
                btnGuardarDirectoTab1.Visible = false;
                Panel_OtrasModalidadesTab1.Visible = true;

                CargaDatosTab1();
                //cargaSubcontratosTab1();
                //CargaResponsableTab1();


            }

            if (valorModalidad == "3")
            {
                ddlModalidadTab1.SelectedValue = "1";

                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                val = _objBLProceso.spu_MON_ModalidadEjecucion(_BEProceso);

                Panel_ProcesoDirectoTab1.Visible = true;
                btnGuardarDirectoTab1.Visible = true;
                Panel_ProcesoIndirectoTab1.Visible = false;
                //cargaSeguimientoProcesoTab1();
                btnCambiarModalidadTab1.Visible = true;
                CargaDatosTab1();
                cargaSubcontratosTab1();
                //CargaResponsableTab1();
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;
                Up_Tab1.Update();
            }

        }

        protected void btnGuardarDirectoTab1_OnClick(object sender, EventArgs e)
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

            _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
            _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);


            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
            _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);
            int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);//2


            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                CargaDatosTab1();

                Up_Tab1.Update();

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void btnGuardarOtraModalidadTab1_OnClick(object sender, EventArgs e)
        {

            if (_Metodo.ValidaFecha(txtfechaConvenioTab1.Text) == false)
            {
                string script = "<script>alert('Ingresar una fecha de convenio valida.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoFinanciamiento = 3;
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);
                //_BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue);
                //_BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

                //_BEProceso.norLic = Convert.ToInt32(txtNroLicTab1.Text);
                //_BEProceso.anio = TxtAnio2Tab1.Text;
                //_BEProceso.detalle = txtDetalleTab1.Text;
                _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                //_BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                _BEProceso.montoConsorcio = txtMontoContratadoOtrasModalidadTab1.Text;
                _BEProceso.NroContratoConsorcio = (txtNroContratoOtrasModalidadTab1.Text);

                _BEProceso.EntidadEjecutora = txtEntidadEjecutoraTab1.Text;
                _BEProceso.CoordinadorEJ = txtCoordinadorEntidadEjecutoraTab1.Text;
                _BEProceso.TelefonoCoordEJ = txtTelefonoCoordEJTab1.Text;
                _BEProceso.CorreoCoordEJ = txtCorreoCoordEJTab1.Text;
                _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);
                _BEProceso.ConvenioContrato = txtConvenioContratoTab1.Text;

                if (FileUploadOtraModalidad.HasFile)
                {
                    _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadOtraModalidad);
                }
                else
                { _BEProceso.urlContrato = LnkbtnOtraModalidad.Text; }

                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso);//2


                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaDatosTab1();

                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }



            }
        }


        protected void ImgbtnOtraModalidad_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnOtraModalidad.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnOtraModalidad.Text);
                Response.End();
            }
        }


        protected void grdSeguimientoProcesoTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_seguimiento;
            id_seguimiento = grdSeguimientoProcesoTab1.SelectedDataKey.Value.ToString();
            lblRegistroSeguimientoP.Text = id_seguimiento.ToString();
            GridViewRow row = grdSeguimientoProcesoTab1.SelectedRow;
            TxtConvocatoriaTab1.Text = ((Label)row.FindControl("lblConvocatoria")).Text;
            txtFechaPubliTab1.Text = ((Label)row.FindControl("lblFecha")).Text;
            ddlTipoAdjudicacionTab1.Text = ((Label)row.FindControl("lblTipoAdjudicacion")).Text;
            ddlResultadoTab1.SelectedValue = ((Label)row.FindControl("lblIDResultado")).Text;
            txtObservacion.Text = ((Label)row.FindControl("lblobservacion")).Text;
            LnkbtnSeguimientoTAb1.Text = ((ImageButton)row.FindControl("imgDocTab1")).ToolTip;
            lblNomUsuarioSeguimientoProceso.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnSeguimientoTAb1.Text, imgbtnSeguimientoTAb1);



            cargaSeguimientoProcesoTab1();
            Up_Tab1.Update();
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            btnModificarSeguimientoProcesoTab1.Visible = true;
            btnGuardarSeguimientoProcesoTab1.Visible = false;

        }

        protected void grdSubcontratosTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_subContratos;
            id_subContratos = grdSubcontratosTab1.SelectedDataKey.Value.ToString();
            lblId_SubContratos.Text = id_subContratos.ToString();
            GridViewRow row = grdSubcontratosTab1.SelectedRow;

            ddlRubroTab1.Text = ((Label)row.FindControl("lblId_Rubros")).Text;
            txtnombreTab1.Text = ((Label)row.FindControl("lblnombre")).Text;
            txtMontoTab1.Text = Convert.ToDouble(((Label)row.FindControl("lblmonto")).Text).ToString("N");
            txtPlazoTab1.Text = ((Label)row.FindControl("lblPlazo")).Text;
            ddlModalidaSubcontratoTab1.Text = ((Label)row.FindControl("lblId_Modalidad")).Text;
            LnkbtnSubContrato.Text = ((ImageButton)row.FindControl("imgDocSubContratoTab1")).ToolTip;

            GeneraIcoFile(LnkbtnSubContrato.Text, imgbtnSubContrato);


            Panel_SubcontratosTab1.Visible = true;
            btnGuardarSubcontratosTab1.Visible = false;
            btnModificarSubcontratosTab1.Visible = true;
            imgbtnAgregarSubcontratoTab1.Visible = false;

        }
        protected void grdConsultoresTab1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_grupo_consorcio;
            id_grupo_consorcio = grdConsultoresTab1.SelectedDataKey.Value.ToString();
            lblId_Registro_Contratista.Text = id_grupo_consorcio.ToString();
            GridViewRow row = grdConsultoresTab1.SelectedRow;
            txtContratistaGrupTab1.Text = ((Label)row.FindControl("lblNombConsultor")).Text;
            txtRucGrupTab1.Text = ((Label)row.FindControl("lblRuc")).Text;
            txtRepresentanGrupTAb1.Text = ((Label)row.FindControl("lblRepresentante")).Text;
            Panel_AgregarConsultorTab1.Visible = true;
            btn_guardarConsultor.Visible = false;
            btn_modificarConsultor.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
        }

        protected void lnkbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoContratistaTab1_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoContratistaTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoContratistaTab1.Text);
                Response.End();
            }
        }

        protected void imgbtnContratoTab1_OnClik(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + lnkbtnContratoTab1.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + lnkbtnContratoTab1.Text);
                Response.End();
            }
        }

        protected void ddlModalidadTab1_OnSelectIndexChanged(object sender, EventArgs e)
        {

            if (ddlProcesoTab1.SelectedValue != "")
            {
                CargaDatosTab1();

                if (ddlModalidadTab1.SelectedValue != "")
                {
                    if (ddlModalidadTab1.SelectedValue == "1")
                    {
                        Panel_ProcesoDirectoTab1.Visible = true;
                        btnGuardarDirectoTab1.Visible = true;
                        Panel_ProcesoIndirectoTab1.Visible = false;
                        Panel_IndirectaNETab1.Visible = false;
                        //cargaSeguimientoProcesoTab1();

                        cargaSubcontratosTab1();
                        //CargaResponsableTab1();

                    }

                    if (ddlModalidadTab1.SelectedValue == "2")
                    {
                        Panel_ProcesoIndirectoTab1.Visible = true;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        cargaSeguimientoProcesoTab1();
                        Panel_IndirectaNETab1.Visible = false;
                        // CargaDatosTab1();

                    }

                    if (ddlModalidadTab1.SelectedValue == "3")
                    {
                        Panel_ProcesoIndirectoTab1.Visible = false;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        Panel_OtrasModalidadesTab1.Visible = true;
                        Panel_IndirectaNETab1.Visible = false;
                        //cargaSeguimientoProcesoTab1();
                        // CargaDatosTab1();

                    }
                    if (ddlModalidadTab1.SelectedValue == "4")
                    {

                        Panel_ProcesoIndirectoTab1.Visible = false;
                        Panel_ProcesoDirectoTab1.Visible = false;
                        btnGuardarDirectoTab1.Visible = false;
                        Panel_OtrasModalidadesTab1.Visible = false;
                        Panel_IndirectaNETab1.Visible = true;
                        //cargaSeguimientoProcesoTab1();
                        // CargaDatosTab1();

                    }

                    Up_Tab1.Update();
                }
                else
                {
                    Panel_ProcesoIndirectoTab1.Visible = false;
                    Panel_ProcesoDirectoTab1.Visible = false;
                    btnGuardarDirectoTab1.Visible = false;
                    Panel_OtrasModalidadesTab1.Visible = false;


                }
            }

            if (Convert.ToInt32(lblCOD_SUBSECTOR.Text) == 1)
            {
                Panel_ProcesoDirectoTab1.Visible = false;
            }

            if (lblID_ACCESO.Text == "0")
            {
                btnGuardarDirectoTab1.Visible = false;
                btnCambiarModalidadTab1.Visible = false;

            }


            Up_Tab1.Update();
        }

        protected void cargaSeguimientoProcesoTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;


            grdSeguimientoProcesoTab1.DataSource = _objBLProceso.F_spMON_SeguimientoProcesoSeleccion(_BEProceso);
            grdSeguimientoProcesoTab1.DataBind();
        }

        protected void grdSeguimientoProcesoTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab1");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void CargaddlTipoAdjudicacionTab1(DropDownList ddl)
        {
            ddl.DataSource = _objBLProceso.F_spMON_TipoAdjudicacion();
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected Boolean ValidarSeguimientoTab1()
        {
            Boolean result;
            result = true;

            if (TxtConvocatoriaTab1.Text == "")
            {
                string script = "<script>alert('Ingrese convocatoria.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaPubliTab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de publicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlTipoAdjudicacionTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de adjudicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlResultadoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione resultado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }

        protected void btnGuardarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSeguimientoTab1())
            {
                if (validaArchivo(FileUploadSeguimientoTAb1))
                {
                    if (validaArchivo(FileUploadContratoContratistaTab1))
                    {

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                        _BEProceso.Date_fechaPublicacion = Convert.ToDateTime(txtFechaPubliTab1.Text);
                        _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                        _BEProceso.resultado = ddlResultadoTab1.SelectedValue;
                        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                        _BEProceso.observacion = txtObservacion.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                        int val = _objBLProceso.spi_MON_SeguimientoProcesoSeleccion(_BEProceso);

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            TxtConvocatoriaTab1.Text = "";
                            txtFechaPubliTab1.Text = "";
                            ddlTipoAdjudicacionTab1.SelectedValue = "";
                            ddlResultadoTab1.SelectedValue = "";
                            txtObservacion.Text = "";

                            Panel_AgregarSeguimientoTab1.Visible = false;
                            imgbtn_AgregarSeguimientoTab1.Visible = true;
                            cargaSeguimientoProcesoTab1();
                            Up_Tab1.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }

                    }
                }
            }

        }

        protected void imgDocTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdSeguimientoProcesoTab1.Rows[row.RowIndex].FindControl("imgDocTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgDocSubContratoTab1_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdSubcontratosTab1.Rows[row.RowIndex].FindControl("imgDocSubContratoTab1");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void rbConsultorTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsultorTab1.Font.Bold = true;
            lblNomConsorcioTab1.Font.Bold = false;
            Up_lblNomConsultorTab1.Update();
            Up_lblNomConsorcioTab1.Update();

            rbConsorcioTab1.Checked = false;
            Up_rbConsorcioTab1.Update();
            Panel_ConsultorTab1.Visible = true;
            Panel_ConsorcioTab1.Visible = false;
            Up_rbProcesoTab1.Update();
        }

        protected void rbConsorcioTab1_OnCheckedChanged(object sender, EventArgs e)
        {
            lblNomConsorcioTab1.Font.Bold = true;
            lblNomConsultorTab1.Font.Bold = false;
            Up_lblNomConsorcioTab1.Update();
            Up_lblNomConsultorTab1.Update();

            rbConsultorTab1.Checked = false;
            Up_rbConsultorTab1.Update();
            Panel_ConsorcioTab1.Visible = true;
            Panel_ConsultorTab1.Visible = false;
            Up_rbProcesoTab1.Update();

        }

        protected void imgbtn_AgregarSeguimientoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = true;
            imgbtn_AgregarSeguimientoTab1.Visible = false;
            lblNomUsuarioSeguimientoProceso.Text = "";
            btnGuardarSeguimientoProcesoTab1.Visible = true;
            btnModificarSeguimientoProcesoTab1.Visible = false;
            Up_Tab1.Update();
        }

        protected void btnCancelaSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoTab1.Visible = false;
            imgbtn_AgregarSeguimientoTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected void imgbtn_AgregarConsultorTab1_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = true;
            imgbtn_AgregarConsultorTab1.Visible = false;
            btn_guardarConsultor.Visible = true;
            btn_modificarConsultor.Visible = false;
            Up_Tab1.Update();
        }

        protected void btn_cancelarConsultor_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarConsultorTab1.Visible = false;
            imgbtn_AgregarConsultorTab1.Visible = true;
            Up_Tab1.Update();
        }

        protected Boolean ValidarResultadoTab1()
        {
            Boolean result;
            result = true;

            //if (ddlAdjudicacionTab1.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione tipo de Adjudicación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtNroLicTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese N° de Licitación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (TxtAnio2Tab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese año.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtDetalleTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese detalle.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtProTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Buena Pro.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtProConsentidaTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Buena Pro Consentida.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtValorReferencialTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Valor Referencial.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (ddlTipoContratacionTab1.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione Tipo de Contrato.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtMontoContratadoTab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese monto contratado.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (rbConsultorTab1.Checked == true)
            {
                if (txtNombConsultorTab1.Text == "")
                {
                    txtNombConsultorTab1.Focus();
                    string script = "<script>alert('Ingrese nombre del contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                if (txtRucConsultorTab1.Text == "")
                {
                    txtRucConsultorTab1.Focus();
                    string script = "<script>alert('Ingrese RUC de contratista.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }



            if (rbConsorcioTab1.Checked == true)
            {
                if (txtNomconsorcioTab1.Text == "")
                {
                    txtNomconsorcioTab1.Focus();

                    string script = "<script>alert('Ingrese nombre del consorcio.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

                //if (txtRucConsorcioTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese RUC de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}
                //if (txtRepresentanteTab1.Text == "")
                //{
                //    string script = "<script>alert('Ingrese representante legal de consorcio.');</script>";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                //    result = false;
                //    return result;

                //}

            }

            //if (txtNroContrato.Text == "")
            //{
            //    string script = "<script>alert('Ingrese N° de contrato.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}



            return result;
        }

        protected void btnGuardarTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarResultadoTab1())
            {
                if (validaArchivo(FileUploadAdjContratoTab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                    if (ddlAdjudicacionTab1.SelectedValue == "")
                    {
                        _BEProceso.id_tipoAdjudicacion = 0;
                    }
                    else
                    { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }

                    _BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

                    if (txtNroLicTab1.Text == "")
                    { txtNroLicTab1.Text = "0"; }
                    _BEProceso.norLic = (txtNroLicTab1.Text);

                    _BEProceso.anio = TxtAnio2Tab1.Text;
                    _BEProceso.detalle = txtDetalleTab1.Text;
                    _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                    _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                    if (txtValorReferencialTab1.Text == "")
                    { txtValorReferencialTab1.Text = "0"; }
                    _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                    if (txtMontoContratadoTab1.Text == "")
                    { txtMontoContratadoTab1.Text = "0"; }

                    _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                    _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
                    if (rbConsultorTab1.Checked == true)
                    {
                        _BEProceso.id_tipoEmpresa = 1;
                        _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                        _BEProceso.rucContratista = txtRucConsultorTab1.Text;

                        if (FileUploadContratoContratistaTab1.HasFile)
                        {
                            _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                        }
                        else
                        { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }
                    }

                    if (rbConsorcioTab1.Checked == true)
                    {
                        _BEProceso.id_tipoEmpresa = 2;
                        _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                        _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                        _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                        _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                        if (FileUploadAdjContratoTab1.HasFile)
                        {
                            _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                        }
                        else
                        { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                    }

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);

                    int val = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso); //1


                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab1();

                        Up_Tab1.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                }
            }


        }

        protected Boolean ValidarGrupoConsorcioTab1()
        {
            Boolean result;
            result = true;

            if (txtContratistaGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese nombre de contratista.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtRucGrupTab1.Text == "")
            {
                string script = "<script>alert('Ingrese N° de RUC.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtRepresentanGrupTAb1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Representante Legal.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            return result;
        }

        protected void btn_guardarConsultor_OnClick(object sender, EventArgs e)
        {
            if (ValidarResultadoTab1())
            {
                if (ValidarGrupoConsorcioTab1())
                {
                    if (validaArchivo(FileUploadAdjContratoTab1))
                    {
                        //REGISTRAR PROCESO DE SELECCION INDIRECTA

                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                        _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue);

                        if (ddlAdjudicacionTab1.SelectedValue == "")
                        {
                            _BEProceso.id_tipoAdjudicacion = 0;
                        }
                        else
                        { _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlAdjudicacionTab1.SelectedValue); }

                        _BEProceso.TipoContratacion = ddlTipoContratacionTab1.SelectedValue;

                        if (txtNroLicTab1.Text == "")
                        { txtNroLicTab1.Text = "0"; }
                        _BEProceso.norLic = (txtNroLicTab1.Text);

                        _BEProceso.anio = TxtAnio2Tab1.Text;
                        _BEProceso.detalle = txtDetalleTab1.Text;
                        _BEProceso.Date_buenapro = VerificaFecha(txtProTab1.Text);
                        _BEProceso.Date_buenaProConsentida = VerificaFecha(txtProConsentidaTab1.Text);
                        if (txtValorReferencialTab1.Text == "")
                        { txtValorReferencialTab1.Text = "0"; }
                        _BEProceso.valorReferencial = txtValorReferencialTab1.Text;

                        if (txtMontoContratadoTab1.Text == "")
                        { txtMontoContratadoTab1.Text = "0"; }

                        _BEProceso.montoConsorcio = txtMontoContratadoTab1.Text;
                        _BEProceso.NroContratoConsorcio = (txtNroContrato.Text);
                        if (rbConsultorTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 1;
                            _BEProceso.nombreContratista = txtNombConsultorTab1.Text;
                            _BEProceso.rucContratista = txtRucConsultorTab1.Text;

                            if (FileUploadContratoContratistaTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadContratoContratistaTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoContratistaTab1.Text; }
                        }

                        if (rbConsorcioTab1.Checked == true)
                        {
                            _BEProceso.id_tipoEmpresa = 2;
                            _BEProceso.nombreConsorcio = txtNomconsorcioTab1.Text;
                            _BEProceso.rucConsorcio = txtRucConsorcioTab1.Text;
                            _BEProceso.representanteConsorcio = txtRepresentanteTab1.Text;
                            _BEProceso.telefonoConsorcio = txtTelefonoConsorcioTab1.Text;

                            if (FileUploadAdjContratoTab1.HasFile)
                            {
                                _BEProceso.urlContrato = _Metodo.uploadfile(FileUploadAdjContratoTab1);
                            }
                            else
                            { _BEProceso.urlContrato = lnkbtnContratoTab1.Text; }


                        }

                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        _BEProceso.Date_fechaConvenio = VerificaFecha(txtfechaConvenioTab1.Text);

                        int valTab1 = _objBLProceso.spi_MON_ProcesoSeleccion(_BEProceso); //1


                        //REGISTRA CONTRATISTA
                        _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEProceso.tipoFinanciamiento = 3;
                        _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                        _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                        _BEProceso.rucContratista = txtRucGrupTab1.Text;
                        _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                        _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        int val = _objBLProceso.spi_MON_GrupoConsorcio(_BEProceso);

                        if (val == 1 && valTab1 == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            cargaConsultoresTab1();

                            txtContratistaGrupTab1.Text = "";
                            txtRucGrupTab1.Text = "";
                            txtRepresentanGrupTAb1.Text = "";

                            Panel_AgregarConsultorTab1.Visible = false;
                            imgbtn_AgregarConsultorTab1.Visible = true;

                            Up_Tab1.Update();

                            CargaDatosTab1();
                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }

                }
            }

        }
        protected Boolean ValidarSubContratoTab1()
        {
            Boolean result;
            result = true;

            if (ddlRubroTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Rubro.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtnombreTab1.Text == "")
            {
                string script = "<script>alert('Ingrese Nombre');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese monto.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtPlazoTab1.Text == "")
            {
                string script = "<script>alert('Ingrese Plazo (Días).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlModalidaSubcontratoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese modalidad.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }
        protected void btnGuardarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {
            if (ValidarSubContratoTab1())
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

                _BEProceso.nombre = txtnombreTab1.Text;
                _BEProceso.id_rubro = Convert.ToInt32(ddlRubroTab1.SelectedValue);
                _BEProceso.plazo = Convert.ToInt32(txtPlazoTab1.Text);
                _BEProceso.monto = txtMontoTab1.Text;
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidaSubcontratoTab1.SelectedValue);
                _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spi_MON_SubContrato(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSubcontratosTab1();

                    // ddlModalidadTab1.SelectedValue = "";
                    txtnombreTab1.Text = "";
                    ddlRubroTab1.SelectedValue = "";
                    txtPlazoTab1.Text = "";
                    txtMontoTab1.Text = "";
                    ddlModalidaSubcontratoTab1.SelectedValue = "";

                    Panel_SubcontratosTab1.Visible = false;
                    imgbtnAgregarSubcontratoTab1.Visible = true;

                    Up_Tab1.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }
        protected void imgbtnAgregarSubcontratoTab1_OnClick(object sender, EventArgs e)
        {
            Panel_SubcontratosTab1.Visible = true;
            imgbtnAgregarSubcontratoTab1.Visible = false;
            btnGuardarSubcontratosTab1.Visible = true;
            btnModificarSubcontratosTab1.Visible = false;
            Up_Tab1.Update();
        }
        protected void btnCancelarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {
            Panel_SubcontratosTab1.Visible = false;
            imgbtnAgregarSubcontratoTab1.Visible = true;
            Up_Tab1.Update();
        }
        protected void grdSubcontratosTab1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocSubContratoTab1");

                GeneraIcoFile(imb.ToolTip, imb);


            }
        }

        protected void cargaSubcontratosTab1()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

            grdSubcontratosTab1.DataSource = _objBLProceso.F_spMON_SubContrato(_BEProceso);
            grdSubcontratosTab1.DataBind();


        }
        //protected void CargaResponsableTab1()
        //{
        //    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;

        //    //grdResponsableTab1.DataSource = _objBLProceso.spMON_ResponsableObra(_BEProceso);
        //    //grdResponsableTab1.DataBind();


        //}
        protected void btnModificarSeguimientoProcesoTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSeguimientoTab1())
            {


                _BEProceso.id_seguimiento = Convert.ToInt32(lblRegistroSeguimientoP.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.convocatoria = TxtConvocatoriaTab1.Text;
                _BEProceso.Date_fechaPublicacion = Convert.ToDateTime(txtFechaPubliTab1.Text);
                _BEProceso.id_tipoAdjudicacion = Convert.ToInt32(ddlTipoAdjudicacionTab1.SelectedValue);
                _BEProceso.resultado = ddlResultadoTab1.SelectedValue;

                if (FileUploadSeguimientoTAb1.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSeguimientoTAb1.Text;
                }


                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSeguimientoTAb1);


                _BEProceso.observacion = txtObservacion.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Procesos_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();


                    TxtConvocatoriaTab1.Text = "";
                    txtFechaPubliTab1.Text = "";
                    ddlTipoAdjudicacionTab1.SelectedValue = "";
                    ddlResultadoTab1.SelectedValue = "";
                    txtObservacion.Text = "";
                    LnkbtnSeguimientoTAb1.Text = "";
                    imgbtnSeguimientoTAb1.Visible = false;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarSeguimientoTab1.Visible = false;
                btnModificarSeguimientoProcesoTab1.Visible = true;
                cargaSeguimientoProcesoTab1();
                Up_Tab1.Update();
                imgbtn_AgregarSeguimientoTab1.Visible = true;

            }



        }
        protected void btnModificarSubcontratosTab1_OnClick(object sender, EventArgs e)
        {

            if (ValidarSubContratoTab1())
            {


                _BEProceso.id_subContratos = Convert.ToInt32(lblId_SubContratos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.id_rubro = Convert.ToInt32(ddlRubroTab1.SelectedValue);
                _BEProceso.nombre = txtnombreTab1.Text;
                _BEProceso.monto = txtMontoTab1.Text;
                _BEProceso.plazo = Convert.ToInt32(txtPlazoTab1.Text);
                _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidaSubcontratoTab1.SelectedValue);

                if (FileUploadSubContrato.PostedFile.ContentLength > 0)
                {
                    _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                }
                else
                {
                    _BEProceso.urlDoc = LnkbtnSubContrato.Text;
                }





                //_BEProceso.urlDoc = _Metodo.uploadfile(FileUploadSubContrato);
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_SubContrato_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    //CargaFinanTransferencia();


                    ddlRubroTab1.SelectedValue = "";
                    txtnombreTab1.Text = "";
                    txtMontoTab1.Text = "";
                    txtPlazoTab1.Text = "";
                    ddlModalidaSubcontratoTab1.SelectedValue = "";
                    LnkbtnSubContrato.Text = "";
                    imgbtnSubContrato.Visible = false;
                    imgbtnAgregarSubcontratoTab1.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_SubcontratosTab1.Visible = false;
                cargaSubcontratosTab1();
                btnModificarSubcontratosTab1.Visible = true;
                Up_Tab1.Update();

            }


        }
        protected void btn_modificarConsultor_OnClick(object sender, EventArgs e)
        {

            if (ValidarGrupoConsorcioTab1())
            {

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(lblId_Registro_Contratista.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEProceso.nombreContratista = txtContratistaGrupTab1.Text;
                _BEProceso.rucContratista = txtRucGrupTab1.Text;
                _BEProceso.representante = txtRepresentanGrupTAb1.Text;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int resul;
                resul = _objBLProceso.spud_MON_Seguimiento_Consorcio_Editar(_BEProceso);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    // CargaFinanTransferencia();

                    txtContratistaGrupTab1.Text = "";
                    txtRucGrupTab1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";
                    txtRepresentanGrupTAb1.Text = "";

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarConsultorTab1.Visible = false;
                cargaConsultoresTab1();
                btn_modificarConsultor.Visible = true;
                imgbtn_AgregarConsultorTab1.Visible = true;
                Up_Tab1.Update();


            }
        }
        protected void grdSeguimientoProcesoTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoProcesoTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_seguimiento = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Procesos_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSeguimientoProcesoTab1();
                    Up_Tab1.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grdSubcontratosTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSubcontratosTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_subContratos = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_SubContrato_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaSubcontratosTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected void grdConsultoresTab1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdConsultoresTab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id_grupo_consorcio = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.spud_MON_Seguimiento_Consorcio_Eliminar(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaConsultoresTab1();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }
        protected Boolean ValidarProcesoIndirectaNETab1()
        {
            Boolean result;
            result = true;

            //if (txtNroConvenioNETab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese nro de convenio.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaConvenioNETab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese fecha de convenio.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (_Metodo.ValidaFecha(txtFechaConvenioNETab1.Text) == false)
            //{
            //    string script = "<script>alert('Formato no valido de fecha de convenio.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //if (Convert.ToDateTime(txtFechaConvenioNETab1.Text) > DateTime.Today || Convert.ToDateTime(txtFechaConvenioNETab1.Text).Year < 2012)
            //{
            //    string script = "<script>alert('La fecha de convenio no debe ser mayor a la fecha actual ni menor al año 2012.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //if (ddlTipoAlcanceNETab1.SelectedValue == "")
            //{
            //    string script = "<script>alert('Seleccione alcance del convenio.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtFechaActaConstitucionNETab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de constitución de NE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaActaConstitucionNETab1.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de constitución de NE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaActaConstitucionNETab1.Text) > DateTime.Today || Convert.ToDateTime(txtFechaActaConstitucionNETab1.Text).Year < 2013)
            {
                string script = "<script>alert('La fecha de constitución de NE no debe ser mayor a la fecha actual ni menor al año 2013.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaPresentacionTab1.Text.Length>1)
            {
                if (Convert.ToDateTime(txtFechaActaConstitucionNETab1.Text) > Convert.ToDateTime(txtFechaPresentacionTab1.Text))
                {
                    string script = "<script>alert('La fecha de constitución de NE no debe ser menor o igual a la fecha de presentación de solicitud.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }
            if (txtAsistentesHombres.Text == "")
            {
                string script = "<script>alert('Ingrese nro. de asistentes hombres.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtAsistentesMujeres.Text == "")
            {
                string script = "<script>alert('Ingrese nro. de asistentes mujeres.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            return result;
        }
        protected void btnGuardarProcesoIndirectaNE_Click(object sender, EventArgs e)
        {
            if (ValidarProcesoIndirectaNETab1())
            {

                if (validaArchivo(FileUploadActaAsambleaNETab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue); // Value default 4

                    //_BEProceso.ConvenioContrato = txtNroConvenioNETab1.Text;
                    //_BEProceso.Date_fechaConvenio = VerificaFecha(txtFechaConvenioNETab1.Text);
                    //_BEProceso.idTipoAlcance = Convert.ToInt32(ddlTipoAlcanceNETab1.SelectedValue);

                    _BEProceso.Date_fechActaConstitucionNE = VerificaFecha(txtFechaActaConstitucionNETab1.Text);
                    _BEProceso.hombres = Convert.ToInt32(txtAsistentesHombres.Text);
                    _BEProceso.mujeres = Convert.ToInt32(txtAsistentesMujeres.Text);

                    if (FileUploadActaAsambleaNETab1.HasFile)
                    {
                        _BEProceso.urlActaAsamblea = _Metodo.uploadfile(FileUploadActaAsambleaNETab1);
                    }
                    else
                    { _BEProceso.urlActaAsamblea = lnkbtnActaAsambleaNETab1.Text; }

                    //if (FileUploadConvenioNETab1.HasFile)
                    //{
                    //    _BEProceso.urlConvenio = _Metodo.uploadfile(FileUploadConvenioNETab1);
                    //}
                    //else
                    //{ _BEProceso.urlConvenio = lnkbtnConvenioNETab1.Text; }

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLProceso.I_spi_MON_ProcesoSeleccionIndirectaNE(_BEProceso); //1

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente la constitución del NE.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab1();

                        Up_Tab1.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }

            }

        }
        protected void lnkbtnActaAsambleaNETab1_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnActaAsambleaNETab1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }
        public int ObtenerEdad(DateTime fechaNacimiento)
        {
            //Obtengo la diferencia en años.
            int edad = DateTime.Now.Year - fechaNacimiento.Year;

            //Obtengo la fecha de cumpleaños de este año.
            DateTime nacimientoAhora = fechaNacimiento.AddYears(edad);
            //Le resto un año si la fecha actual es anterior 
            //al día de nacimiento.
            if (DateTime.Now.CompareTo(nacimientoAhora) > 0)
            {
                edad--;
            }

            return edad;
        }

        protected Boolean ValidarRegistroPadronMiembroNETab1()
        {
            Boolean result;
            result = true;

            if (ddlTipoDocumentoPATab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtNroDocumentoPATab1.Text == "")
            {
                string script = "<script>alert('Ingrese nro. de documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlTipoDocumentoPATab1.SelectedValue == "1")
            {
                if (txtNroDocumentoPATab1.Text.Length != 8)
                {
                    string script = "<script>alert('El  nro. de DNI debe de ser de 8 digitos.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }

            List<BE_MON_PROCESO> ListPadron = new List<BE_MON_PROCESO>();
            ListPadron = _objBLProceso.F_spMON_PadronMiembrosNE();

            if (ListPadron != null)
            {
                ListPadron = (from cust in ListPadron
                              where cust.nroDocumento == txtNroDocumentoPATab1.Text
                              select cust).ToList();
            }

            if (ListPadron != null)
            {
                if (ListPadron.Count > 0)
                {
                    string script = "<script>alert('El DNI registrado ya existe.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }


            if (txtApellidoPaternoPATab1.Text == "")
            {
                string script = "<script>alert('Ingrese apellido paterno.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtApellidoMaternoPATab1.Text == "")
            {
                string script = "<script>alert('Ingrese apellido materno.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtNombrePATab1.Text == "")
            {
                string script = "<script>alert('Ingrese nombre.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (ddlSexoPATab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione tipo de sexo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtFechaNacimientoPATab1.Text == "")
            //{
            //    string script = "<script>alert('Ingrese fecha de nacimiento.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (_Metodo.ValidaFecha(txtFechaNacimientoPATab1.Text) == false)
            //{
            //    string script = "<script>alert('Formato no valido de fecha de nacimiento.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            //int Edad = ObtenerEdad(Convert.ToDateTime(txtFechaNacimientoPATab1.Text));

            //if (Edad < 18 || Edad > 100)
            //{
            //    string script = "<script>alert('La edad debe ser igual o mayor a 18 y menor a 100.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            return result;
        }
        protected void btnGuardarRegistroMiembrosPATab1_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroPadronMiembroNETab1())
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.tipoDocumento = Convert.ToInt32(ddlTipoDocumentoPATab1.SelectedValue);
                _BEProceso.nroDocumento = txtNroDocumentoPATab1.Text;
                _BEProceso.apellidoPaterno = txtApellidoPaternoPATab1.Text;
                _BEProceso.apellidoMaterno = txtApellidoMaternoPATab1.Text;
                _BEProceso.nombre = txtNombrePATab1.Text;
                _BEProceso.sexo = ddlSexoPATab1.SelectedValue;
                _BEProceso.Date_fechaNacimiento = VerificaFecha(txtFechaNacimientoPATab1.Text);
                _BEProceso.domicilio = txtDomicilioPATab1.Text;
                _BEProceso.cod_depa = ddlDepaPATab1.SelectedValue;
                _BEProceso.cod_prov = ddlProvPATab1.SelectedValue;
                _BEProceso.cod_dist = ddlDistPATab1.SelectedValue;
                _BEProceso.cod_CCPP = ddlCCPPPATab1.SelectedValue;

                if (ddlTipoGradoInstruccionPATab1.SelectedValue == "")
                {
                    _BEProceso.tipoGrado = 0;
                }
                else
                {
                    _BEProceso.tipoGrado = Convert.ToInt32(ddlTipoGradoInstruccionPATab1.SelectedValue);
                }

                if (ddlTipoOcupacionPATab1.SelectedValue == "")
                {
                    _BEProceso.tipoOcupacion = 0;
                }
                else
                {
                    _BEProceso.tipoOcupacion = Convert.ToInt32(ddlTipoOcupacionPATab1.SelectedValue);
                }

                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.I_spi_MON_PadronMiembrosNE(_BEProceso); //devuelve ID de miembro

                if (val == 999999) //DNI YA EXISTE
                {
                    string script = "<script>alert('El DNI ingresado ya se encuentra registrado. Verificar.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else if (val > 0) { 
                    string script = "<script>alert('Se registró correctamente en el padron de miembros de NE.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaPadronMiembrosNE();

                    List<BE_MON_PROCESO> ListPadron = new List<BE_MON_PROCESO>();
                    ListPadron = _objBLProceso.F_spMON_PadronMiembrosNE();

                    if (ListPadron != null)
                    {
                        ListPadron = (from cust in ListPadron
                                      where cust.id == val
                                      select cust).ToList();
                    }
                    xcmbNroDocumentoTab1.Value = ListPadron.ElementAt(0).nroDocumento;

                    MPE_MiembrosNucleoTab1.Hide();
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }
        protected void cargaPadronMiembrosNE()
        {

            List<BE_MON_PROCESO> ListPadron = new List<BE_MON_PROCESO>();
            ListPadron = _objBLProceso.F_spMON_PadronMiembrosNE();

            if (ListPadron != null)
            {
                if (ddlTipoDocumentoTab1.SelectedValue != "")
                {
                    int tipoDocumento = Convert.ToInt32(ddlTipoDocumentoTab1.SelectedValue);
                    ListPadron = (from cust in ListPadron
                                  where cust.id_tipoDocumentoidentificacion == tipoDocumento
                                  select cust).ToList();
                }
                else
                {
                    //  ListPadron = null;
                }
            }
            xcmbNroDocumentoTab1.DataSource = ListPadron;

            xcmbNroDocumentoTab1.ValueField = "id";
            xcmbNroDocumentoTab1.TextField = "nroDocumento";
            xcmbNroDocumentoTab1.DataBind();
        }

        protected void CargaTipoEstadoAsignacionTab1()
        {
            ddlTipoEstadoAsignacionTab1.DataSource = _objBLProceso.F_spMON_TipoEstadoAsignacionNE();
            ddlTipoEstadoAsignacionTab1.DataTextField = "nombre";
            ddlTipoEstadoAsignacionTab1.DataValueField = "valor";
            ddlTipoEstadoAsignacionTab1.DataBind();

            ddlTipoEstadoAsignacionTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));
        }

        protected void CargaTipoCargoTab1()
        {
            ddlTipoCargoTab1.DataSource = _objBLProceso.F_spMON_TipoCargoMiembrosNE();
            ddlTipoCargoTab1.DataTextField = "nombre";
            ddlTipoCargoTab1.DataValueField = "valor";
            ddlTipoCargoTab1.DataBind();

            ddlTipoCargoTab1.Items.Insert(0, new ListItem("-SELECCIONE-", ""));
        }

        protected void lnkbtnActaDesignacionTab1_Click(object sender, EventArgs e)
        {

        }

        protected Boolean ValidarRegistroMiembroNETab1()
        {
            Boolean result;
            result = true;

            if (xcmbNroDocumentoTab1.SelectedItem.Text == "")
            {
                string script = "<script>alert('Seleccione Nro. de documento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoCargoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione cargo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (ddlTipoEstadoAsignacionTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione estado de asignación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaAsignaciónTab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de asignación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaAsignaciónTab1.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de asignación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaAsignaciónTab1.Text) > DateTime.Today)
            {
                string script = "<script>alert('La fecha de asignación debe ser menor a la de hoy.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaActaConstitucionNETab1.Text == "")
            {
                string script = "<script>alert('Registre primero datos del Nucleo Ejecutor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (Convert.ToDateTime(txtFechaAsignaciónTab1.Text) < Convert.ToDateTime(txtFechaActaConstitucionNETab1.Text))
            {
                string script = "<script>alert('La fecha de asignación no debe ser menor a la fecha del acta de la asamblea de constitución de NE.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }




            if (ddlTipoCargoTab1.SelectedValue == "5")
            {

                if (txtFechaDesignacionTab1.Text == "")
                {
                    string script = "<script>alert('Ingrese fecha de designación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (_Metodo.ValidaFecha(txtFechaDesignacionTab1.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de designación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }
            return result;
        }

        protected void btnGuardarMiembroNETab1_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroMiembroNETab1())
            {
                if (validaArchivo(FileUploadActaDesignacionTab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue); // Value default 4

                    _BEProceso.id_tipoDocumentoidentificacion = Convert.ToInt32(ddlTipoDocumentoTab1.SelectedValue);
                    _BEProceso.nroDocumento = xcmbNroDocumentoTab1.SelectedItem.Text;
                    _BEProceso.id_TipoCargo = Convert.ToInt32(ddlTipoCargoTab1.SelectedValue);
                    _BEProceso.id_TipoEstadoAsignacion = Convert.ToInt32(ddlTipoEstadoAsignacionTab1.SelectedValue);

                    _BEProceso.Date_fechaAsignacion = VerificaFecha(txtFechaAsignaciónTab1.Text);

                    if (ddlTipoCargoTab1.SelectedValue == "5")
                    {
                        _BEProceso.Date_fechaDesignacion = VerificaFecha(txtFechaDesignacionTab1.Text);
                        _BEProceso.urlDoc = FileUploadActaDesignacionTab1.HasFile ? _Metodo.uploadfile(FileUploadActaDesignacionTab1) : lnkbtnActaDesignacionTab1.Text;
                    }
                    else
                    {
                        _BEProceso.Date_fechaDesignacion = VerificaFecha("");
                        _BEProceso.urlDoc = "";
                    }
                }
                //_BEProceso.urlDoc = "";
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.I_spi_MON_ProcesoMiembroNE(_BEProceso);

                if (val > 0)
                {
                    string script = "<script>alert('Se registró correctamente el miembro del NE.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_MiembrosNE.Visible = false;
                    CargaProcesoMiembroNE();

                    imgbtnAgregaMiembroNucleoTab1.Visible = true;
                    Up_Tab1.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void CargaProcesoMiembroNE()
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.tipoFinanciamiento = 3;
            _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;


            grdMiembroNucleoTAb1.DataSource = _objBLProceso.F_spMON_ProcesoMiembroNE(_BEProceso);
            grdMiembroNucleoTAb1.DataBind();

        }

        protected void imgbtnAgregaMiembroNucleoTab1_Click(object sender, ImageClickEventArgs e)
        {
            Panel_MiembrosNE.Visible = true;
            imgbtnAgregaMiembroNucleoTab1.Visible = false;
            ddlTipoDocumentoTab1.SelectedValue = "1";
            xcmbNroDocumentoTab1.Text = "";

            ddlTipoCargoTab1.SelectedValue = "";
            ddlTipoEstadoAsignacionTab1.SelectedValue = "";
            txtFechaAsignaciónTab1.Text = "";
            txtFechaDesignacionTab1.Text = "";

            lnkbtnActaDesignacionTab1.Text = "";
            imgbtnActaDesignacionTab1.ImageUrl = "~/img/blanco.png";

            btnModificarMiembrNETab1.Visible = false;
            btnGuardarMiembroNETab1.Visible = true;

            cargaPadronMiembrosNE();
            Up_Tab1.Update();

        }
        protected void btnCancelarNETab1_Click(object sender, EventArgs e)
        {
            Panel_MiembrosNE.Visible = false;
            imgbtnAgregaMiembroNucleoTab1.Visible = true;

        }
        protected void lnkRegistrarPersonal_Click(object sender, EventArgs e)
        {

            txtNroDocumentoPATab1.Text = "";
            ddlTipoDocumentoPATab1.SelectedValue = "1";
            txtApellidoPaternoPATab1.Text = "";
            txtApellidoMaternoPATab1.Text = "";
            txtNombrePATab1.Text = "";
            ddlSexoPATab1.SelectedValue = "";
            txtFechaNacimientoPATab1.Text = "";
            //txtAñoResidenciaPATab1.Text = "0";
            txtDomicilioPATab1.Text = "";
            ddlTipoGradoInstruccionPATab1.SelectedValue = "";
            ddlTipoOcupacionPATab1.SelectedValue = "";
            lblNomUsuarioRegistroMiembroNETab01.Text = "";

            HabilitarDNIPadronNE();
            Up_RegistroMiembrosNE.Update();

            MPE_MiembrosNucleoTab1.Show();

            
        }
        protected void ddlDepaPATab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("2", ddlDepaPATab1.SelectedValue, "", "", ddlProvPATab1);
            CargaUbigeo("3", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, "", ddlDistPATab1);
            CargaUbigeo("4", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, ddlDistPATab1.SelectedValue, ddlCCPPPATab1);
            Up_RegistroMiembrosNE.Update();
        }
        protected void ddlProvPATab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("3", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, "", ddlDistPATab1);
            CargaUbigeo("4", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, ddlDistPATab1.SelectedValue, ddlCCPPPATab1);
            Up_RegistroMiembrosNE.Update();
        }
        protected void ddlDistPATab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("4", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, ddlDistPATab1.SelectedValue, ddlCCPPPATab1);
            Up_RegistroMiembrosNE.Update();
        }

        protected void grdMiembroNucleoTAb1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "editar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow row = ctl.NamingContainer as GridViewRow;
                object objTemp = grdMiembroNucleoTAb1.DataKeys[row.RowIndex].Value as object;
                lblIdProcesoMiembro.Text = objTemp.ToString();

                Panel_MiembrosNE.Visible = true;
                imgbtnAgregaMiembroNucleoTab1.Visible = false;

                ddlTipoDocumentoTab1.SelectedValue = ((Label)row.FindControl("lblIdTipoDocumento")).Text;
                xcmbNroDocumentoTab1.Text = ((Label)row.FindControl("lblNrodocumento")).Text;

                ddlTipoCargoTab1.SelectedValue = ((Label)row.FindControl("lblIdCargo")).Text;
                ddlTipoEstadoAsignacionTab1.SelectedValue = ((Label)row.FindControl("lblIdEstadoAsignacion")).Text;

                txtFechaAsignaciónTab1.Text = ((Label)row.FindControl("lblFechaAsignación")).Text;
                txtFechaDesignacionTab1.Text = ((Label)row.FindControl("lblFechaDesignación")).Text;

                lblNomUsuarioRegistroMiembroNETab1.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

                lnkbtnActaDesignacionTab1.Text = ((Label)row.FindControl("lblUrlDoc")).Text;
                GeneraIcoFile(lnkbtnActaDesignacionTab1.Text, imgbtnActaDesignacionTab1);

                if (ddlTipoCargoTab1.SelectedValue == "5")
                {
                    trActaDesignacionTab1.Visible = true;
                    trFechaDesignacionTab1.Visible = true;
                }
                else
                {
                    trActaDesignacionTab1.Visible = false;
                    trFechaDesignacionTab1.Visible = false;
                }

                btnModificarMiembrNETab1.Visible = true;
                btnGuardarMiembroNETab1.Visible = false;
                Panel_MiembrosNE.Visible = true;
                imgbtnAgregaMiembroNucleoTab1.Visible = false;


            }
            else
            {
                if (e.CommandName == "eliminar")
                {
                    Control ctl = e.CommandSource as Control;
                    GridViewRow row = ctl.NamingContainer as GridViewRow;
                    object objTemp = grdMiembroNucleoTAb1.DataKeys[row.RowIndex].Value as object;
                    lblIdProcesoMiembro.Text = objTemp.ToString();

                    _BEProceso.id = Convert.ToInt32(lblIdProcesoMiembro.Text);
                    _BEProceso.id_tipo = 2; // ELIMINAR
                    _BEProceso.Date_fechaAsignacion = VerificaFecha("");
                    _BEProceso.Date_fechaDesignacion = VerificaFecha("");
                    _BEProceso.nroDocumento = "";
                    _BEProceso.urlDoc = "";
                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    int val = _objBLProceso.UD_spud_MON_ProcesoMiembroNE(_BEProceso);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se eliminó correctamente el miembro del NE.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaProcesoMiembroNE();

                        Panel_MiembrosNE.Visible = false;
                        imgbtnAgregaMiembroNucleoTab1.Visible = true;
                        Up_Tab1.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }

                }
            }

        }
        protected void btnModificarMiembrNETab1_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroMiembroNETab1())
            {
                if (validaArchivo(FileUploadActaDesignacionTab1))
                {
                    _BEProceso.id = Convert.ToInt32(lblIdProcesoMiembro.Text);

                    _BEProceso.id_tipo = 1; // MODIFICAR

                    _BEProceso.id_tipoDocumentoidentificacion = Convert.ToInt32(ddlTipoDocumentoTab1.SelectedValue);
                    _BEProceso.nroDocumento = xcmbNroDocumentoTab1.SelectedItem.Text;
                    _BEProceso.id_TipoCargo = Convert.ToInt32(ddlTipoCargoTab1.SelectedValue);
                    _BEProceso.id_TipoEstadoAsignacion = Convert.ToInt32(ddlTipoEstadoAsignacionTab1.SelectedValue);

                    _BEProceso.Date_fechaAsignacion = VerificaFecha(txtFechaAsignaciónTab1.Text);


                    if (ddlTipoCargoTab1.SelectedValue == "5")
                    {
                        _BEProceso.Date_fechaDesignacion = VerificaFecha(txtFechaDesignacionTab1.Text);
                        _BEProceso.urlDoc = FileUploadActaDesignacionTab1.HasFile ? _Metodo.uploadfile(FileUploadActaDesignacionTab1) : lnkbtnActaDesignacionTab1.Text;
                    }
                    else
                    {
                        _BEProceso.Date_fechaDesignacion = VerificaFecha("");
                        _BEProceso.urlDoc = "";
                    }

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    int val = _objBLProceso.UD_spud_MON_ProcesoMiembroNE(_BEProceso);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente el miembro del NE.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaProcesoMiembroNE();

                        Panel_MiembrosNE.Visible = false;
                        imgbtnAgregaMiembroNucleoTab1.Visible = true;
                        Up_Tab1.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }

        protected void lnkbtnSolicitudTab1_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnSolicitudTab1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void lnkbtnResultadoTab1_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnResultadoTab1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected Boolean ValidarProcesoIndirectaNETab1Solicitud()
        {
            Boolean result;
            result = true;

            if (txtFechaPresentacionTab1.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de presentación de solicitud.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaPresentacionTab1.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de presentación de solicitud.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaPresentacionTab1.Text) > DateTime.Today || Convert.ToDateTime(txtFechaPresentacionTab1.Text).Year < 2012)
            {
                string script = "<script>alert('La fecha de solicitud no debe ser mayor a la fecha actual ni menor al año 2012.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaAprobacionTab1.Text.Length > 1)
            {
                if (Convert.ToDateTime(txtFechaPresentacionTab1.Text) > Convert.ToDateTime(txtFechaAprobacionTab1.Text))
                {
                    string script = "<script>alert('La fecha de solicitud no debe ser menor o igual  a la fecha de aprobación de la solicitud');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaConvenioNETab1.Text.Length>1)
                {
                    if (Convert.ToDateTime(txtFechaAprobacionTab1.Text) > Convert.ToDateTime(txtFechaConvenioNETab1.Text))
                    {
                        string script = "<script>alert('La fecha de aprobación de solicitud no debe ser menor o igual  a la fecha de convenio');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }

            if (txtFechaActaConstitucionNETab1.Text.Length > 1)
            {
                if (Convert.ToDateTime(txtFechaPresentacionTab1.Text) < Convert.ToDateTime(txtFechaActaConstitucionNETab1.Text))
                {
                    string script = "<script>alert('La fecha de solicitud no debe ser mayor o igual a la fecha de acta de constitución de NE.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (rbAceptadoTab1.Checked == false && rbRechazadoTab1.Checked == false)
            {
                string script = "<script>alert('Indicar resultado de solicitud.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }



            if (rbRechazadoTab1.Checked == true)
            {
                if (txtMotivoTab1.Text.Trim() == "")
                {
                    string script = "<script>alert('Ingrese motivo de rechazo de solicitud.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }

            }


            return result;
        }
        protected void btnActualizaSolicitudTab1_Click(object sender, EventArgs e)
        {
            if (ValidarProcesoIndirectaNETab1Solicitud())
            {

                if (validaArchivo(FileUploadActaAsambleaNETab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue); // Value default 4

                    _BEProceso.Date_fechaSolicitud = VerificaFecha(txtFechaPresentacionTab1.Text);
                    _BEProceso.Date_fechaAprobacionSolicitud = VerificaFecha(txtFechaAprobacionTab1.Text);

                    if (FileUploadSolicitudTab1.HasFile)
                    {
                        _BEProceso.urlSolicitud = _Metodo.uploadfile(FileUploadSolicitudTab1);
                    }
                    else
                    { _BEProceso.urlSolicitud = lnkbtnSolicitudTab1.Text; }

                    if (rbAceptadoTab1.Checked)
                    {
                        _BEProceso.flagResultado = 1;
                    }
                    else
                    {
                        _BEProceso.flagResultado = 0;
                    }
                    _BEProceso.observacion = txtMotivoTab1.Text;

                    if (FileUploadResultadoTab1.HasFile)
                    {
                        _BEProceso.urlDoc = _Metodo.uploadfile(FileUploadResultadoTab1);
                    }
                    else
                    { _BEProceso.urlDoc = lnkbtnResultadoTab1.Text; }


                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLProceso.I_spi_MON_ProcesoSeleccionIndirectaNESolicitud(_BEProceso); //1

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente la solicitud de convenio.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab1();
                        CargaDatosTab2();
                        Up_Tab1.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }

            }
        }
        protected void rbAceptadoTab1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAceptadoTab1.Checked)
            {
                txtMotivoTab1.Text = "";
                lnkbtnResultadoTab1.Text = "";

                trMotivoTab1.Visible = false;
                trArchivoMotivoTab1.Visible = false;

                trAprobacionTab1.Visible = true;
            }
        }
        protected void rbRechazadoTab1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRechazadoTab1.Checked)
            {
                txtMotivoTab1.Text = "";
                lnkbtnResultadoTab1.Text = "";

                trMotivoTab1.Visible = true;
                trArchivoMotivoTab1.Visible = true;
                trAprobacionTab1.Visible = false;
            }
        }
        protected void lnkbtnConvenioNETab1_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnConvenioNETab1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void ddlTipoCargoTab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoCargoTab1.SelectedValue == "5")
            {
                trActaDesignacionTab1.Visible = true;
                trFechaDesignacionTab1.Visible = true;
            }
            else
            {
                trActaDesignacionTab1.Visible = false;
                trFechaDesignacionTab1.Visible = false;


            }
        }

        protected Boolean ValidarRendicionCuentaTab1()
        {
            Boolean result;
            result = true;

            if (txtAsistentesMujeresTab1.Text == "")
            {
                string script = "<script>alert('Ingresar Nro. de mujeres.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtAsistentesHombresTab1.Text == "")
            {
                string script = "<script>alert('Ingresar Nro. de hombres.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (txtFechaRendicionTab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de rendición.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaRendicionTab1.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de rendición.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaRendicionTab1.Text) > DateTime.Today)
            {
                string script = "<script>alert('La fecha de rendición no puede ser mayor a la de hoy.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (txtMontoRendicionTab1.Text == "")
            {
                string script = "<script>alert('Resgistre primero datos del Nucleo Ejecutor.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            return result;
        }
        protected void btnGuardarRendicionTab1_Click(object sender, EventArgs e)
        {
            if (ValidarRendicionCuentaTab1())
            {
                if (validaArchivo(FileUploadActaAsistenciaTab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue); // Value default 4

                    _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaRendicionTab1.Text);
                    _BEProceso.hombres = Convert.ToInt32(txtAsistentesHombresTab1.Text);
                    _BEProceso.mujeres = Convert.ToInt32(txtAsistentesMujeresTab1.Text);
                    _BEProceso.monto = txtMontoRendicionTab1.Text;
                    _BEProceso.urlDoc = FileUploadActaAsistenciaTab1.HasFile ? _Metodo.uploadfile(FileUploadActaAsistenciaTab1) : lnkbtnActaAsistencia.Text;

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLProceso.I_spi_MON_RendicionCuentaNE(_BEProceso);

                    if (val > 0)
                    {
                        string script = "<script>alert('Se registró correctamente la rendición de cuenta.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaRendicionCuentaNETab1();

                        Panel_RendicionCuentaTab1.Visible = false;
                        imgbtnAgregarRendicionCuentaTab1.Visible = true;
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }

        }
        protected void btnCancelarRendicionTab1_Click(object sender, EventArgs e)
        {
            Panel_RendicionCuentaTab1.Visible = false;
            imgbtnAgregarRendicionCuentaTab1.Visible = true;

        }
        protected void btnModificarRendicionTab1_Click(object sender, EventArgs e)
        {
            if (ValidarRendicionCuentaTab1())
            {
                if (validaArchivo(FileUploadActaAsistenciaTab1))
                {
                    _BEProceso.id = Convert.ToInt32(lblIdRendicionTab1.Text);
                    _BEProceso.id_tipo = 1;

                    _BEProceso.Date_fechaPublicacion = VerificaFecha(txtFechaRendicionTab1.Text);
                    _BEProceso.hombres = Convert.ToInt32(txtAsistentesHombresTab1.Text);
                    _BEProceso.mujeres = Convert.ToInt32(txtAsistentesMujeresTab1.Text);
                    _BEProceso.monto = txtMontoRendicionTab1.Text;
                    _BEProceso.urlDoc = FileUploadActaAsistenciaTab1.HasFile ? _Metodo.uploadfile(FileUploadActaAsistenciaTab1) : lnkbtnActaAsistencia.Text;

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLProceso.UD_spud_MON_RendicionCuentaNE(_BEProceso);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente la rendición de cuenta.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaRendicionCuentaNETab1();

                        Panel_RendicionCuentaTab1.Visible = false;
                        imgbtnAgregarRendicionCuentaTab1.Visible = true;
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }

        }
        protected void imgbtnAgregarRendicionCuentaTab1_Click(object sender, ImageClickEventArgs e)
        {
            Panel_RendicionCuentaTab1.Visible = true;
            imgbtnAgregarRendicionCuentaTab1.Visible = false;

            btnGuardarRendicionTab1.Visible = true;
            btnModificarRendicionTab1.Visible = false;
        }
        protected void lnkbtnActaAsistencia_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnActaAsistencia.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdRendicionCuentaNETab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_rendicionCuenta;
            id_rendicionCuenta = grdRendicionCuentaNETab1.SelectedDataKey.Value.ToString();
            lblIdRendicionTab1.Text = id_rendicionCuenta.ToString();
            GridViewRow row = grdRendicionCuentaNETab1.SelectedRow;

            txtFechaRendicionTab1.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtAsistentesHombresTab1.Text = ((Label)row.FindControl("lblHombres")).Text;
            txtAsistentesMujeresTab1.Text = ((Label)row.FindControl("lblMujeres")).Text;
            txtMontoRendicionTab1.Text = ((Label)row.FindControl("lblMonto")).Text;

            lnkbtnActaAsistencia.Text = ((ImageButton)row.FindControl("imgActaRendicionTab2")).ToolTip;
            GeneraIcoFile(lnkbtnActaAsistencia.Text, imgbtnActaAsistencia);

            lblNomActualizaRendicionTab1.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            Panel_RendicionCuentaTab1.Visible = true;
            imgbtnAgregarRendicionCuentaTab1.Visible = false;

            btnGuardarRendicionTab1.Visible = false;
            btnModificarRendicionTab1.Visible = true;

        }
        protected void grdRendicionCuentaNETab1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdRendicionCuentaNETab1.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEProceso.id = Convert.ToInt32(objTemp.ToString());
                _BEProceso.id_tipo = 2;
                _BEProceso.Date_fechaPublicacion = VerificaFecha("");
                _BEProceso.monto = "0";
                _BEProceso.urlDoc = "";
                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.UD_spud_MON_RendicionCuentaNE(_BEProceso);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación correcta de rendición de cuenta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaRendicionCuentaNETab1();

                    Panel_RendicionCuentaTab1.Visible = false;
                    imgbtnAgregarRendicionCuentaTab1.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdRendicionCuentaNETab1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgActaRendicionTab2");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }
        protected void imgActaRendicionTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdRendicionCuentaNETab1.Rows[row.RowIndex].FindControl("imgActaRendicionTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }



        #endregion

        #region Tab2


        protected void CargaTab2()
        {

            CargaTipoAdjudicacionTab2();
            CargaDatosTab2();

            if (!this.IsPostBack)
            {
                grdAvanceFisicoTab2.PageIndex = Int32.MaxValue;
                //grdAvanceFisicoAdicionalTab2.PageIndex = Int32.MaxValue;
                grdEvaluacionTab2.PageIndex = Int32.MaxValue;
                grdProgramacionTab2.PageIndex = Int32.MaxValue;
            }

            CargaAvanceFisico();
            CargaTipoMonitoreo();
            CargaTipoDocumentos();

            CargaTipoEstadoSituacionalTab2();
            CargaEstadoPreLiquidacionTab2();
            //CargaTipoValorizacion();


            cargarEvaluacionTab2();
            CargaEstadoAsignacionTab2();
            cargaFuncion();
            // CargaAgenteEjecucion();
            CargaAgentesAsignados();

            cargaProgramacionTab2();

            CargaSaldoBancario();
            cargaDesembolsosTab2();

            //cargarConexionesPNSR();
        }

        protected void cargaAgentexFuncion(DropDownList ddl, string tipoAgente)
        {
            ddl.DataSource = _objBLBandeja.F_spMON_ListarTecnicoxTipo(lblCOD_SUBSECTOR.Text, tipoAgente);
            ddl.DataTextField = "tecnico";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void cargaFuncion()
        {
            List<BE_MON_BANDEJA> ListAgente = new List<BE_MON_BANDEJA>();
            ListAgente = _objBLBandeja.F_spMON_TipoAgente(3);

            ddlFuncionTab2.DataSource = ListAgente;
            ddlFuncionTab2.DataTextField = "nombre";
            ddlFuncionTab2.DataValueField = "valor";
            ddlFuncionTab2.DataBind();
            ddlFuncionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));



        }
        //protected void CargaTipoValorizacion()
        //{
        //    ddlValorizacionTab2.DataSource = _objBLEjecucion.F_spMON_TipoValorizacion();
        //    ddlValorizacionTab2.DataTextField = "nombre";
        //    ddlValorizacionTab2.DataValueField = "valor";
        //    ddlValorizacionTab2.DataBind();

        //    ddlValorizacionTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}

        protected void CargaEstadoAsignacionTab2()
        {
            ddlEstadoAsignacionAgenteTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoAsignacionAgente();
            ddlEstadoAsignacionAgenteTab2.DataTextField = "nombre";
            ddlEstadoAsignacionAgenteTab2.DataValueField = "valor";
            ddlEstadoAsignacionAgenteTab2.DataBind();

            ddlEstadoAsignacionAgenteTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }
        protected void CargaTipoMonitoreo()
        {
            ddlTipoMonitoreo.DataSource = _objBLEjecucion.F_spMON_ListarTipoMonitoreo();
            ddlTipoMonitoreo.DataTextField = "nombre";
            ddlTipoMonitoreo.DataValueField = "valor";
            ddlTipoMonitoreo.DataBind();

            ddlTipoMonitoreo.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoDocumentos()
        {
            ddlTipoDocumento.DataSource = _objBLEjecucion.F_spMON_ListarTipoDocumentoEvaluacionRecomendacion();
            ddlTipoDocumento.DataTextField = "nombre";
            ddlTipoDocumento.DataValueField = "valor";
            ddlTipoDocumento.DataBind();

            ddlTipoDocumento.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected DataTable CargaEmpleadoObraTab2(int tipo)
        {
            DataTable dt;
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoempleado = tipo;

            dt = _objBLEjecucion.spMON_EmpleadoObra(_BEEjecucion);
            return dt;

            //  grd.DataBind();
        }

        protected void grdAvanceFisicoTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlFuncionInformeTab2.Enabled = false;
            ddlFechaProgramacionInformeTab2.Visible = false;
            txtFechaProgramacion.Visible = true;

            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_Avance.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoTab2.SelectedRow;


            txtFechaProgramacion.Text = ((Label)row.FindControl("lblMesValorizacionTab2")).Text;
            txtfechaTab2.Text = ((Label)row.FindControl("lblFecha")).Text;

            txtMontoTab2.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtValorizacionCanchaTab2.Text = Server.HtmlDecode(row.Cells[4].Text);
            txtMontoPreliquidacionTab2.Text = Server.HtmlDecode(row.Cells[10].Text);

            txtFisiReal.Text = ((Label)row.FindControl("lblFisicoReal")).Text;
            lblFisRealPreAcumuladoTab2.Text = (Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) - Convert.ToDouble(txtFisiReal.Text)).ToString();

            txtFisiProgTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            lblFisProgPreAcumuladoTab2.Text = (Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) - Convert.ToDouble(txtFisiProgTab2.Text)).ToString();


            txtFinanRealTab2.Text = ((Label)row.FindControl("lblFinancieroReal")).Text;
            lblFinRealPreAcumuladoTab2.Text = (Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text) - Convert.ToDouble(txtFinanRealTab2.Text)).ToString();

            txtFinanProgTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            lblFinProgPreAcumuladoTab2.Text = (Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text) - Convert.ToDouble(txtFinanProgTab2.Text)).ToString();

            txtObservaciontab2.Text = ((Label)row.FindControl("lblObservacion")).Text;
            string _strFechaObservacion = ((Label)row.FindControl("lblFechaObservacion")).Text.Substring(0, 10);
            txtFechaObservacionPreliquidacionTab2.Text = _Metodo.ValidaFecha(_strFechaObservacion)?_strFechaObservacion:"";
            txtFechaPresentacionPreliquidacionTab2.Text = ((Label)row.FindControl("lblFechaPreliquidacion")).Text.Substring(0, 10);
            txtFechaAprobadoPreliquidacionTab2.Text = ((Label)row.FindControl("lblFechaAprobado")).Text.Substring(0, 10);
            ddlEstadoSituacionalTab2.Text = ((Label)row.FindControl("lblid_tipoEstadoSituacional")).Text;
            ddlEstadoPreLiquidacionTab2.Text = ((Label)row.FindControl("lblid_tipoEstadoPreLiquidacion")).Text;

            LnkbtnAvanceTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;

            lblNomUsuarioAvance.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnAvanceTab2.Text, imgbtnAvanceTab2);

            btnGuardarAvanceTab2.Visible = false;
            btnModificarAvanceTab2.Visible = true;
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;

            CargaValorFinanciadoValorizacionTab2(Convert.ToInt32(lblId_Registro_Avance.Text));

            SumarAvanceFinancieroNE();
            ObtenerAvanceFisico();

            Up_Tab2.Update();
        }

        protected void grdAvanceFisicoAdicionalTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdAvanceFisicoAdicionalTab2.SelectedDataKey.Value.ToString();
            lblId_Registro_AvanceAdicional.Text = id_avanceFisico.ToString();
            GridViewRow row = grdAvanceFisicoAdicionalTab2.SelectedRow;
            ddlValorizacionAdicionalTab2.Text = ((Label)row.FindControl("lblid_tipoValorizacion")).Text;
            txtfechaAdicionalTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtMontoAdicionalTab2.Text = Server.HtmlDecode(row.Cells[7].Text);
            txtFisiRealAdicional.Text = ((Label)row.FindControl("lblFisicoReal")).Text;
            txtFisiProgAdicionalTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            txtFinanRealAdicionalTab2.Text = ((Label)row.FindControl("lblFinancieroReal")).Text;
            txtFinanProgAdicionalTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            txtObservacionAdicionaltab2.Text = ((Label)row.FindControl("lblObservacion")).Text;
            ddlEstadoSituacionalAdicionalTab2.Text = ((Label)row.FindControl("lblid_tipoEstadoSituacional")).Text;
            txtNroAdicionalAdicionalTab2.Text = ((Label)row.FindControl("lblAdicional")).Text;
            LnkbtnAvanceAdicionalTab2.Text = ((ImageButton)row.FindControl("imgDocAvanceTab2")).ToolTip;

            lblNomUsuarioAvanceAdicional.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnAvanceAdicionalTab2.Text, imgbtnAvanceAdicionalTab2);

            btnGuardarAvanceAdicionalTab2.Visible = false;
            btnModificarAvanceAdicionalTab2.Visible = true;
            Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoAdicionalTab2.Visible = false;

            Up_Tab2.Update();

        }

        protected void CargaAvanceFisico()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListValorizacion = new List<BE_MON_Ejecucion>();
            ListValorizacion = _objBLEjecucion.F_spMON_InformeAvanceNE(_BEEjecucion);
            grdAvanceFisicoTab2.DataSource = ListValorizacion;
            grdAvanceFisicoTab2.DataBind();

            //MONTOS TOTALES
            txtMontoTotalTab2.Text = (ListValorizacion.Sum(x => Convert.ToDouble(x.monto))).ToString("N");
            lblFisicoRealAcumuladoTab2.Text = (ListValorizacion.Sum(x => Convert.ToDouble(x.fisicoReal))).ToString("N");
            lblFisicoProgAcumuladoTab2.Text = (ListValorizacion.Sum(x => Convert.ToDouble(x.fisicoProgramado))).ToString("N");
            lblFinancieroRealAcumuladoTab2.Text = (ListValorizacion.Sum(x => Convert.ToDouble(x.financieroReal))).ToString("N");
            lblFinancieroProgAcumuladoTab2.Text = (ListValorizacion.Sum(x => Convert.ToDouble(x.financieroProgramado))).ToString("N");

            //DataTable dtAdicional = new DataTable();
            //dtAdicional = _objBLEjecucion.spMON_AvanceFisicoAdicional(_BEEjecucion);
            //if (dtAdicional.Rows.Count > 0)
            //{
            //    grdAvanceFisicoAdicionalTab2.DataSource = dtAdicional;
            //    grdAvanceFisicoAdicionalTab2.DataBind();

            //    trTab2Adicional1.Visible = true;
            //    trTab2Adicional2.Visible = true;
            //    trTab2Adicional3.Visible = true;
            //    trTab2Adicional4.Visible = true;
            //}
        }

        protected void imgDocAvanceTab2_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("imgDocAvanceTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnAvanceFisicoTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = true;
            imgbtnAvanceFisicoTab2.Visible = false;
            lblNomUsuarioAvance.Text = "";
            btnGuardarAvanceTab2.Visible = true;
            btnModificarAvanceTab2.Visible = false;

            //ddlFuncionInformeTab2.Enabled = true;
            ddlFechaProgramacionInformeTab2.Visible = true;
            txtFechaProgramacion.Visible = false;

            lblId_Registro_Avance.Text = "0";

            txtTotalFinanciado.Text = "0";
            lblCostoDirecto.Text = "0";
            txtTotalGasto.Text = "0";

            cargaProgramacionTab2();
            LimpiarAvanceFinancieroNE();
            Up_Tab2.Update();
        }

        protected void imgbtnAvanceFisicoAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceAdicionalTab2.Visible = true;
            imgbtnAvanceFisicoAdicionalTab2.Visible = false;
            lblNomUsuarioAvanceAdicional.Text = "";
            btnGuardarAvanceAdicionalTab2.Visible = true;
            btnModificarAvanceAdicionalTab2.Visible = false;
            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceTab2.Visible = false;
            imgbtnAvanceFisicoTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void btnCancelarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            Panel_AgregarAvanceAdicionalTab2.Visible = false;
            imgbtnAvanceFisicoAdicionalTab2.Visible = true;
            Up_Tab2.Update();
        }

        //protected void imgbtn_SupervisorTab2_OnClick(object sender, EventArgs e)
        //{
        //    Panel_HistorialSupervisorTab2.Visible = true;
        //    btnGuardarSupervisorTab2.Visible = true;
        //    btnModificarSupervisorTab2.Visible = false;

        //    Up_Tab2.Update();
        //}
        //protected void imgbtn_CoordinadorTab2_OnClick(object sender, EventArgs e)
        //{
        //    Panel_Coordinador.Visible = true;
        //    btnGuardar_Coordinador.Visible = true;
        //    btnModificar_Coordinador.Visible = false;
        //    Up_Tab2.Update();
        //}



        protected void CargaTipoAdjudicacionTab2()
        {
            ddlEstadoTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoEjecucion(3,3);
            ddlEstadoTab2.DataTextField = "nombre";
            ddlEstadoTab2.DataValueField = "valor";
            ddlEstadoTab2.DataBind();

            ddlEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaTipoEstadoSituacionalTab2()
        {
            ddlEstadoSituacionalTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoSituacional();
            ddlEstadoSituacionalTab2.DataTextField = "nombre";
            ddlEstadoSituacionalTab2.DataValueField = "valor";
            ddlEstadoSituacionalTab2.DataBind();

            ddlEstadoSituacionalTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlEstadoSituacionalAdicionalTab2.DataSource = _objBLEjecucion.F_spMON_TipoEstadoSituacional();
            ddlEstadoSituacionalAdicionalTab2.DataTextField = "nombre";
            ddlEstadoSituacionalAdicionalTab2.DataValueField = "valor";
            ddlEstadoSituacionalAdicionalTab2.DataBind();

            ddlEstadoSituacionalAdicionalTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void CargaEstadoPreLiquidacionTab2()
        {
            ddlEstadoPreLiquidacionTab2.Items.Insert(0, new ListItem("-Seleccione-", "-1"));
            ddlEstadoPreLiquidacionTab2.Items.Insert(1, new ListItem("Aprobada", "1"));
            ddlEstadoPreLiquidacionTab2.Items.Insert(2, new ListItem("Observada", "2"));

        }

        protected void CargaSubEstadoEjecucionTab2(string tipo)
        {
            ddlSubEstadoTab2.Items.Clear();
            if (tipo.Equals(""))
            {
                ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
            }
            else
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                list = _objBLEjecucion.F_spMON_TipoSubEstadoEjecucion(Convert.ToInt32(tipo));

                ddlSubEstadoTab2.DataSource = list;
                ddlSubEstadoTab2.DataTextField = "nombre";
                ddlSubEstadoTab2.DataValueField = "valor";
                ddlSubEstadoTab2.DataBind();

                ddlSubEstadoTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));

                if (list.Count == 0)
                {
                    spSubestado.Visible = false;
                    ddlSubEstadoTab2.Visible = false;
                    
                    //ddlSubEstadoTab2.SelectedValue = "";
                    //spSubestado.Visible = false;
                    //ddlSubEstadoTab2.Visible = false;
                }
                else
                {
                    spSubestado.Visible = true;
                    ddlSubEstadoTab2.Visible = true;
                }
            }

        }

        protected DateTime VerificaFecha(string fecha)
        {
            DateTime valor;
            valor = Convert.ToDateTime("9/9/9999");

            if (fecha != "")
            {
                valor = Convert.ToDateTime(fecha);
            }

            return valor;

        }

        protected Boolean ValidarEjecucionTab2()
        {
            //if (ddlEstadoTab2.SelectedValue == "")
            //{
            //    string script = "<script>alert('Ingrese Estado de Ejecución.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (ddlSubEstadoTab2.SelectedValue == "")
            //{
            //    string script = "<script>alert('Ingrese Sub - Estado de Ejecución.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            string FecInicioProyecto = txtFechaInicioProyectoTab2.Text,
                   FecTerminoEstimadoProyecto = txtFechaTerminoProbableTab2.Text,
                   FecAprobacionInformeInicial = txtFechaAprobacionInformeTab2.Text,
                   FecAutorizacionRetiroFondo = txtFechaAutorizacionRetiroTab2.Text,
                   FecAprobacionPMA = txtFechaAprobacionPMATab2.Text,
                   FecEntregaTerreno = txtFechaEntregaTerrenoTab2.Text;

            //Campos opcionales
            if (FecInicioProyecto.Length > 0)
            {
                if (_Metodo.ValidaFecha(FecInicioProyecto) == false)
                    return ImprimirMensaje("Formato no válido de fecha de inicio de proyecto.");
            }

            if (FecTerminoEstimadoProyecto.Length > 0)
            {
                if (_Metodo.ValidaFecha(FecTerminoEstimadoProyecto) == false)
                    return ImprimirMensaje("Formato no válido de fecha estimada de termino de Proyecto.");
            }

            if (FecAprobacionInformeInicial.Length > 0)
            {
                if (_Metodo.ValidaFecha(FecAprobacionInformeInicial) == false)
                    return ImprimirMensaje("Formato no válido de fecha de aprobación del informe inicial.");
            }

            if (FecAutorizacionRetiroFondo.Length > 0)
            {
                if (_Metodo.ValidaFecha(FecAutorizacionRetiroFondo) == false)
                    return ImprimirMensaje("Formato no válido de fecha de autorización de primer retiro de fondos.");
            }

            if (FecAprobacionPMA.Length > 0)
            {
                if (_Metodo.ValidaFecha(FecAprobacionPMA) == false)
                    return ImprimirMensaje("Formato no válido de fecha de aprobación del PMA.");
                else
                {
                    if (FecInicioProyecto == "")
                        return ImprimirMensaje("La fecha de inicio de proyecto es obligatorio para validar la fecha de aprobación del PMA.");
                    else if (_Metodo.ValidaFecha(FecInicioProyecto) == false)
                        return ImprimirMensaje("Formato no válido de fecha de inicio de proyecto.");
                    else if (Convert.ToDateTime(FecAprobacionPMA) < Convert.ToDateTime(FecInicioProyecto))
                        return ImprimirMensaje("La fecha de aprobación del PMA debe ser mayor que la fecha de inicio de proyecto.");

                    if (FecEntregaTerreno.Length > 0)
                        if (Convert.ToDateTime(FecAprobacionPMA) > Convert.ToDateTime(FecEntregaTerreno))
                            return ImprimirMensaje("La fecha de aprobación del PMA debe ser menor que la fecha de inicio de obra.");
                }
            }

            //Campos Obligatorios

            if (txtPlazoEjecucionTab2.Text == "")
                return ImprimirMensaje("Ingrese plazo de ejecución de obra.");
            else if (txtPlazoProyectoTab2.Text == "")
                return ImprimirMensaje("Ingrese plazo de ejecución del proyecto.");
            else
                return true;
        }

        protected void CargaDatosTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                //txtCordinadorTab2.Text = dt.Rows[0]["coordinadorMVCS"].ToString();
                lblNombreEstadoGeneral.Text = dt.Rows[0]["vEstadoEjecucion"].ToString().ToUpper();
                lblIdEstado.Text = dt.Rows[0]["id_tipoEstadoEjecucion"].ToString();
                lblIdSubEstado.Text = dt.Rows[0]["id_tipoSubEstadoEjecucion"].ToString();
                lblIdSubEstado2.Text = dt.Rows[0]["id_TipoEstadoParalizado"].ToString();
                //Up_EstadoSituacional.Update();

                txtFechaAprobacionInformeTab2.Text = dt.Rows[0]["fechaAprobacionInformeInicial"].ToString();
                txtFechaAutorizacionRetiroTab2.Text = dt.Rows[0]["fechaRetiroFondos"].ToString();
                txtFechaAprobacionPMATab2.Text = dt.Rows[0]["fechaAprobacionPMA"].ToString();
                if (ddlModalidadTab1.SelectedValue == "4")
                {
                    trFechaAutorizacionTab2.Visible = true;
                }
                txtFechaProbableInicioObraTab2.Text = dt.Rows[0]["fechaProbableInicioObra"].ToString();


                txtFechaIniReal.Text = dt.Rows[0]["fechaInicio"].ToString();


                txtPlazoEjecucionTab2.Text = dt.Rows[0]["plazoEjecucion"].ToString();
                txtPlazoProyectoTab2.Text = dt.Rows[0]["plazoEjecucionProyecto"].ToString();
                txtFechaInicioContractualTab2.Text = dt.Rows[0]["fechaInicioContractual"].ToString();
                txtFechaInicioReal.Text = dt.Rows[0]["fechaInicioReal"].ToString();
                txtFechaFinContractual.Text = dt.Rows[0]["fechaFinContractual"].ToString();
                txtFechaTerminoReal.Text = dt.Rows[0]["fechaFinReal"].ToString();
                txtFechaTerminoProbableTab2.Text = dt.Rows[0]["fechaFinProbable"].ToString();

                txtFechaInicioProyectoTab2.Text = dt.Rows[0]["fechaInicioProyecto"].ToString();

                txtFechaEntregaTerrenoTab2.Text = dt.Rows[0]["FechaEntregaTerreno"].ToString();

                lblFlagProgramaciónTab2.Text = dt.Rows[0]["flagProgramacion"].ToString();
                if (lblFlagProgramaciónTab2.Text == "1")
                {
                    txtFechaEntregaTerrenoTab2.Enabled = false;
                    txtPlazoEjecucionTab2.Enabled = false;
                    btnGenerarProgramacionTab2.Enabled = false;
                }

                lnkbtnActaEntregaTerrenoTab2.Text = dt.Rows[0]["urlEntregaTerreno"].ToString();
                GeneraIcoFile(lnkbtnActaEntregaTerrenoTab2.Text, imgbtnActaEntregaTerrenoTab2);

                lnkbtnInformeInicialTab2.Text = dt.Rows[0]["urlInformeInicial"].ToString();
                GeneraIcoFile(lnkbtnInformeInicialTab2.Text, imgbtnInformeInicialTab2);


                txtFechaRecepcionTab2.Text = dt.Rows[0]["FechaRecepcion"].ToString();
                lnkbtnFichaRecepcionTab2.Text = dt.Rows[0]["urlFichaRecepcion"].ToString();
                GeneraIcoFile(lnkbtnFichaRecepcionTab2.Text, imgbtnFichaRecepcionTab2);


                lblNombActuaTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();
                if (dt.Rows[0]["usuarioUpdateEstado"].ToString().Length > 0)
                {
                    lblFechaEstadoTab2.Text = "Actualizó: " + dt.Rows[0]["usuarioUpdateEstado"].ToString() + " - " + dt.Rows[0]["fechaUpdateEstado"].ToString();
                }


                txtFechaCulminacionProyectoTab2.Text = dt.Rows[0]["fechaCulminacionProyecto"].ToString();
                txtFechaRendicionFinalTab2.Text = dt.Rows[0]["fechaRendicionFinal"].ToString();
                txtTerminoObraTab2.Text = dt.Rows[0]["fechaFin"].ToString();

                lnkbtnActaTerminoTab2.Text = dt.Rows[0]["urlActatermino"].ToString();
                GeneraIcoFile(lnkbtnActaTerminoTab2.Text, imgbtnActaTerminoTab2);

                lblActualizaCulminacionProyectotab2.Text = "Actualizó: " + dt.Rows[0]["UsrUpdateCulminacion"].ToString() + " - " + dt.Rows[0]["fechaUpdateCulminacion"].ToString();

                ActualizaFechasTab1();

            }
            else
            {
                lblNombreEstadoGeneral.Text = "Actos Previos - Por Convoca (Falta registrar estado)";
                lblIdEstado.Text = "41"; //Actos Previos
                lblIdSubEstado.Text = "70"; //Por Convocar
                lblIdSubEstado2.Text = "";
            }


        }

        protected void ActualizaFechasTab1()
        {
            if (txtFechaEntregaTerrenoTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);

                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);

                int diasParalizados;
                diasParalizados = Convert.ToInt32(txtTotalParalizacionTab4.Text);
                fecha = fecha.AddDays(diasParalizados);
                txtFechaTerminoRealFinal.Text = (fecha.ToString()).Substring(0, 10);
            }

            if (txtFechaInicioProyectoTab2.Text != "" && txtPlazoProyectoTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoProyectoTab2.Text);
                fecha = Convert.ToDateTime(txtFechaInicioProyectoTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtTerminoProyectoTab2.Text = (fecha.ToString()).Substring(0, 10);
                //int diasAmpliados;
                //diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                //fecha = fecha.AddDays(diasAmpliados);
                //txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }


        }
        protected void FechaTermino_OnTextChanged(object sender, EventArgs e)
        {
            if (txtFechaEntregaTerrenoTab2.Text != "" && txtPlazoEjecucionTab2.Text != "")
            {
                DateTime fecha;
                int dias;
                dias = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                fecha = Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text);
                fecha = fecha.AddDays(dias - 1);
                txtFechaFinContractual.Text = (fecha.ToString()).Substring(0, 10);
                int diasAmpliados;
                diasAmpliados = Convert.ToInt32(lblTotalDias.Text);
                fecha = fecha.AddDays(diasAmpliados);
                txtFechaTerminoReal.Text = (fecha.ToString()).Substring(0, 10);
            }

            if (txtFechaEntregaTerrenoTab2.Text == "")
            {
                txtFechaFinContractual.Text = "";
                txtFechaTerminoReal.Text = "";

            }
        }
        protected void btnGuardarInfoTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarEjecucionTab2())
            {
                //if (validaArchivo(FileUploadFichaRecepcionTab2))
                //{
                    if (validaArchivo(FileUploadInformeInicialTab2))
                    {
                        if (validaArchivo(FileUploadActaEntregaTerrenoTab2))
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.tipoFinanciamiento = 3;

                            //_BEEjecucion.coordinadorMVCS = txtCordinadorTab2.Text;
                            //_BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                            //_BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;

                            _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaAprobacionInformeTab2.Text);

                            _BEEjecucion.Date_fechaEntregaTerreno = VerificaFecha(txtFechaEntregaTerrenoTab2.Text);
                            _BEEjecucion.UrlActaTerreno = FileUploadActaEntregaTerrenoTab2.HasFile ? _Metodo.uploadfile(FileUploadActaEntregaTerrenoTab2) : lnkbtnActaEntregaTerrenoTab2.Text;
                            _BEEjecucion.UrlInformeInicial = FileUploadInformeInicialTab2.HasFile ? _Metodo.uploadfile(FileUploadInformeInicialTab2) : lnkbtnInformeInicialTab2.Text;

                            _BEEjecucion.Date_fechaRetiro = VerificaFecha(txtFechaAutorizacionRetiroTab2.Text);
                            _BEEjecucion.Date_fechaAprobacionPMA = VerificaFecha(txtFechaAprobacionPMATab2.Text);


                            _BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaIniReal.Text);



                            _BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoEjecucionTab2.Text);
                            _BEEjecucion.plazoProyecto = Convert.ToInt32(txtPlazoProyectoTab2.Text);
                            _BEEjecucion.Date_fechaProbableInicio = VerificaFecha(txtFechaProbableInicioObraTab2.Text);
                            _BEEjecucion.Date_fechaInicioContractual = VerificaFecha(txtFechaInicioContractualTab2.Text);
                            _BEEjecucion.Date_fechaInicioReal = VerificaFecha(txtFechaInicioReal.Text);
                            _BEEjecucion.Date_fechaFinContractual = VerificaFecha(txtFechaFinContractual.Text);
                            _BEEjecucion.Date_fechaFinReal = VerificaFecha(txtFechaTerminoRealFinal.Text);
                            //_BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaTerminoTab2.Text);
                            _BEEjecucion.Date_fechaFinProbable = VerificaFecha(txtFechaTerminoProbableTab2.Text);

                            //_BEEjecucion.Date_fechaRecepcion = VerificaFecha(txtFechaRecepcionTab2.Text);
                            //_BEEjecucion.urlFichaRecepcion = FileUploadFichaRecepcionTab2.HasFile ? _Metodo.uploadfile(FileUploadFichaRecepcionTab2) : lnkbtnFichaRecepcionTab2.Text;

                            _BEEjecucion.Date_fechaInicioProyecto = VerificaFecha(txtFechaInicioProyectoTab2.Text);
                            _BEEjecucion.Date_fechaCulminacionProyecto = VerificaFecha(txtFechaCulminacionProyectoTab2.Text);

                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLEjecucion.I_spi_MON_EstadoEjecucionNE(_BEEjecucion);

                            if (val == 1)
                            {
                                string script = "<script>alert('Se registró correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                CargaDatosTab2();

                                Up_Tab2.Update();

                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }
                        }
                    }
                //}
            }

        }

        protected void grdAvanceFisicoTab2_onRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);

                //Label lblFisReal = (Label)e.Row.FindControl("lblFisicoReal");
                //Label lblFisProg = (Label)e.Row.FindControl("lblFisicoProgramado");
                //Label lblFinanReal = (Label)e.Row.FindControl("lblFinancieroReal");
                //Label lblFinanProg = (Label)e.Row.FindControl("lblFinancieroProgramado");
                //Label lblMonto = (Label)e.Row.FindControl("lblMontoValorizacion");

                //if (lblMonto.Text == "")
                //{
                //    lblMonto.Text = "0.00";
                //}

                //if (lblFisReal.Text == "")
                //{
                //    lblFisReal.Text = "0.00";
                //}

                //if (lblFisProg.Text == "")
                //{
                //    lblFisProg.Text = "0.00";
                //}

                //if (lblFinanReal.Text == "")
                //{
                //    lblFinanReal.Text = "0.00";
                //}

                //if (lblFinanProg.Text == "")
                //{
                //    lblFinanProg.Text = "0.00";
                //}


                //txtFisicoRealTab2.Text = (Convert.ToDouble(txtFisicoRealTab2.Text) + Convert.ToDouble(lblFisReal.Text)).ToString("N2");
                //txtFisicoProgTab2.Text = (Convert.ToDouble(txtFisicoProgTab2.Text) + Convert.ToDouble(lblFisProg.Text)).ToString("N2");
                //txtFinancieroRealTab2.Text = (Convert.ToDouble(txtFinancieroRealTab2.Text) + Convert.ToDouble(lblFinanReal.Text)).ToString("N2");
                //txtFinancieroProgTab2.Text = (Convert.ToDouble(txtFinancieroProgTab2.Text) + Convert.ToDouble(lblFinanProg.Text)).ToString("N2");

                //txtMontoTotalTab2.Text = (Convert.ToDouble(txtMontoTotalTab2.Text) + Convert.ToDouble(lblMonto.Text)).ToString("N2");

            }
        }

        protected void grdAvanceFisicoAdicionalTab2_onRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAvanceTab2");
                GeneraIcoFile(imb.ToolTip, imb);

                //Label lblFisReal = (Label)e.Row.FindControl("lblFisicoReal");
                //Label lblFisProg = (Label)e.Row.FindControl("lblFisicoProgramado");
                //Label lblFinanReal = (Label)e.Row.FindControl("lblFinancieroReal");
                //Label lblFinanProg = (Label)e.Row.FindControl("lblFinancieroProgramado");
                //Label lblMonto = (Label)e.Row.FindControl("lblMontoValorizacion");

                //if (lblMonto.Text == "")
                //{
                //    lblMonto.Text = "0.00";
                //}

                //if (lblFisReal.Text == "")
                //{
                //    lblFisReal.Text = "0.00";
                //}

                //if (lblFisProg.Text == "")
                //{
                //    lblFisProg.Text = "0.00";
                //}

                //if (lblFinanReal.Text == "")
                //{
                //    lblFinanReal.Text = "0.00";
                //}

                //if (lblFinanProg.Text == "")
                //{
                //    lblFinanProg.Text = "0.00";
                //}

                //txtFisicoRealAdicionalTab2.Text = (Convert.ToDouble(txtFisicoRealAdicionalTab2.Text) + Convert.ToDouble(lblFisReal.Text)).ToString("N2");
                //txtFisicoProgAdicionalTab2.Text = (Convert.ToDouble(txtFisicoProgAdicionalTab2.Text) + Convert.ToDouble(lblFisProg.Text)).ToString("N2");
                //txtFinancieroRealAdicionalTab2.Text = (Convert.ToDouble(txtFinancieroRealAdicionalTab2.Text) + Convert.ToDouble(lblFinanReal.Text)).ToString("N2");
                //txtFinancieroProgAdicionalTab2.Text = (Convert.ToDouble(txtFinancieroProgAdicionalTab2.Text) + Convert.ToDouble(lblFinanProg.Text)).ToString("N2");

                //txtMontoTotalAdicionalTab2.Text = (Convert.ToDouble(txtMontoTotalAdicionalTab2.Text) + Convert.ToDouble(lblMonto.Text)).ToString("N2");
            }
        }

        protected Boolean ValidarAvanceTab2()
        {

            string fecObservacion = txtFechaObservacionPreliquidacionTab2.Text;
            string estadoPreLiquidacion = ddlEstadoPreLiquidacionTab2.SelectedValue;

            if (estadoPreLiquidacion == "2")
            {
                if (fecObservacion == "")
                    return ImprimirMensaje("Ingrese la fecha de la observación.");
                else if (_Metodo.ValidaFecha(fecObservacion) == false)
                    return ImprimirMensaje("Formato no valido de fecha de observación.");

            }

            if (txtfechaTab2.Text == "")
                return ImprimirMensaje("Ingrese fecha de Valorizacion.");

            if (txtMontoTab2.Text == "")
                return ImprimirMensaje("Ingrese monto (S/.).");

            if (ValidaDecimal(txtMontoTab2.Text) == false)
                return ImprimirMensaje("Ingrese monto(S/.) valido, por ejemplo: 10000.02");

            if (txtValorizacionCanchaTab2.Text == "")
                return ImprimirMensaje("Ingrese monto valorizado en cancha (S/.).");

            if (ValidaDecimal(txtValorizacionCanchaTab2.Text) == false)
                return ImprimirMensaje("Ingrese monto valorizado en cancha valido, por ejemplo: 10000.02");

            if (Convert.ToDouble(txtTotalFinanciado.Text) == 0)
                return ImprimirMensaje("Ingrese valores financiados - Reg. Gastos.");

            if (txtFisiReal.Text == "")
                return ImprimirMensaje("Ingrese avance físico real.");
            else if (Convert.ToDouble(txtFisiReal.Text) > 100)
                return ImprimirMensaje("El avance físico real no puede ser mayor a 100%.");

      
            if (txtFinanRealTab2.Text == "")
                return ImprimirMensaje("Ingrese avance financiero real.");
            else if (Convert.ToDouble(txtFinanRealTab2.Text) > 100)
                return ImprimirMensaje("El avance financiero real no puede ser mayor a 100%.");

          
            if (txtFechaPresentacionPreliquidacionTab2.Text == "")
                return ImprimirMensaje("Ingrese fecha de presentación de Pre Liquidación.");

            if (txtMontoPreliquidacionTab2.Text == "")
                return ImprimirMensaje("Ingrese monto aprobado de pre liquidación mensual (S/.).");

            if (txtFechaAprobadoPreliquidacionTab2.Text == "")
                return ImprimirMensaje("Ingrese fecha de aprobación de pre liquidación mensual (S/.).");

            if (ddlEstadoSituacionalTab2.SelectedValue == "")
                return ImprimirMensaje("Ingrese estado situacional.");

            if (btnGuardarAvanceTab2.Visible == true)
            {
                if (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance físico real acumulado es:" + (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisicoRealAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");

                if (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance físico programado acumulado es:" + (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisicoProgAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");

                if (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance financiero real acumulado es:" + (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinancieroRealAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%." );

                if (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance financiero programado acumulado es:" + (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinancieroProgAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");
            
            }

            if (btnModificarAvanceTab2.Visible == true)
            {
                if (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisRealPreAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance físico real acumulado es: " + (Convert.ToDouble(txtFisiReal.Text) + Convert.ToDouble(lblFisRealPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");


                if (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisProgPreAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance físico programado acumulado es:" + (Convert.ToDouble(txtFisiProgTab2.Text) + Convert.ToDouble(lblFisProgPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");


                if (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinRealPreAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance financiero real acumulado es:" + (Convert.ToDouble(txtFinanRealTab2.Text) + Convert.ToDouble(lblFinRealPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");

                if (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinProgPreAcumuladoTab2.Text) > 101)
                    return ImprimirMensaje("El avance financiero programado acumulado es:" + (Convert.ToDouble(txtFinanProgTab2.Text) + Convert.ToDouble(lblFinProgPreAcumuladoTab2.Text)).ToString("N2") + "% y debe ser menor a 100%.");

            }
            return true;
        }

        protected Boolean ValidarAvanceAdicionalTab2()
        {
            Boolean result;
            result = true;



            if (ddlValorizacionAdicionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccione Tipo Valorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (Convert.ToInt32(ddlValorizacionAdicionalTab2.SelectedValue) > 1)
            {
                if (txtNroAdicionalAdicionalTab2.Text == "" || txtNroAdicionalAdicionalTab2.Text == "0")
                {
                    string script = "<script>alert('Ingrese N° Adicional de Valorización.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtfechaAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ValidaDecimal(txtMontoAdicionalTab2.Text) == false)
            {
                string script = "<script>alert('Ingrese monto(S/.) valido, por ejemplo: 10000.02 ');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiRealAdicional.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFisiProgAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance físico programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFinanRealAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (txtFinanProgAdicionalTab2.Text == "")
            {
                string script = "<script>alert('Ingrese avance financiero programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            if (ddlEstadoSituacionalAdicionalTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese estado situacional.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            return result;
        }
        protected void ddlEstadoTab2_OnSelecdIndexChanged(object sender, EventArgs e)
        {
            CargaSubEstadoEjecucionTab2(ddlEstadoTab2.SelectedValue);

            if (ddlEstadoTab2.SelectedValue == "5") //PARALIZADO
            {
                ddlSubEstadoTab2.SelectedValue = "82";
                spSubestado.Visible = true;
                ddlSubEstadoTab2.Visible = true;

                //ddlSubEstado2Tab2.SelectedValue = "";
                spSubestado2.Visible = true;
                ddlSubEstado2Tab2.Visible = true;

                CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);

                lblNombreProblematicaTab2.Visible = true;
                //ddlTipoProblematicaTab2.Visible = true;
                chbProblematicaTab2.Visible = true;
                Up_NombreProblematicaTab2.Update();
                Up_TipoProblematicaTab2.Update();

                CargaTipoSubEstadoParalizado(ddlSubEstado2Tab2.SelectedValue);
                //Up_SubEstado2DetalleTab2.Update();
            }
            else
            {
                lblNombreProblematicaTab2.Visible = false;
                //ddlTipoProblematicaTab2.Visible = false;
                chbProblematicaTab2.Visible = false;
                Up_NombreProblematicaTab2.Update();
                Up_TipoProblematicaTab2.Update();

                //ddlSubEstadoTab2.SelectedValue = "";
                //spSubestado.Visible = false;
                //ddlSubEstadoTab2.Visible = false;

                if (ddlSubEstadoTab2.Visible==false) 
                {
                    ddlSubEstado2Tab2.SelectedValue = "";
                    spSubestado2.Visible = false;
                    ddlSubEstado2Tab2.Visible = false;
                }
                else
                {
                    ddlSubEstado2Tab2.SelectedValue = "";
                    spSubestado2.Visible = false;
                    ddlSubEstado2Tab2.Visible = false;

                }

            }

            if (ddlSubEstadoTab2.SelectedValue.Equals("87")) // SUSPENSION DE PLAZO
            {
                trSuspensionTab2.Visible = true;
                trReinicioTab2.Visible = true;
            }
            else
            {
                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
            }
            Up_FechasSuspensionTab2.Update();

            Up_EstadoSituacional.Update();

        }

        protected void btnGuardarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {

                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_Avance.Text);
                    //_BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionInformeTab2.SelectedValue);
                    //_BEEjecucion.tipoFinanciamiento = 3;
                    //_BEEjecucion.dtFechaProgramada = VerificaFecha(ddlFechaProgramacionInformeTab2.Text);
                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaTab2.Text);
                    _BEEjecucion.Date_fechaAprobacion = Convert.ToDateTime(txtFechaAprobadoPreliquidacionTab2.Text);
                    _BEEjecucion.Date_fechaPreLiquidacion = Convert.ToDateTime(txtFechaPresentacionPreliquidacionTab2.Text);
                    _BEEjecucion.monto = txtMontoTab2.Text;
                    _BEEjecucion.montoPreLiquidacion = txtMontoPreliquidacionTab2.Text;
                    _BEEjecucion.montoEjecutado = txtValorizacionCanchaTab2.Text;
                    _BEEjecucion.fisicoReal = txtFisiReal.Text;
                    //_BEEjecucion.diasReal = Convert.ToInt32(txtDiasRealesInformeTab2.Text);
                    _BEEjecucion.financieroReal = txtFinanRealTab2.Text;
                    //_BEEjecucion.financieroProgramado = txtFinanProgTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalTab2.SelectedValue);
                    _BEEjecucion.estadoPreLiquidacion = (ddlEstadoPreLiquidacionTab2.SelectedValue);
                    _BEEjecucion.Date_fechaObsPreLiquidacion = VerificaFecha(txtFechaObservacionPreliquidacionTab2.Text);
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);

                    _BEEjecucion.observacion = txtObservaciontab2.Text;
                    _BEEjecucion.id_tipoValorizacion = "1"; // ddlValorizacionTab2.SelectedValue; Por defecto normal
                                                            //_BEEjecucion.NroAdicional = txtNroAdicionalTab2.Text;

                    //_BEEjecucion.materialReal = txtMaterialRealTab2.Text;
                    //_BEEjecucion.materialProgramado = txtMaterialProgTab2.Text;

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_InformeAvanceNE(_BEEjecucion);

                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    for (int i = 1; i <= 15; i++)
                    {
                        TextBox txtFinanciado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtFinanciado" + i.ToString());
                        TextBox txtGasto = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtGasto" + i.ToString());

                        _BEEjecucion.id_item = i;
                        _BEEjecucion.montoTotal = txtFinanciado.Text;
                        _BEEjecucion.monto = txtGasto.Text;
                        if (txtFinanciado.Enabled == true)
                        {
                            int valTotal = _objBLEjecucion.I_FinancieroTotalNE(_BEEjecucion);
                        }

                        int valMensual = _objBLEjecucion.I_FinancieroMensualNE(_BEEjecucion);
                    }


                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoTab2.Text = "";
                        txtfechaTab2.Text = "";
                        txtFisiReal.Text = "";
                        txtFisiProgTab2.Text = "";
                        txtFinanRealTab2.Text = "";
                        ddlEstadoSituacionalTab2.SelectedValue = "";
                        ddlEstadoPreLiquidacionTab2.SelectedValue = "-1";
                        txtObservaciontab2.Text = "";
                        txtFechaObservacionPreliquidacionTab2.Text = "";
                        txtFechaObservacionPreliquidacionTab2.Enabled = false;
                        txtNroAdicionalTab2.Text = "";

                        cargaProgramacionTab2();

                        Panel_AgregarAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;

                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        protected void btnGuardarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceAdicionalTab2())
            {
                if (validaArchivo(FileUploadAvanceAdicionalTab2))
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;

                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaAdicionalTab2.Text);
                    _BEEjecucion.monto = txtMontoAdicionalTab2.Text;
                    _BEEjecucion.fisicoReal = txtFisiRealAdicional.Text;
                    _BEEjecucion.fisicoProgramado = txtFisiProgAdicionalTab2.Text;
                    _BEEjecucion.financieroReal = txtFinanRealAdicionalTab2.Text;
                    _BEEjecucion.financieroProgramado = txtFinanProgAdicionalTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalAdicionalTab2.SelectedValue);
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEEjecucion.observacion = txtObservacionAdicionaltab2.Text;
                    _BEEjecucion.id_tipoValorizacion = ddlValorizacionAdicionalTab2.SelectedValue;
                    _BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;

                    int val = _objBLEjecucion.spi_MON_AvanceFisico(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoAdicionalTab2.Text = "";
                        txtfechaAdicionalTab2.Text = "";
                        txtFisiRealAdicional.Text = "";
                        txtFisiProgAdicionalTab2.Text = "";
                        txtFinanRealAdicionalTab2.Text = "";
                        txtFinanProgAdicionalTab2.Text = "";
                        ddlEstadoSituacionalAdicionalTab2.SelectedValue = "";
                        txtObservacionAdicionaltab2.Text = "";
                        ddlValorizacionAdicionalTab2.SelectedValue = "";
                        txtNroAdicionalAdicionalTab2.Text = "";

                        Panel_AgregarAvanceAdicionalTab2.Visible = false;
                        imgbtnAvanceFisicoAdicionalTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }

        }

        protected void ddlValorizacionTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlValorizacionTab2.SelectedValue == "1" || ddlValorizacionTab2.SelectedValue == "")
            {
                lblAdicional.Visible = false;
                txtNroAdicionalTab2.Visible = false;

            }
            else
            {
                lblAdicional.Visible = true;
                txtNroAdicionalTab2.Visible = true;
            }

            Up_lblAdicionalTab2.Update();
            Up_txtAdicionalTab2.Update();
        }

        protected void totalMontoGrdTab2(int tipo, Label txt)
        {
            _BEFinanciamiento.tipoGrd = tipo;
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            txt.Text = _objBLFinanciamiento.F_spMON_MontoTotalGrd(_BEFinanciamiento);
        }

        //protected void btnGuardarResidenteTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtResidenteObraTab2.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de residente.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {
        //        //if (txtCIPR.Text == "")
        //        //{
        //        //    string script = "<script>alert('Ingresar CIP.');</script>";
        //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        //}
        //        //else
        //        //{
        //            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //            _BEEjecucion.tipoempleado = 1;

        //            _BEEjecucion.empleadoObra = txtResidenteObraTab2.Text;
        //            _BEEjecucion.CIP = txtCIPR.Text;
        //            _BEEjecucion.telefono = txtTelefonoR.Text;
        //            _BEEjecucion.correo = txtCorreoR.Text;
        //            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //            _BEEjecucion.cap = txtCapR.Text;
        //            _BEEjecucion.Date_fecha = VerificaFecha(txtFechaR.Text);

        //            int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

        //            if (val == 1)
        //            {
        //                string script = "<script>alert('Se registró correctamente.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                txtResidenteTab2.Text = txtResidenteObraTab2.Text;
        //                txtResidenteObraTab2.Text = "";
        //                txtTelefonoR.Text = "";
        //                txtCorreoR.Text = "";
        //                txtCIPR.Text = "";
        //                txtCapR.Text = "";
        //                txtFechaR.Text = "";


        //                //RESIDENTE
        //                dt =CargaEmpleadoObraTab2(1);
        //                grd_HistorialResidenteTab2.DataSource = dt;
        //                grd_HistorialResidenteTab2.DataBind();

        //                Panel_HistorialResidenteTab2.Visible = false;
        //                Up_Tab2.Update();

        //            }
        //            else
        //            {
        //                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            }
        //        }
        //    //}





        //}
        //protected void btnGuardarCoordinador_OnClick(object sender, EventArgs e)
        //{
        //    if (txtCoordinador.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de coordinador MVCS.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEEjecucion.tipoempleado = 4;

        //        _BEEjecucion.empleadoObra = txtCoordinador.Text;

        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
        //        int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se registró coordinador MVCS correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //txtCordinadorTab2.Text = txtCoordinador.Text ;
        //            txtCoordinador.Text = "";
        //            txtFechaC.Text = "";

        //            dt = CargaEmpleadoObraTab2(4);
        //            grd_HistorialCoordinador.DataSource = dt;
        //            grd_HistorialCoordinador.DataBind();
        //            txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            Panel_Coordinador.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }
        //}
        //protected void btnGuardarSupervisorTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtSupervisorS.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //            _BEEjecucion.tipoempleado = 3;

        //            _BEEjecucion.empleadoObra = txtSupervisorS.Text;
        //            _BEEjecucion.CIP = txtCIPS.Text;
        //            _BEEjecucion.telefono = txtTelefonoS.Text;
        //            _BEEjecucion.correo = txtCorreoS.Text;
        //            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //            _BEEjecucion.cap = txtCapS.Text;
        //            _BEEjecucion.Date_fecha = VerificaFecha(txtFechaS.Text);
        //            int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

        //            if (val == 1)
        //            {
        //                string script = "<script>alert('Se registró supervisor correctamente.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                TxtSuperTab2.Text = txtSupervisorS.Text;
        //                txtSupervisorS.Text = "";
        //                txtTelefonoS.Text = "";
        //                txtCIPS.Text = "";
        //                txtCorreoS.Text = "";
        //                txtCapS.Text = "";
        //                txtFechaS.Text = "";

        //                //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);

        //                //SUPERVISOR
        //                dt = CargaEmpleadoObraTab2(3);
        //                grd_HistorialSupervisorTab2.DataSource = dt;
        //                grd_HistorialSupervisorTab2.DataBind();

        //                Panel_HistorialSupervisorTab2.Visible = false;
        //                Up_Tab2.Update();

        //            }
        //            else
        //            {
        //                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            }

        //    }





        //}

        //protected void btnGrabarInspectorTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtInspector.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de inspector.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {
        //        //if (txtCIP.Text == "")
        //        //{
        //        //    string script = "<script>alert('Ingresar CIP.');</script>";
        //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        //}
        //        //else
        //        //{
        //            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //            _BEEjecucion.tipoempleado = 2;

        //            _BEEjecucion.empleadoObra = txtInspector.Text;
        //            _BEEjecucion.telefono = txtTelefono.Text;
        //            _BEEjecucion.correo = txtCorreo.Text;
        //            _BEEjecucion.CIP = txtCIP.Text;
        //            _BEEjecucion.cap = txtCap.Text;
        //            _BEEjecucion.Date_fecha = VerificaFecha(txtFechaP.Text);

        //            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //            int val = _objBLEjecucion.spi_MON_EmpleadoObra(_BEEjecucion);

        //            if (val == 1)
        //            {
        //                string script = "<script>alert('Se registró correctamente.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //                txtInspectorTab2.Text = txtInspector.Text;
        //                txtInspector.Text = "";
        //                txtTelefono.Text = "";
        //                txtCorreo.Text = "";
        //                txtCIP.Text = "";
        //                txtCap.Text = "";
        //                txtFechaP.Text = "";

        //                //INSPECTOR
        //                dt=CargaEmpleadoObraTab2(2);
        //                grd_HistorialInspectorTab2.DataSource = dt;
        //                grd_HistorialInspectorTab2.DataBind();

        //                Panel_HistorialInspectorTab2.Visible = false;
        //                Up_Tab2.Update();

        //            }
        //            else
        //            {
        //                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            }
        //        }
        //    }

        protected void btnModificarAvanceTab2_OnClick(object sender, EventArgs e)
        {
            if (ValidarAvanceTab2())
            {
                if (validaArchivo(FileUploadAvanceTab2))
                {

                    _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_Avance.Text);
                    //_BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionInformeTab2.SelectedValue);
                    //_BEEjecucion.tipoFinanciamiento = 3;
                    //_BEEjecucion.dtFechaProgramada = VerificaFecha(ddlFechaProgramacionInformeTab2.Text);
                    _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaTab2.Text);
                    _BEEjecucion.Date_fechaAprobacion = Convert.ToDateTime(txtFechaAprobadoPreliquidacionTab2.Text);
                    _BEEjecucion.Date_fechaPreLiquidacion = Convert.ToDateTime(txtFechaPresentacionPreliquidacionTab2.Text);
                    _BEEjecucion.monto = txtMontoTab2.Text;
                    _BEEjecucion.montoPreLiquidacion = txtMontoPreliquidacionTab2.Text;
                    _BEEjecucion.montoEjecutado = txtValorizacionCanchaTab2.Text;
                    _BEEjecucion.fisicoReal = txtFisiReal.Text;
                    //_BEEjecucion.diasReal = Convert.ToInt32(txtDiasRealesInformeTab2.Text);
                    _BEEjecucion.financieroReal = txtFinanRealTab2.Text;
                    //_BEEjecucion.financieroProgramado = txtFinanProgTab2.Text;
                    _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalTab2.SelectedValue);
                    _BEEjecucion.estadoPreLiquidacion = (ddlEstadoPreLiquidacionTab2.SelectedValue);

                    _BEEjecucion.Date_fechaObsPreLiquidacion = VerificaFecha(txtFechaObservacionPreliquidacionTab2.Text);

                    if (FileUploadAvanceTab2.PostedFile.ContentLength > 0)
                    {
                        _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                    }
                    else
                    {
                        _BEEjecucion.urlDoc = LnkbtnAvanceTab2.Text;
                    }

                    //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);

                    _BEEjecucion.observacion = txtObservaciontab2.Text;
                    _BEEjecucion.id_tipoValorizacion = "1"; // ddlValorizacionTab2.SelectedValue; Por defecto normal
                                                            //_BEEjecucion.NroAdicional = txtNroAdicionalTab2.Text;

                    //_BEEjecucion.materialReal = txtMaterialRealTab2.Text;
                    //_BEEjecucion.materialProgramado = txtMaterialProgTab2.Text;

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_InformeAvanceNE(_BEEjecucion);
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    for (int i = 1; i <= 15; i++)
                    {
                        TextBox txtFinanciado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtFinanciado" + i.ToString());
                        TextBox txtGasto = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtGasto" + i.ToString());

                        _BEEjecucion.id_item = i;
                        _BEEjecucion.montoTotal = txtFinanciado.Text;
                        _BEEjecucion.monto = txtGasto.Text;
                        if (txtFinanciado.Enabled == true)
                        {
                            int valTotal = _objBLEjecucion.I_FinancieroTotalNE(_BEEjecucion);
                        }

                        int valMensual = _objBLEjecucion.I_FinancieroMensualNE(_BEEjecucion);
                    }


                    if (val == 1)
                    {
                        string script = "<script>alert('Se modificó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaAvanceFisico();

                        txtMontoTab2.Text = "";
                        txtfechaTab2.Text = "";
                        txtFisiReal.Text = "";
                        txtFisiProgTab2.Text = "";
                        txtFinanRealTab2.Text = "";
                        //txtFinanProgTab2.Text = "";
                        ddlEstadoSituacionalTab2.SelectedValue = "";
                        ddlEstadoPreLiquidacionTab2.SelectedValue = "-1";
                        txtObservaciontab2.Text = "";
                        txtFechaObservacionPreliquidacionTab2.Text = "";
                        txtFechaObservacionPreliquidacionTab2.Enabled = false;
                        //ddlValorizacionTab2.SelectedValue = "";
                        txtNroAdicionalTab2.Text = "";

                        cargaProgramacionTab2();

                        Panel_AgregarAvanceTab2.Visible = false;
                        imgbtnAvanceFisicoTab2.Visible = true;
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }

            }
        }

        protected void btnModificarAvanceAdicionalTab2_OnClick(object sender, EventArgs e)
        {

            if (ValidarAvanceAdicionalTab2())
            {


                _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblId_Registro_AvanceAdicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                _BEEjecucion.id_tipoValorizacion = ddlValorizacionAdicionalTab2.SelectedValue;
                _BEEjecucion.Date_fecha = Convert.ToDateTime(txtfechaAdicionalTab2.Text);
                _BEEjecucion.monto = txtMontoAdicionalTab2.Text;
                _BEEjecucion.fisicoReal = txtFisiRealAdicional.Text;
                _BEEjecucion.fisicoProgramado = txtFisiProgAdicionalTab2.Text;
                _BEEjecucion.financieroReal = txtFinanRealAdicionalTab2.Text;
                _BEEjecucion.financieroProgramado = txtFinanProgAdicionalTab2.Text;
                _BEEjecucion.observacion = txtObservacionAdicionaltab2.Text;
                _BEEjecucion.estadoSituacional = (ddlEstadoSituacionalAdicionalTab2.SelectedValue);

                if (FileUploadAvanceAdicionalTab2.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceAdicionalTab2);
                }
                else
                {
                    _BEEjecucion.urlDoc = LnkbtnAvanceAdicionalTab2.Text;
                }


                //_BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadAvanceTab2);
                _BEEjecucion.NroAdicional = txtNroAdicionalAdicionalTab2.Text;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLEjecucion.spud_MON_AvanceFisico_Editar(_BEEjecucion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    // ddlValorizacionTab2.SelectedValue = "";
                    txtfechaAdicionalTab2.Text = "";
                    txtMontoAdicionalTab2.Text = "";
                    txtFisiRealAdicional.Text = "";
                    txtFisiProgAdicionalTab2.Text = "";
                    txtFinanRealAdicionalTab2.Text = "";
                    txtFinanProgAdicionalTab2.Text = "";
                    txtObservacionAdicionaltab2.Text = "";
                    ddlEstadoSituacionalAdicionalTab2.SelectedValue = "";
                    txtNroAdicionalAdicionalTab2.Text = "";
                    LnkbtnAvanceAdicionalTab2.Text = "";
                    imgbtnAvanceAdicionalTab2.Visible = false;
                    imgbtnAvanceFisicoAdicionalTab2.Visible = true;

                    CargaAvanceFisico();



                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_AgregarAvanceAdicionalTab2.Visible = false;
                //CargaAvanceFisico();
                btnModificarAvanceAdicionalTab2.Visible = true;
                Up_Tab2.Update();


            }
        }

        protected void imgbtnAgregarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            //Nuevo registro
            lblID_evaluacionTab2.Text = "0";

            string flagContinuarRegistro = "1";
            string msjContinuarRegistro = "";

            TxtViviendasConcluidas.Text =  "";
            TxtViviendasEnEjecucion.Text = "";
            TxtViviendasPorIniciar.Text = "";
            TxtViviendasParalizadas.Text = "";

            //if (lblIdSubEstado.Text.Equals("87")) //SUSPENSION  VERIFICAR REGISTRO DE FECHA DE REINICIO
            //{
            //    dt = new DataTable();
            //    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            //    dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);

            //    if (dt.Rows.Count > 0)
            //    {
            //        string vIdSubEstado = (dt.Rows[dt.Rows.Count - 1]["id_tipoSubEstadoEjecucion"]).ToString();
            //        string fechaReinicio = (dt.Rows[dt.Rows.Count - 1]["fechaReinicio"]).ToString();
            //        if (vIdSubEstado.Equals("87") && fechaReinicio.Length <= 1)
            //        {
            //            flagContinuarRegistro = "0";
            //            msjContinuarRegistro = "Para continuar con el registro del Avance, debe registrar la fecha de reinicio de la Obra.";
            //        }
            //    }
            //}

            if (flagContinuarRegistro.Equals("0"))
            {
                string script = "<script>alert('" + msjContinuarRegistro + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                Panel_ObservacionTab2.Visible = true;
                imgbtnAgregarEvaluacionTab2.Visible = false;
                lblNomUsuarioObservaciones.Text = "";
                btnGuardarEvaluacionTab2.Visible = true;
                btnModificarEvaluacionTab2.Visible = false;

                chbAyudaMemoriaTab4.Checked = false;
                txtFechaVisitaTab2.Text = "";
                txtEvaluacionTab2.Text = "";
                txtRecomendacionTab2.Text = "";
                txtProgramadoTab2.Text = "";
                txtEjecutadoTab2.Text = "";
                ddlTipoMonitoreo.SelectedValue = "";
                txtNroTramie.Text = "";
                txtTipoDocumento.Text = "";
                ddlTipoDocumento.SelectedValue = "";
                txtAsunto.Text = "";
                txtProgramadoTab2.Text = "0";
                txtEjecutadoTab2.Text = "0";
                txtFinanEjecutadoTab2.Text = "0";
                txtFinanProgramadoTab2.Text = "0";

                lblNomUsuarioFlagAyudaTab4.Text = "";

                imgbtnActaTab4.ImageUrl = "~/img/blanco.png";
                imgbtnInformeTab4.ImageUrl = "~/img/blanco.png";
                imgbtnOficioTab4.ImageUrl = "~/img/blanco.png";

                LnkbtnInformeTab4.Text = "";
                LnkbtnOficioTab4.Text = "";
                LnkbtnActaTab4.Text = "";

                ddlEstadoTab2.SelectedValue = "";
                ddlSubEstadoTab2.SelectedValue = "";
                ddlSubEstado2Tab2.SelectedValue = "";

                chbAyudaMemoriaTab4.Enabled = true;
                ddlEstadoTab2.Enabled = true;
                ddlSubEstadoTab2.Enabled = true;
                ddlSubEstado2Tab2.Enabled = true;

                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
                Up_FechasSuspensionTab2.Update();

                chbProblematicaTab2.Items.Clear();
                txtFechaInicioSuspensionTab2.Text = "";
                txtFechaFinalReinicioTab2.Text = "";

                Panel_InformacionComplementaria.Visible = false;
                Up_Tab2.Update();
            }
        }

        //protected void grd_HistorialCoordinador_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grd_HistorialCoordinador.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
        //        _BEEjecucion.Tipo = 2;//PARA LA ELIMINACION
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.Date_fecha = DateTime.Now;
        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            // TxtSuperTab2.Text = txtSupervisorS.Text;

        //            txtCoordinador.Text = "";
        //            txtFechaC.Text = "";

        //            dt = CargaEmpleadoObraTab2(4);
        //            if (dt.Rows.Count > 0)
        //            {
        //                txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString() ;
        //            }
        //            else
        //            {
        //                txtCordinadorTab2.Text = "";
        //            }
        //            grd_HistorialCoordinador.DataSource = dt;
        //            grd_HistorialCoordinador.DataBind();

        //            Panel_Coordinador.Visible = false;
        //            Up_Tab2.Update();



        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}
        //protected void grd_HistorialSupervisorTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grd_HistorialSupervisorTab2.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
        //        _BEEjecucion.Tipo = 2;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.Date_fecha = DateTime.Now;
        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación Correcta.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //           // TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtSupervisorS.Text = "";
        //            txtTelefonoS.Text = "";
        //            txtCIPS.Text = "";
        //            txtCorreoS.Text = "";
        //            dt = CargaEmpleadoObraTab2(3);
        //            if (dt.Rows.Count > 0)
        //            {

        //                TxtSuperTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                TxtSuperTab2.Text = "";

        //            }


        //            Panel_HistorialSupervisorTab2.Visible = false;
        //            Up_Tab2.Update();



        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}



        //protected void grd_HistorialSupervisorTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id;
        //    id = grd_HistorialSupervisorTab2.SelectedDataKey.Value.ToString();
        //    lblIDSupervisor.Text = id;
        //    GridViewRow row = grd_HistorialSupervisorTab2.SelectedRow;

        //    txtSupervisorS.Text = ((Label)row.FindControl("lblnombreS")).Text;
        //    txtCIPS.Text = ((Label)row.FindControl("lblCIP")).Text;
        //    txtTelefonoS.Text = ((Label)row.FindControl("lblTelefonoS")).Text;
        //    txtCorreoS.Text = ((Label)row.FindControl("lblCorreoS")).Text;
        //    txtCapS.Text = ((Label)row.FindControl("lblCapS")).Text;
        //    txtFechaS.Text = ((Label)row.FindControl("lblFechaSS")).Text;

        //    btnGuardarSupervisorTab2.Visible = false;
        //    btnModificarSupervisorTab2.Visible = true;
        //    Panel_HistorialSupervisorTab2.Visible = true;
        //    Up_Tab2.Update();
        //    //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    //btnModificarContrapartidasTab0.Visible = true;
        //    //btnGuardarContrapartidasTab0.Visible = false;
        //    //Panel_ContrapartidaTab0.Visible = true;
        //    //btnAgregarContrapartidasTab0.Visible = false;
        //    //Up_Tab0.Update();


        //}

        //protected void btnModificarSupervisorTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtSupervisorS.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIDSupervisor.Text);
        //        _BEEjecucion.Tipo = 1;
        //       // _BEEjecucion.empleadoObra = txtu.Text;
        //        _BEEjecucion.empleadoObra = txtSupervisorS.Text;
        //        _BEEjecucion.CIP = txtCIPS.Text;
        //        _BEEjecucion.telefono = txtTelefonoS.Text;
        //        _BEEjecucion.correo = txtCorreoS.Text;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.cap = txtCapS.Text;
        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaS.Text);

        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se modificó el supervisor correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtSupervisorS.Text = "";
        //            txtTelefonoS.Text = "";
        //            txtCIPS.Text = "";
        //            txtCorreoS.Text = "";
        //            txtCapS.Text = "";
        //            txtFechaS.Text = "";

        //            //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
        //            dt = CargaEmpleadoObraTab2(3);
        //            if (dt.Rows.Count > 0)
        //            {

        //                TxtSuperTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                TxtSuperTab2.Text = "";

        //            }

        //            Panel_HistorialSupervisorTab2.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void btnModificarCoordinador_OnClick(object sender, EventArgs e)
        //{
        //    if (txtCoordinador.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de supervisor.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {
        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblIdCoordinadorM.Text);
        //        _BEEjecucion.Tipo = 1;//valor q indica la modificacion
        //        // _BEEjecucion.empleadoObra = txtu.Text;

        //        _BEEjecucion.empleadoObra = txtCoordinador.Text;            
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);            
        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaC.Text);
        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se modificó el coordinador correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtCoordinador.Text = "";                
        //            txtFechaC.Text = "";

        //            //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
        //            dt = CargaEmpleadoObraTab2(4);
        //            if (dt.Rows.Count > 0)
        //            {
        //                txtCordinadorTab2.Text = dt.Rows[0]["nombre"].ToString();
        //            }
        //            else
        //            {
        //                txtCordinadorTab2.Text = "";

        //            }
        //            grd_HistorialCoordinador.DataSource = dt;
        //            grd_HistorialCoordinador.DataBind();
        //            Panel_Coordinador.Visible = false;
        //            btnModificar_Coordinador.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grd_HistorialResidenteTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grd_HistorialResidenteTab2.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
        //        _BEEjecucion.Tipo = 2;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación correcta de residente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            // TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtResidenteObraTab2.Text = "";
        //            txtTelefonoR.Text = "";
        //            txtCIPR.Text = "";
        //            txtCorreoR.Text = "";
        //            dt = CargaEmpleadoObraTab2(1);
        //            if (dt.Rows.Count > 0)
        //            {

        //                txtResidenteTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                txtResidenteTab2.Text = "";

        //            }


        //            Panel_HistorialResidenteTab2.Visible = false;
        //            Up_Tab2.Update();



        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grd_HistorialResidenteTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id;
        //    id = grd_HistorialResidenteTab2.SelectedDataKey.Value.ToString();
        //    lblId_Residente.Text = id;
        //    GridViewRow row = grd_HistorialResidenteTab2.SelectedRow;

        //    txtResidenteObraTab2.Text = ((Label)row.FindControl("lblResidente")).Text;
        //    txtCIPR.Text = ((Label)row.FindControl("lblCIP")).Text;
        //    txtTelefonoR.Text = ((Label)row.FindControl("lblTelefonoR")).Text;
        //    txtCorreoR.Text = ((Label)row.FindControl("lblCorreoR")).Text;
        //    txtCapR.Text = ((Label)row.FindControl("lblcapR")).Text;
        //    txtFechaR.Text = ((Label)row.FindControl("lblFechaRR")).Text;

        //    btnGuardarResidenteTab2.Visible = false;
        //    btnModificarResidenteTab2.Visible = true;
        //    Panel_HistorialResidenteTab2.Visible = true;
        //    Up_Tab2.Update();
        //    //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    //btnModificarContrapartidasTab0.Visible = true;
        //    //btnGuardarContrapartidasTab0.Visible = false;
        //    //Panel_ContrapartidaTab0.Visible = true;
        //    //btnAgregarContrapartidasTab0.Visible = false;
        //    //Up_Tab0.Update();


        //}

        //protected void btnModificarResidenteTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtResidenteObraTab2.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de residente.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblId_Residente.Text);
        //        _BEEjecucion.Tipo = 1;
        //        // _BEEjecucion.empleadoObra = txtu.Text;
        //        _BEEjecucion.empleadoObra = txtResidenteObraTab2.Text;
        //        _BEEjecucion.CIP = txtCIPR.Text;
        //        _BEEjecucion.telefono = txtTelefonoR.Text;
        //        _BEEjecucion.correo = txtCorreoR.Text;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.cap = txtCapR.Text;
        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaR.Text);

        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);


        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se modificó el residente correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtResidenteObraTab2.Text = "";
        //            txtTelefonoR.Text = "";
        //            txtCIPR.Text = "";
        //            txtCorreoR.Text = "";
        //            txtCapR.Text = "";
        //            txtFechaR.Text = "";


        //            //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
        //            dt = CargaEmpleadoObraTab2(1);
        //            if (dt.Rows.Count > 0)
        //            {

        //                txtResidenteTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                txtResidenteTab2.Text = "";

        //            }

        //            Panel_HistorialResidenteTab2.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grd_HistorialInspectorTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "eliminar")
        //    {
        //        Control ctl = e.CommandSource as Control;
        //        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
        //        object objTemp = grd_HistorialInspectorTab2.DataKeys[CurrentRow.RowIndex].Value as object;

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(objTemp.ToString());
        //        _BEEjecucion.Tipo = 2;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Eliminación correcta de Inspector.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            // TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtInspector.Text = "";
        //            txtTelefono.Text = "";
        //            txtCIP.Text = "";
        //            txtCorreo.Text = "";
        //            dt = CargaEmpleadoObraTab2(2);
        //            if (dt.Rows.Count > 0)
        //            {

        //                txtInspectorTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                txtInspectorTab2.Text = "";

        //            }


        //            Panel_HistorialInspectorTab2.Visible = false;
        //            Up_Tab2.Update();



        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        //protected void grd_HistorialInspectorTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string id;
        //    id = grd_HistorialInspectorTab2.SelectedDataKey.Value.ToString();
        //    lblID_Inspector.Text = id;
        //    GridViewRow row = grd_HistorialInspectorTab2.SelectedRow;

        //    txtInspector.Text = ((Label)row.FindControl("lblnombre")).Text;
        //    txtCIP.Text = ((Label)row.FindControl("lblCIP")).Text;
        //    txtTelefono.Text = ((Label)row.FindControl("lblTelefono")).Text;
        //    txtCorreo.Text = ((Label)row.FindControl("lblCorreo")).Text;
        //    txtCap.Text = ((Label)row.FindControl("lblCap")).Text;
        //    txtFechaP.Text = ((Label)row.FindControl("lblFechaII")).Text;

        //    btnGrabarInspectorTab2.Visible = false;
        //    btnModificarInspectorTab2.Visible = true;
        //    Panel_HistorialInspectorTab2.Visible = true;
        //    Up_Tab2.Update();
        //    //lblNomUsuarioContrapartida.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
        //    //btnModificarContrapartidasTab0.Visible = true;
        //    //btnGuardarContrapartidasTab0.Visible = false;
        //    //Panel_ContrapartidaTab0.Visible = true;
        //    //btnAgregarContrapartidasTab0.Visible = false;
        //    //Up_Tab0.Update();


        //}

        //protected void btnModificarInspectorTab2_OnClick(object sender, EventArgs e)
        //{
        //    if (txtInspector.Text == "")
        //    {
        //        string script = "<script>alert('Ingresar nombre de Inspector.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //    }
        //    else
        //    {

        //        _BEEjecucion.Id_empleado_obra = Convert.ToInt32(lblID_Inspector.Text);
        //        _BEEjecucion.Tipo = 1;
        //        // _BEEjecucion.empleadoObra = txtu.Text;
        //        _BEEjecucion.empleadoObra = txtInspector.Text;
        //        _BEEjecucion.CIP = txtCIP.Text;
        //        _BEEjecucion.telefono = txtTelefono.Text;
        //        _BEEjecucion.correo = txtCorreo.Text;
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
        //        _BEEjecucion.cap = txtCap.Text;
        //        _BEEjecucion.Date_fecha = VerificaFecha(txtFechaP.Text);

        //        int val = _objBLEjecucion.spud_MON_EmpleadoObra(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Se modificó el Inspector correctamente.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //            //TxtSuperTab2.Text = txtSupervisorS.Text;
        //            txtInspector.Text = "";
        //            txtTelefono.Text = "";
        //            txtCIP.Text = "";
        //            txtCorreo.Text = "";
        //            txtCap.Text = "";
        //            txtFechaP.Text = "";

        //            //CargaEmpleadoObraTab2(grd_HistorialSupervisorTab2, 3);
        //            dt = CargaEmpleadoObraTab2(2);
        //            if (dt.Rows.Count > 0)
        //            {

        //                txtInspectorTab2.Text = dt.Rows[0]["nombre"].ToString();

        //            }
        //            else
        //            {
        //                txtInspectorTab2.Text = "";

        //            }

        //            Panel_HistorialInspectorTab2.Visible = false;
        //            Up_Tab2.Update();

        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }

        //}

        protected void cargarEvaluacionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            dt = _objBLEjecucion.spMON_EvaluacionRecomendacion(_BEEjecucion);

            if (dt.Rows.Count > 0)
            {
                lblAvanceEjecutadoInfomeTab2.Text = (dt.Rows[dt.Rows.Count - 1]["fisicoEjecutado"]).ToString();
                lblAvanceProgramadoInfomeTab2.Text = (dt.Rows[dt.Rows.Count - 1]["fisicoProgramado"]).ToString();
            }

            grdEvaluacionTab2.DataSource = dt;
            grdEvaluacionTab2.DataBind();

        }

        protected void btnGuardarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_OficioTab4))
            {
                if (validaArchivo(FileUpload_InformeTab4))
                {
                    if (validaArchivo(FileUpload_ActaTab4))
                    {
                        if (ValidarEvaluacionRecomendacionTab2())
                        {

                            /*inicio guardar estado*/
                            if (ValidarEstadoSituacional())
                            {
                                /*otro guardado inicio*/
                                _BEEjecucion.tipoFinanciamiento = 3;
                                _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                                _BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;
                                _BEEjecucion.id_tipoEstadoParalizado = ddlSubEstado2Tab2.SelectedValue;
                                string vDetalleProblemas = "";
                                if (ddlSubEstadoTab2.SelectedValue.Equals("82"))//Paralizados permanentes
                                {
                                    //List<ListItem> selected = new List<ListItem>();
                                    foreach (ListItem item in chbProblematicaTab2.Items)
                                    {
                                        //string myJson = "{'Username': 'myusername','Password':'pass'}";
                                        vDetalleProblemas = vDetalleProblemas + "{\"idSubEstado\":\"" + ddlSubEstadoTab2.SelectedValue + "\", \"idProblematica\":\"" + item.Value;
                                        if (item.Selected)
                                        {
                                            vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"1\"},";
                                        }
                                        else
                                        {
                                            vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"0\"},";
                                        }
                                    }

                                    vDetalleProblemas = vDetalleProblemas.Substring(0, vDetalleProblemas.Length - 1);

                                    vDetalleProblemas = "[" + vDetalleProblemas + "]";
                                }
                                else
                                {
                                    if (ddlSubEstadoTab2.SelectedValue.Equals("87"))//Suspension
                                    {
                                        _BEEjecucion.Date_fechaInicio = _Metodo.VerificaFecha(txtFechaInicioSuspensionTab2.Text);
                                        _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinalReinicioTab2.Text);
                                    }
                                }
                                _BEEjecucion.detalleProblematica = vDetalleProblemas;
                                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                                _BEEjecucion.Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text);

                                _BEEjecucion.Evaluacion = txtEvaluacionTab2.Text;
                                _BEEjecucion.Recomendacion = txtRecomendacionTab2.Text;
                                _BEEjecucion.FisicoEjec = txtEjecutadoTab2.Text;
                                _BEEjecucion.FisicoProg = txtProgramadoTab2.Text;

                                _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab4);
                                _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab4);
                                _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab4);


                                _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                                _BEEjecucion.nrotramite = txtNroTramie.Text;

                                //_BEEjecucion.tipodocumento = txtTipoDocumento.Text;
                                _BEEjecucion.id_tipodocumento = ddlTipoDocumento.SelectedValue;
                                _BEEjecucion.asunto = txtAsunto.Text;
                                _BEEjecucion.financieroReal = txtFinanEjecutadoTab2.Text;
                                _BEEjecucion.financieroProgramado = txtFinanProgramadoTab2.Text;

                                if (chbAyudaMemoriaTab4.Checked == true)
                                {
                                    _BEEjecucion.flagAyuda = "1";
                                }
                                else
                                {
                                    _BEEjecucion.flagAyuda = "0";
                                }

                                int val;

                                val = _objBLEjecucion.spi_MON_EvaluacionRecomendacion(_BEEjecucion);

                                GuardarViviendas(val);

                                lblID_evaluacionTab2.Text = val.ToString();

                                if (val > 0)
                                {
                                    string script = "<script>alert('Se registró Correctamente. Puede continuar con el registro de la información complementaria (Panel Fotográfico).');</script>";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                    cargarEvaluacionTab2();
                                    CargaDatosTab2();

                                    //txtFechaVisitaTab2.Text = "";
                                    //txtEvaluacionTab2.Text = "";
                                    //txtRecomendacionTab2.Text = "";
                                    //txtProgramadoTab2.Text = "";
                                    //txtEjecutadoTab2.Text = "";
                                    //ddlTipoMonitoreo.SelectedValue = "";
                                    //txtNroTramie.Text = "";
                                    //txtTipoDocumento.Text = "";
                                    //ddlTipoDocumento.SelectedValue = "";
                                    //txtAsunto.Text = "";
                                    //Panel_ObservacionTab2.Visible = false;
                                    //imgbtnAgregarEvaluacionTab2.Visible = true;
                                    //lblNomUsuarioFlagAyudaTab4.Text = "";

                                    btnGuardarEvaluacionTab2.Visible = false;
                                    Panel_InformacionComplementaria.Visible = true;

                                    Up_Tab2.Update();


                                }
                                else
                                {
                                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                                }

                            }


                        }
                    }
                }


            }


        }

        protected void lnkbtnRegistrarPanelFotosGrilla_Click(object sender, EventArgs e)
        {
            if (lblID_evaluacionTab2.Text.Length == 0)
            {
                string script = "<script>alert('Debe registrar primero el avance para luego continuar con el registro del panel fotográfico.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                //lblIdRecomendacionInformeMonitoreoPOPU.Text = lblID_evaluacionTab2.Text;
                //ImageButton boton = default(ImageButton);
                //GridViewRow row = default(GridViewRow);
                //boton = (ImageButton)sender;
                //row = (GridViewRow)boton.NamingContainer;
                lblIDEvaluacionRecomendacionFotoTab2.Text = lblID_evaluacionTab2.Text;


                cargaDetalleFotosVisita();
                PN_ListaAvanceFoto.Visible = true;
                PN_AvanceFotoFormulario.Visible = false;
                Up_AVANCEFOTO.Update();

                MPE_AVANCE_FOTO.Show();
            }
        }

        protected void cargaDetalleFotosVisita()
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIDEvaluacionRecomendacionFotoTab2.Text);

            grdAvanceFoto.DataSource = _objBLEjecucion.spMON_DetalleFotoVisita(_BEEjecucion);
            grdAvanceFoto.DataBind();

        }

        protected void imgbtnFotosTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton = default(ImageButton);
            GridViewRow row = default(GridViewRow);
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grdEvaluacionTab2.DataKeys[row.RowIndex]["id_evaluacionRecomendacion"].ToString();
            hfIdEvaluacionRecomentacion.Value = id;
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(id);

            //Falta habilitar Panel Movil
            DataTable dt = _objBLEjecucion.spMON_DetalleFotoVisitaMovil(_BEEjecucion);
            if (dt.Rows.Count > 0)
            {
                MPE_ElegirFoto.Show();
                grdFotoMovil.DataSource = dt;
                grdFotoMovil.DataBind();
                UpdatePanelFotoMovil.Update();
            }
            else
            {
                string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

            //string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }
        protected void grdAvanceFoto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //jc
            if (e.CommandName == "EliminarAvanceFoto")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdAvanceFoto.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.comentario = "";
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.urlDoc = "";
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_MON_DetalleFotoVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDetalleFotosVisita();
                    Up_Tab2.Update();
                    MPE_AVANCE_FOTO.Show();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void grdAvanceFoto_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblIdDetalleFoto.Text = grdAvanceFoto.SelectedDataKey.Value.ToString();
            GridViewRow row = grdAvanceFoto.SelectedRow;

            txtDescripcionFoto.Text = ((Label)row.FindControl("lblDescripcion")).Text;
            lnkCargarAvanceFoto.Text = ((ImageButton)row.FindControl("imgAvanceFoto")).ToolTip;
            GeneraIcoFile(lnkCargarAvanceFoto.Text, imgBtnCargarAvanceFoto);
            lblNomUsuarioAvanceFoto.Text = "Actualizó: " + ((Label)row.FindControl("lblUsuario")).Text + " - " + ((Label)row.FindControl("lblFechaUpdate")).Text;

            Up_AVANCEFOTO.Update();

            btnModificarAvanceFoto.Visible = true;
            PN_AvanceFotoFormulario.Visible = true;
            btnGuardarAvanceFoto.Visible = false;
            MPE_AVANCE_FOTO.Show();

        }
        protected void btnAgregarAvanceFoto_Click(object sender, ImageClickEventArgs e)
        {
            Up_AVANCEFOTO.Update();
            btnModificarAvanceFoto.Visible = false;
            PN_ListaAvanceFoto.Visible = true;
            PN_AvanceFotoFormulario.Visible = true;
            btnGuardarAvanceFoto.Visible = true;
            lnkCargarAvanceFoto.Text = "";
            //txtFechaAvanceFoto.Text = "";
            imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
            lblNomUsuarioAvanceFoto.Text = "";
        }
        protected void btnGuardarAvanceFoto_Click(object sender, EventArgs e)
        {
            if (txtDescripcionFoto.Text.Length == 0)
            {
                string script = "<script>alert('Ingresar una breve descripción.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                if (validaArchivoFotos(FileUploadCargarAvanceFoto))
                {
                    _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblIDEvaluacionRecomendacionFotoTab2.Text);
                    _BEEjecucion.comentario = txtDescripcionFoto.Text;
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCargarAvanceFoto);

                    //AsyncFileUpload AsyncFileUploadPDF = (AsyncFileUpload)Session["FotoCargaAvance"];
                    //_BEEjecucion.urlDoc = _Metodo.uploadAsyncFileMonitoreo(AsyncFileUploadPDF);

                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLEjecucion.I_MON_DetalleFotoVisita(_BEEjecucion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        PN_AvanceFotoFormulario.Visible = false;
                        cargaDetalleFotosVisita();

                        MPE_AVANCE_FOTO.Show();
                        txtDescripcionFoto.Text = "";
                        lnkCargarAvanceFoto.Text = "";
                        imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
                        PN_AvanceFotoFormulario.Visible = false;
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        protected void btnModificarAvanceFoto_Click(object sender, EventArgs e)
        {

            if (validaArchivoFotos(FileUploadCargarAvanceFoto))
            {

                _BEEjecucion.id_item = Convert.ToInt32(lblIdDetalleFoto.Text);

                if (FileUploadCargarAvanceFoto.PostedFile.ContentLength > 0)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadCargarAvanceFoto);
                }
                else
                {
                    _BEEjecucion.urlDoc = lnkCargarAvanceFoto.Text;
                }

                _BEEjecucion.comentario = txtDescripcionFoto.Text;
                _BEEjecucion.Tipo = 1;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int val = _objBLEjecucion.UD_MON_DetalleFotoVisita(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se Actualizo correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                    cargaDetalleFotosVisita();
                    MPE_AVANCE_FOTO.Show();
                    txtDescripcionFoto.Text = "";
                    lnkCargarAvanceFoto.Text = "";
                    imgBtnCargarAvanceFoto.ImageUrl = "~/img/blanco.png";
                    PN_AvanceFotoFormulario.Visible = false;


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void btnCancelarAvanceFoto_Click(object sender, EventArgs e)
        {
            Up_AVANCEFOTO.Update();
            PN_AvanceFotoFormulario.Visible = false;

        }

        protected void lnkCargarAvanceFoto_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkCargarAvanceFoto.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgAvanceFoto_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdAvanceFoto.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdAvanceFoto.Rows[row.RowIndex].FindControl("imgAvanceFoto");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdAvanceFoto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgAvanceFoto");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void btnGenerarPanelFotografico_Click(object sender, EventArgs e)
        {
            string id = lblIDEvaluacionRecomendacionFotoTab2.Text;
            string script = "<script>window.open('PanelFotografico_Visita.aspx?idE=" + id + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }


        protected void btnVerPanelFotografico_Click(object sender, EventArgs e)
        {
            MPE_ElegirFoto.Hide();

            string script = "<script>('#modalElegirFoto').modal('hide');window.open('PanelFotografico_Visita.aspx?idE=" + hfIdEvaluacionRecomentacion.Value + "&idPo=" + LblID_PROYECTO.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void btnVerFotosMovil_Click(object sender, EventArgs e)
        {
            MPE_ElegirFoto.Hide();

            string script = "<script>$('#modalElegirFoto').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //Panel_Movil.Visible = true;
            //btnVerPanelFotografico.Visible = false;
            //btnVerFotosMovil.Visible = false;
            //UpdatePanelFotoMovil.Update();
        }

        protected Boolean ValidarEvaluacionRecomendacionTab2()
        {
            string result = "";
            

            if (ddlTipoMonitoreo.SelectedValue == "")
            {
                result = result + "*Ingrese tipo de monitoreo.\\n";

            }

            if (txtFechaVisitaTab2.Text == "")
            {
                result = result + "*Ingresar Fecha de Visita.\\n";
            }
            else if (_Metodo.ValidaFecha(txtFechaVisitaTab2.Text) == false)
            {
                result = result + "*Formato no valido de Fecha de Visita.\\n";
            }
            else
            {
                if (Convert.ToDateTime(txtFechaVisitaTab2.Text) > DateTime.Today)
                {
                    result = result + "*La fecha ingresada es mayor a la fecha de hoy.\\n";

                }
            }

            if (ddlEstadoTab2.SelectedValue.Equals(""))
            {
                result = result + "*Seleccionar estado.\\n";
            }
            else if (ddlSubEstadoTab2.SelectedValue.Equals("") && ddlSubEstadoTab2.Visible == true)
            {
                result = result + "*Seleccionar Sub Estado.\\n";
            }

            //Suspensión del plazo de ejecución
            if (ddlSubEstadoTab2.SelectedValue.Equals("87") && txtFechaInicioSuspensionTab2.Text.Length < 10)
            {
                result = result + "*Ingresar fecha de suspensión.\\n";
            }

            if (txtFechaInicioSuspensionTab2.Text.Length > 9)
            {
                if (_Metodo.ValidaFecha(txtFechaInicioSuspensionTab2.Text) == false)
                {
                    result = result + "*Formato no valido de fecha de suspensión.\\n";
                }
                else
                {
                    if (Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text) > DateTime.Today)
                    {
                        result = result + "*La fecha de suspensión ingresada es mayor a la fecha de hoy.\\n";
                    }
                }
            }

            if (txtFechaFinalReinicioTab2.Text.Length > 9)
            {
                if (_Metodo.ValidaFecha(txtFechaFinalReinicioTab2.Text) == false)
                {
                    result = result + "*Formato no valido de fecha de reinicio.\\n";
                }
                else
                {
                    if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) > DateTime.Today)
                    {
                        result = result + "*La fecha de reinicio ingresada es mayor a la fecha de hoy.\\n";
                    }

                    if (Convert.ToDateTime(txtFechaFinalReinicioTab2.Text) < Convert.ToDateTime(txtFechaInicioSuspensionTab2.Text))
                    {
                        result = result + "*La fecha de reinicio debe ser mayor a la fecha de suspensión.\\n";
                    }
                }
            }

            if (ddlSubEstado2Tab2.Visible && ddlSubEstado2Tab2.SelectedValue.Equals(""))//SUB ESTADO 2
            {
                result = result + "*Seleccionar Sub Estado 2.\\n";
            }

            if (ddlSubEstado2Tab2.SelectedValue.Equals("82")) // PALIZADO PERMANENTE
            {
                int contadorProblemas = 0;
                foreach (ListItem item in chbProblematicaTab2.Items)
                {
                    if (item.Selected)
                    {
                        contadorProblemas = contadorProblemas + 1;
                    }
                }

                if (contadorProblemas > 0)
                {
                    result = result + "*Seleccionar problematica.\\n";
                }
            }


            // Actos Previos - En Ejecución - Paralizada
            if (ddlEstadoTab2.SelectedValue.Equals("41") || ddlEstadoTab2.SelectedValue.Equals("4") || ddlEstadoTab2.SelectedValue.Equals("5"))
            {
                if (txtTerminoObraTab2.Text.Length > 0)
                {
                    result = result + "*No puede registrar la fecha de termino de obra en el estado  : " + ddlEstadoTab2.SelectedItem.Text + ".\\n";
                }
            }

            // En Ejecución
            if (ddlEstadoTab2.SelectedValue.Equals("4"))
            {
                if (txtFechaIniReal.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio real.\\n";
                }

                if (txtFechaEntregaTerrenoTab2.Text.Length == 0) //FECHA DE ENTREGA DE TERRENO
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de entrega de terreno.\\n";
                }
            }

            // Concluido  - Paralizada
            if (ddlEstadoTab2.SelectedValue.Equals("6") || ddlEstadoTab2.SelectedValue.Equals("5"))
            {
                if (txtFechaIniReal.Text.Length == 0) //FECHA DE INICIO OBLIGATORIO
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de inicio real.\\n";

                }
            }

            // Concluido  - Post ejecución
            if (ddlEstadoTab2.SelectedValue.Equals("6") || ddlEstadoTab2.SelectedValue.Equals("43"))
            {
                if (txtTerminoObraTab2.Text.Length == 0)
                {
                    result = result + "*Para registrar el estado " + ddlEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de termino de obra.\\n";
                }

                //En Liquidación, Liquidada, Recepcionada
                if ((ddlSubEstadoTab2.SelectedValue.Equals("75") || ddlSubEstadoTab2.SelectedValue.Equals("77") || ddlSubEstadoTab2.SelectedValue.Equals("78")) && txtFechaRecepcionTab2.Text.Equals(""))
                {
                    result = result + "*Para registrar el estado Concluido y subestado " + ddlSubEstadoTab2.SelectedItem.Text + " debe registrar primero la fecha de recepción.\\n";
                }
            }

            //Transferencia
            if (ddlSubEstadoTab2.SelectedValue == "44" && (ddlTipoReceptoraTab3.SelectedValue.Equals("") || txtUnidadReceptoraTab3.Text.Equals("") || ddlOperatividadTab3.SelectedValue == "" || txtFechaActaTransferenciaTab3.Text == "" || LnkbtnSostenibilidadTab3.Text.Equals("")))
            {
                result = result + "*Ingrese tipo, nombre, operatividad, fecha y documento de la entidad receptora(Datos de sostenibilidad) antes de cambiar el estado a Transferencia.\\n";

            }

            //Cierre
            if (ddlSubEstadoTab2.SelectedValue == "45" && (ddlFormatoCierreTabCierre.SelectedValue.Equals("") || txtFechaFormato14TabCierre.Text.Equals("") || lnkbtnFormatoCierreTabCierre.Text.Equals("")))
            {
                string script = "<script>alert('Ingrese tipo de fomato de cierre, fecha de cierre y documento de cierre antes de cambiar el estado a Cerrado');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }

            if (txtEjecutadoTab2.Text == "")
            {
                result = result + "*Ingresar avance físico ejecutado.\\n";
            }
            else if (Convert.ToDouble(txtEjecutadoTab2.Text) > 100)
            {
                result = result + "*El avance físico ejecutado no puede ser mayor a 100%.\\n";

            }
            if (txtProgramadoTab2.Text == "")
            {
                result = result + "*Ingresar avance físico programado.\\n";
            }
            else
            {
                if (Convert.ToDouble(txtProgramadoTab2.Text) > 100)
                {
                    result = result + "*El avance físico programado no puede ser mayor a 100%.\\n";
                }
            }

            if (txtFinanEjecutadoTab2.Text == "")
            {
                result = result + "*Ingresar avance financiero ejecutado.\\n";
            }
            else
            {
                if (Convert.ToDouble(txtFinanEjecutadoTab2.Text) > 100)
                {
                    result = result + "*El avance financiero ejecutado no puede ser mayor a 100%.\\n";
                }
            }


            if (txtFinanProgramadoTab2.Text == "")
            {
                result = result + "*Ingresar avance financiero programado.\\n";

            }
            else
            {
                if (Convert.ToDouble(txtFinanProgramadoTab2.Text) > 100)
                {
                    result = result + "*El avance financiero programado no puede ser mayor a 100%.\\n";
                }
            }

            //CONSISTENCIA DE AVANCES REGISTRADOS
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            List<BE_MON_Ejecucion> ListAvance = new List<BE_MON_Ejecucion>();
            ListAvance = _objBLEjecucion.F_spMON_EvaluacionRecomendacion(_BEEjecucion);

            if (btnGuardarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 0)
            {
                result = result + "*Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.\\n";
            }
            else if (btnModificarEvaluacionTab2.Visible && ListAvance.Where(f => f.Date_fechaVisita == Convert.ToDateTime(txtFechaVisitaTab2.Text)).Count() > 1)
            {
                result = result + "*Debe ingresar otra fecha de visita, ya se encuentra registrado dicha fecha.\\n";
            }

            if (txtFechaVisitaTab2.Text.Length > 0)
            {
                BE_MON_Ejecucion v1 = new BE_MON_Ejecucion { Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text), Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text) };
                BE_MON_Ejecucion itemMayor = null;
                BE_MON_Ejecucion itemMenor = null;
                //Item mayor a la fecha ingresada
                if (ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
                {
                    itemMayor = ListAvance.Where(x => x.Date_fechaVisita >= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).First();

                    if (Convert.ToDouble(itemMayor.FisicoEjec) < Convert.ToDouble(txtEjecutadoTab2.Text))
                    {
                        result = result + "*El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.FisicoEjec + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }

                    if (Convert.ToDouble(itemMayor.financieroReal) < Convert.ToDouble(txtFinanEjecutadoTab2.Text))
                    {
                        result = result + "*El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es mayor al avance " + itemMayor.financieroReal + "% del " + itemMayor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }
                }
                //Item menor a la fecha ingresada
                if (ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Count() > 0)
                {
                    itemMenor = ListAvance.Where(x => x.Date_fechaVisita <= v1.Date_fechaVisita && x.Id_ejecucionRecomendacion != v1.Id_ejecucionRecomendacion).Last();

                    if (Convert.ToDouble(itemMenor.FisicoEjec) > Convert.ToDouble(txtEjecutadoTab2.Text))
                    {
                        result = result + "*El avance físico ingresado: " + txtEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.FisicoEjec + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }

                    if (Convert.ToDouble(itemMenor.financieroReal) > Convert.ToDouble(txtFinanEjecutadoTab2.Text))
                    {
                        result = result + "*El avance financiero ingresado: " + txtFinanEjecutadoTab2.Text + "% con fecha " + txtFechaVisitaTab2.Text + " es menor al avance " + itemMenor.financieroReal + "% del " + itemMenor.Date_fechaVisita.ToString("dd/MM/yyyy") + ".";
                    }
                }
            }

            if (result.Length > 0)
            {
                string script = "<script>alert('" + result + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void btnModificarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUpload_OficioTab4))
            {
                if (validaArchivo(FileUpload_InformeTab4))
                {
                    if (validaArchivo(FileUpload_ActaTab4))
                    {

                        if (ValidarEvaluacionRecomendacionTab2())
                        {
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.id_tipoEstadoEjecuccion = Convert.ToInt32(ddlEstadoTab2.SelectedValue);
                            _BEEjecucion.id_tipoSubEstadoEjecucion = ddlSubEstadoTab2.SelectedValue;
                            _BEEjecucion.id_tipoEstadoParalizado = ddlSubEstado2Tab2.SelectedValue;
                            string vDetalleProblemas = "";
                            if (ddlSubEstadoTab2.SelectedValue.Equals("82"))//Paralizados permanentes
                            {
                                //List<ListItem> selected = new List<ListItem>();
                                foreach (ListItem item in chbProblematicaTab2.Items)
                                {
                                    //string myJson = "{'Username': 'myusername','Password':'pass'}";
                                    vDetalleProblemas = vDetalleProblemas + "{\"idSubEstado\":\"" + ddlSubEstadoTab2.SelectedValue + "\", \"idProblematica\":\"" + item.Value;
                                    if (item.Selected)
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"1\"},";
                                    }
                                    else
                                    {
                                        vDetalleProblemas = vDetalleProblemas + "\",\"flagSelec\": \"0\"},";
                                    }
                                }

                                vDetalleProblemas = vDetalleProblemas.Substring(0, vDetalleProblemas.Length - 1);

                                vDetalleProblemas = "[" + vDetalleProblemas + "]";
                            }
                            else
                            {
                                if (ddlSubEstadoTab2.SelectedValue.Equals("87"))//Suspension
                                {
                                    _BEEjecucion.Date_fechaInicio = _Metodo.VerificaFecha(txtFechaInicioSuspensionTab2.Text);
                                    _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaFinalReinicioTab2.Text);
                                }
                            }
                            _BEEjecucion.detalleProblematica = vDetalleProblemas;
                            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(lblID_evaluacionTab2.Text);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            _BEEjecucion.Date_fechaVisita = Convert.ToDateTime(txtFechaVisitaTab2.Text);

                            _BEEjecucion.Evaluacion = txtEvaluacionTab2.Text;
                            _BEEjecucion.Recomendacion = txtRecomendacionTab2.Text;
                            _BEEjecucion.FisicoEjec = txtEjecutadoTab2.Text;
                            _BEEjecucion.id_tipomonitoreo = ddlTipoMonitoreo.SelectedValue;
                            _BEEjecucion.FisicoProg = txtProgramadoTab2.Text;
                            _BEEjecucion.nrotramite = txtNroTramie.Text;
                            _BEEjecucion.id_tipodocumento = ddlTipoDocumento.SelectedValue;
                            _BEEjecucion.asunto = txtAsunto.Text;
                            _BEEjecucion.Tipo = 1;
                            _BEEjecucion.financieroProgramado = txtFinanProgramadoTab2.Text;
                            _BEEjecucion.financieroReal = txtFinanEjecutadoTab2.Text;

                            if (chbAyudaMemoriaTab4.Checked == true)
                            {
                                _BEEjecucion.flagAyuda = "1";
                            }
                            else
                            {
                                _BEEjecucion.flagAyuda = "0";
                            }

                            if (FileUpload_ActaTab4.HasFile)
                            {
                                _BEEjecucion.UrlActa = _Metodo.uploadfile(FileUpload_ActaTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlActa = LnkbtnActaTab4.Text;
                            }

                            if (FileUpload_InformeTab4.HasFile)
                            {
                                _BEEjecucion.UrlInforme = _Metodo.uploadfile(FileUpload_InformeTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlInforme = LnkbtnInformeTab4.Text;
                            }


                            if (FileUpload_OficioTab4.HasFile)
                            {
                                _BEEjecucion.UrlOficio = _Metodo.uploadfile(FileUpload_OficioTab4);
                            }
                            else
                            {
                                _BEEjecucion.UrlOficio = LnkbtnOficioTab4.Text;
                            }

                            int val;
                            try
                            {
                                val = _objBLEjecucion.spud_MON_EvaluacionRecomendacion(_BEEjecucion);

                                if (trViviendaMonitoreoTab4.Visible == true) //SOLO PNVR
                                {
                                    GuardarViviendas(_BEEjecucion.Id_ejecucionRecomendacion);
                                }
                            }
                            catch (Exception ex)
                            {
                                val = 0;
                            }



                            if (val == 1)
                            {




                                txtFechaVisitaTab2.Text = "";
                                txtEvaluacionTab2.Text = "";
                                txtRecomendacionTab2.Text = "";
                                txtProgramadoTab2.Text = "";
                                txtEjecutadoTab2.Text = "";
                                ddlTipoMonitoreo.SelectedValue = "";
                                txtNroTramie.Text = "";
                                txtTipoDocumento.Text = "";
                                ddlTipoDocumento.SelectedValue = "";
                                txtAsunto.Text = "";
                                chbAyudaMemoriaTab4.Checked = false;
                                Panel_ObservacionTab2.Visible = false;
                                imgbtnAgregarEvaluacionTab2.Visible = true;

                                cargarEvaluacionTab2();
                                CargaDatosTab2();
                                Up_Tab2.Update();

                                string script = "<script>alert('Se Actualizó Correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            }

                        }
                    }
                }
            }

        }
        protected void btnCancelarEvaluacionTab2_OnClick(object sender, EventArgs e)
        {
            Panel_ObservacionTab2.Visible = false;
            imgbtnAgregarEvaluacionTab2.Visible = true;
            Up_Tab2.Update();
        }

        protected void grdEvaluacionTab2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_evaluacionRecomendacion;
            id_evaluacionRecomendacion = grdEvaluacionTab2.SelectedDataKey.Value.ToString();
            lblID_evaluacionTab2.Text = id_evaluacionRecomendacion.ToString();

            GridViewRow row = grdEvaluacionTab2.SelectedRow;
            string xidTipoEstado = ((Label)row.FindControl("lblId_TipoEstadoEjecuccion")).Text;
            string xidTipoSubEstado = ((Label)row.FindControl("lblId_tipoSubEstadoEjecucion")).Text;

            if (xidTipoSubEstado.Equals("83")) //Paralizados Temporales
            {
                string script = "<script>alert('No se puede editar esta información.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                txtFechaVisitaTab2.Text = ((Label)row.FindControl("lblFechaVisita")).Text;
                txtEvaluacionTab2.Text = ((Label)row.FindControl("lblEvaluacion")).Text;

                txtRecomendacionTab2.Text = ((Label)row.FindControl("lblRecomendacion")).Text;

                txtEjecutadoTab2.Text = ((Label)row.FindControl("lblFisicoEjec")).Text;
                txtProgramadoTab2.Text = ((Label)row.FindControl("lblFisicoProg")).Text;

                txtFinanProgramadoTab2.Text = ((Label)row.FindControl("lblFinancieroProg")).Text;
                txtFinanEjecutadoTab2.Text = ((Label)row.FindControl("lblFinancieroEjec")).Text;

                ddlTipoMonitoreo.Text = ((Label)row.FindControl("lblid_tipoMonitor")).Text;
                txtNroTramie.Text = ((Label)row.FindControl("lblNroTramite")).Text;
                ddlTipoDocumento.Text = ((Label)row.FindControl("lblid_tipoDocumento")).Text;
                txtAsunto.Text = ((Label)row.FindControl("lblAsunto")).Text;

                chbAyudaMemoriaTab4.Checked = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;

                LnkbtnActaTab4.Text = ((ImageButton)row.FindControl("imgDocActaTab4")).ToolTip;
                LnkbtnInformeTab4.Text = ((ImageButton)row.FindControl("imgDocInformeTab4")).ToolTip;
                LnkbtnOficioTab4.Text = ((ImageButton)row.FindControl("imgDocOficioTab4")).ToolTip;

                lblNomUsuarioFlagAyudaTab4.Text = "(Actualizó: " + ((Label)row.FindControl("lblId_UsuarioFlag")).Text + " - " + ((Label)row.FindControl("lblFecha_UpdateFlag")).Text + ")";
                lblNomUsuarioObservaciones.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

                GeneraIcoFile(LnkbtnActaTab4.Text, imgbtnActaTab4);
                GeneraIcoFile(LnkbtnInformeTab4.Text, imgbtnInformeTab4);
                GeneraIcoFile(LnkbtnOficioTab4.Text, imgbtnOficioTab4);

                btnModificarEvaluacionTab2.Visible = true;
                btnGuardarEvaluacionTab2.Visible = false;
                Panel_ObservacionTab2.Visible = true;
                imgbtnAgregarEvaluacionTab2.Visible = false;
                chbAyudaMemoriaTab4.Enabled = false;


                //cargar las viviendas
                TxtViviendasConcluidas.Text = ((Label)row.FindControl("LblViviendasConcluidas")).Text;
                TxtViviendasEnEjecucion.Text = ((Label)row.FindControl("LblViviendasEnEjecucion")).Text;
                TxtViviendasPorIniciar.Text = ((Label)row.FindControl("LblViviendasPorIniciar")).Text;
                TxtViviendasParalizadas.Text = ((Label)row.FindControl("LblViviendasParalizadas")).Text; 
             

                //aca ini
                CargaTipoAdjudicacionTab2();

                ddlEstadoTab2.SelectedValue = xidTipoEstado;
                CargaSubEstadoEjecucionTab2(ddlEstadoTab2.SelectedValue);
                ddlSubEstadoTab2.SelectedValue = xidTipoSubEstado;

                trSuspensionTab2.Visible = false;
                trReinicioTab2.Visible = false;
                Up_FechasSuspensionTab2.Update();
                if (ddlEstadoTab2.SelectedValue.Equals("5"))
                {
                    CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                    ddlSubEstado2Tab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                    ddlSubEstado2Tab2.Visible = true;
                    spSubestado2.Visible = true;
                    CargaTipoSubEstadoParalizado(ddlSubEstado2Tab2.SelectedValue); //TIPO PROBLEMATICA

                    string idDetalleProblematica = ((Label)row.FindControl("lblId_DetalleProblematica")).Text;
                    string[] itemProblematica = idDetalleProblematica.Split(',');
                    foreach (string item in itemProblematica)
                    {
                        foreach (ListItem itemChkb in chbProblematicaTab2.Items)
                        {
                            if (itemChkb.Value == item)
                            {
                                itemChkb.Selected = true;
                            }
                        }
                    }

                    lblNombreProblematicaTab2.Visible = true;
                    //ddlTipoProblematicaTab2.Visible = true;
                    chbProblematicaTab2.Visible = true;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();
                }
                else
                {
                    //ddlSubEstado2Tab2.SelectedValue = "";
                    ddlSubEstado2Tab2.Visible = false;
                    spSubestado2.Visible = false;

                    lblNombreProblematicaTab2.Visible = false;
                    chbProblematicaTab2.Items.Clear();
                    chbProblematicaTab2.Visible = false;
                    Up_NombreProblematicaTab2.Update();
                    Up_TipoProblematicaTab2.Update();

                    if (xidTipoSubEstado.Equals("87")) // Suspension de plazo
                    {
                        trSuspensionTab2.Visible = true;
                        trReinicioTab2.Visible = true;

                        txtFechaInicioSuspensionTab2.Text = ((Label)row.FindControl("lblFechaSuspensionGrilla")).Text;
                        txtFechaFinalReinicioTab2.Text = ((Label)row.FindControl("lblFechaReinicioGrilla")).Text;
                        Up_FechasSuspensionTab2.Update();

                        //Cargar SubEstado2
                        CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                        ddlSubEstado2Tab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                        ddlSubEstado2Tab2.Visible = true;
                        spSubestado2.Visible = true;

                    }

                    if (xidTipoSubEstado.Equals("80")) // Atrasado
                    {
                        //Cargar SubEstado2
                        CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
                        ddlSubEstado2Tab2.SelectedValue = ((Label)row.FindControl("lblId_TipoEstadoParalizado")).Text;
                        ddlSubEstado2Tab2.Visible = true;
                        spSubestado2.Visible = true;
                    }
                }
                //acater

                Panel_InformacionComplementaria.Visible = true;

                //VALIDACION PARA EVITAR CAMBIO DE ESTADOS
                string vfecha = ((Label)row.FindControl("lblFecha_Update")).Text;
                if (vfecha.Length > 10 && !xidTipoEstado.Equals(""))
                {
                    if (Convert.ToDateTime(vfecha.Substring(0, 10)).ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        ddlEstadoTab2.Enabled = true;
                        ddlSubEstadoTab2.Enabled = true;
                        ddlSubEstado2Tab2.Enabled = true;
                    }
                    else
                    {
                        if (!LblID_USUARIO.Text.Equals("481")) //USUARIO MONITOR PUEDE MODIFIAR LOS ESTADOS
                        {
                            ddlEstadoTab2.Enabled = false;
                            ddlSubEstadoTab2.Enabled = false;
                            ddlSubEstado2Tab2.Enabled = false;
                        }
                        if (ddlSubEstado2Tab2.Visible && ddlSubEstado2Tab2.SelectedValue.Equals(""))
                        {
                            ddlSubEstado2Tab2.Enabled = true;
                        }
                        if (lblCOD_SUBSECTOR.Text.Equals("3"))
                        {
                            ddlSubEstadoTab2.Enabled = true;
                            ddlSubEstado2Tab2.Enabled = true;
                        }
                    }
                }

                Up_Tab2.Update();
            }
        }

        protected void CargaTipoSubEstado2(string pTipoSubEstado)
        {
            //ddlTipoSubEstado2InformeTab2.DataSource = null;
            //ddlTipoSubEstado2InformeTab2.DataBind();
            ddlSubEstado2Tab2.Items.Clear();

            List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

            if (!(pTipoSubEstado == ""))
            {
                list = _objBLEjecucion.F_spMON_TipoSubEstado2(Convert.ToInt32(pTipoSubEstado));
            }
            else
            {
                list = null;
            }

            ddlSubEstado2Tab2.DataSource = list;
            ddlSubEstado2Tab2.DataTextField = "nombre";
            ddlSubEstado2Tab2.DataValueField = "valor";
            ddlSubEstado2Tab2.DataBind();

            ddlSubEstado2Tab2.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }


        protected void grdEvaluacionTab2_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdEvaluacionTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fechaVisita = Convert.ToDateTime("09/09/2012");

                int val = _objBLEjecucion.spud_MON_EvaluacionRecomendacion(_BEEjecucion);

                if (val == 1)
                {
                    CargaDatosTab2();
                    cargarEvaluacionTab2();
                    Up_Tab2.Update();

                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);




                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void grdEvaluacionTab2_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imbActa = (ImageButton)e.Row.FindControl("imgDocActaTab4");

                GeneraIcoFile(imbActa.ToolTip, imbActa);

                ImageButton imbInforme = (ImageButton)e.Row.FindControl("imgDocInformeTab4");
                GeneraIcoFile(imbInforme.ToolTip, imbInforme);

                ImageButton imbOficio = (ImageButton)e.Row.FindControl("imgDocOficioTab4");
                GeneraIcoFile(imbOficio.ToolTip, imbOficio);

                CheckBox chbFlagAyuda = (CheckBox)e.Row.FindControl("chbFlagAyuda");

                if (lblID_ACCESO.Text.Equals("0"))
                {
                    chbFlagAyuda.Enabled = false;
                }

                if (chbFlagAyuda.Text == "1")
                {
                    chbFlagAyuda.Checked = true;
                    chbFlagAyuda.Text = "";
                }
                else
                {
                    chbFlagAyuda.Checked = false;
                    chbFlagAyuda.Text = "";
                }
            }
        }

        protected void imgDocActaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocActaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocInformeTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocInformeTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgDocOficioTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdEvaluacionTab2.Rows[row.RowIndex].FindControl("imgDocOficioTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnActaTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab4.Text);
                Response.End();
            }
        }
        protected void LnkbtnInformeTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnInformeTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnInformeTab4.Text);
                Response.End();
            }
        }
        protected void LnkbtnOficioTab4_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnOficioTab4.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnOficioTab4.Text);
                Response.End();
            }
        }

        protected Boolean ValidarAgentesEjecucionTab2()
        {
            Boolean result;
            result = true;

            if (ddlFuncionTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar función de agente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlNombreAgenteTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar nombre de agente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoContratoAgenteTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto del contrato (días).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (ddlEstadoAsignacionAgenteTab2.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar estado de asignacion de agente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (btnGuardarAgenteEjecucionTab2.Visible == true && ddlEstadoAsignacionAgenteTab2.SelectedValue == "1")
            {
                if (ddlFuncionTab2.SelectedValue != "1" && ddlFuncionTab2.SelectedValue != "2")
                {
                    List<BE_MON_Ejecucion> ListaAgenteAsignados = new List<BE_MON_Ejecucion>();
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                    ListaAgenteAsignados = _objBLEjecucion.F_spMON_Agentes(_BEEjecucion);

                    ListaAgenteAsignados = (from cust in ListaAgenteAsignados
                                            where cust.id_tipoAgente == Convert.ToInt32(ddlFuncionTab2.SelectedValue) && cust.id_tipoEstadoEjecuccion == 1
                                            select cust).ToList();

                    if (ListaAgenteAsignados.Count > 0)
                    {
                        string script = "<script>alert('Ya existe un " + ddlFuncionTab2.SelectedItem.Text + " - Activo. Debe verificar para continuar con el registro.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }

            if (btnModificarAgenteEjecucionTab2.Visible == true && lblEstadoAnteriorTab2.Text != "1")
            {

                List<BE_MON_Ejecucion> ListaAgenteAsignados = new List<BE_MON_Ejecucion>();
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                ListaAgenteAsignados = _objBLEjecucion.F_spMON_Agentes(_BEEjecucion);

                ListaAgenteAsignados = (from cust in ListaAgenteAsignados
                                        where cust.id_tipoAgente == Convert.ToInt32(ddlFuncionTab2.SelectedValue) && cust.id_tipoEstadoEjecuccion == 1
                                        select cust).ToList();

                if (ListaAgenteAsignados.Count > 0)
                {
                    string script = "<script>alert('Ya existe un " + ddlFuncionTab2.SelectedItem.Text + " - Activo. Debe verificar para continuar con el registro.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            return result;
        }

        protected void grdProgramacionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_avanceFisico;
            id_avanceFisico = grdProgramacionTab2.SelectedDataKey.Value.ToString();
            lblIdProgramacion.Text = id_avanceFisico.ToString();

            GridViewRow row = grdProgramacionTab2.SelectedRow;

            txtInformeTab2.Text = "Del " + ((Label)row.FindControl("lblFechaInicioInformar")).Text + " al " + ((Label)row.FindControl("lblFechaFinInformar")).Text;
            txtInicioInformeTab2.Text = ((Label)row.FindControl("lblFechaInicioInformar")).Text;
            txtFinInformeTab2.Text = ((Label)row.FindControl("lblFechaFinInformar")).Text;
            txtMesTab2.Text = ((Label)row.FindControl("lblMes")).Text;

            txtFechaProgramadaTab2.Text = ((Label)row.FindControl("lblFechaProgramada")).Text;

            txtAvanceFisicoProgramacionTab2.Text = ((Label)row.FindControl("lblFisicoProgramado")).Text;
            lblFisicoProgPreAcumuladoCronogramaTab2.Text = (Convert.ToDouble(lblFisicoProgAcumuladoCronogramTab2.Text) - Convert.ToDouble(txtAvanceFisicoProgramacionTab2.Text)).ToString();

            txtAvanceFinancieroProgramacionTab2.Text = ((Label)row.FindControl("lblFinancieroProgramado")).Text;
            lblFinancieroProgPreAcumuladoCronogramaTab2.Text = (Convert.ToDouble(lblFinancieroProgAcumuladoCronogramaTab2.Text) - Convert.ToDouble(txtAvanceFinancieroProgramacionTab2.Text)).ToString();

            lblNomUsuarioProgramacionTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            if (((Label)row.FindControl("lblFlagInforme")).Text == "1")
            {
                lblMsjProgramacionTab2.Visible = true;
            }
            else
            {
                lblMsjProgramacionTab2.Visible = false;
            }

            Panel_RegistrarProgramacionTab2.Visible = true;

            Up_Tab2.Update();
        }
        protected void grdProgramacionTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;

                object objTemp = grdProgramacionTab2.DataKeys[CurrentRow.RowIndex].Value as object;
                _BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                _BEEjecucion.Date_fechaInicio = VerificaFecha("");
                _BEEjecucion.Date_fechaFin = VerificaFecha("");
                _BEEjecucion.fisicoProgramado = "0";
                _BEEjecucion.diasProgramado = 0;

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_ProgramacionInforme(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaProgramacionTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void cargaProgramacionTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;

            List<BE_MON_Ejecucion> ListProgramacion = new List<BE_MON_Ejecucion>();

            ListProgramacion = _objBLEjecucion.F_spMON_ProgramacionInforme(_BEEjecucion);

            grdProgramacionTab2.DataSource = ListProgramacion;
            grdProgramacionTab2.DataBind();
            
            lblFisicoProgAcumuladoCronogramTab2.Text = (ListProgramacion.Where(x => x.fisicoProgramado.Length > 0).Sum(x => Convert.ToDouble(x.fisicoProgramado))).ToString("N");
            lblFinancieroProgAcumuladoCronogramaTab2.Text = (ListProgramacion.Sum(x => Convert.ToDouble(x.financieroProgramado))).ToString("N");

            if (ListProgramacion != null)
            {
                ListProgramacion = (from cust in ListProgramacion
                                    where cust.flagVerificado == "0" && cust.strFechaProgramada != ""
                                    select cust).ToList();
            }

            ddlFechaProgramacionInformeTab2.DataSource = ListProgramacion;
            ddlFechaProgramacionInformeTab2.DataTextField = "nombre";
            ddlFechaProgramacionInformeTab2.DataValueField = "id_avanceFisico";
            ddlFechaProgramacionInformeTab2.DataBind();
            ddlFechaProgramacionInformeTab2.Items.Insert(0, new ListItem("-Seleccione-", ""));


        }

        protected void btnModificarProgramacionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarProgramacionTab2())
            {
                _BEEjecucion.id_avanceFisico = Convert.ToInt32(lblIdProgramacion.Text);
                //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionProgramacionTab2.SelectedValue);
                _BEEjecucion.dtFechaProgramada = VerificaFecha(txtFechaProgramadaTab2.Text);
                _BEEjecucion.fisicoProgramado = txtAvanceFisicoProgramacionTab2.Text;
                _BEEjecucion.financieroProgramado = txtAvanceFinancieroProgramacionTab2.Text;

                _BEEjecucion.Tipo = 1;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_ProgramacionInforme(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se modifico la programación correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    Panel_RegistrarProgramacionTab2.Visible = false;
                    //imgbtnAgregarProgramacionTab2.Visible = true;
                    cargaProgramacionTab2();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void btnCancelarProgramacionTab2_Click(object sender, EventArgs e)
        {
            Panel_RegistrarProgramacionTab2.Visible = false;
            //imgbtnAgregarProgramacionTab2.Visible = true;
        }

        protected Boolean ValidarProgramacionTab2()
        {
            Boolean result;
            result = true;


            if (txtFechaProgramadaTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha limite de Pre Liquidicación.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            if (Convert.ToDateTime(txtFechaProgramadaTab2.Text) < Convert.ToDateTime(txtInicioInformeTab2.Text))
            {
                string script = "<script>alert('La fecha limite de Pre Liquidicación debe ser mayor al " + txtInicioInformeTab2.Text + ".');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            //if (Convert.ToDateTime(txtFechaProgramadaTab2.Text) > Convert.ToDateTime(txtFinInformeTab2.Text).AddDays(9)  )
            //{
            //    string script = "<script>alert('La fecha limite de Pre Liquidicación debe ser menor al " + Convert.ToDateTime(txtFinInformeTab2.Text).AddDays(9) + ".');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}


            if (txtAvanceFisicoProgramacionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar avance fisico programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if ((Convert.ToDouble(txtAvanceFisicoProgramacionTab2.Text) + Convert.ToDouble(lblFisicoProgPreAcumuladoCronogramaTab2.Text)) > 100)
            {
                string script = "<script>alert('El avance físico programado acumulado es: " + (Convert.ToDouble(txtAvanceFisicoProgramacionTab2.Text) + Convert.ToDouble(lblFisicoProgPreAcumuladoCronogramaTab2.Text)).ToString("N2") + " % y debe ser menor a 100%.');</script>";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtAvanceFinancieroProgramacionTab2.Text == "")
            {
                string script = "<script>alert('Ingresar avance financiero programado.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if ((Convert.ToDouble(txtAvanceFinancieroProgramacionTab2.Text) + Convert.ToDouble(lblFinancieroProgPreAcumuladoCronogramaTab2.Text)) > 100)
            {
                string script = "<script>alert('El avance financiero programado acumulado es: " + (Convert.ToDouble(txtAvanceFinancieroProgramacionTab2.Text) + Convert.ToDouble(lblFinancieroProgPreAcumuladoCronogramaTab2.Text)).ToString("N2") + " % y debe ser menor a 100%.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            return result;
        }

        protected void ddlFechaProgramacionInformeTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFechaProgramacionInformeTab2.SelectedValue != "")
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;

                List<BE_MON_Ejecucion> ListProgramacion = new List<BE_MON_Ejecucion>();

                ListProgramacion = _objBLEjecucion.F_spMON_ProgramacionInforme(_BEEjecucion);

                ListProgramacion = (from cust in ListProgramacion
                                    where cust.id_avanceFisico == Convert.ToInt32(ddlFechaProgramacionInformeTab2.SelectedValue)
                                    select cust).ToList();

                lblId_Registro_Avance.Text = ListProgramacion.ElementAt(0).id_avanceFisico.ToString();
                txtFisiProgTab2.Text = ListProgramacion.ElementAt(0).fisicoProgramado;
                //txtDiasProgramadoInformesTab2.Text = ListProgramacion.ElementAt(0).diasProgramado.ToString();
                txtFinanProgTab2.Text = ListProgramacion.ElementAt(0).financieroProgramado;
            }

        }

        protected void ddlEstadoPreLiquidacionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string estadoPreLiquidacion = ddlEstadoPreLiquidacionTab2.SelectedValue;
            if (estadoPreLiquidacion == "2")
                txtFechaObservacionPreliquidacionTab2.Enabled = true;
            else
                txtFechaObservacionPreliquidacionTab2.Enabled = false;
        }

        protected void lnkbtnPagosNETab2_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            Label url = (Label)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("lblid_avanceFisico");

            string urlDocumento = url.Text;

            string script = "<script>window.open('Pago_MensualNE.aspx?id=" + urlDocumento + "&&t=z','doc','width=900,height=800,status=1,scrollbars=1','_blank') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void imgbtnAgregarAgenteTab2_Click(object sender, ImageClickEventArgs e)
        {
            PanelRegistrarAgenteEjecucionTab2.Visible = true;
            imgbtnAgregarAgenteTab2.Visible = false;
            btnGuardarAgenteEjecucionTab2.Visible = true;
            btnModificarAgenteEjecucionTab2.Visible = false;

            ddlFuncionTab2.Enabled = true;
            ddlNombreAgenteTab2.Enabled = true;

            string scriptSelect = "<script>if(typeof(applySelect2)=='function'){applySelect2('.ddl-filter');}</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", scriptSelect, false);

        }
        protected void btnGuardarAgenteEjecucionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarAgentesEjecucionTab2())
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.Date_fechaActaAsignacion = VerificaFecha(txtFechaActaAsignacionAgenteTab2.Text);
                _BEEjecucion.id_tecnico = Convert.ToInt32(ddlNombreAgenteTab2.SelectedValue);
                //_BEEjecucion.nroContrato = txtNroContratoAgenteTab2.Text;
                _BEEjecucion.monto = txtMontoContratoAgenteTab2.Text;
                //_BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaIncioContratoAgenteTab2.Text);
                //_BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoContratoTab2.Text);
                //_BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaEstimadaFinTab2.Text);
                _BEEjecucion.id_estado = Convert.ToInt32(ddlEstadoAsignacionAgenteTab2.Text);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.id_tipoAgente = Convert.ToInt32(ddlFuncionTab2.SelectedValue);

                int val = _objBLEjecucion.I_spi_MON_AsignarAgente(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se guardó Correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    PanelRegistrarAgenteEjecucionTab2.Visible = false;
                    imgbtnAgregarAgenteTab2.Visible = true;
                    CargaAgentesAsignados();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }

        protected void CargaAgentesAsignados()
        {
            List<BE_MON_Ejecucion> ListaAgenteAsignados = new List<BE_MON_Ejecucion>();
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            ListaAgenteAsignados = _objBLEjecucion.F_spMON_Agentes(_BEEjecucion);

            grdAgenteTab2.DataSource = ListaAgenteAsignados;
            grdAgenteTab2.DataBind();
        }
        protected void btnCancelarAgenteEjecucionTab2_Click(object sender, EventArgs e)
        {
            PanelRegistrarAgenteEjecucionTab2.Visible = false;
            imgbtnAgregarAgenteTab2.Visible = true;
        }

        protected void lnkbtnActaEntregaTerrenoTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnActaEntregaTerrenoTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void lnkbtnInformeInicialTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnInformeInicialTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void ddlFuncionTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaAgentexFuncion(ddlNombreAgenteTab2, ddlFuncionTab2.SelectedValue);
            Up_Tab2.Update();

            string scriptSelect = "<script>if(typeof(applySelect2)=='function'){applySelect2('.ddl-filter');}</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", scriptSelect, false);
        }
        protected void grdAgenteTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = grdAgenteTab2.SelectedRow;
            ddlFuncionTab2.SelectedValue = ((Label)row.FindControl("lblIdTipoAgente")).Text;
            cargaAgentexFuncion(ddlNombreAgenteTab2, ddlFuncionTab2.SelectedValue);
            ddlNombreAgenteTab2.SelectedValue = ((Label)row.FindControl("lblIdTecnico")).Text;
            txtFechaActaAsignacionAgenteTab2.Text = ((Label)row.FindControl("lblFechaAsignacion")).Text;
            txtMontoContratoAgenteTab2.Text = ((Label)row.FindControl("lblMontoContrato")).Text;

            if (((Label)row.FindControl("lblIdTipoEstado")).Text != "0")
            {
                ddlEstadoAsignacionAgenteTab2.SelectedValue = ((Label)row.FindControl("lblIdTipoEstado")).Text;
            }

            lblEstadoAnteriorTab2.Text = ((Label)row.FindControl("lblIdTipoEstado")).Text;
            lblNomUsuarioAgenteTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            ddlFuncionTab2.Enabled = false;
            ddlNombreAgenteTab2.Enabled = false;

            btnGuardarAgenteEjecucionTab2.Visible = false;
            btnModificarAgenteEjecucionTab2.Visible = true;
            PanelRegistrarAgenteEjecucionTab2.Visible = true;
            imgbtnAgregarAgenteTab2.Visible = false;

            Up_Tab2.Update();
        }
        protected void btnModificarAgenteEjecucionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarAgentesEjecucionTab2())
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.Date_fechaActaAsignacion = VerificaFecha(txtFechaActaAsignacionAgenteTab2.Text);
                _BEEjecucion.id_tecnico = Convert.ToInt32(ddlNombreAgenteTab2.SelectedValue);
                _BEEjecucion.id_tipoAgente = Convert.ToInt32(ddlFuncionTab2.SelectedValue);
                //_BEEjecucion.nroContrato = txtNroContratoAgenteTab2.Text;
                _BEEjecucion.monto = txtMontoContratoAgenteTab2.Text;
                //_BEEjecucion.Date_fechaInicio = VerificaFecha(txtFechaIncioContratoAgenteTab2.Text);
                //_BEEjecucion.plazoEjecuccion = Convert.ToInt32(txtPlazoContratoTab2.Text);
                //_BEEjecucion.Date_fechaFin = VerificaFecha(txtFechaEstimadaFinTab2.Text);
                _BEEjecucion.id_estado = Convert.ToInt32(ddlEstadoAsignacionAgenteTab2.Text);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Tipo = 1;

                int val = _objBLEjecucion.UD_spud_MON_AsignarAgente(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se guardo correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    PanelRegistrarAgenteEjecucionTab2.Visible = false;
                    imgbtnAgregarAgenteTab2.Visible = true;
                    CargaAgentesAsignados();
                    Up_Tab2.Update();


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void grdAgenteTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;

                _BEEjecucion.id_tecnico = Convert.ToInt32(((Label)CurrentRow.FindControl("lblIdTecnico")).Text);
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

                //object objTemp = grdAgenteTab2.DataKeys[CurrentRow.RowIndex].Value as object;
                //_BEEjecucion.id_avanceFisico = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEEjecucion.Date_fechaActaAsignacion = VerificaFecha("");
                _BEEjecucion.Date_fechaInicio = VerificaFecha("");
                _BEEjecucion.Date_fechaFin = VerificaFecha("");
                //_BEEjecucion.nroContrato = "";
                _BEEjecucion.monto = "0";
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.UD_spud_MON_AsignarAgente(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaAgentesAsignados();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }


        }

        protected void lnkbtnFichaRecepcionTab2_OnClick(object sender, EventArgs e)
        {
            //LinkButton boton;
            //boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFichaRecepcionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void imgbtnFichaRecepcionTab2_OnClick(object sender, ImageClickEventArgs e)
        {
            //ImageButton boton;
            //boton = (ImageButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFichaRecepcionTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void CargaSaldoBancario()
        {
            _BEProceso = new BE_MON_PROCESO();
            _BEProceso = _objBLProceso.F_spMON_SaldoBancoNE(Convert.ToInt32(LblID_PROYECTO.Text));

            if (_BEProceso.monto != null)
            {
                txtSaldoBancarioTab2.Text = _BEProceso.monto;
                lblNomActualizaSaldoTab2.Text = "Actualizó: " + _BEProceso.usuario + " - " + _BEProceso.strFecha_update;
            }

        }
        protected void btnActualizarSaldoTab2_Click(object sender, EventArgs e)
        {
            _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEProceso.monto = txtSaldoBancarioTab2.Text;

            _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLProceso.IU_spiu_MON_SaldoBancoNE(_BEProceso);

            if (val > 0)
            {
                string script = "<script>alert('Se actualizó el saldo bancario.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                CargaSaldoBancario();
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }
        #endregion

        #region Tab3

        protected void CargaTab3()
        {
            
            cargaTipoReceptora();
            CargaFormatoTabCierre();

            CargaLiquidacion();
            // txtTotalTransferencia.Text = txtAcumuladorTranferenciaTab0.Text;


        }

        protected void CargaLiquidacion()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BELiquidacion.tipoFinanciamiento = 3;

            dt = _objBLLiquidacion.sp_MON_Liquidacion(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                txtFechaDocumentacionTab3.Text = dt.Rows[0]["fechaDocumentacionComplementaria"].ToString();
                txtFechaFirmaTab3.Text = dt.Rows[0]["fechaFirmaLiquidadorPrograma"].ToString();

                txtSaldoTab3.Text = dt.Rows[0]["saldoDevolver"].ToString();
                LnkbtnDevolucionTab3.Text = dt.Rows[0]["urlDocDevolucion"].ToString();
                txtLiquidacionTab3.Text = dt.Rows[0]["resolucion"].ToString();
                txtFechaTab3.Text = dt.Rows[0]["fecha"].ToString();
                txtMontoTransferenciaTab3.Text = dt.Rows[0]["montoTransferencia"].ToString();
                txtMontoTab3.Text = dt.Rows[0]["montoLiquidacion"].ToString();
                LnkbtnLiquidacionTab3.Text = dt.Rows[0]["urlLiquidacion"].ToString();
                LnkbtnActaTab3.Text = dt.Rows[0]["urlActaCierre"].ToString();

                lblNomLiquidacion.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

                double val1 = 0;
                if (txtMontoTab3.Text != "")
                { val1 = Convert.ToDouble(txtMontoTab3.Text); }

                double val3 = 0;
                if (txtSaldoTab3.Text != "")
                { val3 = Convert.ToDouble(txtSaldoTab3.Text); }

                GeneraIcoFile(LnkbtnDevolucionTab3.Text, imgbtnDevolucionTab3);
                GeneraIcoFile(LnkbtnLiquidacionTab3.Text, imgbtnLiquidacionTab3);
                GeneraIcoFile(LnkbtnActaTab3.Text, imgbtnActaTab3);

                // INFORMACION DE LIQUIDACION PNVR

                txtFechaSuscripciónTerminacionObraTab3.Text = dt.Rows[0]["fechaSuscripcionTerminacionObra"].ToString();
                txtFechaComunicacionTerminoObraTab3.Text = dt.Rows[0]["fechaTerminoObraInicioLiquidacion"].ToString();
                txtFechaSuscripcionAprobacionTab3.Text = dt.Rows[0]["fechaSuscripcion"].ToString();
                txtNroResolucionTab3.Text = dt.Rows[0]["nroResolucionAprobacion"].ToString();

                txtFechaRegistroBajaTab3.Text = dt.Rows[0]["fechaBajaContable"].ToString();

                //SOSTENIBILIDAD
                ddlTipoReceptoraTab3.SelectedValue = dt.Rows[0]["id_tipoReceptora"].ToString();
                txtUnidadReceptoraTab3.Text = dt.Rows[0]["UnidadReceptora"].ToString();
                ddlflagCapacitadTab3.SelectedValue = dt.Rows[0]["flagCapacidadAdministrar"].ToString();
                ddlOperatividadTab3.SelectedValue = dt.Rows[0]["iFlagOperativo"].ToString();
                txtFechaActaTransferenciaTab3.Text = dt.Rows[0]["fechaFinalEntregaObraOperacion"].ToString();
                LnkbtnSostenibilidadTab3.Text = dt.Rows[0]["urlDocSostenibilidad"].ToString();
                GeneraIcoFile(LnkbtnSostenibilidadTab3.Text, imgbtnSostenibilidadTab3);
                txtComentarioTab3.Text = dt.Rows[0]["comentarioSostenibilidad"].ToString();


                // CIERRE

                ddlFormatoCierreTabCierre.SelectedValue = dt.Rows[0]["iTipoFormatoCierre"].ToString();
                txtFechaFormato14TabCierre.Text = dt.Rows[0]["fechaFormatoSNIP14"].ToString();
                lnkbtnFormatoCierreTabCierre.Text = dt.Rows[0]["urlFormatoCierre"].ToString();
                GeneraIcoFile(lnkbtnFormatoCierreTabCierre.Text, imgbtnFormatoCierreTabCierre);
                lblNomActualizaCierreTab3.Text = "Actualizó: " + dt.Rows[0]["usuarioCierre"].ToString() + " - " + dt.Rows[0]["fechaUpdateCierre"].ToString();
            }

        }


      

        protected void btnGuardarTab3_OnClick(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadDevolucionTab3))
            {
                if (validaArchivo(FileUploadLiquidacionTab3))
                {
                    if (validaArchivo(FileUploadActaTab3))
                    {
                        if (ValidarLiquidacionNETab3())
                        {
                            _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BELiquidacion.tipoFinanciamiento = 3;

                            _BELiquidacion.Date_fechaDocumentacion = VerificaFecha(txtFechaDocumentacionTab3.Text);
                            _BELiquidacion.Date_fechaFirma = VerificaFecha(txtFechaFirmaTab3.Text);
                            _BELiquidacion.saldoDevolver = txtSaldoTab3.Text;
                            if (FileUploadDevolucionTab3.HasFile)
                            {
                                _BELiquidacion.urlDocDevolucion = _Metodo.uploadfile(FileUploadDevolucionTab3);
                            }
                            else
                            { _BELiquidacion.urlDocDevolucion = LnkbtnDevolucionTab3.Text; }
                            _BELiquidacion.resolucion = txtLiquidacionTab3.Text;
                            _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaTab3.Text);
                            _BELiquidacion.monto = txtMontoTab3.Text;
                            _BELiquidacion.montoTransferencia = txtMontoTransferenciaTab3.Text;
                            if (FileUploadLiquidacionTab3.HasFile)
                            {
                                _BELiquidacion.urlLiquidacion = _Metodo.uploadfile(FileUploadLiquidacionTab3);
                            }
                            else
                            { _BELiquidacion.urlLiquidacion = LnkbtnLiquidacionTab3.Text; }

                            if (FileUploadActaTab3.HasFile)
                            {
                                _BELiquidacion.urlActaCierre = _Metodo.uploadfile(FileUploadActaTab3);
                            }
                            else
                            { _BELiquidacion.urlActaCierre = LnkbtnActaTab3.Text; }
                            _BELiquidacion.Date_fechaSuscripcionTerminacionObra = VerificaFecha(txtFechaSuscripciónTerminacionObraTab3.Text);
                            _BELiquidacion.Date_fechaTerminoObraInicioLiquidacion = VerificaFecha(txtFechaComunicacionTerminoObraTab3.Text);
                            _BELiquidacion.Date_fechaSuscripcion = VerificaFecha(txtFechaSuscripcionAprobacionTab3.Text);
                            _BELiquidacion.Date_fechaBajaContable = VerificaFecha(txtFechaRegistroBajaTab3.Text);
                            _BELiquidacion.resolucionAprobacion = txtNroResolucionTab3.Text;

                            _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                            int val = _objBLLiquidacion.IU_spiu_MON_LiquidacionNE(_BELiquidacion);

                            if (val == 1)
                            {
                                string script = "<script>alert('Se registró correctamente.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                CargaLiquidacion();
                            }
                            else
                            {
                                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                            }

                            Up_Tab3.Update();
                        }
                    }
                }
            }
        }

        protected Boolean ValidarLiquidacionNETab3()
        {
            Boolean result;
            result = true;


            //if (txtSaldoTesoroTab3.Text == "")
            //{
            //    string script = "<script>alert('Ingresar saldo a devolver al tesoro público.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            if (txtMontoTransferenciaTab3.Text == "")
            {
                string script = "<script>alert('Ingresar monto total de transferencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtMontoTab3.Text == "")
            {
                string script = "<script>alert('Ingresar monto de liquidación final.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaSuscripciónTerminacionObraTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaSuscripciónTerminacionObraTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha suscripción acta de terminación de obra.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaComunicacionTerminoObraTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaComunicacionTerminoObraTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha asamblea general para comunicar término de obra e inicio de liquidación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaSuscripcionAprobacionTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaSuscripcionAprobacionTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de suscripción.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaRegistroBajaTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaRegistroBajaTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de registro y baja contable.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaTab3.Text.Length > 0)
            {
                if (_Metodo.ValidaFecha(txtFechaTab3.Text) == false)
                {
                    string script = "<script>alert('Formato no valido de fecha de Liq. final.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaRecepcionTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaTab3.Text) < Convert.ToDateTime(txtFechaRecepcionTab2.Text))
                {
                    string script = "<script>alert('La fecha de Liq. final debe ser mayor o igual a la fecha de recepción del proyecto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }

                if (txtFechaActaTransferenciaTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaTab3.Text) > Convert.ToDateTime(txtFechaActaTransferenciaTab3.Text))
                {
                    string script = "<script>alert('La fecha de Liq. final debe ser menor o igual a la fecha de acta de transferencia fisica.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            return result;
        }

        protected void LnkbtnLiquidacion_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnLiquidacionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnActaTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnActaTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnActaTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnLiquidacionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
                Response.End();
            }

        }

        protected void LnkbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
                Response.End();
            }

        }

        protected void imgbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        {


            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + LnkbtnDevolucionTab3.Text);
                Response.End();
            }

        }

        //protected void imgbtnDevolucionTab3_OnClik(object sender, EventArgs e)
        //{


        //    string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
        //    FileInfo toDownload = new FileInfo(pat + LnkbtnDevolucionTab3.Text);
        //    if (toDownload.Exists)
        //    {
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
        //        Response.AddHeader("Content-Length", toDownload.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(pat + LnkbtnLiquidacionTab3.Text);
        //        Response.End();
        //    }

        //}







        #endregion

        #region Tab4
        protected void CargaTab4()
        {
            cargaParalizacionTab4();
            cargaPlazoTAb4();
            cargaPersupuestoTab4();
            cargaDeductivoTab4();
            //cargaCalidadTab4();
            // cargaDatosCalidadTab4();

            //cargaEstadoPruebaTab4();
            //cargaResultadoFinalTab4();
            //cargarCertificacion();

        }

        protected void CargaTipoCertificacion(DropDownList ddl)
        {
            ddl.DataSource = _objBLEjecucion.F_spMON_TipoCertificacion();
            ddl.DataValueField = "valor";
            ddl.DataTextField = "nombre";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));


        }


        protected void cargaPlazoTAb4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 1;

            grdPlazoTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPlazoTab4.DataBind();

        }

        protected void grdPlazoTab4_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPlaTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(lblTotalDias.Text) + Convert.ToInt32(dias.Text);

                GeneraIcoFile(imb.ToolTip, imb);

                lblTotalDias.Text = TotalDias.ToString();
            }


        }

        protected void cargaPersupuestoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 2;

            DataTable dt = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdPresupuestoTab4.DataSource = dt;
            grdPresupuestoTab4.DataBind();

            string MontoTotal = (dt.Compute("Sum(montoPresup)", "").ToString());

            if (dt.Rows.Count > 0)
            {
                lblPresupuestoAdicional.Text = Convert.ToDecimal(MontoTotal).ToString("N");
                hfTotalAdicional.Value = MontoTotal;
            }
            else
            {
                lblPresupuestoAdicional.Text = "0.00";
            }
        }

        protected void btnModificarPlazoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPlazoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Ampliacion_Plazos.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;
                //_BEAmpliacion.ampliacion = txtAmpPlazoPlaTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);

                int dias = (Convert.ToDateTime(txtFechaFin.Text) - Convert.ToDateTime(txtFechaInicio.Text)).Days + 1;
                _BEAmpliacion.ampliacion = dias.ToString();

                if (FileUploadPlaTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPlaTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    //OBTENER FECHA DE INICIO
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.id_tipoModalidad = 4;
                    BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                    _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                    DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                    //DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                    int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                    int ampliacion = _EntidadMaxValores.diasReal;

                    //MODIFICAR PROGRAMACIÓN
                    int iPlazo = 0;
                    iPlazo = plazoEjecucion + ampliacion;

                    GeneraProgramacion(iPlazo, dtFechaInicioProgramacion);

                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtAmpPlazoPlaTab4.Text = "";
                    txtResolAproPlaTab4.Text = "";
                    txtFechaResolPlaTab4.Text = "";
                    txtConceptoPlaTab4.Text = "";
                    txtObservacionPlaTab4.Text = "";
                    txtFechaInicio.Text = "";
                    txtFechaFin.Text = "";
                    LnkbtnPlaTab4.Text = "";
                    imgbtnPlaTab4.Visible = false;
                    imgAgregaPlazoTab4.Visible = true;

                    lblTotalDias.Text = "0";
                    //    cargaPlazoTAb4();

                    Panel_PlazoTab4.Visible = false;
                    cargaPlazoTAb4();
                    ActualizaFechasTab1();
                    //btnModificarPlazoTab4.Visible = true;
                    Up_Tab4.Update();

                    cargaProgramacionTab2();
                    CargaDatosTab2();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }



            }
        }

        protected void btnModificarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarPresupuestoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Adicional.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;
                _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                _BEAmpliacion.observacion = txtObsPreTab4.Text;

                if (FileUploadPreTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnPreTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Presupuesto_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);



                    txtMontoPreTab4.Text = "";
                    txtVincuPreTab4.Text = "";
                    txtResolAproPreTab4.Text = "";
                    txtFechaPreTab4.Text = "";
                    txtIncidenciaPreTab4.Text = "";
                    LnkbtnPreTab4.Text = "";
                    txtConceptoPreTab4.Text = "";
                    txtObsPreTab4.Text = "";
                    imgbtnPreTab4.Visible = false;
                    imbtnAgregarPresupuestoTab4.Visible = true;

                    cargaPersupuestoTab4();

                    // Actualizar Avance Fisico
                    CargaAvanceFisico();
                    Up_Tab2.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_PresupuestoTab4.Visible = false;
                cargaPersupuestoTab4();
                btnModificarPresupuestoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void btnModificarDeductivoTab4_OnClick(object sender, EventArgs e)
        {

            if (ValidarDeductivoTab4())
            {

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_Registro_Presupuesto_Deductivo.Text);
                //_BEFinanciamiento.tipoFinanciamiento = 3;

                _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                _BEAmpliacion.observacion = txtObsDedTab4.Text;

                if (FileUploadDedTab4.PostedFile.ContentLength > 0)
                {
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                }
                else
                {
                    _BEAmpliacion.urlDoc = LnkbtnDedTab4.Text;
                }

                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                int resul;
                resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Deductivo_Editar(_BEAmpliacion);
                if (resul == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    txtMontoDedTab4.Text = "";
                    txtVincuDedTab4.Text = "";
                    txtResolAproDedTab4.Text = "";
                    txtFechaDedTab4.Text = "";
                    txtIncidenciaDedTab4.Text = "";

                    txtConceptoDedTab4.Text = "";
                    txtObsDedTab4.Text = "";
                    LnkbtnDedTab4.Text = "";
                    imgbtnDedTab4.Visible = false;
                    imgbtnDeductivoTab4.Visible = true;
                    cargaDeductivoTab4();

                    // Actualizar Avance Fisico
                    CargaAvanceFisico();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

                Panel_deductivoTab4.Visible = false;
                cargaDeductivoTab4();
                btnModificarDeductivoTab4.Visible = true;
                Up_Tab4.Update();

            }
        }

        protected void grdPresupuestoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocPreTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        protected void cargaDeductivoTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 3;

            DataTable dt = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdDeductivoTab4.DataSource = dt;
            grdDeductivoTab4.DataBind();

            string MontoTotal = (dt.Compute("Sum(montoPresup)", "").ToString());

            if (dt.Rows.Count > 0)
            {
                lblDeductivoTotal.Text = Convert.ToDecimal(MontoTotal).ToString("N");
                hfTotalDeductivo.Value = MontoTotal;
            }
            else
            {
                lblDeductivoTotal.Text = "0.00";
            }

        }
        protected void grdDeductivoTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocDedTab4");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void imgAgregaPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            btnGuardarPlazoTab4.Visible = true;
            btnModificarPlazoTab4.Visible = false;
            lblNomUsuarioAmpDias.Text = "";
            txtAmpPlazoPlaTab4.Enabled = true;
            Up_Tab4.Update();
        }

        protected void btnCancelarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PlazoTab4.Visible = false;
            imgAgregaPlazoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPlazoTab4()
        {
            Boolean result;
            result = true;


            if (txtFechaInicio.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaInicio.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de inicio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaFin.Text == "")
            {
                string script = "<script>alert('Ingrese fecha fin.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaFin.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha fin.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaFin.Text) < Convert.ToDateTime(txtFechaInicio.Text))
            {
                string script = "<script>alert('La fecha fin debe ser mayor o igual a la fecha de inicio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            //if (txtAmpPlazoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese dias de Ampliación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtResolAproPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaResolPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtConceptoPlaTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPlazoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPlazoTab4())
            {

                if (validaArchivo(FileUploadPlaTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 1;

                    _BEAmpliacion.resolAprob = txtResolAproPlaTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolPlaTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoPlaTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionPlaTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPlaTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);

                    int dias = (Convert.ToDateTime(txtFechaFin.Text) - Convert.ToDateTime(txtFechaInicio.Text)).Days + 1;

                    _BEAmpliacion.ampliacion = dias.ToString();

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 4;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        //DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion + ampliacion;

                        GeneraProgramacion(iPlazo, dtFechaInicioProgramacion);

                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        lblTotalDias.Text = "0";
                        cargaPlazoTAb4();
                        ActualizaFechasTab1();
                        txtAmpPlazoPlaTab4.Text = "";
                        txtResolAproPlaTab4.Text = "";
                        txtFechaResolPlaTab4.Text = "";
                        txtConceptoPlaTab4.Text = "";
                        txtObservacionPlaTab4.Text = "";
                        txtFechaInicio.Text = "";
                        txtFechaFin.Text = "";

                        Panel_PlazoTab4.Visible = false;
                        imgAgregaPlazoTab4.Visible = true;

                        Up_Tab4.Update();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPlaTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPlazoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPlazoTab4.Rows[row.RowIndex].FindControl("imgDocPlaTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imbtnAgregarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            lblNomUsuarioPresupuesto.Text = "";
            btnGuardarPresupuestoTab4.Visible = true;
            btnModificarPresupuestoTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void btnCancelarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_PresupuestoTab4.Visible = false;
            imbtnAgregarPresupuestoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarPresupuestoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaPreTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoPreTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarPresupuestoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarPresupuestoTab4())
            {

                if (validaArchivo(FileUploadPreTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 2;

                    _BEAmpliacion.montoPresup = txtMontoPreTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuPreTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproPreTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaPreTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaPreTab4.Text;

                    _BEAmpliacion.concepto = txtConceptoPreTab4.Text;
                    _BEAmpliacion.observacion = txtObsPreTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadPreTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);


                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaPersupuestoTab4();

                        txtMontoPreTab4.Text = "";
                        txtVincuPreTab4.Text = "";
                        txtResolAproPreTab4.Text = "";
                        txtFechaPreTab4.Text = "";
                        txtIncidenciaPreTab4.Text = "";

                        txtConceptoPreTab4.Text = "";
                        txtObsPreTab4.Text = "";

                        Panel_PresupuestoTab4.Visible = false;
                        imbtnAgregarPresupuestoTab4.Visible = true;

                        Up_Tab4.Update();

                        // Actualizar Avance Fisico
                        CargaAvanceFisico();
                        Up_Tab2.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocPreTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdPresupuestoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdPresupuestoTab4.Rows[row.RowIndex].FindControl("imgDocPreTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void imgbtnDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            btnGuardarDeductivoTab4.Visible = true;
            btnModificarDeductivoTab4.Visible = false;
            lblNomUsuarioDeductivo.Text = "";
            Up_Tab4.Update();
        }

        protected void btnCancelarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            Panel_deductivoTab4.Visible = false;
            imgbtnDeductivoTab4.Visible = true;
            Up_Tab4.Update();
        }

        protected Boolean ValidarDeductivoTab4()
        {
            Boolean result;
            result = true;

            if (txtMontoDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtVincuDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Vinculació.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}


            //if (txtResolAproDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            //if (txtFechaDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Fecha de Resolución de Aprobación.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtIncidenciaDedTab4.Text == "")
            {
                string script = "<script>alert('Ingrese % de Incidencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            //if (txtConceptoDedTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese Concepto.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            return result;

        }

        protected void btnGuardarDeductivoTab4_OnClick(object sender, EventArgs e)
        {
            if (ValidarDeductivoTab4())
            {

                if (validaArchivo(FileUploadDedTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 3;

                    _BEAmpliacion.montoPresup = txtMontoDedTab4.Text;
                    _BEAmpliacion.vinculacion = txtVincuDedTab4.Text;
                    _BEAmpliacion.resolAprob = txtResolAproDedTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaDedTab4.Text);
                    _BEAmpliacion.porcentajeIncidencia = txtIncidenciaDedTab4.Text;

                    _BEAmpliacion.concepto = txtConceptoDedTab4.Text;
                    _BEAmpliacion.observacion = txtObsDedTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUploadDedTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicio.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFin.Text);

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        cargaDeductivoTab4();

                        txtMontoDedTab4.Text = "";
                        txtVincuDedTab4.Text = "";
                        txtResolAproDedTab4.Text = "";
                        txtFechaDedTab4.Text = "";
                        txtIncidenciaDedTab4.Text = "";

                        txtConceptoDedTab4.Text = "";
                        txtObsDedTab4.Text = "";

                        Panel_deductivoTab4.Visible = false;
                        imgbtnDeductivoTab4.Visible = true;

                        Up_Tab4.Update();

                        // Actualizar Avance Fisico
                        CargaAvanceFisico();
                        Up_Tab2.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }

        protected void imgDocDedTab4_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdDeductivoTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdDeductivoTab4.Rows[row.RowIndex].FindControl("imgDocDedTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected void grdCalidadTab4_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocVerificadaTab4");
                GeneraIcoFile(imb.ToolTip, imb);

            }
        }

        protected void imgbtnPlaTabb4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPlaTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnPreTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnPreTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnDedTab4_OnClick(object sender, EventArgs e)
        {
            LinkButton boton;
            boton = (LinkButton)sender;

            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            string urlDocumento = LnkbtnDedTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void grdPlazoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPlazoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    lblTotalDias.Text = "0";
                    cargaPlazoTAb4();
                    ActualizaFechasTab1();
                    Up_Tab4.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPresupuestoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPresupuestoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaPersupuestoTab4();
                    Up_Tab4.Update();

                    // Actualizar Avance Fisico
                    CargaAvanceFisico();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected void grdDeductivoTab4_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdDeductivoTab4.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEAmpliacion.id_ampliacion = Convert.ToInt32(objTemp.ToString());
                //_BEFinaTra.tipo = 2;
                _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLAmpliacion.D_MON_Ampliacion(_BEAmpliacion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDeductivoTab4();
                    Up_Tab4.Update();

                    // Actualizar Avance Fisico
                    CargaAvanceFisico();
                    Up_Tab2.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }

        protected void grdPlazoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPlazoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Ampliacion_Plazos.Text = id_ampliacion.ToString();
            GridViewRow row = grdPlazoTab4.SelectedRow;

            txtAmpPlazoPlaTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            //txtAmpPlazoPlaTab4.Enabled = false;
            txtResolAproPlaTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolPlaTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoPlaTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionPlaTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            LnkbtnPlaTab4.Text = ((ImageButton)row.FindControl("imgDocPlaTab4")).ToolTip;
            lblNomUsuarioAmpDias.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicio.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFin.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(LnkbtnPlaTab4.Text, imgbtnPlaTab4);

            btnGuardarPlazoTab4.Visible = false;
            btnModificarPlazoTab4.Visible = true;
            imgAgregaPlazoTab4.Visible = false;
            Panel_PlazoTab4.Visible = true;
            Up_Tab4.Update();


        }
        protected void grdPresupuestoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdPresupuestoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Adicional.Text = id_ampliacion.ToString();
            GridViewRow row = grdPresupuestoTab4.SelectedRow;

            txtMontoPreTab4.Text = Server.HtmlDecode(row.Cells[3].Text);
            txtVincuPreTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproPreTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaPreTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaPreTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;
            txtConceptoPreTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsPreTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioPresupuesto.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            LnkbtnPreTab4.Text = ((ImageButton)row.FindControl("imgDocPreTab4")).ToolTip;

            GeneraIcoFile(LnkbtnPreTab4.Text, imgbtnPreTab4);


            btnGuardarPresupuestoTab4.Visible = false;
            btnModificarPresupuestoTab4.Visible = true;
            imbtnAgregarPresupuestoTab4.Visible = false;
            Panel_PresupuestoTab4.Visible = true;

            Up_Tab4.Update();


        }
        protected void grdDeductivoTab4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string id_ampliacion;
            id_ampliacion = grdDeductivoTab4.SelectedDataKey.Value.ToString();
            lblId_Registro_Presupuesto_Deductivo.Text = id_ampliacion.ToString();
            GridViewRow row = grdDeductivoTab4.SelectedRow;

            txtMontoDedTab4.Text = Server.HtmlDecode(row.Cells[2].Text);
            txtVincuDedTab4.Text = ((Label)row.FindControl("lblVinculacion")).Text;
            txtResolAproDedTab4.Text = ((Label)row.FindControl("lblResolAprob")).Text;
            txtFechaDedTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtIncidenciaDedTab4.Text = ((Label)row.FindControl("lblIncidencia")).Text;

            txtConceptoDedTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObsDedTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;

            lblNomUsuarioDeductivo.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            LnkbtnDedTab4.Text = ((ImageButton)row.FindControl("imgDocDedTab4")).ToolTip;

            GeneraIcoFile(LnkbtnDedTab4.Text, imgbtnDedTab4);


            btnGuardarDeductivoTab4.Visible = false;
            btnModificarDeductivoTab4.Visible = true;
            imgbtnDeductivoTab4.Visible = false;
            Panel_deductivoTab4.Visible = true;

            Up_Tab4.Update();


        }
        protected void cargaParalizacionTab4()
        {
            _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEAmpliacion.tipoFinanciamiento = 3;
            _BEAmpliacion.id_tipo_ampliacion = 6;

            grdParalizacionTab4.DataSource = _objBLAmpliacion.sp_MON_Ampliacion(_BEAmpliacion);
            grdParalizacionTab4.DataBind();

        }


        protected void grdParalizacionTab4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int TotalDias;
            TotalDias = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocParalizacionTab4");
                Label dias = (Label)e.Row.FindControl("lblPlazo");

                TotalDias = Convert.ToInt32(txtTotalParalizacionTab4.Text) + Convert.ToInt32(dias.Text);
                txtTotalParalizacionTab4.Text = TotalDias.ToString();

                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void grdParalizacionTab4_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_ParalizacionTab4.Text = grdParalizacionTab4.SelectedDataKey.Value.ToString();
            GridViewRow row = grdParalizacionTab4.SelectedRow;

            txtDiasParalizadosTab4.Text = ((Label)row.FindControl("lblPlazo")).Text;
            //txtDiasParalizadosTab4.Enabled = false;
            txtResoluciónParalizacionTab4.Text = ((Label)row.FindControl("lblAprobación")).Text;
            txtFechaResolucionParalizacionTab4.Text = ((Label)row.FindControl("lblFechaResol")).Text;
            txtConceptoParalizacionTab4.Text = ((Label)row.FindControl("lblConcepto")).Text;
            txtObservacionParalizacionTab4.Text = ((Label)row.FindControl("lblObservaciones")).Text;
            lnkbtnParalizacionTab4.Text = ((ImageButton)row.FindControl("imgDocParalizacionTab4")).ToolTip;
            lblNomActualizaParalizacionTab4.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;
            txtFechaInicioParalizacionTab4.Text = ((Label)row.FindControl("lblfechainicio")).Text;
            txtFechaFinParalizacionTab4.Text = ((Label)row.FindControl("lblfechaFin")).Text;

            GeneraIcoFile(lnkbtnParalizacionTab4.Text, imgbtnParalizacionTab4);

            btnGuardarParalizacionTab4.Visible = false;
            btnModificarParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;
            Panel_ParalizacionTab4.Visible = true;

            Up_Tab4.Update();
        }

        protected void grdParalizacionTab4_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void lnkbtnParalizacionTab4_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = lnkbtnParalizacionTab4.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }


        protected Boolean ValidarParalizacionTab4()
        {
            Boolean result;
            result = true;

            //if (txtDiasParalizadosTab4.Text == "")
            //{
            //    string script = "<script>alert('Ingrese dias paralizados.');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;

            //}

            if (txtFechaInicioParalizacionTab4.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaInicioParalizacionTab4.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha de inicio de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaInicioReal.Text.Length > 0 && Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text) < Convert.ToDateTime(txtFechaInicioReal.Text))
            {
                string script = "<script>alert('La Fecha de inicio de paralización debe ser mayor o igual a la fecha de inicio real.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaFinParalizacionTab4.Text == "")
            {
                string script = "<script>alert('Ingrese fecha fin de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaFinParalizacionTab4.Text) == false)
            {
                string script = "<script>alert('Formato no valido de Fecha fin de paralización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) < Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text))
            {
                string script = "<script>alert('La fecha fin debe ser mayor o igual a la fecha de inicio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtTerminoObraTab2.Text.Length > 0 && Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) > Convert.ToDateTime(txtTerminoObraTab2.Text))
            {
                string script = "<script>alert('La Fecha de inicio de paralización debe ser menor o igual a la fecha de termino de obra.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }

        protected void btnGuardarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {

                if (validaArchivo(FileUpload_ParalizacionTab4))
                {

                    _BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEAmpliacion.tipoFinanciamiento = 3;
                    _BEAmpliacion.id_tipo_ampliacion = 6;

                    //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                    _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;
                    _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ParalizacionTab4);
                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                    int dias = (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days + 1;
                    _BEAmpliacion.ampliacion = dias.ToString();

                    int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (val == 1)
                    {
                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 4;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        //DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion + ampliacion;

                        GeneraProgramacion(iPlazo, dtFechaInicioProgramacion);

                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        txtTotalParalizacionTab4.Text = "0";
                        cargaParalizacionTab4();
                        //ActualizaFechasTab1();
                        //txtAmpPlazoPlaTab4.Text = "";
                        //txtResolAproPlaTab4.Text = "";
                        //txtFechaResolPlaTab4.Text = "";
                        //txtConceptoPlaTab4.Text = "";
                        //txtObservacionPlaTab4.Text = "";
                        //txtFechaInicio.Text = "";
                        //txtFechaFin.Text = "";

                        Panel_ParalizacionTab4.Visible = false;
                        imgbtnAgregarParalizacionTAb4.Visible = true;

                        Up_Tab4.Update();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }

        protected void GeneraProgramacion(int iPlazo, DateTime dtFechaInicio)
        {
            if (iPlazo == 1)
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.tipoFinanciamiento = 3;
                _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                _BEEjecucion.fisicoProgramado = "0";
                _BEEjecucion.financieroProgramado = "0";
                _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(1);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                iPlazo = 0;
            }
            else
            {
                if (iPlazo > 0)
                {
                    iPlazo = iPlazo - 1;
                }

                while (iPlazo > 0)
                {
                    int iDiasMes = System.DateTime.DaysInMonth(dtFechaInicio.Year, dtFechaInicio.Month) - dtFechaInicio.Day;


                    if (iDiasMes < iPlazo)
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                        _BEEjecucion.fisicoProgramado = "0";
                        _BEEjecucion.financieroProgramado = "0";
                        _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                        _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iDiasMes);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                        dtFechaInicio = dtFechaInicio.AddDays(iDiasMes + 1);

                        if (iPlazo - iDiasMes == 1)
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                            _BEEjecucion.fisicoProgramado = "0";
                            _BEEjecucion.financieroProgramado = "0";
                            _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                            _BEEjecucion.Date_fechaFin = dtFechaInicio;
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int valFin = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);
                        }

                        iPlazo = iPlazo - iDiasMes - 1;

                    }
                    else
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                        _BEEjecucion.fisicoProgramado = "0";
                        _BEEjecucion.financieroProgramado = "0";
                        _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                        _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iPlazo);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                        int val2 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                        iPlazo = iPlazo - iPlazo;
                    }

                }

                if (iPlazo <= 0)
                {
                    //iPlazo = iPlazo - 1;
                    DateTime NewFecha = dtFechaInicio.AddMonths(1);

                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.dtFechaProgramada = NewFecha;
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    int val3 = _objBLEjecucion.D_spd_MON_CronogramaProgramacion(_BEEjecucion);
                }
            }
        }

        protected void btnModificarParalizacionTab4_Click(object sender, EventArgs e)
        {
            if (ValidarParalizacionTab4())
            {
                if (validaArchivo(FileUpload_ParalizacionTab4))
                {
                    _BEAmpliacion.id_ampliacion = Convert.ToInt32(lblId_ParalizacionTab4.Text);
                    //_BEAmpliacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    //_BEAmpliacion.tipoFinanciamiento = 3;
                    //_BEAmpliacion.id_tipo_ampliacion = 6;

                    //_BEAmpliacion.ampliacion = txtDiasParalizadosTab4.Text;
                    _BEAmpliacion.resolAprob = txtResoluciónParalizacionTab4.Text;
                    _BEAmpliacion.Date_fechaResolucion = VerificaFecha(txtFechaResolucionParalizacionTab4.Text);
                    _BEAmpliacion.concepto = txtConceptoParalizacionTab4.Text;
                    _BEAmpliacion.observacion = txtObservacionParalizacionTab4.Text;

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEAmpliacion.Date_fechaInicio = VerificaFecha(txtFechaInicioParalizacionTab4.Text);
                    _BEAmpliacion.Date_FechaFin = VerificaFecha(txtFechaFinParalizacionTab4.Text);

                    int dias = (Convert.ToDateTime(txtFechaFinParalizacionTab4.Text) - Convert.ToDateTime(txtFechaInicioParalizacionTab4.Text)).Days + 1;
                    _BEAmpliacion.ampliacion = dias.ToString();

                    if (FileUpload_ParalizacionTab4.PostedFile.ContentLength > 0)
                    {
                        _BEAmpliacion.urlDoc = _Metodo.uploadfile(FileUpload_ParalizacionTab4);
                    }
                    else
                    {
                        _BEAmpliacion.urlDoc = lnkbtnParalizacionTab4.Text;
                    }

                    _BEAmpliacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


                    int resul;
                    resul = _objBLAmpliacion.spud_MON_Seguimiento_Ampliacion_Editar(_BEAmpliacion);

                    //int val = _objBLAmpliacion.spi_MON_Ampliacion(_BEAmpliacion);

                    if (resul == 1)
                    {

                        //OBTENER FECHA DE INICIO
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.id_tipoModalidad = 4;
                        BE_MON_Ejecucion _EntidadMaxValores = new BE_MON_Ejecucion();
                        _EntidadMaxValores = _objBLEjecucion.F_MON_MaxValoresProgramacion(_BEEjecucion);

                        DateTime dtFechaInicioProgramacion = _EntidadMaxValores.Date_fechaInicioReal;
                        //DateTime dtFechaInicio = _EntidadMaxValores.Date_fechaInicio;
                        int plazoEjecucion = _EntidadMaxValores.plazoEjecuccion;
                        int ampliacion = _EntidadMaxValores.diasReal;

                        //MODIFICAR PROGRAMACIÓN
                        int iPlazo = 0;
                        iPlazo = plazoEjecucion + ampliacion;

                        GeneraProgramacion(iPlazo, dtFechaInicioProgramacion);

                        string script = "<script>alert('Registro Correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        Panel_ParalizacionTab4.Visible = false;
                        imgbtnAgregarParalizacionTAb4.Visible = true;

                        txtTotalParalizacionTab4.Text = "0";
                        cargaParalizacionTab4();
                        ActualizaFechasTab1();

                        Up_Tab4.Update();

                        cargaProgramacionTab2();
                        CargaDatosTab2();
                        Up_Tab2.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }
        }
        protected void btnCancelarParalizacionTab4_Click(object sender, EventArgs e)
        {
            Panel_ParalizacionTab4.Visible = false;
            imgbtnAgregarParalizacionTAb4.Visible = true;

            Up_Tab4.Update();
        }

        protected void imgbtnAgregarParalizacionTAb4_Click(object sender, ImageClickEventArgs e)
        {

            Panel_ParalizacionTab4.Visible = true;
            imgbtnAgregarParalizacionTAb4.Visible = false;
            //lblNomUsuarioPresupuesto.Text = "";
            txtDiasParalizadosTab4.Enabled = true;
            btnGuardarParalizacionTab4.Visible = true;
            btnModificarParalizacionTab4.Visible = false;
            Up_Tab4.Update();
        }

        protected void imgDocParalizacionTab4_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            int id_ampliacion = Convert.ToInt32(grdParalizacionTab4.DataKeys[row.RowIndex].Values[0].ToString());

            ImageButton url = (ImageButton)grdParalizacionTab4.Rows[row.RowIndex].FindControl("imgDocParalizacionTab4");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        #endregion

        #region Tab6

        protected void CargaTab6()
        {
            CargaPanelTab6();
            CargaTipoDocumentoMonitorTab6();
        }

        protected void CargaTipoDocumentoMonitorTab6()
        {
            ddlTipoArchivoTab6.DataSource = _objBLBandeja.F_spMON_TipoDocumentoMonitor();
            ddlTipoArchivoTab6.DataValueField = "valor";
            ddlTipoArchivoTab6.DataTextField = "nombre";
            ddlTipoArchivoTab6.DataBind();

            ddlTipoArchivoTab6.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void CargaPanelTab6()
        {
            _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdPanelTab6.DataSource = _objBLBandeja.F_spMON_DocumentoMonitor(_BEBandeja);
            grdPanelTab6.DataBind();
        }

        protected void imgbtnAgregarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnGuardarPanelTab6.Visible = true;
            btnModificarPanelTab6.Visible = false;
            lblNomUsuarioDocumento.Text = "";

            txtNombreArchivoTab6.Text = "";
            txtDescripcionTab6.Text = "";
            txtFechaTab6.Text = "";
            LnkbtnDocTab6.Text = "";
            ddlTipoArchivoTab6.SelectedValue = "";
            imgbtnDocTab6.ImageUrl = "~/img/blanco.png";

            Up_Tab6.Update();
        }

        protected void btnCancelarPanelTab6_OnClick(object sender, EventArgs e)
        {
            Panel_FotograficoTab6.Visible = false;
            imgbtnAgregarPanelTab6.Visible = true;
            Up_Tab6.Update();
        }

        protected void imgDocTab6_OnClick(object sender, EventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();


            ImageButton url = (ImageButton)grdPanelTab6.Rows[row.RowIndex].FindControl("imgDocTab6");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void btnGuardarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {

                if (validaArchivo(FileUpload_Tab6, 400))
                {
                    _BEBandeja.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;
                    _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLBandeja.spi_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Registro correcto.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;

                        txtNombreArchivoTab6.Text = "";
                        txtDescripcionTab6.Text = "";
                        ddlTipoArchivoTab6.SelectedValue = "";
                        txtFechaTab6.Text = "";
                        imgbtnDocTab6.ImageUrl = "~/img/blanco.png";
                        Up_Tab6.Update();


                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }
            }

        }
        protected void grdPanelTab6_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocTab6");

                GeneraIcoFile(imb.ToolTip, imb);

            }
        }


        protected void grdPanelTab6_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdPanelTab6.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEBandeja.Id_documentoMonitor = Convert.ToInt32(objTemp.ToString());
                _BEBandeja.tipo = "2";
                _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);

                int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaPanelTab6();
                    Up_Tab6.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }

        }


        protected void grdPanelTab6_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            lblId_documento_monitor.Text = grdPanelTab6.SelectedDataKey.Value.ToString();

            GridViewRow row = grdPanelTab6.SelectedRow;

            txtNombreArchivoTab6.Text = ((Label)row.FindControl("lblNombre")).Text;
            txtDescripcionTab6.Text = ((Label)row.FindControl("lblDescripcion")).Text;

            ddlTipoArchivoTab6.SelectedValue = ((Label)row.FindControl("lblIDTipo")).Text;

            txtFechaTab6.Text = ((Label)row.FindControl("lblFecha")).Text;
            LnkbtnDocTab6.Text = ((ImageButton)row.FindControl("imgDocTab6")).ToolTip;

            lblNomUsuarioDocumento.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            GeneraIcoFile(LnkbtnDocTab6.Text, imgbtnDocTab6);

            Panel_FotograficoTab6.Visible = true;
            imgbtnAgregarPanelTab6.Visible = false;
            btnModificarPanelTab6.Visible = true;
            btnGuardarPanelTab6.Visible = false;
            Up_Tab6.Update();
        }

        protected Boolean validarPanelTab6()
        {
            Boolean result;
            result = true;

            if (txtNombreArchivoTab6.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (ddlTipoArchivoTab6.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (txtFechaTab6.Text == "")
            {
                string script = "<script>alert('Ingresar fecha.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (FileUpload_Tab6.HasFile == false && LnkbtnDocTab6.Text == "")
            {
                string script = "<script>alert('Adjuntar archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }
            else
            {
                if (ddlTipoArchivoTab6.SelectedValue.Equals("1")) //Validar Fotos
                {
                    if (validaArchivoFotos(FileUpload_Tab6) == false)
                    {
                        result = false;
                        return result;
                    }
                }
            }

            return result;
        }

        protected void btnModificarPanelTab6_OnClick(object sender, EventArgs e)
        {
            if (validarPanelTab6())
            {
                if (validaArchivo(FileUpload_Tab6))
                {
                    _BEBandeja.Id_documentoMonitor = Convert.ToInt32(lblId_documento_monitor.Text);
                    _BEBandeja.Id_tipo = Convert.ToInt32(ddlTipoArchivoTab6.SelectedValue);
                    _BEBandeja.NombreArchivo = txtNombreArchivoTab6.Text;

                    _BEBandeja.Descripcion = txtDescripcionTab6.Text;
                    _BEBandeja.Date_fecha = VerificaFecha(txtFechaTab6.Text);
                    _BEBandeja.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    _BEBandeja.tipo = "1";

                    if (FileUpload_Tab6.HasFile)
                    {
                        _BEBandeja.UrlDoc = _Metodo.uploadfile(FileUpload_Tab6);
                    }
                    else
                    {
                        _BEBandeja.UrlDoc = LnkbtnDocTab6.Text;
                    }


                    int val = _objBLBandeja.spud_MON_DocumentoMonitor(_BEBandeja);

                    if (val == 1)
                    {
                        string script = "<script>alert('Se actualizó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaPanelTab6();
                        Panel_FotograficoTab6.Visible = false;
                        imgbtnAgregarPanelTab6.Visible = true;
                        Up_Tab6.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }
                }
            }



        }

        protected void LnkbtnDocTab6_OnClick(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            string urlDocumento = LnkbtnDocTab6.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        #endregion


        protected void chbFlagAyuda_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox boton;
            GridViewRow row;
            boton = (CheckBox)sender;
            row = (GridViewRow)boton.NamingContainer;

            var valor = grdEvaluacionTab2.DataKeys[row.RowIndex].Value;
            //_BEEjecucion.id_tabla = Convert.ToInt32(valor.ToString());
            Boolean FlagAyuda = ((CheckBox)row.FindControl("chbFlagAyuda")).Checked;

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(valor.ToString());
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);


            if (FlagAyuda == true)
            {
                _BEEjecucion.flagAyuda = "1";
            }
            else
            {
                _BEEjecucion.flagAyuda = "0";
            }

            int val;
            try
            {
                val = _objBLEjecucion.spu_MON_EvaluacionRecomendacionByFlag(_BEEjecucion);
            }
            catch (Exception ex)
            {
                val = 0;
            }



            if (val == 1)
            {
                string script = "<script>alert('Se Actualizó Correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                cargarEvaluacionTab2();

                txtFechaVisitaTab2.Text = "";
                txtEvaluacionTab2.Text = "";
                txtRecomendacionTab2.Text = "";
                txtProgramadoTab2.Text = "";
                txtEjecutadoTab2.Text = "";
                ddlTipoMonitoreo.SelectedValue = "";
                txtNroTramie.Text = "";
                txtTipoDocumento.Text = "";
                ddlTipoDocumento.SelectedValue = "";
                txtAsunto.Text = "";
                chbAyudaMemoriaTab4.Checked = false;
                Panel_ObservacionTab2.Visible = false;
                imgbtnAgregarEvaluacionTab2.Visible = true;
                cargarEvaluacionTab2();

                Up_Tab2.Update();

            }

            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }



        }

        protected void btnPresupuestoPNVRTab0_Click(object sender, EventArgs e)
        {
            //string script = "<script>window.open('PresupuestoPNVR.aspx?id=" + LblID_PROYECTO.Text + "','_blank','width=800,height=900,scrollbars=yes') </script>";

            string token = Session["clave"].ToString();
            string UrlFrontEnd = ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();
            //script = "<script>window.open('"+ script + "','_blank','width=800,height=900,scrollbars=yes') </script>";

            ifrmPresupuestoPNVR.Attributes["src"] = UrlFrontEnd + "presupuestopnvr/index/" + token + "/" + LblID_PROYECTO.Text;
            Up_PresupuestoPNVR.Update();

            //string script = "<script>ShowPopupRegistroActaVisita();</script>";
            string script = "<script>$('#modalPresupuestoPNVR').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //string script = "<script>window.open('PresupuestoPNVR.aspx?id=" + LblID_PROYECTO.Text + "','_blank') </script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected Boolean ValidarCuentaBancariaTab1()
        {
            Boolean result;
            result = true;

            if (ddlTipoBancoTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar banco.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtNroCuentaTab1.Text == "")
            {
                string script = "<script>alert('Ingresar número de cuenta.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaAperturaTab1.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de apertura.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaAperturaTab1.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de apertura.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaAprobacionExpTab0.Text.Length > 0)
            {
                if(Convert.ToDateTime(txtFechaAperturaTab1.Text)< Convert.ToDateTime(txtFechaAprobacionExpTab0.Text))
                {
                    string script = "<script>alert('La fecha de apertura debe ser mayor o igual a la fecha de aprobación del Exp. Técnico.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaOrientacionTab1.Text.Length > 0)
            {
                if (Convert.ToDateTime(txtFechaAperturaTab1.Text) > Convert.ToDateTime(txtFechaOrientacionTab1.Text))
                {
                    string script = "<script>alert('La fecha de apertura debe ser menor o igual a la fecha de sesion inducción.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }


            if (ddlDepaCuentaTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar departamento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlProvCuentaTab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar provincia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }
            if (ddlDistCuentatab1.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar distrito.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtAgenciaTab1.Text == "")
            {
                string script = "<script>alert('Ingresar nombre de agencia.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }
        protected void btnGuardarCuentaBancariaTab1_Click(object sender, EventArgs e)
        {
            if (ValidarCuentaBancariaTab1())
            {
                _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEProceso.id_tipo = Convert.ToInt32(ddlTipoBancoTab1.SelectedValue);
                _BEProceso.numeroCuenta = txtNroCuentaTab1.Text;
                _BEProceso.Date_fechaApertura = VerificaFecha(txtFechaAperturaTab1.Text);
                _BEProceso.cod_dist = ddlDepaCuentaTab1.SelectedValue + ddlProvCuentaTab1.SelectedValue + ddlDistCuentatab1.SelectedValue;
                _BEProceso.nombre = txtAgenciaTab1.Text;

                _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLProceso.IU_spiu_MON_CuentaBancariaNE(_BEProceso);

                if (val > 0)
                {
                    string script = "<script>alert('Se registró correctamente la cuenta bancaria del NE.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargaTab1();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected Boolean ValidarSesionesTab1()
        {
            string fecDesembolso = "",
                fecInduccion = txtFechaOrientacionTab1.Text,
                fecInicioProyecto = "";

            if (fecInduccion == "")
            { 
                return ImprimirMensaje("Ingresar fecha de sesion."); 
            }
            else if (_Metodo.ValidaFecha(fecInduccion) == false)
            { 
                return ImprimirMensaje("Formato no valido de fecha de sesion."); 
            }
            else if (txtFechaAperturaTab1.Text.Length>1 && Convert.ToDateTime(fecInduccion) < Convert.ToDateTime(txtFechaAperturaTab1.Text))
            {
                return ImprimirMensaje("La fecha de inducción debe ser mayor o igual a la fecha de apertura.");
            }
            else if (txtFechaEntregaTerrenoTab2.Text.Length > 1 && Convert.ToDateTime(fecInduccion) > Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text))
            {
                return ImprimirMensaje("La fecha de inducción debe ser menos o igual a la fecha de entrega de terreno.");
            }
            else if (txtAsistenteOrientacionHombreTab1.Text == "")
            { 
                return ImprimirMensaje("Ingresar número de asistentes hombres."); 
            }
            else if (txtAsistenteOrientacionMujerTab1.Text == "")
            { 
                return ImprimirMensaje("Ingresar número de asistentes mujeres."); 
            }
            else
            {
                return true;
            }
        }
        protected Boolean ImprimirMensaje(String mensaje)
        {
            string script = "<script>alert('" + mensaje + "');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            return false;
        }
        protected void btnGuardarSesionesTab1_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadActaOrientacionTab1))
            {
                if (ValidarSesionesTab1())
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.Date_fecha = VerificaFecha(txtFechaOrientacionTab1.Text);
                    _BEProceso.hombres = Convert.ToInt32(txtAsistenteOrientacionHombreTab1.Text);
                    _BEProceso.mujeres = Convert.ToInt32(txtAsistenteOrientacionMujerTab1.Text);

                    _BEProceso.urlDoc = FileUploadActaOrientacionTab1.HasFile ? _Metodo.uploadfile(FileUploadActaOrientacionTab1) : lnkbtnActaOrientacionTab1.Text;

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLProceso.IU_spiu_MON_SesionesNE(_BEProceso);

                    if (val > 0)
                    {
                        string script = "<script>alert('Se registró correctamente las sesiones de orientación e inducción del NE.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaTab1();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
        }
        protected void lnkbtnActaOrientacionTab1_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnActaOrientacionTab1.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void txtPlazoProyectoTab2_TextChanged(object sender, EventArgs e)
        {
            ActualizaFechasTab1();
        }

        protected Boolean ValidarRegistroGastos()
        {
            Boolean result;
            result = true;
            if (btnGuardarAvanceTab2.Visible == true)
            {
                if (ddlFechaProgramacionInformeTab2.SelectedValue == "")
                {
                    string script = "<script>alert('Seleccionar fecha de programación.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtMontoTab2.Text.Length == 0)
            {
                string script = "<script>alert('Primero debe ingresar el monto de valorización (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (lblCOD_SUBSECTOR.Text == "5")
            {
                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                DataTable dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {

                    string flag = (dt.Rows[0]["flagFinalizoPresupuesto"].ToString());

                    if (flag == "0")
                    {
                        string script = "<script>alert('Primero debe finalizar el registro presupuestal de la pestaña: I.Información.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }

                }
            }

            return result;
        }
        protected void btnFinancieroTab2_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroGastos())
            {
                lblNomMesFinanciamiento.Text = "";
                Up_MesAvanceFinancieroTab2.Update();

                //if (lblBooleanFinancieroTab2.Text == "true")
                //{
                if (Convert.ToDouble(txtTotalFinanciado.Text) > 0 && Convert.ToDouble(txtTotalGasto.Text) > 0)
                {

                }
                else
                {
                    CargaValorFinanciadoValorizacionTab2(Convert.ToInt32(lblId_Registro_Avance.Text));
                }

                btnGuardarAvanceFinancieroTab2.Visible = true;
                SumarAvanceFinancieroNE();
                Up_AvanceFinancieroTab2.Update();

                MPE_AvanceFinanciero.Show();

            }
        }
        protected void btnGuardarAvanceFinancieroTab2_Click(object sender, EventArgs e)
        {
            //    bool val = true;

            //    for (int i = 1; i <= 15; i++)
            //    {
            //        TextBox txtGasto = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtGasto" + i.ToString());

            //        if (txtGasto.ForeColor == System.Drawing.Color.Red)
            //        {
            //            val = false;
            //        }
            //    }

            //    if (val == false)
            //    {
            //        string script = "<script>alert('Los gastos ingresados son erróneos, verifique los montos en rojo.');</script>";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            //    }
            //    else
            //    {
            txtFinanRealTab2.Text = lblAvanceFinanciero.Text;

            ObtenerAvanceFisico();

            MPE_AvanceFinanciero.Hide();
            Up_Tab2.Update();
            //}
        }

        protected void LimpiarAvanceFinancieroNE()
        {
            for (int i = 1; i <= 15; i++)
            {

                TextBox txtFinanciado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtFinanciado" + i.ToString());
                TextBox txtAcumulado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtAcumulado" + i.ToString());
                TextBox txtGasto = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtGasto" + i.ToString());

                if (txtFinanciado.Text == "")
                {
                    txtFinanciado.Text = "0.00";
                }
                if (txtGasto.Text == "")
                {
                    txtGasto.Text = "0.00";
                }


            }

            txtSubFinanciado.Text = "0.00";
            txtSubGasto.Text = "0.00";
            txtSubAcumulado.Text = "0.00";


            txtTotalFinanciado.Text = "0.00";
            txtTotalGasto.Text = "0.00";
            txtTotalAcumulado.Text = "0.00";

            lblAvanceFinanciero.Text = "0.00";
            Up_AvanceFinancieroTab2.Update();
        }
        protected void SumarAvanceFinancieroNE()
        {
            double sumaSubFinan = 0;
            double sumaSubGasto = 0;
            double sumaSubAcumulado = 0;
            double sumaCostoDirecto = 0;
            double TotalFinan = 0;
            double TotalGasto = 0;
            double TotalAcumulado = 0;
            for (int i = 1; i <= 15; i++)
            {
                TextBox txtFinanciado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtFinanciado" + i.ToString());
                TextBox txtAcumulado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtAcumulado" + i.ToString());
                TextBox txtGasto = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtGasto" + i.ToString());

                if (txtFinanciado.Text == "")
                {
                    txtFinanciado.Text = "0.00";
                }
                if (txtGasto.Text == "")
                {
                    txtGasto.Text = "0.00";
                }

                if (i <= 7)
                {
                    sumaCostoDirecto = sumaCostoDirecto + Convert.ToDouble(txtFinanciado.Text);
                }

                if (i < 11)
                {
                    sumaSubFinan = sumaSubFinan + Convert.ToDouble(txtFinanciado.Text);
                    sumaSubGasto = sumaSubGasto + Convert.ToDouble(txtGasto.Text);
                    sumaSubAcumulado = sumaSubAcumulado + Convert.ToDouble(txtAcumulado.Text);
                }

                if (Convert.ToDouble(txtFinanciado.Text) < Convert.ToDouble(txtAcumulado.Text))
                {
                    txtAcumulado.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    txtAcumulado.ForeColor = System.Drawing.Color.Black;
                }


                if (btnGuardarAvanceTab2.Visible == true)
                {
                    if ((Convert.ToDouble(txtFinanciado.Text) - Convert.ToDouble(txtAcumulado.Text)) < Convert.ToDouble(txtGasto.Text))
                    {
                        txtGasto.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        txtGasto.ForeColor = System.Drawing.Color.Blue;
                    }
                }
                else
                {
                    if (btnModificarAvanceTab2.Visible == true)
                    {
                        if (Convert.ToDouble(txtFinanciado.Text) < Convert.ToDouble(txtGasto.Text))
                        {
                            txtGasto.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            txtGasto.ForeColor = System.Drawing.Color.Blue;
                        }
                    }
                    else
                    {
                        txtGasto.ForeColor = System.Drawing.Color.Black;
                    }
                }
            }
            txtSubFinanciado.Text = sumaSubFinan.ToString("N2");
            txtSubGasto.Text = sumaSubGasto.ToString("N2");
            txtSubAcumulado.Text = sumaSubAcumulado.ToString("N2");

            TotalFinan = sumaSubFinan + Convert.ToDouble(txtFinanciado11.Text) + Convert.ToDouble(txtFinanciado12.Text) + Convert.ToDouble(txtFinanciado13.Text) + Convert.ToDouble(txtFinanciado14.Text) + Convert.ToDouble(txtFinanciado15.Text);
            TotalGasto = sumaSubGasto + Convert.ToDouble(txtGasto11.Text) + Convert.ToDouble(txtGasto12.Text) + Convert.ToDouble(txtGasto13.Text) + Convert.ToDouble(txtGasto14.Text) + Convert.ToDouble(txtGasto15.Text);
            TotalAcumulado = sumaSubAcumulado + Convert.ToDouble(txtAcumulado11.Text) + Convert.ToDouble(txtAcumulado12.Text) + Convert.ToDouble(txtAcumulado13.Text) + Convert.ToDouble(txtAcumulado14.Text) + Convert.ToDouble(txtAcumulado15.Text);

            txtTotalFinanciado.Text = (TotalFinan).ToString("N2");
            txtTotalGasto.Text = (TotalGasto).ToString("N2");
            lblCostoDirecto.Text = sumaCostoDirecto.ToString("N2");
            txtTotalAcumulado.Text = TotalAcumulado.ToString("N2");

            if (TotalFinan == 0)
            {
                lblAvanceFinanciero.Text = "0.00";
            }
            else
            {
                lblAvanceFinanciero.Text = ((TotalGasto / TotalFinan) * 100).ToString("N2");
            }
            Up_AvanceFinancieroTab2.Update();
        }
        protected void AvanceFinanciero_TextChanged(object sender, EventArgs e)
        {
            SumarAvanceFinancieroNE();
        }

        protected void cargaDesembolsosTab2()
        {
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdDesembolsosTab2.DataSource = _objBLEjecucion.F_spMON_GastosNE(_BEEjecucion);
            grdDesembolsosTab2.DataBind();

            //TEMPORAL
            //CargaUbigeo("4", ddlDepaCuentaTab1.SelectedValue, ddlProvCuentaTab1.SelectedValue, ddlDistCuentatab1.SelectedValue, ddlCCPPCuentaTab1);
        }
        protected void grdDesembolsosTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id_gastos;
            id_gastos = grdDesembolsosTab2.SelectedDataKey.Value.ToString();
            lblIDGastosNETab2.Text = id_gastos.ToString();
            GridViewRow row = grdDesembolsosTab2.SelectedRow;

            txtAutorizaciónTab2.Text = ((Label)row.FindControl("lblAutorizacion")).Text;
            txtFechaDesembolsoTab2.Text = ((Label)row.FindControl("lblFecha")).Text;
            txtMontoAutorizaciónTab2.Text = ((Label)row.FindControl("lblMonto")).Text;
            ddlTipoGastoNE.SelectedValue = ((Label)row.FindControl("lblIDTipoTab2")).Text;
            txtObservacionGastoTab2.Text = ((Label)row.FindControl("lblObservacionTab2")).Text;

            lnkbtnGastosNETab2.Text = ((ImageButton)row.FindControl("imgDocAutorizacionTab2")).ToolTip;
            GeneraIcoFile(lnkbtnGastosNETab2.Text, imgbtnGastosNETab2);

            lblNomActualizaDesembolsoTab2.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            btnGuardarDesembolsoTab2.Visible = false;
            Panel_DesembolsosTab2.Visible = true;
            btnModificarDesembolsoTab2.Visible = true;
            imgbtnAgregarDesembolsosTab2.Visible = false;

            Up_Tab2.Update();

        }
        protected void grdDesembolsosTab2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdDesembolsosTab2.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.monto = "0";
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Date_fecha = Convert.ToDateTime("09/09/2012");
                _BEEjecucion.observacion = "";
                _BEEjecucion.urlDoc = "";

                int val = _objBLEjecucion.UD_spud_MON_GastosNE(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDesembolsosTab2();
                    Panel_DesembolsosTab2.Visible = false;
                    imgbtnAgregarDesembolsosTab2.Visible = true;


                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }
        protected void imgbtnAgregarDesembolsosTab2_Click(object sender, ImageClickEventArgs e)
        {
            Panel_DesembolsosTab2.Visible = true;
            btnModificarDesembolsoTab2.Visible = false;

            lblNomActualizaDesembolsoTab2.Text = "";
            lnkbtnGastosNETab2.Text = "";
            imgbtnGastosNETab2.ImageUrl = "~/img/blanco.png";
        }
        protected void btnCancelarDesembolsoTab2_Click(object sender, EventArgs e)
        {
            Panel_DesembolsosTab2.Visible = false;
        }
        protected void btnModificarDesembolsoTab2_Click(object sender, EventArgs e)
        {
            if (ValidarDesembolsosTab2())
            {
                _BEEjecucion.id_item = Convert.ToInt32(lblIDGastosNETab2.Text);
                _BEEjecucion.numero = Convert.ToInt32(txtAutorizaciónTab2.Text);
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaDesembolsoTab2.Text);
                _BEEjecucion.monto = txtMontoAutorizaciónTab2.Text;
                _BEEjecucion.monto = txtMontoAutorizaciónTab2.Text;
                _BEEjecucion.id_tabla = Convert.ToInt32(ddlTipoGastoNE.SelectedValue);
                _BEEjecucion.observacion = txtObservacionGastoTab2.Text;
                if (FileUploadGastosNETab2.HasFile)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadGastosNETab2);
                }
                else
                { _BEEjecucion.urlDoc = lnkbtnGastosNETab2.Text; }
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                _BEEjecucion.Tipo = 1;

                int val = _objBLEjecucion.UD_spud_MON_GastosNE(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente el desembolso.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDesembolsosTab2();
                    Panel_DesembolsosTab2.Visible = false;
                    imgbtnAgregarDesembolsosTab2.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }

        protected Boolean ValidarDesembolsosTab2()
        {
            Boolean result;
            result = true;

            if (txtAutorizaciónTab2.Text == "")
            {
                string script = "<script>alert('Ingresar número de autorización.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtMontoAutorizaciónTab2.Text == "")
            {
                string script = "<script>alert('Ingresar monto (S/.).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaDesembolsoTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de desembolso.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (_Metodo.ValidaFecha(txtFechaDesembolsoTab2.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de desembolso.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (ddlTipoGastoNE.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de gasto.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaAprobacionInformeTab2.Text == "")
            {
                string script = "<script>alert('Ingresar fecha de aprobación del informe inicial.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }


            if (Convert.ToDateTime(txtFechaDesembolsoTab2.Text) < Convert.ToDateTime(txtFechaAprobacionInformeTab2.Text))
            {
                string script = "<script>alert('La fecha de autorización de retiro debe ser mayor a la fecha de aprobación del informe inicial.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }
        protected void btnGuardarDesembolsoTab2_Click(object sender, EventArgs e)
        {
            if (ValidarDesembolsosTab2())
            {
                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.numero = Convert.ToInt32(txtAutorizaciónTab2.Text);
                _BEEjecucion.Date_fecha = VerificaFecha(txtFechaDesembolsoTab2.Text);
                _BEEjecucion.monto = txtMontoAutorizaciónTab2.Text;
                _BEEjecucion.id_tabla = Convert.ToInt32(ddlTipoGastoNE.SelectedValue);
                _BEEjecucion.observacion = txtObservacionGastoTab2.Text;
                if (FileUploadGastosNETab2.HasFile)
                {
                    _BEEjecucion.urlDoc = _Metodo.uploadfile(FileUploadGastosNETab2);
                }
                else
                { _BEEjecucion.urlDoc = lnkbtnGastosNETab2.Text; }

                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLEjecucion.I_spi_MON_GastosNE(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente el desembolso.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    cargaDesembolsosTab2();
                    Panel_DesembolsosTab2.Visible = false;
                    imgbtnAgregarDesembolsosTab2.Visible = true;

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }
        }
        protected void ddlDepaCuentaTab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("2", ddlDepaCuentaTab1.SelectedValue, "", "", ddlProvCuentaTab1);
            CargaUbigeo("3", ddlDepaCuentaTab1.SelectedValue, ddlProvCuentaTab1.SelectedValue, "", ddlDistCuentatab1);

        }
        protected void ddlProvCuentaTab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaUbigeo("3", ddlDepaCuentaTab1.SelectedValue, ddlProvCuentaTab1.SelectedValue, "", ddlDistCuentatab1);

        }

        protected Boolean ValidarConvenioNETab1()
        {
            Boolean result;
            result = true;

            if (txtNroConvenioNETab1.Text == "")
            {
                string script = "<script>alert('Ingrese nro de convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (txtFechaConvenioNETab1.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

            if (_Metodo.ValidaFecha(txtFechaConvenioNETab1.Text) == false)
            {
                string script = "<script>alert('Formato no valido de fecha de convenio.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (Convert.ToDateTime(txtFechaConvenioNETab1.Text) > DateTime.Today || Convert.ToDateTime(txtFechaConvenioNETab1.Text).Year < 2012)
            {
                string script = "<script>alert('La fecha de convenio no debe ser mayor a la fecha actual ni menor al año 2012.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaAprobacionTab1.Text.Length > 1)
            {
                if (Convert.ToDateTime(txtFechaConvenioNETab1.Text) < Convert.ToDateTime(txtFechaAprobacionTab1.Text))
                {
                    string script = "<script>alert('La fecha de convenio debe ser mayor o igual a la fecha de solicitud.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtFechaAprobacionExpTab0.Text.Length > 1)
            {
                if (lblCOD_SUBSECTOR.Text != "5")
                {
                    if (Convert.ToDateTime(txtFechaConvenioNETab1.Text) > Convert.ToDateTime(txtFechaAprobacionExpTab0.Text))
                    {
                        string script = "<script>alert('La fecha de convenio debe ser menor o igual a la fecha de aprobación del Exp. Técnico.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        result = false;
                        return result;
                    }
                }
            }



            return result;
        }
        protected void btnGuardarConvenioNE_Click(object sender, EventArgs e)
        {
            if (ValidarConvenioNETab1())
            {

                if (validaArchivo(FileUploadConvenioNETab1))
                {
                    _BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEProceso.tipoFinanciamiento = 3;
                    _BEProceso.flagObra = ddlProcesoTab1.SelectedValue;
                    _BEProceso.id_tipoModalidad = Convert.ToInt32(ddlModalidadTab1.SelectedValue); // Value default 4

                    _BEProceso.ConvenioContrato = txtNroConvenioNETab1.Text;
                    _BEProceso.Date_fechaConvenio = VerificaFecha(txtFechaConvenioNETab1.Text);
                    //_BEProceso.idTipoAlcance = Convert.ToInt32(ddlTipoAlcanceNETab1.SelectedValue);

                    if (FileUploadConvenioNETab1.HasFile)
                    {
                        _BEProceso.urlConvenio = _Metodo.uploadfile(FileUploadConvenioNETab1);
                    }
                    else
                    { _BEProceso.urlConvenio = lnkbtnConvenioNETab1.Text; }

                    _BEProceso.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLProceso.I_spi_MON_ProcesoSeleccionIndirectaNEConvenio(_BEProceso); //1

                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente el convenio del NE.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        CargaDatosTab1();

                        Up_Tab1.Update();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }

                }

            }
        }

        protected void CargaValorFinanciadoValorizacionTab2(int idAvanceFisico)
        {
            List<BE_MON_Ejecucion> ListFinanciero = new List<BE_MON_Ejecucion>();
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_avanceFisico = idAvanceFisico;
            ListFinanciero = _objBLEjecucion.F_spMON_AvanceFinancieroNE(_BEEjecucion);

            int i = 1;
            foreach (BE_MON_Ejecucion ItemFinanciero in ListFinanciero)
            {
                Label txtRubro = (Label)FindControl("ctl00$ContentPlaceHolder1$txtRubro" + i.ToString());
                TextBox txtFinanciado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtFinanciado" + i.ToString());
                TextBox txtGasto = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtGasto" + i.ToString());
                TextBox txtAcumulado = (TextBox)FindControl("ctl00$ContentPlaceHolder1$txtAcumulado" + i.ToString());

                txtRubro.Text = ItemFinanciero.nombre;

                txtFinanciado.Text = ItemFinanciero.monto;
                if (ItemFinanciero.fecha_update != null)
                {
                    txtFinanciado.Enabled = false;
                }

                txtGasto.Text = Convert.ToDouble(ItemFinanciero.montoEjecutado).ToString("N2");
                txtAcumulado.Text = Convert.ToDouble(ItemFinanciero.montoTotal).ToString("N2");

                i = i + 1;

            }
        }

        protected void imgGastosTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            //string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            var val = grdAvanceFisicoTab2.DataKeys[row.RowIndex].Value;

            Label lblMes = (Label)grdAvanceFisicoTab2.Rows[row.RowIndex].FindControl("lblMesValorizacionTab2");
            lblNomMesFinanciamiento.Text = lblMes.Text;
            Up_MesAvanceFinancieroTab2.Update();

            CargaValorFinanciadoValorizacionTab2(Convert.ToInt32(val));

            btnGuardarAvanceFinancieroTab2.Visible = false;
            SumarAvanceFinancieroNE();
            Up_AvanceFinancieroTab2.Update();

            MPE_AvanceFinanciero.Show();

        }

        protected void txtMontoTab2_TextChanged(object sender, EventArgs e)
        {
            ObtenerAvanceFisico();
        }

        protected void ObtenerAvanceFisico()
        {
            if (lblCOD_SUBSECTOR.Text == "5")
            {
                List<BE_MON_Financiamiento> ListItem = new List<BE_MON_Financiamiento>();
                ListItem = _objBLFinanciamiento.F_spMON_PresupuestoPNVR(Convert.ToInt32(LblID_PROYECTO.Text));

                ListItem = (from a in ListItem
                            where a.numero == 15
                            select a).ToList();

                if (Convert.ToDouble(ListItem.ElementAt(0).monto) > 0)
                {
                    txtFisiReal.Text = ((Convert.ToDouble(txtMontoTab2.Text) * 100) / (Convert.ToDouble(ListItem.ElementAt(0).monto) + Convert.ToDouble(hfTotalAdicional.Value) - Convert.ToDouble(hfTotalDeductivo.Value))).ToString();
                }
                else
                {
                    txtFisiReal.Text = "0.00";
                }
            }
            else
            {
                if (Convert.ToDouble(lblCostoDirecto.Text) > 0)
                {
                    txtFisiReal.Text = ((Convert.ToDouble(txtMontoTab2.Text) * 100) / (Convert.ToDouble(lblCostoDirecto.Text) + Convert.ToDouble(hfTotalAdicional.Value) - Convert.ToDouble(hfTotalDeductivo.Value))).ToString();
                }
                else
                {
                    txtFisiReal.Text = "0.00";
                }
            }
        }

        protected Boolean ValidarEstadoSituacional()
        {
            Boolean result;
            result = true;

            if (ddlEstadoTab2.SelectedValue == "")
            {
                string script = "<script>alert('Ingrese Estado de Ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }


            if (ddlSubEstadoTab2.SelectedValue == "" && ddlSubEstadoTab2.Visible == true)
            {
                string script = "<script>alert('Ingrese Sub - Estado de Ejecución.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;

            }

           

            if (ddlEstadoTab2.SelectedValue == "5")
            {
                if (ddlSubEstado2Tab2.SelectedValue == "")
                {
                    string script = "<script>alert('Ingrese Sub - Estado 2 de Ejecución.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
            }
             
            if (lblIdEstado.Text.Trim() != "") 
            {
                if (!(new Validacion().ValidarCambioEstado(Convert.ToInt32(LblID_PROYECTO.Text), Convert.ToInt32(lblIdEstado.Text), Convert.ToInt32(ddlEstadoTab2.SelectedValue), 3)))
                {
                    string script = "<script>alert('No puede volver el proyecto a Actos Previos');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;

                }
 
            }
          
            return result;
        }

        protected Boolean ValidarGeneracionProgramacion()
        {
            Boolean result;
            result = true;


            //_BEProceso.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            //_BEProceso.tipoFinanciamiento = 3;
            //_BEProceso.flagObra = "1";
            //dt = _objBLProceso.spMON_ProcesoSeleccion(_BEProceso);
            //string vMontoContratado = "";
            //if (dt.Rows.Count > 0)
            //{
            //    vMontoContratado = dt.Rows[0]["MontoConsorcio"].ToString();
            //}

            //if (vMontoContratado.Equals("") || vMontoContratado.Equals("0.00") || vMontoContratado.Equals("0"))
            //{
            //    string script = "<script>alert('Ingresar monto contratado en la pestaña Proceso de Selección');</script>";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //    result = false;
            //    return result;
            //}

            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.tipoFinanciamiento = 3;
            dt = _objBLEjecucion.spMON_EstadoEjecuccion(_BEEjecucion);
            string plazo = "";
            string fechaInicio = "";
            if (dt.Rows.Count > 0)
            {
                plazo = dt.Rows[0]["plazoEjecucion"].ToString();
                fechaInicio = dt.Rows[0]["FechaEntregaTerreno"].ToString();
            }

            if (fechaInicio == "")
            {
                string script = "<script>alert('Registrar fecha de entrega de terreno (Fecha de Inicio).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (plazo == "")
            {

                string script = "<script>alert('Registrar plazo de ejecución (días).');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }
        protected void btnGenerarProgramacionTab2_Click(object sender, EventArgs e)
        {
            if (ValidarGeneracionProgramacion())
            {
                DateTime dtFechaInicio = Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text);
                int iPlazo = Convert.ToInt32(txtPlazoEjecucionTab2.Text);

                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                int valor = _objBLEjecucion.I_spi_MON_GeneracionProgramacion(_BEEjecucion);

                if (iPlazo == 1)
                {
                    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BEEjecucion.tipoFinanciamiento = 3;
                    _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                    _BEEjecucion.fisicoProgramado = "0";
                    _BEEjecucion.financieroProgramado = "0";
                    //_BEEjecucion.diasProgramado = Convert.ToInt32(txtDiasAsistirTab2.Text);
                    _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                    _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(1);
                    //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionProgramacionTab2.SelectedValue);
                    _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                    int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                    iPlazo = 0;
                    //dtFechaInicio = dtFechaInicio.AddDays(iDiasMes + 1);
                }
                else
                {
                    if (iPlazo > 0)
                    {
                        iPlazo = iPlazo - 1;
                    }
                    while (iPlazo > 0)
                    {
                        int iDiasMes = System.DateTime.DaysInMonth(dtFechaInicio.Year, dtFechaInicio.Month) - dtFechaInicio.Day;
                        if (iDiasMes < iPlazo)
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                            _BEEjecucion.fisicoProgramado = "0";
                            _BEEjecucion.financieroProgramado = "0";
                            //_BEEjecucion.diasProgramado = Convert.ToInt32(txtDiasAsistirTab2.Text);
                            _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                            _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iDiasMes);
                            //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionProgramacionTab2.SelectedValue);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int val1 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                            dtFechaInicio = dtFechaInicio.AddDays(iDiasMes + 1);

                            if (iPlazo - iDiasMes == 1)
                            {
                                _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                                _BEEjecucion.tipoFinanciamiento = 3;
                                _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                                _BEEjecucion.fisicoProgramado = "0";
                                _BEEjecucion.financieroProgramado = "0";
                                //_BEEjecucion.diasProgramado = Convert.ToInt32(txtDiasAsistirTab2.Text);
                                _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                                _BEEjecucion.Date_fechaFin = dtFechaInicio;
                                //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionProgramacionTab2.SelectedValue);
                                _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                                int valFin = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);
                            }

                            iPlazo = iPlazo - iDiasMes - 1;

                        }
                        else
                        {
                            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                            _BEEjecucion.tipoFinanciamiento = 3;
                            _BEEjecucion.dtFechaProgramada = VerificaFecha("");
                            _BEEjecucion.fisicoProgramado = "0";
                            _BEEjecucion.financieroProgramado = "0";
                            //_BEEjecucion.diasProgramado = Convert.ToInt32(txtDiasAsistirTab2.Text);
                            _BEEjecucion.Date_fechaInicio = dtFechaInicio;
                            _BEEjecucion.Date_fechaFin = dtFechaInicio.AddDays(iPlazo);
                            //_BEEjecucion.id_proyectoTecnico = Convert.ToInt32(ddlFuncionProgramacionTab2.SelectedValue);
                            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);
                            int val2 = _objBLEjecucion.I_spi_MON_ProgramacionInforme(_BEEjecucion);

                            iPlazo = iPlazo - iPlazo;
                            //dtFechaInicio = dtFechaInicio.AddDays(iDiasMes + 1);

                        }
                    }
                }
                string script = "<script>alert('Se generó la programación correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                Panel_RegistrarProgramacionTab2.Visible = false;
                //imgbtnAgregarProgramacionTab2.Visible = true;
                cargaProgramacionTab2();
                CargaDatosTab2();
                Up_Tab2.Update();

            }
        }

        protected Boolean ValidarCulminacionProyecto()
        {
            string fecTerminoProyecto = txtFechaCulminacionProyectoTab2.Text,
                   fecTerminoObra = txtTerminoObraTab2.Text,
                   fecRecepcionObra = txtFechaRecepcionTab2.Text,
                   fecInicioProyecto = txtFechaInicioProyectoTab2.Text,
                   fecInicioObra = txtFechaEntregaTerrenoTab2.Text;

            if (fecRecepcionObra.Length>0)
            {
                if (fecTerminoObra == "")
                    return ImprimirMensaje("Ingresar la fecha de término de obra.");
                else if (Convert.ToDateTime(fecRecepcionObra) < Convert.ToDateTime(fecTerminoObra))
                    return ImprimirMensaje("La fecha de recepcion de obra debe ser mayor y/o igual a la fecha de termino de obra.");
                else if (txtFechaTab3.Text.Length>0 && Convert.ToDateTime(fecRecepcionObra) > Convert.ToDateTime(txtFechaTab3.Text))
                    return ImprimirMensaje("La fecha de recepcion de obra debe ser menor y/o igual a la fecha de liquidación.");


            }

            if (fecTerminoProyecto.Length > 0)
            {
                if (fecTerminoObra == "")
                    return ImprimirMensaje("Ingresar la fecha de término de obra.");
                else if (_Metodo.ValidaFecha(fecTerminoObra) == false)
                    return ImprimirMensaje("Formato no válido de fecha de termino de obra.");
                else if (Convert.ToDateTime(fecTerminoProyecto) < Convert.ToDateTime(fecTerminoObra))
                    return ImprimirMensaje("La fecha de culminación del proyecto debe ser mayor y/o igual a la fecha de termino de obra.");
                else if (fecInicioProyecto == "")
                    return ImprimirMensaje("Ingresar la fecha de inicio de proyecto.");
                else if (_Metodo.ValidaFecha(fecInicioProyecto) == false)
                    return ImprimirMensaje("Formato no válido de fecha de inicio de proyecto.");
                else if (Convert.ToDateTime(fecTerminoProyecto) < Convert.ToDateTime(fecInicioProyecto))
                    return ImprimirMensaje("La fecha de culminación del proyecto debe ser mayor que la fecha de inicio de proyecto.");
                else if (fecInicioObra == "")
                    return ImprimirMensaje("Ingresar la fecha de Inicio de Obra.");
                else if (_Metodo.ValidaFecha(fecInicioObra) == false)
                    return ImprimirMensaje("Formato no válido de fecha de inicio de obra.");
                else if (txtFechaOrientacionTab1.Text.Length>1 && Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text)< Convert.ToDateTime(txtFechaOrientacionTab1.Text))
                    return ImprimirMensaje("La fecha de entrega de terreno debe ser mayor o igual o a la fecha de inducción.");
                else if (txtFechaIniReal.Text.Length > 1 && Convert.ToDateTime(txtFechaEntregaTerrenoTab2.Text) > Convert.ToDateTime(txtFechaIniReal.Text))
                    return ImprimirMensaje("La fecha de entrega de terreno debe ser menor o igual o a la fecha de inicio real.");
                else if (Convert.ToDateTime(fecTerminoProyecto) < Convert.ToDateTime(fecInicioObra))
                    return ImprimirMensaje("La fecha de culminación del proyecto debe ser mayor que la fecha de inicio de obra.");
            }

            if (fecTerminoObra.Length > 0)
            {
                if (fecInicioProyecto == "")
                    return ImprimirMensaje("Ingresar la fecha de inicio de proyecto.");
                else if (_Metodo.ValidaFecha(fecInicioProyecto) == false)
                    return ImprimirMensaje("Formato no válido de fecha de inicio de proyecto.");
                else if (Convert.ToDateTime(fecTerminoObra) < Convert.ToDateTime(fecInicioProyecto))
                    return ImprimirMensaje("La fecha de termino de obra debe ser mayor que la fecha de inicio de proyecto.");
                else if (fecInicioObra == "")
                    return ImprimirMensaje("Ingresar la fecha de Inicio de Obra.");
                else if (_Metodo.ValidaFecha(fecInicioObra) == false)
                    return ImprimirMensaje("Formato no válido de fecha de inicio de obra.");
                else if (Convert.ToDateTime(fecTerminoObra) < Convert.ToDateTime(fecInicioObra))
                    return ImprimirMensaje("La fecha de termino de obra debe ser mayor que la fecha de inicio de obra.");
                else if (txtFechaRecepcionTab2.Text.Length>0 && Convert.ToDateTime(fecTerminoObra) > Convert.ToDateTime(txtFechaRecepcionTab2.Text))
                    return ImprimirMensaje("La fecha de termino de obra debe ser menos o igual de la fecha de recepción.");


            }

            if (grdEvaluacionTab2.Rows.Count > 0)
            {
                int i = grdEvaluacionTab2.Rows.Count - 1;
                string tipoEstadoEjecuccion = ((Label)grdEvaluacionTab2.Rows[i].FindControl("lblTipoEstadoEjecuccion")).Text;
                if (tipoEstadoEjecuccion == "6" || tipoEstadoEjecuccion == "43")
                {
                    if (fecTerminoProyecto == "")
                        return ImprimirMensaje("Ingresar la fecha real de culminación del proyecto.");
                }
            }
            return true;
        }
        protected void btnGuardarCulminacionProyectoTab2_Click(object sender, EventArgs e)
        {
            if (ValidarCulminacionProyecto())
            {
                if (validaArchivo(FileUploadFichaRecepcionTab2))
                {
                    if (validaArchivo(FileUploadActaTerminoTab2))
                    {
                        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                        _BEEjecucion.tipoFinanciamiento = 3;
                        _BEEjecucion.Date_fechaFin = VerificaFecha(txtTerminoObraTab2.Text);
                        _BEEjecucion.Date_fechaInforme = VerificaFecha(txtFechaRendicionFinalTab2.Text);
                        _BEEjecucion.Date_fechaCulminacionProyecto = VerificaFecha(txtFechaCulminacionProyectoTab2.Text);
                        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                        if (FileUploadActaTerminoTab2.PostedFile.ContentLength > 0)
                        {
                            _BEEjecucion.urlActatermino = _Metodo.uploadfile(FileUploadActaTerminoTab2);
                        }
                        else
                        {
                            _BEEjecucion.urlActatermino = lnkbtnActaTerminoTab2.Text;
                        }

                        _BEEjecucion.Date_fechaRecepcion = VerificaFecha(txtFechaRecepcionTab2.Text);
                        _BEEjecucion.urlFichaRecepcion = FileUploadFichaRecepcionTab2.HasFile ? _Metodo.uploadfile(FileUploadFichaRecepcionTab2) : lnkbtnFichaRecepcionTab2.Text;


                        int val = _objBLEjecucion.I_spi_MON_CulminacionEjecucionNE(_BEEjecucion);

                        if (val == 1)
                        {
                            string script = "<script>alert('Se registró correctamente.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                            CargaDatosTab2();

                            Up_Tab2.Update();

                        }
                        else
                        {
                            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                        }
                    }
                }
            }
        }
        protected void btnGuardarCierreTab3_Click(object sender, EventArgs e)
        {
            string msj = ValidarCierre();
            if (msj.Length > 0)
            {
                string script = "<script>alert('"+ msj + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BELiquidacion.tipoFinanciamiento = 3;

                _BELiquidacion.idTipoFormato = Convert.ToInt32(ddlFormatoCierreTabCierre.SelectedValue);
                _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaFormato14TabCierre.Text);
                _BELiquidacion.urlDocumento = lnkbtnFormatoCierreTabCierre.Text;

                if (FileUploadFormatoCierreTabCierre.HasFile)
                {
                    _BELiquidacion.urlDocumento = _Metodo.uploadfile(FileUploadFormatoCierreTabCierre);
                }
                else
                { _BELiquidacion.urlDocumento = lnkbtnFormatoCierreTabCierre.Text; }


                _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLLiquidacion.IU_spiu_MON_CierreProyecto_NE(_BELiquidacion);


                if (val == 1)
                {
                    string script = "<script>alert('Se registró correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaLiquidacion();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }


                Up_TabCierre.Update();
                Up_Tab3.Update();
            }
        }

        protected string ValidarCierre()
        {
            string msj="";
            

            if (ddlFormatoCierreTabCierre.SelectedValue == "")
            {
                msj = msj + "Ingrese tipo de formato de cierre.\\n";
            }

            if (txtFechaFormato14TabCierre.Text == "")
            {
                msj = msj + "Ingrese fecha de registro de formato.\\n";
            }
            else
            {
                if (_Metodo.ValidaFecha(txtFechaFormato14TabCierre.Text) == false)
                {
                    msj = msj + "Formato no valido de Fecha de registro de formato.";
                }
                else if(Convert.ToDateTime(txtFechaFormato14TabCierre.Text)>DateTime.Now.Date) {
                    msj = msj + "La fecha de registro de formato no puede ser mayor a la fecha de hoy.\\n";
                }

                if (txtFechaActaTransferenciaTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaFormato14TabCierre.Text) < Convert.ToDateTime(txtFechaActaTransferenciaTab3.Text))
                {
                    msj = msj + "La fecha de registro de formato debe ser mayor o igual a la fecha de acta de transferencia fisica.";
                }
            }

            return msj;
        }

        protected void imgDocAutorizacionTab2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdDesembolsosTab2.Rows[row.RowIndex].FindControl("imgDocAutorizacionTab2");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void lnkbtnGastosNETab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnGastosNETab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }
        protected void grdDesembolsosTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgDocAutorizacionTab2");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }
        //protected void grdProgramacionTab2_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    double sumaFisico = 0;
        //    double sumaFinanciero = 0;

        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        Label lblFisProgramado = (Label)e.Row.FindControl("lblFisicoProgramado");
        //        Label lblFinanProgramado = (Label)e.Row.FindControl("lblFinancieroProgramado");

        //        if (lblFisProgramado.Text == "")
        //        {
        //            lblFisProgramado.Text = "0.00";
        //        }

        //        if (lblFinanProgramado.Text == "")
        //        {
        //            lblFinanProgramado.Text = "0.00";
        //        }

        //        sumaFisico = Convert.ToDouble(lblFisicoProgamado.Text) + Convert.ToDouble(lblFisProgramado.Text);
        //        sumaFinanciero = Convert.ToDouble(lblFinacieroProgramado.Text) + Convert.ToDouble(lblFinanProgramado.Text);

        //        lblFisicoProgamado.Text = sumaFisico.ToString("N2");
        //        lblFinacieroProgramado.Text = sumaFinanciero.ToString("N2");
        //    }
        //}
        protected void btnViviendasTerminadasTab3_Click(object sender, EventArgs e)
        {
            MPE_ViviendasTerminadas.Show();
            CargarViviendasLiquidadas();
            Up_ViviendasTerminadas.Update();
        }
        protected void grdViviendasLiquidadas_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = grdViviendasLiquidadas.Rows[e.RowIndex];

            TextBox txtMetaRealizada = (TextBox)row.FindControl("txtMetaRealizada");
            TextBox txtObservacion = (TextBox)row.FindControl("txtObservacion");

            if (txtMetaRealizada.Text == "")
            {
                txtMetaRealizada.Text = "0";
            }

            int ID = Int32.Parse(grdViviendasLiquidadas.DataKeys[e.RowIndex].Value.ToString());
            _BEBandeja.id = ID;
            _BEBandeja.metaRealizada = Convert.ToInt32(txtMetaRealizada.Text);
            _BEBandeja.observacion = txtObservacion.Text;

            int val = _objBLBandeja.U_MON_UbigeoMetasTerminadas(_BEBandeja);

            //Reset the edit index.
            grdViviendasLiquidadas.EditIndex = -1;
            //Bind data to the GridView control.
            CargarViviendasLiquidadas();

        }
        protected void grdViviendasLiquidadas_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            grdViviendasLiquidadas.EditIndex = e.NewEditIndex;
            //Bind data to the GridView control.
            CargarViviendasLiquidadas();
        }
        protected void grdViviendasLiquidadas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Reset the edit index.
            grdViviendasLiquidadas.EditIndex = -1;
            //Bind data to the GridView control.
            CargarViviendasLiquidadas();
        }

        protected void CargarViviendasLiquidadas()
        {
            List<BE_MON_BANDEJA> ListUbigeos = new List<BE_MON_BANDEJA>();
            ListUbigeos = _objBLBandeja.F_spMON_UbigeosMetas(Convert.ToInt32(LblID_PROYECTO.Text));
            grdViviendasLiquidadas.DataSource = ListUbigeos;
            grdViviendasLiquidadas.DataBind();

            int resultadoTotal = ListUbigeos.Sum(elemento => elemento.metaRealizada);
            //txtTotalMetasTab0.Text = resultadoTotal.ToString();

            txtViviendasTerminadas.Text = resultadoTotal.ToString() + " de " + txtTotalMetasTab0.Text;
        }
        protected void grdViviendasLiquidadas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdViviendasLiquidadas.PageIndex = e.NewPageIndex;
            //Bind data to the GridView control.
            CargarViviendasLiquidadas();
        }




        protected void txtFechaProgramadaTab2_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToDateTime(txtFechaProgramadaTab2.Text) > Convert.ToDateTime(txtFinInformeTab2.Text).AddDays(9))
            {
                lblMsjValidacionFechaTab2.ForeColor = System.Drawing.Color.Red;
                lblMsjValidacionFechaTab2.Text = "<div class='alert-warning'><b>ADVERTENCIA!:</b> Verificar la fecha límite de presentación de pre-liquidación. </br>";

            }
            else
            {
                lblMsjValidacionFechaTab2.Text = "";
            }
        }


        // Aaron----------------------------------------------

        #region Padron Beneficiario

        #region Button Events
        protected void btnPadronBeneficiario_Click(object sender, EventArgs e)
        {
            hdid_padronBeneficiario.Value = "0";
            ListarDataPadron();
            popupPadronBeneficiario.ShowOnPageLoad = true;

        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            hdid_padronBeneficiario.Value = "0";

            CargarCombos();
            popupPadronBeneficiarioNuevo.ShowOnPageLoad = true;
            LimpiarControles();

        }

        private void LimpiarControles()
        {
            txtDni.Text = "";
            txtNombres.Text = "";
            txtApellidoMaterno.Text = "";
            txtApellidoPaterno.Text = "";
            txtFechaNacimiento.Text = "";
            txtDni.Text = "";
            txtDireccion.Text = "";
            txtNroDomicilio.Text = "";
            txtNroMiemH.Text = "";
            txtNroMiemM.Text = "";
            txtObservaciones.Text = "";

        }

        protected void btnAbrirCargarExcel_Click(object sender, EventArgs e)
        {
            Session["id_proyecto_carga"] = Convert.ToInt32(LblID_PROYECTO.Text);
            Session["usr_registro_carga"] = Convert.ToInt32(LblID_USUARIO.Text);

            popupPadronBeneficiarioCargaMasiva.ShowOnPageLoad = true;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            //recuperar datos
            int result = 0;
            int id_padronBeneficiario = Convert.ToInt32(hdid_padronBeneficiario.Value);

            if (ValidarPadronBeneficiario() == false)
            {
                popupPadronBeneficiarioNuevo.ShowOnPageLoad = true;
            }
            else
            {
                if (id_padronBeneficiario == 0)
                {
                    var data = MapearDatos(0);
                    result = new BL_PadronBeneficiario().Insertar_PadronBeneficiario(data);

                }
                else
                {
                    var data = MapearDatos(id_padronBeneficiario);
                    result = new BL_PadronBeneficiario().Actualizar_PadronBeneficiario(data);
                    string script = "<script>alert('Datos actualizados');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    popupPadronBeneficiarioNuevo.ShowOnPageLoad = false;
                    ListarDataPadron();
                    LimpiarControles();
                }


                //insertar en bd
                if (result > 0)
                {
                    string script = "<script>alert('Datos registrados');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    popupPadronBeneficiarioNuevo.ShowOnPageLoad = false;
                    ListarDataPadron();
                    LimpiarControles();
                }
                else
                {
                    string script = "<script>alert('DNI ya existe');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

                //si es correcto, cerrar popup.
                //popupPadronBeneficiarioNuevo.ShowOnPageLoad = false;

                //listar grilla con el registro nuevo o actualizado
                //ListarDataPadron();
                //LimpiarControles();
            }

        }


        #endregion

        #region Private Methods
        private void ListarDataPadron()
        {
            int id_proyeto = Convert.ToInt32(LblID_PROYECTO.Text);
            var data = new BL_PadronBeneficiario().Listar_PadronBeneficiario(id_proyeto);
            Session["grvMantenimientoDetalle"] = data;
            grvMantenimientoDetalle.DataSource = data;
            grvMantenimientoDetalle.DataBind();
        }

        private void CargarCombos()
        {
            grupoSexo.DataTextField = "Nombre";
            grupoSexo.DataValueField = "IdGrupoDetalle";
            grupoSexo.DataSource = new BL_GrupoDetalle().ListarGrupo(100);
            grupoSexo.DataBind();

            grupoTipoDomicilio.DataTextField = "Nombre";
            grupoTipoDomicilio.DataValueField = "IdGrupoDetalle";
            grupoTipoDomicilio.DataSource = new BL_GrupoDetalle().ListarGrupo(200);
            grupoTipoDomicilio.DataBind();

            grupoTipoBeneficiario.DataTextField = "Nombre";
            grupoTipoBeneficiario.DataValueField = "IdGrupoDetalle";
            grupoTipoBeneficiario.DataSource = new BL_GrupoDetalle().ListarGrupo(300);
            grupoTipoBeneficiario.DataBind();

            grupoEstadoBeneficiario.DataTextField = "Nombre";
            grupoEstadoBeneficiario.DataValueField = "IdGrupoDetalle";
            grupoEstadoBeneficiario.DataSource = new BL_GrupoDetalle().ListarGrupo(400);
            grupoEstadoBeneficiario.DataBind();

            grupoConexionAgua.DataTextField = "Nombre";
            grupoConexionAgua.DataValueField = "IdGrupoDetalle";
            grupoConexionAgua.DataSource = new BL_GrupoDetalle().ListarGrupo(500);
            grupoConexionAgua.DataBind();

            grupoUbs.DataTextField = "Nombre";
            grupoUbs.DataValueField = "IdGrupoDetalle";
            grupoUbs.DataSource = new BL_GrupoDetalle().ListarGrupo(600);
            grupoUbs.DataBind();


            grupoTipoDocMiemNE.DataTextField = "Nombre";
            grupoTipoDocMiemNE.DataValueField = "IdGrupoDetalle";
            grupoTipoDocMiemNE.DataSource = new BL_GrupoDetalle().ListarGrupo(900);
            grupoTipoDocMiemNE.DataBind();


            grupoTipoUbs.DataTextField = "Nombre";
            grupoTipoUbs.DataValueField = "IdGrupoDetalle";
            grupoTipoUbs.DataSource = new BL_GrupoDetalle().ListarGrupo(700);
            grupoTipoUbs.DataBind();

            grupoEstadoPredio.DataTextField = "Nombre";
            grupoEstadoPredio.DataValueField = "IdGrupoDetalle";
            grupoEstadoPredio.DataSource = new BL_GrupoDetalle().ListarGrupo(800);
            grupoEstadoPredio.DataBind();
        }


        private BE_MON_PadronBeneficiario MapearDatos(int id_padronBeneficiario)
        {

            //LblID_USUARIO.Text = (Session["IdUsuario"]).ToString();

            var data = new BE_MON_PadronBeneficiario();

            data.usr_registro = Convert.ToInt32(LblID_USUARIO.Text);
            data.usr_update = Convert.ToInt32(LblID_USUARIO.Text);

            data.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

            data.id_padronBeneficiario = Convert.ToInt32(id_padronBeneficiario);
            data.nombres = txtNombres.Text;
            data.apellidoPaterno = txtApellidoPaterno.Text;
            data.apellidoMaterno = txtApellidoMaterno.Text;
            data.id_sexo = Convert.ToInt32(grupoSexo.SelectedValue);
            data.vfechaNacimiento = txtFechaNacimiento.Text;
            data.fechaNacimiento = Convert.ToDateTime(txtFechaNacimiento.Text);
            data.id_tipoDocumentoMiembroNE = Convert.ToInt32(grupoTipoDocMiemNE.SelectedValue);

            if (data.id_tipoDocumentoMiembroNE == 2)
            {
                if (!string.IsNullOrWhiteSpace(txtDni.Text))
                {
                    data.dni = txtDni.Text;
                }
            }
            else
            {
                data.dni = txtDni.Text;
            }

            data.id_tipoDomicilio = Convert.ToInt32(grupoTipoDomicilio.SelectedValue);

            if (!string.IsNullOrWhiteSpace(txtDireccion.Text))
            {
                data.direccionDomicilio = txtDireccion.Text;
            }

            if (!string.IsNullOrWhiteSpace(txtNroDomicilio.Text))
            {
                data.nroDomicilio = txtNroDomicilio.Text;
            }

            data.id_tipoBeneficio = Convert.ToInt32(grupoTipoBeneficiario.SelectedValue);
            data.id_estadoBeneficiario = Convert.ToInt32(grupoEstadoBeneficiario.SelectedValue);

            if (!string.IsNullOrWhiteSpace(txtNroMiemH.Text))
            {
                data.nroMiembrosHombres = Convert.ToInt32(txtNroMiemH.Text);
            }

            if (!string.IsNullOrWhiteSpace(txtNroMiemM.Text))
            {
                data.nroMiembrosMujeres = Convert.ToInt32(txtNroMiemM.Text);
            }

            data.id_conexioAgua = Convert.ToInt32(grupoConexionAgua.Text);
            data.id_ubs = Convert.ToInt32(grupoUbs.SelectedValue);
            data.id_tipoUbs = Convert.ToInt32(grupoTipoUbs.SelectedValue);
            data.id_estadoPredio = Convert.ToInt32(grupoEstadoPredio.SelectedValue);

            if (!string.IsNullOrWhiteSpace(txtObservaciones.Text))
            {
                data.observaciones = txtObservaciones.Text;
            }

            return data;
        }
        #endregion

        #region GridEvents
        protected void grvMantenimientoDetalle_PageIndexChanged(object sender, EventArgs e)
        {
            grvMantenimientoDetalle.DataSource = Session["grvMantenimientoDetalle"];
            grvMantenimientoDetalle.DataBind();
        }

        protected void imgarchivoModuloIEditarDetalle_OnClick(object sender, ImageClickEventArgs e)
        {
            ImageButton imgbutton = default(ImageButton);
            GridViewDataItemTemplateContainer gvrFila = default(GridViewDataItemTemplateContainer);

            imgbutton = (ImageButton)sender;
            if (imgbutton == null)
                return;

            gvrFila = (GridViewDataItemTemplateContainer)imgbutton.NamingContainer;
            int index = gvrFila.ItemIndex;


            CargarCombos();

            int id_padronBeneficiario = Convert.ToInt32(grvMantenimientoDetalle.GetRowValues(index, "id_padronBeneficiario"));
            hdid_padronBeneficiario.Value = Convert.ToString(id_padronBeneficiario);

            txtNombres.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nombres"));
            txtApellidoPaterno.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "apellidoPaterno"));
            txtApellidoMaterno.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "apellidoMaterno"));
            txtFechaNacimiento.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "vfechaNacimiento"));
            txtDni.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "dni"));
            txtDireccion.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "direccionDomicilio"));
            txtNroDomicilio.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nroDomicilio"));
            txtNroMiemH.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nroMiembrosHombres"));
            txtNroMiemM.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nroMiembrosMujeres"));
            txtObservaciones.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "observaciones"));
            grupoSexo.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_sexo"));
            grupoTipoDocMiemNE.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoDocumentoMiembroNE"));
            grupoTipoDomicilio.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoDomicilio"));
            grupoTipoBeneficiario.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoBeneficio"));
            grupoEstadoBeneficiario.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_estadoBeneficiario"));
            grupoConexionAgua.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_conexioAgua"));
            grupoUbs.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_ubs"));
            grupoTipoUbs.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoUbs"));
            grupoEstadoPredio.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_estadoPredio"));

            popupPadronBeneficiarioNuevo.ShowOnPageLoad = true;
        }

        //protected void imgEliminarModulo_OnClick(object sender, ImageClickEventArgs e)
        //{
        //    ImageButton imgbutton = default(ImageButton);
        //    GridViewDataItemTemplateContainer gvrFila = default(GridViewDataItemTemplateContainer);

        //    imgbutton = (ImageButton)sender;
        //    if (imgbutton == null)
        //        return;

        //    gvrFila = (GridViewDataItemTemplateContainer)imgbutton.NamingContainer;
        //    int index = gvrFila.ItemIndex;

        //    int id_padronBeneficiario = Convert.ToInt32(grvMantenimientoDetalle.GetRowValues(index, "id_padronBeneficiario"));

        //    //llamar metodo eliminar

        //    var result = new BL_PadronBeneficiario().Eliminar_PadronBeneficiario(id_padronBeneficiario);

        //    string script = "<script>alert('Registro Eliminado');</script>";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    //si eliminar es correcto, cargar grilla
        //    ListarDataPadron();

        //}

        protected void imgEliminarModulo_OnClick(object sender, ImageClickEventArgs e)
        {
            ImageButton imgbutton = default(ImageButton);
            GridViewDataItemTemplateContainer gvrFila = default(GridViewDataItemTemplateContainer);

            imgbutton = (ImageButton)sender;
            if (imgbutton == null)
                return;

            gvrFila = (GridViewDataItemTemplateContainer)imgbutton.NamingContainer;
            int index = gvrFila.ItemIndex;


            CargarCombos();

            int id_padronBeneficiario = Convert.ToInt32(grvMantenimientoDetalle.GetRowValues(index, "id_padronBeneficiario"));
            hdid_padronBeneficiario.Value = Convert.ToString(id_padronBeneficiario);

            txtNombres.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nombres"));
            txtApellidoPaterno.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "apellidoPaterno"));
            txtApellidoMaterno.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "apellidoMaterno"));
            txtFechaNacimiento.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "vfechaNacimiento"));
            txtDni.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "dni"));
            txtDireccion.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "direccionDomicilio"));
            txtNroDomicilio.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nroDomicilio"));
            txtNroMiemH.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nroMiembrosHombres"));
            txtNroMiemM.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "nroMiembrosMujeres"));
            txtObservaciones.Text = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "observaciones"));
            grupoSexo.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_sexo"));
            grupoTipoDocMiemNE.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoDocumentoMiembroNE"));
            grupoTipoDomicilio.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoDomicilio"));
            grupoTipoBeneficiario.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoBeneficio"));
            grupoEstadoBeneficiario.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_estadoBeneficiario"));
            grupoConexionAgua.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_conexioAgua"));
            grupoUbs.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_ubs"));
            grupoTipoUbs.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_tipoUbs"));
            grupoEstadoPredio.SelectedValue = Convert.ToString(grvMantenimientoDetalle.GetRowValues(index, "id_estadoPredio"));


            var data = MapearDatos(id_padronBeneficiario);
            //var result = new 

            new BL_PadronBeneficiario().Eliminar_PadronBeneficiario(data);

            string script = "<script>alert('Registro Eliminado');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            //si eliminar es correcto, cargar grilla
            ListarDataPadron();

        }


        #endregion

        #endregion

        protected Boolean ValidarPadronBeneficiario()
        {
            Boolean result;
            result = true;

            if (txtNombres.Text == "")
            {
                string script = "<script>alert('Ingrese Nombre.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtApellidoPaterno.Text == "")
            {
                string script = "<script>alert('Ingrese Apellido Paterno.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtApellidoMaterno.Text == "")
            {
                string script = "<script>alert('Ingrese Apellido Materno.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (txtFechaNacimiento.Text == "")
            {
                string script = "<script>alert('Ingrese fecha de nacimiento.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            DateTime fecha = DateTime.Now;
            if (!DateTime.TryParse(Convert.ToString(txtFechaNacimiento.Text), out fecha))
            {

                string script = "<script>alert('Fecha no tiene formato correcto dd/mm/yyyy');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            //var data = MapearDatos(0);

            //MapearDatos(0).id_tipoDocumentoMiembroNE
            if (MapearDatos(0).id_tipoDocumentoMiembroNE == 1 && txtDni.Text == "")
            {
                string script = "<script>alert('Ingrese el DNI');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }


        protected void btnRecargarPadronBeneficiarios_Click(object sender, EventArgs e)
        {
            ListarDataPadron();
        }




        protected void lnkbtnActaTerminoTab2_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnActaTerminoTab2.Text;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }



        //protected void cargarConexionesPNSR()
        //{
        //    _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);

        //    dt = _objBLEjecucion.spMON_MetasPNSR(_BEEjecucion);

        //    if (dt.Rows.Count > 0)
        //    {
        //        txtConexionAgua.Text = dt.Rows[0]["nroAgua"].ToString();
        //        txtConexionUbs.Text = dt.Rows[0]["nroAlcantarilladoUBS"].ToString();
        //        lblNomActualizaConexionesTab2.Text = "Actualizó: " + dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fechaUpdateMetas"].ToString();
        //    }
        //}

        //protected void btnGuardarConexionTab2_Click(object sender, EventArgs e)
        //{
        //    if (ValidarMetas())
        //    {
        //        _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
        //        _BEEjecucion.nroAgua = Convert.ToInt32(txtConexionAgua.Text);
        //        _BEEjecucion.nroAlcantarillado = Convert.ToInt32(txtConexionUbs.Text);
        //        _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

        //        int val = _objBLEjecucion.U_MON_MetasPNSR(_BEEjecucion);

        //        if (val == 1)
        //        {
        //            string script = "<script>alert('Registro Correcto.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //            //CargaFinanTransferencia();

        //            cargarConexionesPNSR();
        //            Up_Tab2.Update();
        //        }
        //        else
        //        {
        //            string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //        }

        //    }
        //}


        //protected bool ValidarMetas()
        //{
        //    bool result = true;

        //    if (txtConexionAgua.Text.Equals(""))
        //    {
        //        string script = "<script>alert('Ingresar nro. de conexiones de agua.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        result = false;
        //        return result;
        //    }

        //    if (txtConexionUbs.Text.Equals(""))
        //    {
        //        string script = "<script>alert('Ingresar nro. de conexiones UBS.');</script>";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        //        result = false;
        //        return result;
        //    }

        //    return result;
        //}

        //protected void grdSEACEObra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdSEACEObra.PageIndex = e.NewPageIndex;
        //    CargaOBRAEjecutorasTab0SEACEObra();
        //}

        //protected void imgseaceobra_Click(object sender, ImageClickEventArgs e)
        //{

        //}

        //protected void grdSEACEConsultoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdSEACEConsultoria.PageIndex = e.NewPageIndex;
        //    CargaOBRAEjecutorasTab0SEACEConsultoria();
        //}

        //protected void imgseaceConsultoria_Click(object sender, ImageClickEventArgs e)
        //{

        //}

        //protected void grdSEACEBienesServicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdSEACEBienesServicios.PageIndex = e.NewPageIndex;
        //    CargaOBRAEjecutorasTab0SEACEBienesServicios();
        //}

        //protected void imgseaceBienesServicios_Click(object sender, ImageClickEventArgs e)
        //{

        //}

        protected void TabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            ViewState["ActiveTabIndex"] = TabContainer.ActiveTabIndex;
        }

        protected void imgbtnEditarUE_Click(object sender, ImageClickEventArgs e)
        {
            string script = "<script>$('#modalEditarUE').modal('show');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void btnModificarUE_Click(object sender, EventArgs e)
        {
            ObjBEProyecto.id = Convert.ToInt32(LblID_PROYECTO.Text);
            if (LblID_SOLICITUD.Text == "")
            {
                ObjBEProyecto.Id_Solicitudes = 0;
            }
            else
            {
                ObjBEProyecto.Id_Solicitudes = Convert.ToInt32(LblID_SOLICITUD.Text);
            }
            ObjBEProyecto.Entidad_ejec = txtUnidadEjecutora.Text;

            int val = objBLProyecto.U_MON_UnidadEjecutora(ObjBEProyecto);

            if (val == 1)
            {
                string script = "<script>$('#modalEditarUE').modal('hide');alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                ObjBEProyecto.Id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text); ;
                dt = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dt.Rows.Count > 0)
                {
                    string UE = dt.Rows[0]["entidad_ejec"].ToString();
                    txtUnidadEjecutora.Text = UE;
                    lblUE.Text = " / UE: " + UE;
                    //txtMontoSNIPTab0.Text = dt.Rows[0]["CostoMVCS"].ToString();
                    //lblFinanObra.Text = dt.Rows[0]["costo_obra_MVCS"].ToString();
                    //lblFinanSuper.Text = dt.Rows[0]["costo_supervision_MVCS"].ToString();

                    //lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");
                    //lblFinanObra.Text = (Convert.ToDouble(lblFinanObra.Text)).ToString("N");
                    //lblFinanSuper.Text = (Convert.ToDouble(lblFinanSuper.Text)).ToString("N");
                }
                UPTabContainerDetalles.Update();



            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void BtnRefrescar_Click(object sender, EventArgs e)
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);
            DataTable dt = (new BLProyecto().spu_MON_UltimaActualizacion(idProy));
            if (dt.Rows.Count > 0)
            {
                LblUltimaModificacion.Text = Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortDateString() + " " + Convert.ToDateTime(dt.Rows[0]["FechaUltimaModificacion"]).ToShortTimeString();
                LblUsuario.Text = dt.Rows[0]["Usuario"].ToString();
            }
            else
            {
                LblUltimaModificacion.Text = "";
                LblUsuario.Text = "";
            }

        }


        protected void grdRiesgosTab9_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdRiesgosTab9.DataKeys[CurrentRow.RowIndex].Value as object;
                _BERiesgo.id_riesgo = Convert.ToInt32(objTemp.ToString());
                _BERiesgo.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                int val = _objBLRiesgo.spd_MON_Riesgos(_BERiesgo);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    CargaTab9();
                    Up_Tab9.Update();
                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }

            if (e.CommandName == "adjuntar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdRiesgosTab9.DataKeys[CurrentRow.RowIndex].Value as object;
                //_BERiesgo.id_riesgo = Convert.ToInt32(objTemp.ToString());

                hfID_RiesgoAdjuntar.Value = objTemp.ToString();
                Up_IdRiesgo.Update();
                MPE_ArchivoRiesgoTab9.Show();


            }
        }

        protected void imgbtnAgregaRiesgosTab9_Click(object sender, ImageClickEventArgs e)
        {
            if ((lblIdEstado.Text.Equals("41") && lblIdSubEstado.Text.Equals("70")) || lblIdEstado.Text.Equals("5") || lblIdEstado.Text.Equals("43") || lblIdEstado.Text.Equals("42") || lblIdEstado.Text.Equals(""))
            {
                string script = "<script>alert('Solo se puede registrar obras en estado: Actos Previos(Por Iniciar y Proceso de Selección), En Ejecución y Concluido.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {

                if (lblIdEstado.Text.Equals("41"))
                {
                    lblTitleRiesgo.Text = "Seguimiento a la Fase 01: Actos Previos";
                }
                else
                {
                    lblTitleRiesgo.Text = "Acta de Cumplimiento de Obligaciones Contractuales Esenciales";
                }
                lblEstadoRiesgoTab9.Text = lblNombreEstadoGeneral.Text;
                txtFechaRiesgoTab9.Text = "";

                btnGuardarFichaTab9.Visible = true;
                Up_ButtonFichaRiesgoTab9.Update();

                // Limpiar registro
                DataTable dtData = new DataTable();
                dtData = (DataTable)Session["FichasRiesgoTab9"];
                foreach (DataRow dr in dtData.Rows)
                {
                    if (dr["nivel"].ToString().Equals("1"))
                    {
                        HtmlTableRow tr = (HtmlTableRow)tblMitigacionTab9.FindControl("trRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                        HtmlTableCell tdRbl = (HtmlTableCell)tr.FindControl("tdRiesgo2Tab9i" + dr["id_itemRiesgo"].ToString());
                        RadioButtonList rbl = (RadioButtonList)tdRbl.FindControl("rbItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                        HtmlTableCell tdTxt = (HtmlTableCell)tr.FindControl("tdRiesgo3Tab9i" + dr["id_itemRiesgo"].ToString());
                        TextBox txt = (TextBox)tdTxt.FindControl("txtItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                        rbl.ClearSelection();
                        txt.Text = "";
                        if (dr["flagRegistrar"].ToString().Equals("1"))
                        {
                            txt.Enabled = true;
                            rbl.Enabled = true;
                        }
                        else
                        {
                            txt.Enabled = false;
                            rbl.Enabled = false;
                        }
                    }
                }
                string script = "<script>$('#modalRegistroRiesgoTab9').modal('show');MitigarRiesgoNroCaracteres();</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }

        protected void CargaTab9()
        {
            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            grdRiesgosTab9.DataSource = _objBLRiesgo.spMON_Riesgos(pIdProyecto);
            grdRiesgosTab9.DataBind();

            //ChartRiesgoTab9();
            Up_Tab9.Update();
        }

        protected void CargaPlantillaFichaRiesgoTab9(int pIdRiesgo)
        {

            int pIdProyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            DataTable dt = _objBLRiesgo.spMON_RiesgosFicha(pIdProyecto, pIdRiesgo);
            Session["FichasRiesgoTab9"] = dt;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    tr.ID = "trRiesgoTab9i" + dr["id_itemRiesgo"].ToString();

                    if (dr["nivel"].ToString().Equals("0"))
                    {
                        tr.Style["border"] = "1px solid";
                    }

                    if (lblIdEstado.Text.Equals("41"))
                    {
                        if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) > 33)
                            tr.Style["visibility"] = "collapse";
                    }
                    else
                    {
                        if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) < 34)
                            tr.Style["visibility"] = "collapse";
                    }

                    // NOMBRE de ITEM
                    HtmlTableCell td = new HtmlTableCell();
                    td.ID = "tdRiesgo1Tab9i" + dr["id_itemRiesgo"].ToString();
                    td.Attributes.Add("align", "left");
                    td.Style["padding-left"] = "10px";
                    td.Style["width"] = "40%";

                    Label lblNameTab9 = new Label();
                    lblNameTab9.ID = "lblItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString();
                    lblNameTab9.Text = dr["nro"].ToString() + dr["nombre"].ToString();

                    if (dr["nivel"].ToString().Equals("0"))
                    {
                        lblNameTab9.Style["font-weight"] = "bold";
                    }
                    td.Controls.Add(lblNameTab9);
                    tr.Controls.Add(td);

                    // SI NO
                    HtmlTableCell td2 = new HtmlTableCell();
                    td2.ID = "tdRiesgo2Tab9i" + dr["id_itemRiesgo"].ToString();
                    td2.Attributes.Add("align", "left");
                    td2.Style["padding-left"] = "10px";
                    td2.Style["width"] = "10%";

                    if (dr["nivel"].ToString().Equals("1"))
                    {
                        RadioButtonList rb = new RadioButtonList();
                        rb.ID = "rbItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString();
                        rb.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
                        rb.Items.Add(new ListItem("SI", "1"));
                        rb.Items.Add(new ListItem("NO", "0"));

                        if (dr["flagRegistrar"].ToString().Equals("0"))
                        {
                            rb.Enabled = false;
                        }
                        else
                        {
                            rb.Attributes.Add("onClick", "javascript:checkFlags('ContentPlaceHolder1_rbItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString() + "')");
                        }
                        td2.Controls.Add(rb);
                    }
                    tr.Controls.Add(td2);

                    //Comentario
                    HtmlTableCell td3 = new HtmlTableCell();
                    td3.ID = "tdRiesgo3Tab9i" + dr["id_itemRiesgo"].ToString();
                    td3.Attributes.Add("align", "left");
                    td3.Style["padding-left"] = "10px";
                    td3.Style["width"] = "50%";

                    if (dr["nivel"].ToString().Equals("1"))
                    {
                        TextBox txtComentario = new TextBox();
                        txtComentario.ID = "txtItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString();
                        txtComentario.TextMode = TextBoxMode.MultiLine;
                        txtComentario.Style["width"] = "100%";
                        if (dr["flagRegistrar"].ToString().Equals("0"))
                        {
                            txtComentario.Enabled = false;
                        }
                        else
                        {
                            if (dr["id_itemRiesgo"].ToString().Equals("35")
                                || dr["id_itemRiesgo"].ToString().Equals("36") || dr["id_itemRiesgo"].ToString().Equals("37")
                                || dr["id_itemRiesgo"].ToString().Equals("38") || dr["id_itemRiesgo"].ToString().Equals("39")
                                || dr["id_itemRiesgo"].ToString().Equals("40")
                                || dr["id_itemRiesgo"].ToString().Equals("42") || dr["id_itemRiesgo"].ToString().Equals("43")
                                || dr["id_itemRiesgo"].ToString().Equals("44"))
                            {
                                txtComentario.Enabled = false;
                                txtComentario.Attributes.Add("placeholder", "Adjuntar foto en el Panel Fotográfico.");
                                txtComentario.Attributes.Add("disabled", "disabled");
                            }
                        }
                        td3.Controls.Add(txtComentario);
                    }
                    tr.Controls.Add(td3);
                    //EN SANEAMIENTO NO SE CONSIDERA EL ITEM 1.5
                    //if (((lblCOD_SUBSECTOR.Text.Equals("1") || lblCOD_SUBSECTOR.Text.Equals("3")) && lblIdEstado.Text.Equals("41") && dr["id_itemRiesgo"].ToString().Equals("33"))==false) 
                    //{
                    //    tblMitigacionTab9.Controls.Add(tr);
                    //}
                    tblMitigacionTab9.Controls.Add(tr);

                }
            }

        }

        protected string VarificarRegistroRiesgoFichaTab9()
        {
            string mensaje = "";
            txtFechaRiesgoTab9.Text = (txtFechaRiesgoTab9.Text).Trim();

            if (lblIdEstado.Text == "")
            {
                mensaje = mensaje + "Debe registrar el estado del proyecto primero.\\n";
            }

            if ((lblIdEstado.Text.Equals("41") && lblIdEstado.Text.Equals("70")) || lblIdEstado.Text.Equals("5") || lblIdEstado.Text.Equals("43") || lblIdEstado.Text.Equals("42"))
            {
                mensaje = mensaje + "Solo se puede registrar obras en estado: Actos Previos(Por Iniciar y Proceso de Selección), En Ejecución y Concluido. \\n";
            }

            if (txtFechaRiesgoTab9.Text == "")
            {
                mensaje = mensaje + "Ingresar fecha.\\n";
            }
            else
            {
                if (_Metodo.ValidaFecha(txtFechaRiesgoTab9.Text) == false)
                {
                    mensaje = mensaje + "Formato no valido de fecha.\\n";
                }

                if (Convert.ToDateTime(txtFechaRiesgoTab9.Text) > DateTime.Today)
                {
                    mensaje = mensaje + "La fecha  ingresada es mayor a hoy.\\n";
                }
            }

            if (btnGuardarFichaTab9.Visible == true && grdRiesgosTab9.Rows.Count > 0)
            {
                foreach (GridViewRow row in grdRiesgosTab9.Rows)
                {
                    Label texto = ((Label)grdRiesgosTab9.Rows[row.RowIndex].FindControl("lblFecha"));
                    string strFecha = texto.Text;
                    if (texto.Text.Equals(txtFechaRiesgoTab9.Text))
                    {
                        mensaje = mensaje + "Fecha incorrecta, ya existe un registro con la misma fecha.\\n";
                    }
                }
            }

            DataTable dtFichas = new DataTable();
            dtFichas = (DataTable)Session["FichasRiesgoTab9"];

            int contActosPrevios = 0;

            foreach (DataRow dr in dtFichas.Rows)
            {
                if (dr["flagRegistrar"].ToString().Equals("1") && dr["nivel"].ToString().Equals("1"))
                {
                    HtmlTableRow tr = (HtmlTableRow)tblMitigacionTab9.FindControl("trRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                    HtmlTableCell tdRbl = (HtmlTableCell)tr.FindControl("tdRiesgo2Tab9i" + dr["id_itemRiesgo"].ToString());
                    RadioButtonList rbl = (RadioButtonList)tdRbl.FindControl("rbItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                    HtmlTableCell tdTxt = (HtmlTableCell)tr.FindControl("tdRiesgo3Tab9i" + dr["id_itemRiesgo"].ToString());
                    TextBox txt = (TextBox)tdTxt.FindControl("txtItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString());

                    if (Convert.ToInt32(dr["id_itemRiesgo"].ToString()) > 34)
                    {
                        if (rbl.SelectedValue == "")
                        {
                            mensaje = mensaje + "Punto " + dr["nro"].ToString() + " Ingrese SI o NO.\\n";
                        }
                    }
                    else
                    {
                        if (rbl.SelectedValue != "")
                        {
                            contActosPrevios = contActosPrevios + 1;
                        }

                    }
                    //if (txt.Text.Length < 10)
                    //{
                    //    mensaje = mensaje + "Punto " + dr["nro"].ToString() + " Ingrese comentario. Debe tener más de 10 caracteres.\\n";
                    //}
                }
            }

            if (lblIdEstado.Text.Equals("41") && contActosPrevios == 0)
            {
                mensaje = mensaje + "Seleccion una respuesta en la sección de Actos Previos.\\n";
            }

            return mensaje;
        }
        protected void btnGuardarFichaTab9_Click(object sender, EventArgs e)
        {

            DataTable dtFichas = new DataTable();
            dtFichas = (DataTable)Session["FichasRiesgoTab9"];

            string mensaje = VarificarRegistroRiesgoFichaTab9();

            if (mensaje.Length > 0)
            {
                string script = "<script>alert('" + mensaje + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
            else
            {
                _BERiesgo = new BE_MON_Riesgo();
                _BERiesgo.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                _BERiesgo.id_riesgo = 0;
                _BERiesgo.date_fecha = VerificaFecha(txtFechaRiesgoTab9.Text);
                _BERiesgo.id_estado = Convert.ToInt32(lblIdEstado.Text);
                _BERiesgo.id_subEstado = Convert.ToInt32(lblIdSubEstado.Text.Equals("") ? "0" : lblIdSubEstado.Text);
                _BERiesgo.observacion = txtObservacionRiesgoTab9.Text;
                _BERiesgo.urlActa = "";
                _BERiesgo.id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                int valIdRiesgo = _objBLRiesgo.spiu_MON_Riesgos(_BERiesgo);
                _BERiesgo.id_riesgo = valIdRiesgo;

                if (valIdRiesgo > 0)
                {
                    int valor = 0;
                    //Registra información
                    foreach (DataRow dr in dtFichas.Rows)
                    {
                        if (dr["flagRegistrar"].ToString().Equals("1") && dr["nivel"].ToString().Equals("1"))
                        {
                            HtmlTableRow tr = (HtmlTableRow)tblMitigacionTab9.FindControl("trRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                            HtmlTableCell tdRbl = (HtmlTableCell)tr.FindControl("tdRiesgo2Tab9i" + dr["id_itemRiesgo"].ToString());
                            RadioButtonList rbl = (RadioButtonList)tdRbl.FindControl("rbItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                            HtmlTableCell tdTxt = (HtmlTableCell)tr.FindControl("tdRiesgo3Tab9i" + dr["id_itemRiesgo"].ToString());

                            if (rbl.SelectedValue != "")
                            {
                                TextBox txt = (TextBox)tdTxt.FindControl("txtItemRiesgoTab9i" + dr["id_itemRiesgo"].ToString());
                                _BERiesgo.id_riesgo = valIdRiesgo;
                                _BERiesgo.id_item = Convert.ToInt32(dr["id_itemRiesgo"].ToString());
                                _BERiesgo.flagResultado = Convert.ToInt32(rbl.SelectedValue);
                                _BERiesgo.comentario = txt.Text;
                                _BERiesgo.id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                                valor = _objBLRiesgo.spiu_MON_RiesgosFicha(_BERiesgo);
                            }
                        }
                    }
                    if (valor == 1)
                    {
                        string script = "<script>$('#modalRegistroRiesgoTab9').modal('hide');alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaTab9();
                        Up_Tab9.Update();
                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }

        }

        protected void imgActaTab9_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewRow row;
            boton = (ImageButton)sender;
            row = (GridViewRow)boton.NamingContainer;
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();

            ImageButton url = (ImageButton)grdRiesgosTab9.Rows[row.RowIndex].FindControl("imgActaTab9");
            string urlDocumento = url.ToolTip;

            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }

        }

        protected void grdRiesgosTab9_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imb = (ImageButton)e.Row.FindControl("imgActaTab9");
                GeneraIcoFile(imb.ToolTip, imb);
            }
        }

        protected void btnSubirArchivoTab9_Click(object sender, EventArgs e)
        {
            if (FileUploadActaRiesgoTab9.HasFile)
            {
                if (validaArchivo(FileUploadActaRiesgoTab9))
                {
                    _BERiesgo = new BE_MON_Riesgo();
                    _BERiesgo.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BERiesgo.id_riesgo = Convert.ToInt32(hfID_RiesgoAdjuntar.Value);
                    _BERiesgo.date_fecha = VerificaFecha("");
                    _BERiesgo.observacion = "";
                    _BERiesgo.urlActa = _Metodo.uploadfile(FileUploadActaRiesgoTab9);
                    _BERiesgo.id_usuario = Convert.ToInt32(Session["IdUsuario"]);
                    int valIdRiesgo = _objBLRiesgo.spiu_MON_Riesgos(_BERiesgo);

                    if (valIdRiesgo > 0)
                    {
                        MPE_ArchivoRiesgoTab9.Hide();
                        CargaTab9();
                        string script = "<script>alert('Se adjuntó correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }
            }
            else
            {
                string script = "<script>alert('Debe adjuntar archivo.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
            }
        }

        protected void ddlSubEstado2Tab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaTipoSubEstadoParalizado(ddlSubEstado2Tab2.SelectedValue); //TIPO PROBLEMATICA
            Up_TipoProblematicaTab2.Update();
        }

        protected void CargaTipoSubEstadoParalizado(string pTipoEstadoParalizado)
        {
            chbProblematicaTab2.Items.Clear();

            if (ddlSubEstadoTab2.SelectedValue.Equals("82") || ddlSubEstadoTab2.SelectedValue.Equals("80"))//PERMANENTE  y Atrazado
            {
                List<BE_MON_BANDEJA> list = new List<BE_MON_BANDEJA>();

                if (!(pTipoEstadoParalizado == ""))
                {
                    list = _objBLEjecucion.F_spMON_TipoSubEstadoParalizado(Convert.ToInt32(pTipoEstadoParalizado));
                }
                else
                {
                    list = null;
                }

                chbProblematicaTab2.DataSource = list;
                chbProblematicaTab2.DataTextField = "nombre";
                chbProblematicaTab2.DataValueField = "valor";
                chbProblematicaTab2.DataBind();
            }
        }

        protected void ddlSubEstadoTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            trSuspensionTab2.Visible = false;
            trReinicioTab2.Visible = false;
            spSubestado2.Visible = false;
            ddlSubEstado2Tab2.Visible = false;

            if (ddlSubEstadoTab2.SelectedValue.Equals("87")) // SUSPENSION DE PLAZO
            {
                trSuspensionTab2.Visible = true;
                trReinicioTab2.Visible = true;
                Up_FechasSuspensionTab2.Update();

                spSubestado2.Visible = true;
                ddlSubEstado2Tab2.Visible = true;
                //Up_SubEstado2NombreTab2.Update();
                //Up_SubEstado2DetalleTab2.Update();
                CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
            }
            else if (ddlSubEstadoTab2.SelectedValue.Equals("80")) //ATRAZADO
            {
                spSubestado2.Visible = true;
                ddlSubEstado2Tab2.Visible = true;
                //Up_SubEstado2NombreTab2.Update();
                //Up_SubEstado2DetalleTab2.Update();
                CargaTipoSubEstado2(ddlSubEstadoTab2.SelectedValue);
            }

            Up_FechasSuspensionTab2.Update();
        }

        protected void grdEvaluacionTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdEvaluacionTab2.PageIndex = e.NewPageIndex;
            cargarEvaluacionTab2();
        }

        protected void grdAvanceFisicoAdicionalTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAvanceFisicoAdicionalTab2.PageIndex = e.NewPageIndex;
            CargaAvanceFisico();
        }

        protected void grdAvanceFisicoTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAvanceFisicoTab2.PageIndex = e.NewPageIndex;
            CargaAvanceFisico();
        }

        protected void grdRendicionCuentaNETab1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdRendicionCuentaNETab1.PageIndex = e.NewPageIndex;
            CargaRendicionCuentaNETab1();
        }

        protected void grdProgramacionTab2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProgramacionTab2.PageIndex = e.NewPageIndex;
            cargaProgramacionTab2();
        }

        void GuardarViviendas(int id) {
            //aca se inserta las viviendas
            int? Concluidas = null;
            int? EnEjecucion = null;
            int? PorIniciar = null;
            int? Paralizadas = null;

            if (TxtViviendasConcluidas.Text.Trim() != "")
                Concluidas = Convert.ToInt32(TxtViviendasConcluidas.Text);

            if (TxtViviendasEnEjecucion.Text.Trim() != "")
                EnEjecucion = Convert.ToInt32(TxtViviendasEnEjecucion.Text);

            if (TxtViviendasPorIniciar.Text.Trim() != "")
                PorIniciar = Convert.ToInt32(TxtViviendasPorIniciar.Text);

            if (TxtViviendasParalizadas.Text.Trim() != "")
                Paralizadas = Convert.ToInt32(TxtViviendasParalizadas.Text);

            int valor = _objBLEjecucion.paSSP_MON_EvaluacionRecomendacionViviendas(id, Concluidas, EnEjecucion, PorIniciar, Paralizadas);


        }

        protected void imgbtnFormatoCierreTabCierre_Click(object sender, ImageClickEventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFormatoCierreTabCierre.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

  
        protected void CargaFormatoTabCierre()
        {
            DataSet ds = _objBLFinanciamiento.spMON_rParametro(55);

            ddlFormatoCierreTabCierre.DataSource = ds.Tables[0];
            ddlFormatoCierreTabCierre.DataTextField = "nombre";
            ddlFormatoCierreTabCierre.DataValueField = "valor";
            ddlFormatoCierreTabCierre.DataBind();

            ddlFormatoCierreTabCierre.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void lnkbtnFormatoCierreTabCierre_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = lnkbtnFormatoCierreTabCierre.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void btnGuardarSostenibilidadTab3_Click(object sender, EventArgs e)
        {
            if (validaArchivo(FileUploadSostenibilidadTab3))
            {
                string msjVal = ValidarSostenibilidadTab3();
                if (msjVal.Length > 0)
                {
                    string script = "<script>alert('" + msjVal + "');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    _BELiquidacion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
                    _BELiquidacion.tipoFinanciamiento = 3;

                    _BELiquidacion.tipoReceptora = Convert.ToInt32(ddlTipoReceptoraTab3.SelectedValue);
                    _BELiquidacion.unidadReceptora = txtUnidadReceptoraTab3.Text;
                    _BELiquidacion.flagCapacidad = ddlflagCapacitadTab3.SelectedValue;
                    _BELiquidacion.flagOperatividad = ddlOperatividadTab3.SelectedValue;

                    _BELiquidacion.Date_Fecha = VerificaFecha(txtFechaActaTransferenciaTab3.Text);
                    if (FileUploadSostenibilidadTab3.HasFile)
                    {
                        _BELiquidacion.urlDocSostenibilidad = _Metodo.uploadfile(FileUploadSostenibilidadTab3);
                    }
                    else
                    { _BELiquidacion.urlDocSostenibilidad = LnkbtnSostenibilidadTab3.Text; }
                    _BELiquidacion.comentarioCapacidad = txtComentarioTab3.Text;

                    _BELiquidacion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

                    int val = _objBLLiquidacion.IU_MON_SostenibilidadPorContrata(_BELiquidacion);


                    if (val == 1)
                    {
                        string script = "<script>alert('Se registró correctamente.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        CargaLiquidacion();

                    }
                    else
                    {
                        string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    }


                    Up_Tab3.Update();
                }
            }
        }

        protected String ValidarSostenibilidadTab3()
        {
            string msj = "";


            if (ddlTipoReceptoraTab3.SelectedValue == "")
            {
                msj = msj + "*Seleccionar tipo de receptora.\\n";
            }

            if (txtUnidadReceptoraTab3.Text.Count() < 2)
            {
                msj = msj + "*Ingresar nombre de entidad receptora.\\n";
            }
            if (ddlflagCapacitadTab3.SelectedValue == "")
            {
                msj = msj + "*Responder si la entidad receptora cuenta con capacidad para administrar el proyecto.\\n";
            }

            if (ddlOperatividadTab3.SelectedValue == "")
            {
                msj = msj + "*Responder si se encuentra operativa.\\n";
            }
            if (txtFechaActaTransferenciaTab3.Text == "")
            {
                msj = msj + "*Ingresar fecha acta transferencia fisica de la obra.\\n";
            }

            if (_Metodo.ValidaFecha(txtFechaActaTransferenciaTab3.Text) == false)
            {
                msj = msj + "*Formato de fecha de acta transferencia fisica de la obra no valido.\\n";
            }

            if (txtFechaTab3.Text.Length > 0 && Convert.ToDateTime(txtFechaActaTransferenciaTab3.Text) < Convert.ToDateTime(txtFechaTab3.Text))
            {
                msj = msj + "*La fecha de acta transferencia fisica de la obra debe ser mayor o igual a la fecha de liquidación.\\n";
            }
            if (txtFechaFormato14TabCierre.Text.Length > 0 && Convert.ToDateTime(txtFechaActaTransferenciaTab3.Text) > Convert.ToDateTime(txtFechaFormato14TabCierre.Text))
            {
                msj = msj + "*La fecha de acta transferencia fisica de la obra debe ser menor o igual a la fecha de registro de formato del cierre del proyecto.\\n";
            }
            return msj;

        }

        protected void imgbtnSostenibilidadTab3_Click(object sender, ImageClickEventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkbtnSostenibilidadTab3.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void LnkbtnSostenibilidadTab3_Click(object sender, EventArgs e)
        {
            string pat = ConfigurationManager.AppSettings["DocumentosMonitor"].ToString();
            string urlDocumento = LnkbtnSostenibilidadTab3.Text;
            FileInfo toDownload = new FileInfo(pat + urlDocumento);
            if (toDownload.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length", toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(pat + urlDocumento);
                Response.End();
            }
        }

        protected void cargaTipoReceptora()
        {
            ddlTipoReceptoraTab3.DataSource = _objBLLiquidacion.F_spMON_TipoReceptora();
            ddlTipoReceptoraTab3.DataTextField = "nombre";
            ddlTipoReceptoraTab3.DataValueField = "valor";
            ddlTipoReceptoraTab3.DataBind();

            ddlTipoReceptoraTab3.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        protected void lnkbtnBuscarDNIPadron_Click(object sender, EventArgs e)
        {
            BLUtil util = new BLUtil();

            
            string ip = _Metodo.GetIPAddress();
            string url = String.Format(ConfigurationManager.AppSettings["UrlPIDEBackend"]);
            string pJson = _Metodo.PrepareJsonParams(new { pAccion = "1", pContrato = "12", pServicio = "2", pIdSistema = "4", pIp = ip, nuDniConsulta = txtNroDocumentoPATab1.Text, CodArea = "0", codUsuario = LblID_USUARIO.Text, vNomUsuario = "SSP", vArea = "SSP" });
            string respuesta = _Metodo.invokePost(url, pJson);

            try
            {
                Newtonsoft.Json.Linq.JObject JObject = Newtonsoft.Json.Linq.JObject.Parse(respuesta);

                string ErrorCode = JObject["data"]["error"]["xidError"].ToString();
                string msjError = JObject["data"]["error"]["msjError"].ToString();

                if (ErrorCode == "200")
                {
                    string length = JObject["data"]["objects"]["table1"]["length"].ToString();
                    string rpta = JObject["data"]["objects"]["table1"]["data"].ToString();
                    List<BLUtil.DatosDNI> Lista = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BLUtil.DatosDNI>>(rpta);

                    if (length != "0")
                    {
                        txtNombrePATab1.Text = Lista[0].prenombres;
                        txtApellidoPaternoPATab1.Text = Lista[0].apPrimer;
                        txtApellidoMaternoPATab1.Text = Lista[0].apSegundo;
                        txtDomicilioPATab1.Text = Lista[0].direccion;
                        string pUbigeo = Lista[0].ubigeo;

                        string[] ListUbigeo = pUbigeo.Split('/');

                        try
                        {
                            //ddlDepaPATab1.SelectedItem.Text = ListUbigeo[0].ToString();
                            //txtDepartamento.Text = ListUbigeo[0].ToString();
                            //txtProvincia.Text = ListUbigeo[1].ToString();
                            //txtDistrito.Text = ListUbigeo[2].ToString();

                            //ddlDepaPATab1.SelectedValue = lblIdUbigeo.Text.Substring(0, 2);
                            CargaUbigeo("2", ddlDepaPATab1.SelectedValue, "", "", ddlProvPATab1);
                            //ddlProvPATab1.SelectedItem.Text = ListUbigeo[1].ToString();
                            CargaUbigeo("3", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, "", ddlDistPATab1);
                            //ddlDistPATab1.SelectedValue = lblIdUbigeo.Text.Substring(4, 2);
                            CargaUbigeo("4", ddlDepaPATab1.SelectedValue, ddlProvPATab1.SelectedValue, ddlDistPATab1.SelectedValue, ddlCCPPPATab1);
                        }
                        catch { }
                    }
                }
                else
                {
                    string phtml = "<p class='bg-danger paddingSM'>" + "DNI no encontrado. Verificar valor ingresado." + "</p>";
                    divMsjBusqueda.InnerHtml = phtml;

                    txtNombrePATab1.Text = "";
                    txtApellidoPaternoPATab1.Text = "";
                    txtApellidoMaternoPATab1.Text = "";
                }
            }
            catch (Exception ex)
            {
                string phtml = "<p class='bg-danger paddingSM'>" + "DNI no encontrado. Verificar valor ingresado." + "</p>";
                divMsjBusqueda.InnerHtml = phtml;
                txtNombrePATab1.Text = "";
                txtApellidoPaternoPATab1.Text = "";
                txtApellidoMaternoPATab1.Text = "";
            }

        }

        protected void ddlTipoDocumentoPATab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            HabilitarDNIPadronNE();
        }

        protected void HabilitarDNIPadronNE()
        {
            if (ddlTipoDocumentoPATab1.SelectedValue.Equals("1")) //DNI
            {
                lnkbtnBuscarDNIPadron.Visible = true;
                txtApellidoPaternoPATab1.Enabled = false;
                txtApellidoMaternoPATab1.Enabled = false;
                txtNombrePATab1.Enabled = false;
            }
            else
            {
                lnkbtnBuscarDNIPadron.Visible = false;

                txtApellidoPaternoPATab1.Enabled = true;
                txtApellidoMaternoPATab1.Enabled = true;
                txtNombrePATab1.Enabled = true;
            }
        }

        protected void btnFinalizarTab3_Click(object sender, EventArgs e)
        {
            if (lblIdEstadoRegistro.Text.Equals("2")) //ACTIVO
            {
                _BEEjecucion.id_estadoProyecto = 3;
            }
            else
            {
                _BEEjecucion.id_estadoProyecto = 2;
            }
            _BEEjecucion.id_proyecto = Convert.ToInt32(LblID_PROYECTO.Text);
            _BEEjecucion.id_usuario = Convert.ToInt32(LblID_USUARIO.Text);

            int val = _objBLEjecucion.spu_MON_EstadoProyecto(_BEEjecucion);

            if (val == 1)
            {
                //CargaLiquidacion();

                string script = "<script>alert('Se Actualizó Correctamente'); window.location.reload()</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }
    }
}