﻿<%@ Page Title="SIAF" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="CargaSiaf_PNSR.aspx.cs" Inherits="Web.Monitor.CargaSiaf_PNSR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="Grid" style="text-align: center">
        <br />
        <p>
            <span class="titlePage">REGISTRAR INFORMACIÓN SIAF </span>
        </p>
        <br />
        <br />
    </div>

    <br />
  

    <div>
        <table border="0" style="margin: auto; border-spacing: 15px 10px; border-collapse:separate">

             <tr>
                <td>
                    <label class="control-label">Formato&nbsp;&nbsp;&nbsp;</label>
                </td>
                <td>
                    <div style="text-align: left; text-decoration: underline; color: #0e2e8c;">
                        <asp:HyperLink ID="HyperLink1" Font-Size="14px" NavigateUrl="~/Documentos/SiafPNSR(formato).xlsx" runat="server">
                                <i class="fa fa-download" aria-hidden="true"></i>Descargar</asp:HyperLink>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label">Año </label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlAnio" CssClass="form-control" >
                        <asp:ListItem Value="2021"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label">Mes </label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlMes" CssClass="form-control" >
                        <asp:ListItem Value="01"></asp:ListItem>
                        <asp:ListItem Value="02"></asp:ListItem>
                        <asp:ListItem Value="03"></asp:ListItem>
                        <asp:ListItem Value="04"></asp:ListItem>
                        <asp:ListItem Value="05"></asp:ListItem>
                        <asp:ListItem Value="06"></asp:ListItem>
                        <asp:ListItem Value="07"></asp:ListItem>
                        <asp:ListItem Value="08"></asp:ListItem>
                        <asp:ListItem Value="09"></asp:ListItem>
                        <asp:ListItem Value="10"></asp:ListItem>
                        <asp:ListItem Value="11"></asp:ListItem>
                        <asp:ListItem Value="12"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                  
                <td>
                    <label class="control-label">Archivo </label>
                </td>
                <td style="text-align: left">

                    <label class="file-upload">
                        <asp:FileUpload ID="FileUpload1" runat="server"></asp:FileUpload>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <asp:Button Text="REGISTRAR" runat="server" OnClick="Upload_Click" CssClass="btn btn-primary" />
                </td>

                <td style="width: 30%">
                   
                </td>
            </tr>
            <tr>
                <td colspan="2"  align="left" >
                    <br />
                     
                </td>

            </tr>
        </table>
    </div>

     <hr />
    <div runat="server" visible="false" id="divErrorCarga" style="margin: auto; text-align: center">
        <%--<h6>Los siguientes registros no se cargaron. Por favor verifique en el excel</h6>--%>
        <asp:GridView ID="grillaErrorCarga" runat="server" AutoGenerateColumns="False"
            CellPadding="3" CellSpacing="1" CssClass="marginAuto"
            BorderColor="#CCCCCC" Font-Names="Calibri" Font-Size="Small" ForeColor="#333333" GridLines="None">
            <EditRowStyle BackColor="#999999" />
            <FooterStyle ForeColor="White"
                BackColor="#5D7B9D" Font-Bold="True"></FooterStyle>
            <PagerStyle ForeColor="White"
                HorizontalAlign="Center" BackColor="#284775"></PagerStyle>
            <HeaderStyle ForeColor="White" Font-Bold="True" Height="30px"
                BackColor="#5D7B9D"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <%--<asp:BoundField HeaderText="#" DataField="Numero" />--%>
                <asp:BoundField HeaderText="Nombre" DataField="nombre" />
                <%--<asp:BoundField HeaderText="Activo" DataField="activo" />--%>
                <asp:BoundField HeaderText="Tipo Programa" DataField="tipoPrograma" />
                <asp:BoundField HeaderText="Correo" DataField="correo" />
                <asp:BoundField HeaderText="Tipo Agente" DataField="tipoAgente" />
                <asp:BoundField HeaderText="DNI" DataField="dni" />
                <asp:BoundField HeaderText="Tipo Profesion" DataField="tipoProfesion" />
                <asp:BoundField HeaderText="Nro Colegiatura" DataField="nroColegiatura" />
                <asp:BoundField HeaderText="Detalle Error" DataField="Detalle_Error" />

            </Columns>
            <SelectedRowStyle ForeColor="#333333" Font-Bold="True" Font-Names="Calibri"
                BackColor="#E2DED6"></SelectedRowStyle>
            <RowStyle ForeColor="#333333" Font-Names="Calibri"
                BackColor="#F7F6F3"></RowStyle>
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

        </asp:GridView>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceFooter" runat="server">
</asp:Content>
