﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Web.UI;
using Business;
using Entity;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class PanelFotografico_Visita : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_SOSEM _objBLSOSEM = new BL_MON_SOSEM();
        BE_MON_SOSEM _BESOSEM = new BE_MON_SOSEM();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Conclusion _objBLConclusion = new BL_MON_Conclusion();
        BE_MON_Conclusion _BEConclusion = new BE_MON_Conclusion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();


        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BE_MON_VarPNSU _BEVarPNSU = new BE_MON_VarPNSU();

        BL_MON_Calidad _objBLCalidad = new BL_MON_Calidad();
        BE_MON_Calidad _BECalidad = new BE_MON_Calidad();

        BL_MON_Finan_Transparencia _objBLFinaTra = new BL_MON_Finan_Transparencia();
        BE_MON_Finan_Transparencia _BEFinaTra = new BE_MON_Finan_Transparencia();

        DataTable dt = new DataTable();

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Grabar_Log();
                }
                catch (Exception)
                {
                    //throw;
                }

                lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";
            }

            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }

            CargaInformacionGeneral();


        }


        //modificando
        protected void CargaInformacionGeneral()
        {


            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["idPo"].ToString());
            _BELiquidacion.tipoFinanciamiento = 3;
            dt = _objBLLiquidacion.spMON_Ayuda_Memoria(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
            }

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(Request.QueryString["idE"].ToString());
            dt = _objBLEjecucion.spMON_EvaluacionRecomendacionActaVisita(_BEEjecucion);
            lblFechaInforme.Text = dt.Rows[0]["fechaVisita"].ToString();

            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(Request.QueryString["idE"].ToString());
            dt = _objBLEjecucion.spMON_DetalleFotoVisita(_BEEjecucion);
            int total = dt.Rows.Count;
            if (total > 0)
            {
                int i = 1;
                string vFoto = "";
                StringBuilder strFotos = new StringBuilder();
                strFotos.Append("<table  class='table2 table-bordered' cellpadding='5' style='width: 96%'>");
                foreach (DataRow dr in dt.Rows)
                {
                    vFoto = "<img BorderStyl='Solid' BorderWidth='4px' Width='350px' BorderColor='#c8130b' src='FotoPMIB.ashx?img=" + dr["urlFoto"] + "' />";

                    if (i % 2 != 0)
                    {
                        strFotos.Append("<tr>");
                        strFotos.Append(string.Format("<td>" + vFoto + "<br />" + dr["descripcion"] + "</td>"));

                        if (total == i)
                        {
                            strFotos.Append("</tr>");
                        }
                    }
                    else
                    {
                        strFotos.Append(string.Format("<td>" + vFoto + "<br />" + dr["descripcion"] + "</td>"));
                        strFotos.Append("</tr>");
                    }

                    i = i + 1;
                }

                strFotos.Append("</table>");
                lblFotos.Text = strFotos.ToString();


                string j = dt.Rows[0]["urlFoto"].ToString();

            }
            else
            {
                lblFotos.ForeColor = System.Drawing.Color.Red;
                lblFotos.Text = "<br /><br /><div class='alert-warning'><b>NOTA</b> </br></br>";
                lblFotos.Text = lblFotos.Text + "No se registró ninguna foto.</div><br /><br />";
            }

        }

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "PanelFotografico_SNIP_" + lblSnip.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }
    }
}