﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ficha_MitigacionRiesgo_Plantilla.aspx.cs" Inherits="Web.Monitor.Ficha_MitigacionRiesgo_Plantilla" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        BODY {
            font-family: HelveticaNeue;
        }

        @font-face {
            font-family: HelveticaNeue;
            src: local('HelveticaNeue'), url("../css/fuente/HelveticaNeue-Medium.otf") format("opentype");
        }

        .auto-style1 {
            text-align: left;
            border: 1px dotted gray;
            height: 67px;
        }

        @media print {
            #imgbtnImprimir, #imgbtnExportar, #imgbtnExportarMobile {
                display: none;
            }
        }

        table {
            border-spacing: 0;
            border-collapse: collapse;
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <center>
            <div id="div_Ficha" style="width: 800px; height: auto;">
                <center>
       
                    <table style="margin: auto; margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td rowspan="2">
                                <img src="../img/logo_ministerio.jpg" height="50"  alt="" style="padding-right:15px;padding-left:15px;" />
                            </td>
                        </tr>

                    </table>

                    <span style="font-family: Arial; font-size: 16pt">
                        <asp:Label runat="server" ID="lblTitle" Font-Bold="true">
                        </asp:Label>
                    </span>
                    <br />
                    <asp:Label runat="server" Font-Size="9pt" Text="" ID="lblFechaAyuda"></asp:Label>
                    <asp:Label runat="server" Text="0" ID="LblID_PROYECTO" Visible="false"></asp:Label>
                    <asp:Label runat="server" Text="0" ID="lblID_Programa" Visible="false"></asp:Label>
                    <asp:Label runat="server" Text="0" ID="lblID_TipoFinanciamiento" Visible="false"></asp:Label>
                                       
                    <div id="icon_word">
                        <center>
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" Width="25px" Height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                                        <asp:ImageButton ID="imgbtnExportarMobile" runat="server" ImageUrl="~/img/download_48.png" width="90%" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />
                                    </td>
                                  <%--  <td>
                                        <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" Width="25px" Height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />

                                    </td>--%>
                                </tr>
                            </table>
                        </center>

                    </div>
                   
                </center>


                <div style="width: 100%">
                    <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray; width: 148px;">C&Oacute;DIGO DE PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray;"><b>
                                <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label></b></td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">NOMBRE PROYECTO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr runat="server" id ="trTransferencia">
                            <td class="celdaTabla" style="border: 1px dotted gray;">MONTO DE TRANSFERENCIA TOTAL</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblMontoTransferido" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">BENEFICIARIO</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblBeneficiario" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblhabitantes" runat="server" Text="Habitantes (Fuente Formato SNIP 03)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">UNIDAD EJECUTORA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblUnidadEjecutora" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server" id ="trModalidad">
                            <td class="celdaTabla" style="border: 1px dotted gray">MODALIDAD DE EJECUCIÓN</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblModalidadEjecucion" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">FECHA DE VISITA</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <input type="text" style="width:140px; height:25px" />
                            </td>
                        </tr>

                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray">ESTADO DE EJECUCIÓN</td>
                            <td class="celdaTabla" style="border: 1px dotted gray">
                                <asp:Label ID="lblEstado" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>

                    <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%; margin-top:20px; border: 1px dotted gray" cellpadding="5" runat="server" id="tblMitigacionTab9">
                        </table>
                    <br />
                   
                     <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                        <tr>
                            <td class="celdaTabla" style="border: 1px dotted gray; width: 148px;">OBSERVACIÓN</td>
                            <td class="celdaTabla" style="border: 1px dotted gray;">
                                <input type="text" style="height:150px; width:100%" />
                                <asp:label ID="lblObservacion" runat="server" Text=""></asp:label>

                            </td>
                        </tr>
                     </table>

                    <table style="text-align: left; margin-top: 10px; font-family:Calibri; font-size:14px" width="100%" id="tblFirmas" runat="server">
                        <tr>
                            <td colspan="3">
                                <span><b>Nota Importante 1:</b>
                                    <asp:Label runat ="server" ID="lblNota1" Text="La presente acta es elaborada en función de la información proporcionada por la Unidad Ejecutora (Contratista y Supervisión), teniendo en cuenta presunción de la veracidad
reflejado en la tomas fotográficas, y en la suscripción del acta respectiva."></asp:Label></span><br /><br />
                                <span><b>Nota Importante 2:</b> Una vez registrado el formato debe ser firmado, escaneado y adjuntado en el SSP.</span>
                                <br /><br /><br />
                                <span>En el distrito ............................... siendo las ....................... se firma la presente acta en señal de conformidad.</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 160px; text-align:center;">_____________________________________
                                <br />
                                Monitor MVCS
                            </td>
                            <td style="width: 10%; text-align:center;"></td>
                            <td style="text-align:center">_____________________________________
                                <br />
                                <asp:Label runat="server" ID="lblPersonal2" Text="Supervisor Obra (Personal Clave)"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">_____________________________________
                                <br />
                               <asp:Label runat="server" ID="lblPersonal3" Text="Otro Personal Clave (Indicar)"></asp:Label>
                            </td>
                            <td style="width: 10%; text-align:center"></td>
                            <td style="text-align:center">_____________________________________
                                <br />
                                <asp:Label runat="server" ID="lblPersonal4" Text="Residente de Obra (Personal Clave)"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <br />
            </div>

        </center>
    </div>
    </form>
</body>
</html>
