﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Data;

namespace Web.Monitor
{
    public partial class Seace_Cronograma : System.Web.UI.Page
    {
        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        DataTable dt = new DataTable();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CargaInformacionGeneral();
            }
        }

        protected void CargaInformacionGeneral()
        {
            int identificador = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;

            DataSet ds = new DataSet();
            ds = _objBLFinanciamiento.spMON_SEACE_Cronograma(identificador);
            
            grdCronograma.DataSource = ds.Tables[0];
            grdCronograma.DataBind();

            if (ds.Tables[1].Rows.Count > 0)
            {
                string html="";
                string version="";
                int totalRow = ds.Tables[1].Rows.Count;
                int contador = 0;

                int Item = 1;

                html = html + "<table class='table table-bordered' style='width: 98%;'><tr><th>Nro</th><th>Etapa</th><th style='width: 115px'>Fecha Inicio</th><th>Fecha Fin</th></tr>";

                foreach (DataRow row in ds.Tables[1].Rows)
                {
                    if (version == row["VERSION_CRONOGRAMA"].ToString() || contador==0) //Igual al anterior
                    {
                        html = html + "<tr><td style='text-align:center'>" + Item + "</td><td>" + row["ETAPA"].ToString() + " </td><td>" + row["FECHA_INICIO"].ToString() + "</td><td>" + row["FECHA_FIN"].ToString() + "</td></tr>";
                        Item = Item + 1;
                    }
                    else 
                    {
                        Item = 1;
                        html = html + "</table></br>";
                        html = html + "<table class='table table-bordered' style='width: 98%;'><tr><th>Nro</th><th>Etapa</th><th style='width: 115px'>Fecha Inicio</th><th>Fecha Fin</th></tr>";
                        html = html + "<tr><td style='text-align:center'>" + Item + "</td><td>" + row["ETAPA"].ToString() + " </td><td>" + row["FECHA_INICIO"].ToString() + "</td><td>" + row["FECHA_FIN"].ToString() + "</td></tr>";
                      
                    }
                    version = row["VERSION_CRONOGRAMA"].ToString();
                    contador = contador +1;
                }
                html = html + "</table></br>";

                lblTabla.Text = html;

                //grvHistorial.DataSource = ds.Tables[1];
                //grvHistorial.DataBind();
            }
            else
            {
                lbltitHistorico.Visible = false;
                grvHistorial.Visible = false;
            }

        }

    }
}