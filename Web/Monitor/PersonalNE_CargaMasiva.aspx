﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonalNE_CargaMasiva.aspx.cs" Inherits="Web.Monitor.PersonalNE_CargaMasiva" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <table border="0" style="width: 100%">
                <tr>
                    <td style="width: 70%">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <asp:Button Text="Cargar" runat="server" OnClick="Upload_Click" />
                    </td>

                    <td style="width: 30%">
                       <div style="text-align: right">
                            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Documentos/TecnicoPersonalNE(formato).xlsx" runat="server">Descargar Formato</asp:HyperLink>
                       </div>
                    </td>
                </tr>
          </table> 
    </div>

        <hr />
        <div runat="server" Visible="false" id="divErrorCarga">
            <%--<h6>Los siguientes registros no se cargaron. Por favor verifique en el excel</h6>--%>
            <asp:GridView ID="grillaErrorCarga" runat="server" AutoGenerateColumns="False"
                CellPadding="3" CellSpacing="1"
                BorderColor="#CCCCCC" Font-Names="Calibri" Font-Size="Small" ForeColor="#333333" GridLines="None">
                <EditRowStyle BackColor="#999999" />
                <FooterStyle ForeColor="White"
                    BackColor="#5D7B9D" Font-Bold="True"></FooterStyle>
                <PagerStyle ForeColor="White"
                    HorizontalAlign="Center" BackColor="#284775"></PagerStyle>
                <HeaderStyle ForeColor="White" Font-Bold="True"
                    BackColor="#5D7B9D"></HeaderStyle>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <%--<asp:BoundField HeaderText="#" DataField="Numero" />--%>
                    <asp:BoundField HeaderText="Nombre" DataField="nombre" />
                    <%--<asp:BoundField HeaderText="Activo" DataField="activo" />--%>
                    <asp:BoundField HeaderText="Tipo_Programa" DataField="tipoPrograma" />
                    <asp:BoundField HeaderText="Correo" DataField="correo" />
                    <asp:BoundField HeaderText="Tipo Agente" DataField="tipoAgente" />
                    <asp:BoundField HeaderText="DNI" DataField="dni" />
                    <asp:BoundField HeaderText="Tipo Profesion" DataField="tipoProfesion" />
                    <asp:BoundField HeaderText="Nro Colegiatura" DataField="nroColegiatura" />
                   <asp:BoundField HeaderText="Detalle Error" DataField="Detalle_Error" />
                   
                </Columns>
                <SelectedRowStyle ForeColor="#333333" Font-Bold="True" Font-Names="Calibri"
                    BackColor="#E2DED6"></SelectedRowStyle>
                <RowStyle ForeColor="#333333" Font-Names="Calibri"
                    BackColor="#F7F6F3"></RowStyle>
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>

        
    
    </form>
</body>
</html>
