﻿<%@ Page Title="Banco Monitoreo" Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="Banco_Monitoreo.aspx.cs"
    Inherits="Web.Monitor.Banco_Monitoreo" Culture="es-PE" UICulture="es-PE" EnableEventValidation="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%= ResolveUrl("~/sources/select2/css/select2.min.css") %>" rel="stylesheet" />
    <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .tablaRegistro {
            line-height: 15px;
        }

        .tdFiltro {
            font-weight: bold;
            font-size: 12px;
        }

        .titulo {
            font-size: 14pt;
            font-family: 'DINB';
            color: #0094D4;
            line-height: 20px;
        }

        .tituloTipo {
            font-size: 13pt;
            font-family: 'DINB';
            color: #000000;
        }

        .titulo2 {
            font-family: 'DINB';
            font-size: 12pt;
            color: #28375B;
        }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            /*position: absolute;*/
        }

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        .user66 {
            position: absolute;
            bottom: 0;
            left: 30px;
        }

        .tablaMsj {
            border-collapse: collapse;
        }

            .tablaMsj td {
                border: 1px solid #c94e4e;
                padding: 2px;
            }

            .tablaMsj th {
                border: 1px solid #c94e4e;
                padding: 2px;
                font-weight: bold;
            }

        .CuadrosEmergentes {
            border: 1px solid #28375B;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

        .PopUp {
            background-color: #ffffff;
            border-width: 1px;
            border-color: #606060;
            border-style: solid;
            padding: 20px;
        }

        legend {
            font-size: 15px !important;
        }

        .fieldGrupo fieldset {
            border: 1px solid silver !important;
            display: block !important;
            margin-inline-start: 2px !important;
            margin-inline-end: 2px !important;
            padding-block-start: 0.35em !important;
            padding-inline-start: 0.75em !important;
            padding-inline-end: 0.75em !important;
            padding-block-end: 0.625em !important;
            min-inline-size: min-content !important;
            /* min-width: 0; */
            /* padding: 0; */
            /* margin: 0; */
            /* border: 0; */
        }

            .fieldGrupo fieldset legend {
                margin-bottom: 11px !important;
            }

            .fieldGrupo fieldset legend {
                margin-bottom: 11px !important;
            }

        legend {
            font-size: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" lang="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.gif" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; font-weight: bold; font-size: 12px">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

    <div class="Grid" style="margin: auto; text-align: center">
        <br />
        <p>
            <span class="titlePage">REGISTRO DE INVERSIONES</span>
        </p>
    </div>
    <br />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnNuevoRegistro" runat="server" Text="NUEVO" OnClick="btnNuevoRegistro_Click" CssClass="btn btn-primary" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxGridView ID="xgrdBanco" runat="server" AutoGenerateColumns="False" Width="98%" Font-Size="11px" Theme="MetropolisBlue" KeyFieldName="id_proyecto">
                            <Columns>

                                <dx:GridViewDataColumn Caption="EDITAR" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                                    <DataItemTemplate>
                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" OnClick="lnkbtnEdit_Click" CssClass="btn btn-default" ToolTip="Actualizar">
                                           <i class="fa fa-edit"></i>
                                        </asp:LinkButton>
                                    </DataItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                </dx:GridViewDataColumn>

                                <dx:GridViewDataTextColumn Caption="ID" FieldName="id_proyecto" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="PERIODO" FieldName="Anio" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="UNIDAD" FieldName="sector" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="SUB UNIDAD" FieldName="subPrograma" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList" Visible="false">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="SNIP" FieldName="snip" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="UNIFICADO" FieldName="CodigoUnificado" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="NOMBRE DEL PROYECTO" FieldName="nombProyecto" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Left"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="UEI" FieldName="unidadEjecutora" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ETAPA" FieldName="tipo" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList">
                                    <Settings HeaderFilterMode="CheckedList"></Settings>

                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="MONTO VIABLE (S/.)" FieldName="monto_SNIP" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataColumn Caption="ACT." HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" HeaderStyle-Wrap="True" Visible="false">
                                    <DataItemTemplate>
                                        <img src="../img/update32x32.png" alt="Evaluación" style="cursor: pointer" />
                                    </DataItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle Wrap="True" HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataColumn>

                                <dx:GridViewDataTextColumn Caption="AMBITO" FieldName="observacion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="BENEFICIARIO" FieldName="poblacionSnip" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataCheckColumn Caption="RECONSTRUCCIÓN" FieldName="flagReconstruccion" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataCheckColumn Caption="SERVICIO" FieldName="flagServicio" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                            <Settings ShowFooter="True" ShowHeaderFilterButton="true" ShowHeaderFilterBlankItems="true" ShowFilterRow="True" />
                            <Styles Header-BackColor="#2D7BBD" Header-ForeColor="White">
                                <Header BackColor="#2D7BBD" ForeColor="White"></Header>
                            </Styles>
                        </dx:ASPxGridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal New-->
    <div class="modal fade" id="modalNuevoProyecto" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">REGISTRO DE INVERSIÓN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="width: 100%; height: 600px; overflow-y: scroll;">
                        <asp:UpdatePanel runat="server" ID="UpIngreso">
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="Panel_Registro" DefaultButton="btnGrabarBanco">
                                    <center>
                                        <table width="100%" id="tb_uno_nuevo" style="border-collapse: separate !important; border-spacing: 6px;" border="0" cellspacing="10" cellpadding="0" runat="server">
                                            <tr>
                                                <td align="left" style="width: 123px; font-weight: bold">Código :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox CssClass="texto" ID="txtBuscarCodigo" runat="server" MaxLength="10" placeholder="Ingresar código inversión"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" FilterType="Numbers" TargetControlID="txtBuscarCodigo"
                                                        runat="server"></asp:FilteredTextBoxExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtBuscarCodigo"
                                                        Display="Dynamic" ErrorMessage="Ingresar SNIP" ValidationGroup="excel">*</asp:RequiredFieldValidator>

                                                    <asp:Button ID="btnmef" runat="server" CssClass="btn btn-secondary" OnClick="btnValidarMef_Click" Text="VALIDAR" />
                                                    <asp:Button ID="btnNuevoValidar" runat="server" CssClass="btn btn-default" OnClick="btnNuevoValidar_Click" Text="BUSCAR OTRO" Visible="false" />
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="lblbtnMef" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: left">
                                                    <asp:Label ID="lblAlerta" runat="server" Text=""></asp:Label>
                                                    <asp:GridView runat="server" ID="grdSnipHistorial" AllowPaging="true"
                                                        AutoGenerateColumns="False" Visible="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblIdFinanciamiento" Text='<%# Eval("tipoFinanciamiento") %>' runat="server"></asp:Label>
                                                                    <asp:Label ID="lblIdEstado" Text='<%# Eval("id_TipoEstadoEjecucion") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">

                                                    <div class="card card-info">
                                                        <table style="border-collapse: separate !important; border-spacing: 6px;" cellspacing="10" cellpadding="0">
                                                            <tr>
                                                                <td align="left">SNIP:
                                                                </td>
                                                                <td colspan="3" align="left">
                                                                    <asp:TextBox CssClass="texto" ID="TxtSnipConsultado" runat="server" Width="90px"></asp:TextBox>&nbsp;&nbsp;
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" FilterType="Numbers" TargetControlID="TxtSnipConsultado" runat="server"></asp:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="TxtSnipConsultado"
                                                                        Display="Dynamic" ErrorMessage="Ingresar código SNIP" ValidationGroup="excel">*</asp:RequiredFieldValidator>

                                                                    <asp:LinkButton runat="server" ID="lnkbtnFichaMef" CssClass="btn btn-default" Text="Ficha MEF" OnClick="lnkbtnFichaMef_Click"></asp:LinkButton>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td align="left" style="height: 8px">Código Único:
                                                                </td>
                                                                <td colspan="3" align="left" style="height: 8px;">
                                                                    <asp:TextBox CssClass="texto" ID="txtUnificado" runat="server" Width="90px"></asp:TextBox>
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" FilterType="Numbers" TargetControlID="txtUnificado" runat="server"></asp:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtUnificado"
                                                                        Display="Dynamic" ErrorMessage="Ingresar código Unificado" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="height: 8px">Tipo Inversión:</td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="ddlTipoInversion" runat="server">
                                                                        <asp:ListItem Value="" Selected="True">- Seleccione -</asp:ListItem>
                                                                        <asp:ListItem Value="1">PROYECTO DE INVERSION</asp:ListItem>
                                                                        <asp:ListItem Value="2">IOAR</asp:ListItem>
                                                                        <%--<asp:ListItem Value="3">IRIS</asp:ListItem>--%>
                                                                        <%--<asp:ListItem Value="4">ACTIVIDADES</asp:ListItem>--%>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="ddlTipoInversion"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar tipo de inversión/intervención." ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="height: 8px; width: 120px;">Nombre de Inversión:
                                                                </td>
                                                                <td colspan="5" align="left" style="height: 8px;">
                                                                    <asp:TextBox CssClass="texto" ID="txtproyecto" runat="server" Width="100%" TextMode="MultiLine" Height="53px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtproyecto"
                                                                        Display="Dynamic" ErrorMessage="Ingresar Nombre del Proyecto" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">Etapa Proyecto:
                                                                </td>
                                                                <td align="left" style="height: 31px;">
                                                                    <asp:DropDownList ID="ddlFinanciamiento" runat="server" OnSelectedIndexChanged="ddlFinanciamiento_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Value="" Selected="True">- Seleccione -</asp:ListItem>
                                                                        <asp:ListItem Value="1">Pre Inversion</asp:ListItem>
                                                                        <asp:ListItem Value="2">Expediente Técnico</asp:ListItem>
                                                                        <asp:ListItem Value="3">Obra</asp:ListItem>
                                                                        <asp:ListItem Value="4">Mantenimiento</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlFinanciamiento"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar tipo de proyecto" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">Periodo Inicio:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="ddlPeriodo" runat="server">
                                                                        <asp:ListItem Value="" Selected="True">- Seleccione -</asp:ListItem>
                                                                        <asp:ListItem Value="2021"></asp:ListItem>
                                                                        <asp:ListItem Value="2020"></asp:ListItem>
                                                                        <asp:ListItem Value="2019"></asp:ListItem>
                                                                        <asp:ListItem Value="2018"></asp:ListItem>
                                                                        <asp:ListItem Value="2017"></asp:ListItem>
                                                                        <%--<asp:ListItem Value="2016"></asp:ListItem>
                                                                        <asp:ListItem Value="2015"></asp:ListItem>
                                                                        <asp:ListItem Value="2014"></asp:ListItem>
                                                                        <asp:ListItem Value="2013"></asp:ListItem>
                                                                        <asp:ListItem Value="2012"></asp:ListItem>
                                                                        <asp:ListItem Value="2011"></asp:ListItem>
                                                                        <asp:ListItem Value="2010"></asp:ListItem>
                                                                        <asp:ListItem Value="2009"></asp:ListItem>
                                                                        <asp:ListItem Value="2008"></asp:ListItem>
                                                                        <asp:ListItem Value="2007"></asp:ListItem>
                                                                        <asp:ListItem Value="2006"></asp:ListItem>
                                                                        <asp:ListItem Value="2005"></asp:ListItem>--%>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlPeriodo"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar Periodo" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>

                                                                <td align="left">Unidad:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="ddlPrograma" runat="server">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlPrograma"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar Programa" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>

                                                                <td align="right" runat="server" id="tdSub1" visible="false">Sub Unidad:
                                                                </td>
                                                                <td align="left" runat="server" id="tdSub2" visible="false">
                                                                    <asp:DropDownList ID="ddlSubPrograma" runat="server" Width="400px"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlSubPrograma" InitialValue="0"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar Sub Programa" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>

                                                            <tr runat="server" visible="false">
                                                                <td align="left">¿PROYECTO DE LA RECONSTRUCCIÓN?:
                                                                </td>
                                                                <td align="left" style="height: 31px;">

                                                                    <asp:RadioButtonList runat="server" ID="rbFlagReconstruccion" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                                        <asp:ListItem Value="0" Text="NO" Selected="True"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td align="right">¿ES UN SERVICIO?:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:RadioButtonList runat="server" ID="rbFlagServicio" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                                        <asp:ListItem Value="0" Text="NO" Selected="True"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>

                                                            <tr runat="server" visible="false">
                                                                <td align="right">¿ES EMERGENCIA?:
                                                                </td>
                                                                <td align="left" style="height: 31px;">

                                                                    <asp:RadioButtonList runat="server" ID="rbFlagEmergencia" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                                        <asp:ListItem Value="0" Text="NO" Selected="True"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td align="left">Modalidad de Financiamiento:
                                                                </td>
                                                                <td align="left" colspan="5">
                                                                    <asp:DropDownList ID="ddlModalidadEjecucion" runat="server">
                                                                        <asp:ListItem Text="-Seleccione-" Value=""></asp:ListItem>
                                                                        <%--<asp:ListItem Text="NÚCLEO EJECUTOR" Value="4" Selected="True"></asp:ListItem>--%>
                                                                        <asp:ListItem Text="POR CONTRATA" Value="3"></asp:ListItem>
                                                                        <%--<asp:ListItem Text="OBRAS X IMPUESTOS" Value="5"></asp:ListItem>
                                                                        <asp:ListItem Text="APP" Value="6"></asp:ListItem>--%>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlModalidadEjecucion"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar Modalidad" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td align="left">Ambito:
                                                                </td>
                                                                <td align="left">

                                                                    <asp:DropDownList ID="ddlambito" runat="server">
                                                                        <asp:ListItem Value="0" Selected="True">- Seleccione -</asp:ListItem>
                                                                        <asp:ListItem Value="1">URBANO</asp:ListItem>
                                                                        <asp:ListItem Value="2">RURAL</asp:ListItem>
                                                                        <asp:ListItem Value="3">EPS</asp:ListItem>
                                                                        <asp:ListItem Value="4">PEQUEÑAS CIUDADES</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlambito"
                                                                        Display="Dynamic" ErrorMessage="Seleccionar Ambito" InitialValue="0" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">Unidad Ejecutora (UEI):
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox CssClass="texto" ID="txtejecutora" runat="server" Width="400px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtejecutora"
                                                                        Display="Dynamic" ErrorMessage="Ingresar Unidad Ejecutora" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                </td>

                                                                <td align="right" style="height: 8px">Beneficiarios:
                                                                </td>
                                                                <td align="left" style="padding-left: 5px">
                                                                    <asp:TextBox CssClass="texto" ID="txtPoblacion" runat="server" Width="60px" Text="0"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtPoblacion"
                                                                        Display="Dynamic" ErrorMessage="Ingresar Población" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" FilterType="Numbers"
                                                                        TargetControlID="txtPoblacion" runat="server"></asp:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">Monto Viable (S/.):
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox CssClass="texto" ID="txtmonto_snip" runat="server" Width="100px" Text="0"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtmonto_snip"
                                                                        Display="Dynamic" ErrorMessage="Ingresar Monto SNIP" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" ValidChars="." FilterType="Numbers,Custom"
                                                                        TargetControlID="txtmonto_snip" runat="server"></asp:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="TrAprobacionExpediente" visible="false">
                                                                <td align="left">Fecha Aprobación Exp. Técnico:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox CssClass="texto" ID="txtFechaAprobacion" runat="server" Width="90px" MaxLength="10"
                                                                        placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                    <img class="urlTexto" onclick="ShowCalendario('<%=txtFechaAprobacion.ClientID %>');" src="../img/calendario.jpg"
                                                                        style="border: 0px" alt="Calendario" />
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" ValidChars="/" FilterType="Numbers,Custom"
                                                                        TargetControlID="txtFechaAprobacion" runat="server"></asp:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td align="left" style="padding-top: 10px">Ubigeo(S):
                                                                </td>
                                                                <td colspan="3">
                                                                    <table width="90%" style="border-collapse: separate !important; border-spacing: 6px;">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:GridView runat="server" ID="GvUbigeoCrear" EmptyDataText="No hay registro."
                                                                                    ShowHeaderWhenEmpty="True"
                                                                                    AutoGenerateColumns="False"
                                                                                    CellPadding="2" CellSpacing="2" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="100%" DataKeyNames="codigo,ubigeoCCPP" OnRowCommand="GvUbigeoCrear_RowCommand">
                                                                                    <Columns>

                                                                                        <asp:BoundField Visible="false" DataField="codigo" HeaderText="UBIGEO">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:BoundField Visible="true" DataField="depa" HeaderText="Departamento">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:BoundField Visible="true" DataField="prov" HeaderText="Provincia">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:BoundField Visible="true" DataField="dist" HeaderText="Distrito">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:BoundField Visible="false" DataField="ubigeoCCPP" HeaderText="Ubigeo CCPP">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:BoundField Visible="false" DataField="CCPP" HeaderText="CCPP">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:TemplateField HeaderText="">
                                                                                            <ItemTemplate>
                                                                                                <%--<asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="../img/del.gif" CommandName="eliminar" />--%>

                                                                                                <asp:LinkButton ID="lnkbtnelima" runat="server" CssClass="btn btn-light" CommandName="eliminar" ToolTip="Eliminar">
                                                                                    <i class="fa fa-trash"></i>
                                                                                                </asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle Width="24px" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle Font-Size="14px" HorizontalAlign="Left" />
                                                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                                                    <RowStyle HorizontalAlign="Center" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                            <td>
                                                                                <asp:LinkButton ID="btnMostrarUbigeo" runat="server" OnClick="btnMostrarUbigeo_Click" CssClass="btn btn-info" ToolTip="Agregar">
                                                                                             <i class="fa fa-plus-circle"></i>
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" visible="false" id="trUbigeo">
                                                                            <td colspan="2">
                                                                                <div class="row">
                                                                                    <div class="card card-info">
                                                                                        <table width="98%" style="border-collapse: separate !important; border-spacing: 6px;">
                                                                                            <tr>
                                                                                                <td align="left" style="height: 31px">Departamento:
                                                                                                </td>
                                                                                                <td align="left" style="height: 31px;">
                                                                                                    <asp:DropDownList ID="ddlDepa" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDepa_SelectedIndexChanged" Width="140px">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlDepa"
                                                                                                        Display="Dynamic" ErrorMessage="Seleccionar departamento" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="height: 31px">Provincia:
                                                                                                </td>
                                                                                                <td align="left" style="height: 31px;">
                                                                                                    <asp:DropDownList ID="ddl_prov" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_prov_SelectedIndexChanged" Width="150px">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddl_prov"
                                                                                                        Display="Dynamic" ErrorMessage="Seleccionar provincia" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="height: 31px">Distrito:
                                                                                                </td>
                                                                                                <td align="left" style="height: 31px;">
                                                                                                    <asp:DropDownList ID="ddl_dist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_dist_SelectedIndexChanged" Width="150px">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddl_dist"
                                                                                                        Display="Dynamic" ErrorMessage="Seleccionar distrito" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr runat="server" visible="false">
                                                                                                <td align="left">Localidad:</td>
                                                                                                <td colspan="3" style="text-align: left;">
                                                                                                    <asp:DropDownList ID="ddl_ccpp" runat="server" Width="220px" CssClass="ddl-filter">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="ddl_ccpp"
                                                                                                        Display="Dynamic" ErrorMessage="Seleccionar centro poblado" InitialValue="" ValidationGroup="excel">*</asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" class="center-block text-center">
                                                                                                    <asp:LinkButton ID="BtnAgregarUbigeo" runat="server" OnClick="BtnAgregarUbigeo_Click" CssClass="btn btn-info">
                                                                                                                    <i class="fa fa-plus" id="BtnLimpiar2"></i> AGREGAR
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr runat="server" visible="false">
                                                <td colspan="2">
                                                    <br />
                                                    <br />
                                                    <asp:Panel GroupingText="Otros Datos (Opcional)" runat="server" CssClass="fieldGrupo">
                                                        <table>
                                                            <tr>
                                                                <td align="right">CÓDIGO DE IDEA:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox CssClass="texto" ID="TxtCodigoIdea" runat="server" Width="100px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">IDENTIFICADORES DE  CONVOCATORIA<br>
                                                                    (CÓDIGOS SEACE)</td>
                                                                <td align="left" colspan="4">
                                                                    <table width="100%" style="border-collapse: separate !important; border-spacing: 6px;">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:GridView runat="server" ID="GvCodigoSeace" EmptyDataText="No hay registro."
                                                                                    ShowHeaderWhenEmpty="True"
                                                                                    AutoGenerateColumns="False"
                                                                                    CellPadding="2" CellSpacing="2"
                                                                                    Width="500px" DataKeyNames="codigo"
                                                                                    OnRowCommand="GvCodigoSeace_RowCommand">
                                                                                    <Columns>

                                                                                        <asp:BoundField Visible="true" DataField="codigo" HeaderText="Código SEACE">
                                                                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:TemplateField HeaderText="Elim.">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="../img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle Width="24px" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle Height="25px" BackColor="#28375B" ForeColor="White" />
                                                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                                                    <RowStyle HorizontalAlign="Center" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button runat="server" ID="btnMostrarSeace" Text="+" CssClass="btn btn-primary" OnClick="btnMostrarSeace_Click" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" visible="false" id="trSeace">
                                                                            <td colspan="2">
                                                                                <div class="row">

                                                                                    <asp:Panel GroupingText="AGREGAR CÓDIGOS SEACE" runat="server" CssClass="fieldGrupo col-md-offset-1">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>INGRESE CÓDIGO:</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>
                                                                                                    <asp:TextBox runat="server" CssClass="texto" ID="TxtCodigoSeace"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>
                                                                                                    <asp:Button ID="BtnAgregarCodigoSeace" runat="server" Text=" + AGREGAR" OnClick="BtnAgregarCodigoSeace_Click" CssClass="btn btn-primary" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">CÓDIGOS ARCCC<br>
                                                                    (RECONSTRUCCIÓN)</td>
                                                                <td align="left" colspan="4">
                                                                    <table width="100%" style="border-collapse: separate !important; border-spacing: 6px;">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:GridView runat="server" ID="grdCodigoARCC" EmptyDataText="No hay registro."
                                                                                    ShowHeaderWhenEmpty="True"
                                                                                    AutoGenerateColumns="False"
                                                                                    CellPadding="2" CellSpacing="2"
                                                                                    Width="500px" DataKeyNames="codigo"
                                                                                    OnRowCommand="grdCodigoARCC_RowCommand">

                                                                                    <Columns>

                                                                                        <asp:BoundField Visible="true" DataField="codigo" HeaderText="Código ARCC">
                                                                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                                        </asp:BoundField>

                                                                                        <asp:TemplateField HeaderText="Elim.">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="../img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle Width="24px" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle Height="25px" BackColor="#28375B" ForeColor="White" />
                                                                                    <EditRowStyle BackColor="#FFFFB7" />
                                                                                    <RowStyle HorizontalAlign="Center" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button runat="server" ID="btnMostrarARCC" Text="+" CssClass="btn btn-primary" OnClick="btnMostrarARCC_Click" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" visible="false" id="trARCC">
                                                                            <td colspan="2">
                                                                                <div class="row">

                                                                                    <asp:Panel GroupingText="AGREGAR CÓDIGOS ARCC" runat="server" CssClass="fieldGrupo col-md-offset-1">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>INGRESE CÓDIGO:</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>
                                                                                                    <asp:TextBox runat="server" CssClass="texto" ID="txtCodigoArcc"></asp:TextBox>
                                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" FilterType="Numbers" TargetControlID="txtCodigoArcc"
                                                                                                        runat="server"></asp:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>
                                                                                                    <asp:Button ID="btnAgregarCodigoARCC" runat="server" Text=" + AGREGAR" OnClick="btnAgregarCodigoARCC_Click" CssClass="btn btn-primary" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </center>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnMostrarUbigeo" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ddlFinanciamiento" EventName="SelectedIndexChanged" />

                                <asp:AsyncPostBackTrigger ControlID="ddlPrograma" EventName="SelectedIndexChanged" />

                                <asp:AsyncPostBackTrigger ControlID="ddlDepa" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddl_prov" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddl_dist" EventName="SelectedIndexChanged" />

                                <asp:AsyncPostBackTrigger ControlID="btnmef" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="BtnAgregarUbigeo" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="BtnAgregarCodigoSeace" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnNuevoRegistro" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="lnkbtnFichaMef" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel3">
                        <ContentTemplate>
                            <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                            <asp:Button ID="btnGrabarBanco" runat="server" Text="REGITRAR"
                                Width="100px" OnClick="btnGrabarBanco_Click" ValidationGroup="excel" CssClass="btn btn-primary" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Update-->
    <div class="modal fade" id="modalUpdateProyecto" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ACTUALIZAR INFORMACIÓN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="width: 100%; overflow-y: scroll;">
                        <asp:UpdatePanel ID="Up_ModalidaEjecucion" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="defaultid2">
                                    <table width="100%" id="Table1" style="margin: auto; border-collapse: separate !important; border-spacing: 4px;" runat="server">
                                        <tr>
                                            <td class="text-left">SNIP :
                                            </td>
                                            <td class="text-left">
                                                <asp:Label ID="lblIdProyecto" runat="server" Text="" Visible="false"></asp:Label>
                                                <asp:TextBox CssClass="texto" ID="txtSNIPEdit" runat="server" Width="80px" MaxLength="10" Enabled="false"></asp:TextBox>&nbsp;&nbsp;
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" FilterType="Numbers" TargetControlID="txtSNIPEdit"
                                        runat="server"></asp:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Nombre Inversión:
                                            </td>
                                            <td colspan="3" class="text-left">
                                                <asp:TextBox CssClass="texto" ID="txtNombreEdit" runat="server" Width="100%" TextMode="MultiLine" Height="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Unidad:
                                            </td>
                                            <td class="text-left">
                                                <asp:DropDownList ID="ddlProgramaEdit" runat="server" Enabled="false">
                                                </asp:DropDownList>
                                            </td>

                                            <td class="text-left" runat="server" id="tdSubPrograma1" visible="false">Sub Unidad:
                                            </td>
                                            <td class="text-left" runat="server" id="tdSubPrograma2" visible="false">
                                                <asp:DropDownList ID="ddlSubProgramaEdit" runat="server" Width="100%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Tipo Inversión:</td>
                                            <td class="text-left">
                                                <asp:DropDownList ID="ddlTipoInversionEdit" runat="server">
                                                    <asp:ListItem Value="" Selected="True">- Seleccione -</asp:ListItem>
                                                    <asp:ListItem Value="1">PROYECTO DE INVERSION</asp:ListItem>
                                                    <asp:ListItem Value="2">IOAR</asp:ListItem>
                                                    <%--<asp:ListItem Value="3">IRIS</asp:ListItem>
                                                        <asp:ListItem Value="4">ACTIVIDADES</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Periodo:
                                            </td>
                                            <td class="text-left">
                                                <asp:DropDownList ID="ddlPeriodoEdit" runat="server">
                                                    <asp:ListItem Value="" Selected="True">- Seleccione -</asp:ListItem>
                                                    <asp:ListItem Value="2021"></asp:ListItem>
                                                    <asp:ListItem Value="2020"></asp:ListItem>
                                                    <asp:ListItem Value="2019"></asp:ListItem>
                                                    <asp:ListItem Value="2018"></asp:ListItem>
                                                    <asp:ListItem Value="2017"></asp:ListItem>
                                                    <asp:ListItem Value="2016"></asp:ListItem>
                                                    <asp:ListItem Value="2015"></asp:ListItem>
                                                    <asp:ListItem Value="2014"></asp:ListItem>
                                                    <asp:ListItem Value="2013"></asp:ListItem>
                                                    <asp:ListItem Value="2012"></asp:ListItem>
                                                    <asp:ListItem Value="2011"></asp:ListItem>
                                                    <asp:ListItem Value="2010"></asp:ListItem>
                                                    <asp:ListItem Value="2009"></asp:ListItem>
                                                    <asp:ListItem Value="2008"></asp:ListItem>
                                                    <asp:ListItem Value="2007"></asp:ListItem>
                                                    <asp:ListItem Value="2006"></asp:ListItem>
                                                    <asp:ListItem Value="2005"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-left">Ambito:
                                            </td>
                                            <td class="text-left">
                                                <asp:DropDownList ID="ddlAmbitoEdit" runat="server">
                                                    <asp:ListItem Value="0" Selected="True">- - SELECIONAR - -</asp:ListItem>
                                                    <asp:ListItem Value="1">URBANO</asp:ListItem>
                                                    <asp:ListItem Value="2">RURAL</asp:ListItem>
                                                    <asp:ListItem Value="3">EPS</asp:ListItem>
                                                    <asp:ListItem Value="4">PEQUEÑAS CIUDADES</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr runat="server" visible="false">
                                            <td align="right">¿PROYECTO DE LA RECONSTRUCCIÓN?:
                                            </td>
                                            <td align="left" style="height: 31px;">
                                                <asp:RadioButtonList runat="server" ID="rbFlagReconstruccionEdit" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td align="right">¿ES UN SERVICIO?:
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList runat="server" ID="rbFlagServicioEdit" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr runat="server" visible="false">
                                            <td align="right">¿ES EMERGENCIA?:
                                            </td>
                                            <td align="left" style="height: 31px;">

                                                <asp:RadioButtonList runat="server" ID="rbFlagEmergenciaEdit" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">UEI:
                                            </td>
                                            <td class="text-left">
                                                <asp:TextBox CssClass="texto" ID="txtUnidadEjecutoraEdit" runat="server" Width="300px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left" style="height: 8px">Beneficiarios:
                                            </td>
                                            <td class="text-left" style="padding-left: 5px">
                                                <asp:TextBox CssClass="texto" ID="txtPoblacionEdit" runat="server" Width="60px" Text="0"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" FilterType="Numbers" TargetControlID="txtPoblacionEdit" runat="server"></asp:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: center;">
                                                <asp:Button ID="btnActualizarBanco" runat="server" Text="Actualizar" Width="100px" OnClick="btnActualizarBanco_Click" ValidationGroup="excel" CssClass="btn btn-primary" />
                                                <br />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" class="text-left">Ubigeo :
                                                    <br />
                                                <table width="100%" style="border-collapse: separate !important; border-spacing: 6px;">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView runat="server" ID="grdUbicacion" EmptyDataText="No hay registro de agentes."
                                                                ShowHeaderWhenEmpty="True"
                                                                AutoGenerateColumns="False"
                                                                CellPadding="2" CellSpacing="2"
                                                                Width="100%" DataKeyNames="id"
                                                                OnRowCommand="grdUbicacion_RowCommand">
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="Departamento">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomDepa" Text='<%# Eval("depa") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Provincia">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomProv" Text='<%# Eval("prov") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Distrito">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomDist" Text='<%# Eval("dist") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblUbigeo" Text='<%# Eval("codigo") %>' runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="CCPP" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomCCPP" Text='<%# Eval("CCPP") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblUbigeoCCPP" Text='<%# Eval("ubigeoCCPP") %>' runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <%--<asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CommandName="eliminar" />--%>
                                                                            <asp:LinkButton ID="lnkbtnelim" runat="server" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" CssClass="btn btn-default" CommandName="eliminar" ToolTip="Eliminar">
                                                                                    <i class="fa fa-trash"></i>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomUsuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                            <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle Height="15px" />
                                                                <EditRowStyle BackColor="#FFFFB7" />
                                                                <RowStyle HorizontalAlign="Center" />
                                                            </asp:GridView>
                                                        </td>
                                                        <td style="width: 30px; vertical-align: top">
                                                            <asp:ImageButton ID="imgbtnAgregarUbicacion" runat="server" ImageUrl="~/img/add.png"
                                                                onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                AlternateText="Agregar" ToolTip="Agregar" Width="48px" OnClick="imgbtnAgregarUbicacion_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            <asp:Panel ID="Panel_UbicacionMetasTab0" Visible="False" runat="server" Width="90%">
                                                                <div style="width: 95%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                    <table style="border-collapse: separate !important; border-spacing: 4px; width: 100%">
                                                                        <tr>
                                                                            <td colspan="6">&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="6" align="center">
                                                                                <b>REGISTRO DE UBICACIÓN </b>
                                                                                <asp:Label ID="lblIdUbigeoTab0" runat="server" Visible="False"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">Departamento :
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:DropDownList ID="ddlDepartamentoTab0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartamentoTab0_SelectedIndexChanged"></asp:DropDownList>
                                                                            </td>

                                                                            <td align="right">Provincia :
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:DropDownList ID="ddlProvinciaTab0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProvinciaTab0_SelectedIndexChanged"></asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-right">Distrito :
                                                                            </td>
                                                                            <td class="text-left">
                                                                                <asp:DropDownList ID="ddlDistritoTab0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDistritoTab0_SelectedIndexChanged"></asp:DropDownList>
                                                                            </td>
                                                                            <td class="text-right" runat="server" visible="false">CCPP :
                                                                            </td>
                                                                            <td class="text-left" runat="server" visible="false">
                                                                                <asp:DropDownList ID="ddlCCPPTab0" runat="server" CssClass="ddl-filter"></asp:DropDownList>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="6" align="left" style="padding-left: 50px; height: 20px;">
                                                                                <asp:Label ID="lblNomUsuarioUbigeoTab0" runat="server" Font-Size="10px"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="6">
                                                                                <asp:Button ID="btnGuardarUbicacionMetasTab0" runat="server" Text="Agregar" OnClick="btnGuardarUbicacionMetasTab0_Click" CssClass="btn btn-primary" />
                                                                                <asp:Button ID="btnCancelarUbicacionMetasTab0" runat="server" Text="Cancelar" OnClick="btnCancelarUbicacionMetasTab0_Click" CssClass="btn btn-default" />
                                                                            </td>
                                                                    </table>
                                                                </div>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" style="text-align: left">
                                                <asp:Label ID="Label2" runat="server" Text="" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer">
                    <%-- <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                        <ContentTemplate>
                            <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                            <asp:Button ID="Button1" runat="server" Text="REGITRAR"
                                Width="100px" OnClick="btnGrabarBanco_Click" ValidationGroup="excel" CssClass="btn btn-primary" />
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="ContentFooter" ContentPlaceHolderID="ContentPlaceFooter" runat="server">
    <script type="text/javascript">
        applySelect2('.ddl-filter');
    </script>
</asp:Content>