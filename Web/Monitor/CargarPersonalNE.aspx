﻿<%@ Page Title="Carga Personal NE" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="CargarPersonalNE.aspx.cs" Inherits="Web.Monitor.CargarPersonalNE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        select {
            font-family: Tahoma;
            font-size: 9px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro {
            line-height: 15px;
        }

        .tdFiltro {
            font-weight: bold;
            font-size: 12px;
        }


       .titulo {
            font-size: 15pt;
            font-family: Calibri;
            color: #0094D4;
            line-height: 20px;
            font-weight:bold;
        }

        .tituloTipo {
            font-size: 13pt;
            font-family: 'DINB';
            color: #000000;
        }

        .titulo2 {
            font-family: 'DINB';
            font-size: 12pt;
            color: #28375B;
        }

        .tituloContador {
            font-family: Calibri;
            font-size: 10pt;
        }



        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        .CajaDialogo {
            background-color: #CEE3F6;
            border-width: 4px;
            padding: 0px;
            width: 200px;
            font-weight: bold;
        }

            .CajaDialogo div {
                margin: 5px;
                text-align: center;
            }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: absolute;
        }

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        input[type=text] {
            /*	background-image: url(../img/pdf.gif); 
	background-repeat: repeat-x;*/
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-family: Tahoma;
            font-size: 11px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }

        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }

        .Div_Fondo {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: fixed;
            z-index: 1004;
            top: 0;
            left: 0;
            right: 0;
            bottom: -1px;
            height: auto;
        }


        .divBody {
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border: 2px solid #006fa7;
            font-family: Calibri;
            padding: 0px 5px 5px 5px;
        }




        elemento {
            background-color: black;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            position: absolute;
            left: 5px;
            top: 5px;
            width: 936px;
            height: 736px;
            visibility: visible;
            z-index: 1;
        }

     
          .marginAuto {
              margin:auto
          }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="Grid" style="text-align: center">
        <br />
        <p>
            <span class="titlePage">REGISTRO DE PERSONAL DE NE </span>
        </p>
        <br />
        <br />
    </div>

    <br />
    <br />

    <div>
        <table border="0" style="margin: auto;">
            <tr>
                <td style="width: 60%; text-align: right">
                    <asp:FileUpload ID="FileUpload1" runat="server" />

                </td>
                <td style="width: 10%">
                    <asp:Button Text="Cargar" runat="server" OnClick="Upload_Click" CssClass="btn btn-primary" />
                </td>

                <td style="width: 30%">
                    <div style="text-align: left; padding-left: 30px; text-decoration: underline; color: #0e2e8c;">
                        <asp:HyperLink ID="HyperLink1" Font-Size="14px" NavigateUrl="~/Documentos/TecnicoPersonalNE(formato).xlsx" runat="server">
                                <i class="fa fa-download" aria-hidden="true"></i>Descargar Formato</asp:HyperLink>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <hr />
    <div runat="server" visible="false" id="divErrorCarga" style="margin: auto; text-align: center">
        <%--<h6>Los siguientes registros no se cargaron. Por favor verifique en el excel</h6>--%>
        <asp:GridView ID="grillaErrorCarga" runat="server" AutoGenerateColumns="False"
            CellPadding="3" CellSpacing="1" CssClass="marginAuto"
            BorderColor="#CCCCCC" Font-Names="Calibri" Font-Size="Small" ForeColor="#333333" GridLines="None">
            <EditRowStyle BackColor="#999999" />
            <FooterStyle ForeColor="White"
                BackColor="#5D7B9D" Font-Bold="True"></FooterStyle>
            <PagerStyle ForeColor="White"
                HorizontalAlign="Center" BackColor="#284775"></PagerStyle>
            <HeaderStyle ForeColor="White" Font-Bold="True" Height="30px"
                BackColor="#5D7B9D"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <%--<asp:BoundField HeaderText="#" DataField="Numero" />--%>
                <asp:BoundField HeaderText="Nombre" DataField="nombre" />
                <%--<asp:BoundField HeaderText="Activo" DataField="activo" />--%>
                <asp:BoundField HeaderText="Tipo Programa" DataField="tipoPrograma" />
                <asp:BoundField HeaderText="Correo" DataField="correo" />
                <asp:BoundField HeaderText="Tipo Agente" DataField="tipoAgente" />
                <asp:BoundField HeaderText="DNI" DataField="dni" />
                <asp:BoundField HeaderText="Tipo Profesion" DataField="tipoProfesion" />
                <asp:BoundField HeaderText="Nro Colegiatura" DataField="nroColegiatura" />
                <asp:BoundField HeaderText="Detalle Error" DataField="Detalle_Error" />

            </Columns>
            <SelectedRowStyle ForeColor="#333333" Font-Bold="True" Font-Names="Calibri"
                BackColor="#E2DED6"></SelectedRowStyle>
            <RowStyle ForeColor="#333333" Font-Names="Calibri"
                BackColor="#F7F6F3"></RowStyle>
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

        </asp:GridView>
    </div>


</asp:Content>

