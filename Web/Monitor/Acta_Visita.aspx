﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Acta_Visita.aspx.cs" Inherits="Web.Monitor.Acta_Visita" Culture="es-PE" UICulture="es-PE" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/print.css" type="text/css" media="print" />
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }


        .center {
            margin: auto;
        }

        .table2 {
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #a7a7a7 !important;
        }


        .table-bordered {
            border: 1px solid #a7a7a7;
        }

            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid #ddd;
            }

            .table-bordered > thead > tr > th,
            .table-bordered > thead > tr > td {
                border-bottom-width: 2px;
            }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <center>
        <div id="div_Ficha" style="width: 800px; height: auto;">
            <center>
              <%--  <div id="div_imagen">
                    <img src="../img/LogoMVCS1-estado.png" height="55"  alt="" />
                </div>--%>
                            
                <%--<div id="title" style="display: none">
                    <center><span style="font-family: 'Trebuchet MS'; font-size: 9pt"><b>MINISTERIO DE VIVIENDA, CONSTRUCCI&Oacute;N Y SANEAMIENTO</b></span></center>
                </div>--%>
             <table style="margin:auto;margin-top:5px;margin-bottom:6px;font-family:Calibri" cellpadding="0" cellspacing="0">
                 <tr>
                     <td >
                         <asp:Image ImageUrl="../img/pastillaPNSU.jpg" Height="45" runat="server"  ID="pastillaActa" />
                         
                         
                     </td>
                     <td style="border:1px solid #fff;text-align:center">
                         <asp:Label runat="server" Text="Cargo de Recepación" Font-Size="9px"></asp:Label><br />
                         <asp:Label runat="server" Text="(Firma y sello de Unidad Ejecutora)" Font-Size="9px"></asp:Label>
                     </td>
                 </tr>
                 <tr>
                     <td style="text-align:center">
                         <asp:Label runat="server" Text="AÑO DEL BICENTENARIO DEL PERÚ: 200 AÑOS DE INDEPENDENCIA" Font-Size="11px" Font-Bold="true"></asp:Label>
                         <br />
                         <span style="font-family: Arial; font-size: 16pt;margin-top:20px"><b>ACTA DE VISITA DE MONITOREO</b></span>
                         <div style="text-align:left">
                             <table style="text-align:left; margin-left:0">
                                 <tr>
                                     <td style="height:10px"></td>
                                 </tr>
                                 <tr><td valign="bottom"><asp:Label runat="server" Text="Fecha :" Font-Bold="true" Font-Size="10.5pt" ></asp:Label> </td>
                                     <td valign="bottom">
                                        <asp:Label runat="server"  Text="" ID="lblFechaAyuda" Font-Size="10.5pt" ></asp:Label>
                                        
                                     </td></tr>
                             </table>
                             
                         </div>
                     </td>

                     <td style="border:1px solid #000000; width:140px; height:90px" >

                     </td>
                 </tr>
                
             </table>
                                        
                <%--<span style="font-family:'Calibri';font-size:11pt"><b><asp:Label ID="lblComponente1" runat="server" Text=""></asp:Label>, <asp:Label ID="lblDistrito1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblProvincia1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblDepartamento1" runat="server" Text=""></asp:Label></b></span>--%>
            </center>
       
            <%-- <div style="width:100%;text-align:left">
            <span style="font-family:'Calibri';font-size:11pt"><b>DATOS GENERALES:</b></span>
        </div>--%>

              <div style="width: 100%; text-align: left; padding:5px; background-color:black; color:#fff">
                        <span style="font-family: 'Arial'; font-size: 10pt"><b> I. DATOS GENERALES:</b></span>
                    </div>
             <div style="width: 100%; padding-left:20px" >
                 
                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td style="text-align:left"> <b>1.1. Nombre del Proyecto </b></td>
                        <td><b>:</b></td>
                        <td>
                             <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>1.2. Código Proyecto </b></td>
                        <td><b>:</b></td>
                        <td>
                             <table width="90%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td  style="width:40%"></td>
                                    <td>
                                        <asp:Label ID="lblTitConvenio" runat="server" Text="Convenio N° :" Visible="false"></asp:Label>
                                        </td>
                                    <td style="width:40%"><asp:Label ID="lblConvenio" runat="server" Text="" Visible="false"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>1.3. Ubicación </b></td>
                        <td><b>:</b></td>
                        <td>
                            <table width="90%">
                                <tr>
                                    <td>Localidad :</td>
                                    <td  style="width:40%"><asp:Label ID="lblCCPP" runat="server" Text=""></asp:Label></td>
                                    <td>Distrito :</td>
                                    <td style="width:40%"><asp:Label ID="lblDistrito" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Provincia :</td>
                                    <td><asp:Label ID="lblProvincia" runat="server" Text=""></asp:Label></td>
                                    <td>Región :</td>
                                    <td><asp:Label ID="lblRegion" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </table>
                             <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td> <b>1.4. Unidad Ejecutora </b></td>
                         <td><b>:</b></td>
                        <td>
                                <asp:Label ID="lblEjecutora" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>1.5. Monto de compromiso MVCS-PNSU</b></td>
                        <td><b>:</b></td>
                        <td>
                                <asp:Label ID="Label12" runat="server" Text="S/."></asp:Label><asp:Label ID="lblMontoConvenio" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>1.6. Transferencia </b></td>
                        <td><b>:</b></td>
                        <td>
                                 <asp:Label ID="lblMsjTransferencia" runat="server" Text="No hubo transferencia hasta el momento." Visible="false"></asp:Label>
                            <dx:ASPxGridView ID="xgrdTransferencias" runat="server" >
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="dispositivo_aprobacion" VisibleIndex="0" Caption="Dispositivo de transferencia">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="strfecha_aprobacion" VisibleIndex="1" Caption="Fecha">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="montoAprobacion" VisibleIndex="6" UnboundType="Decimal" Caption="Monto Transferido (S/.)">
                                        <FooterCellStyle ForeColor="Black" Font-Bold="true" />

                                        <PropertiesTextEdit DisplayFormatString="N2" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                                <TotalSummary>

                                    <dx:ASPxSummaryItem FieldName="montoAprobacion" SummaryType="Sum" Tag="TOTAL" DisplayFormat="C" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                     <tr>
                        <td> <b>1.7. Modalidad de Ejecución </b></td>
                         <td><b>:</b></td>
                        <td>
                                  <asp:Label ID="lblModalidad1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>1.8. Modalidad de Contratación </b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:CheckBoxList runat="server" ID="chblistModalidadContratacion" RepeatDirection="Horizontal" CellPadding="8" >
                                <asp:ListItem Text="Suma Alzada   " Value="1"></asp:ListItem>
                                <asp:ListItem Text="Precios Unitarios    " Value="2"></asp:ListItem>
                                <asp:ListItem Text="Otros    " Value="3"></asp:ListItem>
                            </asp:CheckBoxList>
                                   
                        </td>
                    </tr>
                    <tr>
                        <td> <b>1.9. Coordinador de la UE </b></td>
                        <td><b>:</b></td>
                        <td>
                            <table style="width:80%">
                                <tr>
                                    <td>Nombre :</td>
                                    <td style="width:60%"><asp:Label ID="lblNombreUE" runat="server" Text=""></asp:Label></td>

                                    <td>Teléfonos :</td>
                                    <td><asp:Label ID="lblTelefonoUE" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Correo Elect :</td>
                                    <td colspan="2"><asp:Label ID="lblCorreoUE" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </table>
                                  
                        </td>
                    </tr>
                    </table>
                 </div>

       
    
        <div style="width: 100%; text-align: left; padding:5px; background-color:black; color:#fff">
                        <span style="font-family: 'Arial'; font-size: 10pt"><b> II. DE LA OBRA : (Datos Proporcionados por la Unidad Ejecutora)</b></span>
                    </div>

             <div style="width: 100%; padding-left:20px" >
                 
                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%; border-spacing: 0; border-collapse: collapse;margin-top:10px" cellpadding="5">
                    <tr>
                        <td style="width:18%"><b>2.1. Contratista </b></td>
                        <td><b>:</b></td>
                        <td class="table2 table-bordered"><asp:Label ID="lblContratista" runat="server" Text=""></asp:Label><asp:Label ID="lblConsorcio" runat="server" Text=""></asp:Label></td>
                        <td class="table2 table-bordered"> Monto Contratado : S/ <asp:Label ID="lblMontoContratista" runat="server" Text=""></asp:Label></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold">2.2. Supervisor </td>
                        <td><b>:</b></td>
                        <td class="table2 table-bordered"><asp:Label ID="lblEmpresaSupervisor" runat="server" Text=""></asp:Label></td>
                         <td class="table2 table-bordered"> Monto Contratado : S/ <asp:Label ID="lblMontoSupervisor" runat="server" Text=""></asp:Label></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                     <td></td>
                        <td>Obra + Supervision : S/ <asp:Label ID="lblTotalMontoContratado" runat="server" Text=""></asp:Label></td>
                        <td></td>
                    </tr>
                    </table>
                  <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td style="font-weight:bold; width:18%">2.3. Residente de Obra </td>
                        <td style="width:2%"><b>:</b></td>
                        <td style="text-align:left">
                            <table width="80%" style="text-align:left">
                                <tr>
                                    <td>Nombre :</td>
                                    <td style="width:60%"><asp:Label ID="lblResidente" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>Teléfonos :</td>
                                    <td><asp:Label ID="lblResidenteTelef" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Correo Elect. :</td>
                                    <td colspan="2">
                                        <asp:Label ID="lblResidenteCorreo" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                       <tr>
                        <td style="font-weight:bold">2.4. 
                             <asp:Label ID="lblCargoAyudaMemoria" runat="server" Text=""></asp:Label> </td>
                            <td><b>:</b></td>
                        <td>
                            <table width="80%">
                                <tr>
                                    <td>Nombre :</td>
                                    <td style="width:60%"><asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                                          
                                            <asp:Label ID="lblSupervisor" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>Teléfonos :</td>
                                    <td><asp:Label ID="lblSupervisorTelef" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Correo Elect. :</td>
                                    <td colspan="2">
                                        <asp:Label ID="lblSupervisorCorreo" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                      <tr>
                        <td style="font-weight:bold">2.5. Fechas </td>
                           <td><b>:</b></td>
                        <td>
                            <table width="90%" class="table2 table-bordered" cellpadding="3">
                                <tr>
                                    <td>Fecha de Entrega de Terreno :
                                        <asp:Label ID="lblFechaEntregaTerreno" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>Fecha de Inicio de Obra : <asp:Label ID="lblFechaInicio1" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Plazo de Ejecución de Obra :
                                        <asp:Label ID="lblPlazoEjecucion1" runat="server" Text=""></asp:Label><asp:Label ID="Label1" runat="server" Text=" (Días Calendario)"></asp:Label>
                                    </td>
                                    <td>Ampliaciones de plazo : <asp:Label ID="lblAmpliacionesPlazo" runat="server" Text=""></asp:Label><asp:Label ID="Label2" runat="server" Text=" (d.c)"></asp:Label>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>Fecha de Término Contractual :
                                        <asp:Label ID="lblFechaFinalAmpliacion" runat="server" Text=""></asp:Label>
                                        <br />
                                        (Incluye ampliaciones de plazo)
                                    </td>
                                <td></td>
                                    </tr>
                            </table></td>
                    </tr>
                        <tr>
                        <td style="font-weight:bold">2.6. Avances </td>
                             <td><b>:</b></td>
                        <td>
                            <table width="90%" class="table2 table-bordered" cellpadding="3" >
                                <tr>
                                    <td>Avance Físico Acum. Ejecutado :</td>
                                    <td>
                                       <asp:Label ID="lblPorcentajeAvance" runat="server" Text=""></asp:Label>
                                    Referido al : <asp:Label ID="lblFechaAvanceFisico" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Avance Físico Programado :</td>
                                    <td>
                                        <asp:Label ID="lblAvanceProgramado" runat="server" Text=""></asp:Label>
                                   Referido al :<asp:Label ID="lblFechaAvanceProgramado" runat="server" Text=""></asp:Label>
                                             <asp:Label ID="lblAvanceFinanciero" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Monto devengado :</td>
                                     <td>
                                        <asp:Label ID="lblSosemMontoDevengado" runat="server" Text=""></asp:Label>
                                 (Según SOSEM al <asp:Label ID="lblSosemFecha" runat="server" Text=""></asp:Label>) </td>
                         
                                </tr>
                                       
                               
                            </table></td>
                    </tr>
                       <tr>
                        <td style="font-weight:bold">2.7. Estado de la Obra </td>
                            <td><b>:</b></td>
                        <td>
                            <table>
                                <tr><td><asp:Label ID="lblEstadoSituacional" runat="server" Text=""></asp:Label></td></tr>
                                <tr><td><asp:Label ID="lblDetalleSituacional" runat="server" Text="" Visible="false"></asp:Label></td></tr>
                            </table>
                            

                        </td>
                       </tr>
                      <tr runat="server" id="trFuncioamientoPNSU1">
                       <td colspan="2" style="font-weight:bold; text-align:right"> Funcionamiento : </td>
                        <td>
                             <asp:CheckBoxList runat="server" RepeatDirection="Vertical" ID="chblTipoFuncionamientoActaVisita" >
                                                <asp:ListItem Text="Los sistemas se encuentran operativos y en funcionamiento" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Las obras no se encuentran operativas" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Los sistemas se encuentran operando con deficiencias" Value="3"></asp:ListItem>
                                            </asp:CheckBoxList>
                        </td>
                       </tr>
                      <tr runat="server" id="trFuncioamientoPNSU2">
                          <td colspan="2" style="font-weight:bold;text-align:right"> Comentario : </td>
                        <td>
                             <asp:Label ID="lblComentarioFuncionamiento" runat="server" Text=""></asp:Label>
                        </td>
                      </tr>
                       <tr runat="server" id="trFuncioamientoPNSU3">
                       <td colspan="2" style="font-weight:bold;text-align:right"> Inaugurada : </td>
                        <td>
                          <asp:CheckBoxList runat="server" RepeatDirection="Horizontal" ID="chblFlagInaugurado"  >
                                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                            </asp:CheckBoxList>
                        </td>
                       </tr>
                       <tr runat="server" id="trFuncioamientoPNSU4">
                        <td style="font-weight:bold;text-align:right">Unidad Receptora de la Obra </td>
                            <td><b>:</b></td>
                        <td>
                            <asp:CheckBoxList runat="server" ID="chblUnidadReceptora" RepeatDirection="Horizontal" CellPadding="5" >
                                <asp:ListItem Text="JASS" Value="4"></asp:ListItem>
                                <asp:ListItem Text="EPS" Value="3"></asp:ListItem>
                                <asp:ListItem Text="OES" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Unidad de Gestión" Value="1"></asp:ListItem>
                                
                            </asp:CheckBoxList>
                        </td>
                       </tr>
                      <tr runat="server" id="trFuncioamientoPNSU5">
                          <td colspan="2" style="text-align: right">Comentario :</td>
                          <td>
                              <asp:Label ID="lblComentarioReceptora" runat="server" Text=""></asp:Label>
                          </td>
                      </tr>

                      <tr runat="server" id="trMetasPNSU1">
                          <td colspan="4" style="font-weight: bold">2.8. Componentes y/o Metas del Proyecto :</td>
                       
                      </tr>
                      <tr runat="server" id="trAsuntosPMIB1" visible="false">
                          <td colspan="4" style="font-weight: bold">2.8. Otros asuntos técnicos y administrativos :</td>
                       
                      </tr>
                      <tr>
                          
                          <td colspan="3" style="text-align:left">
                              <table style="padding-left:20px">
                                  <tr runat="server" id="trMetasPNSU2">
                                      <td>
                                           <asp:Label ID="lblTitAgua" runat="server" Text="Sistema de Agua Potable :" Visible="false" Font-Bold="true"  ></asp:Label>
                                          <dx:ASPxGridView ID="xgrdSistemaAgua" runat="server" CssClass="center" Visible="false" Width="100%"  > 
                                              <Columns>
                                                  <dx:GridViewDataColumn FieldName="nombre" VisibleIndex="0" Caption="Componentes del Proyecto" >
                                                      <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                                      <CellStyle HorizontalAlign="Center"></CellStyle>
                                                  </dx:GridViewDataColumn>
                                                  <dx:GridViewDataColumn FieldName="observacion" VisibleIndex="1" Caption="Comentario">
                                                      <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                                  </dx:GridViewDataColumn>
                                                
                                              </Columns>
                                           
                                          </dx:ASPxGridView>
                                           <br />
                                              <asp:Label ID="lblTitAlcantarillado" runat="server" Text="Sistema de Alcantarillado, UBS y PTAR :" Visible="false" Font-Bold="true" ></asp:Label>
                                          <dx:ASPxGridView ID="xgrdSistemaAlcantarillado" runat="server" CssClass="center" Visible="false" Width="100%"  > 
                                              <Columns>
                                                  <dx:GridViewDataColumn FieldName="nombre" VisibleIndex="0" Caption="Componentes del Proyecto" >
                                                      <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                                      <CellStyle HorizontalAlign="Center"></CellStyle>
                                                  </dx:GridViewDataColumn>
                                                  <dx:GridViewDataColumn FieldName="observacion" VisibleIndex="1" Caption="Comentario">
                                                      <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                                  </dx:GridViewDataColumn>
                                                
                                              </Columns>

                                          </dx:ASPxGridView>
                                      </td>
                                  </tr>
                                  <tr runat="server" id="trAsuntosPMIB2" visible="false">
                                      <td>
                                          <table width="90%">
                                              <tr style="border-bottom: 1px solid #c8c3c3">
                                                  <td><b>Asunto</b></td>
                                                  <td><b>SI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO</b></td>
                                                  <td style="text-align: center"><b>Comentario</b></td>
                                              </tr>
                                              <tr>
                                                  <td>1. Oficina y almacén de obra</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList1" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario1" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>2. Disponibilidad de materiales en condiciones adecuadas</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList2" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario2" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>3. Equipos en las cantidades y estado operativo requeridos</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList3" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario3" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>4. Cartel de obra instalado de acuerdo a modelo PMIB - MVCS</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList4" RepeatDirection="Horizontal" Font-Names="Calibri" Font-Size="13px">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario4" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>5. Presencia del Residente de Obra</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList5" RepeatDirection="Horizontal" Font-Names="Calibri" Font-Size="13px">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario5" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>6. Presencia del Supervisor de Obra</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList6" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario6" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>7. Copia del Expediente Técnico para uso en obra</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList7" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario7" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>8. Cuaderno de obra actualizado</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList8" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario8" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>9. Calendario de obra actualizado a la fecha de inicio</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList9" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario9" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>10. Empleo de implemento de seguridad para los trabajadores</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList10" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario10" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>11. Elementos de seguridad para la población (cercos, señales, etc.)</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList11" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario11" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>12. Cumplimientos de adecuados procesos constructivos</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList12" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario12" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>13. Cumplimientos del expediente técnico de obra</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList13" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario13" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>14. Presenta pruebas y controles de calidad reglamentarios</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList14" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario14" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>15. Cumpliemiento de procesos reglamentarios en casos adicionales o ampliaciones de plazo</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList15" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario15" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                              <tr>
                                                  <td>16. Apreciación objetiva de la calidad de las obras ya realizadas</td>
                                                  <td>
                                                      <asp:RadioButtonList runat="server" ID="RadioButtonList16" RepeatDirection="Horizontal">
                                                          <asp:ListItem Value="1" Text=""></asp:ListItem>
                                                          <asp:ListItem Value="2" Text=""></asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                      <asp:TextBox runat="server" ID="txtComentario16" Text="" TextMode="MultiLine" Width="350px" Font-Names="Calibri" Font-Size="13px"></asp:TextBox></td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                                  <tr runat="server" id="trAsuntosPMIB3" visible="false">
                                      <td style="padding-top: 20px;"><b><u>Cumplimiento de entrega de documentos:</u></b></td>
                                  </tr>
                                  <tr runat="server" id="trAsuntosPMIB4" visible="false">
                                      <td>
                                          <asp:Label ID="lblEntregaDocumentosPMIB" runat="server" Text=""></asp:Label>

                                      </td>
                                  </tr>
                                  <tr>
                                      <td style="padding-top:20px;" ><b><u>OBSERVACIONES:</u></b></td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblObservacionComponentes" runat="server" Text=""></asp:Label>

                                      </td>
                                  </tr>
                                  <tr>
                                      <td style="padding-top:20px;"><b><u>RECOMENDACIONES:</u></b></td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblRecomendacionComponenetes" runat="server" Text=""></asp:Label>

                                      </td>
                                  </tr>
                                  <tr>
                                      <td style="height:10px"></td>
                                  </tr>
                              </table>
                          </td>

                      </tr>
                  </table>
             </div>

              <div style="width: 100%; font-family:Calibri; font-size:13px" >
                  <div style="border:1px solid #000000; font-size:10px; text-align:center">
                              Los abajo firmantes de la presente acta dejan constancia que se encuentran de acuerdo con las observaciones encontradas en la visita de monitoreo. Asimismo la Unidad
                              Ejecutora se compromete a que en un plazo máximo de 15 días, deberá informar al PROGRAMA NACIONAL DE SANEAMIENTO URBANO (PNSU) sobre las acciones adoptadas a fin 
                              de implementar las recomendaciones que se indican en la presente acta; de no tener respuesta la Unidad de Monitoreo y Control (UMC) del PNSU, reiterará el requerimiento a la U.E. 
                              con copia a los Organos de Control de Interno de ser el caso y de considerarlo necesario a la CGR, a fin de que se adopten las medidas que el caso amerite.
                          </div>    
                              <br />
                              <table width="100%" style="text-align:center">
                                  <tr>
                                      <td><u>Por Parte de la Unidad Ejecutora</u></td>
                                      <td><u>Por Parte del MVCS</u></td>
                                  </tr>
                                  <tr>
                                      <td style="height:80px" valign="bottom">____________________________________</td>
                                      <td valign="bottom">____________________________________</td>
                                  </tr>
                                  <tr>
                                      <td style="height:80px" valign="bottom">____________________________________</td>
                                      <td valign="bottom">____________________________________</td>
                                  </tr>
                                  <tr>
                                      <td style="height:80px" valign="bottom">____________________________________</td>
                                      <td valign="bottom">____________________________________</td>
                                  </tr>
                                  <tr>
                                      <td style="height:80px" valign="bottom"></td>
                                  </tr>
                              </table>
                  <br />
                     <div style="font-size:10px; text-align:center" >
                              Se deja constancia que la visación y suscripción del presente documento como coordinador, no es señal de conformidad ni adelanto de opinión ni pre juzgamiento,
                              por lo que, no afecta el control posterior a cargo del Sistema Nacional de Control.
                              </div>
                              <br />
                              <br />
                  </div>

             <div id="icon_word">
                    <center>
                        <table>
                            <tr>
                            <td>
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                            </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                        </table>
                    </center>

                </div>

            <br />
            <br />
             </div>
     </center>
    
    </form>
</body>
</html>
