﻿<%@ Page Title="Consulta Personal NE" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ConsultaPersonalNE.aspx.cs" Inherits="Web.Monitor.ConsultaPersonalNE" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        select {
            font-family: Tahoma;
            font-size: 9px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro {
            line-height: 15px;
        }

        .tdFiltro {
            font-weight: bold;
            font-size: 12px;
        }


        .titulo {
            font-size: 14pt;
            font-family: 'DINB';
            color: #0094D4;
            line-height: 20px;
        }

        .tituloTipo {
            font-size: 13pt;
            font-family: 'DINB';
            color: #000000;
        }

        .titulo2 {
            font-family: 'DINB';
            font-size: 12pt;
            color: #28375B;
        }

        .tituloContador {
            font-family: Calibri;
            font-size: 10pt;
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        .CajaDialogo {
            background-color: #CEE3F6;
            border-width: 4px;
            padding: 0px;
            width: 200px;
            font-weight: bold;
        }

            .CajaDialogo div {
                margin: 5px;
                text-align: center;
            }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            /*position: absolute;*/
        }

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        input[type=text] {
            /*	background-image: url(../img/pdf.gif); 
	background-repeat: repeat-x;*/
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-family: Tahoma;
            font-size: 11px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }

        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }

        .Div_Fondo {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: fixed;
            z-index: 1004;
            top: 0;
            left: 0;
            right: 0;
            bottom: -1px;
            height: auto;
        }

        .divBody {
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border: 2px solid #006fa7;
            font-family: Calibri;
            padding: 0px 5px 5px 5px;
        }

        elemento {
            background-color: black;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            position: absolute;
            left: 5px;
            top: 5px;
            width: 936px;
            height: 736px;
            visibility: visible;
            z-index: 1;
        }

        .user66 {
            position: absolute;
            bottom: 0;
            left: 30px;
        }

        .tablaMsj {
            border-collapse: collapse;
        }

            .tablaMsj td {
                border: 1px solid #c94e4e;
                padding: 2px;
            }

            .tablaMsj th {
                border: 1px solid #c94e4e;
                padding: 2px;
                font-weight: bold;
            }

        .alert-warning {
            color: rgb(169, 68, 66);
            background-color: rgb(242, 222, 222);
            border-color: rgb(235, 204, 209);
            padding: 15px 15px 20px;
            font-family: helvetica;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-info {
            padding: 15px;
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
            font-family: helvetica;
            border: 1px solid transparent;
            border-radius: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <center>
        <div class="Grid">

            <br />
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="center" style="height: 25px">
                        <p>
                            <asp:Label ID="Label3" runat="server" Text="CONSULTA PERSONAL N.E." CssClass="titlePage">
                            </asp:Label>
                        </p>
                    </td>
                </tr>
            </table>

            <br />
            <table style="border-collapse: separate !important; border-spacing: 2px; margin: auto">
                <tr>
                    <td align="center">
                        <b>DNI :</b>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="txtSearch" runat="server" Width="808px" placeholder="Ejemplo búsqueda DNIs:01234567,12345678,45678912"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSearch" FilterType="Custom,Numbers" ValidChars="," Enabled="True" />
                    </td>
                    <td align="center">
                        <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-primary" OnClick="btnBuscar_Click">
                                       <i class="glyphicon glyphicon-search" style="font-size: inherit; color: inherit;"></i> BUSCAR
                        </asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <dx:ASPxGridView ID="grdxPersonalNE" runat="server" Font-Size="10px" Width="85%" Theme="MetropolisBlue" EnableCallBacks="false"
            AutoGenerateColumns="False">

            <Columns>
                <dx:GridViewBandColumn Caption="INFORMACIÓN GENERAL ACTUAL">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="DNI" FieldName="DNI" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  BackColor="#4e8909" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True" >
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="NOMBRE COMPLETO" FieldName="nombre" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  BackColor="#4e8909" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="PROFESIÓN" FieldName="tipoProfesion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  BackColor="#4e8909" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="COLEGIATURA" FieldName="NroAdicional" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  BackColor="#4e8909" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="CARGO ACTUAL" FieldName="tipoAgente" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  BackColor="#4e8909" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="PROGRAMA ACTUAL" FieldName="pruebas" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  BackColor="#4e8909" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                   
                    </Columns>
                    <HeaderStyle BackColor="#4e8909" HorizontalAlign="Center" ForeColor="White" Font-Bold="True"   />
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="DATOS DE PROYECTOS N.E.">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="SNIP" FieldName="snip" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" BackColor="#094E89" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="NOMBRE DE PROYECTO" FieldName="usuario" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" BackColor="#094E89" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="PUESTO CONTRATADO" FieldName="cargo" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" BackColor="#094E89" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ESTADO PERSONAL" FieldName="estadoResidente" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" BackColor="#094E89" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ESTADO SITUACIONAL DEL PROYECTO" FieldName="estadoSituacional" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" BackColor="#094E89" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="PROGRAMA" FieldName="Recomendacion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" BackColor="#094E89" ForeColor="White" />
                            <CellStyle HorizontalAlign="Center" Wrap="True">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle BackColor="#094E89" HorizontalAlign="Center" ForeColor="White" Font-Bold="True" />
                </dx:GridViewBandColumn>
            </Columns>
            
        </dx:ASPxGridView>
    </center>
</asp:Content>