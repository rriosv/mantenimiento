﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class Informe_Visita : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_SOSEM _objBLSOSEM = new BL_MON_SOSEM();
        BE_MON_SOSEM _BESOSEM = new BE_MON_SOSEM();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Conclusion _objBLConclusion = new BL_MON_Conclusion();
        BE_MON_Conclusion _BEConclusion = new BE_MON_Conclusion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();


        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BE_MON_VarPNSU _BEVarPNSU = new BE_MON_VarPNSU();

        BL_MON_Calidad _objBLCalidad = new BL_MON_Calidad();
        BE_MON_Calidad _BECalidad = new BE_MON_Calidad();

        BL_MON_Finan_Transparencia _objBLFinaTra = new BL_MON_Finan_Transparencia();
        BE_MON_Finan_Transparencia _BEFinaTra = new BE_MON_Finan_Transparencia();

        DataTable dt = new DataTable();

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Grabar_Log();
                }
                catch (Exception)
                {
                    //throw;
                }

                lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";
            }

            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }

            CargaInformacionGeneral();


        }


        //modificando
        protected void CargaInformacionGeneral()
        {
            _BEEjecucion.Id_ejecucionRecomendacion = Convert.ToInt32(Request.QueryString["idE"].ToString());
            dt = _objBLEjecucion.spMON_EvaluacionRecomendacionActaVisita(_BEEjecucion);

            lblTituloInforme.Text = dt.Rows[0]["NroInformeVisita"].ToString().ToUpper();
            lblDestinatario.Text = dt.Rows[0]["DirigidoAInforme"].ToString();
            lblCargoDestinatario.Text = dt.Rows[0]["DirigidoCargoAInforme"].ToString();
            lblAsuntoInforme.Text = dt.Rows[0]["AsuntoInforme"].ToString();
            lblReferenciaInforme.Text = dt.Rows[0]["ReferenciaInforme"].ToString();
            lblFechaInforme.Text = dt.Rows[0]["FechaInformeMonitoreo"].ToString();

            StringBuilder strCabecera = new StringBuilder().Append("<div>" + dt.Rows[0]["CabeceraInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblCabeceraInforme.Text = strCabecera.ToString();

            //StringBuilder strMetas = new StringBuilder().Append("<div>" + dt.Rows[0]["metasInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            //lblMetasProyecto.Text = strMetas.ToString();

            StringBuilder strAnalisis = new StringBuilder().Append("<div>" + dt.Rows[0]["analisisInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblAnalisisInforme.Text = strAnalisis.ToString();

            //StringBuilder strConclusion = new StringBuilder().Append("<div><pre style='white-space: pre-wrap;font-family: calibri;'>" + dt.Rows[0]["conclusionInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</pre></div>");
            StringBuilder strConclusion = new StringBuilder().Append("<div>" + dt.Rows[0]["conclusionInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblConclusion.Text = strConclusion.ToString();

            StringBuilder strRecomendacion = new StringBuilder().Append("<div>" + dt.Rows[0]["recomendacionInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblRecomendacion.Text = strRecomendacion.ToString();

            StringBuilder strAntecedente = new StringBuilder().Append("<div>" + dt.Rows[0]["antecedenteInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblAntecedentes.Text = strAntecedente.ToString();

            StringBuilder strFinanciamiento = new StringBuilder().Append("<div>" + dt.Rows[0]["comentarioFinanciamientoInforme"].ToString().Replace(Environment.NewLine, "<br />") + "</div>");
            lblComentarioFinanciero.Text = strFinanciamiento.ToString();


            BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
            List<BE_MON_Financiamiento> ListTransferencia = new List<BE_MON_Financiamiento>();
            ListTransferencia = _objBLTransferencia.F_spSOL_TransferenciaByProyecto(Convert.ToInt32(Request.QueryString["idPo"].ToString()));
            xgrdTransferencias.DataSource = ListTransferencia;
            xgrdTransferencias.DataBind();

            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["idPo"].ToString());
            _BELiquidacion.tipoFinanciamiento = 3;
            dt = _objBLLiquidacion.spMON_Ayuda_Memoria(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                lblTipoContratacion.Text = dt.Rows[0]["TipoContratacionObra"].ToString();
                lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();

                lblEjecutora.Text = dt.Rows[0]["UNIDAD_EJECUTARORA"].ToString();
                lblBeneficiario.Text = dt.Rows[0]["BENEFICIARIOS"].ToString();
                lblBeneficiario.Text = Convert.ToDouble(lblBeneficiario.Text).ToString("N0");

                lblFechaInicio1.Text = dt.Rows[0]["FECHAINICIO"].ToString();
                lblPlazoEjecucion1.Text = dt.Rows[0]["PLAZOEJECUCION"].ToString();

                lblEstadoSituacional.Text = dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();
                lblPorcentajeAvance.Text = dt.Rows[0]["FISICOREAL"].ToString();
                lblAvanceProgramado.Text = dt.Rows[0]["FisicoProgramado"].ToString();

                lblContratista.Text = dt.Rows[0]["CONTRATISTA"].ToString();
                lblConsorcio.Text = dt.Rows[0]["CONSORCIO"].ToString();

                lblEmpresaSupervisora.Text = dt.Rows[0]["EmpresaSupervisor"].ToString();

                lblMontoSupervision.Text = dt.Rows[0]["MontoSupervisor"].ToString();
                lblMontoSupervision.Text = Convert.ToDouble(lblMontoSupervision.Text).ToString("N");

                lblSupervisor.Text = dt.Rows[0]["SUPERVISORDESIGNADO"].ToString();
                lblResidente.Text = dt.Rows[0]["RESIDENTEOBRA"].ToString();

                lblMontoContratista.Text = dt.Rows[0]["MONTOCONSORCIO"].ToString();
                lblMontoContratista.Text = Convert.ToDouble(lblMontoContratista.Text).ToString("N");

                lblFechaEntregaTerreno.Text = dt.Rows[0]["FechaEntregaTerreno"].ToString();
                lblAmpliacionesPlazo.Text = dt.Rows[0]["TotalAmpliacionPlazo"].ToString();
                lblFechaFinAmpliacion.Text = dt.Rows[0]["fechaFinAmplParalizacion"].ToString();
                lblMontoAdelantoDirecto.Text = dt.Rows[0]["MontoAdelantoDirecto"].ToString();
                lblMontoAdelantoDirecto.Text = Convert.ToDouble(lblMontoAdelantoDirecto.Text).ToString("N");

                lblSosemMontoDevengado.Text = dt.Rows[0]["DevengadoAcumulado"].ToString();
                lblSosemMontoDevengado.Text = Convert.ToDouble(lblSosemMontoDevengado.Text).ToString("N");
                lblSosemFecha.Text = "(Según SOSEM al " + dt.Rows[0]["FechaUpdateMEF"].ToString() + ")";
            }

            List<BE_MON_SOSEM> ListSosem = new List<BE_MON_SOSEM>();
            ListSosem = _objBLFinanciamiento.F_spMON_SosemDevengadoMensualizado(Convert.ToInt32(lblSnip.Text), 0);
            grdDetalleSOSEMTab0.DataSource = ListSosem;
            grdDetalleSOSEMTab0.DataBind();

            Double resultadoTotal = 0;
            string vFecha = DateTime.Now.ToString("dd/MM/yyyy");
            if (ListSosem.Count > 0)
            {
                resultadoTotal = ListSosem.Sum(elemento => Convert.ToDouble(elemento.devAcumulado));

                vFecha = ListSosem.ElementAt(0).strFechaUpdate;

            }

            lblMsjSosem.Text = "Con fecha " + vFecha + " la Unidad Ejecutora cuenta con un monto devengado de S/ " + resultadoTotal.ToString("N") + " de acuerdo al Sistema SOSEM - MEF .";
        }

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "InformeVisita_SNIP_" + lblSnip.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }
    }
}