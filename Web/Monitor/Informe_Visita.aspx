﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Informe_Visita.aspx.cs" Inherits="Web.Monitor.Informe_Visita" Culture="es-PE" UICulture="es-PE" Title="SSP" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/print.css" type="text/css" media="print" />
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        .dxgvFooter {
            white-space: nowrap;
            font-size: 10pt;
            font-family: Calibri;
            text-align: center;
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <center>
        <div id="div_Ficha" style="width: 800px; height: auto;">
            <center>
              <%--  <div id="div_imagen">
                    <img src="../img/LogoMVCS1-estado.png" height="55"  alt="" />
                </div>--%>
                            
                <%--<div id="title" style="display: none">
                    <center><span style="font-family: 'Trebuchet MS'; font-size: 9pt"><b>MINISTERIO DE VIVIENDA, CONSTRUCCI&Oacute;N Y SANEAMIENTO</b></span></center>
                </div>--%>
             <table style="margin:auto;margin-top:5px;margin-bottom:6px; font-family:Calibri" cellpadding="0" cellspacing="0" >
                
                 <tr>
                     <td style="text-align:center">
                         <asp:Label runat="server" Text="AÑO DE LA LUCHA CONTRA LA CORRUPCIÓN E IMPUNIDAD" Font-Size="11px" Font-Bold="true"></asp:Label><br />
                         <span style="font-family: Arial; font-size: 16pt;margin-top:20px"><b></b></span>
                         <div style="text-align:left">
                             <table style="text-align:left; margin-left:0" runat="server" visible="false">
                                 <tr><td><asp:Label runat="server" Text="Fecha :" ></asp:Label> </td>
                                     <td>
                                        <asp:Label runat="server"  Text="" ID="lblFechaAyuda" Font-Size="12px" ></asp:Label>
                                        
                                     </td></tr>
                             </table>
                             
                         </div>
                     </td>

                 </tr>
                
             </table>
                 </center>

            <div style="width: 100%; left: 0">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13.5px; width: 100%; text-align:left" cellpadding="5">
                    <tr>
                        <td colspan="3" style="text-align: left">
                            <u>
                                <asp:Label ID="lblTituloInforme" runat="server" Text="" Font-Bold="true"></asp:Label></u>
                        </td>
                    </tr>
                    <tr>
                        <td >A</td>
                        <td>:</td>
                        <td style="width:75%">
                            <asp:Label ID="lblDestinatario" runat="server" Text=""></asp:Label><br />
                            <asp:Label ID="lblCargoDestinatario" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Asunto </td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblAsuntoInforme" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Referencia</td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblReferenciaInforme" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Fecha </td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblFechaInforme" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 90%; border-top: 1px solid #000;" valign="top"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-size:13px;">
                            <asp:Label ID="lblCabeceraInforme" runat="server" Text="" Font-Size="13px"></asp:Label>
                            Al respecto, informo lo siguiente :
                        </td>
                    </tr>
                </table>
            </div>


            <div style="width: 100%; text-align: left; padding:10px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> I. DATOS GENERALES DEL PROYECTO:</b></span>
                    </div>

            <div style="width: 100%; left: 0">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%;padding-left:20px" cellpadding="5">
                    <tr>
                        <td style="text-align: left; width:200px"><b>- Modalidad de ejecución de la obra </b></td>
                          <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblTipoContratacion" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblModalidadEjecucion" runat="server" Text="" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Contratista</b></td>
                          <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblContratista" runat="server" Text="" Font-Bold="true"></asp:Label><asp:Label ID="lblConsorcio" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Residente </b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblResidente" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Monto contrato de ejecución</b></td>
                        <td><b>:</b></td>
                        <td>
                            S/ <asp:Label ID="lblMontoContratista" runat="server" Text=""></asp:Label></td>
                    </tr>
                  
                    <tr>
                        <td style="text-align: left"><b>- Fecha de Entrega de Terreno</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblFechaEntregaTerreno" runat="server" Text=""></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Fecha de Inicio Contractual</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblFechaInicio1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>-Plazo de Ejecución de Obra</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblPlazoEjecucion1" runat="server" Text=""></asp:Label><asp:Label ID="Label1" runat="server" Text=" (Días Calendario)"></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Ampliaciones de plazo </b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblAmpliacionesPlazo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Fecha de Término Contractual (incluye ampliaciones de plazo)</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblFechaFinAmpliacion" runat="server" Text=""></asp:Label><asp:Label ID="Label8" runat="server" Text=" (Días Calendario)"></asp:Label>
                        </td>
                    </tr>



                    <tr>
                        <td style="text-align: left"><b>- Supervisión</b></td>
                         <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblEmpresaSupervisora" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>-Monto Contratado Supervisor</b></td>
                         <td><b>:</b></td>
                        <td>
                            S/ <asp:Label ID="lblMontoSupervision" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><b>- Supervisor</b></td>
                         <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblSupervisor" runat="server" Text=""></asp:Label></td>
                    </tr>

                    <tr>
                         <td style="text-align: left"><b>- Unidad Ejecutora</b></td>
                         <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblEjecutora" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>

                     <tr>
                        <td style="text-align: left"><b>- Estado de la Obra</b></td>
                          <td><b>:</b></td>
                        <td><asp:Label ID="lblEstadoSituacional" runat="server" Text=""></asp:Label></td>
                       </tr>

                    <tr runat="server" visible="false">
                        <td style="text-align: left"><b>- Nombre del Proyecto</b></td>
                         <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblProyecto" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    
                     <tr>
                        <td style="text-align: left"><b>- Adelanto Directo</b></td>
                          <td><b>:</b></td>
                        <td>S/ <asp:Label ID="lblMontoAdelantoDirecto" runat="server" Text=""></asp:Label></td>
                       </tr>

                    <tr>
                        <td style="text-align: left"><b>- Avance Físico Ejecutado</b></td>
                         <td><b>:</b></td>
                                    <td>
                                       <asp:Label ID="lblPorcentajeAvance" runat="server" Text=""></asp:Label>
                                    </td>
                    </tr>
                    <tr>
                          <td style="text-align: left"><b>- Avance Físico Programado</b></td>
                         <td><b>:</b></td>
                                    <td>
                                        <asp:Label ID="lblAvanceProgramado" runat="server" Text=""></asp:Label>
                                    </td>
                    </tr>
                    <tr>
                         <td style="text-align: left"><b>- Avance financiero (SOSEM)</b></td>
                         <td><b>:</b></td>
                                     <td>
                                        S/ <asp:Label ID="lblSosemMontoDevengado" runat="server" Text=""></asp:Label>
                                         <asp:Label ID="lblSosemFecha" runat="server" Text=""></asp:Label>
                                    </td>
                    </tr>

                     <tr>
                          <td style="text-align: left"><b>- Población Beneficiaria </b></td>
                          <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblBeneficiario" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblhabitantes" runat="server" Text="Habitantes (Fuente Formato SNIP 03)"></asp:Label>

                        </td>
                    </tr>
                    <tr runat="server" visible="false">
                        <td><b>1.2 Código SNIP N°</b></td>
                         <td><b>:</b></td>
                        <td>
                            <asp:Label ID="lblSnip" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                  
           
            
                </table>
            </div>

             <div style="width: 100%; text-align: left; padding:20px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> II. ANTECEDENTES:</b></span>
                    </div>

             <div style="width: 100%;padding-left:20px">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="lblAntecedentes" runat="server" Text=""></asp:Label>
                        </td>
                        </tr>
                    </table>
                 </div>

          <%--  <div style="width: 100%; text-align: left; padding:20px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> III. METAS DEL PROYECTO:</b></span>
                    </div>

             <div style="width: 100%;padding-left:20px">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="lblMetasProyecto" runat="server" Text=""></asp:Label>
                        </td>
                        </tr>
                    </table>
                 </div>--%>

            <div style="width: 100%; text-align: left; padding:20px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> III. ASPECTO FINANCIERO:</b></span>
                    </div>

             <div style="width: 100%;">
                 
                  <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td>
                               <asp:Label ID="lblComentarioFinanciero" runat="server" Text=""></asp:Label>

                        </td>
                        </tr>
                    </table>

              
                      <dx:ASPxGridView ID="xgrdTransferencias" runat="server" >
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="dispositivo_aprobacion" VisibleIndex="0" Caption="Dispositivo de transferencia">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10pt" Font-Names="Calibri" />
                                       <CellStyle HorizontalAlign="Center" Font-Size="13px" Font-Names="Calibri"></CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="strfecha_aprobacion" VisibleIndex="1" Caption="Fecha">
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10pt" Font-Names="Calibri"  />
                                        <CellStyle HorizontalAlign="Center" Font-Size="13px" Font-Names="Calibri"></CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="montoAprobacion" VisibleIndex="6" UnboundType="Decimal" Caption="Monto Transferido (S/.)">
                                        <FooterCellStyle ForeColor="Black" Font-Bold="true" />

                                        <PropertiesTextEdit DisplayFormatString="N2" />
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10pt" Font-Names="Calibri"  />
                                        <CellStyle HorizontalAlign="Center" Font-Size="13px" Font-Names="Calibri"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                          
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="montoAprobacion" SummaryType="Sum" Tag="TOTAL" DisplayFormat="C" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                 <br />
                 <div style="text-align: left; font-family: Calibri; font-size: 13px">
                     <p>
                         <asp:Label runat="server" ID="lblMsjSosem" Text=""></asp:Label>
                     </p>
                 </div>
                           <asp:GridView runat="server" ID="grdDetalleSOSEMTab0" EmptyDataText="No hay registro."
                            ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                            CellPadding="2" CellSpacing="2" Width="100%" Font-Size="12px">
                            <Columns>
                                <asp:TemplateField HeaderText="Año" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntidad" Text='<%# Eval("anio") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:TemplateField>

                                <asp:BoundField DataField="pia" HeaderText="Pia" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="pimAcumulado" HeaderText="Pim. Acumulado" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="devAcumulado" HeaderText="Dev. Acum." DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="ene" HeaderText="ENE" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="feb" HeaderText="FEB" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="mar" HeaderText="MAR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="abr" HeaderText="ABR" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="may" HeaderText="MAY" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="jun" HeaderText="JUN" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="jul" HeaderText="JUL" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="ago" HeaderText="AGO" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="sep" HeaderText="SET" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="oct" HeaderText="OCT" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="nov" HeaderText="NOV" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                                <asp:BoundField DataField="dic" HeaderText="DIC" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>

                               <%-- <asp:BoundField DataField="compromisoAnual" HeaderText="Compromiso Anual" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>
                                <asp:BoundField DataField="certificado" HeaderText="Certificación" DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <HeaderStyle CssClass="GridHeader2" />
                                </asp:BoundField>--%>

                            </Columns>
                            <HeaderStyle Height="15px" Font-Names="Calibri" Font-Size="11px" />
                            <RowStyle Font-Size="10px" Font-Names="Calibri" />
                            <EditRowStyle BackColor="#FFFFB7" />
                        </asp:GridView>
                 <br />
                   <asp:Label ID="lblComentarioTotalSOSEM" runat="server" Text=""></asp:Label>
                </div>

             <div style="width: 100%; text-align: left; padding:20px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> IV. ANALISIS:</b></span>
                    </div>

             <div style="width: 100%; padding-left:20px">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="lblAnalisisInforme" runat="server" Text=""></asp:Label>
                        </td>
                        </tr>
                    </table>
                 </div>

            <div style="width: 100%; text-align: left; padding:20px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> V. OBSERVACIONES:</b></span>
                    </div>

             <div style="width: 100%; padding-left:20px">

                <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="lblConclusion" runat="server" Text=""></asp:Label>
                        </td>
                        </tr>
                    </table>
                 </div>

            <div style="width: 100%; text-align: left; padding:20px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px"><b> VI. RECOMENDACIONES:</b></span>
                    </div>

             <div style="width: 100%; padding-left:20px; text-align:left; font-size:10pt;font-family: Calibri;">

                
                            <asp:Label ID="lblRecomendacion" runat="server" Text=""></asp:Label>
                     
                 
                 </div>

            <div style="width: 100%; text-align: left; padding:5px 5px 5px 5px">
                        <span style="font-family: Calibri; font-size: 13px">
                           
                            Es todo cuanto informo. <br /><br />
                            Atentamente,
                            <br /><br /><br /><br /><br /><br /><br />
                            Quien suscribe considera pertinente el presente informe y da su conformidad.
                            <br /><br />

                        </span>
              </div>
          


             <div id="icon_word">
                    <center>
                        <table>
                            <tr>
                            <td>
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                            </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                        </table>
                    </center>

                </div>

            <br />
            <br />
             </div>
     </center>
    
    </form>
</body>
</html>
