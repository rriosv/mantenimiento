﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using Entity;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.Monitor
{
    public partial class Ficha_Proyecto : System.Web.UI.Page
    {
        BLProyecto objBLProyecto = new BLProyecto();
        BEProyecto ObjBEProyecto = new BEProyecto();

        BL_MON_Financiamiento _objBLFinanciamiento = new BL_MON_Financiamiento();
        BE_MON_Financiamiento _BEFinanciamiento = new BE_MON_Financiamiento();

        BL_MON_SOSEM _objBLSOSEM = new BL_MON_SOSEM();
        BE_MON_SOSEM _BESOSEM = new BE_MON_SOSEM();

        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();

        BL_MON_Ampliacion _objBLAmpliacion = new BL_MON_Ampliacion();
        BE_MON_Ampliacion _BEAmpliacion = new BE_MON_Ampliacion();

        BL_MON_Conclusion _objBLConclusion = new BL_MON_Conclusion();
        BE_MON_Conclusion _BEConclusion = new BE_MON_Conclusion();

        BL_MON_Proceso _objBLProceso = new BL_MON_Proceso();
        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();

        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();

        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BE_MON_VarPNSU _BEVarPNSU = new BE_MON_VarPNSU();

        BL_MON_Calidad _objBLCalidad = new BL_MON_Calidad();
        BE_MON_Calidad _BECalidad = new BE_MON_Calidad();

        BL_MON_Finan_Transparencia _objBLFinaTra = new BL_MON_Finan_Transparencia();
        BE_MON_Finan_Transparencia _BEFinaTra = new BE_MON_Finan_Transparencia();

        DataTable dt = new DataTable();
        DataTable dt_2 = new DataTable();

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //if (Session["LoginUsuario"] == null)
            //{ 
            //    Response.Redirect("~/login.aspx?ms=1");
            //}


            Session.Timeout = 120;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Grabar_Log();
                }
                catch (Exception)
                {
                    //throw;
                }

                lblFechaInforme.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";

                CargaInfoGeneral();

                //if (Convert.ToString(Request.QueryString["cod_subsector"]) == "PNSU")
                //{

                //}

            }


            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }
        }

        //protected void CargaInfoPNSU()
        //{
        //    div_Ficha.Visible = true;
        //    div_Ficha_PMIB.Visible = false;
        //    _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
        //    //_BELiquidacion.tipoFinanciamiento = 3;
        //    _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());


        //    dt = _objBLLiquidacion.spMON_Listado_Proyecto(_BELiquidacion);

        //    if (dt.Rows.Count > 0)
        //    {
        //        lblNom_Proyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
        //        lblEjecución.Text = dt.Rows[0]["EJECUCION"].ToString();
        //        lblDepartamento.Text = dt.Rows[0]["DEPARTAMENTO"].ToString();
        //        lblProvincia.Text = dt.Rows[0]["PROVINCIA"].ToString();
        //        lblDistrito.Text = dt.Rows[0]["DISTRITO"].ToString();
        //        lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
        //        lblOperador.Text = dt.Rows[0]["ENTIDAD"].ToString();
        //        lblModalidad.Text = dt.Rows[0]["MODALIDAD"].ToString();
        //        double monto1 = Convert.ToDouble(dt.Rows[0]["MONTO"].ToString());
        //        lblMonto1.Text = monto1.ToString("0.00");
        //        //lblMonto1.Text = dt.Rows[0]["MONTO"].ToString();
        //        lblPlazo.Text = dt.Rows[0]["PLAZO_EJECUCION"].ToString();
        //        lblfinicio.Text = dt.Rows[0]["FECHA_INICIO"].ToString();
        //        lblffin.Text = dt.Rows[0]["FECHA_FIN"].ToString();
        //        lblFechaRecepcion.Text = dt.Rows[0]["FECHA_RECEPCION"].ToString();
        //        lblPoblación.Text = dt.Rows[0]["POBLACION_BEN"].ToString();
        //        lblEstado.Text = dt.Rows[0]["ESTADO_S"].ToString();
        //        lblTipoProyecto.Text = dt.Rows[0]["TIPO_PROYECTO"].ToString();
        //        lblAvanceFisico.Text = dt.Rows[0]["FISICOREAL"].ToString();
        //        lblDS.Text = dt.Rows[0]["dispositivo_aprobacion"].ToString();



        //        double val1 = 0;
        //        if (lblMonto1.Text != "")
        //        { val1 = Convert.ToDouble(lblMonto1.Text); }

        //        int id_solicitudes = Convert.ToInt32(dt.Rows[0]["id_solicitudes"].ToString());
        //        if (id_solicitudes != 0)
        //        { 
        //            tb_pnsu.Visible = true;
        //            VarPnsu(id_solicitudes);
        //        }
        //        else { tb_pnsu.Visible = false; }


        //        //Carga Foto
        //        _BEBandeja.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
        //        List<BE_MON_BANDEJA> ListFoto = new List<BE_MON_BANDEJA>();
        //        ListFoto = _objBLBandeja.F_spMON_FotoDocumentoMonitor(_BEBandeja);

        //        if (ListFoto.Count > 0)
        //        {
        //            string url = ListFoto.ElementAt(0).UrlDoc.ToString();
        //            imgFotoPNSU.ImageUrl = "FotoPMIB.ashx?img=" + url;
        //            imgFotoPNSU.ToolTip = ListFoto.ElementAt(0).nombre.ToString();
        //            imgFotoPNSU.Visible = true;

        //        }
        //    }
        //}
        protected void CargaInfoGeneral()
        {
            //div_Ficha.Visible = false;
            div_Ficha_PMIB.Visible = true;
            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;
            _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());


            dt = _objBLLiquidacion.spMON_Listado_Proyecto(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {

                lblNom_Proyecto_PMIB.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblDepartamento_PMIB.Text = dt.Rows[0]["DEPARTAMENTO"].ToString();
                lblProvincia_PMIB.Text = dt.Rows[0]["PROVINCIA"].ToString();
                lblDistrito_PMIB.Text = dt.Rows[0]["DISTRITO"].ToString();
                lblSnip_PMIB.Text = dt.Rows[0]["SNIP"].ToString();
                lblOperador_PMIB.Text = dt.Rows[0]["ENTIDAD"].ToString();
                lblModalidad_PMIB.Text = dt.Rows[0]["MODALIDAD"].ToString();
                //double monto1_PMIB = Convert.ToDouble(dt.Rows[0]["MONTO"].ToString());
                //lblMonto1_PMIB.Text = monto1_PMIB.ToString("N");
                lblPlazo_PMIB.Text = dt.Rows[0]["PLAZO_EJECUCION"].ToString();
                lblfinicio_PMIB.Text = dt.Rows[0]["FECHA_INICIO"].ToString();
                lblffin_PMIB.Text = dt.Rows[0]["FECHA_FIN"].ToString();
                lblPoblación_PMIB.Text = dt.Rows[0]["POBLACION_BEN"].ToString();
                lblEstado_PMIB.Text = dt.Rows[0]["ESTADO_S"].ToString();
                lblContratista_PMIB.Text = dt.Rows[0]["CONTRATISTA"].ToString();

                //Carga Aportes
                ObjBEProyecto.Id_proyecto = Convert.ToInt32(Request.QueryString["id"]); ;
                DataTable dtMontos = objBLProyecto.spMON_ObtieneProyectos(ObjBEProyecto);
                if (dtMontos.Rows.Count > 0)
                {
                    txtMontoSNIPTab0.Text = dtMontos.Rows[0]["CostoMVCS"].ToString();

                    lblMontoSNIPTab0.Text = (Convert.ToDouble(txtMontoSNIPTab0.Text)).ToString("N");

                }
                CargaContrapartidaTab0();

                lblMonto1_PMIB.Text = (Convert.ToDouble(txtAcumuladogrdContraTab0.Text) + Convert.ToDouble(lblMontoSNIPTab0.Text)).ToString("N");

                //TRANSFERENCIAS
                //lblDispositivoLegal_PMIB.Text = dt.Rows[0]["dispositivo_aprobacion"].ToString();

                BL_MON_Financiamiento _objBLTransferencia = new BL_MON_Financiamiento();
                List<BE_MON_Financiamiento> ListTransferencia = new List<BE_MON_Financiamiento>();
                ListTransferencia = _objBLTransferencia.F_spSOL_TransferenciaByProyecto(Convert.ToInt32(Request.QueryString["id"].ToString()));
                xgrdTransferencias.DataSource = ListTransferencia;
                xgrdTransferencias.DataBind();

                if (ListTransferencia.Count > 0)
                {
                    decimal sum = ListTransferencia.Select(c => Convert.ToDecimal(c.montoAprobacion)).Sum();
                    decimal conv = Convert.ToDecimal(lblMontoSNIPTab0.Text);

                    string val;
                    if (conv == 0)
                    {
                        val = "0.00";
                        lblMsjTransferencia.Text = "Se ha realizado la transferencia(s), conforme se detalla en el siguiente cuadro:";
                    }
                    else
                    {
                        val = ((sum * 100) / conv).ToString("N");
                        lblMsjTransferencia.Text = "Se ha transferido el " + val + "%, conforme se detalla en el siguiente cuadro:";
                    }
                }

                //lblTipoProyecto.Text = dt.Rows[0]["TIPO_PROYECTO"].ToString();
                double val1 = 0;
                if (lblMonto1_PMIB.Text != "")
                { val1 = Convert.ToDouble(lblMonto1_PMIB.Text); }

                int id_solicitudes = Convert.ToInt32(dt.Rows[0]["id_solicitudes"].ToString());

                if (id_solicitudes != 0)
                {
                    if (Convert.ToString(Request.QueryString["cod_subsector"]) == "PNSU")
                    {

                        VarPnsu(id_solicitudes);
                    }
                    else
                    {
                        //table_metas.Visible = true;
                        VarPmib(id_solicitudes);
                    }
                }
                //else { table_metas.Visible = false; }
            }


            //Carga Foto
            _BEBandeja.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            List<BE_MON_BANDEJA> ListFoto = new List<BE_MON_BANDEJA>();
            ListFoto = _objBLBandeja.F_spMON_FotoDocumentoMonitor(_BEBandeja);

            int i = 0;
            foreach (BE_MON_BANDEJA item in ListFoto)
            {
                string extension = item.UrlDoc.Substring(item.UrlDoc.Length - 3, 3);

                if (extension.ToUpper() == "JPG" || extension.ToUpper() == "PNG")
                {
                    if (i == 0)
                    {
                        string url = item.UrlDoc.ToString();
                        imgProyectoPMIB.ImageUrl = "FotoPMIB.ashx?img=" + url;
                        imgProyectoPMIB.ToolTip = item.nombre;
                        imgProyectoPMIB.Visible = true;
                    }


                    if (i == 1)
                    {
                        string url = item.UrlDoc.ToString();
                        imgProyectoPMIB2.ImageUrl = "FotoPMIB.ashx?img=" + url;
                        imgProyectoPMIB2.ToolTip = item.nombre;
                        imgProyectoPMIB2.Visible = true;
                        i = i + 1;

                        break;
                    }
                    i = i + 1;

                }

            }

            if (i == 1)
            {
                imgProyectoPMIB.Width = 500;
            }

        }

        protected void CargaContrapartidaTab0()
        {
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            _BEFinanciamiento.tipoFinanciamiento = 3;
            List<BE_MON_Financiamiento> ListContrapartida = new List<BE_MON_Financiamiento>();
            ListContrapartida = _objBLFinanciamiento.F_spMON_FinanContrapartida(_BEFinanciamiento);
            totalMontoGrdContrapartida();
            double val1 = 0;
            if (txtAcumuladogrdContraTab0.Text != "")
            {
                val1 = Convert.ToDouble(txtAcumuladogrdContraTab0.Text);
            }

            double val2 = 0;
            if (txtMontoSNIPTab0.Text != "")
            { val2 = Convert.ToDouble(txtMontoSNIPTab0.Text); }
            txtTotalInversionTab0.Text = (val1 + val2).ToString("N");

            double Total = Convert.ToDouble(txtTotalInversionTab0.Text);

            int cant = ListContrapartida.Count;
            double gobiernoLocal = 0;
            double pobBenef = 0;
            for (int i = 0; i < cant; i++)
            {
                if (ListContrapartida.ElementAt(i).tipo_aporte.ToString() == "Gobierno Local")
                {
                    gobiernoLocal = gobiernoLocal + Convert.ToDouble(ListContrapartida.ElementAt(i).monto.ToString());
                }
                else if (ListContrapartida.ElementAt(i).tipo_aporte.ToString() == "Pob. Organizada")
                {
                    pobBenef = pobBenef + Convert.ToDouble(ListContrapartida.ElementAt(i).monto.ToString());
                }
            }
            double montoMVCS = Convert.ToDouble(lblMontoSNIPTab0.Text);
            double porcGobLocal = 0;
            double porcpobBenef = 0;
            double porcMVCS = 0;

            if (montoMVCS != 0) { porcMVCS = (montoMVCS * 100) / Total; }
            else { porcMVCS = 0; }
            if (gobiernoLocal != 0) { porcGobLocal = (gobiernoLocal * 100) / Total; } else { porcGobLocal = 0; }
            if (pobBenef != 0) { porcpobBenef = (pobBenef * 100) / Total; } else { porcpobBenef = 0; }



            lblMontoMUNIC_PMIB.Text = gobiernoLocal.ToString("N");
            lblMontoBENEF_PMIB.Text = pobBenef.ToString("N");

            lblPorcentajeMontoMUNIC_PMIB.Text = porcGobLocal.ToString("0.00") + "%" + "  ";
            lblPorcentajeMontoBENEF_PMIB.Text = porcpobBenef.ToString("0.00") + "%" + "  ";
            lblPorcentajeMontoMVCS_PMIB.Text = porcMVCS.ToString("N") + "%";
            txtTotalInversionTab0.Text = Total.ToString("N");
        }

        protected void totalMontoGrdContrapartida()
        {
            _BEFinanciamiento.tipoGrd = 5;
            _BEFinanciamiento.id_proyecto = Convert.ToInt32(Request.QueryString["id"]);

            txtAcumuladogrdContraTab0.Text = _objBLFinanciamiento.F_spMON_MontoTotalGrd(_BEFinanciamiento);

        }

        //private void VarPnsu(Int64 id)
        //{
        //    //if (tipo == 0)
        //    //{
        //    DataTable _obj;
        //    BEVarPnsu _objVarPnsu = new BEVarPnsu();
        //    _objVarPnsu.Id_Solicitudes = id;
        //    _obj = objBLProyecto.spSOL_Seguimiento_Variable_PNSU(_objVarPnsu);
        //    if (_obj.Rows.Count > 0)
        //    {
        //        //AGUA POTABLE
        //        txtA_captacion_nuc.Text = _obj.Rows[0]["a_captacion_nuc"].ToString();//
        //        txtA_captacion_cacl.Text = _obj.Rows[0]["a_captacion_cacl"].ToString();//
        //        txtA_trataPotable_num.Text = _obj.Rows[0]["a_trataPotable_num"].ToString();//
        //        txtA_trataPotable_cap.Text = _obj.Rows[0]["a_trataPotable_cap"].ToString();//

        //        txtA_conduccion_nuc.Text = _obj.Rows[0]["a_conduccion_nuc"].ToString();
        //        txtA_conduccion_cacd.Text = _obj.Rows[0]["a_conduccion_cacd"].ToString();

        //        txtA_impulsion_num.Text = _obj.Rows[0]["a_impulsion_num"].ToString();
        //        txtA_impulsion_cap.Text = _obj.Rows[0]["a_impulsion_cap"].ToString();

        //        txtA_estacion_num.Text = _obj.Rows[0]["a_estacion_num"].ToString();

        //        txtA_reservorio_nur.Text = _obj.Rows[0]["a_reservorio_nur"].ToString();
        //        txtA_reservorio_carm.Text = _obj.Rows[0]["a_reservorio_carm"].ToString();
        //        txtA_aduccion_nua.Text = _obj.Rows[0]["a_aduccion_nua"].ToString();
        //        txtA_aduccion_caad.Text = _obj.Rows[0]["a_aduccion_caad"].ToString();

        //        txtA_redes_nur.Text = _obj.Rows[0]["a_redes_nur"].ToString();
        //        txtA_redes_card.Text = _obj.Rows[0]["a_redes_card"].ToString();

        //        txtA_conexion_nucn.Text = _obj.Rows[0]["a_conexion_nucn"].ToString();
        //        txtA_conexion_nucr.Text = _obj.Rows[0]["a_conexion_nucr"].ToString();
        //        txtA_conexion_nucp.Text = _obj.Rows[0]["a_conexion_nucp"].ToString();

        //        txtA_captacion_costo.Text = _obj.Rows[0]["a_captacion_costo"].ToString();//
        //        txtA_trataPotable_costo.Text = _obj.Rows[0]["a_trataPotable_costo"].ToString();//
        //        txtA_conduccion_costo.Text = _obj.Rows[0]["a_conduccion_costo"].ToString();
        //        txtA_impulsion_costo.Text = _obj.Rows[0]["a_impulsion_costo"].ToString();
        //        txtA_estacion_costo.Text = _obj.Rows[0]["a_estacion_costo"].ToString();
        //        txtA_reservorio_costo.Text = _obj.Rows[0]["a_reservorio_costo"].ToString();
        //        txtA_aduccion_costo.Text = _obj.Rows[0]["a_aduccion_costo"].ToString();
        //        txtA_redes_costo.Text = _obj.Rows[0]["a_redes_costo"].ToString();
        //        txtA_conexion_nucnCosto.Text = _obj.Rows[0]["a_conexion_nucnCosto"].ToString();
        //        txtA_conexion_nucrCosto.Text = _obj.Rows[0]["a_conexion_nucrCosto"].ToString();
        //        txtA_conexion_nucpCosto.Text = _obj.Rows[0]["a_conexion_nucpCosto"].ToString();

        //        if (txtA_captacion_nuc.Text == "0" && txtA_trataPotable_num.Text == "0" && txtA_conduccion_nuc.Text == "0" && txtA_impulsion_num.Text == "0" &&
        //           txtA_estacion_num.Text == "0" && txtA_reservorio_nur.Text == "0" && txtA_aduccion_nua.Text == "0" && txtA_redes_nur.Text == "0" &&
        //           txtA_conexion_nucn.Text == "0" && txtA_conexion_nucr.Text == "0" && txtA_conexion_nucp.Text == "0")
        //        {
        //            ImgLineaAgua.Visible = false;
        //            lblTitAguaPotable.Visible = false;
        //            tblAgua.Visible = false;
        //        }
        //        else
        //        {

        //            if (txtA_captacion_nuc.Text == "0" && txtA_captacion_cacl.Text == "0")
        //            {
        //                captacion_A.Visible = false;
        //            }

        //            if (txtA_conduccion_nuc.Text == "0" && txtA_conduccion_cacd.Text == "0")
        //            {
        //                conduccion_A.Visible = false;
        //            }

        //            if (txtA_trataPotable_num.Text == "0" && txtA_trataPotable_cap.Text == "0")
        //            {
        //                tratoPotable_A.Visible = false;
        //            }
        //            if (txtA_impulsion_num.Text == "0" && txtA_impulsion_cap.Text == "0")
        //            {
        //                impulsion_A.Visible = false;
        //            }
        //            if (txtA_estacion_num.Text == "0" && txtA_estacion_costo.Text == "0")
        //            {
        //                estacion_A.Visible = false;
        //            }
        //            if (txtA_reservorio_nur.Text == "0" && txtA_reservorio_carm.Text == "0")
        //            {
        //                reservorio_A.Visible = false;
        //            }
        //            if (txtA_aduccion_nua.Text == "0" && txtA_aduccion_caad.Text == "0")
        //            {
        //                aduccion_A.Visible = false;
        //            }
        //            if (txtA_redes_nur.Text == "0" && txtA_redes_card.Text == "0")
        //            {
        //                redes_A.Visible = false;
        //            }

        //            if (txtA_conexion_nucn.Text == "0" && txtA_conexion_nucr.Text == "0" && txtA_conexion_nucp.Text == "0")
        //            {
        //                lblConexionAgua.Visible = false;

        //                txtA_conexion_nucn.Visible = false;
        //                lblConexionNuevas.Visible = false;
        //                txtA_conexion_nucnCosto.Visible = false;

        //                txtA_conexion_nucr.Visible = false;
        //                lblConexionRehabilitacion.Visible = false;
        //                txtA_conexion_nucrCosto.Visible = false;

        //                txtA_conexion_nucp.Visible = false;
        //                lblConexionPiletas.Visible = false;
        //                txtA_conexion_nucpCosto.Visible = false;
        //            }
        //            else
        //            {
        //                if (txtA_conexion_nucn.Text == "0" && txtA_conexion_nucnCosto.Text == "0")
        //                {
        //                    txtA_conexion_nucn.Visible = false;
        //                    lblConexionNuevas.Visible = false;
        //                    txtA_conexion_nucnCosto.Visible = false;
        //                }

        //                if (txtA_conexion_nucr.Text == "0" && txtA_conexion_nucrCosto.Text == "0")
        //                {
        //                    txtA_conexion_nucr.Visible = false;
        //                    lblConexionRehabilitacion.Visible = false;
        //                    txtA_conexion_nucrCosto.Visible = false;
        //                }

        //                if (txtA_conexion_nucp.Text == "0" && txtA_conexion_nucpCosto.Text == "0")
        //                {
        //                    txtA_conexion_nucp.Visible = false;
        //                    lblConexionPiletas.Visible = false;
        //                    txtA_conexion_nucpCosto.Visible = false;
        //                }
        //            }

        //        }

        //        //ALCANTARILLADO
        //        txtS_colector_num.Text = _obj.Rows[0]["s_colectores_num"].ToString();
        //        txtS_colector_cap.Text = _obj.Rows[0]["s_colectores_cap"].ToString();

        //        txtS_camara_num.Text = _obj.Rows[0]["s_camara_num"].ToString();

        //        txtS_planta_nup.Text = _obj.Rows[0]["s_planta_nup"].ToString();
        //        txtS_planta_capl.Text = _obj.Rows[0]["s_planta_capl"].ToString();

        //        txtS_agua_nua.Text = _obj.Rows[0]["s_agua_nua"].ToString();
        //        txtS_agua_caad.Text = _obj.Rows[0]["s_agua_caad"].ToString();

        //        txtS_conexion_nucn.Text = _obj.Rows[0]["s_conexion_nucn"].ToString();
        //        txtS_conexion_nucr.Text = _obj.Rows[0]["s_conexion_nucr"].ToString();
        //        txtS_conexion_nuclo.Text = _obj.Rows[0]["s_conexion_nuclo"].ToString();

        //        txtS_efluente_num.Text = _obj.Rows[0]["s_efluente_num"].ToString();
        //        txtS_efluente_cap.Text = _obj.Rows[0]["s_efluente_cap"].ToString();

        //        txtS_impulsion_num.Text = _obj.Rows[0]["s_impulsion_num"].ToString();
        //        txtS_impulsion_cap.Text = _obj.Rows[0]["s_impulsion_cap"].ToString();

        //        txtS_colector_costo.Text = _obj.Rows[0]["s_colectoRedes_costo"].ToString();//
        //        txtS_camara_costo.Text = _obj.Rows[0]["s_camara_costo"].ToString();//
        //        txtS_planta_costo.Text = _obj.Rows[0]["s_planta_costo"].ToString();
        //        txtS_agua_costo.Text = _obj.Rows[0]["s_emisor_costo"].ToString();
        //        txtS_conexion_nucnCosto.Text = _obj.Rows[0]["s_conexion_nucnCosto"].ToString();
        //        txtS_conexion_nucrCosto.Text = _obj.Rows[0]["s_conexion_nucrCosto"].ToString();
        //        txtS_conexion_nucloCosto.Text = _obj.Rows[0]["s_conexion_nucloCosto"].ToString();
        //        txtS_efluente_costo.Text = _obj.Rows[0]["s_efluente_costo"].ToString();
        //        txtS_impulsion_costo.Text = _obj.Rows[0]["s_impulsion_costo"].ToString();

        //        if (txtS_colector_num.Text == "0" && txtS_camara_num.Text == "0" && txtS_planta_nup.Text == "0" && txtS_agua_nua.Text == "0" &&
        //            txtS_conexion_nucn.Text == "0" && txtS_conexion_nucr.Text == "0" && txtS_conexion_nuclo.Text == "0" && txtS_efluente_num.Text == "0" &&
        //            txtS_impulsion_num.Text == "0")
        //        {
        //            ImgLineaAlcantarillado.Visible = false;
        //            lblTitAlcantarillado.Visible = false;
        //            tblAlcantarillado.Visible = false;
        //        }
        //        else
        //        {
        //            if (txtS_efluente_num.Text == "0" && txtS_efluente_cap.Text == "0")
        //            {
        //                efluente_S.Visible = false;
        //            }
        //            if (txtS_planta_nup.Text == "0" && txtS_planta_capl.Text == "0")
        //            {
        //                planta_S.Visible = false;
        //            }
        //            if (txtS_agua_nua.Text == "0" && txtS_agua_caad.Text == "0")
        //            {
        //                emisor_S.Visible = false;
        //            }
        //            if (txtS_impulsion_num.Text == "0" && txtS_impulsion_cap.Text == "0")
        //            {
        //                impulsion_S.Visible = false;
        //            }
        //            if (txtS_camara_num.Text == "0" && txtS_camara_costo.Text == "0")
        //            {
        //                camara_S.Visible = false;
        //            }
        //            if (txtS_colector_num.Text == "0" && txtS_colector_cap.Text == "0")
        //            {
        //                redes_S.Visible = false;
        //            }

        //            if (txtS_conexion_nucn.Text == "0" && txtS_conexion_nucr.Text == "0" && txtS_conexion_nuclo.Text == "0")
        //            {
        //                lblConexionDesagueS.Visible = false;

        //                txtS_conexion_nucn.Visible = false;
        //                lblS_Nuevasconexiones.Visible = false;
        //                txtS_conexion_nucnCosto.Visible = false;

        //                txtS_conexion_nucr.Visible = false;
        //                lblS_Rehabilitacionconexion.Visible = false;
        //                txtS_conexion_nucrCosto.Visible = false;

        //                txtS_conexion_nuclo.Visible = false;
        //                lblS_UnidadesBasicas.Visible = false;
        //                txtS_conexion_nucloCosto.Visible = false;
        //            }
        //            else
        //            {
        //                if (txtS_conexion_nucn.Text == "0" && txtS_conexion_nucnCosto.Text == "0")
        //                {
        //                    txtS_conexion_nucn.Visible = false;
        //                    lblS_Nuevasconexiones.Visible = false;
        //                    txtS_conexion_nucnCosto.Visible = false;
        //                }

        //                if (txtS_conexion_nucr.Text == "0" && txtS_conexion_nucrCosto.Text == "0")
        //                {
        //                    txtS_conexion_nucr.Visible = false;
        //                    lblS_Rehabilitacionconexion.Visible = false;
        //                    txtS_conexion_nucrCosto.Visible = false;
        //                }

        //                if (txtS_conexion_nuclo.Text == "0" && txtS_conexion_nucloCosto.Text == "0")
        //                {
        //                    txtS_conexion_nuclo.Visible = false;
        //                    lblS_UnidadesBasicas.Visible = false;
        //                    txtS_conexion_nucloCosto.Visible = false;
        //                }
        //            }
        //        }

        //        //DRENAJE PLUVIAL
        //        txtD_tuberia_num.Text = _obj.Rows[0]["d_tuberia_num"].ToString();
        //        txtD_tuberia_cap.Text = _obj.Rows[0]["d_tuberia_cap"].ToString();

        //        txtD_cuneta_num.Text = _obj.Rows[0]["d_cuneta_num"].ToString();
        //        txtD_cuneta_cap.Text = _obj.Rows[0]["d_cuneta_cap"].ToString();

        //        txtD_tormenta_num.Text = _obj.Rows[0]["d_tormenta_num"].ToString();
        //        txtD_conexion_num.Text = _obj.Rows[0]["d_conexion_num"].ToString();
        //        txtD_inspeccion_num.Text = _obj.Rows[0]["d_inspeccion_num"].ToString();

        //        txtD_secundario_num.Text = _obj.Rows[0]["d_secundario_num"].ToString();
        //        txtD_secundario_cap.Text = _obj.Rows[0]["d_secundario_cap"].ToString();

        //        txtD_principal_num.Text = _obj.Rows[0]["d_principal_num"].ToString();
        //        txtD_principal_cap.Text = _obj.Rows[0]["d_principal_cap"].ToString();

        //        txtD_tuberia_costo.Text = _obj.Rows[0]["d_tuberia_costo"].ToString();
        //        txtD_cuneta_costo.Text = _obj.Rows[0]["d_cuneta_costo"].ToString();
        //        txtD_tormenta_costo.Text = _obj.Rows[0]["d_tormenta_costo"].ToString();
        //        txtD_conexion_costo.Text = _obj.Rows[0]["d_conexion_costo"].ToString();
        //        txtD_inspeccion_costo.Text = _obj.Rows[0]["d_inspeccion_costo"].ToString();
        //        txtD_secundario_costo.Text = _obj.Rows[0]["d_secundario_costo"].ToString();
        //        txtD_principal_costo.Text = _obj.Rows[0]["d_principal_costo"].ToString();

        //        if (txtD_tuberia_num.Text == "0" && txtD_cuneta_num.Text == "0" && txtD_tormenta_num.Text == "0" && txtD_conexion_num.Text == "0" &&
        //         txtD_inspeccion_num.Text == "0" && txtD_secundario_num.Text == "0" && txtD_principal_num.Text == "0")
        //        {
        //            ImgLineaDrenaje.Visible = false;
        //            lblTitDrenaje.Visible = false;
        //            tblDrenaje.Visible = false;
        //        }
        //        else
        //        {
        //            if (txtD_tuberia_num.Text == "0" && txtD_tuberia_cap.Text == "0")
        //            {
        //                tuberia_D.Visible = false;
        //            }

        //            if (txtD_cuneta_num.Text == "0" && txtD_cuneta_cap.Text == "0")
        //            {
        //                cuneta_D.Visible = false;
        //            }

        //            if (txtD_tormenta_num.Text == "0" && txtD_tormenta_costo.Text == "0")
        //            {
        //                tormenta_D.Visible = false;
        //            }

        //            if (txtD_conexion_num.Text == "0" && txtD_conexion_costo.Text == "0")
        //            {
        //                camara_D.Visible = false;
        //            }
        //        }


        //        //VARIOS
        //        txtV_cant.Text = _obj.Rows[0]["v_cant"].ToString();
        //        txtV_costo.Text = _obj.Rows[0]["v_costo"].ToString();

        //        if (txtV_cant.Text == "0")
        //        {
        //            ImgLineaVarios.Visible = false;
        //            lblTitVarios.Visible = false;
        //            tblVarios.Visible = false;
        //        }



        //    }
        //    else
        //    {

        //        //llenarcampos();
        //        tb_pnsu.Visible = false;
        //    }


        //}

        private void VarPmib(Int64 id)
        {
            //if (tipo == 0)
            //{
            DataTable _obj;
            BEVarPimb _objVarPimb = new BEVarPimb();
            _objVarPimb.Id_Solicitudes = id;
            _obj = objBLProyecto.spSOL_Seguimiento_Variable_PMIB(_objVarPimb);
            if (_obj.Rows.Count > 0)
            {
                StringBuilder strMetasPMIB = new StringBuilder();
                strMetasPMIB.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");

                //PAVIMENTO
                if (Convert.ToDecimal(_obj.Rows[0]["P_concreto_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["P_asfalto_m2"].ToString()) == 0 &&
                        Convert.ToDecimal(_obj.Rows[0]["P_emboquillado_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["P_adoquinado_m2"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPMIB.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >PAVIMENTO</td></tr>");

                    if (Convert.ToDecimal(_obj.Rows[0]["P_concreto_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Concreto: {0} M2.</li></ul></td>", _obj.Rows[0]["P_concreto_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["P_asfalto_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Asfalto: {0} M2.</li></ul></td>", _obj.Rows[0]["P_asfalto_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["P_emboquillado_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Emboquillado: {0} M2.</li></ul></td>", _obj.Rows[0]["P_emboquillado_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["P_adoquinado_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Adoquinado: {0} M2.</li></ul></td>", _obj.Rows[0]["P_adoquinado_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                }

                //VEREDAS
                if (Convert.ToDecimal(_obj.Rows[0]["V_concreto_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["V_adoquinado_m2"].ToString()) == 0 &&
                        Convert.ToDecimal(_obj.Rows[0]["V_piedra_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["V_emboquillado_m2"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPMIB.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >VEREDAS</td></tr>");

                    if (Convert.ToDecimal(_obj.Rows[0]["V_concreto_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Concreto: {0} M2.</li></ul></td>", _obj.Rows[0]["V_concreto_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["V_adoquinado_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Adoquinado: {0} M2.</li></ul></td>", _obj.Rows[0]["V_adoquinado_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["V_piedra_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Piedra laja: {0} M2.</li></ul></td>", _obj.Rows[0]["V_piedra_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["V_emboquillado_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Emboquillado: {0} M2.</li></ul></td>", _obj.Rows[0]["V_emboquillado_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                }

                //Drenaje Pluvial
                if (Convert.ToDecimal(_obj.Rows[0]["D_concreto_ml"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["D_piedra_ml"].ToString()) == 0 &&
                        Convert.ToDecimal(_obj.Rows[0]["V_piedra_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["V_emboquillado_m2"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPMIB.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >DRENAJE PLUVIAL</td></tr>");

                    if (Convert.ToDecimal(_obj.Rows[0]["D_concreto_ml"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Drenaje pluvial de concreto: {0} ML.</li></ul></td>", _obj.Rows[0]["D_concreto_ml"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["D_piedra_ml"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Drenaje pluvial de piedra: {0} ML.</li></ul></td>", _obj.Rows[0]["D_piedra_ml"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                }

                //LOSA DE RECREACION MULTIUSOS
                if (Convert.ToDecimal(_obj.Rows[0]["l_administracion_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["l_losa_m2"].ToString()) == 0 &&
                        Convert.ToDecimal(_obj.Rows[0]["l_tribuna_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["l_cerco_ml"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["l_iluminacion_u"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPMIB.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >LOSA DE RECREACION MULTIUSOS</td></tr>");

                    if (Convert.ToDecimal(_obj.Rows[0]["l_losa_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Losa deportiva de concreto: {0} M2.</li></ul></td>", _obj.Rows[0]["l_losa_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["l_tribuna_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Tribunas: {0} M2.</li></ul></td>", _obj.Rows[0]["l_tribuna_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["l_cerco_ml"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Cerco Perimétrico: {0} ML.</li></ul></td>", _obj.Rows[0]["l_cerco_ml"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["l_iluminacion_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Iluminación externa: {0} unidades.</li></ul></td>", _obj.Rows[0]["l_iluminacion_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["l_administracion_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Administración (Incl.SS.HH.): {0} Global.</li></ul></td>", _obj.Rows[0]["l_administracion_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                }

                //CENTRO COMUNAL COMERCIAL
                if (Convert.ToDecimal(_obj.Rows[0]["c_puestas_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["c_administracion_m2"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["c_cerco_ml"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPMIB.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' CENTRO COMUNAL COMERCIAL</td></tr>");

                    if (Convert.ToDecimal(_obj.Rows[0]["c_puestas_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Puestas de ventas: {0} M2.</li></ul></td>", _obj.Rows[0]["c_puestas_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["c_administracion_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Cerco perimétrico: {0} ML.</li></ul></td>", _obj.Rows[0]["c_administracion_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["c_cerco_ml"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Administración(Incl.SS.HH): {0} Global.</li></ul></td>", _obj.Rows[0]["c_cerco_ml"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                }

                //OTROS
                if (Convert.ToDecimal(_obj.Rows[0]["O_muro_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_ponton_m2"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["O_bermas_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_verdes_m2"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["o_puente_m2"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_jardineras_u"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["O_plantones_u"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_sanitarias_u"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["O_electricas_u"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_bancas_u"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["O_basureros_u"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_pergolas_u"].ToString()) == 0 &&
                    Convert.ToDecimal(_obj.Rows[0]["O_glorietas_u"].ToString()) == 0 && Convert.ToDecimal(_obj.Rows[0]["O_otros_u"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPMIB.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' OTROS</td></tr>");

                    if (Convert.ToDecimal(_obj.Rows[0]["O_ponton_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Ponton: {0} M2.</li></ul></td>", _obj.Rows[0]["O_ponton_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_bermas_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Bermas: {0} M2.</li></ul></td>", _obj.Rows[0]["O_bermas_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_verdes_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Áreas Verdes: {0} M2.</li></ul></td>", _obj.Rows[0]["O_verdes_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["o_puente_m2"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Puente: {0} M2.</li></ul></td>", _obj.Rows[0]["o_puente_m2"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_jardineras_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Jardineras: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_jardineras_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                    if (Convert.ToDecimal(_obj.Rows[0]["O_plantones_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Plantones: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_plantones_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                    if (Convert.ToDecimal(_obj.Rows[0]["O_jardineras_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Instalaciones sanitarias: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_jardineras_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                    if (Convert.ToDecimal(_obj.Rows[0]["O_electricas_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Instalaciones Eléctricas: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_electricas_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_bancas_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Bancas: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_bancas_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_basureros_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Basureros: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_basureros_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_pergolas_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Pergolas: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_pergolas_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_glorietas_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Glorietas: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_glorietas_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }

                    if (Convert.ToDecimal(_obj.Rows[0]["O_otros_u"].ToString()) != 0)
                    {
                        strMetasPMIB.Append("<tr>");
                        strMetasPMIB.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>Otros: {0} Unidad(s).</li></ul></td>", _obj.Rows[0]["O_otros_u"].ToString()));
                        strMetasPMIB.Append("</tr>");
                    }
                }


                strMetasPMIB.Append("</table>");
                lblMetas.Text = strMetasPMIB.ToString();
            }


        }

        private void VarPnsu(Int64 id)
        {
            DataTable _obj;
            BEVarPnsu _objVarPnsu = new BEVarPnsu();
            _objVarPnsu.Id_Solicitudes = id;
            _obj = objBLProyecto.spSOL_Seguimiento_Variable_PNSU(_objVarPnsu);
            if (_obj.Rows.Count > 0)
            {
                StringBuilder strMetasPNSU = new StringBuilder();
                strMetasPNSU.Append("<table cellspacing='0' cellpadding='2' runat='server' style='width: 96%;  id='tblAvanzado'>");

                //AGUAR POTABLE
                if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) == 0)
                {
                }
                else
                {
                    strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Agua Potable</td></tr>");

                    if (Convert.ToInt32(_obj.Rows[0]["a_captacion_nuc"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Captación.</li></ul></td>", _obj.Rows[0]["a_captacion_nuc"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_conduccion_nuc"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de líneas de conducción.</li></ul></td>", _obj.Rows[0]["a_conduccion_nuc"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_trataPotable_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua potable.</li></ul></td>", _obj.Rows[0]["a_trataPotable_num"].ToString(), _obj.Rows[0]["a_trataPotable_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_impulsion_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} ML de línea de impulsion.</li></ul></td>", _obj.Rows[0]["a_impulsion_num"].ToString(), _obj.Rows[0]["a_impulsion_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_estacion_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Estación de bombeo.</li></ul></td>", _obj.Rows[0]["a_estacion_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_reservorio_nur"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Reservorio(s).</li></ul></td>", _obj.Rows[0]["a_reservorio_nur"].ToString(), _obj.Rows[0]["a_reservorio_carm"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_aduccion_nua"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} linea(s) de aducción.</li></ul></td>", _obj.Rows[0]["a_aduccion_nua"].ToString(), _obj.Rows[0]["a_aduccion_caad"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_redes_nur"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Redes de distribución.</li></ul></td>", _obj.Rows[0]["a_redes_nur"].ToString(), _obj.Rows[0]["a_redes_card"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucn"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucn"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucr"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucr"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["a_conexion_nucp"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Piletas.</li></ul></td>", _obj.Rows[0]["a_conexion_nucp"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }



                }

                //ALCANTARILLADO
                if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_colectores_cap"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_planta_capl"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_agua_caad"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) == 0 && Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_impulsion_cap"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["s_efluente_cap"].ToString()) == 0.00)
                {
                }
                else
                {
                    strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Alcantarillado</td></tr>");

                    if (Convert.ToInt32(_obj.Rows[0]["s_efluente_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Linea(s) efluente de agua residual tratada.</li></ul></td>", _obj.Rows[0]["s_efluente_num"].ToString(), _obj.Rows[0]["s_efluente_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_planta_nup"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Planta(s) de tratamiento de agua(s) residual.</li></ul></td>", _obj.Rows[0]["s_planta_nup"].ToString(), _obj.Rows[0]["s_planta_capl"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_agua_nua"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Emisor(s).</li></ul></td>", _obj.Rows[0]["s_agua_nua"].ToString(), _obj.Rows[0]["s_agua_caad"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_impulsion_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Línea(s) de impulsión.</li></ul></td>", _obj.Rows[0]["s_impulsion_num"].ToString(), _obj.Rows[0]["s_impulsion_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_camara_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de Camara de Bombeo.</li></ul></td>", _obj.Rows[0]["s_camara_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_colectores_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} M de Redes de alcantarillado sanitario.</li></ul></td>", _obj.Rows[0]["s_colectores_num"].ToString(), _obj.Rows[0]["s_colectores_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucn"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones nuevas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucn"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nucr"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Conexiones rehabilitadas.</li></ul></td>", _obj.Rows[0]["s_conexion_nucr"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["s_conexion_nuclo"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) básica de saneamiento.</li></ul></td>", _obj.Rows[0]["s_conexion_nuclo"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }
                }

                //DRENAJE PLUVIAL
                if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_tuberia_cap"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_cuneta_cap"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) == 0 &&
                        Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_secundario_cap"].ToString()) == 0.00 &&
                        Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) == 0 && Convert.ToDouble(_obj.Rows[0]["d_principal_cap"].ToString()) == 0.00)
                {
                }
                else
                {

                    strMetasPNSU.Append("<tr><td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' >COMPONENTE: Drenaje Pluvial</td></tr>");

                    if (Convert.ToInt32(_obj.Rows[0]["d_tuberia_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} M Tuberia de conexión.</li></ul></td>", _obj.Rows[0]["d_tuberia_num"].ToString(), _obj.Rows[0]["d_tuberia_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["d_cuneta_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} M de cunetas.</li></ul></td>", _obj.Rows[0]["d_cuneta_num"].ToString(), _obj.Rows[0]["d_cuneta_cap"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["d_tormenta_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de boca de tormenta.</li></ul></td>", _obj.Rows[0]["d_tormenta_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["d_conexion_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de conexión.</li></ul></td>", _obj.Rows[0]["d_conexion_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["d_inspeccion_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} Unidad(s) de cámara de inspección.</li></ul></td>", _obj.Rows[0]["d_inspeccion_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["d_secundario_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} M Colector(s) Secundario.</li></ul></td>", _obj.Rows[0]["d_secundario_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }

                    if (Convert.ToInt32(_obj.Rows[0]["d_principal_num"].ToString()) != 0)
                    {
                        strMetasPNSU.Append("<tr>");
                        strMetasPNSU.Append(string.Format("<td style='text-align: left; border:0px none;font-family: Calibri;font-size: 13px;' ><ul style='margin:0px;-moz-padding-start: 25px;'><li>{0} M Colector(s) Principal.</li></ul></td>", _obj.Rows[0]["d_principal_num"].ToString()));
                        strMetasPNSU.Append("</tr>");
                    }
                }

                strMetasPNSU.Append("</table>");
                lblMetas.Text = strMetasPNSU.ToString();


            }




        }
        protected void imgbtnExportar_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "FichaProyectoSNIP_" + lblSnip_PMIB.Text + "(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }

    }
}