﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Monitor
{
    public class Validacion
    {
        public Boolean ValidarCambioEstado(Int64 id_proyecto, int int_EstadoActual, int int_NuevoEstado, int id_TipoFinanciamiento) {
            if (id_TipoFinanciamiento == 1)
            {
                //Pre Inversion
                if (int_NuevoEstado == 38 & int_EstadoActual != 38)
                    return false;
            }
            else if (id_TipoFinanciamiento == 2)
            {
                //Expediente Técnico
                if (int_NuevoEstado == 40 & int_EstadoActual != 40)
                    return false;
            }
            else if (id_TipoFinanciamiento == 3)
            {
                //Obra
                if (int_NuevoEstado == 41 & int_EstadoActual != 41)
                    return false;
            }
            return true;
        }
    }
}