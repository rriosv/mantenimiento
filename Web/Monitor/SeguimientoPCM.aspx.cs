﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Monitor
{
    public partial class SeguimientoPCM : System.Web.UI.Page
    {
        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
        BLUtil _Metodo = new BLUtil();

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["LoginUsuario"] == null)
            {

                Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                Response.Redirect("~/login?ms=1");
            }

            Session.Timeout = 360;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarRegistro();
            }
        }

        public void CargarRegistro()
        {
            int idProy = Convert.ToInt32(Request.QueryString["id"]);
            _BEEjecucion.id_proyecto = idProy;
            DataTable dt = new DataTable();
            dt = _objBLEjecucion.spMON_SeguimientoPCM(_BEEjecucion);
            grdSeguimientoPCM.DataSource = dt;
            grdSeguimientoPCM.DataBind();
        }

        public void CargarFiltros()
        {
            DataSet ds = new DataSet();

            ds = _objBLEjecucion.spMON_CombosSeguimientoPCM();

            ddlEtapaCompromisoPCM.DataSource = ds.Tables[0];
            ddlEtapaCompromisoPCM.DataValueField = "valor";
            ddlEtapaCompromisoPCM.DataTextField = "nombre";
            ddlEtapaCompromisoPCM.DataBind();
            ddlEtapaCompromisoPCM.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlMesTerminoPCM.DataSource = ds.Tables[1];
            ddlMesTerminoPCM.DataValueField = "valor";
            ddlMesTerminoPCM.DataTextField = "nombre";
            ddlMesTerminoPCM.DataBind();
            ddlMesTerminoPCM.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlAnioTerminoPCM.DataSource = ds.Tables[2];
            ddlAnioTerminoPCM.DataValueField = "valor";
            ddlAnioTerminoPCM.DataTextField = "nombre";
            ddlAnioTerminoPCM.DataBind();
            ddlAnioTerminoPCM.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlGradoCumplimientoPCM.DataSource = ds.Tables[3];
            ddlGradoCumplimientoPCM.DataValueField = "valor";
            ddlGradoCumplimientoPCM.DataTextField = "nombre";
            ddlGradoCumplimientoPCM.DataBind();
            ddlGradoCumplimientoPCM.Items.Insert(0, new ListItem("-Seleccione-", ""));

            ddlCierraBrecha.DataSource = ds.Tables[4];
            ddlCierraBrecha.DataValueField = "valor";
            ddlCierraBrecha.DataTextField = "nombre";
            ddlCierraBrecha.DataBind();
            ddlCierraBrecha.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void imgbtnAgregarSeguimientoPCM_Click(object sender, ImageClickEventArgs e)
        {
            CargarFiltros();
            Panel_AgregarSeguimientoPCM.Visible = true;
            imgbtnAgregarSeguimientoPCM.Visible = false;
            btnGuardarSeguimientoPCM.Visible = true;
            btnModificarSeguimientoPCM.Visible = false;

            Up_PriorizacionPCM.Update();
        }

        protected void btnGuardarSeguimientoPCM_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroPCM())
            {
                int idProy = Convert.ToInt32(Request.QueryString["id"]);
                _BEEjecucion.id_proyecto = idProy;
                _BEEjecucion.id_etapaCompromiso = Convert.ToInt32(ddlEtapaCompromisoPCM.SelectedValue);
                _BEEjecucion.mes = ddlMesTerminoPCM.SelectedValue;
                _BEEjecucion.anio = ddlAnioTerminoPCM.SelectedValue;
                _BEEjecucion.id_gradoCumplimiento = Convert.ToInt32(ddlGradoCumplimientoPCM.SelectedValue);
                _BEEjecucion.id_cierreBrecha = Convert.ToInt32(ddlCierraBrecha.SelectedValue);
                _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaTerminoPCM.Text);
                _BEEjecucion.id_usuario = Convert.ToInt32((Session["IdUsuario"]).ToString());

                int val = _objBLEjecucion.I_MON_SeguimientoPCM(_BEEjecucion);


                if (val == 1)
                {
                    string script = "<script>alert('Registro Correcto.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargarRegistro();

                    Panel_AgregarSeguimientoPCM.Visible = false;
                    imgbtnAgregarSeguimientoPCM.Visible = true;

                    Up_PriorizacionPCM.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }
        }
        protected void btnCancelaSeguimientoPCM_Click(object sender, EventArgs e)
        {
            Panel_AgregarSeguimientoPCM.Visible = false;
            imgbtnAgregarSeguimientoPCM.Visible = true;

            Up_PriorizacionPCM.Update();
        }

        protected void grdSeguimientoPCM_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarFiltros();

            lblIdSeguimientoPCM.Text = grdSeguimientoPCM.SelectedDataKey.Value.ToString();
            GridViewRow row = grdSeguimientoPCM.SelectedRow;

            ddlEtapaCompromisoPCM.SelectedValue = ((Label)row.FindControl("lblIdEtapaCompromiso")).Text;

            ddlMesTerminoPCM.SelectedValue = ((Label)row.FindControl("lblIdMes")).Text;
            ddlAnioTerminoPCM.SelectedValue= ((Label)row.FindControl("lblAnio")).Text;
            ddlGradoCumplimientoPCM.SelectedValue = ((Label)row.FindControl("lblIdGradoCumplimiento")).Text;
            ddlCierraBrecha.SelectedValue = ((Label)row.FindControl("lblIdCierreBrecha")).Text;
            txtFechaTerminoPCM.Text = ((Label)row.FindControl("lblFechaTermino")).Text;

            lblNomUsuarioSeguimientoPCM.Text = "Actualizó: " + ((Label)row.FindControl("lblId_Usuario")).Text + " - " + ((Label)row.FindControl("lblFecha_Update")).Text;

            imgbtnAgregarSeguimientoPCM.Visible = false;
            Panel_AgregarSeguimientoPCM.Visible = true;
            btnGuardarSeguimientoPCM.Visible = false;
            btnModificarSeguimientoPCM.Visible = true;

            Up_PriorizacionPCM.Update();
        }

        protected void grdSeguimientoPCM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                Control ctl = e.CommandSource as Control;
                GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                object objTemp = grdSeguimientoPCM.DataKeys[CurrentRow.RowIndex].Value as object;

                _BEEjecucion.id_item = Convert.ToInt32(objTemp.ToString());
                _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha("");
                _BEEjecucion.mes = "0";
                _BEEjecucion.anio = "0";
                _BEEjecucion.Tipo = 2;
                _BEEjecucion.id_usuario = Convert.ToInt32((Session["IdUsuario"]).ToString());

                int val = _objBLEjecucion.UD_MON_SeguimientoPCM(_BEEjecucion);

                if (val == 1)
                {
                    string script = "<script>alert('Eliminación Correcta.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargarRegistro();
                    Up_PriorizacionPCM.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }

            }
        }

        protected void btnModificarSeguimientoPCM_Click(object sender, EventArgs e)
        {
            if (ValidarRegistroPCM())
            {
                _BEEjecucion.id_item = Convert.ToInt32(lblIdSeguimientoPCM.Text);
                _BEEjecucion.id_etapaCompromiso = Convert.ToInt32(ddlEtapaCompromisoPCM.SelectedValue);
                _BEEjecucion.mes = ddlMesTerminoPCM.SelectedValue;
                _BEEjecucion.anio = ddlAnioTerminoPCM.SelectedValue;
                _BEEjecucion.id_gradoCumplimiento = Convert.ToInt32(ddlGradoCumplimientoPCM.SelectedValue);
                _BEEjecucion.id_cierreBrecha = Convert.ToInt32(ddlCierraBrecha.SelectedValue);
                _BEEjecucion.Date_fechaFin = _Metodo.VerificaFecha(txtFechaTerminoPCM.Text);
                _BEEjecucion.id_usuario = Convert.ToInt32((Session["IdUsuario"]).ToString());
                _BEEjecucion.Tipo = 1;
                int val = _objBLEjecucion.UD_MON_SeguimientoPCM(_BEEjecucion);


                if (val == 1)
                {
                    string script = "<script>alert('Se actualizó correctamente.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    CargarRegistro();

                    Panel_AgregarSeguimientoPCM.Visible = false;
                    imgbtnAgregarSeguimientoPCM.Visible = true;

                    Up_PriorizacionPCM.Update();

                }
                else
                {
                    string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
            }

        }

        protected bool ValidarRegistroPCM()
        {
            Boolean result;
            result = true;
            string msj = "";
            if (ddlEtapaCompromisoPCM.Text == "")
            {
                msj = msj + "Seleccionar etapa compromiso.\\n";
            }

            if (ddlMesTerminoPCM.Text == "")
            {
                msj = msj + "Seleccionar mes término compromiso.\\n";
            }

            if (ddlAnioTerminoPCM.Text == "")
            {
                msj = msj + "Seleccionar año  de término compromiso.\\n";
            }

            if (ddlGradoCumplimientoPCM.Text == "")
            {
                msj = msj + "Seleccionar grado de cumplimiento.\\n";
            }

            if (ddlCierraBrecha.Text == "")
            {
                msj = msj + "Seleccionar cierre de brecha.\\n";
            }

            if (txtFechaTerminoPCM.Text == "")
            {
                msj = msj + "Ingresa fecha estimada de término.\\n";
            }

            if (msj.Length > 0)
            {
                string script = "<script>alert('" + msj + "');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                result = false;
                return result;
            }
            else
            {
                return result;
            }


        }

    }
}