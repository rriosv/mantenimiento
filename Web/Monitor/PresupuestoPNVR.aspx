﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PresupuestoPNVR.aspx.cs" Inherits="Web.Monitor.PresupuestoPNVR" Culture="es-PE" UICulture="es-PE" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Presupuesto</title>
    <link rel="Stylesheet" href="../css/print.css" type="text/css" media="print" />
    <style type="text/css">
        .celdaTabla {
            text-align: left;
            border: 1px dotted gray;
        }

        .celdaTabla2 {
            text-align: right;
        }

        .center {
            margin: auto;
        }

        input[type="submit"] {
            background-color: #28375b;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            border: 1px solid #124d77;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: arial;
            font-size: 12px;
            padding: 3px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #154682;
            width: 85px;
            font-weight: bold;
        }


            input[type="submit"]:hover {
                background-color: #0061a7;
            }

            input[type="submit"]:active {
                position: relative;
                top: 1px;
            }

        input[disabled] {
            /* Your CSS Styles */
            background-color: #F0F0F0 !important;
            color: #303030 !important;
            cursor: inherit;
        }

        input[disabled='disabled'] {
            /* Your CSS Styles */
            background-color: #F0F0F0 !important;
            color: #303030 !important;
            cursor: inherit;
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
    <asp:PlaceHolder runat="server">
        <%:System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>


        <div id="div_Ficha" style="width: 760px; height: auto; text-align: center">


            <table style="margin: auto; margin-top: 5px; margin-bottom: 6px;">
                <tr>
                    <td rowspan="2" style="text-align: left">

                        <img src="../img/logo_ministerio.jpg" height="50" alt="" style="padding-right: 15px; padding-left: 15px;" />
                    </td>
                    <td style="font-family: Calibri; font-size: 12px;">“AÑO DE LA LUCHA CONTRA LA CORRUPCIÓN E IMPUNIDAD”
                    </td>

                    <td rowspan="2" style="text-align: right">
                        <img src="../img/logoPNVR.png" height="50" alt="" style="padding-right: 15px; padding-left: 15px;" />
                    </td>

                </tr>

            </table>


            <span style="font-family: Arial; font-size: 14pt"><b>PRESUPUESTO PROYECTO DE MEJORAMIENTO DE VIVIENDA RURAL - PNVR</b></span>
            <br />
            <%-- <div id="icon_word" style="text-align:center;">
                    
                        <table style="margin:auto">
                            <tr>
                          <td>
                                <asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" width="25px" height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." />
                            </td>
                                <td>
                                    <asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" width="25px" height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />
                                  
                                </td>
                            </tr>
                        </table>
                   

                </div>--%>


            <div style="width: 100%">
                <table style="font-family: Calibri; font-size: 13px; width: 100%">

                    <tr>
                        <td style="border: 1px dotted gray">PROYECTO:</td>
                        <td class="celdaTabla" style="border: 1px dotted gray">
                            <asp:Label ID="lblNomProyecto" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                </table>
                <table id="tblSierra" runat="server" visible="false" style="font-family: Calibri; font-size: 13px; width: 100%;">
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray; width: 50px; background-color: #E1E1E1">
                            <b>N°</b>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; background-color: #E1E1E1">
                            <b>PARTIDA</b></td>
                        <td class="celdaTabla" style="border: 1px solid gray; width: 120px; background-color: #E1E1E1">
                            <b>S/.</b></td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida1" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN1" runat="server" Text="01"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida1" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto1" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="lblMonto1" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida2" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN2" runat="server" Text="02"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida2" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto2" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="lblMonto2" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida3" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN3" runat="server" Text="03"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida3" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto3" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="lblMonto3" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida19" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN19" runat="server" Text="04"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida19" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto19" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="lblMonto19" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida4" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN4" runat="server" Text="05"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida4" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto4" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="lblMonto4" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida5" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN5" runat="server" Text="06"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida5" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto5" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="lblMonto5" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida6" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN6" runat="server" Text="07"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida6" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto6" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="lblMonto6" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida7" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN7" runat="server" Text="08"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida7" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto7" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="lblMonto7" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida8" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN8" runat="server" Text="09"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida8" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto8" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="lblMonto8" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida9" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN9" runat="server" Text="10"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida9" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto9" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="lblMonto9" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida10" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN10" runat="server" Text="11"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida10" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto10" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="lblMonto10" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida11" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN11" runat="server" Text="12"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida11" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto11" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="lblMonto11" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida12" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN12" runat="server" Text="13"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida12" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto12" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="lblMonto12" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>


                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida13" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN13" runat="server" Text="14"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida13" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto13" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="lblMonto13" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblId_Usuario" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblId_Proyecto" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblIdPartida14" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN14" runat="server" Text="15"></asp:Label>

                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida14" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto14" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="lblMonto14" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida25" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN25" runat="server" Text="16"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida25" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto25" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="lblMonto25" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida26" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN26" runat="server" Text="17"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida26" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto26" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="lblMonto26" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida27" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN27" runat="server" Text="18"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida27" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto27" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="lblMonto27" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida28" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN28" runat="server" Text="19"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida28" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto28" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="lblMonto28" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>




                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray; height: 20px;"></td>
                        <td class="celdaTabla" style="border: 1px solid gray"></td>
                        <td class="celdaTabla" style="border: 1px solid gray"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border: 1px solid gray; background-color: #E1E1E1">
                            <asp:Label ID="Label1" runat="server" Text="PRESUPUESTO" Font-Bold="true"></asp:Label>
                            <b></b>
                        </td>

                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida15" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblN15" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida15" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="lblMonto15" runat="server" Text="" Enabled="false" Width="100%"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="lblMonto15" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida16" runat="server" Text="15" Visible="false"></asp:Label>
                            <asp:Label ID="lblN16" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida16" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto16" runat="server" Text="" AutoPostBack="true" OnTextChanged="lblMonto015_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="lblMonto16" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida17" runat="server" Text="16" Visible="false"></asp:Label>
                            <asp:Label ID="lblN17" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida17" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="lblMonto17" runat="server" Text="" Enabled="false" Width="100%"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="lblMonto17" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartida18" runat="server" Text="17" Visible="false"></asp:Label>
                            <asp:Label ID="lblN18" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartida18" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="lblMonto18" runat="server" Text=""></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="lblMonto18" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                </table>

                <table id="tblSelva" visible="false" runat="server" style="font-family: Calibri; font-size: 13px; width: 100%;">
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray; width: 50px; background-color: #E1E1E1">
                            <b>N°</b>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; background-color: #E1E1E1">
                            <b>PARTIDA</b></td>
                        <td class="celdaTabla" style="border: 1px solid gray; width: 120px; background-color: #E1E1E1">
                            <b>S/.</b></td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva1" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label5" runat="server" Text="01"></asp:Label>

                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva1" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva1" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtMontoSelva1" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva2" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label8" runat="server" Text="02"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva2" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva2" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtMontoSelva2" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva20" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label11" runat="server" Text="03"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva20" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva20" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtMontoSelva20" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva4" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label14" runat="server" Text="04"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva4" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva4" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txtMontoSelva4" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva21" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label17" runat="server" Text="05"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva21" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva21" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txtMontoSelva21" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva22" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label20" runat="server" Text="06"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva22" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva22" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txtMontoSelva22" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva5" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label23" runat="server" Text="07"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva5" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva5" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="txtMontoSelva5" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva23" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label26" runat="server" Text="08"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva23" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva23" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txtMontoSelva23" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva24" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label29" runat="server" Text="09"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva24" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva24" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txtMontoSelva24" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva11" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label32" runat="server" Text="10"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva11" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva11" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txtMontoSelva11" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva13" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label35" runat="server" Text="11"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva13" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva13" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtMontoSelva13" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>


                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva14" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label38" runat="server" Text="12"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva14" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva14" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txtMontoSelva14" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva25" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label3" runat="server" Text="13"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva25" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva25" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txtMontoSelva25" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva26" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label2" runat="server" Text="14"></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva26" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva26" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtMontoSelva26" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>


                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray; height: 20px;"></td>
                        <td class="celdaTabla" style="border: 1px solid gray"></td>
                        <td class="celdaTabla" style="border: 1px solid gray"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border: 1px solid gray; background-color: #E1E1E1">
                            <asp:Label ID="Label49" runat="server" Text="PRESUPUESTO" Font-Bold="true"></asp:Label>
                            <b></b>
                        </td>

                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva15" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label51" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva15" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva15" runat="server" Text="" Enabled="false" Width="100%"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txtMontoSelva15" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva16" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label54" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva16" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva16" runat="server" Text="" AutoPostBack="true" OnTextChanged="txtMontoSelva16_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txtMontoSelva16" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva17" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label57" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva17" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray; text-align: right;">
                            <asp:TextBox ID="txtMontoSelva17" runat="server" Text="" Enabled="false" Width="100%"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtMontoSelva17" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblIdPartidaSelva18" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label60" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:Label ID="lblPartidaSelva18" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="celdaTabla" style="border: 1px solid gray">
                            <asp:TextBox ID="txtMontoSelva18" runat="server" Text=""></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="txtMontoSelva18" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                        </td>
                    </tr>

                </table>
                <div style="text-align: center; font-family: Calibri;">
                    <table style="margin: auto">
                        <tr>
                            <td>¿Finalizó el registro presupuestal?:</td>
                            <td>
                                <asp:RadioButtonList runat="server" ID="chbFinalizacion" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </div>

                <br />
                <table style="width: 100%">
                    <tr>
                        <td style="padding-left: 50px; text-align: left; font-size: 14px;">
                            <asp:Label ID="lblNomActualiza" runat="server" Text="" Font-Names="Arial" Font-Size="10px" CssClass="izquierda"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <asp:Button ID="btnGuardarPresupuesto" runat="server" Text="GUARDAR" Width="120px" OnClick="btnGuardarPresupuesto_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
        </div>

    </form>
</body>
</html>
