﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iframe_GraficoRiesgo.aspx.cs" Inherits="Web.Monitor.iframe_GraficoRiesgo" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Seguimiento de Riesgo</title>

    <style type="text/css">
        body {
            font: 10pt arial;
        }

        div#info {
            width: 600px;
            text-align: center;
            margin-top: 2em;
            font-size: 1.2em;
        }
    </style>

    <script type="text/javascript" src="../sources/vis/vis.js"></script>
    <script type="text/javascript">
        var data = null;
        var graph = null;
        var pIdProyecto = null;

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return false;
        }

        pIdProyecto = getQueryVariable('Id');
        //pIdProyecto = "39353";

        function custom(x, y) {
            return (-Math.sin(x / Math.PI) * Math.cos(y / Math.PI) * 10 + 10) * 1000;
        }

        // Called when the Visualization API is loaded.
        function drawVisualization() {
            var style = 'bar';
            var withValue = ['bar-color', 'bar-size', 'dot-size', 'dot-color'].indexOf(style) != -1;

            // Create and populate a data table.
            dataPuntos = new vis.DataSet();
            var extra_content = [
           'Arbitrary information',
           'You can access data from the point source object',
           'Tooltip example content',
            ];

            var oData;

            $.ajax({
                url: "../FacadeProxyWS.aspx/invokeRiesgo", // listaCriteriosEval ListaTipoDocumentoAprobar
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify({
                    idProyecto: pIdProyecto,
                }),

                success: function (oResult) {
                    oData = JSON.parse(oResult.d);
                    for (var clave in oData) {
                        // Controlando que json realmente tenga esa propiedad
                        if (oData.hasOwnProperty(clave)) {
                            // Mostrando en pantalla la clave junto a su valor
                            var titulo = 'Pregunta :' + oData[clave].Item + oData[clave].pregunta + '<br>' +
                                         'Respuesta: <b>' + oData[clave].respuesta + '</b><br>' +
                                         'Fecha: ' + oData[clave].fecha;
                            dataPuntos.add({ x: 2 * oData[clave].nroPregunta, y: 2 * oData[clave].nroFecha, z: parseInt(oData[clave].valor), style: parseInt(oData[clave].valor), extra: titulo });
                        }
                    }
                    var count = Object.keys(oData).length;
                    if (count > 0) {
                        //dataPuntos.add({ x: oData[count - 1].nroPregunta, y: oData[count - 1].nroFecha, z: 0, style: 0, extra: '' });

                        var options = {
                            width: '900px',
                            height: '600px',
                            style: style,
                            zMin: 0,
                            zMax: 2,
                            zStep: 1,
                            yBarWidth: 1,
                            showPerspective: true,
                            showLegend: false,
                            showGrid: true,
                            showShadow: false,

                            // Option tooltip can be true, false, or a function returning a string with HTML contents
                            tooltip: true,
                            tooltip: function (point) {
                                // parameter point contains properties x, y, z
                                //var resp;
                                //if (point.z == 1)
                                //{ resp = 'SI'; }
                                //else if (point.z == 2) {
                                //    resp = 'NO';
                                //}
                                //else { resp = ''; }
                                return  point.data.extra;
                            },
                            //PREGUNTA
                            xValueLabel: function (value) {
                                let result = '';
                                for (var clave in oData) {
                                    // Controlando que json realmente tenga esa propiedad
                                    if (oData.hasOwnProperty(clave)) {
                                        if (oData[clave].nroPregunta == value / 2) {
                                            result = oData[clave].pregunta;
                                        }
                                    }
                                }
                                return result;
                            },
                            //FECHA
                            yValueLabel: function (value) {
                                let result = '';
                                for (var clave in oData) {
                                    // Controlando que json realmente tenga esa propiedad
                                    if (oData.hasOwnProperty(clave)) {
                                        if (oData[clave].nroFecha == value / 2) {
                                            result = oData[clave].fecha;
                                            //result = result.substring(3, 10);
                                        }
                                    }
                                }
                                return result;
                            },

                            zValueLabel: function (value) {
                                let result = ''
                                if (value == 1) {
                                    result = 'Peligro Bajo';
                                }
                                else if (value == 2) {
                                    result = 'Peligro Alto';
                                }
                                return result;
                            },
                            keepAspectRatio: true,
                            verticalRatio: 0.5
                        };

                        var camera = graph ? graph.getCameraPosition() : null;
                        // create our graph
                        var container = document.getElementById('mygraph');
                        graph = new vis.Graph3d(container, dataPuntos, options);
                        //if (camera) graph.setCameraPosition(camera); // restore camera position
                        graph.setCameraPosition({ horizontal: 1.025, vertical: 1.1150000000000002, distance: 1.7 });
                    }
                }
            });
            // specify options
            //document.getElementById('style').onchange = drawVisualization;
        }
    </script>

</head>
<body onload="drawVisualization()">
    <form id="form1" runat="server">
        <div>
            <div id="mygraph"></div>
            <div id="info"></div>
        </div>
    </form>

     <script src="../sources/jquery/jquery.min.js" type="text/javascript"></script>
</body>
</html>
