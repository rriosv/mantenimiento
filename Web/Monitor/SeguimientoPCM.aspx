﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeguimientoPCM.aspx.cs" Inherits="Web.Monitor.SeguimientoPCM" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../css/Monitoreo.css" rel="stylesheet" type="text/css" />
    <title>Seguimiento PCM</title>
    <style type="text/css">
        body {
            font-family:Calibri;
        }
    </style>
      <asp:PlaceHolder runat="server">
        <%:Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div class="BloqueInfoTotal">
            <asp:UpdatePanel runat="server" ID="Up_PriorizacionPCM" UpdateMode="Conditional">
                <ContentTemplate>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="titulo3" align="left" style="padding-left: 30px">PRIORIZACIÓN PCM
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2" align="left" style="padding-left: 80px">
                                            <b>I. SEGUIMIENTO </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <img alt="" src="linea.png" width="98%" height="10px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:GridView runat="server" ID="grdSeguimientoPCM" EmptyDataText="No hay registros."
                                                            ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                                            CellPadding="2" CellSpacing="2" DataKeyNames="id_seguimientoPCM"
                                                            Width="800px" OnSelectedIndexChanged="grdSeguimientoPCM_SelectedIndexChanged"
                                                            OnRowCommand="grdSeguimientoPCM_RowCommand">
                                                            <Columns>
                                                                <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit_27.png" ButtonType="Image"
                                                                    SelectText="Editar" HeaderStyle-CssClass="GridHeader2" HeaderText="Edit."></asp:CommandField>

                                                                <asp:TemplateField HeaderText="Etapa Compromiso" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEtapaCompromiso" Text='<%# Eval("etapaCompromiso") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                        <asp:Label ID="lblIdEtapaCompromiso" Text='<%# Eval("idEtapaCompromiso") %>' runat="server" CssClass="tablaRegistro"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mes Término Compromiso" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblMes" Text='<%# Eval("mes") %>' runat="server"></asp:Label>
                                                                        <asp:Label ID="lblIdMes" Text='<%# Eval("idMesTermino") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" Width="100px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Año Término del Compromiso">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAnio" Text='<%# Eval("anio") %>' CssClass="tablaGrilla"
                                                                            runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Grado Cumplimiento" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGradoCumplimiento" Text='<%# Eval("gradoCumplimiento") %>' CssClass="tablaGrilla" runat="server"></asp:Label>
                                                                        <asp:Label ID="lblIdGradoCumplimiento" Text='<%# Eval("idGradoCumplimiento") %>' CssClass="tablaGrilla" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cierra Brecha" Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCierreBrecha" Text='<%# Eval("cierreBrecha") %>' runat="server"></asp:Label>
                                                                        <asp:Label ID="lblIdCierreBrecha" Text='<%# Eval("idCierreBrecha") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Fecha Estimada Término">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFechaTermino" Text='<%# Eval("fechaEstimadaTermino") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Elim." Visible="true">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/img/del.gif" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');"
                                                                            CommandName="eliminar" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdSeguimientoPCM" Text='<%# Eval("id_seguimientoPCM") %>' runat="server"></asp:Label>
                                                                        <asp:Label ID="lblId_Usuario" Text='<%# Eval("usuario") %>' runat="server"></asp:Label>
                                                                        <asp:Label ID="lblFecha_Update" Text='<%# Eval("fecha_update") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="GridHeader2" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle Height="15px" />
                                                            <EditRowStyle BackColor="#FFFFB7" />
                                                        </asp:GridView>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ImageButton ID="imgbtnAgregarSeguimientoPCM" runat="server" ImageUrl="~/img/add.png"
                                                            onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                            Height="30px" AlternateText="Agregar" ToolTip="Agregar Registro." OnClick="imgbtnAgregarSeguimientoPCM_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <asp:Panel ID="Panel_AgregarSeguimientoPCM" Visible="false" runat="server" Width="100%">
                                                            <div style="width: 100%; padding: 5px 10px 5px 10px; margin-top: 10px" class="CuadrosEmergentes">
                                                                <table class="tablaRegistro" cellpadding="2" width="94%">
                                                                    <tr>
                                                                        <td colspan="4" align="center">
                                                                            <b>REGISTRO DE SEGUIMIENTO</b>
                                                                            <asp:Label ID="lblIdSeguimientoPCM" runat="server" Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="height: 10px"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">Etapa Compromiso :
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="ddlEtapaCompromisoPCM" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">Mes Termino Compromiso :
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="ddlMesTerminoPCM" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">Año Termino Compromiso :
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="ddlAnioTerminoPCM" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">Grado Cumplimiento :
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="ddlGradoCumplimientoPCM" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">Cierra Brecha :
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="ddlCierraBrecha" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">Fecha Estimado Termino :
                                                                        </td>
                                                                        <td align="left" colspan="3">
                                                                            <asp:TextBox runat="server" ID="txtFechaTerminoPCM" placeholder="dd/mm/yyyy"></asp:TextBox>
                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender99" runat="server" TargetControlID="txtFechaTerminoPCM" FilterType="Custom, Numbers" ValidChars="/" Enabled="True" />
                                                                            <asp:CalendarExtender ID="CalendarExtender9" runat="server" TargetControlID="txtFechaTerminoPCM" Format="dd/MM/yyyy"></asp:CalendarExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="left">
                                                                            <asp:Label ID="lblNomUsuarioSeguimientoPCM" runat="server" Font-Size="10px"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" colspan="4">
                                                                            <asp:Button ID="btnGuardarSeguimientoPCM" runat="server" Text="Guardar" OnClick="btnGuardarSeguimientoPCM_Click" />
                                                                            <asp:Button ID="btnModificarSeguimientoPCM" runat="server" Text="Modificar" Visible="false" OnClick="btnModificarSeguimientoPCM_Click" />
                                                                            <asp:Button ID="btnCancelaSeguimientoPCM" runat="server" OnClick="btnCancelaSeguimientoPCM_Click" Text="Cancelar" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
