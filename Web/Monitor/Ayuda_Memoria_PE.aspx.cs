﻿using Entity;
using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;

namespace Web.Monitor
{
    public partial class Ayuda_Memoria_PE : System.Web.UI.Page
    {
        BL_MON_Liquidacion _objBLLiquidacion = new BL_MON_Liquidacion();
        BE_MON_Liquidacion _BELiquidacion = new BE_MON_Liquidacion();
             
        BL_MON_Ejecucion _objBLEjecucion = new BL_MON_Ejecucion();
        BE_MON_Ejecucion _BEEjecucion = new BE_MON_Ejecucion();
    
        BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BEBandeja = new BE_MON_BANDEJA();

        DataTable dt = new DataTable();

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string pGetIdProyecto = (Request.QueryString["id"].ToString());
            string pGetIdRevision = (Request.QueryString["idtipo"].ToString());

            if (!IsPostBack)
            {
                if (pGetIdProyecto.Length > 0 && pGetIdRevision.Length > 0)
                {

                    lblFechaAyuda.Text = "(" + DateTime.Now.ToString("dd.MM.yyyy") + ")";

                    if (Request.Browser.IsMobileDevice && imgbtnExportar.Visible == true)
                    {
                        imgbtnImprimir.Visible = false;
                        imgbtnExportar.Visible = false;
                        imgbtnExportarMobile.Visible = true;
                    }

                    CargaInformacionGeneral();
                }
                else
                {
                    lblTitle.Text = "NOTA: NO EXISTE INFORMACIÓN PARA MOSTRAR.";

                    string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                }
                }

            try
            {
                string t = Request.QueryString["t"].ToString();

                if (t == "p")
                {
                    imgbtnImprimir.Visible = false;
                    imgbtnExportar.Visible = false;
                }
            }
            catch
            { }

         


        }

        protected void CargaInformacionGeneral()
        {
            _BELiquidacion.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
            //_BELiquidacion.tipoFinanciamiento = 3;
            _BELiquidacion.tipoFinanciamiento = Convert.ToInt32(Request.QueryString["idtipo"].ToString());


            dt = _objBLLiquidacion.spMON_Ayuda_MemoriaPE(_BELiquidacion);

            if (dt.Rows.Count > 0)
            {
                //lblSnip.Text = dt.Rows[0]["SNIP"].ToString();
                lblProyecto.Text = dt.Rows[0]["NOMBRE_PROYECTO"].ToString();
                lblMontoMVCS.Text = dt.Rows[0]["MONTO_MVCS"].ToString();
                lblMontoMVCS.Text = dt.Rows[0]["MONEDA_MVCS"].ToString() + " " + Convert.ToDouble(lblMontoMVCS.Text).ToString("N2");
                lblMontoJica.Text = dt.Rows[0]["MONTO_JICA"].ToString();
                lblMontoJica.Text = dt.Rows[0]["MONEDA_JICA"].ToString() + " " + Convert.ToDouble(lblMontoJica.Text).ToString("N2");

                lblMontoInversionTotal.Text = (Convert.ToDouble(dt.Rows[0]["MONTO_MVCS"].ToString()) + Convert.ToDouble(dt.Rows[0]["MONTO_JICA"].ToString())).ToString("N2");
                //lblMontoConvenio.Text = dt.Rows[0]["MONTO_CONVENIO"].ToString();
                lblBeneficiario.Text = dt.Rows[0]["BENEFICIARIOS"].ToString();
                lblEjecutora.Text = dt.Rows[0]["UNIDAD_EJECUTORA"].ToString();
                lblDepartamento.Text = dt.Rows[0]["DEPARTAMENTO"].ToString();
                lblProvincia.Text = dt.Rows[0]["PROVINCIA"].ToString();
                lblDistrito.Text = dt.Rows[0]["DISTRITO"].ToString();
                lblLocalidad.Text = dt.Rows[0]["CCPP"].ToString();

                lblFechaInicio1.Text = dt.Rows[0]["FECHAINICIO"].ToString();
                lblFechaTerminoContractual.Text = dt.Rows[0]["FechaTerminoContractual"].ToString();
                lblFechaTerminoReal.Text = dt.Rows[0]["FECHA_FIN"].ToString();
                lblPlazoEjecucion.Text = dt.Rows[0]["PLAZOEJECUCION"].ToString();

                //lblEstadoSituacional.Text = dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();

                lblContratista.Text = dt.Rows[0]["CONTRATISTA"].ToString();
                lblMontoContratado.Text = dt.Rows[0]["MONTOCONSORCIO"].ToString();
                lblMontoContratado.Text = dt.Rows[0]["MONEDA_MVCS"].ToString() + " " + Convert.ToDouble(lblMontoContratado.Text).ToString("N");

                lblEmpresaSupervisora.Text = dt.Rows[0]["EmpresaSupervisor"].ToString();
                if (dt.Rows[0]["ComentarioSupervisor"].ToString() != "")
                {
                    trComentarioSupervisor.Visible = true;
                    lblComentarioSupervisor.Text = dt.Rows[0]["ComentarioSupervisor"].ToString();
                }

                lblMontoSupervision.Text = dt.Rows[0]["MontoSupervisor"].ToString();
                lblMontoSupervision.Text = Convert.ToDouble(lblMontoSupervision.Text).ToString("N");

                lblFechaEntregaTerreno.Text = dt.Rows[0]["FECHA_ENTREGA_TERRENO"].ToString();

                lblFechaRecepcionObra.Text = dt.Rows[0]["FechaRecepcion"].ToString();
                lblFechaCumplimientoObra.Text = dt.Rows[0]["fechaCertificadoCumplimiento"].ToString();
                lblUnidadReceptora.Text = dt.Rows[0]["UnidadReceptora"].ToString();
                lblFechaActaFinal.Text = dt.Rows[0]["fechaFinalEntregaObraOperacion"].ToString();

                lblLiquidacionContratoObra.Text = dt.Rows[0]["resolucion"].ToString();
                lblLiquidacionContratatoSupervision.Text = dt.Rows[0]["resolucionSupervision"].ToString();

                lblEstado.Text = dt.Rows[0]["ESTADO_SITUACIONAL"].ToString();

                if (lblEstado.Text == "Liquidado" && lblLiquidacionContratatoSupervision.Text == "")
                {
                    lblEstado.Text = "Liquidado - Obra <br /> Pendiente: Liquidación - Supervisión";
                }

                //Carga Foto
                _BEBandeja.id_proyecto = Convert.ToInt32(Request.QueryString["id"].ToString());
                List<BE_MON_BANDEJA> ListFoto = new List<BE_MON_BANDEJA>();
                ListFoto = _objBLBandeja.F_spMON_FotoDocumentoMonitor(_BEBandeja);

                int i = 0;
                foreach (BE_MON_BANDEJA item in ListFoto)
                {
                    string extension = item.UrlDoc.Substring(item.UrlDoc.Length - 3, 3);

                    if (extension.ToUpper() == "JPG" || extension.ToUpper() == "PNG")
                    {
                        if (i == 0)
                        {
                            string url = item.UrlDoc.ToString();
                            imgProyecto.ImageUrl = "FotoPMIB.ashx?img=" + url;
                            imgProyecto.ToolTip = item.nombre;
                            imgProyecto.Visible = true;
                        }


                        if (i == 1)
                        {
                            string url = item.UrlDoc.ToString();
                            imgProyecto2.ImageUrl = "FotoPMIB.ashx?img=" + url;
                            imgProyecto2.ToolTip = item.nombre;
                            imgProyecto2.Visible = true;
                            i = i + 1;

                            break;
                        }
                        i = i + 1;

                    }

                }

                if (i == 1)
                {
                    imgProyecto.Width = 500;
                }
            }
            else
            {
                lblTitle.Text = "NOTA: NO EXISTE INFORMACIÓN PARA MOSTRAR.";

                string script = "<script>(document.getElementById('idDivInformacion')).style.display='none';</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
        }

        protected void imgbtnImprimir_Click(object sender, ImageClickEventArgs e)
        {
            BLUtil _obj = new BLUtil();
            string strnom = "AyudaMemoria_SNIP_PE(" + DateTime.Now.ToString("yyyyMMdd") + ").pdf";

            byte[] fileContent = _obj.GeneratePDFFile(strnom);
            if (fileContent != null)
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strnom);
                Response.AddHeader("content-length", fileContent.Length.ToString());
                Response.BinaryWrite(fileContent);
                Response.End();
            }
        }
    }
}