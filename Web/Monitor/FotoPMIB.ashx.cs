﻿using System.Web;
using System.Configuration;
namespace Web.Monitor
{
    /// <summary>
    /// Descripción breve de FotoPMIB
    /// </summary>
    public class FotoPMIB : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            

            string Path = ConfigurationManager.AppSettings["DocumentosMonitor"];

            string img = context.Request.QueryString["img"];

            context.Response.ContentType = "image/png";

            context.Response.TransmitFile(Path + img);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}