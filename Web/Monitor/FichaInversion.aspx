﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FichaInversion.aspx.cs" Inherits="Web.Monitor.FichaInversion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .celdaTabla{text-align:left;border:1px dotted gray}
        .celdaTabla2{text-align:right;}
        .center {
            margin:auto;
        }
    </style>
    <script type="text/javascript">

        function imprimir(nombre) {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <center>
                <div id="div_Ficha" style="width: 760px; height: auto;">
                    <center>
                        <%--  <div id="div_imagen">
                    <img src="../img/LogoMVCS1-estado.png" height="55"  alt="" />
                </div>--%>

                        <%--<div id="title" style="display: none">
                    <center><span style="font-family: 'Trebuchet MS'; font-size: 9pt"><b>MINISTERIO DE VIVIENDA, CONSTRUCCI&Oacute;N Y SANEAMIENTO</b></span></center>
                </div>--%>
                        <%--      <table style="margin:auto;margin-top:5px;margin-bottom:6px;" cellpadding="0" cellspacing="0">
                 <tr>
                     <td rowspan="2" >
                         <img src="../img/LogoMVCS1-estado.png" height="50"  alt="" style="padding-right:15px;padding-left:15px;" />
                     </td>
                  
                 </tr>
                
             </table>--%>

                        <span style="font-family: Arial; font-size: 16pt">
                            <b>
                                <asp:Label runat="server" Font-Size="16pt" Text="FICHA RESUMEN DE SEGUIMIENTO DE LA INVERSIÓN" ></asp:Label>
                            </b>
                        </span>
                        <br />
                        <asp:Label runat="server" Font-Size="9pt" Text="" ID="lblFechaAyuda" Visible="false"></asp:Label>
                        <div id="icon_word">
                            <center>
                                <table>
                                    <tr>
                                        <td>
                                            <%--<asp:ImageButton ID="imgbtnExportar" runat="server" ImageUrl="~/img/pdf_48x48.png" Width="25px" Height="25px" OnClick="imgbtnImprimir_Click" ToolTip="Exportar a PDF." Visible="false" />--%>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="imgbtnImprimir" runat="server" ImageUrl="~/img/print.png" Width="25px" Height="25px" OnClientClick="javascript:imprimir('div_Ficha')" />--%>

                                        </td>
                                    </tr>
                                </table>
                            </center>

                        </div>
                        <%--<span style="font-family:'Calibri';font-size:11pt"><b><asp:Label ID="lblComponente1" runat="server" Text=""></asp:Label>, <asp:Label ID="lblDistrito1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblProvincia1" runat="server" Text=""></asp:Label> - <asp:Label ID="lblDepartamento1" runat="server" Text=""></asp:Label></b></span>--%>
                    </center>

                    <%-- <div style="width:100%;text-align:left">
            <span style="font-family:'Calibri';font-size:11pt"><b>DATOS GENERALES:</b></span>
        </div>--%>

                    <div style="width: 100%">


                        <table cellspacing="0" style="font-family: Calibri; font-size: 13px; width: 100%" cellpadding="5">
                            <tr>
                                <td colspan="2" style="text-align: left; font-weight: bold">I. DATOS GENERALES</td>
                            </tr>

                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray; width: 148px;">CÓDIGO ÚNICO DE INVERSIONES:</td>
                                <td class="celdaTabla" style="border: 1px dotted gray;"><b>
                                    <asp:Label ID="lblCodUnificado" runat="server" Text=""></asp:Label></b></td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray">NOMBRE DE LA INVESIÓN</td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblNombreInversion" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray;">BENEFICIARIOS
                                </td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblBeneficiarios" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray">UNIDAD EJECUTORA DE INVERSIONES/ PROGRAMA:</td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblUnidadEjecutora" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray">MODALIDAD DE EJECUCIÓN</td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblModalidadEjecucion" runat="server" Text=""></asp:Label></td>
                            </tr>
                             <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray">ESTADO</td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblEstado" runat="server" Text=""></asp:Label></td>
                            </tr>
                           
                            <tr>
                                <td colspan="2" style="text-align: left; font-weight: bold">II. EJECUCIÓN FINANCIERA S/</td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray">FUENTE DE FINANCIAMIENTO</td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    RDR (S/)&nbsp;&nbsp;<asp:Label ID="lblMontoRDR" runat="server" Text=""></asp:Label><br />
                                    ROOC ** &nbsp;&nbsp; 0.0
                                </td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray" colspan="2">
                                    <table cellspacing="0" cellpadding="2" style="text-align:center">
                                        <tr>
                                            <th style="border: 1px dotted gray">COSTO DE INVERSIÓN <br /> S/</th>
                                            <th style="border: 1px dotted gray">EJEC ACUM AL 31.12.2018</th>
                                            <th style="border: 1px dotted gray">PIM 2019 <br /> S/</th>
                                            <th style="border: 1px dotted gray">DEVENGADO 2019 <br /> S/</th>
                                            <th style="border: 1px dotted gray">AVANCE PIM 2019 %</th>
                                            <th style="border: 1px dotted gray">AVANCE TOTAL %</th>
                                            <th style="border: 1px dotted gray">PROYECCIÓN AL CIERRE 2019 <br /> S/</th>
                                            <th style="border: 1px dotted gray">SALDO DE INVERSIÓN 2019 <br /> S/</th>
                                        </tr>
                                        <tr>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblCostoInversion" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblFinanAcumulada2018" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblPIM2019" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblDevengado2019" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblAvancePMI2019" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblAvanceTotalFinan" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblProyeccionCierre2019" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblSaldoInversion" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2" style="text-align: left; font-weight: bold">III. EJECUCIÓN FÍSICA (%)</td>
                             </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray" colspan="2">
                                    <table cellspacing="0" cellpadding="2" style="text-align:center">

                                        <tr>
                                            <th rowspan="2"  style="border: 1px dotted gray">FECHA DE INICIO</th>
                                            <th rowspan="2"  style="border: 1px dotted gray">FECHA DE TERMINO CONTRACTUAL</th>
                                            <th colspan="4" style="border: 1px dotted gray">AVANCE FÍSICO (%)</th>
                                            <th rowspan="2" style="border: 1px dotted gray">ESTADO DE AVANCE ***</th>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px dotted gray">ACUM. A DIC 2018</th>
                                            <th style="border: 1px dotted gray">EJECUTADO 2019</th>
                                            <th style="border: 1px dotted gray">TOTAL ACUMULADO 2019</th>
                                            <th style="border: 1px dotted gray">PROGRAMADO AL CIERRE 2019</th>

                                        </tr>
                                        <tr>
                                            <td  style="border: 1px dotted gray">
                                                <asp:Label ID="lblFechaInicio" runat="server" Text=""></asp:Label></td>
                                            <td  style="border: 1px dotted gray">
                                                <asp:Label ID="lblFechaCulminacion" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblEjecAcum2018" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblEjec2019" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblEjecTotal" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblEjecProg2019" runat="server" Text=""></asp:Label></td>
                                            <td style="border: 1px dotted gray">
                                                <asp:Label ID="lblEstadoAvance" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                              <tr>
                                <td colspan="2" style="text-align: left; font-weight: bold">IV. ESTADO SITUACIONAL / PROBLEMÁTICA</td>
                             </tr>
                             <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray">DESCRIPCIÓN</td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblDescripcion" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray;">ACCIONES A IMPLEMENTAR
                                </td>
                                <td class="celdaTabla" style="border: 1px dotted gray">
                                    <asp:Label ID="lblAcciones" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="celdaTabla" style="border: 1px dotted gray; font-size:8px" colspan="2">
                                    * ET/DE en actuaciones preparatorias, ET/DE en procedimiento de selección, ET/DE en elaboración, Inversión en actuaciones preparatorias,
                                    Inversión en procedimiento de selección, Inversión en ejecución, Ejecución de saldo de inversión, Inversión culminada, o Recepción de la Inversión.<br />
                                    ** Bonos Soberanos, Endeudamiento Externo<br />
                                    *** Adelantada, Normal, Atrasada<br />
                                    **** Modificaciones en calendario de procedimiento de selección, Atrasos y/o paralizaciones, Resolución de contrato, Resolución de convenio, Falta de 
                                    disponibilidad de terreno, Permisos y licencias, Otras autorizaciones, Interferencias, Arbitraje, Falta de recursos financieros, Supervisor/Inspector, 
                                    Riesgo no identificado, Discrepancias (Valoraciones observadas, etc.). Recepción observada u Otro.
                                </td>
                            </tr>
                       
                        </table>
                    </div>
                    <br />
                    <br />
                </div>

            </center>
        </div>
    </form>
</body>
</html>
