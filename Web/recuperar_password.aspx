﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="recuperar_password.aspx.cs" Inherits="Web.recuperar_password" %>

<!DOCTYPE html>
<html lang="es-pe">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="theme-color" content="#343a40" />
    
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--Import Font and Icons-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link media="screen" rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Compiled and minified CSS -->
    <link media="screen" rel="stylesheet" href="sources/bootstrap-4.3.1/css/bootstrap.min.css">
    <!-- Compiled and minified JavaScript -->
    <link media="screen" rel="stylesheet" href="sources/waves/waves.min.css">

    <style>
        .content-wrapper {
            min-height: 800px;
            height: calc(100vh - 70px);
            overflow: auto;
        }

      /*  html {
            background: url('images/1.jpg') no-repeat 50% fixed;
            background-size: cover;
            background-position: center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            height: 100%;
            overflow: hidden;
        }*/

        html {
            width: 100%;
            height: 100%;
            position: absolute;
            /*padding: 90px 70px 50px 70px;*/
            background: #c8c8c8;
            /*background: rgba(40,57,101,.9);*/
        }

         body {
            margin: 0;
            color: #6a6f8c;
            background: #c8c8c8;
            font: 600 16px/18px 'Open Sans',sans-serif;
        }

        main {
            flex: 1;
        }


        .bg-dark-trans {
            background-color: rgba(52, 58, 64, .8) !important;
        }

        .bg-secondary-trans {
            background-color: rgba(108, 117, 125, .9) !important;
        }

        footer p {
            font-size: .88em;
        }

        .bg-infoText {
            background: rgba(40,57,101,.9);
        }
    </style>

    <title>Sistema de Seguimiento de Proyectos</title>
</head>
<body>
   <header>
       <%-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark-trans">
            <div class="container-fluid">
                <a class="navbar-brand" href="login.aspx">
                    <img src="img/logo_ssp.png" alt="Logo SSP" />
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsForm"
                    aria-controls="navbarsForm" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsForm">
                    <div class="mr-auto">
                    </div>
                    <form class="form-inline my-2 my-lg-0 d-flex justify-content-center">
                        <a class="btn btn-danger my-2 ml-1 mr-2" href="<%= ConfigurationSettings.AppSettings["UrlFrontEnd"]%>publica/index"><i class="fas fa-search fa-fw"></i>CONSULTA PÚBLICA</a>
                        <a class="btn btn-primary my-2 ml-1 mr-2" href="login.aspx">IR A LOGIN 
                        </a>
                    </form>
                </div>
            </div>
        </nav>--%>
    </header>

    <div class="content-wrapper d-flex flex-column justify-content-end">
        <main role="main" class="d-flex  flex-column align-items-center justify-content-center">
            <div class="card-deck">
                <div class="card">
                    <div class="card-header text-left bg-infoText text-light">
                        <h5 class="my-0 font-weight-normal">
                            <i class="fas fa-key circle"></i>
                            Recuperar Contraseña
                        </h5>
                    </div>
                    <form runat="server">
                         <div class="card-body bg-light">
                            <div class="container">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-user"></i>
                                                </div>
                                            </div>
                                            <asp:TextBox ID="txtUsuario" autofocus runat="server" MaxLength="20" CssClass="form-control" placeHolder="Usuario (Ejemplo: jperez)" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">@</div>
                                            </div>
                                            <asp:TextBox ID="correo" runat="server" MaxLength="50" CssClass="form-control" placeHolder="Correo Institucional (Ejemplo: jperez@empresa.gob.pe)" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <span class="align-middle">Código:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="codigo" runat="server" ReadOnly="true" BackColor="info" CssClass="form-control" Font-Bold="true" />
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtCodigoSeguridad" runat="server" MaxLength="5" CssClass="form-control" placeHolder="Escriba el código" />
                                    </div>
                                </div>

                                <div id="validators" class="hidden">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsuario"
                                        EnableViewState="False" ErrorMessage="Ingrese su usuario">*</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="correo"
                                        EnableViewState="False" ErrorMessage="Ingrese su correo">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="correo"
                                        ErrorMessage="Formato de correo no válido" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">**</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCodigoSeguridad"
                                        EnableViewState="False" ErrorMessage="No ha ingresado el código de seguridad">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCodigoSeguridad" ForeColor="red"
                                        EnableViewState="False" ControlToCompare="codigo" ErrorMessage="Código de seguridad incorrecto.">*</asp:CompareValidator>
                                </div>
                                <div class="row">
                                    <asp:Label ID="lblmsg" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary1" CssClass="alert alert-danger" runat="server" Width="100%" EnableViewState="False" ShowMessageBox="false" />
                                </div>

                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a class="btn btn-info" href="login">IR A LOGIN 
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <asp:LinkButton
                                        ID="btnAceptar"
                                        runat="server"
                                        CssClass="btn btn-primary "
                                        OnClientClick="procesarAceptar(this)"
                                        OnClick="btnAceptar_Click">
                                ACEPTAR
                                    </asp:LinkButton>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </main>
        <footer>
            <%--<div class="bg-dark-trans">
                <div class="container">
                    <div class="row py-2">
                        <div class="col-12 col-lg-2 d-flex flex-column align-items-center align-items-lg-start">
                            <img src="img/mvcs.jpg" height="46" alt="Logo MVCS" />
                        </div>
                        <div class="col-12 col-lg-8 d-flex flex-column align-items-center mt-2 mt-lg-0">
                            <p class="d-block my-auto text-light text-center">
                                Sistema de Seguimiento de Proyectos - SSP<br />
                                <strong>Ministerio de Vivienda, Construcción y Saneamiento</strong> © Todos los derechos reservados 2019
                            </p>
                        </div>

                        <div class="col-12 col-lg-2  d-flex flex-column align-items-center align-items-lg-end mt-2 mt-lg-0">
                            <a rel="noreferrer" href="https://validator.w3.org/check?uri=http://andromeda.vivienda.gob.pe/ssp/login" target="_blank">
                                <img src="images/ssp-w3c.png" alt="ssp-w3c">
                            </a>
                        </div>
                    </div>
                </div>
            </div>--%>
        </footer>
    </div>

    <script src="sources/jquery/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="sources/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="sources/waves/waves.min.js"></script>
    <script type="text/javascript">

        window.onload = function () {
            Waves.attach('.btn');
            Waves.init();
            $('#txtUsuario').focus();
            $("#txtCodigoSeguridad").on('keypress', function (e) {
                if (e.keyCode == 13) {
                    $("#btnAceptar").trigger("click");
                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnAceptar", "", true, "", "", false, true))
                }
            });
        }

        function procesarAceptar(e) {
            var i = $(e).find('.fa-circle-notch');
            setTimeout(() => {
                if ($("#ValidationSummary1").css('display') == 'none') {
                    i.toggleClass('fa-spin');
                    $(e).addClass('disabled')
                }
            }, 100);

        }

    </script>


</body>
</html>


