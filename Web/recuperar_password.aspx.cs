﻿using pry_san;
using seguridad;
using Entity;
using Business;
using System;
using System.Configuration;
using System.Drawing;
using System.Net.Mail;
using System.Web.UI;

namespace Web
{
    public partial class recuperar_password : System.Web.UI.Page
    {
        acceso objAcceso = new acceso();
        mProyectos objMetodoProyectos = new mProyectos();
        BL_Administrador objBLAdmin = new BL_Administrador();
        BE_Administrador _BEAdmin = new BE_Administrador();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                codigo.Text = objAcceso.RandomNumber(5);
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            string urlbase = ConfigurationManager.AppSettings["UrlBase"];

            _BEAdmin.correo = correo.Text;
            _BEAdmin.login = txtUsuario.Text;

            BE_Administrador _BEUsuario = new BE_Administrador();
            _BEUsuario = objBLAdmin.F_spAdm_ValidarUsuario(_BEAdmin);

            if (_BEUsuario.nombre != null)
            {
                string np = objAcceso.GetPassword();

                if (EnviarCorreo(_BEUsuario.nombre, np, _BEUsuario.correo) == true)
                {
                    objMetodoProyectos.ExecuteFromSQL("update T_USUARIO set passw='" + objAcceso.Encriptar(np) + "' where id_usuario='" + _BEUsuario.id_usuario + "'");

                    objMetodoProyectos.ExecuteFromSQL("exec sp_audit 5,6," + _BEUsuario.id_usuario + ",'" + _BEUsuario.nombre + "'");
                    correo.Text = "";
                    txtCodigoSeguridad.Text = "";
                    lblmsg.CssClass = "alert alert-info  mx-auto";
                    lblmsg.Text = "Su contraseña ha sido enviado a su correo electrónico.";
                    btnAceptar.CssClass += " disabled";
                }
                else
                {
                    lblmsg.CssClass = "alert alert-danger  mx-auto";
                    lblmsg.Text = "No se ha podido enviar su contraseña.";
                }
            }
            else
            {
                lblmsg.CssClass = "alert alert-danger  mx-auto";
                lblmsg.Text = "No se encontró usuario con los datos ingresados, verifique su Usuario y Correo Institucional ingresados.";

            }


        }

        public bool EnviarCorreo(string pNombre, string pClave, string pTO)
        {
            bool result = true;

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                string cuerpo = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
                              "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
                              "<body style=\"font-family: Tahoma;font-size: 0.85em;line-height: 20px;\">" +
                              "<table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"5\" cellpadding=\"5\" style=\"border:#2c5869 1px solid\">" +
                                "<tr><td><img src=\"https://drive.google.com/a/vivienda.gob.pe/uc?authuser=1&id=1YpDV4UOtgckBCqPgo8GqvevrrXPbYeGt&export=download\" /></td></tr>" +
                                "<tr><td style=\"font-size:medium;font-weight:bold;text-align:center\">SISTEMA DE SEGUIMIENTO DE PROYECTOS</td></tr>" +
                                "<tr><td>Estimado(a) <b>" + pNombre + "</b>, su nueva contrase&ntilde;a ha sido generada (respetar minúsculas y/o mayúsculas):</td></tr>" +
                                "<tr><td>Contrase&ntilde;a: <b>" + pClave + "</b></td></tr>" +
                                "<tr><td><div align=\"justify\">Por su seguridad le recomendamos cambiar su contrase&ntilde;a frecuentemente.</div></td></tr>" +
                                "<tr><td>Por favor no responder a este correo, por ser solo informativo. </td></tr>" +
                                "<tr><td><div id=\"div1\" style=\"font-size:0.8em;text-align: center\">Copyright &#169; 2013 - 2018 Ministerio de Vivienda, Construccion y Saneamiento. All Rights Reserved.</div></td></tr>" +
                              "</table>" +
                              "</body>" +
                              "</html>";
                /************************/
                mail.Subject = "RECUPERACIÓN DE CONTRASEÑA";
                mail.To.Add(pTO);
                mail.Body = cuerpo;
                mail.IsBodyHtml = true;
                smtp.Send(mail);

            }
            catch
            {
                result = false;
            }

            return result;
        }
    }
}