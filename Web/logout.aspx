﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="logout.aspx.cs" Inherits="Web.logout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Acceso</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />

    <script src="js/global.js" type="text/javascript"></script>

</head>
<body leftmargin="0px" rightmargin="0px" topmargin="0px">
    <form id="form1" runat="server">
        <table align="center" width="100%" border="0" cellspacing="0" cellpadding="1px">
            <tr>
                <td valign="top">
                    <asp:Panel Width="99.7%" ID="Panel2" runat="server" CssClass="Grid2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr style="background-color: #b9c8e3">
                                <td valign="middle" style="padding-left: 1px;" colspan="2">
                                    <br />
                                    <table width="100%">
                                        <tr style="color: white; vertical-align: middle;">
                                            <td width="50%" style="vertical-align: top">
                                                <asp:Image ID="Image2" runat="server" ImageUrl="img/logo_ministerio_peq.jpg" /></td>  
                                            <td style="vertical-align: top" width="50%">
                    <asp:Label ID="lblHora" runat="server" Font-Names="arial" ForeColor="White" Font-Bold="True" Font-Size="9pt" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                             <tr style="background-color: #b9c8e3">
                <td colspan="2" style="padding-left: 1px;" valign="middle">
                <asp:Image ID="Image1" runat="server" ImageUrl="img/seg_pry7.jpg" /></td>
            </tr>
          <tr style="background-color:#B9C8E3">
            <td colspan="3">
              <asp:Panel Width="100%" ID="Panel3" runat="server" >
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left">
                     
                    </td>
                    <td></td>
                  </tr>
                </table>
              </asp:Panel>
            </td>
          </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
    <td valign="top" style="height: 100px">
     
    </td>
  </tr>
        </table>
        <table border="0" align="center" style="width: 370px">
            <tr>
                <td height="20" colspan="3">
                    </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Panel ID="Panel1" runat="server" BorderColor="White" BorderWidth="2px" DefaultButton="btnIng"
                        >
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    </td>
                            </tr>
                            <tr>
                                <td align="left">
                            <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red" >Su sesión ha expirado, por favor vuelva a ingresar.</asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="color:#6082bf;">
                                    </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button CssClass="boton" ID="btnIng" runat="server" Text="Aceptar" OnClick="btnIngresar" /></td>
                            </tr>
                            <tr>
                                <td align="left">
                            </td>
                            </tr>
                        </table>
                        <center>
                            &nbsp;</center>
                    </asp:Panel>
                    </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
    </form>
</body>
</html>
