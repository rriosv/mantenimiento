﻿(function ($) {
    $.fn.TextCounter = function (max) {
        return this.each(function () {
            $(this)
                .after("<span class='counter'></span>")
	            .keyup(function (e) {
	                var current = $(this).val().length;
	                $(this).removeClass("counterAlert");
	                if (current >= max) {
	                    if (e.which != 0 && e.which != 8) {
	                        $(this).addClass("counterAlert");
	                        var cadena = $(this).val();
	                        cadena = cadena.substring(0, max);
	                        $(this).val(cadena);
	                        current = $(this).val().length;
	                    }
	                }
	                $(this).next().show().text('Caracteres:'+ String(current) + '(Máx. ' + String(max) + ')');
	            });
        });
    }
})(jQuery);