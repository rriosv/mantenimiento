﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Entity;
using System.Collections.Generic;
using System.Linq;
//using ssp.services;
//using ssp.services.webMEF;
using System.Text;
using System.Drawing;

namespace Web.Solicitudes
{
    public partial class ConsultaOld : System.Web.UI.Page
    {
        BLUtil _objBLUtil = new BLUtil();
        BEProyecto _BEProyecto = new BEProyecto();
        BLProyecto _objProyecto = new BLProyecto();
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();

        DataTable SeguimientoPry()
        {
            double totalsnip = 0;
            _BEProyecto.Id_Documentacio = Convert.ToInt32(ddlbusq_documentacion.SelectedValue);
            //_BEProyecto.IdEstado_Snip = Convert.ToInt32(ddlbusq_snip.SelectedValue);
            _BEProyecto.IdEstado_Snip = 0;
            _BEProyecto.Criterio = cboSearch.SelectedValue;
            _BEProyecto.Buscar = txtSearch.Text;
            _BEProyecto.Cod_subsector = Convert.ToInt32(ddlbusq_subsector.SelectedValue);
            _BEProyecto.Cod_depa = ddlbusqdepa.SelectedValue.ToString();

            _BEProyecto.Cod_prov = ddlbusqprov.SelectedValue.ToString();
            _BEProyecto.Cod_dist = ddlbusqdist.SelectedValue.ToString();
            _BEProyecto.id_Proceso = Convert.ToInt32(ddlProceso.SelectedValue);
            if (ddlProceso.SelectedValue == "4")
            {
                _BEProyecto.estado = ddlEstadoProceso.SelectedItem.Text;
            }
            else
            {
                _BEProyecto.estado = ddlEstadoProceso.SelectedValue.ToString();
            }


            if (ddl_estrategia.SelectedValue != "")
            {
                _BEProyecto.IdEstrategia = Convert.ToInt32(ddl_estrategia.SelectedValue.ToString());
            }

            if (ddlModalidadFinanciamiento.SelectedValue == "3")
            {
                _BEProyecto.id_ModalidadFinanciamiento = 3;
                _BEProyecto.id_SubModalidadFinanciamiento = Convert.ToInt32(ddlSubModalidadFinanciamiento.SelectedValue);
            }
            else
            {
                _BEProyecto.id_ModalidadFinanciamiento = Convert.ToInt32(ddlModalidadFinanciamiento.SelectedValue);
                _BEProyecto.id_SubModalidadFinanciamiento = 0;
            }

            if (Session["obj"] == null)
            {
                DataTable DTPrincipal = _objProyecto.spSOL_ConsultaPry(_BEProyecto);

                //DTPrincipal.Columns.Add("Origen", typeof(String));
                //DTPrincipal.Columns.Add("vFechaIngreso", typeof(String));
                //DTPrincipal.Columns.Add("vAdmEdoDesc", typeof(String));
                //DTPrincipal.Columns.Add("vAdmEvalFecha", typeof(String));
                //DTPrincipal.Columns.Add("vEleEdoDesc", typeof(String));
                //DTPrincipal.Columns.Add("vEleEvalFecha", typeof(String));

                DataTable DTPrincipalcopia = DTPrincipal.Clone();

                Session["obj"] = DTPrincipal;
            }

            if (((DataTable)Session["obj"]).Rows.Count > 0)
            {
                btnExcel.Visible = true;
                //lblMarcaPRESET1.Visible = true;
                //lblMarcaPRESET2.Visible = true;
            }
            else
            {
                btnExcel.Visible = false;
                //lblMarcaPRESET1.Visible = false;
                //lblMarcaPRESET2.Visible = false;
            }

            for (int x = 0; x <= ((DataTable)Session["obj"]).Rows.Count - 1; x++)
            {
                totalsnip = totalsnip + Convert.ToDouble(((DataTable)Session["obj"]).Rows[x]["monto_snip"]);
            }
            Session["snip"] = totalsnip;
            return (DataTable)Session["obj"];

        }

        protected void grvproy_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                Label lblFlag = (Label)e.Row.FindControl("lblFlagVirtual");
                Label lblTipoEvaluacion = (Label)e.Row.FindControl("lblvTipoEvaluacion");
                if (lblTipoEvaluacion.Text.Equals("VIRTUAL"))
                {
                    lblFlag.Visible = true;
                }
                //DETALLE
                LinkButton lnkAdmi = (LinkButton)e.Row.FindControl("lnkbtnAdmisibilidad");
                LinkButton lnkEleg = (LinkButton)e.Row.FindControl("lnkbtnElegibilidad");
                LinkButton lnkEvalu = (LinkButton)e.Row.FindControl("lnkbtnEvaluacion");
                LinkButton lnkMonit = (LinkButton)e.Row.FindControl("lnkbtnMonitoreo");

                string vIdProyecto = ((Label)e.Row.FindControl("lblIdProyecto")).Text;
                string vIdSolicitud = ((Label)e.Row.FindControl("lblIdSolicitud")).Text;

                Label lblEstadoAdmisibilidad = (Label)e.Row.FindControl("lblEstadoAdmisibilidad");
                Label lblElegibilidad = (Label)e.Row.FindControl("lblElegibilidad");
                Label lblEstadoCalidad = (Label)e.Row.FindControl("lblEstadoCalidad");
                Label lblEstadoMonitoreo = (Label)e.Row.FindControl("lblEstadoMonitoreo");
                Label lblFechaAdmisibilidad = (Label)e.Row.FindControl("lblFechaAdmisibilidad");
                Label lblFechaElegibilidad = (Label)e.Row.FindControl("lblFechaElegibilidad");

                HtmlGenericControl divMonitoreo = (HtmlGenericControl)e.Row.FindControl("divMonitoreo");
                HtmlGenericControl divCalidad = (HtmlGenericControl)e.Row.FindControl("divCalidad");

                if (lblEstadoAdmisibilidad.Text.Length > 0)
                {
                    lnkAdmi.Visible = true;
                    lblEstadoAdmisibilidad.Visible = true;
                    lblFechaAdmisibilidad.Visible = true;
                }

                if (lblElegibilidad.Text.Length > 0)
                {
                    lnkEleg.Visible = true;
                    lblElegibilidad.Visible = true;
                    lblFechaElegibilidad.Visible = true;
                }

                if (!(vIdProyecto.Equals("0") || vIdProyecto.Equals("")))
                {
                    lnkMonit.Visible = true;
                    divMonitoreo.Visible = true;
                    lblEstadoMonitoreo.Visible = true;
                    //lnkbtnEvaluacion.
                }

                if (!(vIdSolicitud.Equals("0") || vIdSolicitud.Equals("")))
                {
                    string vIdEstado = ((Label)e.Row.FindControl("lblIdEstadoSolicitud")).Text;

                    if (!(vIdEstado.Equals("30")))
                    {
                        lnkEvalu.Visible = true;
                        divCalidad.Visible = true;
                        lblEstadoCalidad.Visible = true;

                        if (vIdEstado.Equals("4") || vIdEstado.Equals("5") || vIdEstado.Equals("6") || vIdEstado.Equals("7") || vIdEstado.Equals("10") || vIdEstado.Equals("11") || vIdEstado.Equals("12"))
                        {
                            Label lblFechaCalidad2 = (Label)e.Row.FindControl("lblFechaCalidad");
                            lblFechaCalidad2.Visible = true;
                        }

                        if (vIdEstado.Equals("1") || vIdEstado.Equals("2") || vIdEstado.Equals("21") || vIdEstado.Equals("3"))
                        {
                            LinkButton lnkbtnMetasCalidadDetalle = (LinkButton)e.Row.FindControl("lnkbtnMetasCalidadDetalle");
                            lnkbtnMetasCalidadDetalle.Visible = false;

                            if (vIdEstado.Equals("1"))
                            {
                                Label lblFechaRegistroSSP = (Label)e.Row.FindControl("lblFechaRegistroSSP");
                                lblFechaRegistroSSP.Visible = true;
                            }
                        }

                    }
                }

                //DEVOLUCION
                Label lblId_TipoDevolucion = (Label)e.Row.FindControl("lblId_TipoDevolucion");
                Label lblFechaCalidad = (Label)e.Row.FindControl("lblFechaCalidad");

                if (!String.IsNullOrEmpty(lblId_TipoDevolucion.Text))
                {
                    int tipo = Convert.ToInt32(lblId_TipoDevolucion.Text);

                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFA6A6");
                    lnkEvalu.Visible = false;
                    divCalidad.Visible = false;
                    lblEstadoCalidad.Visible = false;
                    lblFechaCalidad.Visible = false;

                    LinkButton lnkbtnEstado = (LinkButton)e.Row.FindControl("lnkbtnEstado");
                    lnkbtnEstado.Visible = true;
                    Label lblFechaRegistroDevolucion = (Label)e.Row.FindControl("lblFechaRegistroDevolucion");
                    lblFechaRegistroDevolucion.Visible = true;

                    Label lblFechaRegistroSSP = (Label)e.Row.FindControl("lblFechaRegistroSSP");
                    lblFechaRegistroSSP.Visible = false;
                }

                //EN PROCESO DE CARGA POR PRESET

                Label lblFlagCargaPRESET = (Label)e.Row.FindControl("lblFlagCargaPRESET");
                Label lblFechaActivacionPRESET = (Label)e.Row.FindControl("lblFechaActivacionPRESET");

                if (lblFlagCargaPRESET.Text.Equals("1"))
                {
                    Label lblIngresadoPRESET = (Label)e.Row.FindControl("lblIngresadoPRESET");
                    Label lblIngresadoPRESET2 = (Label)e.Row.FindControl("lblIngresadoPRESET2");
                    lblIngresadoPRESET.Visible = true;
                    lblIngresadoPRESET2.Visible = true;

                    //lnkAdmi.Visible = false;
                    //lblFechaAdmisibilidad.Visible = false;
                    //lblEstadoAdmisibilidad.Visible = false;

                    //lnkEleg.Visible = false;
                    //lblFechaElegibilidad.Visible = false;
                    //lblElegibilidad.Visible = false;

                    //lnkEvalu.Visible = false;
                    //lblFechaElegibilidad.Visible = false;
                    //lblEstadoCalidad.Visible = false;
                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                //Llenar area
                ddlbusq_subsector.DataSource = _objBLUtil.listarCombosSOL_SSP("9", "");
                ddlbusq_subsector.DataTextField = "nom_estado";
                ddlbusq_subsector.DataValueField = "cod_estado";
                ddlbusq_subsector.DataBind();
                //llenar departamento
                ddlbusqdepa.DataSource = _objBLUtil.F_spSOL_Listar_Departamento(1, null, null, null);
                ddlbusqdepa.DataValueField = "codigo";
                ddlbusqdepa.DataTextField = "nombre";
                ddlbusqdepa.DataBind();

                ddl_estrategia.DataSource = _objBLUtil.F_spSOL_Listar_Estrategia();
                ddl_estrategia.DataTextField = "nombre";
                ddl_estrategia.DataValueField = "codigo";
                ddl_estrategia.DataBind();
                ddl_estrategia.SelectedValue = "0";

                ddlModalidadFinanciamiento.DataSource = _objBLUtil.listarCombosSOL_SSP("17", "");
                ddlModalidadFinanciamiento.DataTextField = "nom_estado";
                ddlModalidadFinanciamiento.DataValueField = "cod_estado";
                ddlModalidadFinanciamiento.DataBind();
                ddlModalidadFinanciamiento.Items.Insert(0, new ListItem("- TODOS -", "0"));
            }

        }

        protected void ddlbusqdepa_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlbusqdist.Items.Clear();
            if (ddlbusqdepa.SelectedValue.ToString() != "00")
            {
                tdProvincia.Visible = true;
                tdProvincia2.Visible = true;

                ddlbusqprov.DataSource = _objBLUtil.F_spSOL_Listar_Departamento(2, ddlbusqdepa.SelectedValue.ToString(), null, null);
                ddlbusqprov.DataValueField = "codigo";
                ddlbusqprov.DataTextField = "nombre";
                ddlbusqprov.DataBind();
            }
            else
            {
                tdProvincia.Visible = false;
                tdProvincia2.Visible = false;
                tdDistrito.Visible = false;
                tdDistrito2.Visible = false;

                ddlbusqprov.Items.Clear();
            }
            //Session["obj"] = null;
            //grvproy.DataSource = SeguimientoPry();
            //grvproy.DataBind();
        }

        protected void ddlbusqprov_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlbusqprov.SelectedValue.ToString() != "00")
            {
                tdDistrito.Visible = true;
                tdDistrito2.Visible = true;

                ddlbusqdist.DataSource = _objBLUtil.F_spSOL_Listar_Departamento(3, ddlbusqdepa.SelectedValue.ToString(), ddlbusqprov.SelectedValue.ToString(), null);
                ddlbusqdist.DataValueField = "codigo";
                ddlbusqdist.DataTextField = "nombre";
                ddlbusqdist.DataBind();
            }
            else
            {
                tdDistrito.Visible = false;
                tdDistrito2.Visible = false;
                ddlbusqdist.Items.Clear();
            }
            //Session["obj"] = null;
            //grvproy.DataSource = SeguimientoPry();
            //grvproy.DataBind();
        }

        //protected void ddlbusq_documentacion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Session["obj"] = null;
        //    grvproy.DataSource = SeguimientoPry();
        //    grvproy.DataBind();
        //}

        protected void grvproy_DataBound(object sender, EventArgs e)
        {

            if (lblPaginado2 != null)
            {

                DataTable obj = (DataTable)Session["obj"];
                double totalsnip = (double)Session["snip"];

                int currentPage = grvproy.PageIndex + 1;
                string paginado = obj.Rows.Count.ToString() + " Registro(s) (Página " + currentPage.ToString() +
                    " de " + grvproy.PageCount.ToString() + ")";
                lblPaginado2.Text = paginado;

                //lblPryNoAsignados2.Text = "TOTAL MONTO SNIP (S/.) : " + totalsnip.ToString("N"); ;
            }
        }

        protected void grvproy_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvproy.PageIndex = e.NewPageIndex;
            grvproy.DataSource = SeguimientoPry();
            grvproy.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (cboSearch.SelectedValue == "COD" && txtSearch.Text.Length > 0)
            {
                txtSearch.Text = txtSearch.Text.Replace(" ", "");
                txtSearch.Text = txtSearch.Text.Replace(".", "");
                char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
                string[] words = txtSearch.Text.Split(delimiterChars);

                foreach (string s in words)
                {
                    if (s.Length > 0)
                    {
                        try
                        {
                            Convert.ToInt32(s);
                        }
                        catch (Exception ex)
                        {
                            string script = "<script>alert('Solo debe ingresar números y comas');</script>";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script5", script, false);
                            return;
                        }
                    }
                }
            }
            tblMEF.Visible = false;
            lblMsjMEF.Text = "";
            trGrd.Visible = true;
            grvproy.Visible = true;
            divMEF.Visible = false;

            DataTable dt = new DataTable();

            Session["obj"] = null;
            dt = SeguimientoPry();

            grvproy.DataSource = dt;
            grvproy.DataBind();

            lblMsjGrvProy.Visible = false;

            if (dt.Rows.Count == 0)
            {
                trGrd.Visible = false;

                if (cboSearch.SelectedValue == "COD" && txtSearch.Text.Length > 0 && ValidarEntero(txtSearch.Text))
                {
                    _BEProyecto.Cod_snip = Convert.ToInt32(txtSearch.Text);
                    _BEProyecto.Tipo = 1;
                    DataTable dt3 = new DataTable();
                    dt3 = _objProyecto.spSOL_SNIP_RelacionadosBuscar(_BEProyecto);
                    string msj = null;

                    if (dt3.Rows.Count == 0)
                    {
                        _BEProyecto.Tipo = 2;
                        DataTable dt2 = new DataTable();
                        dt2 = _objProyecto.spSOL_SNIP_RelacionadosBuscar(_BEProyecto);

                        if (dt2.Rows.Count > 0)
                        {
                            lblMsjGrvProy.Text = "El SNIP fue eliminado por el siguiente motivo: " + dt2.Rows[0]["motivo_elim"].ToString();
                            lblMsjGrvProy.Visible = true;
                        }
                        else
                        {
                            //CargaMEF(txtSearch.Text);
                        }
                    }
                    //else
                    //{
                    //    foreach (DataRow dr in dt3.Rows)
                    //    {
                    //        msj = msj + dr["snip"].ToString() + " , ";
                    //    }

                    //    lblMsjGrvProy.Text = "El SNIP buscado es antiguo y esta relacionado con el siguiente(s) SNIP: " + msj;
                    //    lblMsjGrvProy.Visible = true;

                    //    lblMsjGrvProy.ForeColor = System.Drawing.Color.Red;
                    //    lblMsjGrvProy.Text = "<div class='alert-warning'><b>NOTA</b> </br></br>";
                    //    lblMsjGrvProy.Text = lblMsjGrvProy.Text + "El SNIP buscado es antiguo y esta relacionado con el siguiente(s) SNIP: "+ msj +"</div>";
                    //}
                }
            }

            if (cboSearch.SelectedValue == "COD" && txtSearch.Text.Length > 0 && ValidarEntero(txtSearch.Text))
            {
                _BEProyecto.Cod_snip = Convert.ToInt32(txtSearch.Text);
                _BEProyecto.Tipo = 1;
                DataTable dt3 = new DataTable();
                dt3 = _objProyecto.spSOL_SNIP_RelacionadosBuscar(_BEProyecto);
                string msj = null;

                if (dt3.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt3.Rows)
                    {
                        msj = msj + dr["snip"].ToString();
                    }

                    lblMsjGrvProy.Text = "El SNIP buscado es antiguo y esta relacionado con el siguiente(s) SNIP: " + msj;
                    lblMsjGrvProy.Visible = true;

                    lblMsjGrvProy.ForeColor = System.Drawing.Color.Red;
                    lblMsjGrvProy.Text = "<div class='alert-warning'><b>NOTA</b> </br></br>";
                    lblMsjGrvProy.Text = lblMsjGrvProy.Text + "El SNIP buscado es antiguo y esta relacionado con el siguiente(s) SNIP: " + msj + "</div>";
                }
            }




        }


        //protected void CargaMEF(string snip)
        //{
        //    try
        //    {
        //        ConsultaMEF objMef = new ConsultaMEF();

        //        Array DatosProyectos;
        //        DatosProyectos = objMef.ConsultaSNIP(snip);

        //        if (DatosProyectos.Length > 0)
        //        {
        //            divMEF.Visible = true;
        //            tblMEF.Visible = true;

        //            lblMsjMEF.Text = "EL SNIP BUSCADO NO SE ENCUENTRA REGISTRADO EN EL SSP.";
        //            grvproy.Visible = false;

        //            oProyecto proyecto = new oProyecto();
        //            oUbigeo ubigeo = new oUbigeo();
        //            // oMeta meta = new oMeta();

        //            proyecto = (oProyecto)DatosProyectos.GetValue(0);

        //            lblProyectoMef.Text = proyecto.NombreInversion.ToString();
        //            lblSNIPMef.Text = snip;
        //            lblEstadoMef.Text = proyecto.Estado.ToString();
        //            lblMontoSnipMef.Text = proyecto.MontoViable.ToString("N");
        //            lblViabilidadMef.Text = proyecto.Situacion.ToString();

        //            lblFuncionMef.Text = proyecto.Funcion.ToString();
        //            lblProgramaMef.Text = proyecto.Programa.ToString();
        //            lblSubprogramaMef.Text = proyecto.SubPrograma.ToString();

        //            lblSectorMef.Text = "";// proyecto.SectorUf.ToString();
        //            lblPliegoMef.Text = "";// proyecto.PliegoUf.ToString();
        //            lblNomFormuladoraMef.Text = "";// proyecto.Uf.ToString();



        //        }
        //        else
        //        {
        //            lblMsjMEF.Text = "NO SE PUDO ENCONTRAR EL SNIP EN EL SSP Y EN EL BANCO DEL MEF.";
        //            grvproy.Visible = false;

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string mensaje = e.Message;
        //        tblMEF.Visible = false;
        //        lblMsjMEF.Text = "En estos momentos hay problemas de conexión con el MEF, vuelva a intentarlo después.";
        //    }

        //}

        //protected void ddlbusq_subsector_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Session["obj"] = null;
        //    grvproy.DataSource = SeguimientoPry();
        //    grvproy.DataBind();
        //}

        protected void btnReiniciar_Click(object sender, EventArgs e)
        {
            ddlbusq_subsector.SelectedValue = "0";
            ddlbusq_documentacion.SelectedValue = "0";
            cboSearch.SelectedValue = "COD";
            txtSearch.Text = "";
            ddlbusqdepa.SelectedValue = "00";

            tdProvincia.Visible = false;
            tdProvincia2.Visible = false;
            tdDistrito.Visible = false;
            tdDistrito2.Visible = false;

            ddl_estrategia.ClearSelection();
            ddlModalidadFinanciamiento.ClearSelection();
            ddlSubModalidadFinanciamiento.ClearSelection();
            ddlProceso.ClearSelection();

            ddlbusqprov.Items.Clear();
            ddlbusqdist.Items.Clear();
            ddlProceso.SelectedValue = "0";

            tdEstadoProceso2.Visible = false;
            tdEstadoProceso1.Visible = false;

            tdSubModalidadFinanciamiento2.Visible = false;
            tdSubModalidadFinanciamiento1.Visible = false;

            ddlEstadoProceso.Items.Clear();
            Session["obj"] = null;

            //grvproy.DataSource = SeguimientoPry();
            //grvproy.DataBind();
        }

        //protected void ddlbusqdist_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Session["obj"] = null;
        //    grvproy.DataSource = SeguimientoPry();
        //    grvproy.DataBind();
        //}
        //protected void ddlbusq_snip_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Session["obj"] = null;
        //    grvproy.DataSource = SeguimientoPry();
        //    grvproy.DataBind();
        //}

        //protected void ddl_estrategia_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Session["obj"] = null;
        //    grvproy.DataSource = SeguimientoPry();
        //    grvproy.DataBind();
        //}

        //protected void imgbtnRequisito_OnClick(object sender, EventArgs e)
        //{

        //    ImageButton boton;
        //    GridViewRow row;
        //    boton = (ImageButton)sender;
        //    row = (GridViewRow)boton.NamingContainer;

        //    //string id = grvproy.DataKeys[row.RowIndex].Values[0].ToString();
        //    lblIDSolicitudes.Text = grvproy.DataKeys[row.RowIndex].Values[0].ToString();

        //    RadioButton100.Checked = false;
        //    RadioButton101.Checked = false;
        //    RadioButton102.Checked = false;

        //    CargaRequisitoMinimoET();
        //    MPE_Requisitos.Show();

        //    //string script = "<script type='text/javascript'>$(document).ready(function(){HabilitarpopupLlamada();});</script>";
        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //    // Up_Fondo.Update();

        //    Up_bodyRequirimiento.Update();

        //}

        //protected void CargaRequisitoMinimoET()
        //{
        //    DataTable dt;
        //    string id_solicitud = lblIDSolicitudes.Text;

        //    dt = _objProyecto.spSOL_RequisitoMinimoET_Info(Convert.ToInt32(id_solicitud));
        //    if (dt.Rows.Count > 0)
        //    {
        //        lblSNIP.Text = dt.Rows[0]["snip"].ToString();
        //        txtTotalFolios.Text = dt.Rows[0]["totalFolios"].ToString();
        //        txtFecha.Text = dt.Rows[0]["fecha"].ToString();
        //        txtNombreProyecto.Text = dt.Rows[0]["nombre_proyecto"].ToString();
        //        txtMunicipalidad.Text = dt.Rows[0]["municipalidad"].ToString();
        //        txtAlcalde.Text = dt.Rows[0]["alcalde"].ToString();
        //        txtCorreoAlcalde.Text = dt.Rows[0]["correoAlcalde"].ToString();
        //        txtTelefonoAlcalde.Text = dt.Rows[0]["telefonoAlcalde"].ToString();
        //        txtUnidadEjecutora.Text = dt.Rows[0]["UnidadEjecutora"].ToString();
        //        txtResponsable.Text = dt.Rows[0]["responsableDocumentacion"].ToString();
        //        txtTelefonoResponsable.Text = dt.Rows[0]["telefonoResponsable"].ToString();
        //        txtCorreoResponsable.Text = dt.Rows[0]["correoResponsable"].ToString();
        //        txtCostoProyecto.Text = dt.Rows[0]["costoProyecto"].ToString();
        //        txtPoblacion.Text = dt.Rows[0]["poblacion"].ToString();
        //        txtComentario.Text = dt.Rows[0]["comentario"].ToString();
        //        lblUser.Text = dt.Rows[0]["usuario"].ToString() + " - " + dt.Rows[0]["fecha_update"].ToString();

        //        if (IsNull(txtPoblacion.Text) != 0)
        //        {
        //            txtRatio.Text = ((IsNull(txtCostoProyecto.Text) / IsNull(txtPoblacion.Text))).ToString("N");
        //        }
        //        else
        //            txtRatio.Text = "0";

        //    }
        //    else
        //    {
        //        txtTotalFolios.Text = "";
        //        txtFecha.Text = "";
        //        txtNombreProyecto.Text = "";
        //        txtMunicipalidad.Text = "";
        //        txtAlcalde.Text = "";
        //        txtCorreoAlcalde.Text = "";
        //        txtTelefonoAlcalde.Text = "";
        //        txtUnidadEjecutora.Text = "";
        //        txtResponsable.Text = "";
        //        txtTelefonoResponsable.Text = "";
        //        txtCorreoResponsable.Text = "";
        //        txtCostoProyecto.Text = "";
        //        txtPoblacion.Text = "";
        //        txtComentario.Text = "";
        //        txtRatio.Text = "";
        //        lblUser.Text = "";



        //    }

        //    dt = _objProyecto.spSOL_RequisitoMinimoET_CheckList(Convert.ToInt32(id_solicitud));

        //    if (dt.Rows.Count > 0)
        //    {

        //        int i = 0;
        //        foreach (DataRow dtRow in dt.Rows)
        //        {
        //            i = i + 1;

        //            //}
        //            //for (int i = 1; i < 34; i++)
        //            //{
        //            //string IDCampo = dt.Rows[i - 1]["id_campos_fonie"].ToString();

        //            int k = 3 * (i - 1) + 1;
        //            RadioButton rb1 = (RadioButton)FindControl("ctl00$ContentPlaceHolder1$RadioButton" + k.ToString());

        //            RadioButton rb2 = (RadioButton)FindControl("ctl00$ContentPlaceHolder1$RadioButton" + (k + 1).ToString());

        //            RadioButton rb3 = (RadioButton)FindControl("ctl00$ContentPlaceHolder1$RadioButton" + (k + 2).ToString());

        //            rb1.Checked = false;
        //            rb2.Checked = false;
        //            rb3.Checked = false;

        //            if (dt.Rows[i - 1]["valor"].ToString() == "0")
        //            {

        //                rb2.Checked = true;


        //            }
        //            else
        //            {
        //                if (dt.Rows[i - 1]["valor"].ToString() == "1")
        //                {
        //                    rb1.Checked = true;

        //                }
        //                else
        //                {
        //                    if (dt.Rows[i - 1]["valor"].ToString() == "2")
        //                    {
        //                        rb3.Checked = true;
        //                    }
        //                    else
        //                    {
        //                        rb1.Checked = false;
        //                        rb2.Checked = false;
        //                        rb3.Checked = false;

        //                    }
        //                }
        //            }



        //        }


        //    }
        //    else
        //    {
        //        for (int i = 1; i < 35; i++)
        //        {
        //            //string IDCampo = dt.Rows[i - 1]["id_campos_fonie"].ToString();


        //            RadioButton rb1 = (RadioButton)FindControl("ctl00$ContentPlaceHolder1$RadioButton" + i.ToString());

        //            RadioButton rb2 = (RadioButton)FindControl("ctl00$ContentPlaceHolder1$RadioButton" + (i + 1).ToString());

        //            RadioButton rb3 = (RadioButton)FindControl("ctl00$ContentPlaceHolder1$RadioButton" + (i + 2).ToString());

        //            rb1.Checked = false;
        //            rb2.Checked = false;
        //            rb3.Checked = false;
        //        }

        //    }
        //}

        protected double IsNull(string val)
        {
            if (val == "")
            {
                val = "0";
            }

            return Convert.ToDouble(val);

        }

        //protected void btnImprimirRequisitos_OnClick(object sender, EventArgs e)
        //{
        //    int id_solicitud;
        //    id_solicitud = Convert.ToInt32(lblIDSolicitudes.Text);

        //    string script = "<script>window.open('Reporte_RequisitoMinimoET.aspx?id=" + id_solicitud + "','_blank') </script>";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        //}

        protected void lnkbtnEstado_Click(object sender, EventArgs e)
        {
            LinkButton lnkbtn;
            GridViewRow row;
            lnkbtn = (LinkButton)sender;
            row = (GridViewRow)lnkbtn.NamingContainer;

            //string id = grvproy.DataKeys[row.RowIndex].Values[0].ToString();
            string lblIDSolicitudes = grvproy.DataKeys[row.RowIndex].Values[0].ToString();
            string vSNIP = ((Label)row.FindControl("lblsnip")).Text;


            BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();
            BL_MON_BANDEJA _objBLbandeja = new BL_MON_BANDEJA();
            _BE_Bandeja.snip = vSNIP;

            _BE_Bandeja.nombProyecto = "";
            _BE_Bandeja.depa = "";
            _BE_Bandeja.prov = "";
            _BE_Bandeja.dist = "";
            _BE_Bandeja.sector = "";
            _BE_Bandeja.tipoFinanciamientoFiltro = "";
            _BE_Bandeja.Estado = "";
            List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();
            ListBandeja = _objBLbandeja.F_spSOL_BandejaDevolucion(_BE_Bandeja);

            ListBandeja = (from cust in ListBandeja
                           where cust.id_proyecto == Convert.ToInt32(lblIDSolicitudes)
                           select cust).ToList();

            if (ListBandeja.Count > 0)
            {
                lblNombreProyectoDev.Text = ListBandeja.ElementAt(0).nombProyecto;
                lblSNIPDev.Text = ListBandeja.ElementAt(0).snip;
                lblTipoFinanciamientoDev.Text = ListBandeja.ElementAt(0).tipoFinanciamientoFiltro;
                lblMotivoDevolucionDev.Text = ListBandeja.ElementAt(0).motivoDevolucion;
                lblTipoDevolucionDev.Text = ListBandeja.ElementAt(0).tipoDevolucion;
                lblNroOficioDev.Text = ListBandeja.ElementAt(0).nroOficio;
                lblNroTramiteDev.Text = ListBandeja.ElementAt(0).nroTramite;
            }

            Up_Devolución.Update();
            MPE_Devolucion.Show();
        }

        protected void imgbtnAyudaMemoria_Click(object sender, EventArgs e)
        {

            GridViewRow row;
            LinkButton boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            string id = grvproy.DataKeys[row.RowIndex].Values[0].ToString();

            Label lblIdProyecto = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdProyecto");
            Label lblIdTipoFinanciamiento = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdTipoFinanciamiento");

            if (lblIdProyecto.Text.Length > 0 && !lblIdProyecto.Text.Equals("0"))
            {
                _BEProyecto.id = Convert.ToInt32(lblIdProyecto.Text);
                _BEProyecto = _objProyecto.F_MON_ObtieneModalidad(_BEProyecto);

                string script;

                switch (_BEProyecto.idTipoProyecto)
                {
                    case 4:
                        script = "<script>window.open('../Monitor/Ayuda_MemoriaNE.aspx?id=" + lblIdProyecto.Text + "&idtipo=" + lblIdTipoFinanciamiento.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        break;

                    case 6: //Proy. Especiales JICA
                        script = "<script>window.open('../Monitor/Ayuda_Memoria_PE.aspx?id=" + lblIdProyecto.Text + "&idtipo=" + lblIdTipoFinanciamiento.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        break;

                    default:
                        script = "<script>window.open('../Monitor/Ayuda_Memoria.aspx?id=" + lblIdProyecto.Text + "&idtipo=" + lblIdTipoFinanciamiento.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                        break;
                }

            }
            else
            {
                Label lblFlagAdmisibilidad = (Label)grvproy.Rows[row.RowIndex].FindControl("lblFlagAdmisibilidad");
                Label lblsnip = (Label)grvproy.Rows[row.RowIndex].FindControl("lblsnip");

                if (lblFlagAdmisibilidad.Text.Equals("1"))
                {
                    id = ("ADM") + lblsnip.Text;
                    string script = "<script>window.open('../Solicitudes/Ayuda_Memoria.aspx?id=" + id + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>window.open('../Solicitudes/Ayuda_Memoria.aspx?id=" + id + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
            }


        }

        protected void imgbtnAyudaMemoriaExterno_Click(object sender, EventArgs e)
        {
            string script = "<script>window.open('../Solicitudes/Ayuda_Memoria.aspx?id=" + 0 + "&snip=" + lblSNIPMef.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

        }

        protected void lnkbtnEvaluacion_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblIdSolicitud = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdSolicitud");
            Label lblIdPrograma = (Label)grvproy.Rows[row.RowIndex].FindControl("lblsector");
            //Label lblIdTipoFinanciamiento = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdTipoFinanciamiento");

            string script;

            if (lblIdSolicitud.Text.Length > 0 && !lblIdSolicitud.Text.Equals("0"))
            {
                if (lblIdPrograma.Text.Equals("PNSU"))
                {
                    script = "<script>window.open('../solicitudes/reg_FichaVerificacionPNSU.aspx?id=" + lblIdSolicitud.Text + "&subsector=1','_blank') </script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    if (lblIdPrograma.Text.Equals("PNSR"))
                    {
                        script = "<script>window.open('../solicitudes/reg_FichaVerificacionPNSU.aspx?id=" + lblIdSolicitud.Text + "&subsector=3','_blank') </script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                    else
                    {
                        script = "<script>window.open('../solicitudes/reg_CheckListPimb.aspx?id=" + lblIdSolicitud.Text + "&subsector=2','_blank') </script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    }
                }

            }
        }

        protected void lnkbtnMonitoreo_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblIdProyecto = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdProyecto");

            string script;

            if (lblIdProyecto.Text.Length > 0 && !lblIdProyecto.Text.Equals("0"))
            {
                _BEProyecto.id = Convert.ToInt32(lblIdProyecto.Text);
                _BEProyecto = _objProyecto.F_MON_ObtieneModalidad(_BEProyecto);

                switch (_BEProyecto.IDFINANCIMIENTO)
                {
                    case "1":
                        switch (_BEProyecto.id_ModalidadFinanciamiento) //MODALIDAD FINANCIAMIENTO
                        {
                            case 1:

                                script = "<script>window.open('../Monitor/Registro_PreInversion_PorTransferencia.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;

                            default:
                                script = "<script>window.open('../Monitor/Registro_PreInversion.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;
                        }
                        break;

                    case "2":
                        switch (_BEProyecto.id_ModalidadFinanciamiento) //MODALIDAD FINANCIAMIENTO
                        {
                            case 1:

                                script = "<script>window.open('../Monitor/Registro_ExpedienteTecnico_PorTransferencia.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;

                            default:
                                script = "<script>window.open('../Monitor/Registro_ExpedienteTecnico.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;
                        }
                        break;

                    case "3":
                        switch (_BEProyecto.id_ModalidadFinanciamiento) //MODALIDAD FINANCIAMIENTO
                        {
                            case 1:

                                script = "<script>window.open('../Monitor/Registro_Obra.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;

                            case 4:

                                if (_BEProyecto.Cod_programa.Equals("4")) //PNT
                                {
                                    script = "<script>window.open('../Monitor/NE_Tambo.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                }
                                else
                                {
                                    script = "<script>window.open('../Monitor/Registro_NE.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                }
                                break;

                            case 3:
                                script = "<script>window.open('../Monitor/Registro_EjecucionContrata.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;

                            case 2:
                                script = "<script>window.open('../Monitor/Registro_Jica.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;

                            default:
                                script = "<script>window.open('../Monitor/Registro_Obra.aspx?id=" + lblIdProyecto.Text + "','_blank') </script>";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                                break;
                        }
                        break;
                }
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            DataTable dtInfo = (DataTable)Session["obj"];

            GridView grd = new GridView();
            grd.Columns.Add(new BoundField { DataField = "tipo" });
            grd.Columns.Add(new BoundField { DataField = "snip" });
            grd.Columns.Add(new BoundField { DataField = "departamento" });
            grd.Columns.Add(new BoundField { DataField = "provincia" });
            grd.Columns.Add(new BoundField { DataField = "distrito" });
            grd.Columns.Add(new BoundField { DataField = "ccpp" });
            grd.Columns.Add(new BoundField { DataField = "nombre_proyecto" });
            grd.Columns.Add(new BoundField { DataField = "unidad_ejec" });
            grd.Columns.Add(new BoundField { DataField = "monto_snip" });
            grd.Columns.Add(new BoundField { DataField = "monto_inversion" });
            grd.Columns.Add(new BoundField { DataField = "poblacion" });
            grd.Columns.Add(new BoundField { DataField = "ambito_sector" });
            //grd.Columns.Add(new BoundField { DataField = "ratioSnip" });
            grd.Columns.Add(new BoundField { DataField = "estado_snip" });
            grd.Columns.Add(new BoundField { DataField = "aprobacion_viabilidad" });
            grd.Columns.Add(new BoundField { DataField = "documentacion" });
            grd.Columns.Add(new BoundField { DataField = "Tecnico" });
            grd.Columns.Add(new BoundField { DataField = "tipoFinanciamiento" });

            grd.Columns.Add(new BoundField { DataField = "EstadoAdmisibilidd" });
            grd.Columns.Add(new BoundField { DataField = "FechaEvaluacionAdmisibilidad" });
            grd.Columns.Add(new BoundField { DataField = "EstadoElegibilidad" });
            grd.Columns.Add(new BoundField { DataField = "FechaEvaluacionElegibilidad" });
            grd.Columns.Add(new BoundField { DataField = "EstadoCalidad" });
            grd.Columns.Add(new BoundField { DataField = "fechaEvaluacionCalidad" });
            grd.Columns.Add(new BoundField { DataField = "EstadoMonitoreo" });

            grd.AutoGenerateColumns = false;
            grd.DataSource = dtInfo;
            grd.DataBind();


            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter sw = new System.IO.StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_ConsultaProyecto_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                //grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;
                grd.HeaderRow.Font.Size = 11;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 16; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                }

                for (int i = 16; i < 24; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("C6E0B4", 16));
                }

                //for (int i = 33; i < 39; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //}
                grd.HeaderRow.Cells[0].Text = "PROGRAMA";
                grd.HeaderRow.Cells[1].Text = "SNIP";
                grd.HeaderRow.Cells[2].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[3].Text = "PROVINCIA";
                grd.HeaderRow.Cells[4].Text = "DISTRITO";
                grd.HeaderRow.Cells[4].Width = 120;
                grd.HeaderRow.Cells[5].Text = "CENTRO POBLADO";
                grd.HeaderRow.Cells[5].Width = 120;
                grd.HeaderRow.Cells[6].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[6].Width = 500;
                grd.HeaderRow.Cells[7].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[8].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[9].Text = "MONTO SEGUN FASE DE INVERSION";
                grd.HeaderRow.Cells[9].Width = 120;

                grd.HeaderRow.Cells[10].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[10].Width = 90;
                grd.HeaderRow.Cells[11].Text = "AMBITO";
                grd.HeaderRow.Cells[12].Text = "ESTADO SNIP MEF";
                grd.HeaderRow.Cells[12].Width = 90;
                grd.HeaderRow.Cells[13].Text = "FECHA VIABILIDAD";
                grd.HeaderRow.Cells[13].Width = 90;
                grd.HeaderRow.Cells[14].Text = "DOCUMENTACION PRESENTADA AL MVCS";
                grd.HeaderRow.Cells[15].Text = "EVALUADOR";
                grd.HeaderRow.Cells[16].Text = "ETAPA";
                grd.HeaderRow.Cells[16].Width = 120;
                grd.HeaderRow.Cells[17].Text = "ESTADO";
                grd.HeaderRow.Cells[18].Text = "FECHA ULTIMA EVALUACIÓN";
                grd.HeaderRow.Cells[18].Width = 90;
                grd.HeaderRow.Cells[19].Text = "ESTADO";
                grd.HeaderRow.Cells[20].Text = "FECHA ULTIMA EVALUACIÓN";
                grd.HeaderRow.Cells[20].Width = 90;
                grd.HeaderRow.Cells[21].Text = "ESTADO";
                grd.HeaderRow.Cells[22].Text = "FECHA ULTIMA EVALUACIÓN";
                grd.HeaderRow.Cells[22].Width = 90;
                grd.HeaderRow.Cells[23].Text = "ESTADO";


                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];

                    for (int j = 0; j < 24; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();
                myTable.Text = "INFORMACIÓN DEL PROYECTO";
                myTable.ColumnSpan = 16;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("C6E0B4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADMISIBILIDAD";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("C6E0B4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ELEGIBILIDAD";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("C6E0B4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CALIDAD";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("C6E0B4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "MONITOREO";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("C6E0B4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);

                // Agrega título en primera celda
                //string Titulo = "REPORTE DE CONSULTA DE PROYECTOS DEL MINISTERIO DE VIVIENDA, CONSTRUCCION Y SANEAMIENTO AL " + DateTime.Now.ToString("dd-MM-yyyy");
                //HttpContext.Current.Response.Write(Titulo);
                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }

        protected void lnkbtnAdmisibilidad_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblsnip = (Label)grvproy.Rows[row.RowIndex].FindControl("lblsnip");

            string script = "<script>window.open('../Solicitudes/Ficha_AdmEle?id=" + lblsnip.Text + "&xidTipoEval=483','_blank','width=960,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void lnkbtnElegibilidad_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblsnip = (Label)grvproy.Rows[row.RowIndex].FindControl("lblsnip");

            string script = "<script>window.open('../Solicitudes/Ficha_AdmEle?id=" + lblsnip.Text + "&xidTipoEval=484','_blank','width=960,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void ddlProceso_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlbusqdist.Items.Clear();
            if (ddlProceso.SelectedValue.ToString() != "0")
            {
                tdEstadoProceso1.Visible = true;
                tdEstadoProceso2.Visible = true;

                if (ddlProceso.SelectedValue.Equals("1") || ddlProceso.SelectedValue.Equals("2"))
                {
                    ddlEstadoProceso.DataSource = _objBLUtil.listarCombosSOL_SSP("16", "");
                    ddlEstadoProceso.DataTextField = "nom_estado";
                    ddlEstadoProceso.DataValueField = "cod_estado";
                    ddlEstadoProceso.DataBind();

                    ddlEstadoProceso.Items.Insert(0, new ListItem("- TODOS -", "0"));

                }
                if (ddlProceso.SelectedValue.Equals("3"))
                {
                    ddlEstadoProceso.DataSource = _objBLUtil.listarCombosSOL_SSP("19", "");
                    ddlEstadoProceso.DataTextField = "nom_estado";
                    ddlEstadoProceso.DataValueField = "cod_estado";
                    ddlEstadoProceso.DataBind();
                }

                if (ddlProceso.SelectedValue.Equals("4"))
                {
                    ddlEstadoProceso.DataSource = objBLbandeja.F_spMON_FiltroEstadoEjecucion();
                    ddlEstadoProceso.DataTextField = "nombre";
                    ddlEstadoProceso.DataValueField = "valor";
                    ddlEstadoProceso.DataBind();

                    ddlEstadoProceso.Items.Insert(0, new ListItem("- TODOS -", "0"));
                }

            }
            else
            {
                tdEstadoProceso1.Visible = false;
                tdEstadoProceso2.Visible = false;

                //ddlEstadoProceso.Visible = false;

                ddlEstadoProceso.Items.Clear();
            }
        }

        protected void ddlModalidadFinanciamiento_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSubModalidadFinanciamiento.Items.Clear();

            if (ddlModalidadFinanciamiento.SelectedValue.ToString() == "3")
            {
                ddlSubModalidadFinanciamiento.DataSource = _objBLUtil.listarCombosSOL_SSP("18", "3");
                ddlSubModalidadFinanciamiento.DataTextField = "nom_estado";
                ddlSubModalidadFinanciamiento.DataValueField = "cod_estado";
                ddlSubModalidadFinanciamiento.DataBind();

                ddlSubModalidadFinanciamiento.Items.Insert(0, new ListItem("- TODOS -", "0"));

                tdSubModalidadFinanciamiento2.Visible = true;
                tdSubModalidadFinanciamiento1.Visible = true;
            }
            else
            {
                tdSubModalidadFinanciamiento2.Visible = false;
                tdSubModalidadFinanciamiento1.Visible = false;
            }




        }

        protected void lnkbtnAyudaMonitoreoDetalle_Click(object sender, EventArgs e)
        {

        }

        protected void lnkbtnMetasCalidadDetalle_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblIdSolicitud = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdSolicitud");

            string script = "<script>window.open('../Solicitudes/HistorialSolicitudes.aspx?id=" + lblIdSolicitud.Text + "&tipo=0&log=9','_blank','width=1100,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);


        }

        protected void lnkbtnArchivosCalidadDetalle_Click(object sender, EventArgs e)
        {

            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblIdSolicitud = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdSolicitud");

            string script = "<script>window.open('../Solicitudes/reg_doc.aspx?id=" + lblIdSolicitud.Text + "&tipo=0&log=9','_blank','width=800,height=600,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);



        }

        protected void lnkbtnAyudaMemoriaCalidadDetalle_Click(object sender, EventArgs e)
        {

            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            Label lblIdSolicitud = (Label)grvproy.Rows[row.RowIndex].FindControl("lblIdSolicitud");

            string script = "<script>window.open('../Solicitudes/Ayuda_Memoria.aspx?id=" + lblIdSolicitud.Text + "&t=r','_blank','width=800,height=900,scrollbars=yes') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected bool ValidarEntero(string snip)
        {
            bool result = true;

            try
            {
                Convert.ToInt32(snip);
            }
            catch
            {
                result = false;
            }



            return result;
        }
    }
}