﻿<%@ Page Title="Consulta de Proyectos" Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="ConsultaOld.aspx.cs" Inherits="Web.Solicitudes.ConsultaOld" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ContentPlaceHolderID="head" runat="server" ID="Head1">
    <style type="text/css">
        .CajaDialogo {
            background-color: #CEE3F6;
            border-width: 4px;
            padding: 0px;
            width: 200px;
            font-weight: bold;
        }

            .CajaDialogo div {
                margin: 5px;
                text-align: center;
            }
           .FondoAplicacion {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }

        .style1 {
            width: 312px;
        }

        .style2 {
            width: 208px;
        }

         .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            font-family: Calibri;
        }
          
        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        input[type=text] {
            font-family: Tahoma;
            font-size: 11px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }

        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }
                    

        elemento {
            background-color: black;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            position: absolute;
            left: 5px;
            top: 5px;
            width: 936px;
            height: 736px;
            visibility: visible;
            z-index: 1;
        }

        .user66 {
            position: absolute;
            bottom: 0;
            left: 30px;
        }

        .BloqueInfoTotal {
    border-radius: 20px;
    -moz-border-radius: 12px;
    -webkit-border-radius: 12px;
    border: 2px solid #0080C0;
    line-height: 14px;
   
    padding: 20px;
    font-family: Calibri;
    max-width:600px;
}
    
        .alert-warning {
            color: rgb(169, 68, 66);
            background-color: rgb(242, 222, 222);
            border-color: rgb(235, 204, 209);
            padding: 15px 15px 20px;
            font-family: helvetica;
            border: 1px solid transparent;
            border-radius: 4px;
            width:30%;
            margin:auto;
        }

    </style>

    <script type="text/javascript">

        function EnviarMEF() {

            var paginaMef = "http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=" + document.getElementById("ctl00_ContentPlaceHolder1_lblSNIPMef").innerHTML;
            //alert(paginaMef);
            window.open(paginaMef);
            //location.href = pagina
        }

    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" >
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.svg" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; ">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

    <asp:UpdatePanel runat="server" ID="Up_Consulta" UpdateMode="Conditional">
        <ContentTemplate>

            <center>

        <div>

            <br />
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="center" style="height: 25px">
                        <asp:Label runat="server" Text="CONSULTA DE PROYECTOS" CssClass="titlePage"></asp:Label> </td>
                </tr>
            </table>

            <br />
            <asp:Panel runat="server" DefaultButton="btnBuscar">
                        <table  style="border-collapse:separate !important; border-spacing: 4px; margin:auto">
                            <tr>
                                <td align="center" class="formHeader">
                                    <asp:DropDownList ID="cboSearch" runat="server" Width="320px">
                                        <asp:ListItem Value="COD">CÓDIGO SNIP</asp:ListItem>
                                        <asp:ListItem Value="PRY">NOMBRE DE PROYECTO</asp:ListItem>
                                    </asp:DropDownList></td>

                                <td align="center" class="formHeader">PROGRAMA</td>
                                <td align="center" class="formHeader"> DEPARTAMENTO</td>
                                <td align="center" runat="server" id="tdProvincia" visible="false" class="formHeader">PROVINCIA</td>
                                <td align="center" runat="server" id="tdDistrito" visible="false" class="formHeader">DISTRITO</td>
                                <td align="center"  runat="server" visible="false" class="formHeader">DOCUMENTACIÓN</td>
                                <td align="center" class="formHeader">ESTRATEGIA</td>
                                 <td align="center" style="width:70px" class="formHeader" >MODALIDAD DE FINANCIAMIENTO</td>
                                <td align="center" runat="server" id="tdSubModalidadFinanciamiento1" visible="false" style="width:80px" class="formHeader">
                                     SUB MODALIDAD FINANCIAMIENTO</td>
                                <td align="center" class="formHeader">PROCESO</td>
                                <td align="center" runat="server" id="tdEstadoProceso1" visible="false" class="formHeader">
                                     ESTADO DEL PROCESO</td>
                               
                                <td>
                                  </td>

                            </tr>
                            <tr>
                                <td align="center">
                                        <asp:TextBox ID="txtSearch" runat="server" MaxLength="200" Width="320px" placeholder="Ejemplo búsqueda por SNIPs: 1234,4446,212948"></asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:DropDownList ID="ddlbusq_subsector" runat="server">
                                    </asp:DropDownList></td>
                                <td align="center">
                                    <asp:DropDownList ID="ddlbusqdepa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlbusqdepa_SelectedIndexChanged">
                                    </asp:DropDownList></td>
                                <td align="center" runat="server" id="tdProvincia2" visible="false" >
                                    <asp:DropDownList ID="ddlbusqprov" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="ddlbusqprov_SelectedIndexChanged">
                                    </asp:DropDownList></td>
                                <td align="center" runat="server" id="tdDistrito2" visible="false">
                                    <asp:DropDownList ID="ddlbusqdist" runat="server">
                                    </asp:DropDownList></td>
                                <td align="center" runat="server" visible="false">
                                    <asp:DropDownList ID="ddlbusq_documentacion" runat="server">
                                        <asp:ListItem Value="0" Selected="True">- TODOS -</asp:ListItem>

                                        <asp:ListItem Value="1">SOLICITUD</asp:ListItem>
                                        <asp:ListItem Value="2">PERFIL</asp:ListItem>
                                        <asp:ListItem Value="3">EXPEDIENTE TEC.</asp:ListItem>
                                        <asp:ListItem Value="5">PERFIL Y EXPEDIENTE</asp:ListItem>
                                        <asp:ListItem Value="4">NINGUNA</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td align="center">
                                    <asp:DropDownList ID="ddl_estrategia" runat="server">
                                    </asp:DropDownList></td>

                                 <td align="center">
                                    <asp:DropDownList ID="ddlModalidadFinanciamiento" Width="140px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlModalidadFinanciamiento_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="center" runat="server" id="tdSubModalidadFinanciamiento2" visible="false">
                                     <asp:DropDownList ID="ddlSubModalidadFinanciamiento" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="center">
                                    <asp:DropDownList ID="ddlProceso" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProceso_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Selected="True">- - TODOS - -</asp:ListItem>
                                        <asp:ListItem Text="ADMISIBILIDAD" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="ELEGIBILIDAD" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="CALIDAD" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="MONITOREO" Value="4"></asp:ListItem>
                                    </asp:DropDownList></td>

                                <td align="center" runat="server" id="tdEstadoProceso2" visible="false">
                                    <asp:DropDownList ID="ddlEstadoProceso" runat="server">
                                    </asp:DropDownList>
                                </td>
                             </tr>
                            <tr>
                                <td style="height:10px"></td>
                            </tr>
                            <tr><td colspan="9" style="text-align:center">
                                <asp:LinkButton ID="btnBuscar" runat="server"  CssClass="btn btn-primary" OnClick="btnBuscar_Click"> <i class="fa fa-search"></i> BUSCAR</asp:LinkButton>
                                <asp:LinkButton ID="btnReiniciar2" runat="server"  CssClass="btn btn-default" OnClick="btnReiniciar_Click"> <i class="fa fa-paint-brush"></i> LIMPIAR</asp:LinkButton>
                                <%--<asp:LinkButton ID="lnkbtn" runat="server"  CssClass="btn btn-default" OnClick="btnReiniciar_Click"> <i class="fa fa-paint-brush"></i> LIMPIAR</asp:LinkButton>--%>
                                 <%--<asp:Button ID="btnReiniciar" runat="server" CssClass="btn btn-default" OnClick="btnReiniciar_Click" Text="LIMPIAR" />--%>
                                </td></tr>
                        </table>
            </asp:Panel>
        </div>

                <asp:Label runat="server" ID="lblMsjGrvProy" Font-Bold="true" ForeColor="Red" Text="" Visible="false"></asp:Label>

                <div style="padding: 20px 30px">

                    <div class="text-right" style="padding-right: 10px">
                        <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_Click" class="btn btn-verde" Visible="false"><i class="fa fa-file-excel-o"></i>  Exportar</asp:LinkButton>
                    </div>
                  

                    <div>

                        <asp:GridView
                            ID="grvproy"
                            runat="server"
                            Width="97%"
                            AllowPaging="True"
                            AutoGenerateColumns="False"
                            CssClass="Grid"
                            EmptyDataText="No se han registrado proyectos."
                            EditRowStyle-BackColor="#ffffb7"
                            OnDataBound="grvproy_DataBound"
                            DataKeyNames="id_solicitudes"
                            OnPageIndexChanging="grvproy_PageIndexChanging"
                            OnRowDataBound="grvproy_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="PROG.">
                                    <HeaderStyle HorizontalAlign="Center" Height="40px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblsector" Text='<%#Eval("tipo") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CODIGO">
                                    <ItemTemplate>
                                        <a style="text-decoration: underline; cursor: pointer; color: #1853E1"
                                            href="<%# Eval("snip", "http://ofi2.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo={0}") %>"
                                            target="_blank" title="Hacer clic para ver el proyecto en el MEF">
                                            <%# (Convert.ToInt32(Eval("snip")) == 0 ? "NO" : Eval("snip"))%>
                                        </a>
                                        <asp:Label ID="lblFlagVirtual" runat="server" Text="(*)" Font-Bold="true" Visible="false" ForeColor="#f90000"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CODIGO" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsnip" runat="server" Text='<%# Eval("snip") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="4%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="UBIGEO">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("departamento") +" - "+ Eval("provincia") +" - "+ Eval("distrito")  %>'></asp:Label>
                                        <asp:Label ID="lbldepa" runat="server" Text='<%# Eval("departamento") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle VerticalAlign="Middle" Width="4%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PROVINCIA" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblprov" runat="server" Text='<%# Eval("provincia") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="4%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="DISTRITO" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldist" runat="server" Text='<%# Eval("distrito") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="4%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CENTRO POBLADO" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCCPP" runat="server" Text='<%# Eval("ccpp") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="4%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="NOMBRE DEL PROYECTO">
                                    <HeaderStyle />
                                    <ItemStyle VerticalAlign="Middle" Width="20%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblpry" runat="server" Text='<%# Eval("nombre_proyecto") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="UNIDAD EJECUTORA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblunidad" runat="server" Text='<%# Eval("unidad_ejec") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="AMBITO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblambito" runat="server" Text='<%# Eval("ambito_sector") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="3%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="BENEFICIARIO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPoblacion" runat="server" Text='<%# Eval("poblacion") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="4%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MONTO VIABLE (S/.)" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmonto_snip" runat="server" Text='<%# Convert.ToDouble(Eval("monto_snip")).ToString("N") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MONTO INVERSIÓN (S/.)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmonto_inversion" runat="server" Text='<%# Convert.ToDouble(Eval("monto_inversion")).ToString("N") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ESTADO MEF (VIABILIDAD)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblestado_snip" runat="server" Text='<%# Eval("estado_snip") %>'></asp:Label><br />
                                        <asp:Label ID="lblviabilidad" runat="server" Text='<%# Eval("aprobacion_viabilidad").ToString().Length>0 ? ("(" +Eval("aprobacion_viabilidad")+ ")") : "" %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="EVALUADOR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTecnico" runat="server" Text='<%# Eval("Tecnico") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Center" Width="8%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="OBJETO DE FINANCIAMIENTO">
                                    <ItemTemplate>
                                        &nbsp;
                            <asp:Label ID="lblTipoFinanciamiento" runat="server" Text='<%# Eval("tipoFinanciamiento") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="4%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MODALIDAD FINANCIAMIENTO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblModalidadFinanciamiento" runat="server" Text='<%# Eval("ModalidadFinanciamiento") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Center" Width="2%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="DETALLE SITUACIONAL" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblIngresadoPRESET" Text='<%# "Proyecto activado en la PRESET a pedido de la unidad ejecutora (Evaluación física desestimada). Fecha de Activación en la PRESET : " +Eval("fechaActivacionPreset")+"<br />" %>' Visible="false" Font-Bold="true"
                                            Style="font-family: Calibri; color: #2c60f5; line-height: 13px; font-size: 12px"></asp:Label>

                                        <asp:Label runat="server" ID="lblIngresadoPRESET2" Text="Evaluación Anterior :<br />" Visible="false" Font-Bold="true"
                                            Style="font-family: Calibri; color: #000; line-height: 13px; font-size: 12px"></asp:Label>

                                        <div>
                                            <asp:LinkButton ID="lnkbtnAdmisibilidad" runat="server" Visible="false" OnClick="lnkbtnAdmisibilidad_Click" title="" CssClass="btn btn-tag1 btn-xs tooltips">
                                                        Admisibilidad<span>Clic para visualizar la ficha de admisibilidad</span>
                                            </asp:LinkButton>
                                            <asp:Label runat="server" ID="lblEstadoAdmisibilidad" Text='<%# Eval("EstadoAdmisibilidd") %>' Visible="false"></asp:Label>
                                            <asp:Label runat="server" ID="lblFechaAdmisibilidad" Text='<%# Eval("FechaEvaluacionAdmisibilidad").ToString().Length==0 ? "" : (" - " +Eval("FechaEvaluacionAdmisibilidad").ToString().Substring(0,10)) %>' Visible="false"></asp:Label>
                                        </div>
                                        <div>
                                            <asp:LinkButton ID="lnkbtnElegibilidad" runat="server" Visible="false" CssClass="btn btn-tag1 btn-xs tooltips" OnClick="lnkbtnElegibilidad_Click">
                                                        Puntaje<span>Clic para visualizar la ficha de elegibilidad</span>
                                            </asp:LinkButton>
                                            <asp:Label runat="server" ID="lblElegibilidad" Text='<%# Eval("EstadoElegibilidad") %>' Visible="false"></asp:Label>
                                            <asp:Label runat="server" ID="lblFechaElegibilidad" Text='<%# Eval("FechaEvaluacionElegibilidad").ToString().Length==0 ? "" : (" - " +Eval("FechaEvaluacionElegibilidad").ToString().Substring(0,10)) %>' Visible="false"></asp:Label>
                                        </div>
                                        <div>
                                            <div class="btn btn-tag1 btn-xs tooltips" id="divCalidad" runat="server" visible="false">
                                                Calidad 
                                                        <span>
                                                            <asp:LinkButton ID="lnkbtnMetasCalidadDetalle" runat="server" Visible="true" OnClick="lnkbtnMetasCalidadDetalle_Click"><i class="fa fa-list-ol"></i>
                                                                     <div>Metas</div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbtnArchivosCalidadDetalle" runat="server" Visible="true" OnClick="lnkbtnArchivosCalidadDetalle_Click"><i class="fa fa-file-archive-o"></i>
                                                                    <div>Archivos</div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbtnEvaluacion" runat="server" Visible="false" OnClick="lnkbtnEvaluacion_Click"><i class="fa fa-pencil-square-o"></i>
                                                                        <div>Ficha de Evaluación</div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbtnAyudaMemoriaCalidadDetalle" runat="server" Visible="false" OnClick="lnkbtnAyudaMemoriaCalidadDetalle_Click"><i class="fa fa-question-circle-o"></i>
                                                                        <div>Ayuda Memoria</div>
                                                            </asp:LinkButton>
                                                            <span></span>
                                                        </span>
                                            </div>
                                            <asp:Label runat="server" ID="lblEstadoCalidad" Text='<%# Eval("EstadoCalidad") %>' Visible="false"></asp:Label>
                                            <asp:Label runat="server" ID="lblFechaCalidad" Text='<%# " - " + Eval("fechaEvaluacionCalidad") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkbtnEstado" runat="server" Visible="false" OnClick="lnkbtnEstado_Click" CssClass="btn btn-tag3 btn-xs tooltips">DEVUELTO</asp:LinkButton>
                                            <asp:Label ID="lblFechaRegistroDevolucion" runat="server" Text='<%# Eval("FechaRegistroDevolucion") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblFechaRegistroSSP" runat="server" Text='<%# Eval("FechaRegistoSSP") %>' Visible="false"></asp:Label>
                                        </div>
                                        <div>
                                            <div class="btn btn-tag1 btn-xs tooltips" id="divMonitoreo" runat="server" visible="false">
                                                Monitoreo 
                                                        <span>
                                                            <asp:LinkButton ID="lnkbtnFichaMonitoreoDetalle" runat="server" Visible="false" OnClick="lnkbtnMonitoreo_Click"><i class="fa fa-file-text-o"></i>
                                                                    <div>Ficha Técnica</div>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="lnkbtnAyudaMonitoreoDetalle" runat="server" Visible="false" OnClick="lnkbtnAyudaMonitoreoDetalle_Click"><i class="fa fa-question-circle-o"></i>
                                                                    <div>Ayuda Memoria</div>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="lnkbtnMonitoreo" runat="server" Visible="false" OnClick="lnkbtnMonitoreo_Click"><i class="fa fa-files-o"></i>
                                                                        <div>Detalle</div>
                                                            </asp:LinkButton>

                                                            <span></span>
                                                        </span>
                                            </div>

                                            <asp:Label runat="server" ID="lblEstadoMonitoreo" Text='<%# Eval("EstadoMonitoreo") %>' Visible="false"></asp:Label>
                                        </div>


                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" Width="16%" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="TRAMITE (SITRAD)" Visible="false">
                                    <ItemTemplate>
                                        <a target="_blank" href='ConsultaTramite.aspx?snip=<%#Eval("snip")%>' title="Tramite Documentario" class="tooltips">
                                            <i class="fa fa-file-o tramite"></i>
                                        </a>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Center" Width="2%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="AYUDA MEMORIA">
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="imgbtnAyudaMemoria" runat="server" OnClick="imgbtnAyudaMemoria_Click" CssClass="tooltips">
                                                                    <i class="fa fa-question-circle ayuda"></i><span>Ver Ayuda Memoria</span>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="IDs" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId_TipoDevolucion" runat="server" Text='<%# Eval("id_tipoDevolucion") %>'></asp:Label>
                                        <asp:Label ID="lblIdProyecto" runat="server" Text='<%# Eval("id_proyecto") %>'></asp:Label>
                                        <asp:Label ID="lblIdSolicitud" runat="server" Text='<%# Eval("id_solicitudes") %>'></asp:Label>
                                        <asp:Label ID="lblIdEstadoSolicitud" runat="server" Text='<%# Eval("cod_estado") %>'></asp:Label>
                                        <asp:Label ID="lblFlagAdmisibilidad" runat="server" Text='<%# Eval("flagAdmisibilidad") %>'></asp:Label>
                                        <asp:Label ID="lblIdTipoFinanciamiento" runat="server" Text='<%# Eval("id_TipoFinanciamiento") %>'></asp:Label>
                                        <asp:Label ID="lblvTipoEvaluacion" runat="server" Text='<%# Eval("vTipoEvaluacion") %>'></asp:Label>
                                        <asp:Label ID="lblFlagCargaPRESET" runat="server" Text='<%# Eval("iEnCargaPRESE") %>'></asp:Label>
                                        <asp:Label ID="lblFechaActivacionPRESET" runat="server" Text='<%# Eval("fechaActivacionPreset") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle VerticalAlign="Middle" Width="7%" HorizontalAlign="Center" />
                                </asp:TemplateField>


                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" />
                            <SelectedRowStyle CssClass="GridRowSelected" />
                            <HeaderStyle Height="25px" BackColor="#2D7BBD" />
                            <AlternatingRowStyle CssClass="GridAlternating" />
                            <EditRowStyle BackColor="#FFFFB7" />
                            <RowStyle CssClass="GridRowNormal" />
                        </asp:GridView>


                    </div>

                    <table style="width: 100%" border="0" cellspacing="0" cellpadding="0">
                        <tr runat="server" id="trGrd" visible="true">
                            <td style="width: 33.33%; height: 20px" align="center">

                                <table width="240px" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblPryNoAsignados2" runat="server" Font-Bold="true" CssClass="small"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 5px"></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblPryNoAsignados3" runat="server" Font-Bold="true"  CssClass="small"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td style="width: 33.33%" align="center">
                                <asp:Label ID="lblPaginado2" runat="server" Font-Bold="true" CssClass="small"></asp:Label></td>
                            <td style="width: 33.33%" align="right"></td>
                        </tr>

                        <tr>
                            <td colspan="3" style="text-align: center;">
                                <asp:Label runat="server" ID="lblMsjMEF" Font-Bold="true" ForeColor="Red" Text="" Font-Size="12px"></asp:Label>

                                <div class="BloqueInfoTotal" style="margin-top: 15px; font-size: 10.5px; margin: auto;" runat="server" id="divMEF" visible="false">
                                    <table runat="server" id="tblMEF" visible="false">
                                        <tr>
                                            <td colspan="4" style="text-align: center; font-weight: bold; font-size: 11px">INFORMACIÓN DEL PROYECTO (SEGÚN MEF)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px;"></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">NOMBRE :
                                            </td>
                                            <td style="text-align: left" colspan="3">
                                                <asp:Label ID="lblProyectoMef" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">SNIP :
                                            </td>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblSNIPMef" runat="server" Text=""></asp:Label>
                                            </td>

                                            <td style="text-align: right">ESTADO :
                                            </td>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblEstadoMef" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">MONTO SNIP (S/.) :
                                            </td>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblMontoSnipMef" runat="server" Text=""></asp:Label>
                                            </td>

                                            <td style="text-align: right">ESTADO VIABILIDAD :
                                            </td>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblViabilidadMef" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center" colspan="4">
                                                <asp:Panel runat="server" ID="PanelResponsabilidaMEF" GroupingText="RESPONSABILIDAD FUNCIONAL DEL PROYECTO">
                                                    <table>

                                                        <tr>
                                                            <td style="text-align: right">Función:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblFuncionMef" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">Programa:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblProgramaMef" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="text-align: right">SubPrograma:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblSubprogramaMef" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>


                                                    </table>
                                                </asp:Panel>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="text-align: center" colspan="4">
                                                <asp:Panel runat="server" ID="Panel1" GroupingText="UNIDAD FORMULADORA DEL PROYECTO">
                                                    <table>

                                                        <tr>
                                                            <td style="text-align: right">Sector:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblSectorMef" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">Pliego:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblPliegoMef" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="text-align: right">Nombre:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <asp:Label ID="lblNomFormuladoraMef" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">Ayuda Memoria:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <%--<asp:ImageButton ID="imgbtnAyudaMemoriaExterno" runat="server" ImageUrl="~/img/helpDoc.png" ToolTip="Ayuda Memoria" OnClick="imgbtnAyudaMemoriaExterno_Click" onmouseover="this.src='../img/helpDoc2.png';" onmouseout="this.src='../img/helpDoc.png';" />--%>
                                                                <%-- <asp:LinkButton ID="imgbtnAyudaMemoriaExterno" runat="server" OnClick="imgbtnAyudaMemoriaExterno_Click">
                                                                    <i class="fa fa-question-circle-o ayuda"></i>
                                                                    </asp:LinkButton>--%>

                                                                <asp:LinkButton ID="imgbtnAyudaMemoriaExterno" runat="server" OnClick="imgbtnAyudaMemoriaExterno_Click" CssClass="tooltips">
                                                                    <i class="fa fa-question-circle ayuda font-lg"></i><span>Ver Ayuda Memoria</span>
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right">Más Información:
                                                            </td>
                                                            <td style="text-align: left">
                                                                <button onclick="EnviarMEF()">Ficha de registro - Banco de Proyectos - SNIP</button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

            </center>


        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExcel" />
        </Triggers>
    </asp:UpdatePanel>


    <%-- DEVOLUCIÓN--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:ModalPopupExtender ID="MPE_Devolucion" runat="server" TargetControlID="HiddenField1"
        BackgroundCssClass="modalBackground" OkControlID="imgbtnCerrarDevolucion" DropShadow="true"
        PopupControlID="Panel_Devolucion">
    </asp:ModalPopupExtender>

    <asp:Panel ID="Panel_Devolucion" runat="server" Height="250px" Width="600px" CssClass="modalPopup" Style="display:none">

        <table width="100%" style="background-color: #E2ECF3">
            <tr>
                <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label13" CssClass="titulo2" runat="server" Font-Bold="true">DEVOLUCIÓN DE PROYECTO</asp:Label>
                </td>
                <td align="right" style="width: 26px">
                    <asp:ImageButton ID="imgbtnCerrarDevolucion" runat="server" AlternateText="Cerrar ventana"
                        ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                        Height="25px" Width="26px" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel runat="server" ID="Up_Devolución" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="font-size: 11px; line-height: 14px; font-family: Calibri">
                    <tr>
                        <td style="text-align: right">
                            <b>SNIP:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblSNIPDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <b>NOMBRE DEL PROYECTO:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblNombreProyectoDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <b>TIPO DE FINANCIAMIENTO:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTipoFinanciamientoDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <b>MOTIVO DE DEVOLUCIÓN:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblMotivoDevolucionDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <b>TIPO DE DEVOLUCIÓN:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTipoDevolucionDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>

                    <tr>
                        <td style="text-align: right">
                            <b>NRO. OFICIO:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblNroOficioDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>

                    <tr>
                        <td style="text-align: right">
                            <b>NRO. TRAMITE:</b>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblNroTramiteDev" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>

    </asp:Panel>

    


</asp:Content>


