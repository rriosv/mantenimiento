﻿<%@ Page Title="Intervenciones SG" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ReporteIntervenciones_SG.aspx.cs" Inherits="Web.reportes.ReporteIntervenciones_SG" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        .headerReporte {
            margin:auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
     <script type="text/javascript" lang="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" >
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.svg" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; ">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
            
    <div style="text-align:center">
            <p style="padding-top:12px">
                <span class="titlePage">INTERVENCIONES DE SANEAMIENTO</span>
            </p>
            <br />
            <br />
        </div>
    <table style="margin:auto;border-collapse:separate !important; border-spacing: 4px;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="PROGRAMA: " Font-Size="12px"></asp:Label></td>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlPrograma" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma_SelectedIndexChanged">
                            <asp:ListItem Text=" - TODOS - " Value=""></asp:ListItem>
                            <asp:ListItem Text="PNSU" Value="1"></asp:ListItem>
                            <asp:ListItem Text="PNSR" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
      <div style="text-align:center">
    <asp:LinkButton ID="btnReporteResumen" runat="server" OnClick="btnReporteResumen_Click" CssClass="btn btn-verde"><i class="fa fa-file-excel-o"></i> Exportar</asp:LinkButton>
  </div>
            <div style="padding: 20px 20px">
                <%--<div class="text-right" style="padding-right: 10px">
                    <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_Click" class="btn btn-verde" Visible="false"><i class="fa fa-file-excel-o"></i>  Exportar</asp:LinkButton>
                    <br />
                </div>--%>
                <div style="margin:auto; text-align:center">
                    <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:GridView
                        ID="grdProyectos"
                        runat="server"
                        Width="100%"
                        AllowPaging="True"
                        AutoGenerateColumns="False"
                        CssClass="Grid"
                        EmptyDataText="No se han registrado proyectos."
                        EditRowStyle-BackColor="#ffffb7"
                        OnPageIndexChanging="grdProyectos_PageIndexChanging"
                        
                        PageSize="10">
                        <Columns>
                                                        
                            <asp:TemplateField HeaderText="N°" >
                                <ItemTemplate>
                                    <asp:Label ID="numero" runat="server" Text='<%# Eval("numero") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle  VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="ID PROYECTO" >
                                <ItemTemplate>
                                    <asp:Label ID="id_proyecto" runat="server" Text='<%# Eval("id_proyecto") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle  VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                          

                            <asp:TemplateField HeaderText="AÑO" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="Anio" runat="server" Text='<%# Eval("Anio") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="PROGRAMA" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="Programa" runat="server" Text='<%# Eval("Programa") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="UNIDAD" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="Unidad" runat="server" Text='<%# Eval("Unidad") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SNIP" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="cod_snip" runat="server" Text='<%# Eval("cod_snip") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>
                         
                            <asp:TemplateField HeaderText="DEPARTAMENTO">
                                <ItemTemplate>
                                    <asp:Label ID="departamento" runat="server" Text='<%# Eval("Departamento") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="PROVINCIA">
                                <ItemTemplate>
                                    <asp:Label ID="provincia" runat="server" Text='<%# Eval("Provincia") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DISTRITO">
                                <ItemTemplate>
                                    <asp:Label ID="distrito" runat="server" Text='<%# Eval("Distrito") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CENTRO POBLADO">
                                <ItemTemplate>
                                    <asp:Label ID="ccpp" runat="server" Text='<%# Eval("CentroPoblado") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="BENEFICIARIOS">
                                <ItemTemplate>
                                    <asp:Label ID="Beneficiarios" runat="server" Text='<%# Eval("Beneficiarios") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                          <asp:TemplateField HeaderText="UNIDAD EJECUTORA">
                                <ItemTemplate>
                                    <asp:Label ID="UnidadEjecutora" runat="server" Text='<%# Eval("UnidadEjecutora") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOMBRE DE PROYECTO">
                                <ItemTemplate>
                                    <asp:Label ID="NombreProyecto" runat="server" Text='<%# Eval("NombreProyecto") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Nº CONEXIONES DE AGUA">
                                <ItemTemplate>
                                    <asp:Label ID="NroConexionesAgua" runat="server" Text='<%# Eval("NroConexionesAgua") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Nº CONEXIONES DE ALCANTARILLADO /UBS">
                                <ItemTemplate>
                                    <asp:Label ID="NroConexionUBS" runat="server" Text='<%# Eval("NroConexionUBS") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                 
                            <asp:TemplateField HeaderText="ETAPA">
                                <ItemTemplate>
                                    <asp:Label ID="Etapa" runat="server" Text='<%# Eval("Etapa") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="FECHA DE INICIO DE LA ETAPA">
                                <ItemTemplate>
                                    <asp:Label ID="FechaInicio" runat="server" Text='<%# Eval("FechaInicio") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                 
                            <asp:TemplateField HeaderText="FECHA TERMINO DE LA ETAPA">
                                <ItemTemplate>
                                    <asp:Label ID="fechaFinReal" runat="server" Text='<%# Eval("fechaFinReal") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="MODALIDAD DE FINANCIAMIENTO">
                                <ItemTemplate>
                                    <asp:Label ID="Modalidad" runat="server" Text='<%# Eval("Modalidad") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                 
                            <asp:TemplateField HeaderText="COSTO DEL PROYECTO S/.">
                                <ItemTemplate>
                                    <asp:Label ID="MontoActualizado" runat="server" Text='<%# Eval("MontoActualizado") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="AVANCE FISICO (%)">
                                <ItemTemplate>
                                    <asp:Label ID="AvanceFisicoReal" runat="server" Text='<%# Eval("AvanceFisicoReal") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="AVANCE FINANCIERO (%)">
                                <ItemTemplate>
                                    <asp:Label ID="AvanceFinancieroReal" runat="server" Text='<%# Eval("AvanceFinancieroReal") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ESTADO">
                                <ItemTemplate>
                                    <asp:Label ID="Estado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OBSERVACION">
                                <ItemTemplate>
                                    <asp:Label ID="Observacion" runat="server" Text='<%# Eval("Observacion") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>


                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                        <HeaderStyle Height="25px" BackColor="#2D7BBD" />
                        <AlternatingRowStyle CssClass="GridAlternating" />
                        <EditRowStyle BackColor="#FFFFB7" />
                        <RowStyle CssClass="GridRowNormal" />
                    </asp:GridView>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
             
</asp:Content>



