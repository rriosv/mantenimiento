﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Business;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Drawing;

namespace Web.reportes
{
    public partial class Reporte_PorContrata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();

            dt = _objBLBandeja.spMON_Reporte_PorContrata();
            xgrdReporte.DataSource = dt;
            xgrdReporte.DataBind();

        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {

            //xgrdExporter.WriteXlsToResponse();
            BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();
            DataTable obj = _objBLBandeja.spMON_Reporte_PorContrata();
            //obj = selectDisctinct(obj, "id_proyecto");
            GridView grd2 = new GridView();
            grd2.DataSource = obj;
            grd2.DataBind();

            Exportar_XLS(grd2);
        }

        protected void Exportar_XLS(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_RM086_2015_VIVIENDA.xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 15; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("0072C6", 16));
                    grd.HeaderRow.Cells[i].ForeColor = Color.White;
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "PROGRAMA";
                grd.HeaderRow.Cells[1].Text = "SNIP";
                grd.HeaderRow.Cells[2].Text = "NOMBRE DE PROYECTO";
                grd.HeaderRow.Cells[2].Width = 500;
                grd.HeaderRow.Cells[3].Text = "UNIDADA EJECUTORA";
                grd.HeaderRow.Cells[4].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[5].Text = "PROVINCIA";
                grd.HeaderRow.Cells[6].Text = "DISTRITO";

                //grd.HeaderRow.Cells[6].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[7].Text = "CCPPP";
                grd.HeaderRow.Cells[7].Width = 200;
                grd.HeaderRow.Cells[8].Text = "POBLACION";
                grd.HeaderRow.Cells[9].Text = "MONTO SNIP (S/.)";
                grd.HeaderRow.Cells[9].Width = 90;
                grd.HeaderRow.Cells[10].Text = "COSTO MVCS (S/.)";
                grd.HeaderRow.Cells[10].Width = 90;
                grd.HeaderRow.Cells[11].Text = "FECHA VIABILIDAD";
                grd.HeaderRow.Cells[12].Text = "UNIDAD";
                grd.HeaderRow.Cells[13].Text = "ESTADO";
                grd.HeaderRow.Cells[14].Text = "RESPONSABLE";

                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;

                    for (int j = 0; j < 15; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "PROYECTOS BAJO LA MODALIDAD POR CONTRATA";
                myTable.ColumnSpan = 15;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("0072C6", 16));
                myTable.Font.Bold = true;
                myTable.ForeColor = Color.White;
                gvHeaderRow.Cells.Add(myTable);


                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
    }
}