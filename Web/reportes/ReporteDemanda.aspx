﻿<%@ Page Title="Demanda" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ReporteDemanda.aspx.cs" Inherits="Web.reportes.ReporteDemanda" Culture="es-PE" UICulture="es-PE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div>
        <div style="margin: auto; text-align: center">
            <br />
         
            <span class="titlePage">
                PROYECTOS EN LA ETAPA DE CALIDAD
            </span>
            <br />
            <br />
        </div>
    </div>


    <table border="0" style="width: 50%; margin: auto; border-collapse: separate !important; border-spacing: 4px;">
        <tr>
            <td>
                <asp:DropDownList ID="cboSearch" runat="server" Width="290px">
                    <asp:ListItem Value="COD">SNIP</asp:ListItem>
                    <asp:ListItem Value="PRY">Proyecto</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="center">PROGRAMA
            </td>
            <td align="center">DEPARTAMENTO
            </td>
            <td align="center">PROVINCIA
            </td>
            <td align="center">DISTRITO
            </td>
            <td valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button Width="90px" ID="btnReiniciar" runat="server" Text="Limpiar" CssClass="btn btn-primary"
                                OnClick="btnReiniciar_Click" /></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtSearch" runat="server" MaxLength="50" Width="210px" Text=' '></asp:TextBox>
                <asp:Button CssClass="btn btn-primary" ID="btnBuscar" runat="server" OnClick="btnBuscar_Click" Text="Buscar" />
            </td>
            <td align="center">
                <asp:DropDownList ID="ddl_subsector" runat="server" AutoPostBack="true" OnSelectedIndexChanged="FiltrarSector">
                    <asp:ListItem Value="1" Text="PNSU"></asp:ListItem>
                    <asp:ListItem Value="2" Text="PMIB"></asp:ListItem>
                    <asp:ListItem Value="3" Text="PNSR"></asp:ListItem>
                    <asp:ListItem Value="" Text="-Seleccione-"></asp:ListItem>
                </asp:DropDownList>

            </td>

            <td style="height: 24px" align="center">
                <asp:DropDownList ID="ddl_depa" runat="server" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="FiltrarDepa" />
            </td>

            <td style="height: 24px" align="center">
                <asp:DropDownList ID="ddl_prov" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="FiltrarProv">
                </asp:DropDownList>
            </td>
            <td style="height: 24px" align="center">
                <asp:DropDownList ID="ddl_dist" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="FiltrarDist">
                </asp:DropDownList>
            </td>
        </tr>
    </table>


    <br />
    <center>
        <div style="width: 92%">

            <div style="text-align: right">
                <%--<asp:ImageButton ID="btnExcel" runat="server" OnClick="btnExcel_onclick" ImageUrl="~/img/xls.gif" />--%>

                <%--               <asp:ImageButton ID="btnExcel" runat="server" OnClick="btnExcel_onclick" ImageUrl="~/img/EXPORTAR.png"   />--%>

                <asp:LinkButton ID="btnReporteResumen" runat="server" OnClick="btnExcel_onclick" CssClass="btn btn-verde"><i class="fa fa-file-excel-o"></i> Exportar</asp:LinkButton>

                <%--     <asp:Button
                  ID="btnExcel" runat="server" Text="Exportar a Excel" Width="130px" CssClass="botonMonitor" OnClick="btnExcel_onclick" />--%>
            </div>
            <asp:UpdatePanel runat="server" ID="up_grd" UpdateMode="Conditional">
                <ContentTemplate>


                    <asp:GridView
                        ID="grd"
                        runat="server"
                        CssClass="Grid"
                        EditRowStyle-BackColor="#ffffb7"
                        EmptyDataText="No existe registros"
                        BorderWidth="1px"
                        DataKeyNames="id_solicitudes"
                        AutoGenerateColumns="false"
                        PageSize="15"
                        OnDataBound="grd_OnDataBound"
                        AllowPaging="True"
                        OnPageIndexChanging="grd_PageIndexChanging">
                        <Columns>


                            <asp:BoundField DataField="numero" HeaderText="N°" ItemStyle-HorizontalAlign="center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle CssClass="GridHeader2" Width="10px" />
                            </asp:BoundField>

                            <asp:BoundField DataField="id_solicitudes" HeaderText="ID Solicitud">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>


                            <asp:BoundField DataField="subsector" HeaderText="Programa">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="snip" HeaderText="SNIP">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="iCodUnificado" HeaderText="Código Unificado">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="nombre_proyecto" HeaderText="Proyecto">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="unidad_ejec" HeaderText="Unidad Ejecutora">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="nom_depa" HeaderText="Departamento">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="nom_prov" HeaderText="Provincia">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="nom_dist" HeaderText="Distrito">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="poblacionBeneficiad" HeaderText="Población">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ambito" HeaderText="Ambito">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="estadoEstudio" HeaderText="Estado de Estudio">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="aprobacion_viabilidad" HeaderText="Fecha Viabilidad">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="monto_snip" HeaderText="Monto Viable">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="costoProyecto" HeaderText="Monto de Inversion Total">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DevengadoAcumulado" HeaderText="Devengado Acumulado">
                                <HeaderStyle CssClass="GridHeader" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>

                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                        <HeaderStyle Height="15px" />
                        <AlternatingRowStyle CssClass="GridAlternating" />
                        <EditRowStyle BackColor="#FFFFB7" />
                        <RowStyle CssClass="GridRowNormal" />

                    </asp:GridView>
                    <br />
                    <asp:Label ID="LabeltotalAsignado" runat="server" Font-Bold="true" CssClass="tituloContador" /><asp:Label ID="lblPaginadoAsignado" runat="server" Font-Bold="true" CssClass="tituloContador" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </center>
</asp:Content>


