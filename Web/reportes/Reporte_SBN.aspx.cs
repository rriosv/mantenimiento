﻿using System;

namespace Web.reportes
{
    public partial class Reporte_SBN : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Session["clave"].ToString();
            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();
            ifrm.Attributes["src"] = UrlFrontEnd + "reportesbn/index/" + token;
            ifrm.Attributes["width"] = "100%";
            //ifrm.Attributes["height"] = "100%";
            ifrm.Attributes["scrolling"] = "yes";
            ifrm.Attributes["frameborder"] = "0";
        }
    }
}