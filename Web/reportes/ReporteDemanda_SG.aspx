﻿<%@ Page Title="Demanda SG" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ReporteDemanda_SG.aspx.cs" Inherits="Web.reportes.ReporteDemanda_SG" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <script type="text/javascript" lang="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" >
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.svg" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; ">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

    <div style="text-align: center">
        <p style="padding-top: 12px">
            <span class="titlePage">DEMANDA DE PROYECTOS DE SANEAMIENTO (EN EVALUACION DE ADMISIBILIDAD Y CALIDAD)</span>
        </p>
        <br />
        <br />
        <asp:LinkButton ID="btnReporteResumen" runat="server" OnClick="btnReporteResumen_Click" CssClass="btn btn-verde"><i class="fa fa-file-excel-o"></i> Exportar</asp:LinkButton>
    </div>

    <div style="padding: 40px 50px">
                
                <%--<div class="text-right" style="padding-right: 10px">
                    <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_Click" class="btn btn-verde" Visible="false"><i class="fa fa-file-excel-o"></i>  Exportar</asp:LinkButton>
                    <br />
                </div>--%>
                <div style="margin:auto; text-align:center">
                    <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:GridView
                        ID="grdProyectos"
                        runat="server"
                        Width="100%"
                        AllowPaging="True"
                        AutoGenerateColumns="False"
                        CssClass="Grid"
                        EmptyDataText="No se han registrado proyectos."
                        EditRowStyle-BackColor="#ffffb7"
                        OnPageIndexChanging="grdProyectos_PageIndexChanging"
                        
                        PageSize="15">
                        <Columns>
                                                        
                            <asp:TemplateField HeaderText="N°" >
                                <ItemTemplate>
                                    <asp:Label ID="numero" runat="server" Text='<%# Eval("numero") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle  VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="SNIP" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblsnip" runat="server" Text='<%# Eval("snip") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="AMBITO" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="ambito_sector" runat="server" Text='<%# Eval("ambito_sector") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PROGRAMA" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="programa" runat="server" Text='<%# Eval("programa") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOMBRE DEL PROYECTO">
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="lblpry" runat="server" Text='<%# Eval("nombre_proyecto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DEPARTAMENTO">
                                <ItemTemplate>
                                    <asp:Label ID="departamento" runat="server" Text='<%# Eval("departamento") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="PROVINCIA">
                                <ItemTemplate>
                                    <asp:Label ID="provincia" runat="server" Text='<%# Eval("provincia") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DISTRITO">
                                <ItemTemplate>
                                    <asp:Label ID="distrito" runat="server" Text='<%# Eval("distrito") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="LOCALIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="ccpp" runat="server" Text='<%# Eval("ccpp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="UNIDAD EJECUTORA">
                                <ItemTemplate>
                                    <asp:Label ID="unidad_ejec" runat="server" Text='<%# Eval("unidad_ejec") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                          <asp:TemplateField HeaderText="POBLACIÓN BENEFICIARIA">
                                <ItemTemplate>
                                    <asp:Label ID="poblacion" runat="server" Text='<%# Eval("poblacion") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MONTO DE INVERSION  (S/.)">
                                <ItemTemplate>
                                    <asp:Label ID="monto_inversion" runat="server" Text='<%# Eval("monto_inversion") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ADMISIBILIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="EstadoAdmisibilidd" runat="server" Text='<%# Eval("EstadoAdmisibilidd") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                 
                            <asp:TemplateField HeaderText="FECHA ADMISIBILIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="FechaEvaluacionAdmisibilidad" runat="server" Text='<%# Eval("FechaEvaluacionAdmisibilidad") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="ELEGIBILIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="EstadoElegibilidad" runat="server" Text='<%# Eval("EstadoElegibilidad") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                 
                            <asp:TemplateField HeaderText="FECHA ELEGIBILIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="FechaEvaluacionElegibilidad" runat="server" Text='<%# Eval("FechaEvaluacionElegibilidad") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="CALIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="EstadoCalidad" runat="server" Text='<%# Eval("EstadoCalidad") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>
                 
                            <asp:TemplateField HeaderText="FECHA CALIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="fechaEvaluacionCalidad" runat="server" Text='<%# Eval("fechaEvaluacionCalidad") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle"  HorizontalAlign="Center" />
                            </asp:TemplateField>

                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                        <HeaderStyle Height="25px" BackColor="#2D7BBD" />
                        <AlternatingRowStyle CssClass="GridAlternating" />
                        <EditRowStyle BackColor="#FFFFB7" />
                        <RowStyle CssClass="GridRowNormal" />
                    </asp:GridView>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

</asp:Content>


