﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="Reporte_PorContrata.aspx.cs" Inherits="Web.reportes.Reporte_PorContrata" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <div style="text-align: center">
        <b>
            <asp:Label runat="server" Font-Size="15px" Text="REPORTE DE PROYECTOS EN EL MARCO DE LA RM N°086-2015-VIVIENDA"></asp:Label>
        </b>
    </div>
    <br />
    <br />
    <center>

        <asp:Button ID="btnExportar" runat="server" Text="EXPORTAR" OnClick="btnExportar_Click" />

        <div style="height: 5px;">
        </div>
        
        <dx:ASPxGridView ID="xgrdReporte" ClientInstanceName="xgrdReporte" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="92%" Font-Size="9" SettingsPager-PageSize="12" AutoGenerateColumns="false" >
           
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Styles Cell-Wrap="True" Header-Wrap="True" Header-BackColor="#0072C6" Header-ForeColor="White"></Styles>
            <Columns>
                 <dx:GridViewDataTextColumn Caption="PROGRAMA" FieldName="Programa" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Width="70px" >
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                  <CellStyle HorizontalAlign="Center"></CellStyle>
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="SNIP" FieldName="cod_snip" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Width="50">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="NOMBRE DE PROYECTO" FieldName="nom_proyecto" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UNIDAD EJECUTORA" FieldName="entidad_ejec" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="DEPARTAMENTO" FieldName="depa" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PROVINCIA" FieldName="prov" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="DISTRITO" FieldName="dist" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CCPP" FieldName="CCPP" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="POBLACIÓN" FieldName="poblacion" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="MONTO SNIP (S/.)" FieldName="monto_snip" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="COSTO MVCS (S/.)" FieldName="CostoMVCS" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="FECHA DE VIABILIDAD" FieldName="FechaViabilidad" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UNIDAD" FieldName="Unidad" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="ESTADO" FieldName="Estado" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RESPONSABLE" FieldName="tecnico" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                  <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                 </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        
        <dx:ASPxGridViewExporter ID="xgrdExporter" runat="server" GridViewID="xgrdReporte" ReportHeader="{\rtf1\ansi\ansicpg1252\deff0\deflang10250{\fonttbl{\f0\fnil\fcharset0 Times New Roman;}}
\viewkind4\uc1\pard\f0\fs20 REPORTE DE OBRAS BAJO LA MODALIDAD POR CONTRATA\par
}
"></dx:ASPxGridViewExporter>
    </center>
</asp:Content>


