﻿<%@ Page Title="Reporte Monitoreo" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ReporteShockInversiones.aspx.cs" Inherits="Web.reportes.ReporteShockInversiones" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function imprimir(nombre) {
            document.getElementById("imprimirOculto").style.display = 'inline'
            var ficha = document.getElementById(nombre);
            var ventimp = window.open(' ', 'popimpr');
            ventimp.document.write(ficha.innerHTML);
            ventimp.document.close();
            ventimp.print();
            document.getElementById("imprimirOculto").style.display = 'none'
            ventimp.close();
        }
   
         function ValidNum(e) {
             var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
             return ((tecla > 47 && tecla < 58) || tecla == 13 || tecla == 8);
         }
     
        function button1_Click(s, e) {
            if (xgrdBandeja.IsCustomizationWindowVisible())
                xgrdBandeja.HideCustomizationWindow();
            else
                xgrdBandeja.ShowCustomizationWindow();
            UpdateButtonText();
        }
        function xgrdBandeja_CustomizationWindowCloseUp(s, e) {
            UpdateButtonText();
        }
        function UpdateButtonText() {
            var text = xgrdBandeja.IsCustomizationWindowVisible() ? "Ocultar" : "Mostrar";
            text += " Selector de Columnas";
            button1.SetText(text);
        }
    </script>
 <style type="text/css">
     
     @font-face
     {font-family:'DINB';
      SRC:url('DINB.ttf');
         }
     .subtit
     {font-size:11px; 
      font-weight:bold; 
      color:#0B0B61 !important; 
      background:url(../img/arrow1.png) 0 2px no-repeat; 
      padding-left:15px; 
      border-bottom:1px dotted #0B0B61; 
      display:block; 
      margin-top:20px }
      
      .ajax__tab_xp .ajax__tab_body {
        font-family: Calibri;
        font-size: 10pt;
      }

.tablaRegistro
{
    line-height:15px; 
    }


      
  .tituloTipo
  { font-size:13pt;
    font-family:'DINB';
    color :#000000; 
  }
      
  .titulo2
  { font-family:'DINB';
    font-size:12pt;
    color :#28375B; 
    
      }
      
        .tituloContador
  { font-family:Calibri;
    font-size:10pt;
  }

</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div>

        <br />
        <div style="text-align: center">
            <p>
                <span class="titlePage" >REPORTE DETALLADO DE MONITOREO</span>
            </p>
        </div>
        <br />


        <asp:UpdatePanel runat="server" ID="Up_Filtro" UpdateMode="Conditional" Visible="false">
            <ContentTemplate>
                <div style="width: 80%; margin: auto">

                    <table style="margin: auto; visibility:hidden;border-collapse:separate !important; border-spacing: 4px;" >
                        <tr>
                            <td align="center">
                                <b>PER&Iacute;ODO</b>
                            </td>
                            <td align="center">
                                <b>SNIP</b>
                            </td>
                            <td align="center">
                                <b>PROYECTO</b>
                            </td>
                            <td align="center">
                                <b>TIPO PROYECTO</b></td>
                            <td align="center">
                                <b>TIPO EJECUCI&Oacute;N</b>
                            </td>
                            <td align="center">
                                <b>ESTADO</b>
                            </td>
                            <td align="center">
                                <b>DEPARTAMENTO</b>
                            </td>
                            <td align="center">
                                <b>PROVINCIA</b>
                            </td>
                            <td align="center">
                                <b>DISTRITO</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <table>
                                    <tr>
                                        <td>Del</td>
                                        <td>
                                            <asp:TextBox ID="txtAnioMayor" runat="server" Width="30px" onpaste="return false" oncut="return false" oncopy="return false"
                                                MaxLength="4"></asp:TextBox></td>
                                        <td>al</td>
                                        <td>
                                            <asp:TextBox ID="txtAnioMenor" runat="server" Width="30px" onpaste="return false" oncut="return false"
                                                oncopy="return false" MaxLength="4"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtSnipFiltro" runat="server" Width="60px"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtProyectoFiltro" runat="server" Width="160px"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlTipo" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlTecnico" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlSubsector" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubsector_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlDepa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepa_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlprov" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlprov_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddldist" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9">
                                <asp:Button ID="btnBuscar" runat="server" Width="80px" Text="Buscar" CssClass="btn btn-azul" OnClick="btnBuscar_OnClick" />
                                <asp:Button ID="btnLimpiar" runat="server" Width="80px" Text="Limpiar" CssClass="btn btn-azul" OnClick="btnLimpiar_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="margin:auto; text-align:center" >
        <table style="margin:auto;border-collapse:separate !important; border-spacing: 4px;">
            <tr><td><b>PROGRAMA :</b></td>
                <td>
                    <asp:DropDownList ID="ddlPrograma" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPrograma_SelectedIndexChanged">
                        <asp:ListItem Value="1" Text="PNSU"></asp:ListItem>
                        <asp:ListItem Value="2" Text="PMIB"></asp:ListItem>
                        <asp:ListItem Value="3" Text="PNSR"></asp:ListItem>
                        <asp:ListItem Value="" Text=" - TODOS - "></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_Click"  class="btn btn-verde" Visible="true" ><i class="fa fa-file-excel-o"></i>  Exportar</asp:LinkButton>
    </div>
    
    <br />
  

            <div id="imprimirGrilla" style="margin: auto;">
                <div id="imprimirOculto" style="display: none">
                    <table style="width: 100%">
                        <tr>
                            <td align="left"><span style="font-family: Calibri; font-size: 12px;"><b>MINISTERIO DE VIVIENDA, CONSTRUCCI&Oacute;N Y SANEAMIENTO</b></span></td>
                            <td align="right"><span style="font-family: Calibri; font-size: 12px;"><b>Sistema de Gesti&oacute;n de Proyectos</b></span></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center><span style="font-family: Calibri; font-size: 24px;"><b>REPORTE DETALLADO DE MONITOREO</b></span></center>
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="width: 90%; margin: auto">

                    <tr>
                        <td style="text-align: center">
                            <div style="margin: auto">

                                <dx:ASPxButton runat="server" ID="button1" ClientInstanceName="button1" Text="Mostrar Selector de Columnas"
                                    UseSubmitBehavior="false" AutoPostBack="false">
                                    <ClientSideEvents Click="button1_Click" />
                                </dx:ASPxButton>

                                <%--<dx:ASPxGridView ID="xgrdBandeja" ClientInstanceName="xgrdBandeja" runat="server" KeyFieldName="id_proyecto" SettingsPager-PageSize="50" AutoGenerateColumns="False" Theme="PlasticBlue" Font-Size="9px" Styles-Header-Font-Size="9px">--%>
                                     <dx:ASPxGridView ID="xgrdBandeja" ClientInstanceName="xgrdBandeja" runat="server" AutoGenerateColumns="False" EnableTheming="True" Theme="MetropolisBlue" SettingsPager-PageSize="10" KeyFieldName="id_proyecto" Styles-Header-Font-Size="10px">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="DATOS GENERALES">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ID" Settings-HeaderFilterMode="CheckedList" FieldName="xidIdProyecto" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True" Font-Size="11px">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="PROGRAMA" Settings-HeaderFilterMode="CheckedList" FieldName="vTipoPrograma" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>


                                                <dx:GridViewDataTextColumn Caption="AÑO 1erDS" Settings-HeaderFilterMode="CheckedList" FieldName="vAnioDS" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="SNIP" Settings-HeaderFilterMode="CheckedList" FieldName="iSNIP" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="NOMBRE DEL PROYECTO" Settings-HeaderFilterMode="CheckedList" FieldName="vNombreProyecto" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True" Font-Size="10px">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="UNIDAD EJECUTORA" Settings-HeaderFilterMode="CheckedList" FieldName="vUnidadEjecutora" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True"  />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True" Font-Size="10px">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="DEPARTAMENTO" Settings-HeaderFilterMode="CheckedList" FieldName="vDepartamento" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="PROVINCIA" Settings-HeaderFilterMode="CheckedList" FieldName="vProvincia" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="DISTRITO" Settings-HeaderFilterMode="CheckedList" FieldName="vDistrito" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="BENEFICIARIOS" Settings-HeaderFilterMode="CheckedList" FieldName="iPoblacionSnip" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="FINANCIAMIENTO">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="MONTO SNIP (S/.)" Settings-HeaderFilterMode="CheckedList" FieldName="deMontoViable" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="MONTO DE INVERSION TOTAL (S/.)" Settings-HeaderFilterMode="CheckedList" FieldName="fMontoActualizadoSOSEM" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="MONTO TRANSFERIDO ACUMULADO(S/.)" Settings-HeaderFilterMode="CheckedList" FieldName="deMontoTransferidoAcumuladoDS" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="MONTO DEVENGADO ACUMULADO (S/.)" Settings-HeaderFilterMode="CheckedList" FieldName="deDevevengadoAcumulado" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="MODALIDAD DE FINANCIAMIENTO" Settings-HeaderFilterMode="CheckedList" FieldName="vModalidadFinanciamiento" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="TIPO EJECUCIÓN" Settings-HeaderFilterMode="CheckedList" FieldName="vTipoEjecuccion" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ETAPA" Settings-HeaderFilterMode="CheckedList" FieldName="vTipoProyecto" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="AMBITO" Settings-HeaderFilterMode="CheckedList" FieldName="vTipoAmbito" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="PROCESO DE SELECCIÓN DE LA ETAPA DE EJECUCION" Visible="false">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="VALOR REFERENCIAS (S/.)" Settings-HeaderFilterMode="CheckedList" FieldName="deValorReferencial" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA CONVOCATORIA" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaConvocatoria" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA BUENA PRO" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaBuenaPro" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA BUENA PRO CONSENTIDA" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaBuenaProConsentida" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="CONSORCIO/CONTRATISTA" Settings-HeaderFilterMode="CheckedList" FieldName="vTipoEmpresa" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="NOMBRE EMPRESA" Settings-HeaderFilterMode="CheckedList" FieldName="vNombreEmpresa" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="MONTO CONTRATADO" Settings-HeaderFilterMode="CheckedList" FieldName="deMontoContratado" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA FIRMA DE CONTRATO" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaFirmaContrato" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="PROCESO DE SELECCIÓN DE LA SUPERVISION" Visible="false">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="VALOR REFERENCIAS SUP. (S/.)" Settings-HeaderFilterMode="CheckedList" FieldName="deValorReferencialSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA CONVOCATORIA SUP." Settings-HeaderFilterMode="CheckedList" FieldName="vFechaConvocatoriaSupervicion" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA BUENA PRO SUP." Settings-HeaderFilterMode="CheckedList" FieldName="vFechaBuenaProSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA BUENA PRO CONSENTIDA SUP." Settings-HeaderFilterMode="CheckedList" FieldName="vFechaBuenaProConsentidaSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="CONSORCIO/CONTRATISTA SUP." Settings-HeaderFilterMode="CheckedList" FieldName="vTipoEmpresaSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="NOMBRE EMPRESA SUP" Settings-HeaderFilterMode="CheckedList" FieldName="vNombreEmpresaSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="MONTO CONTRATADO SUP." Settings-HeaderFilterMode="CheckedList" FieldName="deMontoContratadoSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="FECHA FIRMA DE CONTRATO SUP." Settings-HeaderFilterMode="CheckedList" FieldName="vFechaFirmaContratoSupervision" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="COORDINADOR MVCS" Visible="false">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="COORDINADOR MVCS" Settings-HeaderFilterMode="CheckedList" FieldName="vCoordinadorMVCS" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="COORDINADOR UNIDAD EJECUTORA" Visible="false">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="COORDINADOR UE" Settings-HeaderFilterMode="CheckedList" FieldName="vCoordinadorUE" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="TELEFONO UE" Settings-HeaderFilterMode="CheckedList" FieldName="vTelefonoUE" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="CORREO UE" Settings-HeaderFilterMode="CheckedList" FieldName="vCorreoUE" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="ESTADO DE AVANCE DEL PROYECTO">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="FECHA INICIO" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaInicio" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="PLAZO CONTRACTUAL" Settings-HeaderFilterMode="CheckedList" FieldName="iPlazoContractual" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="FECHA FIN CONTRACTUAL" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaFinContractualFormula" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="FECHA FIN + AMPL. + PARAL." Settings-HeaderFilterMode="CheckedList" FieldName="vFechaFinAmplParalizacion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="FECHA TERMINO REAL" Settings-HeaderFilterMode="CheckedList" FieldName="fechaTerminoReal" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="AVANCE FISICO EJECUTADO" Settings-HeaderFilterMode="CheckedList" FieldName="deAvanceFisicoEjecutado" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ESTADO" Settings-HeaderFilterMode="CheckedList" FieldName="vEstadoEjecucion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="SUB ESTADO" Settings-HeaderFilterMode="CheckedList" FieldName="vSubEstado" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="SUB ESTADO 2" Settings-HeaderFilterMode="CheckedList" FieldName="vSubEstado2" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="LIQUIDACIÓN" Visible="false">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="RESOLUCION LIQUIDACION" Settings-HeaderFilterMode="CheckedList" FieldName="ResolucionLiquidacion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="FECHA LIQUIDACION" Settings-HeaderFilterMode="CheckedList" FieldName="vFechaLiquidacion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="MONTO LIQUIDACION" Settings-HeaderFilterMode="CheckedList" FieldName="deMontoLiquidacion" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                    <PropertiesTextEdit DisplayFormatString="N2">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="OBSERVACIONES Y RECOMENDACIONES">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="DETALLE SITUACIONAL" Settings-HeaderFilterMode="CheckedList" FieldName="vDetalleSituacional" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True" Font-Size="10px">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ACCIONES" Settings-HeaderFilterMode="CheckedList" FieldName="vAcciones" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True" Font-Size="10px">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="FECHA ACTUALIZACION AVANCES" Settings-HeaderFilterMode="CheckedList" FieldName="FechaActualizacionAvances" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="FECHA ULTIMO CAMBIO ESTADO" Settings-HeaderFilterMode="CheckedList" FieldName="FechaUltimoCambioEstado" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ULTIMA ETAPA" Settings-HeaderFilterMode="CheckedList" FieldName="vUltimaEtapa" HeaderStyle-Font-Bold="true" VisibleIndex="1">
                                                    <Settings HeaderFilterMode="CheckedList" />
                                                    <HeaderStyle Font-Bold="True" Wrap="True" />
                                                    <CellStyle HorizontalAlign="Center" Wrap="True">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                 <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                                 <SettingsCustomizationWindow Enabled="True" />
                                 <SettingsPager PageSize="5"></SettingsPager>

                        <Settings ShowFooter="True" ShowHeaderFilterBlankItems="true" ShowHeaderFilterButton="true"/>
                        <SettingsDataSecurity AllowInsert="False" />
                        <Styles Header-BackColor="#2D7BBD" Header-ForeColor="White">
                        <Header BackColor="#2D7BBD" ForeColor="White"></Header>
                        </Styles>
                                    <ClientSideEvents CustomizationWindowCloseUp="xgrdBandeja_CustomizationWindowCloseUp"/>
                                </dx:ASPxGridView>
                                <dx:ASPxGridViewExporter ID="xgrdExporterBandeja" runat="server" GridViewID="xgrdBandeja" PageHeader-Center="REPORTE DE MONITOREO"></dx:ASPxGridViewExporter>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 33.33%; text-align: center">
                            <asp:Label ID="LabeltotalAsignado"
                                runat="server" Font-Bold="True" CssClass="tituloContador" Font-Names="Calibri"
                                Font-Size="10pt" /><asp:Label ID="lblPaginadoAsignado" runat="server"
                                    Font-Bold="True" CssClass="tituloContador" Font-Names="Calibri"
                                    Font-Size="10pt" /></td>

                    </tr>

                </table>
            </div>
   
   
</asp:Content>

<asp:Content ID="FooterContent"  ContentPlaceHolderID="ContentPlaceFooter" runat="server">
     
</asp:Content>



