﻿<%@ Page Title="Reporte General" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ReporteTotal.aspx.cs" Inherits="Web.reportes.ReporteTotal" Culture="es-PE" UICulture="es-PE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <style type="text/css">
     
       
  .botonMonitor {
	/*filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#78b1c7', EndColorStr='#264d5b');
	background-color:#264d5b;*/
	filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#8C99B9', EndColorStr='#28375B');
	background-color:#28375B;
	color: white;
    font-weight:bold;
	border: 0px;
	border-style: solid;
	/*border-color: #2c5869;*/
	border-color: #4864A4;
	cursor:pointer;
	font-size: 12px;
	padding: 3px;
	
	border-radius:10px;
   -moz-border-radius: 10px;
   -webkit-border-radius: 10px;
}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <table style="background-color: White;" class="Grid" width="100%" border="0" cellspacing="2"
        cellpadding="0">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">
            <br />
               <br />
            <asp:Label ID="Label3" runat="server" Text="REPORTE GENERAL DEL PROGRAMA NACIONAL DE SANEAMIENTO URBANO"  style="font-size:12pt;">
            </asp:Label>
                
                <br />
                  <br />
<br />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center">
            <table><tr><td >
            <table width="40%" border="0" cellspacing="2" cellpadding="1">
                    <tr>
                        <td>
                            <asp:DropDownList ID="cboSearch" runat="server" Width="120px">
                                <%--<asp:ListItem Value="ALL">Buscar por:</asp:ListItem>--%>
                                <asp:ListItem Value="COD">SNIP</asp:ListItem>
                                <asp:ListItem Value="PRY">Proyecto</asp:ListItem>
                                <asp:ListItem Value="SIT">Cod. Tramite</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                      
                        <td align="center">
                            <b>DEPARTAMENTO</b>
                        </td>
                        <td align="center">
                            <b>PROVINCIA</b>
                        </td>
                        <td align="center">
                            <b>DISTRITO</b>
                        </td>
                                        
                   
                    </tr>
                    <tr>
                        <td >
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="50" Width="50px" Text=' '></asp:TextBox>
                            <asp:Button CssClass="botonMonitor" ID="btnBuscar" runat="server" OnClick="btnBuscar_Click"
                                Text="Buscar" Width="58px"  />
                        </td>
                       
                      
                        <td style="height: 24px" align="center">
                            <asp:DropDownList ID="ddl_depa" runat="server" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="FiltrarDepa" />
                        </td>
                      
                        <td style="height: 24px" align="center">
                            <asp:DropDownList ID="ddl_prov" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="FiltrarProv">
                            </asp:DropDownList>
                        </td>
                        <td style="height: 24px" align="center">
                            <asp:DropDownList ID="ddl_dist" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="FiltrarDist">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table></td>
            
            <td valign="middle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button Width="90px" CssClass="botonMonitor" ID="btnReiniciar" runat="server" Text="Limpiar"
                                OnClick="btnReiniciar_Click" /></td></tr></table>
                
           </td> 
    </tr> 
  </table>
    <br />
  <center>
  <div style="width:90%">

    <div style="text-align:right">
              <%--<asp:ImageButton ID="btnExcel" runat="server" OnClick="btnExcel_onclick" ImageUrl="~/img/xls.gif" />--%>
              <asp:Button
                  ID="btnExcel" runat="server" Text="Exportar a Excel" Width="130px" CssClass="botonMonitor" OnClick="btnExcel_onclick" />
            </div>
     <asp:UpdatePanel runat="server" ID="up_grd" UpdateMode="Conditional">
            <ContentTemplate>
          
        
                <asp:GridView 
                 ID="grd"
                 runat="server" 
                 CssClass="Grid" 
                 EditRowStyle-BackColor="#ffffb7" 
                 emptydatatext="No existe registros" 
                 BorderWidth="1px"
                 
                 AutoGenerateColumns="false"
                PageSize="15"
                 OnDataBound="grd_OnDataBound"
                 AllowPaging="True"
                 OnPageIndexChanging="grd_PageIndexChanging" >
                   <Columns>
                 
                     

                <asp:BoundField DataField="programa" HeaderText="Programa">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>

                 <asp:BoundField DataField="subPrograma" HeaderText="Unidad">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>

                     <asp:BoundField DataField="CMD" HeaderText="CMD">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>
                
                 <asp:BoundField DataField="AnioDS" HeaderText="Año de Inicio">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField> 
                 <asp:BoundField DataField="SNIP" HeaderText="SNIP">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField> 

                <asp:BoundField DataField="nom_proyecto" HeaderText="Proyecto">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>    
                      
               <asp:BoundField DataField="monto_snip" HeaderText="Monto SNIP">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>   
                
                 <asp:BoundField DataField="monto_inversion" HeaderText="Costo de Proyecto">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>   
                
                 <asp:BoundField DataField="num_hab_snip" HeaderText="Poblacion SNIP">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>    

               <asp:BoundField DataField="unidadEjecutora" HeaderText="Unidad Ejecutora">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                                                                                             
                <asp:BoundField DataField="depa" HeaderText="Departamento">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>

                <asp:BoundField DataField="prov" HeaderText="Provincia">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>

                <asp:BoundField DataField="dist" HeaderText="Distrito">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:BoundField>
                      
         
                                
                 <asp:BoundField DataField="ambito" HeaderText="Ambito">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                                
                 <asp:BoundField DataField="Tipo" HeaderText="Tipo">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                
                 <asp:BoundField DataField="tipoEjecuccion" HeaderText="Tipo Ejecucion">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="conObservacion" HeaderText="Pendiente Levantamiento Observaciones">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="sinObservacion" HeaderText="Sin Obs.">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                     <asp:BoundField DataField="NoConvocado" HeaderText="No Convocado">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                     <asp:BoundField DataField="concursoProceso" HeaderText="Concurso en Proceso">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                     <asp:BoundField DataField="BuenaProConsentida" HeaderText="Buena Pro">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                     <asp:BoundField DataField="ContratoSuscrito" HeaderText="Contrato Suscrito">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="EnEjecucion" HeaderText="En Ejecucion">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="EnProblemas" HeaderText="En Problemas">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="Terminado" HeaderText="Terminado">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="Liquidacion" HeaderText="Liquidacion">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                   <asp:BoundField DataField="Suspendido" HeaderText="Suspendido">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                   <asp:BoundField DataField="FUltimaInspeccion" HeaderText="Fecha Última Insepección">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

           
                   <asp:BoundField DataField="fisicoProgramado" HeaderText="% Ejecutado">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

             
                   <asp:BoundField DataField="fisicoEjecutado" HeaderText="% Real">
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                   </Columns>

                    <PagerSettings Mode="NumericFirstLast"  />
                    <SelectedRowStyle CssClass="GridRowSelected"  />
                    <HeaderStyle Height="15px"  />
                    <AlternatingRowStyle CssClass="GridAlternating"  />
                      <EditRowStyle BackColor="#FFFFB7"  />

                </asp:GridView>
                <br />
                <asp:Label ID="LabeltotalAsignado" runat="server" Font-Bold="true" CssClass="tituloContador" /><asp:Label ID="lblPaginadoAsignado" runat="server" Font-Bold="true" CssClass="tituloContador" />
            </ContentTemplate>
            </asp:UpdatePanel>
  </div>
  </center>
</asp:Content>


