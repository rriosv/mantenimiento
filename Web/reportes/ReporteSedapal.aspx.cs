﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.reportes
{
    public partial class ReporteSedapal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Session["clave"].ToString();
            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();
            ifrm.Attributes["src"] = UrlFrontEnd + "Reporte/Sedapal/" + token;
            ifrm.Attributes["width"] = "100%";
            //ifrm.Attributes["height"] = "100%";
            ifrm.Attributes["scrolling"] = "yes";
            ifrm.Attributes["frameborder"] = "0";
        }
    }
}