﻿<%@ Page Title="Saneamiento: Obras" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="Reporte_SaneamientoObras.aspx.cs" Inherits="Web.reportes.Reporte_SaneamientoObras" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        select {
            font-family: Tahoma;
            font-size: 9px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro {
            line-height: 15px;
        }

        .tdFiltro {
            font-weight: bold;
            font-size: 12px;
        }


        .titulo {
            font-size: 15pt;
            font-family: Calibri;
            color: #0094D4;
            line-height: 20px;
            font-weight: bold;
        }

        .tituloTipo {
            font-size: 13pt;
            font-family: 'DINB';
            color: #000000;
        }

        .titulo2 {
            font-family: 'DINB';
            font-size: 12pt;
            color: #28375B;
        }

        .tituloContador {
            font-family: Calibri;
            font-size: 10pt;
        }



        .subtit {
            font-size: 11px;
            font-weight: bold;
            color: #0B0B61 !important;
            background: url(../img/arrow1.png) 0 2px no-repeat;
            padding-left: 15px;
            border-bottom: 1px dotted #0B0B61;
            display: block;
            margin-top: 20px;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: Calibri;
            font-size: 10pt;
        }

        .CajaDialogo {
            background-color: #CEE3F6;
            border-width: 4px;
            padding: 0px;
            width: 200px;
            font-weight: bold;
        }

            .CajaDialogo div {
                margin: 5px;
                text-align: center;
            }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: absolute;
        }

        .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        input[type=text] {
            /*	background-image: url(../img/pdf.gif); 
	background-repeat: repeat-x;*/
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-family: Tahoma;
            font-size: 11px;
            padding: 3px;
            border: solid 1px #203f4a;
        }

        .tablaRegistro3 {
            line-height: 15px;
            font-family: Calibri;
            font-size: 11px;
        }

        .MPEDiv_Carta {
            position: fixed;
            text-align: center;
            z-index: 1005;
            background-color: #fff;
            width: auto;
            top: 20%;
            left: 20%;
        }

        .Div_Fondo {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: fixed;
            z-index: 1004;
            top: 0;
            left: 0;
            right: 0;
            bottom: -1px;
            height: auto;
        }


        .divBody {
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border: 2px solid #006fa7;
            font-family: Calibri;
            padding: 0px 5px 5px 5px;
        }




        elemento {
            background-color: black;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            position: absolute;
            left: 5px;
            top: 5px;
            width: 936px;
            height: 736px;
            visibility: visible;
            z-index: 1;
        }

        .user66 {
            position: absolute;
            bottom: 0;
            left: 30px;
        }

        .sinLinea {
            border-style: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <center>
        <div class="Grid">
            <br />
            <p>
                <asp:Label ID="Label3" runat="server" Text="SANEAMIENTO: EJECUCIÓN DE OBRAS" CssClass="titulo"></asp:Label>
                <br />
                <p>
                    <asp:Label ID="lblTitulo" runat="server" Text="GESTIÓN ACTUAL 2016" CssClass="titulo4"></asp:Label>
                </p>
                <p>
                    <asp:Label ID="Label1" runat="server" Text="MVCS (PNSU + PNSR + SEDAPAL) / GL y GR" CssClass="titulo4"></asp:Label>
                </p>
            </p>
            <br />

            <asp:UpdatePanel runat="server" ID="Up_Filtro" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 100%;">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <br />
    </center>

    <center>

        <asp:UpdatePanel ID="Up_grdBandeja" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView
                    ID="grdConsolidado"
                    runat="server"
                    AllowPaging="True"
                    AutoGenerateColumns="False"
                    BorderWidth="0"
                    EmptyDataText="No hay proyectos."
                    Font-Names="Calibri">
                    <Columns>
                        <asp:TemplateField HeaderText="OBRAS DE SANEAMIENTO" ShowHeader="false" HeaderStyle-Font-Size="14px">
                            <ItemTemplate>
                                <table cellspacing="0" cellpadding="6" border="0" style="font-family: Calibri; font-size: 12px">
                                    <tr style="background-color: #e8eef8">
                                        <td style="width: 180px; font-weight: bold; border: 1px solid #000000;">
                                            <%# (Eval("Estado").ToString())%>
                                        </td>
                                        <td align="center" style="width: 100px; font-weight: bold; border: 1px solid #000000;">PNSU</td>
                                        <td align="center" style="width: 100px; font-weight: bold; border: 1px solid #000000;">PNSR</td>
                                        <td align="center" style="width: 100px; font-weight: bold; border: 1px solid #000000;">TOTAL</td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: 1px solid #000000; border-left: 1px solid #000000;">Cantidad de Proyectos</td>
                                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                            <asp:HiddenField ID="HfEstado" runat="server" Value='<%# (Eval("Estado").ToString())%>' />
                                            <asp:LinkButton ID="LbPnsuN" runat="server" OnClick="LbPnsuN_Click"><%# (Eval("proyectosPNSU").ToString())%></asp:LinkButton>
                                        </td>
                                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                            <asp:LinkButton ID="LbPnsrN" runat="server" OnClick="LbPnsrN_Click"><%# (Eval("proyectosPNSR").ToString())%></asp:LinkButton>
                                        </td>
                                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                            <asp:LinkButton ID="LbTN" runat="server" OnClick="LbTN_Click"><%# (Eval("proyectosTotal").ToString())%></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #000000;">Costo del Proyecto (S/)</td>
                                        <td align="center" style="border: 1px solid #000000;">
                                            <%# (Convert.ToDouble(Eval("costoProyectoPNSU")).ToString("N2"))%>
                                        </td>
                                        <td align="center" style="border: 1px solid #000000;">
                                            <%# (Convert.ToDouble(Eval("costoProyectoPNSR")).ToString("N2"))%>
                                        </td>
                                        <td align="center" style="border: 1px solid #000000;">
                                            <%# (Convert.ToDouble(Eval("costoProyectoTotal")).ToString("N2"))%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: 1px solid #000000; border-left: 1px solid #000000;">Inversión del MVCS (S/)</td>
                                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                            <%# (Convert.ToDouble(Eval("InversionPNSU")).ToString("N2"))%>
                                        </td>
                                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                            <%# (Convert.ToDouble(Eval("InversionPNSR")).ToString("N2"))%>
                                        </td>
                                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                            <%# (Convert.ToDouble(Eval("InversionTotal")).ToString("N2"))%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #000000;">Beneficiarios</td>
                                        <td align="center" style="border: 1px solid #000000;">
                                            <%# Convert.ToInt32(Eval("BeneficiarioPNSU")).ToString("N0")%>
                                        </td>
                                        <td align="center" style="border: 1px solid #000000;">
                                            <%# Convert.ToInt32(Eval("BeneficiarioPNSR")).ToString("N0")%>
                                        </td>
                                        <td align="center" style="border: 1px solid #000000;">
                                            <%# Convert.ToInt32(Eval("BeneficiarioTotal")).ToString("N0")%>
                                        </td>
                                    </tr>
                                </table>

                            </ItemTemplate>

                            <HeaderStyle Font-Size="14px" />

                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="sinLinea" Height="20px" />
                    <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                    <FooterStyle HorizontalAlign="Center" />
                    <HeaderStyle Height="25px" BackColor="#2D7BBD" CssClass="GridHeader3" BorderColor="Black" />
                    <%-- <AlternatingRowStyle CssClass="GridAlternating" />--%>
                    <%-- <RowStyle CssClass="GridRowNormal" Height="28px"  />--%>
                </asp:GridView>

                <table cellspacing="0" cellpadding="6" style="text-align: center; font-family: Segoe UI; font-size: 11px">
                    <tr style="background-color: #2D7BBD; color: white">
                        <td align="left" style="width: 180px; font-weight: bold; border: 1px solid #000000;">TOTAL OBRAS DE SANEAMIENTO
                        </td>
                        <td align="center" style="width: 100px; font-weight: bold; border: 1px solid #000000;">PNSU</td>
                        <td align="center" style="width: 100px; font-weight: bold; border: 1px solid #000000;">PNSR</td>
                        <td align="center" style="width: 100px; font-weight: bold; border: 1px solid #000000;">TOTAL</td>
                    </tr>
                    <tr>
                        <td align="left" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">Cantidad de Proyectos</td>
                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                            <asp:LinkButton ID="LbPnsuT" runat="server" OnClick="LbPnsuT_Click">
                                <asp:Label ID="lblNroPNSU" runat="server" Text="0"></asp:Label>
                            </asp:LinkButton>
                            
                        </td>
                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                               <asp:LinkButton ID="LblPnsrT" runat="server" OnClick="LblPnsrT_Click">
                                <asp:Label ID="lblNroPNSR" runat="server" Text="0"></asp:Label>
                            </asp:LinkButton>
                            
                        </td>
                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                 <asp:LinkButton ID="LblTotT" runat="server" OnClick="LblTotT_Click">
                                <asp:Label ID="lblNroTOTAL" runat="server" Text="0"></asp:Label>
                            </asp:LinkButton>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 180px; border: 1px solid #000000">Costo del Proyecto (S/)</td>
                        <td style="width: 100px; border: 1px solid #000000">
                            <asp:Label ID="lblTotalCostoPNSU" runat="server" Text="0"></asp:Label>
                        </td>
                        <td style="width: 100px; border: 1px solid #000000">
                            <asp:Label ID="lblTotalCostoPNSR" runat="server" Text="0"></asp:Label>
                        </td>
                        <td style="width: 100px; border: 1px solid #000000">
                            <asp:Label ID="lblTotalCostoTOTAL" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">Inversión del MVCS (S/)</td>
                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                            <asp:Label ID="lblTotalInverPNSU" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                            <asp:Label ID="lblTotalInverPNSR" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center" style="border-right: 1px solid #000000; border-left: 1px solid #000000;">
                            <asp:Label ID="lblTotalInverTOTAL" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border: 1px solid #000000;">Beneficiarios</td>
                        <td align="center" style="border: 1px solid #000000;">
                            <asp:Label ID="lblTotalBenePNSU" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center" style="border: 1px solid #000000;">
                            <asp:Label ID="lblTotalBenePNSR" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center" style="border: 1px solid #000000;">
                            <asp:Label ID="lblTotalBeneTOTAL" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>

        </asp:UpdatePanel>
        <br />
        <div>
            <table>
                <tr>
                    <td style="font-size:12px">Guia de generación de reporte:</td>
                    <td> 
            <a href="../Documentos/GUIA_REPORTE_CONSOLIDADO.pdf" target="_blank" >
                <img src="../img/Word-doc.ico" width="36" title="Guia" />
             </a></td>
                </tr>
            </table>
           
        </div>
    </center>

    <%-- DETALLE DE PROYECTOS--%>
    <asp:LinkButton ID="lnkbtnDetalle" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="MPE_Detalle" runat="server" TargetControlID="lnkbtnDetalle"
        BackgroundCssClass="modalBackground" OkControlID="imgbtnCerrarDevolucion" DropShadow="true"
        PopupControlID="Panel_Detalle">
    </asp:ModalPopupExtender>

    <asp:Panel ID="Panel_Detalle" runat="server" Visible="true" Height="700px" Width="900px"
        CssClass="modalPopup">

        <table width="100%" style="background-color: #E2ECF3">
            <tr>
                <td align="center" class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <asp:UpdatePanel runat="server" ID="Up_TituloDetalle">
                          <ContentTemplate>
                              <asp:Label ID="lblTituloDetalle" CssClass="titulo2" runat="server" Font-Bold="true"></asp:Label>
                              <br />
                              <br />
                              <asp:Label runat="server" ID="LblTotalRegistros"> </asp:Label>
                              <asp:HiddenField runat="server" ID="Hfest" />
                          </ContentTemplate>
                      </asp:UpdatePanel>
                </td>
                <td align="right" style="width: 26px">
                    <asp:ImageButton ID="imgbtnCerrarDevolucion" runat="server" AlternateText="Cerrar ventana"
                        ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"
                        Height="25px" Width="26px" />
                </td>
            </tr>
            <tr>
                <td align="center" class="style1">
                    <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_Click" class="btn btn-verde">
                                                                    <i class="fa fa-file-excel-o"></i>  Exportar
                    </asp:LinkButton>

                </td>
                <td align="right" style="width: 26px">&nbsp;</td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="560px" Width="890px">
            <asp:UpdatePanel runat="server" ID="Up_Detalle">
                <ContentTemplate>
                    <asp:GridView
                        ID="grdDetalle"
                        runat="server"
                        Width="870px"
                        AllowPaging="True"
                        AutoGenerateColumns="False"
                        BorderWidth="1px"
                        CssClass="Grid"
                        EmptyDataText="No hay proyectos."
                        EditRowStyle-BackColor="#ffffb7"
                        PageSize="2000">
                        <Columns>
                            <asp:TemplateField HeaderText="PROGRAMA">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstados" Text='<%# Eval("Programa") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle Height="45px" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SNIP" HeaderStyle-BorderColor="Black">
                                <ItemTemplate>
                                    <asp:Label ID="lblNroProyectos" Text='<%# Eval("cod_snip") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOMBRE DE PROYECTO" HeaderStyle-BorderColor="Black">
                                <ItemTemplate>
                                    <asp:Label ID="lblNomProyecto" Text='<%# Eval("nom_proyecto") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="BENEFICIARIOS" HeaderStyle-BorderColor="Black">
                                <ItemTemplate>
                                    <asp:Label ID="lblPoblacion" Text='<%# Convert.ToInt32(Eval("num_hab_snip")).ToString("N0") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                            </asp:TemplateField>

                                <asp:TemplateField HeaderText="COSTO DEL PROYECTO (S/.)">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalInversion" Text='<%# Convert.ToDouble(Eval("monto_inversion")).ToString("N2") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="INVERSIÓN DEL MVCS (S/.)">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" Text='<%# Convert.ToDouble(Eval("CostoMVCS")).ToString("N2") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA TERMINO REAL" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaFin" Text='<%# Eval("fechaFin") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA INICIO" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaInicio" Text='<%# Eval("fechaInicio") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA FIN ÚLTIMA PARALIZACIÓN" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaFinParalizacion" Text='<%# Eval("FechaFinParalizacion") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA ÚLTIMA CONVOCATORIA" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaConvocatoria" Text='<%# Eval("FechaConvocatoria") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ESTADO">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" Text='<%# Eval("EstadoMVCS") %>' runat="Server" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderColor="Black" />
                            </asp:TemplateField>

                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                        <FooterStyle HorizontalAlign="Center" />
                        <HeaderStyle Height="15px" BackColor="#2D7BBD" CssClass="GridHeader3" BorderColor="Black" />
                        <%-- <AlternatingRowStyle CssClass="GridAlternating" />--%>
                        <RowStyle CssClass="GridRowNormal" Height="28px" />



                    </asp:GridView>


                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>


</asp:Content>


