﻿<%@ Page Title="Núcleo Ejecutor" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="Reporte_NE.aspx.cs" Inherits="Web.reportes.Reporte_NE" Culture="es-PE" UICulture="es-PE" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        @font-face {
            font-family: 'DINB';
            SRC: url('DINB.ttf');
        }

        .tablaRegistro {
            line-height: 15px;
        }

        .titulo {
            font-size: 15pt;
            font-family: Calibri;
            color: #0094D4;
            line-height: 20px;
            font-weight: bold;
        }

        .tituloTipo {
            font-size: 13pt;
            font-family: 'DINB';
            color: #000000;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" >
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.svg" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; ">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />


    <div class="Grid">
        <br />
        <table style="margin:auto">
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Tipo de Reporte:" CssClass="titulo"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoReporte" runat="server" Font-Size="12px" Width="500px">
                    </asp:DropDownList>

               
                </td>
            </tr>
        </table>

        <br />
        <br />

        <asp:UpdatePanel runat="server" ID="Up_Filtro" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="width: 100%; margin: auto; text-align: center">
                    <table style="width: 55%; margin: auto; border-collapse: separate !important; border-spacing: 4px;" class="tablaRegistro">
                        <tr>
                            <td align="center" class="formHeader">
                                <%--<b>AÑO</b>--%>
                            </td>
                            <td align="center" class="formHeader">
                                SNIP
                            </td>
                            <td align="center" class="formHeader">
                                PROYECTO
                            </td>
                            <td align="center" class="formHeader">
                                DEPARTAMENTO
                            </td>
                            <td align="center" class="formHeader" runat="server" id="tdProvincia" visible="false">
                                PROVINCIA
                            </td>
                            <td align="center" class="formHeader" runat="server" id="tdDistrito" visible="false">
                               DISTRITO
                            </td>
                            <td align="center" class="formHeader">
                                PROGRAMA
                            </td>
                            <td align="center" class="formHeader">
                                TÉCNICO
                            </td>
                            <td align="center" class="formHeader">
                                TIPO FINANCIAMIENTO</td>
                            <td align="center" class="formHeader">
                                <%--<b>MODALIDAD FINANCIAMIENTO</b>--%>

                            </td>
                            <td align="center" class="formHeader">
                                <%-- <b>ESTADO</b>--%>
                            </td>
                            <td align="center" class="formHeader">
                                <%-- <b>ESTRATEGIA</b>--%>

                            </td>

                            <td align="center" class="formHeader">
                                <%-- <b>CMD</b>--%>

                            </td>

                            <td style="width: 40px" rowspan="2"></td>

                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtanno" runat="server" Width="30px" onkeypress="return esInteger(event)" placeholder="yyyy" Visible="false"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtSnipFiltro" runat="server" Width="60px"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1001" runat="server" TargetControlID="txtSnipFiltro" FilterType="Numbers" Enabled="True" />

                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtProyectoFiltro" runat="server" Width="160px"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlDepa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepa_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center" runat="server" id="tdProvincia2" visible="false">
                                <asp:DropDownList ID="ddlprov" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlprov_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center" runat="server" id="tdDistrito2" visible="false">
                                <asp:DropDownList ID="ddldist" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlSubsector" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubsector_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlTecnico" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlTipo" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlModalidad" runat="server" Visible="false">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEstado" runat="server" Visible="false">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEstrategia" runat="server" Width="200px" Visible="false">
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chbFlagCMD" runat="server" AutoPostBack="true" Visible="false" />
                            </td>

                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnReporteResumen" runat="server" OnClick="btnReporteResumen_OnClick" CssClass="btn btn-verde"><i class="fa fa-file-excel-o"></i> Exportar</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnReporteResumen" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />

</asp:Content>

