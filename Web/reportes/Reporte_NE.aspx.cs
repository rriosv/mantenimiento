﻿using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Business;
using Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.reportes
{
    public partial class Reporte_NE : System.Web.UI.Page
    {

        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();

        BE_MON_PROCESO _BEProceso = new BE_MON_PROCESO();
        BL_MON_Proceso objBLProceso = new BL_MON_Proceso();

        BLUtil objUtil = new BLUtil();

        public void Grabar_Log()
        {
            BL_LOG _objBLLog = new BL_LOG();
            BE_LOG _BELog = new BE_LOG();

            string ip;
            string hostName;
            string pagina;
            string tipoDispositivo = "";
            string agente = "";
            try
            {
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception ex)
            {
                ip = "";
            }

            try
            {
                hostName = (Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
            }
            catch (Exception ex)
            {
                hostName = "";
            }

            try
            {
                pagina = HttpContext.Current.Request.Url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                pagina = "";
            }

            try
            {
                string uAg = Request.ServerVariables["HTTP_USER_AGENT"];
                agente = uAg;
                Regex regEx = new Regex(@"android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase);
                bool isMobile = regEx.IsMatch(uAg);
                if (isMobile)
                {
                    tipoDispositivo = "Movil";
                }
                else if (Request.Browser.IsMobileDevice)
                {
                    tipoDispositivo = "Movil";
                }
                else
                {
                    tipoDispositivo = "PC";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }

            _BELog.Id_usuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            _BELog.Ip = ip;
            _BELog.HostName = hostName;
            _BELog.Pagina = pagina;
            _BELog.Agente = agente;
            _BELog.TipoDispositivo = tipoDispositivo;

            int val = _objBLLog.spi_Log(_BELog);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Grabar_Log();

                string usuario = Session["IdUsuario"].ToString();

                _BE_Bandeja.id_usuario = Convert.ToInt32(usuario);
                _BE_Bandeja.tipo = "1";

                BE_MON_BANDEJA _ENTBandeja = new BE_MON_BANDEJA();
                _ENTBandeja = objBLbandeja.F_spMON_ObtieneInfo(_BE_Bandeja);

                int codSubsector;
                codSubsector = Convert.ToInt32(Session["CodSubsector"].ToString());
                if (codSubsector > 0)
                {
                    cargaSector(ddlSubsector);
                    if (ddlSubsector.SelectedValue == "2" || ddlSubsector.SelectedValue == "3")
                    {
                        ddlSubsector.Enabled = false;
                    }
                    ddlSubsector.SelectedValue = Session["CodSubsector"].ToString();

                }

                cargaTecnico(ddlTecnico);

                if (_ENTBandeja != null)
                {
                    if (_ENTBandeja.nivel == "T")
                    {

                        ddlTecnico.SelectedValue = _ENTBandeja.id_tecnico.ToString();

                        ddlTecnico.Enabled = false;
                    }
                }

                Session["flagMonitor"] = "1";
                cargaUbigeo(ddlDepa, "1", null, null, null);
                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
                cargaSector(ddlSubsector);

                //if (Session["CodSubsector"].ToString() == "2")
                //{

                //    ddlTipoReporte.Items.Insert(0, new ListItem("Obras Desactualizadas", "6"));
                //    ddlTipoReporte.Items.Insert(0, new ListItem("Metas Físicas", "5"));
                //    ddlTipoReporte.Items.Insert(0, new ListItem("Detalle", "3"));
                //    ddlTipoReporte.Items.Insert(0, new ListItem("Resumen", "1"));
                //    ddlTipoReporte.Items.Insert(0, new ListItem("Reporte Paralizados", "13"));
                //    ddlTipoReporte.Items.Insert(0, new ListItem("Seleccionar Reporte", ""));
                //}
                //else
                //{
                //    if (Session["CodSubsector"].ToString() == "1")
                //    {

                //        ddlTipoReporte.Items.Insert(0, new ListItem("Obras Desactualizadas", "6"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PNSU", "2"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Detalle PNSU", "4"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Reporte Paralizados", "13"));

                //        ddlTipoReporte.Items.Insert(0, new ListItem("Detalle Desembolso NE", "10"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Valor Financiado NE", "11"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Seguimiento NE", "12"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Reporte NE", "7"));
                //        ddlTipoReporte.Items.Insert(0, new ListItem("Seleccionar Reporte", ""));
                //    }
                //    else
                //    {
                //        if (Session["CodSubsector"].ToString() == "3")
                //        {

                //            ddlTipoReporte.Items.Insert(0, new ListItem("Seguimiento Transf. PNSU", "8"));
                //            ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PNSR", "9"));

                //            ddlTipoReporte.Items.Insert(0, new ListItem("Detalle Desembolso NE", "10"));
                //            ddlTipoReporte.Items.Insert(0, new ListItem("Valor Financiado NE", "11"));
                //            ddlTipoReporte.Items.Insert(0, new ListItem("Seguimiento NE", "12"));
                //            ddlTipoReporte.Items.Insert(0, new ListItem("Reporte NE - PNSR", "7"));
                //            ddlTipoReporte.Items.Insert(0, new ListItem("Seleccionar Reporte", ""));
                //        }
                //        else
                //        {
                //            if (Session["CodSubsector"].ToString() == "5" || Session["CodSubsector"].ToString() == "6")
                //            {


                //                ddlTipoReporte.Items.Insert(0, new ListItem("Detalle Desembolso NE", "10"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Valor Financiado NE", "11"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Seguimiento NE", "12"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Reporte NE", "7"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Seleccionar Reporte", ""));
                //            }
                //            else
                //            {
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Obras Desactualizadas", "6"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PMIB", "1"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Detalle PMIB", "3"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Metas PMIB", "5"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PNSR", "9"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PNSU", "2"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Detalle PNSU", "4"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Reporte Paralizados", "13"));

                //                ddlTipoReporte.Items.Insert(0, new ListItem("Detalle Desembolso NE", "10"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Valor Financiado NE", "11"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Seguimiento NE", "12"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Reporte NE ", "7"));
                //                ddlTipoReporte.Items.Insert(0, new ListItem("Seleccionar Reporte", ""));
                //            }
                //        }
                //    }
                //}

                ddlTipoReporte.Items.Insert(0, new ListItem("Obras Desactualizadas", "6"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PMIB", "1"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Detalle PMIB", "3"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Metas PMIB", "5"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PNSR", "9"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Resumen PNSU", "2"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Detalle PNSU", "4"));
                //ddlTipoReporte.Items.Insert(0, new ListItem("Reporte Paralizados", "13"));

                ddlTipoReporte.Items.Insert(0, new ListItem("Detalle Desembolso NE", "10"));
                ddlTipoReporte.Items.Insert(0, new ListItem("Valor Financiado NE", "11"));
                ddlTipoReporte.Items.Insert(0, new ListItem("Seguimiento NE", "12"));
                ddlTipoReporte.Items.Insert(0, new ListItem("Reporte NE ", "7"));
                ddlTipoReporte.Items.Insert(0, new ListItem("Seleccionar Reporte", ""));

                cargaTipo();
                cargaEstrategia();
                cargaEstado();
                CargaModalidad();
                //cargaBandejaMonitor(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtanno.Text);
             

            }
        }

        #region FILTRO

        protected void cargaSector(DropDownList ddl)
        {
            List<BE_MON_BANDEJA> ListSector = new List<BE_MON_BANDEJA>();
            ListSector = objBLbandeja.F_spMON_TipoPrograma();
            ddl.DataSource = ListSector;
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("--TODOS--", ""));
        }
        protected void cargaTipo()
        {
            ddlTipo.DataSource = objBLbandeja.F_spMON_TipoFinanciamiento();
            ddlTipo.DataTextField = "nombre";
            ddlTipo.DataValueField = "valor";
            ddlTipo.DataBind();

            ddlTipo.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        protected void cargaEstado()
        {
            ddlEstado.DataSource = objBLbandeja.F_spMON_FiltroEstadoEjecucion();
            ddlEstado.DataTextField = "nombre";
            ddlEstado.DataValueField = "valor";
            ddlEstado.DataBind();

            ddlEstado.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        protected void cargaEstrategia()
        {
            ddlEstrategia.DataSource = objUtil.F_spSOL_Listar_Estrategia();
            ddlEstrategia.DataTextField = "nombre";
            ddlEstrategia.DataValueField = "codigo";
            ddlEstrategia.DataBind();

            ddlEstrategia.SelectedValue = "0";
        }
        protected void cargaTecnico(DropDownList ddl)
        {
            List<BE_MON_BANDEJA> List = new List<BE_MON_BANDEJA>();
            List = objBLbandeja.F_spMON_ListarTecnico(ddlSubsector.SelectedValue);

            List = List.OrderBy(x => x.tecnico).ToList();

            ddl.DataSource = List;
            ddl.DataTextField = "tecnico";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        protected void CargaModalidad()
        {
            ddlModalidad.Items.Clear();

            ddlModalidad.Items.Insert(0, new ListItem("Cooperación Internacional", "2"));
            ddlModalidad.Items.Insert(0, new ListItem("Núcleo Ejecutor", "4"));
            ddlModalidad.Items.Insert(0, new ListItem("Por Contrata", "3"));
            ddlModalidad.Items.Insert(0, new ListItem("Transferencia", "1"));
            ddlModalidad.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        protected void ddlDepa_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepa.SelectedValue != "")
            {
                tdProvincia.Visible = true;
                tdProvincia2.Visible = true;

                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
            }
            else
            {
                tdProvincia.Visible = false;
                tdProvincia2.Visible = false;
                tdDistrito.Visible = false;
                tdDistrito2.Visible = false;
            }
            Up_Filtro.Update();
            //cargaBandeja();
        }
        protected void ddlprov_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlprov.SelectedValue != "")
            {
                tdDistrito.Visible = true;
                tdDistrito2.Visible = true;

                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
            }
            else
            {
                tdDistrito.Visible = false;
                tdDistrito2.Visible = false;
            }
            Up_Filtro.Update();
            //cargaBandeja();
        }
        //protected void ddldist_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //cargaBandeja();
        //}
        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {

            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("--TODOS--", ""));

        }
        //protected void ddlSubsector_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    cargaTecnico(ddlTecnico);
        //    Up_Filtro.Update();
        //    cargaBandeja();
        //}
        protected void btnBuscar_OnClick(object sender, EventArgs e)
        {
            if (txtanno.Text != "")
            {
                if (!(Convert.ToInt32(txtanno.Text) >= 1900 && Convert.ToInt32(txtanno.Text) <= DateTime.Now.Year))
                {
                    string script = "<script>alert('Rango de Año es de 1900 hasta la actualidad');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                    return;
                }
            }
        }
      
        #endregion
    
        protected void btnReporteResumen_OnClick(object sender, EventArgs e)
        {
            if (ddlTipoReporte.SelectedValue == "")
            {
                string script = "<script>alert('Seleccionar tipo de reporte.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }
            else
            {
                if (ddlTipoReporte.SelectedValue == "1")
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.Anio = txtanno.Text;
                    if (chbFlagCMD.Checked == true)
                    {
                        _BE_Bandeja.flagCMD = "1";
                    }
                    else
                    {
                        _BE_Bandeja.flagCMD = "";
                    }

                    DataTable obj = objBLbandeja.spMON_ReporteGeneralPMIB(_BE_Bandeja);
                    //obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_XLS(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "2") // REPORTE GENERAL PNSU
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.sector = Session["CodSubsector"].ToString();
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;

                    DataTable obj = objBLbandeja.spMON_ReporteGeneral(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_ReporteGeneralPNSU(grd2);
     
                }

                if (ddlTipoReporte.SelectedValue == "3") //Detalle PMIB
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.Anio = txtanno.Text;
                    if (chbFlagCMD.Checked == true)
                    {
                        _BE_Bandeja.flagCMD = "1";
                    }
                    else
                    {
                        _BE_Bandeja.flagCMD = "";
                    }

                    DataTable obj = objBLbandeja.spMON_ReporteDetallePMIB(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_DetallePMIB(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "4") // PNSU DETALLE
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    if (chbFlagCMD.Checked == true)
                    {
                        _BE_Bandeja.flagCMD = "1";
                    }
                    else
                    {
                        _BE_Bandeja.flagCMD = "";
                    }
                    DataTable obj = objBLbandeja.spMON_ReporteSuperGeneralPNSU(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");


                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_DetallePNSU(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "5")  // METAS FISICAS PMIB
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    //_BE_Bandeja.sector = ddlSubsector.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.Anio = txtanno.Text;
                    if (chbFlagCMD.Checked == true)
                    {
                        _BE_Bandeja.flagCMD = "1";
                    }
                    else
                    {
                        _BE_Bandeja.flagCMD = "";
                    }

                    DataTable obj = objBLbandeja.spMON_ReporteMetasPMIB(_BE_Bandeja);
                    // obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_MetasPMIB(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "6")  // OBRAS DESACTUALIZADAS
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;
                    _BE_Bandeja.tecnico = ddlTecnico.SelectedValue;

                    List<BE_MON_BANDEJA> ListBandeja = new List<BE_MON_BANDEJA>();
                    ListBandeja = objBLbandeja.F_spMON_ReporteObrasDesactualizadas(_BE_Bandeja);
                    // obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.Columns.Add(new BoundField { DataField = "sector" });
                    grd2.Columns.Add(new BoundField { DataField = "snip" });
                    grd2.Columns.Add(new BoundField { DataField = "depa" });
                    grd2.Columns.Add(new BoundField { DataField = "nombProyecto" });
                    grd2.Columns.Add(new BoundField { DataField = "Estado" });
                    grd2.Columns.Add(new BoundField { DataField = "fecha" });
                    grd2.Columns.Add(new BoundField { DataField = "fisicoEjecutado" });
                    grd2.Columns.Add(new BoundField { DataField = "tecnico" });

                    grd2.AutoGenerateColumns = false;
                    grd2.DataSource = ListBandeja;
                    grd2.DataBind();

                    Exportar_ObrasDesactualizadas(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "7")  //REPORTE NE
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;


                    DataTable obj = objBLbandeja.spMON_ReporteNE(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_NE(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "8") // Seguimiento PNSU
                {
                    DataTable obj = objBLbandeja.spMON_ReporteMonitoreoPNSUparaPNSR();
                    obj = selectDisctinct(obj, "id_proyecto");


                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_ReportePNSUparaPNSR(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "9") //PROYECTOS PNSR
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.sector = "3";
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.Anio = txtanno.Text;
                    if (chbFlagCMD.Checked == true)
                    {
                        _BE_Bandeja.flagCMD = "1";
                    }
                    else
                    {
                        _BE_Bandeja.flagCMD = "";
                    }

                    DataTable obj = objBLbandeja.spMON_ReporteDetallePMIB(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_DetallePMIB(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "10")  //Detalle Desembolso NE
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;


                    DataTable obj = objBLbandeja.spMON_ReporteDetalleDevengadoNE(_BE_Bandeja);
                    //obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_DetalleDevengadoNE(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "11")  //Valor Financiado NE
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;


                    DataTable obj = objBLbandeja.spMON_ReporteValorFinanciadoNE(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_ValorFinanciadoNE(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "12")  //Seguimiento de NE
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;


                    DataTable obj = objBLbandeja.spMON_ReporteSeguimientoNE(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_SeguimientoNE(grd2);

                }

                if (ddlTipoReporte.SelectedValue == "13")  //REPORTE PARALIZADOS
                {
                    _BE_Bandeja.snip = txtSnipFiltro.Text;
                    _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
                    _BE_Bandeja.depa = ddlDepa.SelectedValue;
                    _BE_Bandeja.prov = ddlprov.SelectedValue;
                    _BE_Bandeja.dist = ddldist.SelectedValue;
                    _BE_Bandeja.tecnicofiltro = ddlTecnico.SelectedValue;
                    _BE_Bandeja.tipoFinanciamientoFiltro = ddlTipo.SelectedValue;
                    _BE_Bandeja.sector = ddlSubsector.SelectedValue;
                    _BE_Bandeja.Anio = txtanno.Text;
                    if (chbFlagCMD.Checked == true)
                    {
                        _BE_Bandeja.flagCMD = "1";
                    }
                    else
                    {
                        _BE_Bandeja.flagCMD = "";
                    }

                    DataTable obj = objBLbandeja.spMON_ReporteParalizados(_BE_Bandeja);
                    obj = selectDisctinct(obj, "id_proyecto");
                    GridView grd2 = new GridView();
                    grd2.DataSource = obj;
                    grd2.DataBind();

                    Exportar_Paralizados(grd2);

                }

            }

            //List<BE_MON_BANDEJA> ListBandejaMonitor = new List<BE_MON_BANDEJA>();

            //ListBandejaMonitor = (List<BE_MON_BANDEJA>)Session["Monitoreo"];

            //grdBandeja.DataSource = ListBandejaMonitor;
            //grdBandeja.DataBind();
            //LabeltotalAsignado.Text = "Total Asignados: " + ListBandejaMonitor.Count;
        }

        public static DataTable selectDisctinct(DataTable dt, string columnName)
        {
            try
            {
                if (columnName == null || columnName.Length == 0)
                    throw new ArgumentNullException(columnName, "El parámetro no puede ser nulo");
                DataTable distintos = dt.DefaultView.ToTable(true, columnName);
                DataTable aux = new DataTable();
                foreach (DataColumn dc in dt.Columns)
                    aux.Columns.Add(new DataColumn(dc.Caption, dc.DataType));
                foreach (DataRow dr in distintos.Rows)
                {
                    aux.ImportRow(dt.Select(columnName + " = '" + dr[0] + "'")[0]);
                }
                return aux;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        protected void Exportar_XLS(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Resumen_Monitor_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 1; i < 22; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                }
                for (int i = 22; i < 40; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }



                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                //grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[1].Text = "Subsector";
                grd.HeaderRow.Cells[2].Text = "SNIP";
                grd.HeaderRow.Cells[3].Text = "GESTION";
                grd.HeaderRow.Cells[4].Text = "VRAEM";
                grd.HeaderRow.Cells[5].Text = "FRONTERIZO";
                grd.HeaderRow.Cells[6].Text = "EMERGENCIA TERREMOTO 2007";
                grd.HeaderRow.Cells[6].Width = 100;
                //grd.HeaderRow.Cells[6].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[7].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "PROVINCIA";
                grd.HeaderRow.Cells[8].Width = 150;
                grd.HeaderRow.Cells[9].Text = "DISTRITO";
                grd.HeaderRow.Cells[9].Width = 150;
                grd.HeaderRow.Cells[10].Text = "TIPO FINANCIAMIENTO";
                grd.HeaderRow.Cells[10].Width = 120;
                grd.HeaderRow.Cells[11].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[11].Width = 500;
                grd.HeaderRow.Cells[12].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[13].Text = "MONTO DEL REGISTRO FASE DE INVERSIÓN S/.";
                grd.HeaderRow.Cells[13].Width = 100;
                grd.HeaderRow.Cells[14].Text = "MONTO ACTUALIZO PIP";
                grd.HeaderRow.Cells[14].Width = 100;
                grd.HeaderRow.Cells[15].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[16].Text = "NRO CONVENIO";
                grd.HeaderRow.Cells[17].Text = "FECHA CONVENIO";
                grd.HeaderRow.Cells[18].Text = "AÑO Q SE FINANCIO X PRIMERA VEZ";
                grd.HeaderRow.Cells[18].Width = 100;
                grd.HeaderRow.Cells[19].Text = "N° D.S.";
                grd.HeaderRow.Cells[20].Text = "FECHA D.S.";

                grd.HeaderRow.Cells[21].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[22].Text = "FECHA INICO";
                ////grd.HeaderRow.Cells[19].Width = 150;
                grd.HeaderRow.Cells[23].Text = "PLAZO DE EJECUCION (DIAS)";
                grd.HeaderRow.Cells[23].Width = 80;
                grd.HeaderRow.Cells[24].Text = "TERMINO CONTRACTUAL";
                grd.HeaderRow.Cells[25].Text = "AVANCE FISICO REAL SUSTENTADO";
                grd.HeaderRow.Cells[26].Text = "FECHA VISITA";
                grd.HeaderRow.Cells[27].Text = "AVANCE FISICO PROGRAMADO";
                grd.HeaderRow.Cells[28].Text = "AVANCE FISICO ACUMULADO DE ACUERDO A MONITOREO";
                grd.HeaderRow.Cells[28].Width = 100;
                grd.HeaderRow.Cells[29].Text = "ESTADO DE EJECUCION";
                grd.HeaderRow.Cells[30].Text = "SITUACION DE OBRA";
                grd.HeaderRow.Cells[30].Width = 300;


                grd.HeaderRow.Cells[31].Text = "FECHA VALORIZACION";
                grd.HeaderRow.Cells[32].Text = "MONTO TRANSFERIDO";
                grd.HeaderRow.Cells[33].Text = "% DE TRANSFERENCIA";
                grd.HeaderRow.Cells[34].Text = "MODALIDAD DE EJECUCION";
                grd.HeaderRow.Cells[35].Text = "SISTEMA DE CONTRATACION";
                grd.HeaderRow.Cells[36].Text = "SOSTENIBILIDAD";
                //grd.HeaderRow.Cells[22].Width = 160;
                grd.HeaderRow.Cells[37].Text = "RECOMENDACIONES DE VISITA";

                grd.HeaderRow.Cells[38].Text = "MONITOR";
                grd.HeaderRow.Cells[39].Text = "AMBITO";
                //grd.HeaderRow.Cells[26].Text = "DISTRITO FRONTERIZO";
                //grd.HeaderRow.Cells[27].Text = "ZONA HUALLAGA";
                //grd.HeaderRow.Cells[28].Text = "ZONA HELADA";
                //grd.HeaderRow.Cells[29].Text = "CUENCA RAMIS";
                //grd.HeaderRow.Cells[30].Text = "LISTA";
                //grd.HeaderRow.Cells[31].Text = "SUB-LISTA";
                //grd.HeaderRow.Cells[32].Text = "REVISOR";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    row.Cells[0].Visible = false;

                    for (int j = 0; j < 40; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "RESUMEN DE PROYECTOS FINANCIADOS";
                myTable.ColumnSpan = 28;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                
                //myTable = new TableCell();
                //myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                //myTable.ColumnSpan = 4;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "AMBITO DE INTERVENCION";
                //myTable.ColumnSpan = 6;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                
                //myTable = new TableCell();
                //myTable.Text = "INFORMACIÓN ADICIONAL";
                //myTable.ColumnSpan = 2;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_DetallePMIB(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Detalle_Monitor_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;
                grd.HeaderRow.Font.Size = 11;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 12; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                }
                for (int i = 12; i < 40; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }



                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[0].Width = 80;
                grd.HeaderRow.Cells[1].Text = "GESTION";
                grd.HeaderRow.Cells[2].Text = "PROGRAMA";
                grd.HeaderRow.Cells[3].Text = "SNIP";
                grd.HeaderRow.Cells[4].Text = "TIPO FINANCIAMIENTO";
                grd.HeaderRow.Cells[5].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[5].Width = 150;
                grd.HeaderRow.Cells[6].Text = "PROVINCIA";
                grd.HeaderRow.Cells[6].Width = 150;
                grd.HeaderRow.Cells[7].Text = "DISTRITO";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[8].Width = 500;
                grd.HeaderRow.Cells[9].Text = "N° D.S.";
                grd.HeaderRow.Cells[10].Text = "N° CONVENIO";
                grd.HeaderRow.Cells[11].Text = "MONTO TRANSFERIDO";
                grd.HeaderRow.Cells[12].Text = "FECHA CONVOCATORIA";
                grd.HeaderRow.Cells[12].Width = 100;
                grd.HeaderRow.Cells[13].Text = "FECHA BUENA PRO";
                grd.HeaderRow.Cells[13].Width = 80;
                grd.HeaderRow.Cells[14].Text = "MONTO CONTRATADO OBRA";
                grd.HeaderRow.Cells[15].Text = "MONTO CONTRATADO SUPERVISION";
                grd.HeaderRow.Cells[16].Text = "AVANCE FISICO PROGRAMADO";
                grd.HeaderRow.Cells[16].Width = 100;
                grd.HeaderRow.Cells[17].Text = "AVANCE FISICO REAL";
                grd.HeaderRow.Cells[17].Width = 80;
                grd.HeaderRow.Cells[18].Text = "AVANCE FINANCIERO PROGRAMADO";
                grd.HeaderRow.Cells[18].Width = 100;
                grd.HeaderRow.Cells[19].Text = "AVANCE FINANCIERO REAL";
                grd.HeaderRow.Cells[19].Width = 90;
                grd.HeaderRow.Cells[20].Text = "ESTADO SITUACIONAL";
                grd.HeaderRow.Cells[21].Text = "ESTADO";
                grd.HeaderRow.Cells[22].Text = "SUB ESTADO";
                grd.HeaderRow.Cells[23].Text = "SUB ESTADO 2";
                grd.HeaderRow.Cells[24].Text = "OBSERVACION Y RECOMENDACIONES";
                grd.HeaderRow.Cells[25].Text = "FECHA ENTREGA TERRENO";
                grd.HeaderRow.Cells[25].Width = 80;
                grd.HeaderRow.Cells[26].Text = "FECHA INICO";
                grd.HeaderRow.Cells[27].Text = "PLAZO DE EJECUCION (DIAS)";
                grd.HeaderRow.Cells[27].Width = 80;
                grd.HeaderRow.Cells[28].Text = "TERMINO CONTRACTUAL";
                grd.HeaderRow.Cells[29].Text = "TOTAL AMPLIACION (DIAS)";
                grd.HeaderRow.Cells[29].Width = 80;
                grd.HeaderRow.Cells[30].Text = "TERMINO REAL";
                grd.HeaderRow.Cells[31].Text = "FECHA RECEPCION OBRA";
                grd.HeaderRow.Cells[31].Width = 80;

                grd.HeaderRow.Cells[32].Text = "TIPO MODALIDAD OBRA";
                grd.HeaderRow.Cells[33].Text = "TIPO EMPRESA";
                grd.HeaderRow.Cells[34].Text = "CONTRATISTA/CONSORCIO";
                grd.HeaderRow.Cells[35].Text = "INTEGRANTES CONSORCIO";
                grd.HeaderRow.Cells[35].Width = 400;
                grd.HeaderRow.Cells[36].Text = "SUPERVISOR";
                grd.HeaderRow.Cells[37].Text = "RESIDENTE";

                grd.HeaderRow.Cells[38].Text = "FECHA ULT. MONITOREO";
                grd.HeaderRow.Cells[39].Text = "MONITOR";

                //grd.HeaderRow.Cells[26].Text = "DISTRITO FRONTERIZO";
                //grd.HeaderRow.Cells[27].Text = "ZONA HUALLAGA";
                //grd.HeaderRow.Cells[28].Text = "ZONA HELADA";
                //grd.HeaderRow.Cells[29].Text = "CUENCA RAMIS";
                //grd.HeaderRow.Cells[30].Text = "LISTA";
                //grd.HeaderRow.Cells[31].Text = "SUB-LISTA";
                //grd.HeaderRow.Cells[32].Text = "REVISOR";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    //row.Cells[0].Visible = false;

                    for (int j = 0; j < 40; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "DETALLES DE PROYECTOS FINANCIADOS";
                myTable.ColumnSpan = 40;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                //myTable = new TableCell();
                //myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                //myTable.ColumnSpan = 4;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "AMBITO DE INTERVENCION";
                //myTable.ColumnSpan = 6;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);



                //myTable = new TableCell();
                //myTable.Text = "INFORMACIÓN ADICIONAL";
                //myTable.ColumnSpan = 2;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_DetallePNSU(GridView grd)
        {
            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Detalle_Monitor_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;
                grd.HeaderRow.Cells[1].Visible = false;

                for (int i = 0; i < 64; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 10;
                }


                //for (int i = 11; i < 34; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //}



                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[1].Text = "N°";
                grd.HeaderRow.Cells[2].Text = "PROGRAMA";
                grd.HeaderRow.Cells[3].Text = "AÑO";
                grd.HeaderRow.Cells[4].Text = "SNIP";
                grd.HeaderRow.Cells[5].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[5].Width = 500;
                grd.HeaderRow.Cells[6].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[6].Width = 400;
                grd.HeaderRow.Cells[7].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "PROVINCIA";
                grd.HeaderRow.Cells[8].Width = 150;
                grd.HeaderRow.Cells[9].Text = "DISTRITO";
                grd.HeaderRow.Cells[9].Width = 150;

                grd.HeaderRow.Cells[10].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[11].Text = "FECHA VIABILIDAD";
                grd.HeaderRow.Cells[12].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[13].Text = "MONTO FASE INVERSION";
                grd.HeaderRow.Cells[13].Width = 100;
                grd.HeaderRow.Cells[14].Text = "MONTO ACTUALIZADO PIP (SOSEM)";
                grd.HeaderRow.Cells[14].Width = 100;
                grd.HeaderRow.Cells[15].Text = "MONTO CONVENIO";
                grd.HeaderRow.Cells[16].Text = "MONTO TRANSFERIDO ACUMULADO SEGÚN DECRETOS ";
                grd.HeaderRow.Cells[16].Width = 100;
                grd.HeaderRow.Cells[17].Text = "MONTO TRANSFERIDO REGISTRADO EN EL SSP";
                grd.HeaderRow.Cells[17].Width = 100;
                grd.HeaderRow.Cells[18].Text = "MODALIDAD FINANCIAMIENTO";
                grd.HeaderRow.Cells[19].Text = "TIPO EJECUCIÓN";
                grd.HeaderRow.Cells[20].Text = "TIPO DE PROYECTO";
                grd.HeaderRow.Cells[21].Text = "AMBITO";

                grd.HeaderRow.Cells[22].Text = "MONTO REFERENCIAL";
                grd.HeaderRow.Cells[23].Text = "FECHA CONVOCATORIA";
                grd.HeaderRow.Cells[24].Text = "BUENA PRO";
                grd.HeaderRow.Cells[25].Text = "FECHA BUENA PRO CONSENTIDA";
                grd.HeaderRow.Cells[26].Text = "TIPO EMPRESA";
                grd.HeaderRow.Cells[26].Width = 80;
                grd.HeaderRow.Cells[27].Text = "CONSORCIO/CONTRATISTA";
                grd.HeaderRow.Cells[28].Text = "MONTO CONTRATADO";
                grd.HeaderRow.Cells[28].Width = 100;
                grd.HeaderRow.Cells[29].Text = "FECHA FIRMA CONTRATO";
                grd.HeaderRow.Cells[29].Width = 80;

                grd.HeaderRow.Cells[30].Text = "MONTO REFERENCIAL";
                grd.HeaderRow.Cells[31].Text = "FECHA CONVOCATORIA";
                grd.HeaderRow.Cells[32].Text = "BUENA PRO"; ;
                grd.HeaderRow.Cells[33].Text = "FECHA BUENA PRO CONSENTIDA";
                grd.HeaderRow.Cells[34].Text = "TIPO EMPRESA";
                grd.HeaderRow.Cells[34].Width = 80;
                grd.HeaderRow.Cells[35].Text = "CONSORCIO/CONTRATISTA";
                grd.HeaderRow.Cells[36].Text = "MONTO CONTRATADO";
                grd.HeaderRow.Cells[36].Width = 100;
                grd.HeaderRow.Cells[37].Text = "FECHA FIRMA CONTRATO";
                grd.HeaderRow.Cells[37].Width = 80;

                grd.HeaderRow.Cells[38].Text = "FECHA ENTREGA TERRENO";
                grd.HeaderRow.Cells[38].Width = 80;
                grd.HeaderRow.Cells[39].Text = "MONTO";
                grd.HeaderRow.Cells[40].Text = "FECHA";
                grd.HeaderRow.Cells[41].Text = "MONTO";
                grd.HeaderRow.Cells[42].Text = "FECHA";
                grd.HeaderRow.Cells[43].Text = "RESIDENTE";
                grd.HeaderRow.Cells[44].Text = "SUPERVISOR";
                grd.HeaderRow.Cells[45].Text = "NOMBRE";
                grd.HeaderRow.Cells[46].Text = "TELÉFONO";
                grd.HeaderRow.Cells[47].Text = "CORREO";
                grd.HeaderRow.Cells[48].Text = "FECHA INICIO DE OBRA ";
                grd.HeaderRow.Cells[48].Width = 80;
                grd.HeaderRow.Cells[49].Text = "PLAZO CONTRACTUAL";
                grd.HeaderRow.Cells[50].Text = "TERMINO DE PLAZO CONTRACTUAL";
                grd.HeaderRow.Cells[50].Width = 100;
                grd.HeaderRow.Cells[51].Text = "% AVANCE DE OBRA";
                grd.HeaderRow.Cells[52].Text = "ESTADO SITUACIONAL";

                grd.HeaderRow.Cells[53].Text = "ESTADO";
                grd.HeaderRow.Cells[54].Text = "SUB ESTADO";
                grd.HeaderRow.Cells[55].Text = "SUB ESTADO 2";

                grd.HeaderRow.Cells[56].Text = "FECHA TERMINO CONTRACTUAL + AMPL. + PARALIZ.";
                grd.HeaderRow.Cells[56].Width = 80;
                grd.HeaderRow.Cells[57].Text = "FECHA REAL TERMINO OBRA";
                grd.HeaderRow.Cells[57].Width = 80;
                grd.HeaderRow.Cells[58].Text = "FECHA RECEPCION OBRA";
                grd.HeaderRow.Cells[58].Width = 80;
                grd.HeaderRow.Cells[59].Text = "RESOLUCIÓN DE LIQUIDACIÓN";
                grd.HeaderRow.Cells[60].Text = "DETALLE SITUACIONAL";
                grd.HeaderRow.Cells[60].Width = 800;
                grd.HeaderRow.Cells[61].Text = "ACCIONES";
                grd.HeaderRow.Cells[62].Text = "FECHA ACTUALIZACIÓN";
                grd.HeaderRow.Cells[63].Text = "FECHA ULT. CAMBIO EST.";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    //row.Cells[0].Visible = false;
                    row.Cells[1].Visible = false;

                    for (int j = 0; j < 63; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 9;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "DATOS GENERALES";
                myTable.ColumnSpan = 11;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                myTable = new TableCell();
                myTable.Text = "  ";
                myTable.ColumnSpan = 10;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PROCESO DE SELECCIÓN PERFIL/EXPEDIENTE T./OBRA";
                myTable.ColumnSpan = 8;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PROCESO DE SELECCIÓN SUPERVISION PERFIL/EXPEDIENTE T./OBRA";
                myTable.ColumnSpan = 8;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADELANTO DIRECTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADELANTO MATERIALES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "RESIDENTE";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "SUPERVISOR";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "COORDINADOR UE";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "ESTADO DE EJECUCION";
                myTable.ColumnSpan = 11;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "OBSERVACIONES Y RECOMENDACIONES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_ReporteGeneralPNSU(GridView grd)
        {
            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                //string filename = "Resumen_PNSU.xls";
                string filename = "Resumen_PNSU_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;
                //grd.HeaderRow.Cells[1].Visible = false;

                for (int i = 0; i < 37; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                }
                //for (int i = 11; i < 34; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //}



                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "SNIP";
                grd.HeaderRow.Cells[3].Text = "NOMBRE DE PROYECTO";
                grd.HeaderRow.Cells[3].Width = 500;
                grd.HeaderRow.Cells[4].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[5].Text = "PROVINCIA";
                grd.HeaderRow.Cells[6].Text = "DISTRITO";
                grd.HeaderRow.Cells[7].Text = "F. CONTRACTUAL INICIO OBRA";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "CULMINACION PROGRAMADA";
                grd.HeaderRow.Cells[9].Text = "AVANCE DE OBRA";
                grd.HeaderRow.Cells[10].Text = "FECHA VISITA";
                grd.HeaderRow.Cells[11].Text = "PROGRAMADO %";
                grd.HeaderRow.Cells[12].Text = "REAL %";
                grd.HeaderRow.Cells[13].Text = "ESTADO DE OBRA";
                grd.HeaderRow.Cells[14].Text = "SITUACIÓN DE OBRA";
                grd.HeaderRow.Cells[14].Width = 500;
                grd.HeaderRow.Cells[15].Text = "EIA";
                grd.HeaderRow.Cells[16].Text = "San.Fisico";
                grd.HeaderRow.Cells[17].Text = "Fuente Agua";
                grd.HeaderRow.Cells[18].Text = "Vert. Agua. R";
                grd.HeaderRow.Cells[19].Text = "Infobras";
                grd.HeaderRow.Cells[20].Text = "Fecha Valorización";
                grd.HeaderRow.Cells[21].Text = "Monto (S/.)";
                grd.HeaderRow.Cells[22].Text = "%";
                grd.HeaderRow.Cells[23].Text = "Modalidad  de Ejecución";
                grd.HeaderRow.Cells[23].Width = 120;
                grd.HeaderRow.Cells[24].Text = "Sostenibilidad de Mantenimiento de la Obra";
                grd.HeaderRow.Cells[25].Text = "Recomendación";
                grd.HeaderRow.Cells[26].Text = "Meta";
                grd.HeaderRow.Cells[27].Text = "Avance %";
                grd.HeaderRow.Cells[28].Text = "Meta";
                grd.HeaderRow.Cells[29].Text = "Avance %"; ;
                grd.HeaderRow.Cells[30].Text = "Meta";
                grd.HeaderRow.Cells[31].Text = "Avance %";
                grd.HeaderRow.Cells[32].Text = "Meta";
                grd.HeaderRow.Cells[33].Text = "Avance %";
                grd.HeaderRow.Cells[34].Text = "POBLACIÓN";
                grd.HeaderRow.Cells[35].Text = "AMBITO";
                grd.HeaderRow.Cells[36].Text = "TÉCNICO";

                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];

                    //row.Cells[0].Visible = false;
                    //row.Cells[1].Visible = false;

                    for (int j = 0; j < 37; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico

                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable = new TableCell();
                myTable.Text = "RESUMEN DE OBRAS CON VISITAS DE MONITOREO";
                myTable.ColumnSpan = 37;
                //myTable.RowSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FABF8F", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 14;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 7;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "FECHA CONTRACTUAL";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ULTIMA VISITA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AVANCE";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CERTIFICACIÓN Y AUTORIZACIÓN";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "TRANSFERIDO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "AGUA NUEVA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "ALCANTARILLADO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable = new TableCell();
                myTable.Text = "AGUA REHAB.";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);
                myTable = new TableCell();

                myTable.Text = "Alcantarillado Rehab.";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDECB3", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(1, gvHeaderRow);



                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_ReportePNSUparaPNSR(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Seguimiento_Transferencia_PNSU.xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                for (int i = 0; i < 57; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                }
                //for (int i = 11; i < 34; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //}



                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                grd.HeaderRow.Cells[0].Text = "ID _PROYECTO";
                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "AÑO";
                grd.HeaderRow.Cells[3].Text = "SNIP";
                grd.HeaderRow.Cells[4].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[4].Width = 500;
                grd.HeaderRow.Cells[5].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[5].Width = 400;
                grd.HeaderRow.Cells[6].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[6].Width = 150;
                grd.HeaderRow.Cells[7].Text = "PROVINCIA";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "DISTRITO";
                grd.HeaderRow.Cells[8].Width = 150;

                grd.HeaderRow.Cells[9].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[10].Text = "FECHA VIABILIDAD";
                grd.HeaderRow.Cells[11].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[12].Text = "MONTO FASE INVERSION";
                grd.HeaderRow.Cells[13].Text = "MONTO CONVENIO";
                grd.HeaderRow.Cells[14].Text = "MONTO TOTAL TRANSFERIDO";
                grd.HeaderRow.Cells[15].Text = "DETALLE TRANSFERENCIA";
                grd.HeaderRow.Cells[16].Text = "TIPO EJECUCIÓN";
                grd.HeaderRow.Cells[17].Text = "TIPO DE PROYECTO";
                grd.HeaderRow.Cells[18].Text = "AMBITO";

                grd.HeaderRow.Cells[19].Text = "MONTO REFERENCIAL";
                grd.HeaderRow.Cells[20].Text = "FECHA CONVOCATORIA";
                grd.HeaderRow.Cells[21].Text = "BUENA PRO";
                grd.HeaderRow.Cells[22].Text = "FECHA BUENA PRO CONSENTIDA";
                grd.HeaderRow.Cells[23].Text = "TIPO EMPRESA";
                grd.HeaderRow.Cells[23].Width = 80;
                grd.HeaderRow.Cells[24].Text = "CONSORCIO/CONTRATISTA";
                grd.HeaderRow.Cells[25].Text = "MONTO CONTRATADO";
                grd.HeaderRow.Cells[25].Width = 100;
                grd.HeaderRow.Cells[26].Text = "FECHA FIRMA CONTRATO";
                grd.HeaderRow.Cells[26].Width = 80;

                grd.HeaderRow.Cells[27].Text = "MONTO REFERENCIAL";
                grd.HeaderRow.Cells[28].Text = "FECHA CONVOCATORIA";
                grd.HeaderRow.Cells[29].Text = "BUENA PRO"; ;
                grd.HeaderRow.Cells[30].Text = "FECHA BUENA PRO CONSENTIDA";
                grd.HeaderRow.Cells[31].Text = "TIPO EMPRESA";
                grd.HeaderRow.Cells[31].Width = 80;
                grd.HeaderRow.Cells[32].Text = "CONSORCIO/CONTRATISTA";
                grd.HeaderRow.Cells[33].Text = "MONTO CONTRATADO";
                grd.HeaderRow.Cells[33].Width = 100;
                grd.HeaderRow.Cells[34].Text = "FECHA FIRMA CONTRATO";
                grd.HeaderRow.Cells[34].Width = 80;

                grd.HeaderRow.Cells[35].Text = "FECHA ENTREGA TERRENO";
                grd.HeaderRow.Cells[35].Width = 80;
                grd.HeaderRow.Cells[36].Text = "MONTO";
                grd.HeaderRow.Cells[37].Text = "FECHA";
                grd.HeaderRow.Cells[38].Text = "MONTO";
                grd.HeaderRow.Cells[39].Text = "FECHA";
                grd.HeaderRow.Cells[40].Text = "RESIDENTE";
                grd.HeaderRow.Cells[41].Text = "SUPERVISOR";
                grd.HeaderRow.Cells[42].Text = "NOMBRE";
                grd.HeaderRow.Cells[43].Text = "TELÉFONO";
                grd.HeaderRow.Cells[44].Text = "CORREO";
                grd.HeaderRow.Cells[45].Text = "FECHA INICIO DE OBRA ";
                grd.HeaderRow.Cells[45].Width = 80;
                grd.HeaderRow.Cells[46].Text = "PLAZO CONTRACTUAL";
                grd.HeaderRow.Cells[47].Text = "TERMINO DE PLAZO CONTRACTUAL";
                grd.HeaderRow.Cells[47].Width = 100;
                grd.HeaderRow.Cells[48].Text = "% AVANCE DE OBRA";
                grd.HeaderRow.Cells[49].Text = "ESTADO";
                grd.HeaderRow.Cells[50].Text = "FECHA REAL TERMINO OBRA";
                grd.HeaderRow.Cells[50].Width = 80;
                grd.HeaderRow.Cells[51].Text = "FECHA RECEPCION OBRA";
                grd.HeaderRow.Cells[51].Width = 80;
                grd.HeaderRow.Cells[52].Text = "RESOLUCIÓN DE LIQUIDACIÓN";
                grd.HeaderRow.Cells[53].Text = "ESTADO SITUACIONAL";
                grd.HeaderRow.Cells[53].Width = 500;
                grd.HeaderRow.Cells[54].Text = "ACCIONES";
                grd.HeaderRow.Cells[54].Width = 500;
                grd.HeaderRow.Cells[55].Text = "FECHA ACTUALIZACIÓN";
                grd.HeaderRow.Cells[56].Text = "FECHA ULT. CAMBIO EST.";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    for (int j = 0; j < 56; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "DATOS GENERALES";
                myTable.ColumnSpan = 13;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                myTable = new TableCell();
                myTable.Text = "  ";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PROCESO DE SELECCIÓN PERFIL/EXPEDIENTE T./OBRA";
                myTable.ColumnSpan = 8;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PROCESO DE SELECCIÓN SUPERVISION PERFIL/EXPEDIENTE T./OBRA";
                myTable.ColumnSpan = 8;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADELANTO DIRECTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADELANTO MATERIALES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "RESIDENTE";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "SUPERVISOR";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "COORDINADOR UE";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "ESTADO DE EJECUCION";
                myTable.ColumnSpan = 7;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable.Text = "OBSERVACIONES Y RECOMENDACIONES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                myTable = new TableCell();

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_MetasPMIB(GridView grd)
        {
            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "MetasPMIB_Monitoreo_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;
                //grd.HeaderRow.Cells[1].Visible = false;

                for (int i = 0; i < 12; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B8CCE4", 16));
                }
                for (int i = 12; i < 77; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                grd.HeaderRow.Height = 40;
                grd.HeaderRow.Cells[0].Text = "PERIODO";
                grd.HeaderRow.Cells[1].Text = "SNIP";
                grd.HeaderRow.Cells[2].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[3].Text = "PROVINCIA";
                grd.HeaderRow.Cells[4].Text = "DISTRITO";
                grd.HeaderRow.Cells[5].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[5].Width = 500;
                grd.HeaderRow.Cells[6].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[6].Width = 400;
                grd.HeaderRow.Cells[7].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[8].Text = "COSTO FASE INVERSION";
                grd.HeaderRow.Cells[9].Text = "RESPONSABLES";
                grd.HeaderRow.Cells[9].Width = 250;

                grd.HeaderRow.Cells[10].Text = "ESTADO";
                grd.HeaderRow.Cells[11].Text = "MONTO TOTAL";
                //PAVIMENTO
                grd.HeaderRow.Cells[12].Text = "M2";
                grd.HeaderRow.Cells[13].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[14].Text = "M2";
                grd.HeaderRow.Cells[15].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[16].Text = "M2";
                grd.HeaderRow.Cells[17].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[18].Text = "M2";
                grd.HeaderRow.Cells[19].Text = "Costo (S/.)";
                //VEREDAS
                grd.HeaderRow.Cells[20].Text = "M2";
                grd.HeaderRow.Cells[21].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[22].Text = "M2";
                grd.HeaderRow.Cells[23].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[24].Text = "M2";
                grd.HeaderRow.Cells[25].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[26].Text = "M2";
                grd.HeaderRow.Cells[27].Text = "Costo (S/.)";
                //DRENAJE PLUVIAL
                grd.HeaderRow.Cells[28].Text = "ML";
                grd.HeaderRow.Cells[29].Text = "Costo (S/.)"; ;
                grd.HeaderRow.Cells[30].Text = "ML";
                grd.HeaderRow.Cells[31].Text = "Costo (S/.)";
                //LOSA DE RECREACION MULTIUSOS
                grd.HeaderRow.Cells[32].Text = "M2";
                grd.HeaderRow.Cells[33].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[34].Text = "M2";
                grd.HeaderRow.Cells[35].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[36].Text = "Global";
                grd.HeaderRow.Cells[37].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[38].Text = "ML";
                grd.HeaderRow.Cells[39].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[40].Text = "Unidad";
                grd.HeaderRow.Cells[41].Text = "Costo (S/.)";
                //CENTRO COMUNAL COMERCIAL
                grd.HeaderRow.Cells[42].Text = "M2";
                grd.HeaderRow.Cells[43].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[44].Text = "Global";
                grd.HeaderRow.Cells[45].Text = "Costo (S/.)";
                grd.HeaderRow.Cells[45].Width = 80;
                grd.HeaderRow.Cells[46].Text = "ML";
                grd.HeaderRow.Cells[47].Text = "COSTO (S/.)";
                //OTROS
                grd.HeaderRow.Cells[48].Text = "M2";
                grd.HeaderRow.Cells[49].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[50].Text = "M2";
                grd.HeaderRow.Cells[51].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[52].Text = "M2";
                grd.HeaderRow.Cells[53].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[54].Text = "M2";
                grd.HeaderRow.Cells[55].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[56].Text = "Unidad";
                grd.HeaderRow.Cells[57].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[58].Text = "Unidad";
                grd.HeaderRow.Cells[59].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[60].Text = "Unidad";
                grd.HeaderRow.Cells[61].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[62].Text = "Unidad";
                grd.HeaderRow.Cells[63].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[64].Text = "Unidad";
                grd.HeaderRow.Cells[65].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[66].Text = "Unidad";
                grd.HeaderRow.Cells[67].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[68].Text = "Unidad";
                grd.HeaderRow.Cells[69].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[70].Text = "Unidad";
                grd.HeaderRow.Cells[71].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[72].Text = "M2";
                grd.HeaderRow.Cells[73].Text = "COSTO (S/.)";
                grd.HeaderRow.Cells[74].Text = "Unidad";
                grd.HeaderRow.Cells[75].Text = "COSTO (S/.)";
                //grd.HeaderRow.Cells[76].Text = "OBSERVACIÓN";
                grd.HeaderRow.Cells[76].Visible = false;
                //grd.HeaderRow.Cells31].Text = "SUB-LISTA";
                //grd.HeaderRow.Cells[32].Text = "REVISOR";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;
                    //row.Cells[1].Visible = false;
                    for (int j = 0; j < 77; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].VerticalAlign = VerticalAlign.Middle;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                // GridViewRow gvHeaderRow_3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable.Text = "DATOS GENERALES";
                myTable.ColumnSpan = 12;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("B8CCE4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CONCRETO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ASFALTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "EMBOQUILLADO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADOQUINADO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 12;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PAVIMENTO";
                myTable.ColumnSpan = 8;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CONCRETO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADOQUINADO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PIEDRA LAJA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ENBOQUILLADO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "VEREDAS";
                myTable.ColumnSpan = 8;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CONCRETO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PIEDRA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "DRENAJE PLUVIAL";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "LOSA DEPORTIVA DE CONCRETO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "TRIBUNAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADMINISTRACIÓN (INCL.SSHH)";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CERCO PERIMETRICO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ILUMINACION EXTERNA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "LOSA DE RECREACION MULTIUSOS";
                myTable.ColumnSpan = 10;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PUESTAS DE VENTAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "ADMINISTRACIÓN (INCL.SSHH)";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CERCO PERIMETRICO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CENTRO COMUNAL COMERCIAL";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "MURO DE CONTENCIÓN";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PONTON";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "BERMAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AREAS VERDES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "JARDINERAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PLANTONES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "INST. SANITARIAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "INST. ELECTRICAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "BANCAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "BASUREROS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PERGOLAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "GLORIETAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PUENTE";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "OTROS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "OTROS";
                myTable.ColumnSpan = 28;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);


                myTable = new TableCell();
                myTable.Text = "OBSERVACIÓN";
                myTable.ColumnSpan = 1;
                myTable.RowSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);
                //myTable = new TableCell();
                //myTable.Text = "INFORMACIÓN ADICIONAL";
                //myTable.ColumnSpan = 2;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(1, gvHeaderRow);
                //grd.Controls[0].Controls.AddAt(2, gvHeaderRow_3);
                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_ObrasDesactualizadas(GridView grd)
        {
            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "ReporteObrasDesactualizadas_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;
                //grd.HeaderRow.Cells[1].Visible = false;

                for (int i = 0; i < 8; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FAACAC", 16));
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                grd.HeaderRow.Height = 40;
                grd.HeaderRow.Cells[0].Text = "PROGRAMA";
                grd.HeaderRow.Cells[1].Text = "SNIP";
                grd.HeaderRow.Cells[2].Text = "REGIÓN";
                grd.HeaderRow.Cells[3].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[3].Width = 500;
                grd.HeaderRow.Cells[4].Text = "ESTADO";
                grd.HeaderRow.Cells[4].Width = 300;
                grd.HeaderRow.Cells[5].Text = "ÚLTIMA FECHA DE REGISTRO";
                grd.HeaderRow.Cells[6].Text = "AVANCE %";
                grd.HeaderRow.Cells[7].Text = "RESPONSABLES";
                grd.HeaderRow.Cells[7].Width = 250;

                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;
                    //row.Cells[1].Visible = false;
                    for (int j = 0; j < 8; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].VerticalAlign = VerticalAlign.Middle;

                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                //GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                // GridViewRow gvHeaderRow_3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable.Text = "";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B8CCE4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "SEGÚN VALORIZACIÓNES O ESTADO DEL PROYECTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FAACAC", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                //grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);
                //grd.Controls[0].Controls.AddAt(2, gvHeaderRow_3);
                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_NE(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "ReporteNE_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 98; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 10;
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "SUB PROG.";
                grd.HeaderRow.Cells[3].Text = "CONVENIO";
                grd.HeaderRow.Cells[4].Text = "SNIP";
                grd.HeaderRow.Cells[5].Text = "NOMBRE DE PROYECTO";
                grd.HeaderRow.Cells[6].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[6].Width = 120;
                grd.HeaderRow.Cells[7].Text = "PROVINCIA";
                grd.HeaderRow.Cells[7].Width = 140;
                grd.HeaderRow.Cells[8].Text = "DISTRITO";
                grd.HeaderRow.Cells[8].Width = 140;
                grd.HeaderRow.Cells[9].Text = "CCPP";
                grd.HeaderRow.Cells[9].Width = 150;
                grd.HeaderRow.Cells[10].Text = "POBREZA MIDIS";
                grd.HeaderRow.Cells[10].Width = 100;
                grd.HeaderRow.Cells[11].Text = "POBLACION";
                grd.HeaderRow.Cells[11].Width = 100;
                grd.HeaderRow.Cells[12].Text = "VIVIENDA META";
                grd.HeaderRow.Cells[12].Width = 100;
                grd.HeaderRow.Cells[13].Text = "VIVIENDA TERMINADA";
                grd.HeaderRow.Cells[13].Width = 100;
                grd.HeaderRow.Cells[14].Text = "MONTO FINANCIAMIENTO MVCS";
                grd.HeaderRow.Cells[14].Width = 130;
                grd.HeaderRow.Cells[15].Text = "MONTO REFORMULADO";
                grd.HeaderRow.Cells[15].Width = 120;
                grd.HeaderRow.Cells[16].Text = "MONTO F15";
                grd.HeaderRow.Cells[17].Text = "MONTO F16";
                grd.HeaderRow.Cells[18].Text = "MONTO LAUDO";
                grd.HeaderRow.Cells[19].Text = "FECHA CERTIFICACIÓN";
                grd.HeaderRow.Cells[19].Width = 100;
                grd.HeaderRow.Cells[20].Text = "CERTIFICACIÓN";
                grd.HeaderRow.Cells[21].Text = "MONTO CERTIFICADO";
                grd.HeaderRow.Cells[22].Text = "FECHA COMPROMISO";
                grd.HeaderRow.Cells[22].Width = 100;
                grd.HeaderRow.Cells[23].Text = "COMPROMISO";
                grd.HeaderRow.Cells[24].Text = "FECHA DEVENGADO";
                grd.HeaderRow.Cells[25].Text = "DEVENGADO";
                grd.HeaderRow.Cells[26].Text = "FECHA GIRADO";
                grd.HeaderRow.Cells[27].Text = "GIRADO";
                grd.HeaderRow.Cells[28].Text = "TOTAL DEVENGADO";
                grd.HeaderRow.Cells[29].Text = "TOTAL GIRADO";
                grd.HeaderRow.Cells[30].Text = "FECHA ACTA DE ASAMBLEA DE CONST. NE";
                grd.HeaderRow.Cells[30].Width = 80;

                grd.HeaderRow.Cells[31].Text = "FECHA PRESENTACIÓN DE SOLICITUD";
                grd.HeaderRow.Cells[31].Width = 100;
                grd.HeaderRow.Cells[32].Text = "RESULTADO DE SOLICITUD";
                grd.HeaderRow.Cells[32].Width = 80;
                grd.HeaderRow.Cells[33].Text = "FECHA DE APROBACIÓN DE SOLICITUD";
                grd.HeaderRow.Cells[33].Width = 80;
                grd.HeaderRow.Cells[34].Text = "MONTO A TRANSFERIR AL NE";
                grd.HeaderRow.Cells[35].Text = "FECHA APROBACIÓN EXP. TÉCNICO";
                grd.HeaderRow.Cells[35].Width = 80;
                grd.HeaderRow.Cells[36].Text = "FECHA APERTURA CUENTA";
                grd.HeaderRow.Cells[37].Text = "FECHA CONVENIO";
                grd.HeaderRow.Cells[38].Text = "NUMERO DE MUJERES COMO RNE";
                grd.HeaderRow.Cells[38].Width = 80;
                grd.HeaderRow.Cells[39].Text = "DNI";
                grd.HeaderRow.Cells[40].Text = "FECHA NACIMIENTO";
                grd.HeaderRow.Cells[41].Text = "SEXO";
                grd.HeaderRow.Cells[42].Text = "FECHA ASIGNACIÓN";
                grd.HeaderRow.Cells[43].Text = "NOMBRE";
                //grd.HeaderRow.Cells[42].Text = "A. PATERNO";
                //grd.HeaderRow.Cells[43].Text = "A. MATERNO";
                grd.HeaderRow.Cells[44].Text = "DNI";
                grd.HeaderRow.Cells[45].Text = "FECHA NACIMIENTO";
                grd.HeaderRow.Cells[46].Text = "SEXO";
                grd.HeaderRow.Cells[47].Text = "FECHA ASIGNACIÓN";
                grd.HeaderRow.Cells[48].Text = "NOMBRE";
                //grd.HeaderRow.Cells[49].Text = "A. PATERNO";
                //grd.HeaderRow.Cells[50].Text = "A. MATERNO";
                grd.HeaderRow.Cells[49].Text = "DNI";
                grd.HeaderRow.Cells[50].Text = "FECHA NACIMIENTO";
                grd.HeaderRow.Cells[51].Text = "SEXO";
                grd.HeaderRow.Cells[52].Text = "FECHA ASIGNACIÓN";
                grd.HeaderRow.Cells[53].Text = "NOMBRE";
                //grd.HeaderRow.Cells[56].Text = "A. PATERNO";
                //grd.HeaderRow.Cells[57].Text = "A. MATERNO";
                grd.HeaderRow.Cells[54].Text = "DNI";
                grd.HeaderRow.Cells[55].Text = "FECHA NACIMIENTO";
                grd.HeaderRow.Cells[56].Text = "SEXO";
                grd.HeaderRow.Cells[57].Text = "FECHA ASIGNACIÓN";
                grd.HeaderRow.Cells[58].Text = "NOMBRE";
                //grd.HeaderRow.Cells[63].Text = "A. PATERNO";
                //grd.HeaderRow.Cells[64].Text = "A. MATERNO";
                grd.HeaderRow.Cells[59].Text = "DNI";
                grd.HeaderRow.Cells[60].Text = "FECHA NACIMIENTO";
                grd.HeaderRow.Cells[61].Text = "SEXO";
                grd.HeaderRow.Cells[62].Text = "FECHA ASIGNACIÓN";
                grd.HeaderRow.Cells[63].Text = "NOMBRE";
                //grd.HeaderRow.Cells[70].Text = "A. PATERNO";
                //grd.HeaderRow.Cells[71].Text = "A. MATERNO";
                grd.HeaderRow.Cells[64].Text = "DNI";
                grd.HeaderRow.Cells[65].Text = "NOMBRE";
                grd.HeaderRow.Cells[66].Text = "PROFESIÓN";
                grd.HeaderRow.Cells[67].Text = "COLEGIATURA";
                grd.HeaderRow.Cells[68].Text = "DNI";
                grd.HeaderRow.Cells[69].Text = "NOMBRE";
                grd.HeaderRow.Cells[70].Text = "PROFESIÓN";
                grd.HeaderRow.Cells[71].Text = "COLEGIATURA";
                grd.HeaderRow.Cells[72].Text = "FECHA ORIENTACIÓN";
                grd.HeaderRow.Cells[72].Width = 80;
                grd.HeaderRow.Cells[73].Text = "FECHA INFORME INICIAL";
                grd.HeaderRow.Cells[73].Width = 80;
                grd.HeaderRow.Cells[74].Text = "FECHA ENTREGA TERRENO (FECHA INICIO OBRA)";
                grd.HeaderRow.Cells[74].Width = 80;
                //grd.HeaderRow.Cells[77].Text = "FECHA DE INICIO";
                //grd.HeaderRow.Cells[77].Width = 80;
                grd.HeaderRow.Cells[75].Text = "PLAZO DE PROYECTO (DIAS)";
                grd.HeaderRow.Cells[75].Width = 80;
                grd.HeaderRow.Cells[76].Text = "PLAZO DE OBRA (DIAS)";
                grd.HeaderRow.Cells[76].Width = 80;
                grd.HeaderRow.Cells[77].Text = "AMPLIACION PLAZO";
                grd.HeaderRow.Cells[77].Width = 100;

                grd.HeaderRow.Cells[78].Text = "AVANCE PROGRAMADO (%)";
                grd.HeaderRow.Cells[79].Text = "AVANCE FISICO (%)";
                grd.HeaderRow.Cells[80].Text = "AVANCE FINANCIERO (%)";

                grd.HeaderRow.Cells[81].Text = "ESTADO";
                grd.HeaderRow.Cells[82].Text = "SALDO BANCARIO";
                grd.HeaderRow.Cells[83].Text = "FECHA TÉRMINO OBRA (SEGÚN ACTA DE ENTREGA)";
                grd.HeaderRow.Cells[83].Width = 100;
                grd.HeaderRow.Cells[84].Text = "FECHA CULMINACIÓN DEL PROYECTO";
                grd.HeaderRow.Cells[84].Width = 100;
                grd.HeaderRow.Cells[85].Text = "FECHA RENDICIÓN FINAL DE CUENTAS A LA COMUNIDAD";
                grd.HeaderRow.Cells[85].Width = 100;
                grd.HeaderRow.Cells[86].Text = "FECHA PRESENTACIÓN DOCUMENTACIÓN COMPLEMENTARIA";
                grd.HeaderRow.Cells[86].Width = 120;
                grd.HeaderRow.Cells[87].Text = "FECHA APROBACIÓN DE LIQUIDACIÓN FINAL";
                grd.HeaderRow.Cells[87].Width = 100;
                grd.HeaderRow.Cells[88].Text = "MONTO DE LIQUIDACIÓN FINAL";
                grd.HeaderRow.Cells[88].Width = 80;
                grd.HeaderRow.Cells[89].Text = "DEVOLUCIÓN MONTO NO EJECUTADO";
                grd.HeaderRow.Cells[89].Width = 100;
                grd.HeaderRow.Cells[90].Text = "FECHA LIQUIDACIÓN FINAL";
                grd.HeaderRow.Cells[90].Width = 90;
                grd.HeaderRow.Cells[91].Text = "FECHA REGISTRO FORMATO SNIP-14";
                grd.HeaderRow.Cells[91].Width = 80;
                grd.HeaderRow.Cells[92].Text = "REVISOR";
                ////grd.HeaderRow.Cells[19].Width = 150;
                grd.HeaderRow.Cells[93].Text = "OBSERVACION";
                grd.HeaderRow.Cells[94].Text = "¿EMERGENCIA?";
                grd.HeaderRow.Cells[95].Text = "¿Reconstrucción?";
                grd.HeaderRow.Cells[96].Text = "¿Servicio?";
                grd.HeaderRow.Cells[97].Text = "TIPO INVERSIÓN";

                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;
                    for (int j = 0; j < 98; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 9;
                        row.Cells[j].Attributes.Add("class", "textmode");
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                // GridViewRow gvHeaderRow_3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable.Text = "INFORMACION GENERAL";
                myTable.ColumnSpan = 19;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "1er DESEMBOLSO";
                myTable.ColumnSpan = 9;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "TOTAL DE DESEMBOLSOS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = " ";
                myTable.ColumnSpan = 9;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PRESIDENTE";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "TESORERO";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "SECRETARIO";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "FISCAL";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "VEEDOR";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "RESIDENTE";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "SUPERVISOR";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "EJECUCIÓN";
                myTable.ColumnSpan = 14;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "LIQUIDACION Y CIERRE";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(1, gvHeaderRow);

                //string style = @"";
                string style = @"<style> .textmode { mso-number-format:\@; } </style> "; //PERMITE MOSTRAR CEROS A LA IZQUIERDA

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_ValorFinanciadoNE(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "ReporteValorFinanciadoNE_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;
                for (int i = 0; i < 7; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("DDEBF7", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 11;
                }

                for (int i = 7; i < 25; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 11;
                }

                for (int i = 25; i < 43; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F8F7D6", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 11;
                }
                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "SUB PROG.";
                grd.HeaderRow.Cells[3].Text = "CONVENIO";
                grd.HeaderRow.Cells[4].Text = "SNIP";
                grd.HeaderRow.Cells[5].Text = "NOMBRE DE PROYECTO";
                grd.HeaderRow.Cells[6].Text = "PLAZO DE OBRAS (DIAS)";
                grd.HeaderRow.Cells[6].Width = 80;
                grd.HeaderRow.Cells[7].Text = "Mano de obra calificada";
                grd.HeaderRow.Cells[7].Width = 100;
                grd.HeaderRow.Cells[8].Text = "Mano de obra no calificada";
                grd.HeaderRow.Cells[8].Width = 100;
                grd.HeaderRow.Cells[9].Text = "Materiales";
                grd.HeaderRow.Cells[9].Width = 100;
                grd.HeaderRow.Cells[10].Text = "Maquinaria y equipo";
                grd.HeaderRow.Cells[10].Width = 100;
                grd.HeaderRow.Cells[11].Text = "Herramientas";
                grd.HeaderRow.Cells[11].Width = 100;
                grd.HeaderRow.Cells[12].Text = "Flete";
                grd.HeaderRow.Cells[12].Width = 100;
                grd.HeaderRow.Cells[13].Text = "Otros";
                grd.HeaderRow.Cells[13].Width = 100;
                grd.HeaderRow.Cells[14].Text = "COSTO DIRECTO";
                grd.HeaderRow.Cells[15].Text = "GASTOS GENERALES";
                grd.HeaderRow.Cells[16].Text = "COSTOS FINANCIEROS";
                grd.HeaderRow.Cells[17].Text = "INTERES";
                grd.HeaderRow.Cells[18].Text = "SUBTOTAL INVERSION";
                grd.HeaderRow.Cells[19].Text = "GASTOS DE SUPERVISION";
                grd.HeaderRow.Cells[20].Text = "GASTOS DE COMP. SOCIAL";
                grd.HeaderRow.Cells[21].Text = "GASTOS DE N. EJECUTOR";
                grd.HeaderRow.Cells[22].Text = "GASTOS DE OPERACION";
                grd.HeaderRow.Cells[23].Text = "OTROS";
                grd.HeaderRow.Cells[24].Text = "TOTAL";

                grd.HeaderRow.Cells[25].Text = "Mano de obra calificada";
                grd.HeaderRow.Cells[25].Width = 100;
                grd.HeaderRow.Cells[26].Text = "Mano de obra no calificada";
                grd.HeaderRow.Cells[26].Width = 100;
                grd.HeaderRow.Cells[27].Text = "Materiales";
                grd.HeaderRow.Cells[27].Width = 100;
                grd.HeaderRow.Cells[28].Text = "Maquinaria y equipo";
                grd.HeaderRow.Cells[28].Width = 100;
                grd.HeaderRow.Cells[29].Text = "Herramientas";
                grd.HeaderRow.Cells[29].Width = 100;
                grd.HeaderRow.Cells[30].Text = "Flete";
                grd.HeaderRow.Cells[30].Width = 100;
                grd.HeaderRow.Cells[31].Text = "Otros";
                grd.HeaderRow.Cells[31].Width = 100;
                grd.HeaderRow.Cells[32].Text = "COSTO DIRECTO";
                grd.HeaderRow.Cells[33].Text = "GASTOS GENERALES";
                grd.HeaderRow.Cells[34].Text = "COSTOS FINANCIEROS";
                grd.HeaderRow.Cells[35].Text = "INTERES";
                grd.HeaderRow.Cells[36].Text = "SUBTOTAL INVERSION";
                grd.HeaderRow.Cells[37].Text = "GASTOS DE SUPERVISION";
                grd.HeaderRow.Cells[38].Text = "GASTOS DE COMP. SOCIAL";
                grd.HeaderRow.Cells[39].Text = "GASTOS DE N. EJECUTOR";
                grd.HeaderRow.Cells[40].Text = "GASTOS DE OPERACION";
                grd.HeaderRow.Cells[41].Text = "OTROS";
                grd.HeaderRow.Cells[42].Text = "TOTAL";

                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;
                    for (int j = 0; j < 24; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 9;
                        row.Cells[j].Attributes.Add("class", "textmode");
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                // GridViewRow gvHeaderRow_3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable.Text = "INFORMACION GENERAL";
                myTable.ColumnSpan = 7;
                //myTable.RowSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("DDEBF7", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                myTable = new TableCell();
                myTable.Text = "DETALLE VALOR FINANCIADO (PRESUPUESTO)";
                myTable.ColumnSpan = 18;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "DETALLE GASTOS ACUMULADOS";
                myTable.ColumnSpan = 18;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("F8F7D6", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(1, gvHeaderRow);

                //string style = @"";
                string style = @"<style> .textmode { mso-number-format:\@; } </style> "; //PERMITE MOSTRAR CEROS A LA IZQUIERDA

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_DetalleDevengadoNE(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "ReporteDetalleDevengadoNE_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 23; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 11;
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "SUB PROG.";
                grd.HeaderRow.Cells[3].Text = "CONVENIO";
                grd.HeaderRow.Cells[4].Text = "SNIP";
                grd.HeaderRow.Cells[5].Text = "NOMBRE DE PROYECTO";
                grd.HeaderRow.Cells[6].Text = "PERIODO";
                grd.HeaderRow.Cells[7].Text = "META";
                grd.HeaderRow.Cells[8].Text = "CERTIFICADO";
                grd.HeaderRow.Cells[9].Text = "SECUENCIA";
                grd.HeaderRow.Cells[10].Text = "FUENTE";
                grd.HeaderRow.Cells[11].Text = "FECHA";
                grd.HeaderRow.Cells[12].Text = "MONTO";
                grd.HeaderRow.Cells[13].Text = "EXPEDIENTE";
                grd.HeaderRow.Cells[14].Text = "SECUENCIA";
                grd.HeaderRow.Cells[15].Text = "FECHA";
                grd.HeaderRow.Cells[16].Text = "MONTO";
                grd.HeaderRow.Cells[17].Text = "SECUENCIA";
                grd.HeaderRow.Cells[18].Text = "FECHA";
                grd.HeaderRow.Cells[19].Text = "MONTO";
                grd.HeaderRow.Cells[20].Text = "SECUENCIA";
                grd.HeaderRow.Cells[21].Text = "FECHA";
                grd.HeaderRow.Cells[22].Text = "MONTO";

                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;
                    for (int j = 0; j < 23; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        row.Cells[j].Attributes.Add("class", "textmode");
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                //GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                // GridViewRow gvHeaderRow_3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable.Text = "INFORMACION GENERAL";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CERTIFICADO";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "COMPROMISO";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "DEVENGADO";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "GIRADO";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("E2EFDA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                //grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                //string style = @"";
                string style = @"<style> .textmode { mso-number-format:\@; } </style> "; //PERMITE MOSTRAR CEROS A LA IZQUIERDA

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_SeguimientoNE(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "ReporteSeguimientoNE_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.White;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 14; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("00B0F0", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }

                for (int i = 14; i < 33; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("8497B0", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }

                for (int i = 33; i < 64; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("7030A0", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }

                for (int i = 64; i < 70; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("BF8F00", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }

                for (int i = 70; i < 77; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("548237", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }

                for (int i = 77; i < 78; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("7092BE", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }

                for (int i = 78; i < 82; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("1F4E78", 16));
                    grd.HeaderRow.Cells[i].Font.Size = 9;
                }
                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[1].Text = "PROGRAMA";
                grd.HeaderRow.Cells[2].Text = "SUB PROG.";
                grd.HeaderRow.Cells[3].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[4].Text = "PROVINCIA";
                grd.HeaderRow.Cells[5].Text = "DISTRITO";
                grd.HeaderRow.Cells[5].Width = 140;
                grd.HeaderRow.Cells[6].Text = "CCPP";
                grd.HeaderRow.Cells[6].Width = 250;
                grd.HeaderRow.Cells[7].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[8].Text = "SNIP";
                grd.HeaderRow.Cells[9].Text = "POBREZA MIDIS";
                grd.HeaderRow.Cells[9].Width = 100;
                grd.HeaderRow.Cells[10].Text = "POBLACION";
                grd.HeaderRow.Cells[10].Width = 100;
                grd.HeaderRow.Cells[11].Text = "MONTO FINANCIAMIENTO MVCS";
                grd.HeaderRow.Cells[11].Width = 130;
                grd.HeaderRow.Cells[12].Text = "MONTO REFORMULADO";
                grd.HeaderRow.Cells[12].Width = 120;
                grd.HeaderRow.Cells[13].Text = "MONTO F15";
                grd.HeaderRow.Cells[13].Width = 120;
                grd.HeaderRow.Cells[14].Text = "Fecha Acta Asamblea constitución NE";
                grd.HeaderRow.Cells[14].Width = 100;
                grd.HeaderRow.Cells[15].Text = "Registró acta (Si/No)";
                grd.HeaderRow.Cells[15].Width = 80;
                grd.HeaderRow.Cells[16].Text = "Total Miembros";
                grd.HeaderRow.Cells[17].Text = "Número Mujeres";
                grd.HeaderRow.Cells[17].Width = 70;
                grd.HeaderRow.Cells[18].Text = "Fecha presentación";
                grd.HeaderRow.Cells[19].Text = "Resultado solicitud";
                grd.HeaderRow.Cells[20].Text = "Fecha aprobación";
                grd.HeaderRow.Cells[21].Text = "Cargó Solicitud (Si/No)";
                grd.HeaderRow.Cells[21].Width = 80;
                grd.HeaderRow.Cells[22].Text = "Nro. de Convenio";
                grd.HeaderRow.Cells[23].Text = "Fecha Convenio";
                grd.HeaderRow.Cells[24].Text = "Cargó Convenio (Si/No)";
                grd.HeaderRow.Cells[25].Text = "Fecha de aprobación";
                grd.HeaderRow.Cells[26].Text = "Monto aprobado (S/.)";
                grd.HeaderRow.Cells[27].Text = "Monto a transferir al N.E.(S/.)";
                grd.HeaderRow.Cells[27].Width = 80;
                grd.HeaderRow.Cells[28].Text = "Cargó Resolución Ministerial (Si/No)";
                grd.HeaderRow.Cells[28].Width = 80;
                grd.HeaderRow.Cells[29].Text = "Fecha de apertura";
                grd.HeaderRow.Cells[29].Width = 80;
                grd.HeaderRow.Cells[30].Text = "Nombre de Banco";
                grd.HeaderRow.Cells[30].Width = 80;
                grd.HeaderRow.Cells[31].Text = "Fecha orientación";
                grd.HeaderRow.Cells[31].Width = 80;
                grd.HeaderRow.Cells[32].Text = "Cargó Acta (Si/No)";
                grd.HeaderRow.Cells[33].Text = "Fecha de inicio del proyecto";
                grd.HeaderRow.Cells[33].Width = 80;
                grd.HeaderRow.Cells[34].Text = "Plazo de Ejecución Proy. (Días Calendario)";
                grd.HeaderRow.Cells[34].Width = 80;
                grd.HeaderRow.Cells[35].Text = "Fecha estimada de termino de proyecto";
                grd.HeaderRow.Cells[35].Width = 80;
                grd.HeaderRow.Cells[36].Text = "Fecha de aprobación del informe inicial";
                grd.HeaderRow.Cells[36].Width = 80;
                grd.HeaderRow.Cells[37].Text = "Fecha autorización de primer retiro de fondos";
                grd.HeaderRow.Cells[37].Width = 90;
                grd.HeaderRow.Cells[38].Text = "Fecha Entrega Terreno(Inicio Obra)";
                grd.HeaderRow.Cells[38].Width = 80;
                grd.HeaderRow.Cells[39].Text = "Plazo de Ejecución Obra (Días Calendario";
                grd.HeaderRow.Cells[39].Width = 80;
                grd.HeaderRow.Cells[40].Text = "Fecha estimada de término de obra";
                grd.HeaderRow.Cells[40].Width = 80;
                grd.HeaderRow.Cells[41].Text = "F. estimada de Ter. + Ampl.";
                grd.HeaderRow.Cells[42].Width = 80;
                grd.HeaderRow.Cells[42].Text = "Cargó Acta (Si/No)";
                grd.HeaderRow.Cells[43].Text = "Total de personal del NE";
                grd.HeaderRow.Cells[43].Width = 110;
                grd.HeaderRow.Cells[44].Text = "Primera Fecha";
                grd.HeaderRow.Cells[45].Text = "Última Fecha";
                grd.HeaderRow.Cells[46].Text = "Nro de rendiciones";
                grd.HeaderRow.Cells[47].Text = "Número de actas cargadas";
                grd.HeaderRow.Cells[47].Width = 80;
                grd.HeaderRow.Cells[48].Text = "Último mes avance de obra registrado";
                grd.HeaderRow.Cells[48].Width = 80;
                grd.HeaderRow.Cells[49].Text = "Físico Real %";
                grd.HeaderRow.Cells[50].Text = "Físico Prog. %";
                grd.HeaderRow.Cells[51].Text = "Financiero Real %";
                grd.HeaderRow.Cells[51].Width = 80;
                grd.HeaderRow.Cells[52].Text = "Financiero Prog. %";
                grd.HeaderRow.Cells[52].Width = 80;
                grd.HeaderRow.Cells[53].Text = "Monto Valorizado Acumulado";
                grd.HeaderRow.Cells[53].Width = 80;
                grd.HeaderRow.Cells[54].Text = "Primera Fecha Certificado";
                grd.HeaderRow.Cells[54].Width = 80;
                grd.HeaderRow.Cells[55].Text = "Primer Monto Certificado (S/.)";
                grd.HeaderRow.Cells[55].Width = 80;
                grd.HeaderRow.Cells[56].Text = "Primer Fecha Girado";
                grd.HeaderRow.Cells[56].Width = 80;
                grd.HeaderRow.Cells[57].Text = "Primer Monto Girado (S/.)";
                grd.HeaderRow.Cells[57].Width = 80;
                grd.HeaderRow.Cells[58].Text = "Monto Total Girado (S/.)";
                grd.HeaderRow.Cells[58].Width = 80;
                grd.HeaderRow.Cells[59].Text = "Total de Monto autorizado";
                grd.HeaderRow.Cells[60].Text = "Saldo Bancario";
                grd.HeaderRow.Cells[61].Text = "Fecha de término de obra";
                grd.HeaderRow.Cells[61].Width = 80;
                grd.HeaderRow.Cells[62].Text = "Fecha de rendición final de cuentas a la comunidad";
                grd.HeaderRow.Cells[62].Width = 90;
                grd.HeaderRow.Cells[63].Text = "Fecha real de culminación del proyecto";
                grd.HeaderRow.Cells[63].Width = 80;
                grd.HeaderRow.Cells[64].Text = "Número de gestiones de ampliación";
                grd.HeaderRow.Cells[64].Width = 80;
                grd.HeaderRow.Cells[65].Text = "Total de días ampliados";
                grd.HeaderRow.Cells[65].Width = 80;
                grd.HeaderRow.Cells[66].Text = "Número de gestiones de presupuesto adicional";
                grd.HeaderRow.Cells[66].Width = 80;
                grd.HeaderRow.Cells[67].Text = "Total de presupuesto adicional";
                grd.HeaderRow.Cells[67].Width = 80;
                grd.HeaderRow.Cells[68].Text = "Número de gestiones de deductivos y reducciones";
                grd.HeaderRow.Cells[68].Width = 80;
                grd.HeaderRow.Cells[69].Text = "Total de presupuesto deductivo";
                grd.HeaderRow.Cells[69].Width = 80;
                grd.HeaderRow.Cells[70].Text = "Fecha de presentación de documentación complementaria";
                grd.HeaderRow.Cells[70].Width = 100;
                grd.HeaderRow.Cells[71].Text = "Fecha firma liquidador del programa ficha aprobación de liquidación final";
                grd.HeaderRow.Cells[71].Width = 140;
                grd.HeaderRow.Cells[72].Text = "Devolución de monto no ejecutado (S/.)";
                grd.HeaderRow.Cells[72].Width = 80;
                grd.HeaderRow.Cells[73].Text = "Monto Total de Transferencia (S/.)";
                grd.HeaderRow.Cells[73].Width = 80;
                grd.HeaderRow.Cells[74].Text = "Monto de Liq. Final (S/.)";
                grd.HeaderRow.Cells[74].Width = 80;
                grd.HeaderRow.Cells[75].Text = "Cargó Acta y/o Oficio de cierre de convenio (Si /No)";
                grd.HeaderRow.Cells[75].Width = 100;
                grd.HeaderRow.Cells[76].Text = "Cargó Resolución de Liquidación (Si /No)";
                grd.HeaderRow.Cells[76].Width = 80;
                grd.HeaderRow.Cells[77].Text = "Fecha de registro de formato SNIP-14";
                grd.HeaderRow.Cells[77].Width = 170;
                grd.HeaderRow.Cells[78].Text = "Estado";
                grd.HeaderRow.Cells[79].Text = "Sub Estado";
                grd.HeaderRow.Cells[80].Text = "Coordinador";
                grd.HeaderRow.Cells[81].Text = "Observación";


                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];
                    //row.Cells[0].Visible = false;
                    for (int j = 0; j < 82; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 9;
                        row.Cells[j].Attributes.Add("class", "textmode");
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                GridViewRow gvHeaderRow_2 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                // GridViewRow gvHeaderRow_3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();

                myTable = new TableCell();
                myTable.Text = "INFORMACION GENERAL DEL PROYECTO";
                myTable.ColumnSpan = 14;
                myTable.RowSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("00B0F0", 16));
                myTable.Font.Bold = true;
                myTable.ForeColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CONSTITUCIÓN DEL NÚCLEO EJECUTOR";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "MIEMBROS DEL NE";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "DATOS DE LA SOLICITUD";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                //myTable.Height = 30;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CONVENIO DEL NE";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "DATOS DE LA APROBACIÓN DEL EXPEDIENTE TÉCNICO";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CUENTA BANCARIA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "SESIÓN DE ORIENTACIÓN";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "II. ETAPA PRE OPERATIVA";
                myTable.ColumnSpan = 19;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("8497B0", 16));
                myTable.ForeColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);


                myTable = new TableCell();
                myTable.Text = "INICIO DEL PROYECTO";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "INICIO DE LA OBRA";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PERSONAL NE Y SEGUIMIENTO DEL PROG.";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PROGRAMACIÓN DE OBRA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "RENDICIÓN DE CUENTAS A LA COMUNIDAD";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AVANCE DE OBRA SEGÚN INFORMES - PRE LIQUIDACIÓN MENSUAL DE OBRA";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "SEGUIMIENTO AL AVANCE FINANCIERO";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AUTORIZACIÓN DE GASTOS Y SALDO BANCARIO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CULMINACIÓN DEL PROYECTO";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "III. ETAPA OPERATIVA";
                myTable.ColumnSpan = 31;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("7030A0", 16));
                myTable.ForeColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AMPLIACIÓN DE PLAZO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PRESUPUESTO ADICIONAL";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "PRESUPUESTO DEDUCTIVO Y REDUCCIONES";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "IV. ADENDAS";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("BF8F00", 16));
                myTable.ForeColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "LIQUIDACIÓN FINAL";
                myTable.ColumnSpan = 7;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "V. LIQUIDACIÓN Y CIERRE";
                myTable.ColumnSpan = 7;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("548237", 16));
                myTable.ForeColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "CIERRE DE PROYECTO";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCDA", 16));
                myTable.Font.Bold = true;
                myTable.Font.Size = 10;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "VI. CIERRE DE PROYECTO";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("7092BE", 16));
                myTable.ForeColor = Color.FromArgb(Convert.ToInt32("FFFFFF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow_2.Cells.Add(myTable);


                grd.Controls[0].Controls.AddAt(0, gvHeaderRow_2);
                grd.Controls[0].Controls.AddAt(1, gvHeaderRow);

                //string style = @"";
                string style = @"<style> .textmode { mso-number-format:\@; } </style> "; //PERMITE MOSTRAR CEROS A LA IZQUIERDA

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
        protected void Exportar_Paralizados(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_Paralizados_";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("B7DEE8", 16));F2DCDB
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;
                grd.HeaderRow.Font.Size = 11;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 9; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                }
                for (int i = 9; i < 34; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }

                grd.HeaderRow.Cells[0].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[0].Width = 80;
                grd.HeaderRow.Cells[1].Text = "GESTION";
                grd.HeaderRow.Cells[2].Text = "PROGRAMA";
                grd.HeaderRow.Cells[3].Text = "SNIP";
                grd.HeaderRow.Cells[4].Text = "TIPO FINANCIAMIENTO";
                grd.HeaderRow.Cells[5].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[5].Width = 150;
                grd.HeaderRow.Cells[6].Text = "PROVINCIA";
                grd.HeaderRow.Cells[6].Width = 150;
                grd.HeaderRow.Cells[7].Text = "DISTRITO";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[8].Width = 500;

                grd.HeaderRow.Cells[9].Text = "FECHA CONVOCATORIA";
                grd.HeaderRow.Cells[9].Width = 100;
                grd.HeaderRow.Cells[10].Text = "FECHA BUENA PRO";
                grd.HeaderRow.Cells[10].Width = 80;
                grd.HeaderRow.Cells[11].Text = "MONTO CONTRATADO OBRA";
                grd.HeaderRow.Cells[12].Text = "MONTO CONTRATADO SUPERVISION";

                grd.HeaderRow.Cells[13].Text = "TIPO MODALIDAD OBRA";
                grd.HeaderRow.Cells[14].Text = "TIPO EMPRESA";
                grd.HeaderRow.Cells[15].Text = "CONTRATISTA/CONSORCIO";
                grd.HeaderRow.Cells[16].Text = "INTEGRANTES CONSORCIO";
                grd.HeaderRow.Cells[16].Width = 400;

                grd.HeaderRow.Cells[17].Text = "FECHA ENTREGA TERRENO";
                grd.HeaderRow.Cells[17].Width = 80;
                grd.HeaderRow.Cells[18].Text = "FECHA INICO";
                grd.HeaderRow.Cells[19].Text = "PLAZO DE EJECUCION (DIAS)";
                grd.HeaderRow.Cells[19].Width = 80;
                grd.HeaderRow.Cells[20].Text = "TERMINO CONTRACTUAL";
                grd.HeaderRow.Cells[21].Text = "TOTAL AMPLIACION (DIAS)";
                grd.HeaderRow.Cells[21].Width = 90;

                grd.HeaderRow.Cells[22].Text = "SUPERVISOR";
                grd.HeaderRow.Cells[23].Text = "RESIDENTE";

                grd.HeaderRow.Cells[24].Text = "AVANCE FISICO PROGRAMADO";
                grd.HeaderRow.Cells[24].Width = 100;
                grd.HeaderRow.Cells[25].Text = "AVANCE FISICO REAL";
                grd.HeaderRow.Cells[25].Width = 80;
                grd.HeaderRow.Cells[26].Text = "AVANCE FINANCIERO PROGRAMADO";
                grd.HeaderRow.Cells[26].Width = 100;
                grd.HeaderRow.Cells[27].Text = "AVANCE FINANCIERO REAL";
                grd.HeaderRow.Cells[27].Width = 90;
                grd.HeaderRow.Cells[28].Text = "ESTADO DE EJECUCION";
                grd.HeaderRow.Cells[29].Text = "SUB-ESTADO DE EJECUCION";
                grd.HeaderRow.Cells[30].Text = "DETALLE";
                grd.HeaderRow.Cells[31].Text = "ACCIONES";

                grd.HeaderRow.Cells[32].Text = "FECHA ULT. MONITOREO";
                grd.HeaderRow.Cells[33].Text = "MONITOR";

                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    //row.Cells[0].Visible = false;

                    for (int j = 0; j < 33; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "DETALLES DE PROYECTOS FINANCIADOS";
                myTable.ColumnSpan = 34;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
       
        protected void ddlSubsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTecnico.Enabled == true)
            {
                cargaTecnico(ddlTecnico);
            }
            Up_Filtro.Update();
        }
    }

}