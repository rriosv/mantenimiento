﻿<%@ Page Title="Reporte Monitoreo" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="ReporteMonitoreo_OEEE.aspx.cs" Inherits="Web.reportes.ReporteMonitoreo_OEEE" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <br />
    <br />
    <b> 
        <asp:Label runat="server" Font-Size="15px" Text="REPORTE DE MONITOREO DE PROYECTOS" ></asp:Label>
        
    </b>
    <br />
    <br />
    <center>

    <asp:Button  ID="btnExportar" runat="server" Text="EXPORTAR"  OnClick="btnExportar_Click" />
    <div style="height:5px;">

    </div>
    <dx:ASPxGridView ID="xgrdReporte" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="98%" KeyFieldName="IdSSP" >
        
        
        <Settings ShowGroupPanel="True"  />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <Styles Cell-Wrap="True" Header-Wrap="True" Header-BackColor="#0072C6" Header-ForeColor="White"></Styles>
    </dx:ASPxGridView>
     
        <dx:ASPxGridViewExporter ID="xgrdExporter" runat="server" GridViewID="xgrdReporte"></dx:ASPxGridViewExporter>
        </center>
</asp:Content>


