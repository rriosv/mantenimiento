﻿using pry_san;
using Entity;
using Business;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Web.reportes
{
    public partial class ReporteDemanda : System.Web.UI.Page
    {
        mProyectos objMetodo = new mProyectos();
        //mGeneral objMetodoGeneral = new mGeneral();
        BLUtil objUtil = new BLUtil();


        private BEProyecto _BEProyecto = new BEProyecto();
        BLProyecto objBLProyecto = new BLProyecto();

        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();
        DataTable dt;

     
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
               
                cargaUbigeo(ddl_depa, "1", null, null, null);
                cargaUbigeo(ddl_prov, "2", ddl_depa.SelectedValue, null, null);
                cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);
                //cargaSubsector();

                if (Session["CodSubsector"].ToString() == "1" || Session["CodSubsector"].ToString() == "2" || Session["CodSubsector"].ToString() == "3")
                {
                    ddl_subsector.Enabled = false;
                    ddl_subsector.SelectedValue = Session["CodSubsector"].ToString();
                }

                CargaGrilla();
            }

        }

        protected void FiltrarDepa(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_prov, "2", ddl_depa.SelectedValue, null, null);
            cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);

            CargaGrilla();
            up_grd.Update();
        }

        protected void FiltrarProv(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);
            CargaGrilla();
            up_grd.Update();
        }

        protected void FiltrarDist(object sender, EventArgs e)
        {
            CargaGrilla();
            up_grd.Update();
        }

        protected void FiltrarSector(object sender, EventArgs e)
        {
            CargaGrilla();
            up_grd.Update();

        }

        protected void btnReiniciar_Click(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_depa, "1", null, null, null);
            cargaUbigeo(ddl_prov, "2", ddl_depa.SelectedValue, null, null);
            cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);
            ddl_subsector.SelectedValue = "";
            txtSearch.Text = "";
            CargaGrilla();
            up_grd.Update();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargaGrilla();
            up_grd.Update();

        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        //public void cargaSubsector()
        //{
        //    ddl_subsector.DataSource = objBLbandeja.F_spMON_TipoPrograma();
        //    ddl_subsector.DataTextField = "nombre";
        //    ddl_subsector.DataValueField = "valor";
        //    ddl_subsector.DataBind();

        //    ddl_subsector.Items.Insert(0, new ListItem("-Seleccione-", ""));
        //}

        protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grd.PageIndex = e.NewPageIndex;
            CargaGrilla();
            up_grd.Update();
        }

        public void DisplayCurrentPage()
        {

            int currentPage = grd.PageIndex + 1;
            string paginado = " (Pagina " + currentPage.ToString() +
                    " de " + grd.PageCount.ToString() + ")";
            lblPaginadoAsignado.Text = paginado;

        }

        public void CargaGrilla()
        {
            DataTable dt;
            _BEProyecto.Criterio = cboSearch.SelectedValue;
            _BEProyecto.Buscar = txtSearch.Text;
            _BEProyecto.Sector = ddl_subsector.SelectedValue;
            _BEProyecto.Cod_depa = ddl_depa.SelectedValue;
            _BEProyecto.Cod_prov = ddl_prov.SelectedValue;
            _BEProyecto.Cod_dist = ddl_dist.SelectedValue;

            dt = objBLProyecto.spSOL_ReporteDemanda(_BEProyecto);

            grd.DataSource = dt;
            grd.DataBind();

            LabeltotalAsignado.Text = "Total: " + dt.Rows.Count.ToString();
        }

        protected void grd_OnDataBound(object sender, EventArgs e)
        {
            DisplayCurrentPage();

        }

        protected void btnExcel_onclick(object sender, EventArgs e)
        {
            if (ddl_subsector.SelectedValue == "2") // PMIB
            {
                _BEProyecto.Criterio = cboSearch.SelectedValue;
                _BEProyecto.Buscar = txtSearch.Text;

                _BEProyecto.Sector = ddl_subsector.SelectedValue;

                _BEProyecto.Cod_depa = ddl_depa.SelectedValue;
                _BEProyecto.Cod_prov = ddl_prov.SelectedValue;
                _BEProyecto.Cod_dist = ddl_dist.SelectedValue;
                dt = objBLProyecto.spSOL_ReporteDemandaPMIB(_BEProyecto);
                dt.DefaultView.Sort = "numero ASC";
                GridView grd2 = new GridView();
                grd2.DataSource = dt;
                grd2.DataBind();

                Exportar_DemandaPMIB(grd2);
            }
            else
            {
                _BEProyecto.Criterio = cboSearch.SelectedValue;
                _BEProyecto.Buscar = txtSearch.Text;
                _BEProyecto.Sector = ddl_subsector.SelectedValue;
                _BEProyecto.Cod_depa = ddl_depa.SelectedValue;
                _BEProyecto.Cod_prov = ddl_prov.SelectedValue;
                _BEProyecto.Cod_dist = ddl_dist.SelectedValue;
                dt = objBLProyecto.spSOL_ReporteDemanda(_BEProyecto);
                dt.DefaultView.Sort = "numero ASC";
                GridView grd2 = new GridView();
                grd2.DataSource = dt;
                grd2.DataBind();

                Exportar_XLS(grd2);
            }

            // Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
        }

        protected void Exportar_XLS(GridView grd)
        {


            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_Demanda.xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 24; i < 29; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }

                for (int i = 29; i <33; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                }

                grd.HeaderRow.Cells[35].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[36].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[1].Text = "ID SOLICITUD";
                grd.HeaderRow.Cells[2].Text = "PROGRAMA";
                grd.HeaderRow.Cells[3].Text = "SNIP";
                grd.HeaderRow.Cells[4].Text = "SNIP RELACION";
                grd.HeaderRow.Cells[5].Text = "CÓDIGO UNIFICADO";
                grd.HeaderRow.Cells[5].Width = 80;
                grd.HeaderRow.Cells[6].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[6].Width = 500;
                grd.HeaderRow.Cells[7].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[8].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[9].Text = "PROVINCIA";
                grd.HeaderRow.Cells[10].Text = "DISTRITO";
                grd.HeaderRow.Cells[11].Text = "CCPP";
                grd.HeaderRow.Cells[12].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[12].Width = 100;
                grd.HeaderRow.Cells[13].Text = "POBLACION PROMEDIO";
                grd.HeaderRow.Cells[13].Width = 100;
                grd.HeaderRow.Cells[14].Text = "UBIGEO";
                grd.HeaderRow.Cells[15].Text = "AMBITO";
                grd.HeaderRow.Cells[16].Text = "ESTADO DE ESTUDIO";
                grd.HeaderRow.Cells[17].Text = "ESTADO DEL PROYECTO";
                //grd.HeaderRow.Cells[19].Width = 150;
                grd.HeaderRow.Cells[18].Text = "FECHA DE VIABILIDAD";
                grd.HeaderRow.Cells[18].Width = 80;
                grd.HeaderRow.Cells[19].Text = "FECHA DE VENCIMIENTO DE VIABILIDAD";
                grd.HeaderRow.Cells[19].Width = 100;
                grd.HeaderRow.Cells[20].Text = "MONTO VIABLE";
                grd.HeaderRow.Cells[20].Width = 80;
                grd.HeaderRow.Cells[21].Text = "MONTO DE INVESION TOTAL";
                grd.HeaderRow.Cells[21].Width = 80;
                grd.HeaderRow.Cells[22].Text = "DEVENGADO ACUMULADO";
                grd.HeaderRow.Cells[22].Width = 80;
                grd.HeaderRow.Cells[23].Text = "RATIO";
                grd.HeaderRow.Cells[24].Text = "RANGO DE INVERSION";
                grd.HeaderRow.Cells[24].Width = 120;


                grd.HeaderRow.Cells[25].Text = "TIPO DE PROYECTO QUE SOLICITA";
                grd.HeaderRow.Cells[25].Width = 100;
                grd.HeaderRow.Cells[26].Text = "TIENE EXPEDIENTE TECNICO";
                grd.HeaderRow.Cells[26].Width = 100;
                grd.HeaderRow.Cells[27].Text = "PRESENTO EXPEDIENTE TECNICO AL MVCS PARA EVALUACION";
                grd.HeaderRow.Cells[27].Width = 160;
                grd.HeaderRow.Cells[28].Text = "ESTADO";

                //grd.HeaderRow.Cells[29].Text = "VRAEM";
                grd.HeaderRow.Cells[29].Text = "FONIE";
                grd.HeaderRow.Cells[30].Text = "DISTRITO FRONTERIZO";
                grd.HeaderRow.Cells[31].Text = "CODEHUALLAGA";
                //grd.HeaderRow.Cells[33].Text = "ZONA HELADA";
                grd.HeaderRow.Cells[32].Text = "CUENCA (Pastaza - Corriente - Tigre - Marañon";
                grd.HeaderRow.Cells[32].Width = 140;
                grd.HeaderRow.Cells[33].Text = "LISTA";
                grd.HeaderRow.Cells[34].Text = "SUB-LISTA";
                grd.HeaderRow.Cells[35].Text = "REVISOR";
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];

                    //row.Cells[0].Visible = false;

                    for (int j = 0; j < 29; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "INFORMACIÓN DEL PROYECTO";
                myTable.ColumnSpan = 24;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                myTable = new TableCell();
                myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AMBITO DE INTERVENCION";
                myTable.ColumnSpan = 4;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                myTable = new TableCell();
                myTable.Text = "";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "INFORMACIÓN ADICIONAL";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);

                // Agrega título en primera celda
                string Titulo = "REPORTE DE CALIDAD DEL MINISTERIO DE VIVIENDA, CONSTRUCCION Y SANEAMIENTO";
                HttpContext.Current.Response.Write(Titulo);
                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }

        protected void Exportar_DemandaPMIB(GridView grd)
        {


            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_Demanda_PMIB_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;
                grd.HeaderRow.Font.Size = 11;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 21; i < 28; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }

                for (int i = 28; i < 33; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                }

                for (int i = 33; i < 39; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                }

                grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[1].Text = "ID SOLICITUD";
                grd.HeaderRow.Cells[2].Text = "PROGRAMA";
                grd.HeaderRow.Cells[3].Text = "USUARIO QUE REGISTRO EN EL BANCO";
                grd.HeaderRow.Cells[3].Width = 120;
                grd.HeaderRow.Cells[4].Text = "FECHA DE REGISTRO EN EL BANCO";
                grd.HeaderRow.Cells[4].Width = 120;
                grd.HeaderRow.Cells[5].Text = "SNIP";
                grd.HeaderRow.Cells[6].Text = "SNIP RELACION";
                grd.HeaderRow.Cells[7].Text = "HOJA DE TRAMITE";
                grd.HeaderRow.Cells[7].Width = 180;
                //grd.HeaderRow.Cells[8].Text = "FECHA DE INGRESO DE TRAMITE";
                //grd.HeaderRow.Cells[8].Width = 120;
                grd.HeaderRow.Cells[8].Text = "DOCUMENTACIÓN PRESENTADA AL MVCS";
                grd.HeaderRow.Cells[8].Width = 120;
                grd.HeaderRow.Cells[9].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[9].Width = 500;
                grd.HeaderRow.Cells[10].Text = "INTERVENCIÓN";
                grd.HeaderRow.Cells[11].Text = "UBIGEO";
                grd.HeaderRow.Cells[12].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[13].Text = "PROVINCIA";
                grd.HeaderRow.Cells[14].Text = "DISTRITO";
                grd.HeaderRow.Cells[15].Text = "LOCALIDAD";
                grd.HeaderRow.Cells[16].Text = "MONTO VIABLE";
                grd.HeaderRow.Cells[16].Width = 80;
                grd.HeaderRow.Cells[17].Text = "MONTO FASE INVERSION";
                grd.HeaderRow.Cells[17].Width = 80;
                grd.HeaderRow.Cells[18].Text = "MONTO ACTUALIZADO PIP";
                grd.HeaderRow.Cells[18].Width = 100;
                grd.HeaderRow.Cells[19].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[19].Width = 300;
                grd.HeaderRow.Cells[20].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[20].Width = 100;
                grd.HeaderRow.Cells[21].Text = "SITUACION";
                grd.HeaderRow.Cells[22].Text = "ESTADO";
                grd.HeaderRow.Cells[23].Text = "FlagRegistroFaseInversion";
                grd.HeaderRow.Cells[24].Text = "FlagCerrado";

                //grd.HeaderRow.Cells[19].Width = 150;
                grd.HeaderRow.Cells[25].Text = "FECHA DE VIABILIDAD";
                grd.HeaderRow.Cells[25].Width = 80;
                grd.HeaderRow.Cells[26].Text = "SOLICITA FINANCIAMIENTO PARA";
                grd.HeaderRow.Cells[26].Width = 100;
                grd.HeaderRow.Cells[27].Text = "ESTADO";
                grd.HeaderRow.Cells[28].Text = "VRAEM";
                grd.HeaderRow.Cells[29].Text = "DISTRITO FRONTERIZO";
                grd.HeaderRow.Cells[30].Text = "ZONA HUALLAGA";
                grd.HeaderRow.Cells[31].Text = "ZONA HELADA";
                grd.HeaderRow.Cells[32].Text = "AMBITO";

                grd.HeaderRow.Cells[33].Text = "REVISOR";
                grd.HeaderRow.Cells[34].Text = "FUNCIÓN";
                grd.HeaderRow.Cells[35].Text = "PROGRAMA";
                grd.HeaderRow.Cells[36].Text = "SUB PROGRAMA";
                grd.HeaderRow.Cells[37].Text = "SECTOR";
                grd.HeaderRow.Cells[38].Text = "MONTO DEVENGADO ACULUMULADO SOSEM";
                grd.HeaderRow.Cells[38].Width = 100;
                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];

                    //row.Cells[0].Visible = false;

                    for (int j = 0; j < 39; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "INFORMACIÓN DEL PROYECTO";
                myTable.ColumnSpan = 21;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                myTable = new TableCell();
                myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                myTable.ColumnSpan = 7;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "AMBITO DE INTERVENCION";
                myTable.ColumnSpan = 5;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                //myTable = new TableCell();
                //myTable.Text = "";
                //myTable.ColumnSpan = 1;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "INFORMACIÓN ADICIONAL";
                myTable.ColumnSpan = 6;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);

                // Agrega título en primera celda
                string Titulo = "REPORTE DE DEMANDA DEL MINISTERIO DE VIVIENDA, CONSTRUCCION Y SANEAMIENTO";
                HttpContext.Current.Response.Write(Titulo);
                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
    }
}