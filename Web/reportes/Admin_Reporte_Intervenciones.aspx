﻿<%@ Page Title="Administrador de Reportes" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="Admin_Reporte_Intervenciones.aspx.cs" Inherits="Web.reportes.Admin_Reporte_Intervenciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

       <style type="text/css">
              .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
            position: absolute;
        }

               .modalPopup {
            /*background-color:#EFF5FB;*/
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 15px;
            width: 250px;
            font-family: Calibri;
            border-radius: 18px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

   
       </style>
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" lang="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <script type="text/javascript" src="../js/jsUpdateProgress.js"></script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" >
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="60" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../js/loader.svg" style="vertical-align: middle" alt="Processing" />
                    <p style="color: White; ">Espere un momento ...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

    <asp:UpdatePanel runat="server" ID="Up_Admin" UpdateMode="Conditional">
        <ContentTemplate>

            <div style="text-align: center">
                <p style="padding-top: 12px">
                    <asp:Label ID="Label3" runat="server" Text="ADMINISTRADOR DE REPORTES - INTERVENCIONES DEL MVCS" CssClass="titulo">
                    </asp:Label>
                </p>
            </div>
            <br />
            <br />

            <div style="width: 100%;">
                <table style="margin: auto; border-collapse: separate !important; border-spacing: 4px;">
                    <tr>
                        <td align="center" class="formHeader">CÓDIGO
                        </td>
                        <td align="center" class="formHeader">PROYECTO
                        </td>
                        <td align="center" class="formHeader">PROGRAMA
                        </td>
                        <td align="center" class="formHeader">DEPARTAMENTO
                        </td>
                        <td align="center" class="formHeader">PROVINCIA
                        </td>
                        <td align="center" class="formHeader">DISTRITO
                        </td>

                    </tr>
                    <tr>

                        <td align="center" class="tdFiltro">
                            <asp:TextBox ID="txtSnipFiltro" runat="server" Width="80px"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1001" runat="server" TargetControlID="txtSnipFiltro" FilterType="Numbers" Enabled="True" />

                        </td>
                        <td align="center" class="tdFiltro">
                            <asp:TextBox ID="txtProyectoFiltro" runat="server" Width="260px"></asp:TextBox>
                        </td>
                        <td align="center" class="tdFiltro">
                            <asp:DropDownList ID="ddlSubsector" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="center" class="tdFiltro">
                            <asp:DropDownList ID="ddlDepa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepa_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="center" class="tdFiltro" runat="server">
                            <asp:DropDownList ID="ddlprov" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlprov_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="center" class="tdFiltro" runat="server">
                            <asp:DropDownList ID="ddldist" runat="server">
                            </asp:DropDownList>
                        </td>

                    </tr>
                </table>

                <div style="margin: auto; margin-top: 10px; text-align: center">
                    <asp:Button ID="btnBuscar" runat="server" Width="80px" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" />
                    <asp:Button ID="btnLimpiar" runat="server" Width="80px" Text="Limpiar" CssClass="btn btn-primary" OnClick="btnLimpiar_Click" />
                </div>

            </div>

            <div style="padding: 40px 20px">

                <div style="margin: auto; text-align: center">

                    <asp:GridView
                        ID="grdProyectos"
                        runat="server"
                        Width="100%"
                        AllowPaging="True"
                        AutoGenerateColumns="False"
                        CssClass="Grid"
                        EmptyDataText="No se han registrado proyectos."
                        EditRowStyle-BackColor="#ffffb7"
                        OnPageIndexChanging="grdProyectos_PageIndexChanging"
                        OnRowDataBound="grdProyectos_RowDataBound"
                        DataKeyNames="id_proyecto"
                        PageSize="20">
                        <Columns>

                            <asp:TemplateField HeaderText="ACTUALIZAR">
                                <HeaderStyle CssClass="GridHeader2" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnModificar" runat="server" OnClick="lnkbtnModificar_Click">
                                                            <i class="fa fa-pencil pencil" aria-hidden="true"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="AÑO">
                                <ItemTemplate>
                                    <asp:Label ID="lblPeriodo" runat="server" Text='<%# Eval("anio") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID SSP">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdProyecto" runat="server" Text='<%# Eval("id_proyecto") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PROGRAMA">
                                <HeaderStyle HorizontalAlign="Center" Height="40px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblsector" Text='<%#Eval("subsector") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SNIP">
                                <ItemTemplate>
                                    <a style="text-decoration: underline; cursor: pointer; color: #1853E1"
                                        href="<%# Eval("snip", "http://ofi2.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo={0}") %>"
                                        target="_blank" title="Hacer clic para ver el proyecto en el MEF">
                                        <%# (Convert.ToInt32(Eval("snip")) == 0 ? "NO" : Eval("snip"))%>
                                    </a>
                                </ItemTemplate>
                                <ItemStyle Width="50px" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SNIP" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblsnip" runat="server" Text='<%# Eval("snip") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="4%" VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DEPARTAMENTO">
                                <ItemTemplate>
                                    <asp:Label ID="lbldepa" runat="server" Text='<%# Eval("nom_depa") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle" Width="4%" HorizontalAlign="Center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOMBRE DEL PROYECTO">
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="lblpry" runat="server" Text='<%# Eval("nom_proyecto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA REGISTRO">
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="fecha_registro" runat="server" Text='<%# Eval("fecha_registro") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ETAPA DE INTERVENCION">
                                <ItemTemplate>
                                    <asp:Label ID="lblTipoFinanciamiento" runat="server" Text='<%# Eval("tipoFinanciamiento") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ESTADO">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server" Text='<%# Eval("estado") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FECHA INICIO">
                                <ItemTemplate>
                                    <asp:Label ID="fechaInicio" runat="server" Text='<%# Eval("fechaInicio") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="¿Financiamiento por Etapas?">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagEtapa" runat="server" Text='<%# Eval("flagEtapa") %>' Visible="false"></asp:Label>
                                    <div id="imgEtapa" runat="server" class="tooltips">
                                        <i class="fa fa-square-o o1"></i>
                                    </div>

                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ETAPA PIP (I, II ,III) <span class='tooltips'><i class='fa fa-question-circle-o fa-lg'></i><span>Solo para proyectos con financiamiento por etapas.</span></span>">
                                <ItemTemplate>
                                    <asp:Label ID="lblEtapa" runat="server" Text='<%# Eval("etapa") %>'></asp:Label>
                                    <asp:Label ID="lblIdEtapa" runat="server" Text='<%# Eval("id_etapa") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="¿Saldo? <span class='tooltips'><i class='fa fa-question-circle-o fa-lg'></i><span>Indica si la Etapa que se ejecuta es un saldo</span></span>">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagSaldo" runat="server" Text='<%# Eval("flagSaldoObra") %>' Visible="false"></asp:Label>

                                    <div id="imgSaldo" runat="server" class="tooltips">
                                        <i class="fa fa-square-o o1"></i>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="¿Elaboración del Exp. Técnico (Saldo de Obra)? <span class='tooltips'><i class='fa fa-question-circle-o fa-lg'></i><span>Indica si se esta elaborando el Expediente Técnico del Saldo de la Obra.</span></span>">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagSaldoObraExpediente" runat="server" Text='<%# Eval("flagSaldoObraExpediente") %>' Visible="false"></asp:Label>

                                    <div id="imgSaldoObraExpediente" runat="server" class="tooltips">
                                        <i class="fa fa-square-o o1"></i>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ORDEN">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrden" runat="server" Text='<%# Eval("orden") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="¿Se muestra en los reportes?">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagActivoReporte" runat="server" Text='<%# Eval("flagActivoReporte") %>' Visible="false"></asp:Label>
                                    <div id="imgActivoReporte" runat="server" class="tooltips">
                                        <i class="fa fa-square-o o1"></i>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ACTUALIZADO">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaActualizacion" runat="server" Text='<%# Eval("FechaActualizacion") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="IdProyecto" Visible="false">
                                <ItemTemplate>
                                    <%--  <asp:Label ID="lblIdProyecto" runat="server" Text='<%# Eval("id_proyecto") %>'></asp:Label>
                                    <asp:Label ID="lblIdSolicitud" runat="server" Text='<%# Eval("id_solicitudes") %>'></asp:Label>
                                    <asp:Label ID="lblIdEstadoSolicitud" runat="server" Text='<%# Eval("cod_estado") %>'></asp:Label>
                                    <asp:Label ID="lblFlagAdmisibilidad" runat="server" Text='<%# Eval("flagAdmisibilidad") %>'></asp:Label>--%>
                                </ItemTemplate>
                                <HeaderStyle />
                                <ItemStyle VerticalAlign="Middle" Width="7%" HorizontalAlign="Center" />
                            </asp:TemplateField>



                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                        <HeaderStyle Height="25px" BackColor="#2D7BBD" />
                        <AlternatingRowStyle CssClass="GridAlternating" />
                        <EditRowStyle BackColor="#FFFFB7" />
                        <RowStyle CssClass="GridRowNormal" />
                    </asp:GridView>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>


    <%-- PANEL UPDATE --%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:ModalPopupExtender ID="MPE_Update" runat="server" TargetControlID="HiddenField1"
        BackgroundCssClass="modalBackground" OkControlID="ImageButton1" DropShadow="true"
        PopupControlID="Panel_Update">
    </asp:ModalPopupExtender>

    <asp:Panel ID="Panel_Update" runat="server" Height="500px" Width="600px" CssClass="modalPopup">
        <table style="width:100%; background-color: #E2ECF3; font-size:12px">
            <tr>
                <td align="center" style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label7" CssClass="titulo2" runat="server" Font-Bold="true">ACTUALIZAR INFORMACIÓN</asp:Label>
                </td>
                <td align="right" style="width: 26px">
                    <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="Cerrar ventana"
                        ImageUrl="~/img/cancel3.png" onmouseover="this.src='../img/cancel3_2.png';" onmouseout="this.src='../img/cancel3.png';"/>
                </td>
            </tr>
        </table>

        <br />

        <asp:UpdatePanel runat="server" ID="Up_RegistroUpdate" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="margin: auto; border-collapse: separate !important; border-spacing: 6px;">
                    <tr>
                        <td style="text-align: right">SNIP:
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblSNIP" runat="server" Text="" Font-Size="14px"></asp:Label>
                            <asp:Label ID="lblIdProyecto" runat="server" Text="" Visible="false" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">NOMBRE: 
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblNombre" runat="server" Text="" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">FINANCIAMIENTO:
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblEtapaFinanciamiento" runat="server" Text="" Font-Size="14px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">¿LA INTERVENCIÓN INCLUYE VARIAS ETAPAS?
                        </td>
                        <td style="text-align: left">
                            <asp:RadioButtonList ID="rbtnEtapa" runat="server" RepeatDirection ="Horizontal" OnSelectedIndexChanged="rbtnEtapa_SelectedIndexChanged" AutoPostBack="true" >
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr runat="server" id="trEtapa" visible="false">
                        <td style="text-align: right">NRO. ETAPA:
                        </td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlEtapa" runat="server">
                                <asp:ListItem Value="" Text="SELECCIONAR"></asp:ListItem>
                                <asp:ListItem Value="1" Text="ETAPA I"></asp:ListItem>
                                <asp:ListItem Value="2" Text="ETAPA II"></asp:ListItem>
                                <asp:ListItem Value="3" Text="ETAPA III"></asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">¿SALDO DE EJECUCIÓN?
                        </td>
                        <td style="text-align: left">
                            <asp:RadioButtonList ID="rbtnSaldo" runat="server" RepeatDirection ="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rbtnSaldo_SelectedIndexChanged"   >
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr runat="server" id="trSaldoObraExpediente" visible="false">
                        <td style="text-align: right">¿ELABORACIÓN DEL EXP. TÉCNICO (SALDO DE OBRA)?
                        </td>
                        <td style="text-align: left" >
                            <asp:RadioButtonList ID="rbtnSaldoObraExpediente" runat="server" RepeatDirection ="Horizontal" >
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">ORDEN
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtOrden" runat="server" Width="40px"></asp:TextBox>
                              <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender164" runat="server" TargetControlID="txtOrden" FilterType="Numbers"  Enabled="True" />

                        </td>
                    </tr>
                     <tr>
                        <td style="text-align: right">¿SE MUESTRA EN LOS REPORTES?
                        </td>
                        <td style="text-align: left">
                            <asp:RadioButtonList ID="rbtnReporte" runat="server" RepeatDirection ="Horizontal"  >
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr><td>
                        <br />
                        </td></tr>
                    <tr>
                        <td colspan="2" style="text-align:center" >
                            <asp:Button ID="btnGuardar" runat="server" Text="ACTUALIZAR" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />

    </asp:Panel>

</asp:Content>


