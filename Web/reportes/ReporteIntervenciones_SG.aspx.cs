﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;


namespace Web.reportes
{
    public partial class ReporteIntervenciones_SG : System.Web.UI.Page
    {
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                cargaProyecto();

            }

        }

        protected void cargaProyecto()
        {
            grdProyectos.DataSource = objBLbandeja.spMON_ReporteIntervencionesSG(ddlPrograma.SelectedValue);
            grdProyectos.DataBind();

        }

        protected void grdProyectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProyectos.PageIndex = e.NewPageIndex;
            cargaProyecto();
        }

        protected void btnReporteResumen_Click(object sender, EventArgs e)
        {
            GridView grd = new GridView();
            grd.DataSource = objBLbandeja.spMON_ReporteIntervencionesSG(ddlPrograma.SelectedValue);
            grd.DataBind();

            Exportar_XLS(grd);

        }

        protected void Exportar_XLS(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "IntervencionSG.";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                grd.HeaderRow.Cells[24].Visible = false;

                for (int i = 0; i < 24; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("2F75B5", 16));
                    grd.HeaderRow.Cells[i].ForeColor = Color.White;
                    grd.HeaderRow.Cells[i].Font.Size = 10;
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                //grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[1].Text = "ID PROYECTO";
                grd.HeaderRow.Cells[2].Text = "AÑO";
                grd.HeaderRow.Cells[3].Text = "PROGRAMA";
                grd.HeaderRow.Cells[4].Text = "UNIDAD";
                grd.HeaderRow.Cells[5].Text = "SNIP";
                grd.HeaderRow.Cells[6].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[7].Text = "PROVINCIA";
                grd.HeaderRow.Cells[8].Text = "DISTRITO";
                grd.HeaderRow.Cells[8].Width = 150;
                grd.HeaderRow.Cells[9].Text = "CENTRO POBLADO";
                grd.HeaderRow.Cells[9].Width = 150;
                grd.HeaderRow.Cells[10].Text = "BENEFICIARIOS";

                grd.HeaderRow.Cells[11].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[12].Text = "NOMBRE DE PROYECTO";

                grd.HeaderRow.Cells[13].Text = "Nº CONEXIONES DE AGUA";
                grd.HeaderRow.Cells[13].Width = 120;
                grd.HeaderRow.Cells[14].Text = "Nº CONEXIONES DE ALCANTARILLADO/UBS";
                grd.HeaderRow.Cells[14].Width = 120;
                grd.HeaderRow.Cells[15].Text = "ETAPA";
                grd.HeaderRow.Cells[16].Text = "FECHA INICIO DE LA ETAPA";
                grd.HeaderRow.Cells[16].Width = 90;
                grd.HeaderRow.Cells[17].Text = "FECHA TERMINO DE LA ETAPA";
                grd.HeaderRow.Cells[17].Width = 90;
                grd.HeaderRow.Cells[18].Text = "MODALIDAD DE FINANCIAMIENTO";
                grd.HeaderRow.Cells[18].Width = 90;
                grd.HeaderRow.Cells[19].Text = "COSTO DEL PROYECTO S/.";
                grd.HeaderRow.Cells[19].Width = 90;
                grd.HeaderRow.Cells[20].Text = "AVANCE FISICO (%)";
                grd.HeaderRow.Cells[20].Width = 90;
                grd.HeaderRow.Cells[21].Text = "AVANCE FINANCIERO (%)";
                grd.HeaderRow.Cells[21].Width = 90;
                grd.HeaderRow.Cells[22].Text = "ESTADO";
                grd.HeaderRow.Cells[23].Text = "OBSERVACIONES";


                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    row.Cells[24].Visible = false;

                    for (int j = 0; j < 24; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 9;
                        row.Cells[j].Attributes.Add("class", "textmode");
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "INTERVENCIONES AL " + DateTime.Today.ToString("dd.MM.yyyy");
                myTable.ColumnSpan = 24;
                myTable.Height = 40;
                myTable.CssClass = "headerReporte";
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("2F75B5", 16));
                myTable.ForeColor = Color.White;
                myTable.Font.Bold = true;
                myTable.Font.Size = 16;
                gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                //myTable.ColumnSpan = 4;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "AMBITO DE INTERVENCION";
                //myTable.ColumnSpan = 6;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "INFORMACIÓN ADICIONAL";
                //myTable.ColumnSpan = 2;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }



        protected void ddlPrograma_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaProyecto();
        }
    }
}