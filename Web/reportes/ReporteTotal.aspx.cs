﻿using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using pry_san;
using Business;
using Entity;
using System.Text;
namespace Web.reportes
{
    public partial class ReporteTotal : System.Web.UI.Page
    {
        mProyectos objMetodo = new mProyectos();
        //mGeneral objMetodoGeneral = new mGeneral();
        BLUtil objUtil = new BLUtil();

        private BEProyecto _BEProyecto = new BEProyecto();
        BLProyecto objBLProyecto = new BLProyecto();

        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargaUbigeo(ddl_depa, "1", null, null, null);
                cargaUbigeo(ddl_prov, "2", ddl_depa.SelectedValue, null, null);
                cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);
                // cargaSubsector();



                CargaGrilla();
            }

        }

        protected void FiltrarDepa(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_prov, "2", ddl_depa.SelectedValue, null, null);
            cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);

            CargaGrilla();
            up_grd.Update();
        }

        protected void FiltrarProv(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);
            CargaGrilla();
            up_grd.Update();
        }

        protected void FiltrarDist(object sender, EventArgs e)
        {
            CargaGrilla();
            up_grd.Update();
        }

        protected void FiltrarSector(object sender, EventArgs e)
        {
            CargaGrilla();
            up_grd.Update();

        }

        protected void btnReiniciar_Click(object sender, EventArgs e)
        {
            cargaUbigeo(ddl_depa, "1", null, null, null);
            cargaUbigeo(ddl_prov, "2", ddl_depa.SelectedValue, null, null);
            cargaUbigeo(ddl_dist, "3", ddl_depa.SelectedValue, ddl_prov.SelectedValue, null);
            //cargaSubsector();
            txtSearch.Text = "";
            CargaGrilla();
            up_grd.Update();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargaGrilla();
            up_grd.Update();

        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }



        protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grd.PageIndex = e.NewPageIndex;
            CargaGrilla();
            up_grd.Update();
        }

        public void DisplayCurrentPage()
        {

            int currentPage = grd.PageIndex + 1;
            string paginado = " (Pagina " + currentPage.ToString() +
                    " de " + grd.PageCount.ToString() + ")";
            lblPaginadoAsignado.Text = paginado;

        }

        public void CargaGrilla()
        {
            DataTable dt = null;
            _BEProyecto.Criterio = cboSearch.SelectedValue;
            _BEProyecto.Buscar = txtSearch.Text;
            _BEProyecto.Cod_depa = ddl_depa.SelectedValue;
            _BEProyecto.Cod_prov = ddl_prov.SelectedValue;
            _BEProyecto.Cod_dist = ddl_dist.SelectedValue;

            //dt = objBLProyecto.spMON_ReporteGeneralMINISTRO(_BEProyecto);

            grd.DataSource = dt;
            grd.DataBind();

            LabeltotalAsignado.Text = "Total: " + dt.Rows.Count.ToString();
        }

        protected void grd_OnDataBound(object sender, EventArgs e)
        {
            DisplayCurrentPage();

        }


        protected void btnExcel_onclick(object sender, EventArgs e)
        {

            _BEProyecto.Criterio = cboSearch.SelectedValue;
            _BEProyecto.Buscar = txtSearch.Text;

            _BEProyecto.Cod_depa = ddl_depa.SelectedValue;
            _BEProyecto.Cod_prov = ddl_prov.SelectedValue;
            _BEProyecto.Cod_dist = ddl_dist.SelectedValue;
            //dt = objBLProyecto.spMON_ReporteGeneralMINISTRO(_BEProyecto);
            //  dt.DefaultView.Sort = "numero ASC";
            GridView grd2 = new GridView();
            grd2.DataSource = dt;
            grd2.DataBind();

            Exportar_XLS(grd2);

            // Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
        }


        //protected void myGridView_ItemCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {

        //        GridView gvHeader = (GridView)sender;
        //        GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

        //        TableCell myTable = new TableCell();


        //        myTable.Text = "Columna Mezclada";
        //        myTable.ColumnSpan = 2;

        //        gvHeaderRow.Cells.Add(myTable);


        //        myTable = new TableCell();
        //        myTable.Text = "Merged Column2";
        //        myTable.ColumnSpan = 2;
        //        gvHeaderRow.Cells.Add(myTable);

        //        gvHeader.Controls[0].Controls.AddAt(0, gvHeaderRow);

        //    }
        //}


        protected void Exportar_XLS(GridView grd)
        {


            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_GeneralPNSU.xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                for (int i = 17; i < 35; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                }

                for (int i = 35; i < 49; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                }
                for (int i = 49; i < 52; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                }

                for (int i = 52; i < 55; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                }

                for (int i = 55; i < 62; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("9999FF", 16));
                }


                for (int i = 62; i < 64; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("AEAAAA", 16));
                }

                for (int i = 64; i < 70; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FFF2CC", 16));
                }


                for (int i = 70; i < 77; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("D6DCE4", 16));
                }



                grd.HeaderRow.Cells[0].Text = "PROGRAMA";
                grd.HeaderRow.Cells[1].Text = "UNIDAD";
                grd.HeaderRow.Cells[2].Text = "CMD";
                grd.HeaderRow.Cells[3].Text = "AÑO DE INICIO";
                grd.HeaderRow.Cells[4].Text = "SNIP";
                grd.HeaderRow.Cells[5].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[5].Width = 500;
                grd.HeaderRow.Cells[6].Text = "MONTO SNIP";
                grd.HeaderRow.Cells[7].Text = "COSTO PROYECTO";
                grd.HeaderRow.Cells[8].Text = "POBLACION SNIP";
                grd.HeaderRow.Cells[8].Width = 100;
                grd.HeaderRow.Cells[9].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[10].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[11].Text = "PROVINCIA";
                grd.HeaderRow.Cells[12].Text = "DISTRITO";
                grd.HeaderRow.Cells[13].Text = "UBIGEO";
                grd.HeaderRow.Cells[14].Text = "AMBITO";
                grd.HeaderRow.Cells[15].Text = "TIPO DE PROYECTO";
                grd.HeaderRow.Cells[16].Text = "TIPO DE EJECUCION";
                //grd.HeaderRow.Cells[19].Width = 150;
                //grd.HeaderRow.Cells[14].Text = "FECHA DE VIABILIDAD";
                //grd.HeaderRow.Cells[14].Width = 80;
                //grd.HeaderRow.Cells[15].Text = "FECHA DE VENCIMIENTO DE VIABILIDAD";
                //grd.HeaderRow.Cells[15].Width = 100;


                grd.HeaderRow.Cells[17].Text = "PENDIENTE LEVANTAMIENTO OBSERVACIONES";
                grd.HeaderRow.Cells[17].Width = 120;
                grd.HeaderRow.Cells[18].Text = "SIN OBSERVACIONES";
                grd.HeaderRow.Cells[19].Text = "NO CONVOCADO";
                grd.HeaderRow.Cells[20].Text = "CONCURSO EN PROCESO";
                grd.HeaderRow.Cells[20].Width = 120;
                grd.HeaderRow.Cells[21].Text = "BUENA PRO OTORGADA";
                grd.HeaderRow.Cells[21].Width = 100;
                grd.HeaderRow.Cells[22].Text = "CONTRATO SUSCRITO";
                grd.HeaderRow.Cells[23].Text = "EN EJECUCION";
                grd.HeaderRow.Cells[24].Text = "CON PROBLEMAS";
                grd.HeaderRow.Cells[25].Text = "TERMINADO";
                grd.HeaderRow.Cells[26].Text = "SIN LIQUIDAR";
                grd.HeaderRow.Cells[27].Text = "LIQUIDADO";
                grd.HeaderRow.Cells[28].Text = "SUSPENDIDO";
                grd.HeaderRow.Cells[29].Text = "FECHA ULTIMA INSPECCION";
                grd.HeaderRow.Cells[29].Width = 100;
                grd.HeaderRow.Cells[30].Text = "% PROGRAMADO";
                grd.HeaderRow.Cells[31].Text = "% REAL";
                grd.HeaderRow.Cells[32].Text = "SITUACION";
                grd.HeaderRow.Cells[33].Text = "CAUSA SITUACION";
                grd.HeaderRow.Cells[34].Text = "%AVANCE FINANCIERO";

                grd.HeaderRow.Cells[35].Text = "MONTO COMPROMISO";
                grd.HeaderRow.Cells[36].Text = "COFINANCIAMIENTO";
                grd.HeaderRow.Cells[37].Text = "2006";
                grd.HeaderRow.Cells[38].Text = "2007";
                grd.HeaderRow.Cells[39].Text = "2008";
                grd.HeaderRow.Cells[40].Text = "2009";
                grd.HeaderRow.Cells[41].Text = "2010";
                grd.HeaderRow.Cells[42].Text = "2011";
                grd.HeaderRow.Cells[43].Text = "2012";
                grd.HeaderRow.Cells[44].Text = "2013";
                grd.HeaderRow.Cells[45].Text = "TRANSFERIDO ANTERIOR GESTION";
                grd.HeaderRow.Cells[46].Text = "TRANSFERIDO EN LA ACTUAL GESTION";
                grd.HeaderRow.Cells[47].Text = "TOTAL TRANSFERIDO";
                grd.HeaderRow.Cells[48].Text = "SALDO A TRANSFERIR";


                grd.HeaderRow.Cells[49].Text = "FINANCIADO EN LA ACTUAL GESTION";
                grd.HeaderRow.Cells[49].Width = 120;
                grd.HeaderRow.Cells[50].Text = "OBJETO DE FINANCIAMIENTO";
                grd.HeaderRow.Cells[50].Width = 120;
                grd.HeaderRow.Cells[51].Text = "NORMA LEGAL QUE APROBÓ LA TRANSFERENCIA";
                grd.HeaderRow.Cells[51].Width = 120;

                grd.HeaderRow.Cells[52].Text = "FECHA DE INICIO";
                grd.HeaderRow.Cells[52].Width = 120;
                grd.HeaderRow.Cells[53].Text = "FECHA DE TERMINO CONTRACTUAL";
                grd.HeaderRow.Cells[53].Width = 120;
                grd.HeaderRow.Cells[54].Text = "FECHA DE TERMINO REAL";
                grd.HeaderRow.Cells[54].Width = 120;

                grd.HeaderRow.Cells[55].Text = "EIA";
                grd.HeaderRow.Cells[56].Text = "DIGESA (TANQUE SEPTICO E INFILTRACION EN TERRENO)";
                grd.HeaderRow.Cells[56].Width = 120;
                grd.HeaderRow.Cells[57].Text = "DIGESA (OPCION TÉCNICA FAVORABLE)";
                grd.HeaderRow.Cells[57].Width = 120;
                grd.HeaderRow.Cells[58].Text = "ESTUDIO DE FUENTES DE AGUA";
                grd.HeaderRow.Cells[58].Width = 120;
                grd.HeaderRow.Cells[59].Text = "VERTIMIENTO DE AGUAS RESIDUALES";
                grd.HeaderRow.Cells[59].Width = 120;
                grd.HeaderRow.Cells[60].Text = "DISPONIBILIDAD DE TERRENO";
                grd.HeaderRow.Cells[60].Width = 120;
                grd.HeaderRow.Cells[61].Text = "CIRA";

                grd.HeaderRow.Cells[62].Text = "TIPO DE OPERADOR DE OBRA";
                grd.HeaderRow.Cells[62].Width = 120;
                grd.HeaderRow.Cells[63].Text = "ENTIDAD QUE OPERA";

                grd.HeaderRow.Cells[64].Text = "AGUA";
                grd.HeaderRow.Cells[65].Text = "ALCANTARILLADO";
                grd.HeaderRow.Cells[66].Text = "AGUA";
                grd.HeaderRow.Cells[67].Text = "ALCANTARILLADO";
                grd.HeaderRow.Cells[68].Text = "PILETAS";
                grd.HeaderRow.Cells[69].Text = "LETRINAS";

                grd.HeaderRow.Cells[70].Text = "NOMBRE";
                grd.HeaderRow.Cells[71].Text = "NOMBRE";
                grd.HeaderRow.Cells[72].Text = "TELÉFONO";
                grd.HeaderRow.Cells[73].Text = "NOMBRE";
                grd.HeaderRow.Cells[74].Text = "TELÉFONO";
                grd.HeaderRow.Cells[75].Text = "NOMBRE";
                grd.HeaderRow.Cells[76].Text = "TELÉFONO";


                //grd.HeaderRow.Cells[49].Width = 600;
                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];



                    for (int j = 0; j < 60; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "INFORMACIÓN DEL PROYECTO";
                myTable.ColumnSpan = 17;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                myTable = new TableCell();
                myTable.Text = "INFORMACIÓN DE LA SITUACION DEL PROYECTO";
                myTable.ColumnSpan = 18;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);

                myTable = new TableCell();
                myTable.Text = "MONTOS TRANSFERIDOS Y POR TRANSFERIR";
                myTable.ColumnSpan = 14;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);



                myTable = new TableCell();
                myTable.Text = "DATOS DE FINANCIAMIENTO EN LA GESTIÓN ACTUAL";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "FECHAS DE EJECUCIÓN";
                myTable.ColumnSpan = 3;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);


                myTable = new TableCell();
                myTable.Text = "CERTIFICACIONE Y AUTORIZACIONES";
                myTable.ColumnSpan = 7;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("9999FF", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "MANTENIMIENTO DE LA OBRA";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("AEAAAA", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "CONEXIONES DOMICILIARIAS EXTERNAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFF2CC", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "CONEXIONES INTRADOMICILIARIAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFF2CC", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "PILETAS Y LETRINAS";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("FFF2CC", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "RESPONSABLE DEL MVCS";
                myTable.ColumnSpan = 1;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCE4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "RESPONSABLE DE LA U.E. EN INFORMAR ACERCA DEL PROYECTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCE4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "RESPONSABLE DEL CONTRATISTA EN INFORMAR ACERCA DEL PROYECTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCE4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                myTable = new TableCell();
                myTable.Text = "RESPONSABLE DEL SUPERVISOR EN INFORMAR ACERCA DEL PROYECTO";
                myTable.ColumnSpan = 2;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("D6DCE4", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);

                // Agrega título en primera celda
                //string Titulo = "                     PROGRAMA NACIONAL DE SANEAMIENTO URBANO - EQUIPO DE ESTUDIO DE INVERSIÓN RELACIÓN DE PROYECTOS EN CARTERA PENDIENTES DE FINANCIAMIENTO DE OBRA ";
                //HttpContext.Current.Response.Write(Titulo);
                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }
    }
}