﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Data;
using System.Web.UI.HtmlControls;

namespace Web.reportes
{
    public partial class Admin_Reporte_Intervenciones : System.Web.UI.Page
    {
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string idUsuario = Session["IdUsuario"].ToString();

                if (idUsuario.Equals("481") || idUsuario.Equals("134") || idUsuario.Equals("688") || idUsuario.Equals("689") || idUsuario.Equals("658"))
                {
                    cargaSector(ddlSubsector);

                    cargaUbigeo(ddlDepa, "1", null, null, null);
                    cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                    cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

                    cargaProyectos();
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                    Response.Redirect("~/login?ms=1", true);
                }
            }
        }

        protected void cargaSector(DropDownList ddl)
        {
            List<BE_MON_BANDEJA> ListSector = new List<BE_MON_BANDEJA>();
            ListSector = objBLbandeja.F_spMON_TipoPrograma();
            ddl.DataSource = ListSector;
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Todos-", ""));
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Todos-", ""));
        }

        protected void cargaProyectos()
        {
            DataTable dt = new DataTable();

            _BE_Bandeja.snip = txtSnipFiltro.Text;
            _BE_Bandeja.nombProyecto = txtProyectoFiltro.Text;
            _BE_Bandeja.depa = ddlDepa.SelectedValue;
            _BE_Bandeja.prov = ddlprov.SelectedValue;
            _BE_Bandeja.dist = ddldist.SelectedValue;
            _BE_Bandeja.sector = ddlSubsector.SelectedValue;

            dt = objBLbandeja.spMON_ListaProyectosDetalle(_BE_Bandeja);

            grdProyectos.DataSource = dt;
            grdProyectos.DataBind();

        }
        protected void ddlDepa_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);

        }

        protected void ddlprov_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            cargaProyectos();
            Up_Admin.Update();
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtSnipFiltro.Text = "";
            txtProyectoFiltro.Text = "";
            ddlDepa.ClearSelection();
            ddlprov.ClearSelection();
            ddldist.ClearSelection();

            Up_Admin.Update();
        }

        protected void grdProyectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProyectos.PageIndex = e.NewPageIndex;
            cargaProyectos();
        }

        protected void lnkbtnModificar_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewRow row;
            boton = (LinkButton)sender;
            row = (GridViewRow)boton.NamingContainer;

            int idProyecto = Convert.ToInt32(grdProyectos.DataKeys[row.RowIndex].Values[0].ToString());

            lblIdProyecto.Text = idProyecto.ToString();
            //Label lblSNIP = (Label)grdProyectos.Rows[row.RowIndex].FindControl("lblSNIP");

            lblSNIP.Text = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblSNIP")).Text;
            lblNombre.Text = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblpry")).Text;
            lblEtapaFinanciamiento.Text = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblTipoFinanciamiento")).Text.ToUpper();

            string vEtapa = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblFlagEtapa")).Text;
            if (vEtapa.Equals(""))
            {
                rbtnEtapa.ClearSelection();
                trEtapa.Visible = false;
                ddlEtapa.SelectedValue = "";
            }
            else
            {
                rbtnEtapa.SelectedValue = vEtapa;
                trEtapa.Visible = true;

                string iEtapa = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblIdEtapa")).Text;

                if (iEtapa.Equals("") || iEtapa.Equals("0"))
                {
                    ddlEtapa.SelectedValue = "";
                    trEtapa.Visible = false;

                }
                else
                {
                    ddlEtapa.SelectedValue = iEtapa;
                }
            }

            string vSaldo = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblFlagSaldo")).Text;
            if (vSaldo.Equals(""))
            {
                rbtnSaldo.ClearSelection();

            }
            else
            {
                rbtnSaldo.SelectedValue = vSaldo;

                if (vSaldo.Equals("1"))
                {
                    trSaldoObraExpediente.Visible = true;
                }
                else
                {
                    trSaldoObraExpediente.Visible = false;
                }
            }

            string vSaldoObraExpediente = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblFlagSaldoObraExpediente")).Text;
            if (vSaldoObraExpediente.Equals("") )
            {
                rbtnSaldoObraExpediente.ClearSelection();
            }
            else
            {
                rbtnSaldoObraExpediente.SelectedValue = vSaldoObraExpediente;
            }

            txtOrden.Text = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblOrden")).Text;

            string vActivo = ((Label)grdProyectos.Rows[row.RowIndex].FindControl("lblFlagActivoReporte")).Text;
            if (vActivo.Equals(""))
            {
                rbtnReporte.ClearSelection();

            }
            else
            {
                rbtnReporte.SelectedValue = vActivo;
            }

            Up_RegistroUpdate.Update();
            MPE_Update.Show();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validarRegistro())
            {
                _BE_Bandeja.id_proyecto = Convert.ToInt32(lblIdProyecto.Text);
                _BE_Bandeja.flagEtapa = Convert.ToInt32(rbtnEtapa.SelectedValue);
                _BE_Bandeja.etapa = ddlEtapa.SelectedValue;
                _BE_Bandeja.flagSaldo = Convert.ToInt32(rbtnSaldo.SelectedValue);
                _BE_Bandeja.flagSaldoObraExpediente = Convert.ToInt32(rbtnSaldoObraExpediente.SelectedValue);
                _BE_Bandeja.orden = txtOrden.Text;
                _BE_Bandeja.flagActivo = Convert.ToInt32(rbtnReporte.SelectedValue);
                _BE_Bandeja.id_usuario = Convert.ToInt32(Session["IdUsuario"]);

                int val = objBLbandeja.U_MON_AdminitradorReporte(_BE_Bandeja);

                //int val = 1;

                if (val == 1)
                {
                    string script = "<script>swal('Éxito!','Se registró correctamente.','success');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }
                else
                {
                    string script = "<script>swal('Error...', 'Vuelva a Intentarlo después.', 'error');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
                }

                Up_RegistroUpdate.Update();
                MPE_Update.Hide();

                cargaProyectos();
                Up_Admin.Update();

            }
        }

        protected void rbtnEtapa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtnEtapa.SelectedValue.Equals("1"))
            {
                trEtapa.Visible = true;
            }
            else
            {
                trEtapa.Visible = false;
            }

            Up_RegistroUpdate.Update();
        }


        protected bool validarRegistro()
        {
            bool result = true;

            if (rbtnEtapa.SelectedValue == "")
            {
                string script = "<script>swal('Error...', 'Seleccionar si incluye varias etapas.', 'error');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (rbtnEtapa.SelectedValue == "1")
            {
                if (ddlEtapa.SelectedValue == "")
                {
                    string script = "<script>swal('Error...', 'Seleccionar número de etapa.', 'error');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (rbtnSaldo.SelectedValue == "")
            {
                string script = "<script>swal('Error...', 'Seleccionar si pertenece a un saldo de obra.', 'error');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (rbtnSaldo.SelectedValue == "1")
            {
                if (rbtnSaldoObraExpediente.SelectedValue == "")
                {
                    string script = "<script>swal('Error...', 'Seleccionar si pertenece a la Elaboración del Exp. Técnico(Saldo de obra).', 'error');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                    result = false;
                    return result;
                }
            }

            if (txtOrden.Text.Trim() == "")
            {
                string script = "<script>swal('Error...', 'Ingresar orden.', 'error');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            if (rbtnSaldo.SelectedValue == "")
            {
                string script = "<script>swal('Error...', 'Seleccionar si se mostrará en los reportes.', 'error');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                result = false;
                return result;
            }

            return result;
        }

        protected void grdProyectos_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HtmlContainerControl imgEtapa = (HtmlContainerControl)e.Row.FindControl("imgEtapa");
                HtmlContainerControl imgActivoReporte = (HtmlContainerControl)e.Row.FindControl("imgActivoReporte");
                HtmlContainerControl imgSaldo = (HtmlContainerControl)e.Row.FindControl("imgSaldo");
                HtmlContainerControl imgSaldoObraExpediente = (HtmlContainerControl)e.Row.FindControl("imgSaldoObraExpediente");

                if (((Label)e.Row.FindControl("lblFlagActivoReporte")).Text == "1")
                {
                    imgActivoReporte.InnerHtml = "<i class='fa fa-check-square-o'></i><span>" + "Se mostrará en los reportes." + "</span>";
                }

                if (((Label)e.Row.FindControl("lblFlagSaldo")).Text == "1")
                {
                    imgSaldo.InnerHtml = "<i class='fa fa-check-square-o'></i><span>" + "Saldo." + "</span>";
                }

                if (((Label)e.Row.FindControl("lblFlagSaldoObraExpediente")).Text == "1")
                {
                    imgSaldoObraExpediente.InnerHtml = "<i class='fa fa-check-square-o'></i><span>" + "Saldo." + "</span>";
                }


                if (((Label)e.Row.FindControl("lblFlagEtapa")).Text == "1")
                {
                    imgEtapa.InnerHtml = "<i class='fa fa-check-square-o'></i><span>" + "Proyecto ejecutado por etapas." + "</span>";
                }


            }

        }

        protected void rbtnSaldo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtnSaldo.SelectedValue.Equals("1"))
            {
                trSaldoObraExpediente.Visible = true;
            }
            else
            {
                trSaldoObraExpediente.Visible = false;
                rbtnSaldoObraExpediente.SelectedValue = "0";
            }
            Up_RegistroUpdate.Update();
        }
    }
}