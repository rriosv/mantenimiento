﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;

namespace Web.reportes
{
    public partial class ReporteDemanda_SG : System.Web.UI.Page
    {
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                cargaProyecto();

            }

        }
        protected void cargaProyecto()
        {
            grdProyectos.DataSource = objBLbandeja.spSOL_ReporteDemandaSG();
            grdProyectos.DataBind();

        }

        protected void grdProyectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProyectos.PageIndex = e.NewPageIndex;
            cargaProyecto();
        }

        protected void btnReporteResumen_Click(object sender, EventArgs e)
        {
            GridView grd = new GridView();
            grd.DataSource = objBLbandeja.spSOL_ReporteDemandaSG();
            grd.DataBind();

            Exportar_XLS(grd);

        }

        protected void Exportar_XLS(GridView grd)
        {

            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "DemandaSG.";
                filename = filename + DateTime.Today.ToString("dd.MM.yyyy") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                // grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;

                //grd.HeaderRow.Cells[0].Visible = false;

                for (int i = 0; i < 18; i++)
                {
                    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("2F75B5", 16));
                    grd.HeaderRow.Cells[i].ForeColor = Color.White;
                    grd.HeaderRow.Cells[i].Font.Size = 10;
                }

                //grd.HeaderRow.Cells[30].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //grd.HeaderRow.Cells[31].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));

                //grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[0].Text = "N°";
                grd.HeaderRow.Cells[1].Text = "SNIP";
                grd.HeaderRow.Cells[2].Text = "AMBITO";
                grd.HeaderRow.Cells[3].Text = "PROGRAMA";
                grd.HeaderRow.Cells[4].Text = "NOMBRE DEL PROYECTO";
                grd.HeaderRow.Cells[5].Text = "DEPARTAMENTO";
                grd.HeaderRow.Cells[6].Text = "PROVINCIA";
                grd.HeaderRow.Cells[7].Text = "DISTRITO";
                grd.HeaderRow.Cells[7].Width = 150;
                grd.HeaderRow.Cells[8].Text = "LOCALIDAD";
                grd.HeaderRow.Cells[8].Width = 150;
                grd.HeaderRow.Cells[9].Text = "UNIDAD EJECUTORA";
                grd.HeaderRow.Cells[10].Text = "POBLACIÓN BENEFICIARIA";
                grd.HeaderRow.Cells[10].Width = 120;

                grd.HeaderRow.Cells[11].Text = "MONTO DE INVERSIÓN S/";
                grd.HeaderRow.Cells[11].Width = 90;
                grd.HeaderRow.Cells[12].Text = "ADMISIBILIDAD";
                grd.HeaderRow.Cells[13].Text = "FECHA ADMISIBILIDAD";
                grd.HeaderRow.Cells[14].Text = "ELEGIBILIDAD";
                grd.HeaderRow.Cells[14].Width = 90;
                grd.HeaderRow.Cells[15].Text = "FECHA ELEGIBILIDAD";
                grd.HeaderRow.Cells[15].Width = 90;
                grd.HeaderRow.Cells[16].Text = "CALIDAD";
                grd.HeaderRow.Cells[17].Text = "FECHA CALIDAD";
                grd.HeaderRow.Cells[17].Width = 90;

                //// Recorre todas las filas
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];


                    //row.Cells[0].Visible = false;

                    for (int j = 0; j < 18; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 9;
                        row.Cells[j].Attributes.Add("class", "textmode");
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "DEMANDA DE PROYECTOS DE SANEAMIENTO (EN EVALUACION DE ADMISIBILIDAD Y CALIDAD) AL " + DateTime.Today.ToString("dd.MM.yyyy");
                myTable.ColumnSpan = 18;
                myTable.Height = 40;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.VerticalAlign = VerticalAlign.Middle;
                myTable.BackColor = Color.FromArgb(Convert.ToInt32("2F75B5", 16));
                myTable.ForeColor = Color.White;
                myTable.Font.Bold = true;
                myTable.Font.Size = 16;
                gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "ESTADO SITUACIONAL DEL PROYECTO";
                //myTable.ColumnSpan = 4;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "AMBITO DE INTERVENCION";
                //myTable.ColumnSpan = 6;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);

                //myTable = new TableCell();
                //myTable.Text = "INFORMACIÓN ADICIONAL";
                //myTable.ColumnSpan = 2;
                //myTable.HorizontalAlign = HorizontalAlign.Center;
                //myTable.BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //myTable.Font.Bold = true;
                //gvHeaderRow.Cells.Add(myTable);
                grd.Controls[0].Controls.AddAt(0, gvHeaderRow);

                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);


                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();
            }
        }

    }
}