﻿using System;


namespace Web.reportes
{
    public partial class ReportesMonitoreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string token = Session["clave"].ToString();

            string UrlFrontEnd = System.Configuration.ConfigurationManager.AppSettings["UrlFrontEnd"].ToString();

            ifrm.Attributes["src"] = UrlFrontEnd + "reporte/index/" + token;

            //ifrm.Attributes["src"] = "http://10.169.7.146:8082/reporte/index/" + token;
            ifrm.Attributes["width"] = "100%";
            //ifrm.Attributes["height"] = "100%";
            ifrm.Attributes["scrolling"] = "yes";
            ifrm.Attributes["frameborder"] = "0";
        }
    }
}