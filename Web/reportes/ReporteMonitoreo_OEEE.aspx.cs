﻿using Business;
using System;
using System.Data;

namespace Web.reportes
{
    public partial class ReporteMonitoreo_OEEE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            BL_MON_BANDEJA _objBLBandeja = new BL_MON_BANDEJA();

            dt = _objBLBandeja.spMON_ReporteMonitoreoOEEE();
            xgrdReporte.DataSource = dt;
            xgrdReporte.DataBind();

        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            xgrdExporter.WriteXlsToResponse();
        }
    }
}