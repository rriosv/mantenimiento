﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Entity;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;


namespace Web.reportes
{
    public partial class Reporte_SaneamientoObras : System.Web.UI.Page
    {
        DataTable dtDatos = new DataTable();
        BL_MON_BANDEJA _objBandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _EntBandeja = new BE_MON_BANDEJA();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lblTitulo.Text = "(29/07/2016 al " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
                cargarGeneral();
            }
        }

        protected void lnkbtnDetalle_Click(object sender, EventArgs e)
        {

        }

        protected void cargarGeneral()
        {
            dtDatos = new DataTable();

            _EntBandeja.id_sector = Convert.ToInt32(0);
            _EntBandeja.Id_tipo = 1;

            dtDatos = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);

            DataTable table = new DataTable();
            table.Columns.Add("Estado", typeof(string));

            table.Columns.Add("proyectosPNSU", typeof(int));
            table.Columns.Add("costoProyectoPNSU", typeof(double));
            table.Columns.Add("InversionPNSU", typeof(double));
            table.Columns.Add("BeneficiarioPNSU", typeof(int));

            table.Columns.Add("proyectosPNSR", typeof(int));
            table.Columns.Add("costoProyectoPNSR", typeof(double));
            table.Columns.Add("InversionPNSR", typeof(double));
            table.Columns.Add("BeneficiarioPNSR", typeof(int));

            table.Columns.Add("proyectosTotal", typeof(int));
            table.Columns.Add("costoProyectoTotal", typeof(double));
            table.Columns.Add("InversionTotal", typeof(double));
            table.Columns.Add("BeneficiarioTotal", typeof(int));

            table.Rows.Add("POR CONVOCAR", 0, "0", "0", 0, 0, "0", "0", 0, 0);
            table.Rows.Add("CONVOCADOS", 0, "0", "0", 0, 0, "0", "0", 0, 0);
            table.Rows.Add("INICIADOS", 0, "0", "0", 0, 0, "0", "0", 0, 0);
            table.Rows.Add("REINICIADOS", 0, "0", "0", 0, 0, "0", "0", 0, 0);
            table.Rows.Add("PARALIZADAS", 0, "0", "0", 0, 0, "0", "0", 0, 0);
            table.Rows.Add("CONCLUIDOS", 0, "0", "0", 0, 0, "0", "0", 0, 0);

            foreach (DataRow row in table.Rows) // Loop over the items.
            {
                foreach (DataRow rowDatos in dtDatos.Rows)
                {
                    if (row["Estado"].ToString().Equals(rowDatos["EstadoMVCS"].ToString()) && rowDatos["Programa"].ToString().Equals("PNSU"))
                    {
                        row["proyectosPNSU"] = rowDatos["NroProyectos"].ToString();
                        row["costoProyectoPNSU"] = Convert.ToDouble(rowDatos["TotalInversion"]).ToString("N2");
                        row["InversionPNSU"] = Convert.ToDouble(rowDatos["CostoMVCS"]).ToString("N2");
                        row["BeneficiarioPNSU"] = rowDatos["Poblacion"].ToString();
                    }

                    if (row["Estado"].ToString().Equals(rowDatos["EstadoMVCS"].ToString()) && rowDatos["Programa"].ToString().Equals("PNSR"))
                    {
                        row["proyectosPNSR"] = rowDatos["NroProyectos"].ToString();
                        row["costoProyectoPNSR"] = Convert.ToDouble(rowDatos["TotalInversion"]).ToString("N2");
                        row["InversionPNSR"] = Convert.ToDouble(rowDatos["CostoMVCS"]).ToString("N2");
                        row["BeneficiarioPNSR"] = rowDatos["Poblacion"].ToString();
                    }
                }

                row["proyectosTotal"] = (Convert.ToInt32(row["proyectosPNSU"].ToString()) + Convert.ToInt32(row["proyectosPNSR"])).ToString();
                row["costoProyectoTotal"] = (Convert.ToDouble(row["costoProyectoPNSU"].ToString()) + Convert.ToDouble(row["costoProyectoPNSR"])).ToString("N2");
                row["InversionTotal"] = (Convert.ToDouble(row["InversionPNSU"].ToString()) + Convert.ToDouble(row["InversionPNSR"])).ToString("N2");
                row["BeneficiarioTotal"] = (Convert.ToInt32(row["BeneficiarioPNSU"].ToString()) + Convert.ToInt32(row["BeneficiarioPNSR"])).ToString();
            }

            table.Compute("Sum(proyectosPNSU)", "").ToString();

            lblNroPNSU.Text = table.Compute("Sum(proyectosPNSU)", "").ToString();
            lblNroPNSR.Text = table.Compute("Sum(proyectosPNSR)", "").ToString();
            lblNroTOTAL.Text = table.Compute("Sum(proyectosTotal)", "").ToString();

            lblTotalCostoPNSU.Text = Convert.ToDouble(table.Compute("Sum(costoProyectoPNSU)", "")).ToString("N2");
            lblTotalCostoPNSR.Text = Convert.ToDouble(table.Compute("Sum(costoProyectoPNSR)", "")).ToString("N2");
            lblTotalCostoTOTAL.Text = Convert.ToDouble(table.Compute("Sum(costoProyectoTotal)", "")).ToString("N2");

            lblTotalBenePNSU.Text = Convert.ToDouble(table.Compute("Sum(BeneficiarioPNSU)", "")).ToString("N0");
            lblTotalBenePNSR.Text = Convert.ToDouble(table.Compute("Sum(BeneficiarioPNSR)", "")).ToString("N0");
            lblTotalBeneTOTAL.Text = Convert.ToDouble(table.Compute("Sum(BeneficiarioTotal)", "")).ToString("N0");

            lblTotalInverPNSU.Text = Convert.ToDouble(table.Compute("Sum(InversionPNSU)", "")).ToString("N2");
            lblTotalInverPNSR.Text = Convert.ToDouble(table.Compute("Sum(InversionPNSR)", "")).ToString("N2");
            lblTotalInverTOTAL.Text = Convert.ToDouble(table.Compute("Sum(InversionTotal)", "")).ToString("N2");

            grdConsolidado.DataSource = table;
            grdConsolidado.DataBind();
        }


        protected void LbPnsuN_Click(object sender, EventArgs e)
        {
            LinkButton LbPnsuN = (LinkButton)sender;
            HiddenField HfEstado = (HiddenField)LbPnsuN.NamingContainer.FindControl("HfEstado");

            DataTable dt;

            _EntBandeja.id_sector = 1;
            _EntBandeja.Id_tipo = 2;
            _EntBandeja.Estado = HfEstado.Value;
            dt = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);
            Session["obj"] = dt;
            grdDetalle.DataSource = dt;
            grdDetalle.DataBind();
            OcultarColumnas(grdDetalle, HfEstado.Value);

            lblTituloDetalle.Text = HfEstado.Value;
            Hfest.Value = HfEstado.Value;
            LblTotalRegistros.Text = "Se encontrarón " + dt.Rows.Count.ToString() + " registros.";
            MPE_Detalle.Show();
        }
        protected void LbPnsrN_Click(object sender, EventArgs e)
        {
            LinkButton LbPnsuN = (LinkButton)sender;
            HiddenField HfEstado = (HiddenField)LbPnsuN.NamingContainer.FindControl("HfEstado");
            DataTable dt;
            _EntBandeja.id_sector = 3;
            _EntBandeja.Id_tipo = 2;
            _EntBandeja.Estado = HfEstado.Value;
            dt = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);
            Session["obj"] = dt;
            grdDetalle.DataSource = dt;
            grdDetalle.DataBind();
            OcultarColumnas(grdDetalle, HfEstado.Value);

            lblTituloDetalle.Text = HfEstado.Value;
            Hfest.Value = HfEstado.Value;
            LblTotalRegistros.Text = "Se encontrarón " + dt.Rows.Count.ToString() + " registros.";
            MPE_Detalle.Show();
        }
        protected void LbTN_Click(object sender, EventArgs e)
        {
            LinkButton LbPnsuN = (LinkButton)sender;
            HiddenField HfEstado = (HiddenField)LbPnsuN.NamingContainer.FindControl("HfEstado");
            DataTable dt;
            _EntBandeja.id_sector = 0;
            _EntBandeja.Id_tipo = 2;
            _EntBandeja.Estado = HfEstado.Value;
            dt = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);
            Session["obj"] = dt;
            grdDetalle.DataSource = dt;
            grdDetalle.DataBind();
            OcultarColumnas(grdDetalle, HfEstado.Value);

            lblTituloDetalle.Text = HfEstado.Value;
            Hfest.Value = HfEstado.Value;
            LblTotalRegistros.Text = "Se encontrarón " + dt.Rows.Count.ToString() + " registros.";
            MPE_Detalle.Show();
        }


        void OcultarColumnas(GridView gv, string estado)
        {
            //FECHA FIN 4
            //FECHA INICIO 5
            //FECHA FIN ÚLTIMA PARALIZACIÓN 6
            //FECHA ÚLTIMA CONVOCATORIA 7

            if (estado == "POR CONVOCAR")
            {
                gv.Columns[6].Visible = false;
                gv.Columns[7].Visible = false;
                gv.Columns[8].Visible = false;
                gv.Columns[9].Visible = false;
            }
            else if (estado == "CONVOCADOS")
            {
                gv.Columns[6].Visible = false;
                gv.Columns[7].Visible = false;
                gv.Columns[8].Visible = false;
                gv.Columns[9].Visible = true;
            }
            else if (estado == "INICIADOS")
            {
                gv.Columns[6].Visible = false;
                gv.Columns[7].Visible = true;
                gv.Columns[8].Visible = false;
                gv.Columns[9].Visible = false;
            }
            else if (estado == "REINICIADOS")
            {
                gv.Columns[6].Visible = false;
                gv.Columns[7].Visible = false;
                gv.Columns[8].Visible = true;
                gv.Columns[9].Visible = false;
            }
            else if (estado == "PARALIZADAS")
            {
                gv.Columns[6].Visible = false;
                gv.Columns[7].Visible = false;
                gv.Columns[8].Visible = false;
                gv.Columns[9].Visible = false;
            }
            else if (estado == "CONCLUIDOS")
            {
                gv.Columns[6].Visible = true;
                gv.Columns[7].Visible = false;
                gv.Columns[8].Visible = false;
                gv.Columns[9].Visible = false;
            }

        }
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            // - Exporta Gridview a Excel, crea planilla completa aunque el Gridview tenga páginas

            DataTable dtInfo = (DataTable)Session["obj"];

            DataTable dtInfo2 = dtInfo.Clone();
            dtInfo2.Columns.Add("CostoMVCS2", typeof(string));
            dtInfo2.Columns.Add("monto_inversion2", typeof(string));

            foreach (DataRow dr in dtInfo.Rows)
            {
                DataRow DR1;
                DR1 = dtInfo2.NewRow();
                DR1["Programa"] = dr["Programa"];
                DR1["cod_snip"] = dr["cod_snip"];
                DR1["nom_proyecto"] = dr["nom_proyecto"];
                DR1["num_hab_snip"] = dr["num_hab_snip"];
                DR1["CostoMVCS"] = dr["CostoMVCS"];
                DR1["monto_inversion"] = dr["monto_inversion"];
                DR1["fechaFin"] = dr["fechaFin"];
                DR1["fechaInicio"] = dr["fechaInicio"];
                DR1["FechaFinParalizacion"] = dr["FechaFinParalizacion"];
                DR1["FechaConvocatoria"] = dr["FechaConvocatoria"];
                DR1["EstadoMVCS"] = dr["EstadoMVCS"];
                DR1["CostoMVCS2"] = Convert.ToDecimal(dr["CostoMVCS"]).ToString("N2");
                DR1["monto_inversion2"] = Convert.ToDecimal(dr["monto_inversion"]).ToString("N2");
                dtInfo2.Rows.Add(DR1);
            }

            GridView grd = new GridView();

            grd.Columns.Add(new BoundField { DataField = "Programa" });
            grd.Columns.Add(new BoundField { DataField = "cod_snip" });
            grd.Columns.Add(new BoundField { DataField = "nom_proyecto" });
            grd.Columns.Add(new BoundField { DataField = "num_hab_snip" });
            grd.Columns.Add(new BoundField { DataField = "CostoMVCS2" });
            grd.Columns.Add(new BoundField { DataField = "monto_inversion2" });
            grd.Columns.Add(new BoundField { DataField = "fechaFin" });
            grd.Columns.Add(new BoundField { DataField = "fechaInicio" });
            grd.Columns.Add(new BoundField { DataField = "FechaFinParalizacion" });
            grd.Columns.Add(new BoundField { DataField = "FechaConvocatoria" });
            grd.Columns.Add(new BoundField { DataField = "EstadoMVCS" });

            if (Hfest.Value == "POR CONVOCAR")
            {
                grd.Columns[6].Visible = false;
                grd.Columns[7].Visible = false;
                grd.Columns[8].Visible = false;
                grd.Columns[9].Visible = false;
            }
            else if (Hfest.Value == "CONVOCADOS")
            {
                grd.Columns[6].Visible = false;
                grd.Columns[7].Visible = false;
                grd.Columns[8].Visible = false;
                grd.Columns[9].Visible = true;
            }
            else if (Hfest.Value == "INICIADOS")
            {
                grd.Columns[6].Visible = false;
                grd.Columns[7].Visible = true;
                grd.Columns[8].Visible = false;
                grd.Columns[9].Visible = false;
            }
            else if (Hfest.Value == "REINICIADOS")
            {
                grd.Columns[6].Visible = false;
                grd.Columns[7].Visible = false;
                grd.Columns[8].Visible = true;
                grd.Columns[9].Visible = false;
            }
            else if (Hfest.Value == "PARALIZADAS")
            {
                grd.Columns[6].Visible = false;
                grd.Columns[7].Visible = false;
                grd.Columns[8].Visible = false;
                grd.Columns[9].Visible = false;
            }
            else if (Hfest.Value == "CONCLUIDOS")
            {
                grd.Columns[6].Visible = true;
                grd.Columns[7].Visible = false;
                grd.Columns[8].Visible = false;
                grd.Columns[9].Visible = false;
            }


            grd.AutoGenerateColumns = false;
            grd.DataSource = dtInfo2;
            grd.DataBind();


            if (grd.Rows.Count > 0 && grd.Visible == true)
            {

                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter sw = new System.IO.StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();
                HtmlForm form = new HtmlForm();

                string filename = "Reporte_ConsultaProyecto_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";

                grd.EnableViewState = false;
                grd.AllowPaging = false;
                grd.AllowSorting = false;
                grd.DataBind();

                grd.HeaderStyle.Reset();

                // grd.HeaderRow.BackColor = System.Drawing.Color.DarkBlue;
                grd.HeaderRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                grd.HeaderRow.ForeColor = System.Drawing.Color.Black;
                grd.HeaderRow.Font.Size = 11;

                //grd.HeaderRow.Cells[0].Visible = false;

                //for (int i = 21; i < 28; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("F2DCDB", 16));
                //}

                //for (int i = 28; i < 33; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("FDE9D9", 16));
                //}

                //for (int i = 33; i < 39; i++)
                //{
                //    grd.HeaderRow.Cells[i].BackColor = Color.FromArgb(Convert.ToInt32("B7DEE8", 16));
                //}


                grd.HeaderRow.Cells[0].Text = "PROGRAMA";
                grd.HeaderRow.Cells[1].Text = "SNIP";
                grd.HeaderRow.Cells[2].Text = "NOMBRE DE PROYECTO";
                grd.HeaderRow.Cells[3].Text = "BENEFICIARIOS";
                grd.HeaderRow.Cells[4].Text = "COSTO DEL PROYECTO(S/.)";
                grd.HeaderRow.Cells[5].Text = "INVERSIÓN DEL MVCS (S/.)";
                grd.HeaderRow.Cells[6].Text = "FECHA TERMINO REAL";
                grd.HeaderRow.Cells[6].Width = 80;
                grd.HeaderRow.Cells[7].Text = "FECHA INICIO";
                grd.HeaderRow.Cells[7].Width = 80;
                grd.HeaderRow.Cells[8].Text = "FECHA FIN ÚLTIMA PARALIZACIÓN";
                grd.HeaderRow.Cells[8].Width = 100;
                grd.HeaderRow.Cells[9].Text = "FECHA ÚLTIMA CONVOCATORIA";
                grd.HeaderRow.Cells[9].Width = 110;
                grd.HeaderRow.Cells[10].Text = "ESTADO";
                grd.HeaderRow.Cells[10].Width = 115;

                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    GridViewRow row = grd.Rows[i];

                    for (int j = 0; j <= 10; j++)
                    {
                        row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[j].Font.Size = 10;
                        //    row.Cells[j].Visible = false;
                        //    //    row.Cells[1].Visible = false;
                        //    //    if (j == 3 || j == 6){ row.Cells[j].Attributes.Add("class", "num1");  // formato numero 
                        //    //    }
                        //    //    else { row.Cells[j].Attributes.Add("class", "textmode");  // formato texto
                        //    //    }
                    }
                }
                // Define estilo para formato texto y numérico


                GridViewRow gvHeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell myTable = new TableCell();


                myTable.Text = "INFORMACIÓN DEL PROYECTO";
                myTable.ColumnSpan = 19;
                myTable.HorizontalAlign = HorizontalAlign.Center;
                myTable.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32("EEECE1", 16));
                myTable.Font.Bold = true;
                gvHeaderRow.Cells.Add(myTable);


                string style = @"";

                page.EnableEventValidation = false;
                page.DesignerInitialize();
                page.Controls.Add(form);
                form.Controls.Add(grd);
                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                // Escribe estilo
                Response.Write(style);

                // Agrega título en primera celda
                //string Titulo = "REPORTE DE CONSULTA DE PROYECTOS DEL MINISTERIO DE VIVIENDA, CONSTRUCCION Y SANEAMIENTO AL " + DateTime.Now.ToString("dd-MM-yyyy");
                //HttpContext.Current.Response.Write(Titulo);
                Response.Write(sb.ToString());
                //Session["StringBuilder"] = sb.ToString();
                //Response.Redirect("../Solicitudes/Excel_Revisiones.aspx");
                // Response.Write("<script> abrirVentana('Excel_Revisiones.aspx','Reporte',800,600,'no','yes','no','no','no','yes','no')</script>");
                // ClientScript.RegisterStartupScript(GetType(), "MostrarReporteA3", "<script>abrirVentana('Excel_Revisiones.aspx',800,600,'no','yes','no','no','no','yes','no');</script>");
                Response.End();

            }
        }

        protected void LbPnsuT_Click(object sender, EventArgs e)
        {
            LinkButton LbPnsuT = (LinkButton)sender;

            DataTable dt;

            _EntBandeja.id_sector = 1;
            _EntBandeja.Id_tipo = 2;
            _EntBandeja.Estado = "";
            dt = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);
            Session["obj"] = dt;
            grdDetalle.DataSource = dt;
            grdDetalle.DataBind();
            OcultarColumnas(grdDetalle, "");

            lblTituloDetalle.Text = "TODOS";
            Hfest.Value = "";
            LblTotalRegistros.Text = "Se encontrarón " + dt.Rows.Count.ToString() + " registros.";
            MPE_Detalle.Show();
        }
        protected void LblPnsrT_Click(object sender, EventArgs e)
        {
            LinkButton LbPnsuT = (LinkButton)sender;

            DataTable dt;

            _EntBandeja.id_sector = 3;
            _EntBandeja.Id_tipo = 2;
            _EntBandeja.Estado = "";
            dt = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);
            Session["obj"] = dt;
            grdDetalle.DataSource = dt;
            grdDetalle.DataBind();
            OcultarColumnas(grdDetalle, "");

            lblTituloDetalle.Text = "TODOS";
            Hfest.Value = "";
            LblTotalRegistros.Text = "Se encontrarón " + dt.Rows.Count.ToString() + " registros.";
            MPE_Detalle.Show();
        }
        protected void LblTotT_Click(object sender, EventArgs e)
        {
            LinkButton LbPnsuT = (LinkButton)sender;

            DataTable dt;

            _EntBandeja.id_sector = 0;
            _EntBandeja.Id_tipo = 2;
            _EntBandeja.Estado = "";
            dt = _objBandeja.spMON_ReporteSaneamientoObras(_EntBandeja);
            Session["obj"] = dt;
            grdDetalle.DataSource = dt;
            grdDetalle.DataBind();
            OcultarColumnas(grdDetalle, "");

            lblTituloDetalle.Text = "TODOS";
            Hfest.Value = "";
            LblTotalRegistros.Text = "Se encontrarón " + dt.Rows.Count.ToString() + " registros.";
            MPE_Detalle.Show();
        }

        protected void imgbtnGuia_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}