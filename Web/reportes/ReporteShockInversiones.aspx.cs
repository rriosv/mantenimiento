﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Business;
using Entity;

namespace Web.reportes
{
    public partial class ReporteShockInversiones : System.Web.UI.Page
    {
        BL_MON_BANDEJA objBLbandeja = new BL_MON_BANDEJA();
        BE_MON_BANDEJA _BE_Bandeja = new BE_MON_BANDEJA();
        DataTable dt;

     
        protected void Page_Load(object sender, EventArgs e)
        {
            txtAnioMayor.Attributes.Add("onkeypress", "javascript:return ValidNum(event);");
            txtAnioMenor.Attributes.Add("onkeypress", "javascript:return ValidNum(event);");
            string usuario = Session["IdUsuario"].ToString();
            
            if (!IsPostBack)
            {
              
                txtAnioMayor.Attributes.Add("onkeypress", "javascript:return ValidNum(event);");
                txtAnioMenor.Attributes.Add("onkeypress", "javascript:return ValidNum(event);");
                cargaUbigeo(ddlDepa, "1", null, null, null);
                cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
                cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);
                cargaEstado();
                //if (Session["CodSubsector"].ToString() == "1" || Session["CodSubsector"].ToString() == "2")
                //{
                //    ddlSubsector.Enabled = false;
                //    ddlSubsector.SelectedValue = Session["CodSubsector"].ToString();
                //}

                cargaTipoEjecucion(ddlTecnico);

                _BE_Bandeja.id_usuario = Convert.ToInt32(usuario);
                _BE_Bandeja.tipo = "1";

                BE_MON_BANDEJA _ENTBandeja = new BE_MON_BANDEJA();
                _ENTBandeja = objBLbandeja.F_spMON_ObtieneInfo(_BE_Bandeja);
                // dt = objBLbandeja.spMON_ObtieneInfo(_BE_Bandeja);

                if (_ENTBandeja != null)
                {
                    if (_ENTBandeja.nivel == "T")
                    {
                        ddlTecnico.SelectedValue = _ENTBandeja.id_tecnico.ToString();
                        ddlTecnico.Enabled = false;
                    }

                }
                cargaTipo();

                //cargaBandejaMonitor(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue);
                //cargaReporteShocInversion(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtAnioMayor.Text != "" ? Convert.ToInt32(txtAnioMayor.Text):0, txtAnioMenor.Text  != "" ? Convert.ToInt32(txtAnioMenor.Text):0);

                string vSector = Session["CodSubsector"].ToString();
                if (vSector.Equals("1") || vSector.Equals("2") || vSector.Equals("3"))
                {
                    ddlPrograma.SelectedValue = vSector;
                }
                else
                {
                    ddlPrograma.SelectedValue = "1";
                }
            }


            cargaReporteShocInversion(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlPrograma.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtAnioMayor.Text != "" ? Convert.ToInt32(txtAnioMayor.Text) : 0, txtAnioMenor.Text != "" ? Convert.ToInt32(txtAnioMenor.Text) : 0);

            //Up_grdBandeja.Update();
        }
        #region FILTRO

        //protected void cargaSector()
        //{
        //    ddlSubsector.DataSource = objBLbandeja.spMON_TipoPrograma();
        //    ddlSubsector.DataTextField = "nombre";
        //    ddlSubsector.DataValueField = "valor";
        //    ddlSubsector.DataBind();

        //    ddlSubsector.Items.Insert(0, new ListItem("-Seleccione-", ""));
        //}
        protected void cargaEstado()
        {
            ddlSubsector.DataSource = objBLbandeja.F_spMON_ListarEstado();
            ddlSubsector.DataTextField = "nombre";
            ddlSubsector.DataValueField = "valor";
            ddlSubsector.DataBind();

            ddlSubsector.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }
        protected void cargaTipo()
        {
            ddlTipo.DataSource = objBLbandeja.F_spMON_TipoFinanciamiento();
            ddlTipo.DataTextField = "nombre";
            ddlTipo.DataValueField = "valor";
            ddlTipo.DataBind();

            ddlTipo.Items.Insert(0, new ListItem("-Seleccione-", ""));
        }

        //protected void cargaTecnico(DropDownList ddl)
        //{
        //    ddl.DataSource = objBLbandeja.spMON_ListarTecnico(ddlSubsector.SelectedValue);
        //    ddl.DataTextField = "tecnico";
        //    ddl.DataValueField = "valor";
        //    ddl.DataBind();

        //    ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        //}
        protected void cargaTipoEjecucion(DropDownList ddl)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarTipoEjecucion();
            ddl.DataTextField = "tecnico";
            ddl.DataValueField = "valor";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void ddlDepa_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddlprov, "2", ddlDepa.SelectedValue, null, null);
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

            Up_Filtro.Update();
        }

        protected void ddlprov_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cargaUbigeo(ddldist, "3", ddlDepa.SelectedValue, ddlprov.SelectedValue, null);

            Up_Filtro.Update();
        }

        protected void cargaUbigeo(DropDownList ddl, string tipo, string depa, string prov, string dist)
        {
            ddl.DataSource = objBLbandeja.F_spMON_ListarUbigeo(tipo, depa, prov, dist);
            ddl.DataTextField = "nombre";
            ddl.DataValueField = "codigo";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("-Seleccione-", ""));

        }

        protected void ddlSubsector_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //cargaTecnico(ddlTecnico);
            //Up_Filtro.Update();
        }

        protected void btnBuscar_OnClick(object sender, EventArgs e)
        {
            //cargaBandejaMonitor(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue);
            cargaReporteShocInversion(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtAnioMayor.Text != "" ? Convert.ToInt32(txtAnioMayor.Text) : 0, txtAnioMenor.Text != "" ? Convert.ToInt32(txtAnioMenor.Text) : 0);

            //Up_grdBandeja.Update();

        }

        protected void btnLimpiar_OnClick(object sender, EventArgs e)
        {
            txtSnipFiltro.Text = "";
            txtProyectoFiltro.Text = "";
            ddlDepa.SelectedValue = "";
            ddlprov.SelectedValue = "";
            ddldist.SelectedValue = "";
            ddlSubsector.SelectedValue = "";
            ddlTecnico.SelectedValue = "";
            ddlTipo.SelectedValue = "";
            Up_Filtro.Update();

            //cargaReporteShocInversion(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlSubsector.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtAnioMayor.Text != "" ? Convert.ToInt32(txtAnioMayor.Text) : 0, txtAnioMenor.Text != "" ? Convert.ToInt32(txtAnioMenor.Text) : 0);
            //Up_grdBandeja.Update();
        }
        #endregion

        //protected void cargaBandejaMonitor(string snip, string proyecto, string depa, string prov, string dist, string sector, string tecnico, string tipo)
        //{
        //    _BE_Bandeja.snip = snip;
        //    _BE_Bandeja.nombProyecto = proyecto;
        //    _BE_Bandeja.depa = depa;
        //    _BE_Bandeja.prov = prov;
        //    _BE_Bandeja.dist = dist;
        //    _BE_Bandeja.sector = sector;
        //    _BE_Bandeja.tecnicofiltro = tecnico;
        //    _BE_Bandeja.tipoFinanciamientoFiltro = tipo;
        //    dt = objBLbandeja.spMON_BandejaMonitor(_BE_Bandeja);
        //    grdBandeja.DataSource = dt;
        //    grdBandeja.DataBind();
        //    LabeltotalAsignado.Text = "Total: " + dt.Rows.Count.ToString();
        //}

        protected void cargaReporteShocInversion(string snip, string proyecto, string depa, string prov, string dist, string sector, string tecnico, string tipo, int anioMayor, int anioMenor)
        {
            _BE_Bandeja.snip = snip;
            _BE_Bandeja.nombProyecto = proyecto;
            _BE_Bandeja.depa = depa;
            _BE_Bandeja.prov = prov;
            _BE_Bandeja.dist = dist;
            _BE_Bandeja.sector = sector;
            _BE_Bandeja.tecnicofiltro = tecnico;
            _BE_Bandeja.tipoFinanciamientoFiltro = tipo;
            _BE_Bandeja.estadoProyecto = anioMayor;
            _BE_Bandeja.tipo = anioMenor.ToString();
            dt = objBLbandeja.spMON_ReporteDetalladoPersonalizable(_BE_Bandeja);

            xgrdBandeja.DataSource = dt;
            xgrdBandeja.DataBind();
            LabeltotalAsignado.Text = "Total: " + dt.Rows.Count.ToString();
        }
       
   
        //protected void btnExportar_Click(object sender, ImageClickEventArgs e)
        //{
           
        //    // Exportar_Excel("Shock_Inversiones.xls",grdBandeja);
        //}

        protected void btnExcel_Click(object sender, EventArgs e)
        {

            xgrdExporterBandeja.WriteXlsToResponse("ReporteDetalladoMonitoreo." + DateTime.Now.ToString("yyyy.MM.dd"));

        }

        protected void ddlPrograma_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaReporteShocInversion(txtSnipFiltro.Text, txtProyectoFiltro.Text, ddlDepa.SelectedValue, ddlprov.SelectedValue, ddldist.SelectedValue, ddlPrograma.SelectedValue, ddlTecnico.SelectedValue, ddlTipo.SelectedValue, txtAnioMayor.Text != "" ? Convert.ToInt32(txtAnioMayor.Text) : 0, txtAnioMenor.Text != "" ? Convert.ToInt32(txtAnioMenor.Text) : 0);

        }
    }
}