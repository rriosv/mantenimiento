﻿<%@ Page Title="Notificación" Language="C#" MasterPageFile="~/MasterPages/Sistema.Master" AutoEventWireup="true" CodeBehind="Bandeja.aspx.cs" Inherits="Web.Notificacion.Bandeja" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/20.2.4/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/20.2.4/css/dx.light.css" />
    <style type="text/css">
        .dx-datagrid-content .dx-datagrid-table .dx-row > td[role=gridcell] {
            vertical-align: middle;
        }

        .bg-atendido {
            margin: 1px 5px;
            padding: 5px 10px 5px 10px;
            color: #fff;
            background-color: #5cb85c;
            border-color: #4cae4c;
        }

        .bg-pendiente {
            margin: 1px 5px;
            padding: 5px 10px 5px 10px;
            color: #fff;
            background-color: #eb7339;
            border-color: #eb7339;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="padding: 2% 3% 2% 4%">
        <center>
            <p>
                <span class="titlePage">BANDEJA DE NOTIFICACIONES</span>
            </p>
        </center>
        <br />
        <br />

        <div class="row">
            <div class="col-xs-2 col-sm-1">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="rbSnip" value="option1" checked>
                    <label class="form-check-label" for="exampleRadios1">
                        SNIP
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="rbUnificado" value="option2">
                    <label class="form-check-label" for="exampleRadios2">
                        UNIFICADO
                    </label>
                </div>

            </div>
            <div class="col-xs-4 col-sm-2">
                <input id="txtCodigo" class="form-control" type="text" />
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-2">
                <div id="btnBuscar" class="btn btn-primary"><b><i class="glyphicon glyphicon-search"></i>BUSCAR</b></div>
                <div id="btnClear" class="btn btn-default"><b><i class="icon-broom"></i>LIMPIAR</b></div>
            </div>
        </div>

        <br />
        <br />
        <div class="row">
            <div class="col-xs-12">
                <div id="gridContainer"></div>
            </div>
        </div>

        <asp:HiddenField runat="server" ID="hfIdSesion" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceFooter" runat="server">
    <script src="https://cdn3.devexpress.com/jslib/20.2.4/js/dx.all.js"></script>
    <script type="text/javascript" language="javascript">
        var st;
        st = {
            columns: [
                {
                    caption: "SNIP",
                    dataField: "snip",
                    width: 80,
                    alignment: "center",
                },
                {
                    caption: "UNIFICADO",
                    dataField: "unificado",
                    width: 100,
                    alignment: "center",
                },
                {
                    caption: "FECHA NOTIFICACIÓN",
                    dataField: "fecha",
                    width: 130,
                    alignment: "center",
                },
                {
                    caption: "CLASIFICADOR",
                    dataField: "clasificador",
                    width: 200,
                    alignment: "center",
                },
                {
                    caption: "ESTADO",
                    width: 120,
                    alignment: "center",
                    cellTemplate: function (container, options) {
                        let item = options.data;
                        let dato = '<div class="text-center">';
                        if (item.Estado == "ATENDIDO") {
                            dato = dato + '<span class="bg-atendido">' + item.Estado+'</span>';
                        }
                        else {
                            dato = dato + '<span class="bg-pendiente">' + item.Estado+'</span>';
                        }
                        dato = dato + '</div> ';
                        dato = $(dato);
                        dato.appendTo(container);
                    }
                },
                {
                    caption: "DETALLE",
                    alignment: "left",
                    cellTemplate: function (container, options) {
                        let itemD = options.data;
                        let dato2 = '<div >';
                        if ((itemD.vUrl).length >0) {
                            dato2 = dato2 + '<span>' + itemD.mensaje + '</span><br>';
                            dato2 = dato2 + '<span><b>Documento:</b>'+ '<a target="_blank" href="' + itemD.vUrl + '" title="Ver documento.">' +
                                    '<img src="../img/pdf_48x48.png" width="24px" style="cursor: pointer; border: 0px" alt="Descargar archivos." />'+
                                '</a></span>';
                        }
                        else {
                            dato2 = dato2 + '<span>' + itemD.mensaje + '</span>';
                        }
                        dato2 = dato2 + '</div> ';
                        dato2 = $(dato2);
                        dato2.appendTo(container);
                    }
                },
            ],
            data: {
                'pSnip': '',
                'pUnificado': '',
                'pID':''
            },
        };
        var dom;
        dom = {};
        dom.btnBuscar = $('#btnBuscar');
        dom.btnClear = $('#btnClear');
        dom.txtCodigo = $("#txtCodigo");
        dom.rbSnip = $("#rbSnip");
        dom.rbUnificado = $("#rbUnificado");
        dom.ID = $("#ContentPlaceHolder1_hfIdSesion");

        dom.btnBuscar.on("click", function () {
            CargaBandeja();

        });

        dom.btnClear.on("click", function () {
            dom.txtCodigo.val("");

        });


        function CargaBandeja() {

            st.data.pID = dom.ID.val();
            if (dom.rbSnip.is(":checked")) {
                st.data.pSnip = dom.txtCodigo.val();
                st.data.pUnificado = "";
            }
            else {
                st.data.pSnip = "";
                st.data.pUnificado = dom.txtCodigo.val();
            }

            $.ajax({
                url: 'Bandeja.aspx/CargaBandeja',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(st.data),

                success: function (oResult) {
                    console.log(oResult);
                    oData = JSON.parse(oResult.d);

                    $("#gridContainer").dxDataGrid({
                        dataSource: oData,
                        columns: st.columns,
                        sorting: {
                            mode: 'none'
                        },
                        "allowColumnReordering": true,
                        "allowColumnResizing": true,
                        //"columnAutoWidth": true,
                        "height": "100%",
                        scrolling: {
                            mode: "standard"
                        }, paging: {
                            enabled: true,
                            pageSize: 10
                        },
                        export: {
                            enabled: false
                        },
                        wordWrapEnabled: true,
                        showColumnLines: true,
                        showBorders: true,
                        headerFilter: {
                            visible: false
                        },
                        filterRow: {
                            visible: false
                        },


                    });
                    //setTimeout(function () {
                    //    msg.remove();
                    //    newProjModal.modal("hide");
                    //    window.location.reload();
                    //}, 4000);

                },
                //error: function (xhr, ajaxOptions, thrownError) {
                //    console.log(xhr.status);
                //    console.log(thrownError);

                //    var msg = errorAlert.clone();
                //    newProjModal.find(".modal-body").append(msg);
                //    msg.show();
                //    msg.html("Ha ocurrido un error en el sistema, verifique la data enviada, de persistir el error, por favor sírvase de llamar a mesa de ayuda");
                //    newProjModal.find(":input").prop("disabled", false);
                //},
            });
        }

        CargaBandeja();


    </script>
</asp:Content>
