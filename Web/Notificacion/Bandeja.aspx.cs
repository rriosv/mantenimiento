﻿using Newtonsoft.Json;
using Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Notificacion
{
    public partial class Bandeja : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["LoginUsuario"] == null)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Logout", "<script>cerrar();</script>");
                    Response.Redirect("~/login?ms=1");
                }

                hfIdSesion.Value = Session["IdUsuario"].ToString();
            }

        }
        [WebMethod]
        public static string CargaBandeja(string pSnip, String pUnificado, int pID)
        {
            BL_Notificacion _obj = new BL_Notificacion();
            DataTable dt = new DataTable();
            string vResult = "";
            dt = _obj.spMON_Notificacion(pSnip, pUnificado, pID);
            vResult = JsonConvert.SerializeObject(dt);

            return vResult;
        }

 
    }
}