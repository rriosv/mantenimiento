﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace Web
{

    class NonOrderingBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }

    static class BundleExtentions
    {
        public static Bundle NonOrdering(this Bundle bundle)
        {
            bundle.Orderer = new NonOrderingBundleOrderer();
            return bundle;
        }
    }

    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Login

            bundles.Add(new StyleBundle("~/bundles/login2Css")
                        //.Include("/sources/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                        .Include("~/sources/materialize/css/materialize.min.css", new CssRewriteUrlTransform())
                        .Include("~/css/global.css")
                        .Include("~/css/style.css")
                        .Include("~/css/menuVertical.css")
                        .Include("~/sources/css/custom.css")
                        .Include("~/css/login.css")

                );

            bundles.Add(new ScriptBundle("~/bundles/login2Js").NonOrdering().Include(
                        "~/sources/jquery/jquery.min.js",
                        "~/sources/materialize/js/materialize.min.js",
                        "~/js/index.js",
                        "~/js/global.js"));

            #endregion

            //string Sources = "/Sources/";

            bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
                            "~/Scripts/WebForms/WebForms.js",
                            "~/Scripts/WebForms/WebUIValidation.js",
                            "~/Scripts/WebForms/MenuStandards.js",
                            "~/Scripts/WebForms/Focus.js",
                            "~/Scripts/WebForms/GridView.js",
                            "~/Scripts/WebForms/DetailsView.js",
                            "~/Scripts/WebForms/TreeView.js",
                            "~/Scripts/WebForms/WebParts.js"));

            // Order is very important for these files to work, they have explicit dependencies
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));
      

            ////LOGIN CSS
            bundles.Add(new StyleBundle("~/bundles/loginCss")
                        //.Include("/sources/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                        .Include("~/sources/bootstrap/css/bootstrap.min.css", new CssRewriteUrlTransform())
                        .Include("~/css/global.css")
                        .Include("~/css/style.css")
                        .Include("~/css/menuVertical.css")
                        .Include("~/sources/css/custom.css")
                        .Include("~/css/login.css")

                );


            //LOGIN JS
            bundles.Add(new ScriptBundle("~/bundles/loginJs").NonOrdering().Include(
                        "~/sources/jquery/jquery.min.js",
                        "~/sources/bootstrap/js/bootstrap.min.js",
                        "~/js/index.js",
                        "~/js/global.js"));

            //Master General JS 2
            bundles.Add(new ScriptBundle("~/bundles/masterGeneral2Js").NonOrdering().Include(
                       "~/sources/jquery/jquery-3.3.1.min.js",
                       "~/sources/adminLTE/js/adminlte.min.js",
                       "~/js/global.js",
                       "~/sources/sweetAlert/sweetalert.min.js",
                       "~/sources/select2/js/select2.full.min.js",
                       "~/sources/custom.js",
                       "~/sources/bootstrap/js/bootstrap.min.js",
                       "~/sources/tagEditor/jquery.caret.min.js",
                       "~/sources/tagEditor/jquery.tag-editor.js"));

       
            //Master General CSS 2
            bundles.Add(new StyleBundle("~/bundles/masterGeneral2Css")
                        .Include("~/sources/adminLTE/css/AdminLTE.min.css")
                        .Include("~/sources/adminLTE/css/skins/_all-skins.min.css")
                        .Include("~/css/global.css")
                        .Include("~/sources/sweetAlert/sweetalert.css")
                        .Include("~/sources/moon/style.css")
                        .Include("~/sources/css/custom.css")
                        .Include("~/css/SistemaMain.css")
                        .Include("~/sources/tagEditor/jquery.tag-editor.css")
                );

           

            //Master Ficha Evaluacion JS
            bundles.Add(new ScriptBundle("~/bundles/masterFichaJs").NonOrdering().Include(
                        "~/sources/sweetAlert/sweetalert.min.js",
                        "~/js/global.js",
                        "~/sources/jquery/jquery.min.js",
                        "~/Scripts/jquery.jaTextCounter-1.0.js"));

            // Use the Development version of Modernizr to develop with and learn from. Then, when you’re
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-*"));

            //ScriptManager.ScriptResourceMapping.AddDefinition(
            //    "respond",
            //    new ScriptResourceDefinition
            //    {
            //        Path = "~/Scripts/respond.min.js",
            //        DebugPath = "~/Scripts/respond.js",
            //    });

            //BundleTable.EnableOptimizations = true;
        }
    }
}