﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pagina403.aspx.cs" Inherits="Web.Pagina403" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>403</title>
    <link href="sources/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        .container {
    text-align: center;
    margin: 1rem 0.5rem 0;
}
        .tablero {
                border: 2px solid #1560ae;
    padding: 5%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="background-color:#353d37;">

        <div style="padding: 5% 20% 20% 20%">
            <center>

                <div class="tablero" >
                    <img src="img/logo_master_page.png" height="80px" />
                    <br />
                    <br />
                    <label>No tiene permiso para acceder a esta página.</label>

                    <br />
                    <br />
                    <div id="btnInicio" class="btn btn-success"><b><i class="glyphicon glyphicon-search"></i>INICIO</b></div>
                </div>
            </center>
        </div>

    </form>

    <script src="sources/jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">

        $('#btnInicio').on("click", function () {
            window.location.href = "login?ms=1";

        });

    </script>
</body>
</html>
