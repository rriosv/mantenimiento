﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaError.aspx.cs" Inherits="Web.PaginaError" %>

<!DOCTYPE>
<html>
<head runat="server">
    
    <title>Error</title>

    <link href="sources/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />
      <style type="text/css">
        .container {
    text-align: center;
    margin: 1rem 0.5rem 0;
}
        .tablero {
                border: 2px solid #fff;
    padding: 5%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="background-color:#353d37;">


         <div style="padding: 5% 20% 0% 20%; height:98%" >
            <center>

                <div class="tablero" >
                    <img src="img/logo_master_page.png" height="80px" />
                    <br />
                    <br />
                    
                     <span style="color: #fff">Ha ocurrido una acción inesperada, vuelva a intetarlo en unos minutos...
                                        </span>

                    <br />
                    <br />
                    <div id="btnInicio" class="btn btn-success"><b><i class="glyphicon glyphicon-search"></i>INICIO</b></div>
                </div>
            </center>
        </div>


    </form>

       <script src="sources/jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">

        $('#btnInicio').on("click", function () {
            window.location.href = "login?ms=1";

        });

    </script>
</body>
</html>
