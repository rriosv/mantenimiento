﻿<%@ Page Title="Mantenimiento" Language="C#" MasterPageFile="~/MasterPages/Registro.Master" AutoEventWireup="true" CodeBehind="Mantenimiento.aspx.cs" Inherits="Web.Mantenimiento.Mantenimiento" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <b>
        INFORMACIÓN GENERAL
    </b>

    <table>
        <tr>
            <td style="text-align:right">
                Nombre :
            </td>
            <td style="text-align:left">
                <asp:label ID="lblNombre" runat="server"></asp:label>
            </td>
        </tr>
        <tr>
            <td style="text-align:right">
                Perfil :
            </td>
            <td style="text-align:left">
                <asp:label ID="lblPerfil" runat="server"></asp:label>
            </td>
        </tr>

        <tr>
            <td style="text-align:right">
                Correo :
            </td>
            <td style="text-align:left">
                <asp:label ID="lblCorreo" runat="server"></asp:label>
            </td>
        </tr>

        <tr>
            <td style="text-align:right">
                Estado :
            </td>
            <td style="text-align:left">
                <asp:label ID="lblEstado" runat="server"></asp:label>
            </td>
        </tr>
    </table>

    <table width="95%" style="margin:auto;">
        <tr>
            <td style="text-align:center;">
                <b>ACCESOS</b>
                </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                       <tr id="trRegistro" runat="server" visible="false">
                        <td colspan="2" style="text-align:center;" >
                            <div  style="padding: 5px 10px 5px 10px; margin-top: 10px ; margin:auto; width:70%;" class="CuadrosEmergentes">
                            <table  style="margin:auto;" >
                                <tr>
                                    <td colspan="2">
                                        <b>REGISTRAR ACCESO</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">Item:</td>
                                    <td style="text-align:left;">
                                        <asp:DropDownList ID="ddlItem" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlItem_SelectedIndexChanged"></asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td style="text-align:right;">Menu:</td>
                                    <td style="text-align:left;">
                                        <asp:DropDownList ID="ddlMenu" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="height:10px"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:center;">
                                        <asp:Button ID="btnAgregarAcceso" runat="server" Text="Agregar" OnClick="btnAgregarAcceso_Click" />
                                        <asp:Button ID="btnCancelarAcceso" runat="server" Text="Cancelar" OnClick="btnCancelarAcceso_Click" />
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="xgrdAccesoActual" runat="server" AutoGenerateColumns="False" Theme="PlasticBlue"  SettingsPager-PageSize="15" KeyFieldName="id_Acceso" Width="100%" >
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ITEM" FieldName="titulo" GroupIndex="1" HeaderStyle-Font-Bold="true" VisibleIndex="1" >
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn Caption="MENU" FieldName="subtitulo" HeaderStyle-Font-Bold="true" VisibleIndex="1" >
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn Caption="PÁGINA" FieldName="pagina" HeaderStyle-Font-Bold="true" VisibleIndex="1" >
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataColumn Caption="" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" Visible="true" VisibleIndex="1" Width="20px">
                                        <DataItemTemplate>
                                            <asp:ImageButton ID="imgbtnElim" runat="server" BorderWidth="0"  Height="18px" ImageUrl="~/img/del.gif" OnClick="imgbtnElim_Click" ToolTip="Eliminar Acceso." />
                                        </DataItemTemplate>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>

                                </Columns>
                            </dx:ASPxGridView>

                        </td>
                        <td style="vertical-align:top">
                                     <asp:ImageButton ID="imgbtnNuevoAcceso" runat="server" ImageUrl="~/img/add.png"
                                                                                        onmouseover="this.src='../img/add2.png';" onmouseout="this.src='../img/add.png';"
                                                                                        Height="30px" AlternateText="Agregar" ToolTip="Nuevo Acceso."
                                                                                        OnClick="imgbtnNuevoAcceso_Click"  />

                             
                        </td>
                    </tr>
                 
                </table>
               

            
                  

                                                                                              
            </td>
        </tr>
    </table>
</asp:Content>


