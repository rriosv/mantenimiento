﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="cambioclave.aspx.cs" Inherits="Web.Mantenimiento.cambioclave" Culture="es-PE" UICulture="es-PE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />

    <div style="text-align:center">
        <span class="titlePage">CAMBIO DE CONTRASEÑA</span>
        <br />
        <br />
    </div>
    <table style="border-collapse:separate !important; border-spacing: 4px; margin:auto; text-align:center" width="400" border="0">
        <tr>
            <td align="right" >Contraseña anterior :</td>
            <td align="left">
                
                <asp:TextBox id="apassw" runat="server" textmode="Password" maxlength="20" width="180px" placeholder="Contraseña Actual"></asp:TextBox>
                <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" controltovalidate="passw"
                    enableviewstate="False" errormessage="Ingrese la contraseña">*</asp:requiredfieldvalidator>
            </td>
        </tr>
        <tr>
            <td align="right" >Nueva contraseña :</td>
            <td align="left">
                <asp:TextBox id="passw" runat="server" textmode="Password"  maxlength="20" width="180px" placeholder="Nueva contraseña"></asp:TextBox>
                <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" controltovalidate="passw"
                    enableviewstate="False" errormessage="Ingrese la contraseña">*</asp:requiredfieldvalidator>
            </td>
        </tr>
        <tr>
            <td align="right" >Confirmar nueva contraseña :</td>
           <td align="left">
                <asp:TextBox id="cpassw" runat="server" textmode="Password"  maxlength="20" width="180px" placeholder="Repetir nueva contraseña"></asp:TextBox>
                <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" controltovalidate="cpassw"
                    enableviewstate="False" errormessage="No ha repetido la contraseña">*</asp:RequiredFieldValidator>
                <asp:CompareValidator id="CompareValidator1" runat="server" controltovalidate="cpassw" forecolor="red"
                    enableviewstate="False" controltocompare="passw" errormessage="Confirmacion de contraseña errada.">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
               <br />
                            <asp:Button id="btnAceptar" runat="server" cssclass="btn btn-primary" text="Guardar" onclick="btnAceptar_Click" />
                        
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label id="lblmsg" runat="server"  />
                <asp:ValidationSummary id="ValidationSummary1" runat="server" width="100%" enableviewstate="False" showmessagebox="true" />
            </td>
        </tr>
    </table>
</asp:Content>
