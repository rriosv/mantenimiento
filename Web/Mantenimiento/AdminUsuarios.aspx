﻿<%@ Page Title="Administración de Usuarios" Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="AdminUsuarios.aspx.cs" Inherits="Web.Mantenimiento.AdminUsuarios" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        table tr.dxgvDataRow_MetropolisBlue:hover {
            background-color: #FFFF99;
            color: black;
        }

        .dxgvControl_MetropolisBlue, .dxgvDisabled_MetropolisBlue {
            font: 12px "Segoe UI",Helvetica,"Droid Sans",Tahoma,Geneva,sans-serif;
            color: #808080;
            cursor: default;
        }

        .dxgvDataRow_MetropolisBlue {
            color: #333;
        }

        .titulo {
            font-size: 14pt;
            font-family: 'DINB',Calibri;
            color: #0094D4;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript">
        function ShowLoginWindow() {
            xpcNuevoRegistro.Show();
        }
        function ShowCreateAccountWindow() {
            pcCreateAccount.Show();
            tbUsername.Focus();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="width: 95%; margin: auto; text-align:center">
        <br />
        <p>
            <asp:Label ID="Label3" runat="server" Text="CONTROL DE USUARIOS" CssClass="titulo" Style="font-size: 15pt;">
            </asp:Label>
        </p>
        <center>
            <br />
            <table>
                <tr>
                    <td style="text-align: left; width: 80%">
                        <asp:TextBox ID="txtNombreFiltro" runat="server" Width="50%"></asp:TextBox>
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                    </td>
                    <td style="text-align: right;">
                        <asp:Button ID="btnNuevo" runat="server" Text="Nuevo Usuario" OnClick="btnNuevo_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <dx:ASPxGridView ID="xgrdUsuarios" runat="server" AutoGenerateColumns="False" EnableTheming="True" Theme="MetropolisBlue" SettingsPager-PageSize="20" KeyFieldName="id_usuario">
                                    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                                    <Columns>

                                        <dx:GridViewDataColumn Caption="..." HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                                            <DataItemTemplate>
                                                <asp:ImageButton ID="imgbtnEditarUsuario" runat="server" ImageUrl="~/img/edit_20x20.png" OnClick="imgbtnEditarUsuario_Click" />
                                            </DataItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                        </dx:GridViewDataColumn>

                                        <dx:GridViewDataTextColumn Caption="ID" FieldName="id_usuario" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="15px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataColumn Caption="NOMBRE" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" VisibleIndex="1">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lnkbtnUsuario" runat="server" Font-Size="13px" OnClick="lnkbtnUsuario_Click" Text='<%#Eval("nombre") %>'></asp:LinkButton>

                                            </DataItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Wrap="True" />
                                        </dx:GridViewDataColumn>

                                        <dx:GridViewDataTextColumn Caption="USUARIO" FieldName="login" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Contains">
                                            <Settings AutoFilterCondition="Contains" HeaderFilterMode="CheckedList"></Settings>

                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="DNI" FieldName="vDNI" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="15px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="OFICINA" FieldName="oficina" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="15px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="PERFIL" FieldName="id_perfil_usuario" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="15px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="CLAVE ENCRIPTADA" FieldName="passw" HeaderStyle-Font-Bold="true" VisibleIndex="1" Visible="false">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="CORREO" FieldName="correo" HeaderStyle-Font-Bold="true" VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                            <Settings AutoFilterCondition="Contains"></Settings>

                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="S" FieldName="nivelEstudio" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="10px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="M" FieldName="nivelMonitoreo" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="10px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="E" FieldName="nivelEmblematico" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="10px" Settings-HeaderFilterMode="CheckedList" Settings-AutoFilterCondition="Default">
                                            <Settings HeaderFilterMode="CheckedList"></Settings>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataCheckColumn Caption=" " FieldName="activo" HeaderStyle-Font-Bold="true" VisibleIndex="1" Width="10px">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" Wrap="True" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataCheckColumn>

                                    </Columns>

                                    <SettingsPager PageSize="20"></SettingsPager>

                                    <Settings ShowFooter="True" ShowHeaderFilterBlankItems="true" ShowHeaderFilterButton="true" />
                                    <SettingsDataSecurity AllowInsert="False" />
                                    <Styles Header-BackColor="#2D7BBD" Header-ForeColor="White">
                                        <Header BackColor="#2D7BBD" ForeColor="White"></Header>
                                    </Styles>



                                </dx:ASPxGridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="xpcNuevoRegistro" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
                PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="xpcNuevoRegistro"
                HeaderText="REGISTRO DE USUARIO" AllowDragging="true" HeaderStyle-Font-Bold="true" PopupAnimationType="None"
                EnableViewState="False" Theme="PlasticBlue" Width="400px">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <dx:ASPxPanel ID="Panel1" runat="server">
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server">
                                    <table cellpadding="2" cellspacing="2" style="font-family: Calibri; margin: auto; font-size: 12px;">
                                        <tr runat="server" id="trIdUsuairo" style="visibility: collapse">
                                            <td rowspan="15" style="width: 35px;">
                                                <div class="pcmSideSpacer">
                                                </div>
                                            </td>
                                            <td style="text-align: right;">ID Usuario:
                                            </td>
                                            <td style="text-align: left;">
                                                <dx:ASPxTextBox ID="txtIdUsuario" runat="server" Width="170px" Border-BorderStyle="None" ClientEnabled="false">
                                                </dx:ASPxTextBox>


                                            </td>

                                            <td rowspan="10" style="width: 35px;">
                                                <div class="pcmSideSpacer">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Nombre:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="txtNombre" runat="server" Width="250px"></asp:TextBox>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Correo:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="txtCorreo" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;">DNI:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="txtDNI" runat="server" Width="100px" MaxLength="8"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;">Oficina:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:DropDownList ID="ddlOficina" runat="server">
                                                    <asp:ListItem Text="SELECCIONAR" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="CSMVCS" Value="30"></asp:ListItem>
                                                    <asp:ListItem Text="DM" Value="13"></asp:ListItem>
                                                    <asp:ListItem Text="DGAA" Value="27"></asp:ListItem>
                                                    <asp:ListItem Text="DGADT" Value="26"></asp:ListItem>
                                                    <asp:ListItem Text="DGPPCS" Value="20"></asp:ListItem>
                                                    <asp:ListItem Text="DGPPVU" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="DGPRCS" Value="15"></asp:ListItem>
                                                    <asp:ListItem Text="DGPRVU" Value="19"></asp:ListItem>
                                                    <asp:ListItem Text="GA" Value="22"></asp:ListItem>
                                                    <asp:ListItem Text="OAC" Value="14"></asp:ListItem>
                                                    <asp:ListItem Text="OCI" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="ODGS" Value="16"></asp:ListItem>
                                                    <asp:ListItem Text="OGA" Value="12"></asp:ListItem>
                                                    <asp:ListItem Text="OGC" Value="21"></asp:ListItem>
                                                    <asp:ListItem Text="OGEI" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="OGMEI" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="OGPP" Value="24"></asp:ListItem>
                                                    <asp:ListItem Text="OILCC" Value="29"></asp:ListItem>
                                                    <asp:ListItem Text="OTASS" Value="25"></asp:ListItem>
                                                    <asp:ListItem Text="PASLC" Value="31"></asp:ListItem>
                                                    <asp:ListItem Text="PGSU" Value="17"></asp:ListItem>
                                                    <asp:ListItem Text="PMIB" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="PNC" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="PNSU" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="PNSR" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="PNVR" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="PNT" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="SG" Value="23"></asp:ListItem>
                                                    <asp:ListItem Text="VMCS" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="VMVU" Value="18"></asp:ListItem>
                                                    <asp:ListItem Text="EXTERNO" Value="28"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Perfil:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:DropDownList ID="ddlPerfil" runat="server">
                                                    <asp:ListItem Text="SELECCIONAR" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="TECHO PROPIO" Value="999"></asp:ListItem>
                                                    <%--<asp:ListItem Text="EMBLEMATICOS" Value="22"></asp:ListItem>--%>
                                                    <asp:ListItem Text="SUPERVISOR" Value="20"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR PASLC" Value="76"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR PMIB" Value="23"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR PNSU" Value="24"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR PNSR" Value="71"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR PNVR" Value="73"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR PNC" Value="74"></asp:ListItem>
                                                    <asp:ListItem Text="REVISOR PASLC" Value="77"></asp:ListItem>
                                                    <asp:ListItem Text="REVISOR PMIB" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="REVISOR PNSU" Value="64"></asp:ListItem>
                                                    <asp:ListItem Text="REVISOR PNSR" Value="70"></asp:ListItem>
                                                    <asp:ListItem Text="REVISOR PNVR" Value="72"></asp:ListItem>
                                                    <asp:ListItem Text="REVISOR PNC" Value="75"></asp:ListItem>
                                                    <asp:ListItem Text="VISUALIZADOR" Value="21"></asp:ListItem>

                                                </asp:DropDownList>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;">Usuario:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="txtUsuario" runat="server" Width="150px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Clave:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="txtClave" runat="server" Width="150px"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr runat="server" visible="false" id="trNuevaClave">
                                            <td style="text-align: right;">Nueva Clave:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="txtNuevaClave" runat="server" Width="150px"></asp:TextBox><asp:CheckBox ID="chbFlagNuevaClave" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Calidad:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:DropDownList ID="ddlNivelEstudios" runat="server">
                                                    <asp:ListItem Text="NINGUNO" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="TECNICO" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR" Value="C"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;">Monitoreo:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:DropDownList ID="ddlNivelMonitoreo" runat="server">
                                                    <asp:ListItem Text="NINGUNO" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="TECNICO" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR" Value="C"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Emblematicos:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:DropDownList ID="ddlNivelEmblematicos" runat="server">
                                                    <asp:ListItem Text="NINGUNO" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="TECNICO" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="COORDINADOR" Value="C"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">Activo:
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:CheckBox ID="chbActivo" runat="server" />

                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="height: 8px;"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" style="text-align: center;">

                                                <table style="margin: auto;">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnRegistrarNuevo" runat="server" Text="Registrar" CssClass="btn btn-primary" OnClick="btnRegistrarNuevo_Click" />
                                                            <asp:Button ID="btnModificarUsuario" runat="server" Text="Modificar" CssClass="btn btn-primary" OnClick="btnModificarUsuario_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnCancelarNuevo" runat="server" Text="Cancelar" CssClass="btn btn-default" OnClientClick="javascript:xpcNuevoRegistro.Hide();" /></td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>


                                </dx:PanelContent>

                            </PanelCollection>
                        </dx:ASPxPanel>


                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

