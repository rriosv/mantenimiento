﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnviarClave.aspx.cs" Inherits="Web.Mantenimiento.EnviarClave" Culture="es-PE" UICulture="es-PE" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   
    <title>Recuperar Clave</title>
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="../sources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../sources/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../sources/css/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
  <div class="form-group col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <asp:TextBox ID="txtUsuario" runat="server" MaxLength="20" CssClass="form-control" placeHolder="Usuario" />
        </div>
  </div> 
  <div class="form-group col-xs-12">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">@</span>
            <asp:TextBox ID="correo" runat="server" MaxLength="50" CssClass="form-control"   placeHolder="Correo Institucional." />
        </div>
  </div>
  <div class="form-group text-right">

        <div class="col-xs-6">
                        <asp:TextBox ID="codigo" runat="server" ReadOnly="true" BackColor="info"  CssClass="form-control"  Font-Bold="true"  />
        </div>
        <div class="col-xs-6">
                        <asp:TextBox ID="txtCodigoSeguridad" runat="server" MaxLength="5" CssClass="form-control" placeHolder="Escriba el código"/>
        </div>
      <div class="clearfix"></div>
  </div>
<div class="hidden">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsuario"
                            EnableViewState="False" ErrorMessage="Ingrese Usuario">*</asp:RequiredFieldValidator>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="correo"
                            EnableViewState="False" ErrorMessage="Ingrese correo">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="correo"
                            ErrorMessage="Formato de correo no válido" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">**</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCodigoSeguridad"
                            EnableViewState="False" ErrorMessage="No ha ingresado código de seguridad">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCodigoSeguridad" ForeColor="red"
                            EnableViewState="False" ControlToCompare="codigo" ErrorMessage="Código de seguridad incorrecto.">*</asp:CompareValidator>
        </div>
        <div>
            <table>
                <tr>
                    <td colspan="2" style="text-align: left;">
                        <asp:Label ID="lblmsg" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="100%" EnableViewState="False" ShowMessageBox="true" />
                    </td>
                </tr>
            </table>
        </div>
  <div class="form-group col-xs-12">
                <asp:Button ID="btnAceptar" runat="server" CssClass="btn btn-azul" Text="ACEPTAR" OnClick="btnAceptar_Click" />
  </div>

    </form>
</body>
</html>
