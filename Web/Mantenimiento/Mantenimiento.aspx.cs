﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entity;
using Business;
using pry_san;
using DevExpress.Web;


namespace Web.Mantenimiento
{
    public partial class Mantenimiento : System.Web.UI.Page
    {
        BLMenu _objBLMenu = new BLMenu();
        BEMenu _BEMenu = new BEMenu();
        mMenu _objMenu = new mMenu();
        mAcceso _objAcceso = new mAcceso();
        BL_Administrador _objBLAdm = new BL_Administrador();
        BE_Administrador _BEAdm = new BE_Administrador();

        protected void Page_Load(object sender, EventArgs e)
        {
            int idUsuario = 0;
            try
            {
                idUsuario = Convert.ToInt32(Request.QueryString["id"].ToString());
            }
            catch
            {
                Response.Redirect("~/login.aspx?ms=1");
            }

            cargaAccesosActuales(idUsuario);

            if (!IsPostBack)
            {
                xgrdAccesoActual.ExpandAll();

                CargaInformacion();
                CargaItem(idUsuario);
                CargaMenu();
            }
        }

        protected void CargaInformacion()
        {
            _BEAdm = new BE_Administrador();
            _BEAdm = _objBLAdm.F_sp_GetByIdUsuario(Convert.ToInt32(Request.QueryString["id"].ToString()));
            lblNombre.Text = _BEAdm.nombre;
            lblCorreo.Text = _BEAdm.correo;

            lblPerfil.Text = _BEAdm.id_perfil_usuario.ToString();

            switch (_BEAdm.id_perfil_usuario)
            {
                case 64:
                    lblPerfil.Text = lblPerfil.Text + " - Revisor PNSU";
                    break;
                case 24:
                    lblPerfil.Text = lblPerfil.Text + " - Coordinador PNSU";
                    break;
                case 23:
                    lblPerfil.Text = lblPerfil.Text + " - Coordinador PMIB";
                    break;
                case 11:
                    lblPerfil.Text = lblPerfil.Text + " - Revisor PMIB";
                    break;
                case 22:
                    lblPerfil.Text = lblPerfil.Text + " - Emblematico";
                    break;

            }


            if (_BEAdm.activo == true)
            {
                lblEstado.Text = "ACTIVO";
            }
            else
            {
                lblEstado.Text = "DESACTIVADO";
            }

        }

        protected void imgbtnNuevoAcceso_Click(object sender, ImageClickEventArgs e)
        {
            trRegistro.Visible = true;
        }

        protected void cargaAccesosActuales(int idUsuario)
        {
            xgrdAccesoActual.DataSource = _objBLMenu.F_sp_ListarOpcionesMenu(idUsuario);
            xgrdAccesoActual.DataBind();
        }
        protected void imgbtnElim_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton boton;
            GridViewDataItemTemplateContainer row;
            boton = (ImageButton)sender;
            row = (GridViewDataItemTemplateContainer)boton.NamingContainer;
            int id = Convert.ToInt32(row.KeyValue.ToString());
            _objAcceso.sp_EliminaAcceso(id, "");
            trRegistro.Visible = false;
            int idUsuario;
            idUsuario = Convert.ToInt32(Request.QueryString["id"].ToString());
            cargaAccesosActuales(idUsuario);
        }

        protected void CargaItem(int idUsuario)
        {
            ddlItem.DataSource = _objMenu.sp_CargarGrupoMenu(1); // POR DEFECTO PARA VER TODAS LAS OPCIONES.
            ddlItem.DataValueField = "id_menu_grupo";
            ddlItem.DataTextField = "titulo";
            ddlItem.DataBind();
        }

        protected void CargaMenu()
        {
            ddlMenu.DataSource = _objAcceso.sp_CargarMenu(Convert.ToInt32(ddlItem.SelectedValue));
            ddlMenu.DataValueField = "id_menu";
            ddlMenu.DataTextField = "subtitulo";
            ddlMenu.DataBind();
        }
        protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMenu();
        }
        protected void btnCancelarAcceso_Click(object sender, EventArgs e)
        {
            trRegistro.Visible = false;
        }
        protected void btnAgregarAcceso_Click(object sender, EventArgs e)
        {
            int idUsuario;
            idUsuario = Convert.ToInt32(Request.QueryString["id"].ToString());
            _objAcceso.sp_agregarAcceso(0, Convert.ToInt32(ddlMenu.SelectedValue), idUsuario, "");
            trRegistro.Visible = false;

            cargaAccesosActuales(idUsuario);

        }
    }
}