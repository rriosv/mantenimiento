﻿using pry_san;
using seguridad;
using System;
using System.Data;
using System.Drawing;
using System.Web.UI;


namespace Web.Mantenimiento
{
    public partial class cambioclave : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Title = "Cambio de contraseña";
                apassw.Focus();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            acceso objAcceso = new acceso();
            mProyectos objMetodoProyectos = new mProyectos();
            int id_usuario = int.Parse(Session["IdUsuario"].ToString());
            string usu = Session["LoginUsuario"].ToString();
            string passw_a = objAcceso.Encriptar(apassw.Text);
            DataTable dt = objAcceso.sp_validarUsuario(usu, passw_a);

            if (dt.Rows.Count > 0)
            {
                string passw_n = objAcceso.Encriptar(passw.Text);
                objMetodoProyectos.ExecuteFromSQL("update T_USUARIO set passw='" + passw_n + "' where id_usuario=" + id_usuario);
                objMetodoProyectos.ExecuteFromSQL("exec sp_audit 5,6," + id_usuario + ",'" + usu + "'");
                lblmsg.ForeColor = Color.Blue;
                lblmsg.Text = "Ha cambiado la contraseña satisfactoriamente.";
                dt.Clear();
                dt.Dispose();
            }
            else
            {
                lblmsg.ForeColor = Color.Red;
                lblmsg.Text = "No ha ingresado contraseña actual correcta.";
            }
        }

    }
}