﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entity;
using Business;
using DevExpress.Web;
using seguridad;


namespace Web.Mantenimiento
{
    public partial class AdminUsuarios : System.Web.UI.Page
    {
        BL_Administrador _objBLAdmin = new BL_Administrador();
        BE_Administrador _BEAdmin = new BE_Administrador();

        protected void Page_Load(object sender, EventArgs e)
        {
            cargaUsuarios(0);

            //CargaOficina();
        }

        protected void cargaUsuarios(int recarga)
        {
            List<BE_Administrador> ListUsuarios = new List<BE_Administrador>();


            if (recarga == 1)
            {
                ListUsuarios = _objBLAdmin.F_spAdm_ListarUsuarios();
                Session["ListUsuario"] = ListUsuarios;
            }
            else
            {
                if (Session["ListUsuario"] == null)
                {
                    ListUsuarios = _objBLAdmin.F_spAdm_ListarUsuarios();
                    Session["ListUsuario"] = ListUsuarios;
                }

                ListUsuarios = (List<BE_Administrador>)Session["ListUsuario"];
            }


            if (txtNombreFiltro.Text != "")
            {
                ListUsuarios = (from cust in ListUsuarios
                                where
                                cust.nombre.ToString().ToUpper().Contains(txtNombreFiltro.Text.ToUpper())
                                select cust).ToList();
                //Order By cust.dtFecha Descending).ToList();
            }


            xgrdUsuarios.DataSource = ListUsuarios;
            xgrdUsuarios.DataBind();

        }

        protected void CargaOficina()
        {
            ddlOficina.DataSource = _objBLAdmin.F_spAdm_ListaOficina();
            ddlOficina.DataTextField = "nombre";
            ddlOficina.DataValueField = "valor";
            ddlOficina.DataBind();

            ddlOficina.Items.Insert(0, new ListItem("SELECCIONAR", ""));
        }

        protected void lnkbtnUsuario_Click(object sender, EventArgs e)
        {
            LinkButton boton;
            GridViewDataItemTemplateContainer row;
            boton = (LinkButton)sender;
            row = (GridViewDataItemTemplateContainer)boton.NamingContainer;

            string id = row.KeyValue.ToString();
            //string script = "<script>window.open('Actividad2.aspx?id=" + id + "','_blank') </script>";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            string script = "<script>window.open('Mantenimiento.aspx?id=" + id + "','doc','width=900,height=600,status=1,scrollbars=1','_blank') </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
            acceso objAcceso = new acceso();

            _BEAdmin.id_usuario = 0;
            _BEAdmin.nombre = txtNombre.Text;
            _BEAdmin.correo = txtCorreo.Text;
            _BEAdmin.id_perfil_usuario = Convert.ToInt32(ddlPerfil.SelectedValue);
            _BEAdmin.login = txtUsuario.Text;
            _BEAdmin.vDNI = txtDNI.Text;
            _BEAdmin.iCodOFicina = ddlOficina.SelectedValue;
            _BEAdmin.passw = objAcceso.Encriptar(txtClave.Text);
            _BEAdmin.nivelEstudio = ddlNivelEstudios.SelectedValue;
            _BEAdmin.nivelMonitoreo = ddlNivelMonitoreo.SelectedValue;
            _BEAdmin.nivelEmblematico = ddlNivelEmblematicos.SelectedValue;

            _BEAdmin.activo = true;

            int val = _objBLAdmin.spiuAdm_Usuario(_BEAdmin);

            if (val == 1)
            {
                string script = "<script>alert('Se registró correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                xpcNuevoRegistro.ShowOnPageLoad = false;
                cargaUsuarios(1);
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            cargaUsuarios(0);

        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            trNuevaClave.Visible = false;
            btnRegistrarNuevo.Visible = true;
            btnModificarUsuario.Visible = false;

            xpcNuevoRegistro.ShowOnPageLoad = true;


            txtNombre.Text = "";
            txtCorreo.Text = "";
            txtClave.Text = "";
            txtUsuario.Text = "";
            txtDNI.Text = "";
            //ddlOficina.SelectedValue = "";
            chbActivo.Checked = true;


        }

        protected void imgbtnEditarUsuario_Click(object sender, ImageClickEventArgs e)
        {

            //LinkButton boton;
            //GridViewDataItemTemplateContainer row;
            //boton = (LinkButton)sender;
            //row = (GridViewDataItemTemplateContainer)boton.NamingContainer;

            //string id = row.KeyValue.ToString();

            int id = 0;
            ImageButton grd = (ImageButton)sender;
            id = Convert.ToInt32((grd.Parent as DevExpress.Web.GridViewDataItemTemplateContainer).ItemIndex.ToString());
            //List<T_CONVENIO> listConvenio = new List<T_CONVENIO>();
            //listConvenio = obj.spListConvenio();

            BE_Administrador _BEAdmin = xgrdUsuarios.GetRow(id) as BE_Administrador;
            //T_CONVENIO convenio = listConvenio.Where(c => c.Id_Convenio == id).First();

            txtIdUsuario.Text = _BEAdmin.id_usuario.ToString();
            //txtIdUsuario.Text = _BEAdmin.id_usuario.ToString();
            txtNombre.Text = _BEAdmin.nombre;
            txtCorreo.Text = _BEAdmin.correo;
            txtUsuario.Text = _BEAdmin.login;
            txtClave.Text = _BEAdmin.passw;
            txtDNI.Text = _BEAdmin.vDNI;
            ddlNivelEstudios.SelectedValue = _BEAdmin.nivelEstudio;
            ddlNivelMonitoreo.SelectedValue = _BEAdmin.nivelMonitoreo;
            ddlNivelEmblematicos.SelectedValue = _BEAdmin.nivelEmblematico;
            if (_BEAdmin.iCodOFicina.Length > 0)
            {
                ddlOficina.SelectedValue = _BEAdmin.iCodOFicina;
            }
            else
            {
                ddlOficina.SelectedValue = "";
            }

            int iPerfil = _BEAdmin.id_perfil_usuario;

            if (iPerfil > 0)
            {
                ddlPerfil.SelectedValue = iPerfil.ToString();
            }
            else
            {
                ddlPerfil.SelectedValue = "0";
            }
            txtNuevaClave.Text = "";
            chbActivo.Checked = _BEAdmin.activo;

            trNuevaClave.Visible = true;
            btnRegistrarNuevo.Visible = false;
            xpcNuevoRegistro.ShowOnPageLoad = true;

        }

        protected void btnModificarUsuario_Click(object sender, EventArgs e)
        {
            acceso objAcceso = new acceso();

            _BEAdmin.id_usuario = Convert.ToInt32(txtIdUsuario.Text);
            //_BEAdmin.id_usuario = Convert.ToInt32(txtIdUsuario.Text);
            _BEAdmin.nombre = txtNombre.Text;
            _BEAdmin.correo = txtCorreo.Text;
            _BEAdmin.id_perfil_usuario = Convert.ToInt32(ddlPerfil.SelectedValue);
            _BEAdmin.login = txtUsuario.Text;
            _BEAdmin.vDNI = txtDNI.Text;
            _BEAdmin.iCodOFicina = ddlOficina.SelectedValue;
            _BEAdmin.nivelEstudio = ddlNivelEstudios.SelectedValue;
            _BEAdmin.nivelMonitoreo = ddlNivelMonitoreo.SelectedValue;
            _BEAdmin.nivelEmblematico = ddlNivelEmblematicos.SelectedValue;

            if (chbFlagNuevaClave.Checked == true)
            {
                _BEAdmin.passw = objAcceso.Encriptar(txtNuevaClave.Text);

            }
            else
            {
                _BEAdmin.passw = txtClave.Text;
            }

            _BEAdmin.activo = chbActivo.Checked;

            int val = _objBLAdmin.spiuAdm_Usuario(_BEAdmin);

            if (val == 1)
            {
                string script = "<script>alert('Se modificó correctamente.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

                xpcNuevoRegistro.ShowOnPageLoad = false;
                cargaUsuarios(1);
            }
            else
            {
                string script = "<script>alert('Vuelva a Intentarlo después.');</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", script, false);

            }

        }

    }
}