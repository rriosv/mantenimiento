﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Web.componente
{
    public static class UtilsJSON
    {
        public static string parseToJsonCabDet(DataTable poDt)
        {
            StringBuilder vResult = new StringBuilder("\"data\":[");
            int xidObjeto = -1;
            int vCantObjetos = 0;
            foreach (DataRow oDr in poDt.Rows)
            {
                if (xidObjeto != Int32.Parse(oDr[0].ToString()))
                {
                    xidObjeto = Int32.Parse(oDr[0].ToString());
                    vResult.Append("{");
                    vResult.Append("\"" + poDt.Columns[0].ColumnName + "\":\"" + oDr[0] + "\", \"" + poDt.Columns[1].ColumnName + "\":\"" + oDr[1] + "\", ");
                    var oHijos = (from r in poDt.AsEnumerable()
                                  where r.Field<Int32>(poDt.Columns[0].ColumnName) == xidObjeto
                                  select new
                                  {
                                      xid = r.Field<Int32>(poDt.Columns[3].ColumnName),
                                      vDescripcion = r.Field<string>(poDt.Columns[4].ColumnName),
                                      vValue = r.Field<string>(poDt.Columns[5].ColumnName)
                                  }).ToList();
                    vResult.Append("\"" + oDr[2] + "\":" + JsonConvert.SerializeObject(oHijos) + " ");
                    vResult.Append("},");
                    vCantObjetos++;
                }
            }
            if (vResult.ToString().EndsWith(","))
                vResult.Length--;
            vResult.Append("]");
            return "{\"length\":" + vCantObjetos.ToString() + ", " + vResult.ToString();
        }

        public static string armarJsonError(DataTable dt)
        {
            string vRespuesta = "";
            if (dt.Rows.Count > 0)
                vRespuesta = "\"error\":{ \"xidError\":" + dt.Rows[0]["vCod"].ToString() + ", \"xidTipoError\":\"" + dt.Rows[0]["vType"].ToString() + "\", \"msjError\":\"" + dt.Rows[0]["vDesc"].ToString().Replace("\"", "\\\"") + "\"}";
            else
                vRespuesta = "\"error\":{ \"xidError\":-1, \"xidTipoError\":\"ER\", \"msjError\":\"Ha ocurrido un error desconocido!!\"}";
            return Regex.Replace(vRespuesta.ToString(), @"\r\n?|\n", "");
        }

        public static string serializarJsonResponse(DataSet oDs)
        {
            StringBuilder vObjSerialize = new StringBuilder();
            string vTipoObjeto = "";
            vObjSerialize.Append("\"data\":{");
            try
            {
                if (oDs.Tables.Count == 1)
                {
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"length\":0,\"objects\":{}");
                }
                else if (oDs.Tables.Count == 2)
                {
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"length\":1,\"objects\":{");
                    vTipoObjeto = "table";
                    vObjSerialize.Append("\"" + vTipoObjeto + "1\":{\"length\":" + oDs.Tables[1].Rows.Count.ToString() + ",\"data\":");
                    vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[1], Formatting.None));
                    vObjSerialize.Append("}");
                    vObjSerialize.Append("}");
                }
                else if (oDs.Tables.Count > 2)
                {
                    int vCantTablas = 1;//ID de tabla sobre el cual avanza el bucle del dataset, empieza en 1 porque la primera tabla del dataset es el error/success
                    int vIdTabla = 0;
                    int vIdRow = 0;
                    int vidGrid = 0;
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"objects\":{");
                    while (vCantTablas < oDs.Tables.Count)
                    {
                        bool vFlagPaginado = oDs.Tables[vCantTablas].Columns.Contains("cantFilas");
                        bool vFlagTipoTabla = oDs.Tables[vCantTablas].Columns.Contains("vTipoTabla");

                        if (vFlagPaginado)
                        {
                            vidGrid++;
                            vObjSerialize.Append("\"grid" + vidGrid.ToString() + "\":{\"length\":" + oDs.Tables[vCantTablas].Rows[0]["cantFilas"].ToString() + ",\"data\":");
                            vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[(vCantTablas + 1)], Formatting.None));
                            vObjSerialize.Append("},");
                            vCantTablas++;
                        }
                        else if (!vFlagPaginado && !vFlagTipoTabla)
                        {
                            vTipoObjeto = "table";
                            vIdTabla++;
                            vObjSerialize.Append("\"" + vTipoObjeto + "" + vIdTabla.ToString() + "\":{\"length\":" + oDs.Tables[vCantTablas].Rows.Count.ToString() + ",\"data\":");
                            vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[vCantTablas], Formatting.None));
                            vObjSerialize.Append("},");
                        }
                        else if (!vFlagPaginado && vFlagTipoTabla)
                        {
                            if (oDs.Tables[vCantTablas].Rows[0]["vTipoTabla"].Equals("Pivot"))
                            {
                                vTipoObjeto = "pivot";
                                vIdTabla++;
                                vCantTablas++;
                                vObjSerialize.Append("\"" + vTipoObjeto + "" + vIdTabla.ToString() + "\":");
                                vObjSerialize.Append(parseToJsonCabDet(oDs.Tables[vCantTablas]));
                                vObjSerialize.Append("},");
                            }
                        }
                        vCantTablas++;
                    }
                    if (vObjSerialize.ToString().EndsWith(","))
                        vObjSerialize.Length--;
                    vObjSerialize.Append("}");
                    vObjSerialize.Append(",\"length\":" + (vIdRow + vIdTabla + vidGrid).ToString() + " ");
                }
                vObjSerialize.Append("}").Replace("'", "");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return Regex.Replace(vObjSerialize.ToString(), @"\r\n?|\n", "");
        }

        public static DataSet agregarJsonMensajeDS(DataSet oDs)
        {
            List<KeyValuePair<string, string>> pCampos = new List<KeyValuePair<string, string>>();
            pCampos.Add(new KeyValuePair<string, string>("vCod", "200"));
            pCampos.Add(new KeyValuePair<string, string>("vType", "RS"));
            pCampos.Add(new KeyValuePair<string, string>("vDesc", "OK"));
            DataSet ds = new DataSet();
            int cont = 1;
            DataTable dtError = new DataTable();
            foreach (KeyValuePair<string, string> columna in pCampos)
                dtError.Columns.Add(columna.Key.ToString());
            DataRow row = dtError.NewRow();
            if (oDs.Tables[0].Columns[0].ColumnName == "vCod")
            {
                int temp = 0;
                foreach (KeyValuePair<string, string> columna in pCampos)
                {
                    row[columna.Key.ToString()] = oDs.Tables[0].Rows[0].ItemArray[temp].ToString();
                    temp++;
                }
            }
            else
            {
                foreach (KeyValuePair<string, string> columna in pCampos)
                    row[columna.Key.ToString()] = columna.Value.ToString();
            }
            dtError.Rows.Add(row);
            ds.Tables.Add(dtError);
            foreach (DataTable tabla in oDs.Tables)
            {
                cont++;
                DataTable dt = new DataTable();
                dt = tabla.Copy();
                dt.TableName = string.Concat("Table", cont);
                ds.Tables.Add(dt);
            }
            return ds;
        }

    }
}