﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using GooglePickerAPI.googleApi;
using Newtonsoft.Json.Linq;
using Utils;

namespace WebSCA.googleApi
{
    public class VGoogleApi
    {
        private VCredentialSecrets vCredentialSecrets;
        public VCredentialUser vCredentialUser;
        private string url_token = "https://www.googleapis.com/oauth2/v4/token";

        public VGoogleApi()
        {
            vCredentialSecrets = new VCredentialSecrets();
            vCredentialUser = new VCredentialUser();
        }

        /// <summary>
        /// Genera las credenciales necesarias para trabajar con la API de google drive
        /// </summary>
        /// <param name="apiName">Direccíón relativa donde se almacenara el archivo de autorización</param>
        /// <param name="scopes">Alcance del uso de la API - https://developers.google.com/drive/api/v2/about-auth</param>
        /// <returns></returns>
        public DriveService GenCredential(string apiName, string[] scopes)
        {
            try
            {
                string pathCredentialJson = Params.PATH_CREDENTIAL_SSP_JSON;

                if (string.IsNullOrEmpty(pathCredentialJson))
                    throw new Exception("Path to the service account credentials file is required.");

                if (!System.IO.File.Exists(pathCredentialJson))
                    throw new Exception("The service account credentials file does not exist at: " + pathCredentialJson);

                //https://developers.google.com/identity/protocols/googlescopes
                string pathStoreCredential = AppDomain.CurrentDomain.BaseDirectory + apiName;
                UserCredential credential;

                using (FileStream fileStream = new FileStream(pathCredentialJson, FileMode.Open, FileAccess.Read))
                {
                    ClientSecrets clientSecrets = GoogleClientSecrets.Load(fileStream).Secrets;
                    vCredentialSecrets.client_id = clientSecrets.ClientId;
                    vCredentialSecrets.client_secret = clientSecrets.ClientSecret;

                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        clientSecrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(pathStoreCredential, true)).Result;

                    if (credential.Token.IsExpired(credential.Flow.Clock))
                    {
                        //Console.WriteLine("The access token has expired, refreshing it");

                        if (credential.RefreshTokenAsync(CancellationToken.None).Result)
                        {
                            Console.WriteLine("The access token is now refreshed");
                        }
                        else
                        {
                            //Console.WriteLine("The access token has expired but we can't refresh it :(");
                            //return;
                        }
                    }

                    vCredentialUser.access_token = credential.Token.AccessToken;
                    HttpContext.Current.Session["access_token"] = credential.Token.AccessToken;

                    vCredentialUser.token_type = credential.Token.TokenType;
                    vCredentialUser.expires_in = credential.Token.ExpiresInSeconds;

                    vCredentialUser.refresh_token = credential.Token.RefreshToken;
                    HttpContext.Current.Session["refresh_token"] = credential.Token.RefreshToken;

                    vCredentialUser.IssuedUtc = credential.Token.IssuedUtc;
                    HttpContext.Current.Session["expired_time_Token"] = credential.Token.IssuedUtc;

                }

                // Create the  Analytics service.
                return new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "PRESET",
                });

            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo generar las credenciales de Usuario", ex);
            }
        }

        /// <summary>
        /// En caso de culminacion de tiempo del access token, se generara uno nuevo a partir de un refresh token
        /// </summary>
        /// <param name="refreshToken">token refresh obtenido del token actual</param>
        protected void NewAccessToken(string refreshToken)
        {
            try
            {
                string pathCredentialJson = Params.PATH_CREDENTIAL_SSP_JSON;

                using (FileStream fileStream = new FileStream(pathCredentialJson, FileMode.Open, FileAccess.Read))
                {
                    ClientSecrets clientSecrets = GoogleClientSecrets.Load(fileStream).Secrets;
                    vCredentialSecrets.client_id = clientSecrets.ClientId;
                    vCredentialSecrets.client_secret = clientSecrets.ClientSecret;

                    List<String> data = new List<string>(); //HttpUtility.UrlEncode
                    data.Add("client_id=" + WebUtility.HtmlEncode(vCredentialSecrets.client_id)); //HttpUtility.HtmlEncode
                    data.Add("client_secret=" + WebUtility.HtmlEncode(vCredentialSecrets.client_secret));
                    data.Add("refresh_token=" + refreshToken);
                    data.Add("grant_type=" + WebUtility.HtmlEncode("refresh_token"));

                    vCredentialUser.IssuedUtc = vCredentialUser.IssuedUtc.AddSeconds(3600);
                    dynamic data_response = JObject.Parse(Utils.FacadeProxyWSUtils.invokePostExterno(url_token, data));

                    vCredentialUser.access_token = data_response.access_token;
                    HttpContext.Current.Session["access_token"] = data_response.access_token;

                    double aux = data_response.expires_in.Value;
                    HttpContext.Current.Session["expired_time_Token"] = DateTime.Now.AddSeconds(aux);
                }


            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo renovar el Token de Acceso", ex);
            }

        }
    }

    public class VGoogleDriveApi : VGoogleApi
    {
        string nameFileCredentialUser = "componente";
        string[] scopes = new string[] { "https://www.googleapis.com/auth/drive" };


        /// <summary>
        /// Atributo que tendra la conexion con la API de Google, se invoca una sola vez para uso global
        /// </summary>
        public static DriveService service;

        ///: base(apiName, scopes)
        public VGoogleDriveApi() : base()
        {
        }

        public string getAccessToken(string refreshToken)
        {
            try
            {

                if (refreshToken == "")
                {
                    GenCredential(nameFileCredentialUser, scopes);
                    NewAccessToken(vCredentialUser.refresh_token);
                }
                else
                {
                    NewAccessToken(refreshToken);
                }

                return vCredentialUser.access_token;
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo obtener el Hit con las credenciales actuales", ex);
            }

        }

        /// <summary>
        /// Genera un access token para uso del picker
        /// </summary>
        /// <returns></returns>
        public string getAccessToken()
        {
            HttpContext.Current.Session.Clear();

            try
            {
                if (HttpContext.Current.Session["expired_time_Token"] != null)
                {
                    this.vCredentialUser.IssuedUtc = (DateTime)HttpContext.Current.Session["expired_time_Token"];
                    if (!this.vCredentialUser.ValidToken())
                    {
                        NewAccessToken(HttpContext.Current.Session["refresh_token"].ToString());
                    }
                    else vCredentialUser.access_token = HttpContext.Current.Session["access_token"].ToString();
                }
                else
                    GenCredential(nameFileCredentialUser, scopes);

                return vCredentialUser.access_token;
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo obtener el Hit con las credenciales actuales", ex);
            }

        }

        /// <summary>
        /// Obtiene el tamaño de un archivo
        /// </summary>
        /// <param name="id">ID del archivo proveniente de google drive</param>
        /// <returns></returns>
        public string getFileSize(string id)
        {

            service = GenCredential(nameFileCredentialUser, scopes);

            // Retrieve the existing parents to remove
            Google.Apis.Drive.v3.FilesResource.GetRequest getRequest = service.Files.Get(id);
            getRequest.Fields = "size";
            Google.Apis.Drive.v3.Data.File file = getRequest.Execute();

            return file.Size.ToString();
        }

        /// <summary>
        /// Elimina un archivo de google drive
        /// </summary>
        /// <param name="id">ID del archivo proveniente de google drive</param>
        /// <returns></returns>
        public string deleteFile(string id)
        {

            service = GenCredential(nameFileCredentialUser, scopes);

            try
            {
                // Initial validation.
                if (service == null)
                    throw new ArgumentNullException("service");

                if (id == null)
                    throw new ArgumentNullException(id);

                // Make the request.
                return service.Files.Delete(id).Execute();
            }
            catch (Exception ex)
            {
                throw new Exception("Request Files.Delete failed.", ex);
            }

            return "-1";

        }

        /// <summary>
        /// Realiza un download bajo conexion con la API
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns>string FilePath</returns>
        public string DownloadGoogleFile(string fileId)
        {
            service = GenCredential(nameFileCredentialUser, scopes);

            //DriveService service = AuthenticateServiceAccount();
            string FolderPath = HttpContext.Current.Server.MapPath("~/GoogleDriveFilesTemp");
            FilesResource.GetRequest request = service.Files.Get(fileId);

            string FileName = request.Execute().Name;
            string FilePath = System.IO.Path.Combine(FolderPath, FileName);

            MemoryStream stream1 = new MemoryStream();

            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case DownloadStatus.Downloading:
                        {
                            //Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case DownloadStatus.Completed:
                        {
                            //Console.WriteLine("Download complete.");
                            SaveStream(stream1, FilePath);
                            break;
                        }
                    case DownloadStatus.Failed:
                        {
                            //Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream1);
            return FilePath;
        }

        private static void SaveStream(MemoryStream stream, string FilePath)
        {
            using (System.IO.FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                stream.WriteTo(file);
            }
        }

    }

    public class VCredentialSecrets
    {
        public string client_id { get; set; }
        public string client_secret { get; set; }
    }

    public class VCredentialUser
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public long? expires_in { get; set; }
        public string refresh_token { get; set; }
        public DateTime Issued { get; set; }
        public DateTime IssuedUtc { get; set; }

        public bool ValidToken()
        {
            DateTime utcNow = DateTime.UtcNow;
            TimeSpan diff = utcNow.Subtract(IssuedUtc);
            return diff.TotalSeconds <= 3600;
        }
    }

 

}