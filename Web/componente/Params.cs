﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace GooglePickerAPI.googleApi
{
    public class Params
    {
        public static string WS_API_URL = ConfigurationManager.AppSettings["WS_API_URL"];
        public static string WS_API_GEO_URL = ConfigurationManager.AppSettings["WS_API_GEO_URL"];
        public static string WS_WEB_FORM_URL = ConfigurationManager.AppSettings["WS_WEB_FORM_URL"];
        public static string VER_STATIC = ConfigurationManager.AppSettings["VER_STATIC"];
        public static string URL_STATIC = ConfigurationManager.AppSettings["URL_STATIC"];
        public static string PATH_CREDENTIAL_SSP_JSON = HostingEnvironment.MapPath(@"~/componente") + "\\" + ConfigurationManager.AppSettings["GAUTH_CRED_FILE_NAME"];


    }
}