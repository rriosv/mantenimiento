﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;



namespace Utils
{
    public class StringUtils
    {
        #region string
        public static string serializarJsonResponse(DataSet oDs)
        {
            StringBuilder vObjSerialize = new StringBuilder();
            string vTipoObjeto = "";
            vObjSerialize.Append("\"data\":{");
            try
            {
                if (oDs.Tables.Count == 1)
                {
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"length\":0,\"objects\":{}");
                }
                else if (oDs.Tables.Count == 2)
                {
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"length\":1,\"objects\":{");
                    vTipoObjeto = "table";
                    vObjSerialize.Append("\"" + vTipoObjeto + "1\":{\"length\":" + oDs.Tables[1].Rows.Count.ToString() + ",\"data\":");
                    vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[1], Formatting.None));
                    vObjSerialize.Append("}");
                    vObjSerialize.Append("}");
                }
                else if (oDs.Tables.Count > 2)
                {
                    int vCantTablas = 1;//ID de tabla sobre el cual avanza el bucle del dataset, empieza en 1 porque la primera tabla del dataset es el error/success
                    int vIdTabla = 0;
                    int vIdRow = 0;
                    int vidGrid = 0;
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"objects\":{");
                    while (vCantTablas < oDs.Tables.Count)
                    {
                        bool vFlagPaginado = oDs.Tables[vCantTablas].Columns.Contains("cantFilas");
                        bool vFlagTipoTabla = oDs.Tables[vCantTablas].Columns.Contains("vTipoTabla");

                        if (vFlagPaginado)
                        {
                            vidGrid++;
                            vObjSerialize.Append("\"grid" + vidGrid.ToString() + "\":{\"length\":" + oDs.Tables[vCantTablas].Rows[0]["cantFilas"].ToString() + ",\"data\":");
                            vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[(vCantTablas + 1)], Formatting.None));
                            vObjSerialize.Append("},");
                            vCantTablas++;
                        }
                        else if (!vFlagPaginado && !vFlagTipoTabla)
                        {
                            vTipoObjeto = "table";
                            vIdTabla++;
                            vObjSerialize.Append("\"" + vTipoObjeto + "" + vIdTabla.ToString() + "\":{\"length\":" + oDs.Tables[vCantTablas].Rows.Count.ToString() + ",\"data\":");
                            vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[vCantTablas], Formatting.None));
                            vObjSerialize.Append("},");
                        }
                        else if (!vFlagPaginado && vFlagTipoTabla)
                        {
                            if (oDs.Tables[vCantTablas].Rows[0]["vTipoTabla"].Equals("Pivot"))
                            {
                                vTipoObjeto = "pivot";
                                vIdTabla++;
                                vCantTablas++;
                                vObjSerialize.Append("\"" + vTipoObjeto + "" + vIdTabla.ToString() + "\":");
                                vObjSerialize.Append(parseToJsonCabDet(oDs.Tables[vCantTablas]));
                                vObjSerialize.Append("},");
                            }
                        }
                        vCantTablas++;
                    }
                    if (vObjSerialize.ToString().EndsWith(","))
                        vObjSerialize.Length--;
                    vObjSerialize.Append("}");
                    vObjSerialize.Append(",\"length\":" + (vIdRow + vIdTabla + vidGrid).ToString() + " ");
                }
                vObjSerialize.Append("}").Replace("'", "");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return Regex.Replace(vObjSerialize.ToString(), @"\r\n?|\n", "");
        }
        public static string serializarJsonResponse(DataSet oDs, int[] xidTables)
        {
            StringBuilder vObjSerialize = new StringBuilder();
            vObjSerialize.Append("\"data\":{");
            try
            {
                if (oDs.Tables.Count == 1)
                {
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"length\":0,\"objects\":{}");
                }
                else
                {
                    int vCantTablas = 1;//ID de tabla sobre el cual avanza el bucle del dataset, empieza en 1 porque la primera tabla del dataset es el error/success
                    int vIdTabla = 0;
                    int vidTranspose = 0;
                    vObjSerialize.Append(armarJsonError(oDs.Tables[0]));
                    vObjSerialize.Append(",\"objects\":{");
                    DataTable oDt = null;
                    while (vCantTablas < oDs.Tables.Count)
                    {
                        if (xidTables.Contains(vCantTablas) && oDs.Tables[vCantTablas].Rows.Count > 0)
                        {
                            vidTranspose++;
                            oDt = ObjectUtils.transponerTabla(oDs.Tables[vCantTablas]);
                            vObjSerialize.Append("\"transpose" + vidTranspose.ToString() + "\":{\"length\":" + oDs.Tables[vCantTablas].Rows.Count.ToString() + ",\"data\":");
                            vObjSerialize.Append("[");
                            foreach (DataRow oDr in oDt.Rows)
                            {
                                vObjSerialize.Append("{");
                                vObjSerialize.Append("type: \"stackedColumn\",");
                                vObjSerialize.Append("toolTipContent:\"{label}<br/><span " + "style=" + "\'" + @"\" + "\"" + "\'" + "color: {color};" + "\'" + @"\" + "\"" + "\'" + " ><strong>{name}</strong></span>: {y}mn tonnes\",");
                                vObjSerialize.Append("name: \"" + oDr[0].ToString() + "\",");
                                vObjSerialize.Append("showInLegend: \"true\",");
                                vObjSerialize.Append("dataPoints: [");
                                foreach (DataColumn oDc in oDt.Columns)
                                {
                                    if (oDc.Ordinal > 0)
                                    {
                                        vObjSerialize.Append("{");
                                        vObjSerialize.Append("y:" + oDr[oDc.ColumnName].ToString());
                                        vObjSerialize.Append(",label:\"" + oDc.ColumnName + "\"");
                                        vObjSerialize.Append("},");
                                    }
                                }
                                if (vObjSerialize.ToString().EndsWith(","))
                                    vObjSerialize.Length--;
                                vObjSerialize.Append("]");
                                vObjSerialize.Append("},");
                            }
                            if (vObjSerialize.ToString().EndsWith(","))
                                vObjSerialize.Length--;
                            vObjSerialize.Append("]");
                            vObjSerialize.Append("},");
                        }
                        else
                        {
                            vIdTabla++;
                            vObjSerialize.Append("\"table" + vIdTabla.ToString() + "\":{\"length\":" + oDs.Tables[vCantTablas].Rows.Count.ToString() + ",\"data\":");
                            vObjSerialize.Append(JsonConvert.SerializeObject(oDs.Tables[vCantTablas], Formatting.None));
                            vObjSerialize.Append("},");
                        }
                        vCantTablas++;
                    }
                    if (vObjSerialize.ToString().EndsWith(","))
                        vObjSerialize.Length--;
                    vObjSerialize.Append("}");
                    vObjSerialize.Append(",\"length\":" + (vIdTabla + vidTranspose).ToString() + " ");
                }
                vObjSerialize.Append("}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return Regex.Replace(vObjSerialize.ToString(), @"\r\n?|\n", "");
        }
        public static string serializarJsonResponse(List<String> oList)
        {
            StringBuilder vObjSerialize = new StringBuilder();
            string vTipoObjeto = "";
            vObjSerialize.Append("\"data\":{");
            try
            {
                if (oList.Count == 0)
                {
                    vObjSerialize.Append("\"objects\":{\"length\":0,\"data\":{}}");
                }
                else if (oList.Count > 0)
                {
                    vObjSerialize.Append("\"objects\":{");
                    vTipoObjeto = "table";
                    vObjSerialize.Append("\"" + vTipoObjeto + "1\":{\"length\":" + oList.Count.ToString() + ",\"data\":");
                    vObjSerialize.Append(JsonConvert.SerializeObject(oList, Formatting.None));
                    vObjSerialize.Append("}");
                    vObjSerialize.Append("}");
                }

                vObjSerialize.Append("}").Replace("'", "");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return Regex.Replace(vObjSerialize.ToString(), @"\r\n?|\n", "");
        }
        public static string armarJsonError(DataTable dt)
        {
            string vRespuesta = "";
            if (dt.Rows.Count > 0)
                vRespuesta = "\"error\":{ \"xidError\":" + dt.Rows[0]["vCod"].ToString() + ", \"xidTipoError\":\"" + dt.Rows[0]["vType"].ToString() + "\", \"msjError\":\"" + dt.Rows[0]["vDesc"].ToString().Replace("\"", "\\\"") + "\"}";
            else
                vRespuesta = "\"error\":{ \"xidError\":-1, \"xidTipoError\":\"ER\", \"msjError\":\"Ha ocurrido un error desconocido!!\"}";
            return Regex.Replace(vRespuesta.ToString(), @"\r\n?|\n", "");
        }
        public static string armarJsonError(int xidError, string xidTipoError, string pMsjError)
        {
            string vRespuesta = "";
            vRespuesta = "\"data\":{\"error\":{ \"xidError\":" + xidError.ToString() + ", \"xidTipoError\":\"" + xidTipoError + "\", \"msjError\":\"" + pMsjError.Replace("\"", "\\\"") + "\"},\"length\":0,\"objects\":{}}";
            return Regex.Replace(vRespuesta.ToString(), @"\r\n?|\n", "");
        }
        public static string armarJsonMensaje(string pNombreObjeto, Dictionary<string, string> pDiccionario)
        {
            string vRespuesta = "";
            try
            {
                vRespuesta = JsonConvert.SerializeObject(pDiccionario);
                vRespuesta = "\"" + pNombreObjeto + "\":" + vRespuesta;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return Regex.Replace(vRespuesta.ToString(), @"\r\n?|\n", "");
        }
        public static string eliminarDuplicadosCSV(string pObjeto)
        {
            string vRespuesta = "";
            try
            {
                vRespuesta = string.Join(",", pObjeto.Split(',').Distinct());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                vRespuesta = pObjeto;
                throw ex;
            }
            return vRespuesta;
        }
        public static string parseToJson(DataTable poDt)
        {
            return JsonConvert.SerializeObject(poDt);
        }
        public static string parseToJsonCabDet(DataTable poDt)
        {
            StringBuilder vResult = new StringBuilder("\"data\":[");
            int xidObjeto = -1;
            int vCantObjetos = 0;
            foreach (DataRow oDr in poDt.Rows)
            {
                if (xidObjeto != Int32.Parse(oDr[0].ToString()))
                {
                    xidObjeto = Int32.Parse(oDr[0].ToString());
                    vResult.Append("{");
                    vResult.Append("\"" + poDt.Columns[0].ColumnName + "\":\"" + oDr[0] + "\", \"" + poDt.Columns[1].ColumnName + "\":\"" + oDr[1] + "\", ");
                    var oHijos = (from r in poDt.AsEnumerable()
                                  where r.Field<Int32>(poDt.Columns[0].ColumnName) == xidObjeto
                                  select new
                                  {
                                      xid = r.Field<Int32>(poDt.Columns[3].ColumnName),
                                      vDescripcion = r.Field<string>(poDt.Columns[4].ColumnName),
                                      vValue = r.Field<string>(poDt.Columns[5].ColumnName)
                                  }).ToList();
                    vResult.Append("\"" + oDr[2] + "\":" + JsonConvert.SerializeObject(oHijos) + " ");
                    vResult.Append("},");
                    vCantObjetos++;
                }
            }
            if (vResult.ToString().EndsWith(","))
                vResult.Length--;
            vResult.Append("]");
            return "{\"length\":" + vCantObjetos.ToString() + ", " + vResult.ToString();
        }
        public static string[] parseDatarowToArray(DataRow poDr)
        {
            return (from o in poDr.ItemArray select o.ToString()).ToArray();
        }
        public static string armarJsonFiltro(Dictionary<string, string> poDictParam)
        {
            string vRespuesta = "";
            foreach (KeyValuePair<string, string> oPair in poDictParam)
            {
                vRespuesta = vRespuesta + armarJsonFiltro(oPair.Key, "", oPair.Value, "LIKE", "string") + ",";
            }
            if (vRespuesta.Length > 0)
                vRespuesta = vRespuesta.Remove(vRespuesta.Length - 1);
            return "[" + vRespuesta + "]";
        }
        public static string armarJsonFiltro(string pNomColumna, string pTipoOrden, string pValue, string pOperador, string pTipoDato)
        {
            string vRespuesta = "";
            if (!pNomColumna.Equals("") && !pValue.Equals("") && !pOperador.Equals("") && !pTipoDato.Equals(""))
                vRespuesta = "{\"col\":\"" + pNomColumna + "\",\"ord\":\"" + pTipoOrden + "\",\"value\":\"" + pValue + "\",\"type\":\"" + pOperador + "\",\"datatype\":\"" + pTipoDato + "\"}";
            return vRespuesta;
        }
        public static string buscarValorLlave(Dictionary<string, string> oDict, string pKey)
        {
            string vValor = "";
            if (oDict.ContainsKey(pKey) && oDict[pKey] != null)
                vValor = oDict[pKey];
            return vValor;
        }
        public static string crearPassword(int pLongitud, int pComplejidad)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider oCsp = new System.Security.Cryptography.RNGCryptoServiceProvider();
            // Define the possible character classes where complexity defines the number
            // of classes to include in the final output.
            char[][] oClasses = { @"abcdefghijklmnopqrstuvwxyz".ToCharArray(), @"ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray(), @"0123456789".ToCharArray(), @" !""#$%&'()*+,./:;<>?@[\]^_{|}~".ToCharArray(), };
            pComplejidad = Math.Max(1, Math.Min(oClasses.Length, pComplejidad));
            if (pLongitud < pComplejidad)
                throw new ArgumentOutOfRangeException("length");
            // Since we are taking a random number 0-255 and modulo that by the number of
            // characters, characters that appear earilier in this array will recieve a
            // heavier weight. To counter this we will then reorder the array randomly.
            // This should prevent any specific character class from recieving a priority
            // based on it's order.
            char[] oAllchars = oClasses.Take(pComplejidad).SelectMany(c => c).ToArray();
            byte[] oBytes = new byte[oAllchars.Length];
            oCsp.GetBytes(oBytes);
            for (int i = 0; i < oAllchars.Length; i++)
            {
                char oTmp = oAllchars[i];
                oAllchars[i] = oAllchars[oBytes[i] % oAllchars.Length];
                oAllchars[oBytes[i] % oAllchars.Length] = oTmp;
            }
            // Create the random values to select the characters
            Array.Resize(ref oBytes, pLongitud);
            char[] oResult = new char[pLongitud];
            while (true)
            {
                oCsp.GetBytes(oBytes);
                // Obtain the character of the class for each random byte
                for (int i = 0; i < pLongitud; i++)
                    oResult[i] = oAllchars[oBytes[i] % oAllchars.Length];
                // Verify that it does not start or end with whitespace
                if (Char.IsWhiteSpace(oResult[0]) || Char.IsWhiteSpace(oResult[(pLongitud - 1) % pLongitud]))
                    continue;
                string vTestResult = new string(oResult);
                // Verify that all character classes are represented
                if (0 != oClasses.Take(pComplejidad).Count(c => vTestResult.IndexOfAny(c) < 0))
                    continue;
                return vTestResult;
            }
        }
        public static string eliminarSignosAcentos(string pTexto)
        {
            string vConsignos = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
            string vSinsignos = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
            StringBuilder vTextoSinAcentos = new StringBuilder(pTexto.Length);
            int vIndexConAcento;
            foreach (char vCaracter in pTexto)
            {
                vIndexConAcento = vConsignos.IndexOf(vCaracter);
                if (vIndexConAcento > -1)
                    vTextoSinAcentos.Append(vSinsignos.Substring(vIndexConAcento, 1));
                else
                    vTextoSinAcentos.Append(vCaracter);
            }
            return vTextoSinAcentos.ToString();
        }
        public static string crearUniqueId()
        {
            return Guid.NewGuid().ToString();
        }
        public static string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonstring = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonstring;
        }
        #endregion
        #region bool
        public static bool TryParseJson(string pValue)
        {
            bool vFlag = true;
            try
            {
                dynamic oResult = JsonConvert.DeserializeObject(pValue);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                vFlag = false;
            }
            return vFlag;
        }
        public static bool buscarValorLlave(Dictionary<string, string> oDict, string pTexto, string pKey)
        {
            if (oDict.ContainsKey(pKey) && oDict[pKey] != null)
                return oDict[pKey].Equals(pTexto);
            return false;
        }
        #endregion
        #region httpResponse
        public static HttpResponseMessage JsonResult(string pResultado)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(pResultado, Encoding.UTF8, "application/json")
            };
        }

        //public JsonNetResult()
        //{
        //   Settings = new JsonSerializerSettings
        //    {
        //        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        //        NullValueHandling = NullValueHandling.Ignore
        //    };
        //}

        //public JsonSerializerSettings Settings { get; private set; }

        //public override void ExecuteResult(ControllerContext context)
        //{
        //    if (context == null)
        //        throw new ArgumentNullException("context");
        //    if (this.JsonRequestBehavior == JsonRequestBehavior.DenyGet && string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
        //        throw new InvalidOperationException("JSON GET is not allowed");

        //    HttpResponseBase response = context.HttpContext.Response;
        //    response.ContentType = string.IsNullOrEmpty(this.ContentType) ? "application/json" : this.ContentType;

        //    if (this.ContentEncoding != null)
        //        response.ContentEncoding = this.ContentEncoding;
        //    if (this.Data == null)
        //        return;

        //    var scriptSerializer = JsonSerializer.Create(this.Settings);

        //    using (var sw = new StringWriter())
        //    {
        //        scriptSerializer.Serialize(sw, this.Data);
        //        response.Write(sw.ToString());
        //    }
        //}

        #endregion
    }
}
