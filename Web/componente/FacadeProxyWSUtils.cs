﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Utils
{
    public class Response
    {
        public Response(HttpWebResponse response)
        {
            if (response == null)
            {
                this.description = "response == null";
                this.code = HttpStatusCode.BadRequest;
            }
            else
            {
                this.description = response.StatusDescription;
                this.code = response.StatusCode;
            }
        }
        public string description { get; set; }
        public HttpStatusCode code { get; set; }
        public bool unsuccess { get { return code != HttpStatusCode.OK; } }
        public override string ToString()
        {
            return ((int)code).ToString() + " - " + description;
        }
    }
    public class FacadeProxyWSUtils
    {

        public static string invokeGetExterno(string url)
        {
            String responseString = "";
            WebRequest request1;
            try
            {
                request1 = WebRequest.Create(url);
                request1.Method = "GET";
                WebResponse response1 = request1.GetResponse();
                using (Stream stream = response1.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("{0} Exception caught.", ex);
                responseString = "";
            }
            return responseString;
        }

        public static string invokePostExterno(string url, List<string> lKeyVal)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.Append(String.Join("&", lKeyVal));
            string sPostData = sbPostData.ToString();
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(sPostData);
            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;
            try
            {
                // Write data  
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }
            }
            catch (Exception e)
            {
                return jsonError("503", e.Message);
            }
            Response response_last;
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    response_last = new Response(response);
                    // Get the response stream  
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string responseString = reader.ReadToEnd();
                    return responseString;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response == null)
                {
                    response_last = new Response(null);
                    return jsonError("500", wex.Message);
                }
                using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                {
                    response_last = new Response(errorResponse);
                    StreamReader reader = new StreamReader(errorResponse.GetResponseStream(), Encoding.UTF8);
                    return jsonError("500", reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                return jsonError("503", e.Message);
            }
        }

        public static string invoke(string url)
        {
            WebRequest request1 = WebRequest.Create(url);
            request1.Method = "GET";
            WebResponse response1 = request1.GetResponse();
            String responseString = "";
            using (Stream stream = response1.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                responseString = reader.ReadToEnd();
            }
            responseString = responseString.Replace("\\", "");
            responseString = responseString.Trim(new Char[] { '\\', '\"' });
            responseString = responseString.Substring(1, responseString.Length - 2);
            var jo = JObject.Parse(responseString);
            return "[" + jo.ToString() + "]";
        }

        /// <summary>
        /// It makes a POST WS invocation to the WS layer, it returns a JSON result according to the parameters
        /// </summary>
        /// <param name="pPath">Web Service URL to consume</param>
        /// <param name="pController">Controller name</param>
        /// <param name="pAction">Action Name</param>
        /// <param name="pArgs">String with json parameters</param>
        /// <returns>string</returns>
        public static string invoke(string pPath, string pController, string pAction, string pArgs)
        {
            string vResponseReturnValue = string.Empty;
            try
            {
                CurlUtils oCurl = new CurlUtils();
                string vJsonDat = EncodeDecodeUtils.Base64Encode(pArgs);
                vJsonDat = Regex.Replace(vJsonDat, @"\t|\n|\r", "");
                vJsonDat = "=" + System.Net.WebUtility.HtmlEncode(vJsonDat);
                vJsonDat = oCurl.doPostEncodingUtf8(pPath + "api/" + pController.Replace("Controller", "").ToLower() + "/" + pAction + "/", vJsonDat);
                dynamic vDynPlainData = JsonConvert.DeserializeObject(vJsonDat);
                vResponseReturnValue = vJsonDat;
            }
            catch (Exception ex)
            {
                vResponseReturnValue = "[{" + StringUtils.armarJsonError(0, "ER", ex.Message) + "}]";
            }

            return vResponseReturnValue;
        }

        /// <summary>
        /// It makes a POST WS invocation to the WS layer, it returns a JSON result according to the parameters
        /// </summary>
        /// <param name="url">Web Service URL to consume</param>
        /// <param name="parameters">JSON Parameters encrypted via Base64Encode</param>
        /// <returns>string</returns>
        public static string invokePost(string url, string parameters)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            List<string> lKeyVal = new List<string>();
            StringBuilder sbPostData = new StringBuilder();
            lKeyVal.Add("=" + System.Net.WebUtility.HtmlEncode(parameters));
            sbPostData.Append(String.Join("&", lKeyVal));
            string sPostData = sbPostData.ToString();
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(sPostData);
            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;
            try
            {
                // Write data  
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }
            }
            catch (Exception e)
            {
                return jsonError("503", e.Message);
            }
            Response response_last;
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    response_last = new Response(response);
                    // Get the response stream  
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string responseString = reader.ReadToEnd();
                    return responseString;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response == null)
                {
                    response_last = new Response(null);
                    return jsonError("500", wex.Message);
                }
                using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                {
                    response_last = new Response(errorResponse);
                    StreamReader reader = new StreamReader(errorResponse.GetResponseStream(), Encoding.UTF8);
                    return jsonError("500", reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                return jsonError("503", e.Message);
            }
        }

        public async static Task<string> invokeGetAsync(string url)
        {
            return await Task.Factory.StartNew<string>(() =>
            {
                String responseString = "";
                WebRequest request1;
                try
                {
                    request1 = WebRequest.Create(url);
                    request1.Method = "GET";
                    WebResponse response1 = request1.GetResponse();
                    using (Stream stream = response1.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                        responseString = reader.ReadToEnd();
                        return responseString;
                    }
                }
                catch (Exception ex)
                {
                    return jsonError("503", ex.Message);
                }
            });
        }

        public async static Task<string> InvokePostAsync(string url, string parameters)
        {
            return await Task.Factory.StartNew<string>(() =>
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                List<string> lKeyVal = new List<string>();
                StringBuilder sbPostData = new StringBuilder();
                lKeyVal.Add("=" + System.Net.WebUtility.HtmlEncode(parameters));
                sbPostData.Append(String.Join("&", lKeyVal));
                string sPostData = sbPostData.ToString();
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(sPostData);
                // Set the content length in the request headers  
                request.ContentLength = byteData.Length;
                try
                {
                    // Write data  
                    using (Stream postStream = request.GetRequestStream())
                    {
                        postStream.Write(byteData, 0, byteData.Length);
                    }
                }
                catch (Exception e)
                {
                    return jsonError("503", e.Message);
                }
                Response response_last;
                try
                {
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        response_last = new Response(response);
                        // Get the response stream  
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        //string responseString = await reader.ReadToEndAsync();
                        string responseString = reader.ReadToEnd();
                        return responseString;
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response == null)
                    {
                        response_last = new Response(null);
                        return jsonError("500", wex.Message);
                    }
                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        response_last = new Response(errorResponse);
                        StreamReader reader = new StreamReader(errorResponse.GetResponseStream(), Encoding.UTF8);
                        return jsonError("500", reader.ReadToEnd());
                    }
                }
                catch (Exception e)
                {
                    return jsonError("503", e.Message);
                }
            });
        }
        private static string jsonError(string xidError, string msjError)
        {
            return "[{\"data\":{ \"error\":{\"xidError\": " + xidError + ",\"xidTipoError\":\"ER\", \"msjError\":\"" + msjError + "\"},\"length\":0, \"data\":{}}}]";
        }
        /// <summary>
        /// Realiza invocaciones a WS de forma generica, sólo aplica para los WS que sean de tipo SOAP
        /// </summary>
        /// <param name="pAmbito"></param>
        /// <param name="pWebMethodName"></param>
        /// <param name="pJsonArgs"></param>
        /// <param name="pJsonFilters"></param>
        /// <returns></returns>
        public static string invokeG(string pAmbito, string pWebMethodName, string pJsonArgs, Type oClassType, string pJsonFilters = "")
        {
            Dictionary<string, string> oDictionary = validarSesion();
            // NO hay sesion
            if (!oDictionary["msg"].Equals("OK"))
            {
                return "[{" + StringUtils.armarJsonMensaje("Session", oDictionary) + ",\"data\":{}}]";
            }
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            string vMethodReturnValue = string.Empty;
            string vResponseReturnValue = string.Empty;
            try
            {
                // Obteniendo informacion de sesion
                string vCookieValue = getCookieValue("UserData");
                Dictionary<string, string> vDiccionario = JsonConvert.DeserializeObject<Dictionary<string, string>>(vCookieValue);
                // SI hay sesion
                // Argumentos en formato Json que se pasaran al metodo a invocar
                dynamic vJsonArgs = oSerializer.Deserialize<object>(pJsonArgs);
                // Obteniendo la clase
                //Type oClassType = Type.GetType("WebSIP." + pAmbito + "SR." + pAmbito + "WSSoapClient");
                //Type oClassType = Type.GetType("FrontEnd." + pAmbito + "SR." + pAmbito + "WSSoapClient");

                // Obteniendo el metodo constructor
                ConstructorInfo oClassConstructor = oClassType.GetConstructor(Type.EmptyTypes);
                // Instanciando clase
                object oClassInstance = oClassConstructor.Invoke(new object[] { });
                // Obteniendo metodo statico
                MethodInfo oClassMethod = oClassType.GetMethod(pWebMethodName);
                ParameterInfo[] oArgs = oClassMethod.GetParameters();
                // Argumentos para pasar al metodo a invocar
                object[] oMethodArgs = new object[oArgs.Length];
                // Mapeando parametros
                foreach (ParameterInfo oArg in oArgs)
                {
                    // Addicionando el IdSession como primer paramentro
                    if (oArg.Position == 0) { oMethodArgs[0] = vDiccionario["IdSesion"]; continue; }
                    // Else
                    var gut = vJsonArgs[oArg.Name];
                    var gut2 = vJsonArgs[oArg.Name];
                    oMethodArgs[oArg.Position] = dynamicCast(gut, oArg.ParameterType);
                }
                vResponseReturnValue = "[{" + StringUtils.armarJsonMensaje("Session", oDictionary) + "," + (string)oClassMethod.Invoke(oClassInstance, oMethodArgs) + "}]";
            }
            catch (KeyNotFoundException ex)
            {
                vResponseReturnValue = "[{" + StringUtils.armarJsonMensaje("Session", oDictionary) + "," + StringUtils.armarJsonError(0, "ER", "Uno de los parámetros ingresados es incorrecto! " + ex.Message) + "}]";
            }
            catch (TargetInvocationException ex)
            {
                vResponseReturnValue = "[{" + StringUtils.armarJsonMensaje("Session", oDictionary) + "," + StringUtils.armarJsonError(0, "ER", "La función ejecutada no existe o aún no está implementada! " + ((ex.InnerException != null) ? ex.InnerException.Message : ex.Message)) + "}]";
            }
            catch (Exception ex)
            {
                vResponseReturnValue = "[{" + StringUtils.armarJsonMensaje("Session", oDictionary) + "," + StringUtils.armarJsonError(0, "ER", "[NK] " + ex.Message) + "}]";
            }
            return vResponseReturnValue;
        }
        /// <summary>
        /// Metodo que valida sesión de usuario
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> validarSesion()
        {
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            try
            {
                string vCookieValue = getCookieValue("UserData");
                if (!vCookieValue.Equals(""))
                {
                    oDictionary.Add("state", "1");
                    oDictionary.Add("msg", "OK");
                    oDictionary.Add("href", "");
                }
                else
                {
                    oDictionary.Add("state", "0");
                    oDictionary.Add("msg", "El usuario no tiene una sesión Iniciada.");
                    oDictionary.Add("href", "/LogOn.aspx");
                }
            }
            catch (Exception ex)
            {
                oDictionary.Add("state", "-1");
                oDictionary.Add("msg", "[CK] " + ex.Message);
                oDictionary.Add("href", "");
            }
            return oDictionary;
        }
        /// <summary>
        /// Metodo para obtener valores de una cookie
        /// </summary>
        /// <param name="pNombre"></param>
        /// <returns></returns>
        public static string getCookieValue(string pNombre)
        {
            HttpCookie oCookie = HttpContext.Current.Request.Cookies[pNombre];
            if (oCookie == null)
                return "";
            else
                return HttpUtility.UrlDecode(oCookie.Value);
        }
        /// <summary>
        /// dynamicCast
        /// </summary>
        /// <param name="oValue"></param>
        /// <param name="oTipoVar"></param>
        /// <returns></returns>
        public static dynamic dynamicCast(object oValue, Type oTipoVar)
        {
            return Convert.ChangeType(oValue, oTipoVar);
        }
        public static String getIP()
        {
            String ip = String.Empty;
            try
            {
                ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(ip))
                {
                    string[] ipRange = ip.Split(',');
                    int le = ipRange.Length - 1;
                    string trueIP = ipRange[le];
                }
                else
                {
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("El error es : " + ex.Message);
            }
            return ip;
        }
    }

}
