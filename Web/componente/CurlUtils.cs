﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Utils
{
   public class CurlUtils
    {
        public string doPost(string pUrl, string pData, Dictionary<string, string> pHeader = null)
        {
            string vRespuesta = "";
            WebRequest request = WebRequest.Create(pUrl);
            request.Proxy = null;
            request.Credentials = CredentialCache.DefaultCredentials;
            ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);
            try
            {
                request.Method = "POST";
                request.ContentType = "application/json;charset=\"utf-8\"";
                if (pHeader != null)
                    foreach (KeyValuePair<string, string> entry in pHeader)
                    {
                        request.Headers.Add(entry.Key, entry.Value);
                    }
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.WriteLine(pData);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                vRespuesta = reader.ReadToEnd();
            }
            catch (WebException webEx)
            {
                vRespuesta = "{\"id\":\"\",\"state\":0,\"msg\":\"" + webEx.Message + "\"}";
            }
            catch (Exception ex)
            {
                vRespuesta = "{\"id\":\"\",\"state\":0,\"msg\":\"" + ex.Message + "\"}";
            }
            return vRespuesta;
        }
        public string doPostEncodingUtf8(string pUrl, string pData, Dictionary<string, string> pHeader = null)
        {
            string vRespuesta = "";
            WebRequest request = WebRequest.Create(pUrl);
            request.Proxy = null;
            request.Credentials = CredentialCache.DefaultCredentials;
            ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);
            try
            {
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                if (pHeader != null)
                    foreach (KeyValuePair<string, string> entry in pHeader)
                    {
                        request.Headers.Add(entry.Key, entry.Value);
                    }
                byte[] oByteData = UTF8Encoding.UTF8.GetBytes(pData);
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(oByteData,0,oByteData.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                vRespuesta = reader.ReadToEnd();
            }
            catch (WebException webEx)
            {
                vRespuesta = "[{" + StringUtils.armarJsonError(0, "ER", webEx.Message) + "}]";
            }
            catch (Exception ex)
            {
                vRespuesta = "[{" + StringUtils.armarJsonError(0, "ER", ex.Message) + "}]";
            }
            return vRespuesta;
        }
        private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}