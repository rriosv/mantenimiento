﻿using System;
using System.Web.Security;

namespace Web
{
    public partial class PaginaError : System.Web.UI.Page
    {
        protected void Page_UnLoad(object sender, EventArgs e)
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
        }
        protected void Regresar(object sender, EventArgs e)
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            Response.Redirect("login");
        }
    }
}