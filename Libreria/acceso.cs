﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Data;

namespace seguridad
{
    public class acceso
    {
        RSAParameters rsaPrivateParams = new RSAParameters();
 
        private SqlConnection strCon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CnSTD"].ToString());

        public string ERROR;

        public acceso()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public SqlConnection GetStringConection()
        {
            /*SqlConnection strCon = new SqlConnection("Data Source = " 
                                                    + SERVER 
                                                    + ";initial Catalog = " 
                                                    + DATABASE 
                                                    + ";user Id=" 
                                                    + USER 
                                                    + ";Password = " 
                                                    + PASSWORD);*/
            return strCon;
        }

        public DataTable sp_validarUsuario(string strLogin, string strPassword)
        {
            SqlConnection oCon = null;
            string strCon = this.GetStringConection().ConnectionString;
            DataTable dt = null;
            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@login", SqlDbType.VarChar);
                arParams[0].Value = strLogin;
                arParams[1] = new SqlParameter("@passw", SqlDbType.VarChar);
                arParams[1].Value = strPassword;
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_validarUsuario", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }

        public string Encriptar(string str)
        {
            SHA1 sha1 = SHA1Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
        public string DecryptData(byte[] encrypted)
        {
            byte[] fromEncrypt;
            string roundTrip;
            ASCIIEncoding myAscii = new ASCIIEncoding();
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            rsaPrivateParams = rsaCSP.ExportParameters(true);
            rsaCSP.ImportParameters(rsaPrivateParams);
            fromEncrypt = rsaCSP.Decrypt(encrypted, false);
            roundTrip = myAscii.GetString(fromEncrypt);
            roundTrip = String.Format("{0}", roundTrip);
            return roundTrip;
        }
        
         public string RandomNumber(int size)
        {
            Random r = new Random();
            string legalChars = "123456789";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < size; i++)
                sb.Append(legalChars.Substring(r.Next(0, legalChars.Length - 1), 1));
            return sb.ToString();
        }

        public string RandomString(int size)
        {
            Random r = new Random();
            string legalChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < size; i++)
                sb.Append(legalChars.Substring(r.Next(0, legalChars.Length - 1), 1));
            return sb.ToString();
        }

        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public string GetPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }
    }
}
