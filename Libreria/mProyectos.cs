﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using seguridad;
using Data;
using System.Text.RegularExpressions;


namespace pry_san
{
    #region "Clase mGeneral"
    public class mGeneral
    {
        private acceso objAcceso = new acceso();
        mProyectos objMetodoProyectos = new mProyectos();
        public mGeneral()
        {
            //
            // TODO: Add constructor logic here
            //
        }
      
        
        public string CortarTexto(string textCrop, int num, string trail)
        {

            int iPos;
            int iMax;
            string sOut = string.Empty;

            // retira qualquer codigo HTML que a string Tiver
            Regex rx = new Regex("<[^>]*>");
            textCrop = rx.Replace(textCrop, "");

            iMax = num - trail.Length;

            if (iMax <= 0)
            {
                return "";
            }
            else if (textCrop.Length <= num)
            {
                return textCrop;
            }
            else
            {
                sOut = textCrop.Substring(0, num);
                iPos = sOut.LastIndexOf(" ");
                return sOut.Substring(0, iPos) + trail;
            }
        }
        
       
    }
    #endregion

    #region "Clase mAcceso"
    public class mAcceso
    {
        private acceso objAcceso = new acceso();
        public mAcceso()
        {

        }
       
        public void sp_agregarAcceso(int id_acceso, int id_menu, int id_usuario, string nom_usu)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlParameter[] arParams = new SqlParameter[4];
                arParams[0] = new SqlParameter("@id_acceso", SqlDbType.Int);
                arParams[0].Value = id_acceso;
                arParams[1] = new SqlParameter("@id_menu", SqlDbType.Int);
                arParams[1].Value = id_menu;
                arParams[2] = new SqlParameter("@id_usuario", SqlDbType.Int);
                arParams[2].Value = id_usuario;
                arParams[3] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
                arParams[3].Value = nom_usu;
                SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_agregarAcceso", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        public void sp_EliminaAcceso(int id_acceso, string nom_usu)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@id_acceso", SqlDbType.Int);
                arParams[0].Value = id_acceso;
                arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
                arParams[1].Value = nom_usu;
                SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_EliminaAcceso", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        //public DataTable sp_CargarUsuario(int cod_tipo_usuario, int cod_subsector, int cod_programa)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        SqlParameter[] arParams = new SqlParameter[3];
        //        arParams[0] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
        //        arParams[0].Value = cod_tipo_usuario;
        //        arParams[1] = new SqlParameter("@cod_subsector", SqlDbType.Int);
        //        arParams[1].Value = cod_subsector;
        //        arParams[2] = new SqlParameter("@cod_programa", SqlDbType.Int);
        //        arParams[2].Value = cod_programa;
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        public DataTable sp_CargarMenu(int id_menu_grupo)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;
            SqlDataReader dr = null;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlParameter[] arParams = new SqlParameter[1];
                arParams[0] = new SqlParameter("@id_menu_grupo", SqlDbType.Int);
                arParams[0].Value = id_menu_grupo;
                dr = SqlHelper.ExecuteReader(oCon, CommandType.StoredProcedure, "sp_CargarMenu", arParams);
                dt = new DataTable();
                dt.Load(dr);
                dr.Close();
                oCon.Close();
            }
            catch (Exception ex)
            {
                dr.Close();
                oCon.Close();
            }
            finally
            {
                dr.Close();
                oCon.Close();
            }
            return dt;
        }
        //public DataTable sp_CargarMenuPorUsuario(int id_usuario, int id_menu_grupo)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@id_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_usuario;
        //        arParams[1] = new SqlParameter("@id_menu_grupo", SqlDbType.Int);
        //        arParams[1].Value = id_menu_grupo;
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarMenuPorUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarPrograma(int id_usuario)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;
        //    SqlDataReader dr = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@id_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_usuario;
        //        dr = SqlHelper.ExecuteReader(oCon, CommandType.StoredProcedure, "sp_CargarPrograma", arParams);
        //        dt = new DataTable();
        //        dt.Load(dr);
        //        dr.Close();
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        dr.Close();
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        dr.Close();
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public int sp_verificaIdMenu(int id_menu_grupo, int id_menu)
        //{
        //    int val = 0;
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@id_menu_grupo", SqlDbType.Int);
        //        arParams[0].Value = id_menu_grupo;
        //        arParams[1] = new SqlParameter("@id_menu", SqlDbType.Int);
        //        arParams[1].Value = id_menu;
        //        val = Convert.ToInt16(SqlHelper.ExecuteScalar(oCon, CommandType.StoredProcedure, "sp_verificaIdMenu", arParams));
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return val;
        //}
        
    }
    #endregion
    
    #region "Clase mMenu"

    public class mMenu
    {
        private acceso objAcceso = new acceso();
        public mMenu()
        {

        }
        
        public void sp_EliminaMenu(int id_menu, string nom_usu)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@id_menu", SqlDbType.Int);
                arParams[0].Value = id_menu;
                arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
                arParams[1].Value = nom_usu;
                SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_EliminaMenu", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
       
        public DataTable sp_CargarGrupoMenu(int cod_tipo_usuario)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlParameter[] arParams = new SqlParameter[1];
                arParams[0] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
                arParams[0].Value = cod_tipo_usuario;
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarGrupoMenu", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }
    }
    #endregion
     
      
    #region "Clase mUsuario"
    public class mUsuario
    {
        private acceso objAcceso = new acceso();
        public mUsuario()
        {

        }
        //public DataTable sp_listarPerfilUsuario(int cod_tipo_usuario, int cod_subsector, int cod_programa)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        SqlParameter[] arParams = new SqlParameter[3];
        //        arParams[0] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
        //        arParams[0].Value = cod_tipo_usuario;
        //        arParams[1] = new SqlParameter("@cod_subsector", SqlDbType.Int);
        //        arParams[1].Value = cod_subsector;
        //        arParams[2] = new SqlParameter("@cod_programa", SqlDbType.Int);
        //        arParams[2].Value = cod_programa;
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_listarPerfilUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_listarUsuario(int id_perfil_usuario, int cod_tipo_usuario, int cod_subsector)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        SqlParameter[] arParams = new SqlParameter[3];
        //        arParams[0] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_perfil_usuario;
        //        arParams[1] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
        //        arParams[1].Value = cod_tipo_usuario;
        //        arParams[2] = new SqlParameter("@cod_subsector", SqlDbType.Int);
        //        arParams[2].Value = cod_subsector;
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_listarUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_GetByIdPerfilUsuario(int id_perfil_usuario)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_perfil_usuario;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_GetByIdPerfilUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_GetByIdUsuario(int id_usuario)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@id_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_usuario;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_GetByIdUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}

        //public void sp_agregarPerfilUsuario(int id_perfil_usuario, int cod_tipo_usuario, int cod_subsector, int cod_programa, string cod_depa, int cod_nivel_acceso, string nom_usu,int cod_entidad,string cod_prov)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[9];
        //        arParams[0] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_perfil_usuario;
        //        arParams[1] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
        //        arParams[1].Value = cod_tipo_usuario;
        //        arParams[2] = new SqlParameter("@cod_subsector", SqlDbType.Int);
        //        arParams[2].Value = cod_subsector;
        //        arParams[3] = new SqlParameter("@cod_programa", SqlDbType.Int);
        //        arParams[3].Value = cod_programa;
        //        arParams[4] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
        //        arParams[4].Value = cod_depa;
        //        arParams[5] = new SqlParameter("@cod_nivel_acceso", SqlDbType.Int);
        //        arParams[5].Value = cod_nivel_acceso;
        //        arParams[6] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[6].Value = nom_usu;
        //        arParams[7] = new SqlParameter("@cod_entidad", SqlDbType.Int);
        //        arParams[7].Value = cod_entidad;
        //        arParams[8] = new SqlParameter("@cod_prov", SqlDbType.VarChar);
        //        arParams[8].Value = cod_prov;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_agregarPerfilUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public void sp_agregarUsuario(int id_usuario, int id_perfil_usuario, string login, string passw, string nombre, string correo, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[7];
        //        arParams[0] = new SqlParameter("@id_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_usuario;
        //        arParams[1] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[1].Value = id_perfil_usuario;
        //        arParams[2] = new SqlParameter("@login", SqlDbType.VarChar);
        //        arParams[2].Value = login;
        //        arParams[3] = new SqlParameter("@passw", SqlDbType.VarChar);
        //        arParams[3].Value = passw;
        //        arParams[4] = new SqlParameter("@nombre", SqlDbType.VarChar);
        //        arParams[4].Value = nombre;
        //        arParams[5] = new SqlParameter("@correo", SqlDbType.VarChar);
        //        arParams[5].Value = correo;
        //        arParams[6] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[6].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_agregarUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
       
        //public void sp_EliminaUsuario(int id_usuario, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@id_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_usuario;
        //        arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[1].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_EliminaUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public void sp_EliminaPerfilUsuario(int id_perfil_usuario, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_perfil_usuario;
        //        arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[1].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_EliminaPerfilUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public void sp_ActivaDesactivaPerfilUsuario(int id_perfil_usuario, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_perfil_usuario;
        //        arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[1].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_ActivaDesactivaPerfilUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public void sp_ActivaDesactivaUsuario(int id_usuario, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@id_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_usuario;
        //        arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[1].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_ActivaDesactivaUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public DataTable sp_CargarTipoUsuario(int cod_tipo_usuario,int cod_entidad)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[2];
        //        arParams[0] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
        //        arParams[0].Value = cod_tipo_usuario;
        //        arParams[1] = new SqlParameter("@cod_entidad", SqlDbType.Int);
        //        arParams[1].Value = cod_entidad;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarTipoUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarRegion(int cod_sector)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@cod_entidad", SqlDbType.Int);
        //        arParams[0].Value = cod_sector;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarRegion",arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarRegionEdit(int id_perfil_usuario)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@id_perfil_usuario", SqlDbType.Int);
        //        arParams[0].Value = id_perfil_usuario;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarRegionEdit", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarRegionNuevo()
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarRegionNuevo");
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarPerfilUsuario(int cod_tipo_usuario)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
        //        arParams[0].Value = cod_tipo_usuario;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarPerfilUsuario", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarProv(int cod_depa)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@cod_depa", SqlDbType.Int);
        //        arParams[0].Value = cod_depa;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarProv", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
    }
    #endregion
    
    #region "Clase mProyectos"
    public class mProyectos
    {
        private acceso objAcceso = new acceso();
        public mProyectos()
        {
            //
            // TODO: Add constructor logic here
            //
            //this.objAcceso = new acceso();
        }
        public object GetValueFromSQL(string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            object obj = null;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                obj = SqlHelper.ExecuteScalar(oCon, CommandType.Text, sql);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return obj;
        }
        public void ExecuteFromSQL(string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlHelper.ExecuteNonQuery(oCon, CommandType.Text, sql);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        public DataTable dtListarFromSQL(string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.Text, sql);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }
        public void cargarListBox(ListBox lb, string ValueField, string TextField, string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            SqlDataReader dr = null;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                dr = SqlHelper.ExecuteReader(oCon, CommandType.Text, sql);
                lb.DataSource = dr;
                lb.DataValueField = ValueField;
                lb.DataTextField = TextField;
                lb.DataBind();
                dr.Close();
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        public void cargarCheckedListBox(MetaBuilders.WebControls.CheckedListBox chklb, string ValueField, string TextField, string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlDataReader dr = SqlHelper.ExecuteReader(oCon, CommandType.Text, sql);
                chklb.DataSource = dr;
                chklb.DataValueField = ValueField;
                chklb.DataTextField = TextField;
                chklb.DataBind();
                dr.Close();
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        public void cargarDropDownList(DropDownList ddl, string ValueField, string TextField, string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlDataReader dr = SqlHelper.ExecuteReader(oCon, CommandType.Text, sql);
                ddl.DataSource = dr;
                ddl.DataValueField = ValueField;
                ddl.DataTextField = TextField;
                ddl.DataBind();
                dr.Close();
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        public void cargarDropDownList(DropDownList ddl, string ValueField, string TextField, string setValue, string sql)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                SqlDataReader dr = SqlHelper.ExecuteReader(oCon, CommandType.Text, sql);
                ddl.DataSource = dr;
                ddl.DataValueField = ValueField;
                ddl.DataTextField = TextField;
                ddl.SelectedValue = setValue;
                ddl.DataBind();
                dr.Close();
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        //public DataTable sp_dtListadoProyectos(string strCriterio, string strBuscar, int codPrograma, int codPrioridad, string codDepa, int periodo, int estado, int CurrentPage, int PageSize)
        //{
        //    SqlParameter[] arParams = new SqlParameter[9];
        //    arParams[0] = new SqlParameter("@strCriterio", SqlDbType.VarChar);
        //    arParams[0].Value = strCriterio;
        //    arParams[1] = new SqlParameter("@strBuscar", SqlDbType.VarChar);
        //    arParams[1].Value = strBuscar;
        //    arParams[2] = new SqlParameter("@codPrograma", SqlDbType.Int);
        //    arParams[2].Value = codPrograma;
        //    arParams[3] = new SqlParameter("@codPrioridad", SqlDbType.Int);
        //    arParams[3].Value = codPrioridad;
        //    arParams[4] = new SqlParameter("@codDepa", SqlDbType.VarChar);
        //    arParams[4].Value = codDepa;
        //    arParams[5] = new SqlParameter("@periodo", SqlDbType.Int);
        //    arParams[5].Value = periodo;
        //    arParams[6] = new SqlParameter("@estado", SqlDbType.Int);
        //    arParams[6].Value = estado;
        //    arParams[7] = new SqlParameter("@CurrentPage", SqlDbType.Int);
        //    arParams[7].Value = CurrentPage;
        //    arParams[8] = new SqlParameter("@PageSize", SqlDbType.Int);
        //    arParams[8].Value = PageSize;
        //    DataTable dt = DatabaseUtility.ExecuteDataTable(oCon, "sp_dtListadoProyectos", CommandType.StoredProcedure, arParams);
        //    //DataTable dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_dtListadoProyectos", arParams);
        //    return dt;
        //}
        public DataTable sp_listadoProyectos(string strCriterio, string strBuscar, int id_usuario, int cod_tipo_usuario,
                         int cod_subsector, int cod_subsector_filtro, int cod_programa, int cod_programa_filtro,
                         int cod_prioridad, int cod_estrategia, int cod_clasificacion, string cod_depa, int periodo, 
                        int cod_estado,int intervencion, int foto)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[16];
                arParams[0] = new SqlParameter("@strCriterio", SqlDbType.VarChar);
                arParams[0].Value = strCriterio;
                arParams[1] = new SqlParameter("@strBuscar", SqlDbType.VarChar);
                arParams[1].Value = strBuscar;
                arParams[2] = new SqlParameter("@id_usuario", SqlDbType.Int);
                arParams[2].Value = id_usuario;
                arParams[3] = new SqlParameter("@cod_tipo_usuario", SqlDbType.Int);
                arParams[3].Value = cod_tipo_usuario;
                arParams[4] = new SqlParameter("@cod_subsector", SqlDbType.Int);
                arParams[4].Value = cod_subsector;
                arParams[5] = new SqlParameter("@cod_subsector_filtro", SqlDbType.Int);
                arParams[5].Value = cod_subsector_filtro;
                arParams[6] = new SqlParameter("@cod_programa", SqlDbType.Int);
                arParams[6].Value = cod_programa;
                arParams[7] = new SqlParameter("@cod_programa_filtro", SqlDbType.Int);
                arParams[7].Value = cod_programa_filtro;
                arParams[8] = new SqlParameter("@cod_prioridad", SqlDbType.Int);
                arParams[8].Value = cod_prioridad;
                arParams[9] = new SqlParameter("@cod_estrategia", SqlDbType.Int);
                arParams[9].Value = cod_estrategia;
                arParams[10] = new SqlParameter("@cod_clasificacion", SqlDbType.Int);
                arParams[10].Value = cod_clasificacion;
                arParams[11] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
                arParams[11].Value = cod_depa;
                arParams[12] = new SqlParameter("@periodo", SqlDbType.Int);
                arParams[12].Value = periodo;
                arParams[13] = new SqlParameter("@cod_estado", SqlDbType.Int);
                arParams[13].Value = cod_estado;
                arParams[14] = new SqlParameter("@intervencion", SqlDbType.Int);
                arParams[14].Value = intervencion;
                arParams[15] = new SqlParameter("@foto", SqlDbType.Int);
                arParams[15].Value = foto;
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_listadoProyectos1", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }
     
        public void sp_ActivaDesactivaProyecto(int id_proyecto, string nom_usu)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@id_proyecto", SqlDbType.Int);
                arParams[0].Value = id_proyecto;
                arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
                arParams[1].Value = nom_usu;
                oCon.Open();
                SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_ActivaDesactivaProyecto", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        //se modifico fviab,freso,fconv
        public int spiu_datosProyecto_DataEntry(int id_proyecto, int cprog, int cpri, int cestra, int claspry, int csest, string nompr, 
            int perio, string fviab, string nreso, string freso, string nconv, string fconv, string leyfu, int ctpro, int ctobr, int cmsni, 
            int csnip, double msnip, string osnip, string ccong, int npips, int cftef, int cmeje, string eejec, string coord, int nhsni, 
            int pbdir, int pbind, double minvi, double minve, double mejec, double mppto, double mpim, double mconv, int nempleo, string obpry, string nom_usu,
            string funcion,string prog, string subprog, string actividad, string proyecto, string compo,int meta,string submeta, int fuenteRO, 
            int fuenteFE,int fuenteGR, int fuenteGM,int fuentePO,double mcontraGR, double mcontraGM,double mcontraPO, int nro_conexAgua, int nro_conexAlc,
            int anticrisis, int continuidad, int inicial, string nemonico, int fuenteDonacion, double mDonancion, string clasificador,int intervencion,
            int tipo_accion,double mtransf,int subsector,string nrotransf, double mexpe_ficha,double mobra_ficha,double msuper_ficha, double mCIRA_ficha,
            string acuerdo_expe_ficha,string acuerdo_obra_ficha,string acuerdo_super_ficha,string acuerdo_CIRA_ficha,double cexpe_compro,double cobra_expe,
            double csuper_expe,double cCIRA_expe,string NroFORSUR )
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            int intVal = 0;
            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[81];
                arParams[0] = new SqlParameter("@id_proyecto", SqlDbType.Int);
                arParams[0].Value = id_proyecto;
                arParams[1] = new SqlParameter("@cprog", SqlDbType.Int);
                arParams[1].Value = cprog;
                arParams[2] = new SqlParameter("@cpri", SqlDbType.Int);
                arParams[2].Value = cpri;
                arParams[3] = new SqlParameter("@cestra", SqlDbType.Int);
                arParams[3].Value = cestra;
                arParams[4] = new SqlParameter("@claspry", SqlDbType.Int);
                arParams[4].Value = claspry;
                arParams[5] = new SqlParameter("@csest", SqlDbType.Int);
                arParams[5].Value = csest;
                arParams[6] = new SqlParameter("@nompr", SqlDbType.VarChar);
                arParams[6].Value = nompr;
                arParams[7] = new SqlParameter("@perio", SqlDbType.Int);
                arParams[7].Value = perio;
                arParams[8] = new SqlParameter("@fviab", SqlDbType.DateTime);
                arParams[8].Value = (fviab.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fviab);
                arParams[9] = new SqlParameter("@nreso", SqlDbType.VarChar);
                arParams[9].Value = nreso;
                arParams[10] = new SqlParameter("@freso", SqlDbType.DateTime);
                arParams[10].Value = (freso.Length == 0) ? Convert.DBNull : Convert.ToDateTime(freso);
                arParams[11] = new SqlParameter("@nconv", SqlDbType.VarChar);
                arParams[11].Value = nconv;
                arParams[12] = new SqlParameter("@fconv", SqlDbType.DateTime);
                arParams[12].Value = (fconv.Length == 0) ? Convert.DBNull : Convert.ToDateTime(fconv);
                arParams[13] = new SqlParameter("@leyfu", SqlDbType.VarChar);
                arParams[13].Value = leyfu;
                arParams[14] = new SqlParameter("@ctpro", SqlDbType.Int);
                arParams[14].Value = ctpro;
                arParams[15] = new SqlParameter("@ctobr", SqlDbType.Int);
                arParams[15].Value = ctobr;
                arParams[16] = new SqlParameter("@cmsni", SqlDbType.Int);
                arParams[16].Value = cmsni;
                arParams[17] = new SqlParameter("@csnip", SqlDbType.Int);
                arParams[17].Value = csnip;
                arParams[18] = new SqlParameter("@msnip", SqlDbType.Float);
                arParams[18].Value = msnip;
                arParams[19] = new SqlParameter("@osnip", SqlDbType.VarChar);
                arParams[19].Value = osnip;
                arParams[20] = new SqlParameter("@ccong", SqlDbType.VarChar);
                arParams[20].Value = ccong;
                arParams[21] = new SqlParameter("@npips", SqlDbType.Int);
                arParams[21].Value = npips;
                arParams[22] = new SqlParameter("@cftef", SqlDbType.Int);
                arParams[22].Value = cftef;
                arParams[23] = new SqlParameter("@cmeje", SqlDbType.Int);
                arParams[23].Value = cmeje;
                arParams[24] = new SqlParameter("@eejec", SqlDbType.VarChar);
                arParams[24].Value = eejec;
                arParams[25] = new SqlParameter("@coord", SqlDbType.VarChar);
                arParams[25].Value = coord;
                arParams[26] = new SqlParameter("@nhsni", SqlDbType.Int);
                arParams[26].Value = nhsni;
                arParams[27] = new SqlParameter("@pbdir", SqlDbType.Int);
                arParams[27].Value = pbdir;
                arParams[28] = new SqlParameter("@pbind", SqlDbType.Int);
                arParams[28].Value = pbind;
                arParams[29] = new SqlParameter("@minvi", SqlDbType.Float);
                arParams[29].Value = minvi;
                arParams[30] = new SqlParameter("@minve", SqlDbType.Float);
                arParams[30].Value = minve;
                arParams[31] = new SqlParameter("@mejec", SqlDbType.Float);
                arParams[31].Value = mejec;
                arParams[32] = new SqlParameter("@mppto", SqlDbType.Float);
                arParams[32].Value = mppto;
                arParams[33] = new SqlParameter("@mpim", SqlDbType.Float);
                arParams[33].Value = mpim;
                arParams[34] = new SqlParameter("@mconv", SqlDbType.Float);
                arParams[34].Value = mconv;
                arParams[35] = new SqlParameter("@obpry", SqlDbType.VarChar);
                arParams[35].Value = obpry;
                arParams[36] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
                arParams[36].Value = nom_usu;
                arParams[37] = new SqlParameter("@n_empleo", SqlDbType.Int);
                arParams[37].Value = nempleo;
                arParams[38] = new SqlParameter("@cod_funcion", SqlDbType.VarChar);
                arParams[38].Value = funcion;
                arParams[39] = new SqlParameter("@cod_progcf", SqlDbType.VarChar);
                arParams[39].Value =prog;
                arParams[40] = new SqlParameter("@cod_sub_progcf", SqlDbType.VarChar);
                arParams[40].Value = subprog;
                arParams[41] = new SqlParameter("@cod_actividad", SqlDbType.VarChar);
                arParams[41].Value =actividad;
                arParams[42] = new SqlParameter("@cod_actividadP", SqlDbType.VarChar);
                arParams[42].Value = proyecto;
                arParams[43] = new SqlParameter("@cod_comp", SqlDbType.VarChar);
                arParams[43].Value = compo;
                arParams[44] = new SqlParameter("@id_meta", SqlDbType.Int);
                arParams[44].Value = meta;
                arParams[45] = new SqlParameter("@id_submeta", SqlDbType.VarChar);
                arParams[45].Value = submeta;
                arParams[46] = new SqlParameter("@fuente_finanRO", SqlDbType.Int);
                arParams[46].Value = fuenteRO;
                arParams[47] = new SqlParameter("@fuente_finanFE", SqlDbType.Int);
                arParams[47].Value = fuenteFE;
                arParams[48] = new SqlParameter("@fuente_finanGR", SqlDbType.Int);
                arParams[48].Value = fuenteGR;
                arParams[49] = new SqlParameter("@fuente_finanGM", SqlDbType.Int);
                arParams[49].Value = fuenteGM;
                arParams[50] = new SqlParameter("@fuente_finanPO", SqlDbType.Int);
                arParams[50].Value = fuentePO;
                arParams[51] = new SqlParameter("@nro_conex_agua", SqlDbType.Int);
                arParams[51].Value = nro_conexAgua;
                arParams[52] = new SqlParameter("@nro_conex_alcan", SqlDbType.Int);
                arParams[52].Value = nro_conexAlc;
                arParams[53] = new SqlParameter("@monto_contraGR", SqlDbType.Float);
                arParams[53].Value = mcontraGR;
                arParams[54] = new SqlParameter("@monto_contraGM", SqlDbType.Float);
                arParams[54].Value = mcontraGM;
                arParams[55] = new SqlParameter("@monto_contraPO", SqlDbType.Float);
                arParams[55].Value = mcontraPO;
                arParams[56] = new SqlParameter("@flag_anticrisis", SqlDbType.Int);
                arParams[56].Value =anticrisis;
                arParams[57] = new SqlParameter("@flag_continu", SqlDbType.Int);
                arParams[57].Value = continuidad;
                arParams[58] = new SqlParameter("@nemonico", SqlDbType.VarChar);
                arParams[58].Value = nemonico;
                arParams[59] = new SqlParameter("@fuente_donacion", SqlDbType.Int);
                arParams[59].Value = fuenteDonacion;
                arParams[60] = new SqlParameter("@monto_donacion", SqlDbType.Float);
                arParams[60].Value = mDonancion;
                arParams[61] = new SqlParameter("@flag_inicial", SqlDbType.Int);
                arParams[61].Value = inicial;
                arParams[62] = new SqlParameter("@clasificador", SqlDbType.VarChar);
                arParams[62].Value = clasificador;
                arParams[63] = new SqlParameter("@intervencion", SqlDbType.Int);
                arParams[63].Value = intervencion;
                arParams[64] = new SqlParameter("@tipo_accion", SqlDbType.Int);
                arParams[64].Value = tipo_accion;
                arParams[65] = new SqlParameter("@monto_transf", SqlDbType.Float);
                arParams[65].Value = mtransf;
                arParams[66] = new SqlParameter("@cod_subsector", SqlDbType.Int);
                arParams[66].Value = subsector;
                arParams[67] = new SqlParameter("@nro_transf", SqlDbType.VarChar);
                arParams[67].Value = nrotransf;
                arParams[68] = new SqlParameter("@mexpe_ficha", SqlDbType.Float);
                arParams[68].Value = mexpe_ficha;
                arParams[69] = new SqlParameter("@mobra_ficha", SqlDbType.Float);
                arParams[69].Value = mobra_ficha;
                arParams[70] = new SqlParameter("@msuper_ficha", SqlDbType.Float);
                arParams[70].Value = msuper_ficha;
                arParams[71] = new SqlParameter("@mCIRA_ficha", SqlDbType.Float);
                arParams[71].Value = mCIRA_ficha;
                arParams[72] = new SqlParameter("@acuerdo_expe_ficha", SqlDbType.VarChar);
                arParams[72].Value = acuerdo_expe_ficha;
                arParams[73] = new SqlParameter("@acuerdo_obra_ficha", SqlDbType.VarChar);
                arParams[73].Value = acuerdo_obra_ficha;
                arParams[74] = new SqlParameter("@acuerdo_super_ficha", SqlDbType.VarChar);
                arParams[74].Value = acuerdo_super_ficha;
                arParams[75] = new SqlParameter("@acuerdo_CIRA_ficha", SqlDbType.VarChar);
                arParams[75].Value = acuerdo_CIRA_ficha;
                arParams[76] = new SqlParameter("@cexpe_compro", SqlDbType.Float);
                arParams[76].Value = cexpe_compro;
                arParams[77] = new SqlParameter("@cobra_expe", SqlDbType.Float);
                arParams[77].Value = cobra_expe;
                arParams[78] = new SqlParameter("@csuper_expe", SqlDbType.Float);
                arParams[78].Value = csuper_expe;
                arParams[79] = new SqlParameter("@cCIRA_expe", SqlDbType.Float);
                arParams[79].Value = cCIRA_expe;
                arParams[80] = new SqlParameter("@nro_FORSUR", SqlDbType.VarChar);
                arParams[80].Value = NroFORSUR;

                oCon.Open();
                intVal = SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "spiu_datosProyecto_DataEntry", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return intVal;
        }

        public void sp_cierreProyecto(int cod_programa, DateTime fecha_cierre)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@codPrograma", SqlDbType.Int);
                arParams[0].Value = cod_programa;
                arParams[1] = new SqlParameter("@fechaCierre", SqlDbType.DateTime);
                arParams[1].Value = fecha_cierre;
                oCon.Open();
                SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_cierreProyecto", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }

        public DataTable sp_ListarDepartamento()
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_ListarDepartamento");
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }

        public DataTable sp_ListarProvincia(string cod_depa)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[1];
                arParams[0] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
                arParams[0].Value = cod_depa;
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_ListarProvincia", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }

        public DataTable sp_ListarDistrito(string cod_depa, string cod_prov)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
                arParams[0].Value = cod_depa;
                arParams[1] = new SqlParameter("@cod_prov", SqlDbType.VarChar);
                arParams[1].Value = cod_prov;
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_ListarDistrito", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }

        public DataTable sp_ListarCentroPoblado(string cod_depa, string cod_prov, string cod_dist)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[3];
                arParams[0] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
                arParams[0].Value = cod_depa;
                arParams[1] = new SqlParameter("@cod_prov", SqlDbType.VarChar);
                arParams[1].Value = cod_prov;
                arParams[2] = new SqlParameter("@cod_dist", SqlDbType.VarChar);
                arParams[2].Value = cod_dist;
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_ListarCentroPoblado", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }
        public DataTable sp_ListarUbigeos(int id_proyecto)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;
            DataTable dt = null;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[1];
                arParams[0] = new SqlParameter("@id_proyecto", SqlDbType.Int);
                arParams[0].Value = id_proyecto;
                oCon.Open();
                dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_ListarUbigeos", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return dt;
        }
        //public void spd_Ubigeo(string id_ubigeos, int id_proyecto, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[3];
        //        arParams[0] = new SqlParameter("@id_ubigeos", SqlDbType.VarChar);
        //        arParams[0].Value = id_ubigeos;
        //        arParams[1] = new SqlParameter("@id_proyecto", SqlDbType.Int);
        //        arParams[1].Value = id_proyecto;
        //        arParams[2] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[2].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "spd_Ubigeo", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public void spiu_Ubigeo(int id_ubigeo_proy, int id_proyecto, string cod_depa, string cod_prov, string cod_dist, string cod_ccpp, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[7];
        //        arParams[0] = new SqlParameter("@id_ubigeo_proy", SqlDbType.Int);
        //        arParams[0].Value = id_ubigeo_proy;
        //        arParams[1] = new SqlParameter("@id_proyecto", SqlDbType.Int);
        //        arParams[1].Value = id_proyecto;
        //        arParams[2] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
        //        arParams[2].Value = cod_depa;
        //        arParams[3] = new SqlParameter("@cod_prov", SqlDbType.VarChar);
        //        arParams[3].Value = cod_prov;
        //        arParams[4] = new SqlParameter("@cod_dist", SqlDbType.VarChar);
        //        arParams[4].Value = cod_dist;
        //        arParams[5] = new SqlParameter("@cod_ccpp", SqlDbType.VarChar);
        //        arParams[5].Value = cod_ccpp;
        //        arParams[6] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[6].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "spiu_Ubigeo", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        public object sp_FormatoFechaSeguimiento(int id_proyecto, string tabla, int audit)
        {
            SqlConnection oCon = null;
            object obj = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[3];
                arParams[0] = new SqlParameter("@id_proyecto", SqlDbType.Int);
                arParams[0].Value = id_proyecto;
                arParams[1] = new SqlParameter("@tabla", SqlDbType.VarChar);
                arParams[1].Value = tabla;
                arParams[2] = new SqlParameter("@tipo_audit", SqlDbType.Int);
                arParams[2].Value = audit;
                oCon.Open();
                obj = SqlHelper.ExecuteScalar(oCon, CommandType.StoredProcedure, "sp_FormatoFechaSeguimiento", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
            return obj;
        }
        public int fn_DiferenciaFecha(int id_proyecto, string tabla, int audit)
        {
            string sql ="";
            int dia = 0;
            if (audit == 0)
            {
                sql = "select top 1 datediff(dd, fecha_registro, getdate()) " +
                            "from " + tabla + " where id_proyecto=" + id_proyecto + " order by fecha_registro desc";
                dia = Convert.ToInt32(GetValueFromSQL(sql));
            }
            else
            {
                if (audit == 3)
                {
                    sql = "select isnull(datediff(dd, max(fec_accion), getdate()),0) " +
                                   "from T_AUDIT where id_tabla=" + id_proyecto ;
                }
                else if(audit == 1)
                {
                 sql = "select isnull(datediff(dd, max(fec_accion), getdate()),0) " +
                                "from T_AUDIT where cod_tabla=1 and id_tabla=" + id_proyecto + " and cod_accion=" + audit;
                }
                else
                {
                    sql = "select isnull(datediff(dd, max(fec_accion), getdate()),0) " +
                                "from T_AUDIT where id_tabla=" + id_proyecto + " and cod_accion=" + audit;
                }
                    dia = Convert.ToInt32(GetValueFromSQL(sql));
            }
            return dia;
        }

        //public DataTable sp_buscarProyecto(int id_proy,string flag)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@idproyecto", SqlDbType.Int);
        //        arParams[0].Value = id_proy;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_buscar_proyecto", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
    }
    #endregion

    #region "Clase mFotos"
    public class mFotos
    {
        private acceso objAcceso = new acceso();
        public mFotos()
        {
            //
            // TODO: Add constructor logic here
            //
            //this.objAcceso = new acceso();
        }
        //public DataTable sp_listarFotos(int id_proyecto)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@id_proyecto", SqlDbType.Int);
        //        arParams[0].Value = id_proyecto;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_listarFotos", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public void sp_agregarFoto(int id_foto, int id_proyecto, int cod_tipo, DateTime fecha, string cod_depa, string descripcion, string archivo, string nom_usu)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[8];
        //        arParams[0] = new SqlParameter("@id_foto", SqlDbType.Int);
        //        arParams[0].Value = id_foto;
        //        arParams[1] = new SqlParameter("@id_proyecto", SqlDbType.Int);
        //        arParams[1].Value = id_proyecto;
        //        arParams[2] = new SqlParameter("@cod_tipo", SqlDbType.Int);
        //        arParams[2].Value = cod_tipo;
        //        arParams[3] = new SqlParameter("@fecha", SqlDbType.DateTime);
        //        arParams[3].Value = fecha;
        //        arParams[4] = new SqlParameter("@cod_depa", SqlDbType.VarChar);
        //        arParams[4].Value = cod_depa;
        //        arParams[5] = new SqlParameter("@descripcion", SqlDbType.Text);
        //        arParams[5].Value = descripcion;
        //        arParams[6] = new SqlParameter("@archivo", SqlDbType.VarChar);
        //        arParams[6].Value = archivo;
        //        arParams[7] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
        //        arParams[7].Value = nom_usu;
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_agregarFoto", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public DataTable sp_GetByIdFoto(int id_foto)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        SqlParameter[] arParams = new SqlParameter[1];
        //        arParams[0] = new SqlParameter("@id_foto", SqlDbType.Int);
        //        arParams[0].Value = id_foto;
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_GetByIdFoto", arParams);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public void SetField(int id_foto, string campo, string valor)
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        string sql = String.Format("update T_FOTO set {1} = {2} where id_foto = {0}", id_foto, campo, valor);
        //        oCon.Open();
        //        SqlHelper.ExecuteNonQuery(oCon, CommandType.Text, sql);
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //}
        //public DataTable sp_CargarTipoFoto()
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarTipoFoto");
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        //public DataTable sp_CargarDepa()
        //{
        //    SqlConnection oCon = null;
        //    string strCon = objAcceso.GetStringConection().ConnectionString;
        //    DataTable dt = null;

        //    try
        //    {
        //        oCon = new SqlConnection(strCon);
        //        oCon.Open();
        //        dt = SqlHelper.ExecuteDataTable(oCon, CommandType.StoredProcedure, "sp_CargarDepa");
        //        oCon.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        oCon.Close();
        //    }
        //    finally
        //    {
        //        oCon.Close();
        //    }
        //    return dt;
        //}
        public void sp_ActivaDesactivaFoto(int id_foto, string nom_usu)
        {
            SqlConnection oCon = null;
            string strCon = objAcceso.GetStringConection().ConnectionString;

            try
            {
                oCon = new SqlConnection(strCon);
                SqlParameter[] arParams = new SqlParameter[2];
                arParams[0] = new SqlParameter("@id_foto", SqlDbType.Int);
                arParams[0].Value = id_foto;
                arParams[1] = new SqlParameter("@nom_usu", SqlDbType.VarChar);
                arParams[1].Value = nom_usu;
                oCon.Open();
                SqlHelper.ExecuteNonQuery(oCon, CommandType.StoredProcedure, "sp_ActivaDesactivaFoto", arParams);
                oCon.Close();
            }
            catch (Exception ex)
            {
                oCon.Close();
            }
            finally
            {
                oCon.Close();
            }
        }
        //public string _Metodo.uploadfile(FileUpload UpFile, string OldArchivo, string depa)
        //{
        //    string nom_depa = depa.Replace(" ", "");
        //    return _Metodo.uploadfile("", UpFile, OldArchivo, nom_depa,1);
        //}
        //public string UploadFileFORSUR(FileUpload UpFile, string OldArchivo, string depa)
        //{
        //    string nom_depa = depa.Replace(" ", "");
        //    return _Metodo.uploadfile("", UpFile, OldArchivo, nom_depa,2);
        //}
        //public string _Metodo.uploadfile(string SubDir, FileUpload UpFile, string OldArchivo, string depa,int entidad)
        //{
        //    string arch = Path.GetFileName(OldArchivo);
        //    if (UpFile.HasFile)
        //    {
        //        string pat = ConfigurationManager.AppSettings["PathInside"].ToString();//UploadFilesPath("", depa);
        //        //pat = "//mrojas/fotos/" + depa + "/";
        //        //pat = "//cronos/fotos/" + depa + "/";
        //        //pat = "K:/johnny/johnny/proy/data_entry/ssp_v1.0/fotos";
        //        if(entidad == 1)
        //        pat = pat + "fotos/" + depa + "/";
        //        else
        //        pat = pat + "fotosFORSUR/" + depa + "/";

        //        if (SubDir.Length > 0) { pat += SubDir + "\\"; }

        //        //Eliminar el anterior
        //        if (OldArchivo != null && OldArchivo.Length > 0)
        //        {
        //            if (FileSystem.FileExists(pat + OldArchivo))
        //            {
        //                FileSystem.DeleteFile(pat + OldArchivo);
        //            }
        //        }
        //        arch = Path.GetFileName(UpFile.FileName);

        //        //Calcular el nuevo nombre
        //        if (FileSystem.FileExists(pat + arch))
        //        {
        //            int indice = 1;
        //            string ext = Path.GetExtension(arch);
        //            string nombre = Path.GetFileNameWithoutExtension(arch);
        //            while (true)
        //            {
        //                arch = nombre + indice.ToString() + ext;
        //                if (FileSystem.FileExists(pat + arch))
        //                {
        //                    indice += 1;
        //                }
        //                else
        //                {
        //                    break;
        //                }
        //            }
        //        }
        //        //UpLoad el archivo
        //        arch = arch.Replace(" ", "_");
        //        UpFile.SaveAs(pat + arch);
        //    }
        //    return arch;
        //}
        public string UploadFilesPath(string FileName, string depa)
        {
            string arch = "";
            try
            {
                arch = HttpContext.Current.Server.MapPath("/Intranet/Fotos");
            }
            catch (Exception ex)
            {
                arch = HttpContext.Current.Server.MapPath("~/Fotos/" + depa);
            }
            if (FileName.Length == 0)
            {
                arch = arch + "/";
            }
            else
            {
                arch = Path.Combine(arch, FileName);
            }
            return arch;
        }
        //public void DeleteUploadFile(string archivo, string depa)
        //{
        //    if (!String.IsNullOrEmpty(archivo))
        //    {
        //        string arch = UploadFilesPath(archivo, depa);
        //        if (FileSystem.FileExists(arch))
        //        {
        //            try
        //            {
        //                FileSystem.DeleteFile(arch);
        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //        }
        //    }
        //}
    }
    #endregion
      


   
 
}
