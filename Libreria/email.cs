﻿using System;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace correo
{
    public class email
    {
        public email()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public bool SendMail(string smtpServerKey
                            , string emailFrom
                            , string emailTo
                            , string emailCopy
                            , string emailHideCopy
                            , string emailSubject
                            , string emailBody
                            , MailPriority emailPriority)
        {
            bool res = false;
            // Recuperamos del web config el servidor SSMTP
            //string smtpServer = System.Configuration.ConfigurationSettings.AppSettings[smtpServerKey];


            MailMessage mail2 = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            if (emailCopy != String.Empty)
                mail2.CC.Add(emailCopy);

            if (emailHideCopy != String.Empty)
                mail2.Bcc.Add(emailHideCopy);

            mail2.To.Add(emailTo);

            mail2.Subject = emailSubject;
            mail2.From = new MailAddress(emailFrom);
            mail2.Body = emailBody;
            mail2.IsBodyHtml = true;
            //mail2.Priority = emailPriority;
             
            try
            {
                //smtpMail.Send(mail);
                smtp.Send(mail2);
                res = true;
            }
            catch (SmtpException ex)
            {
                res = false;
            }
            return res;
        }
        public bool SendMail(string smtpServerKey
                            , string emailFrom
                            , string emailTo
                            , string emailCopy
                            , string emailHideCopy
                            , string emailSubject
                            , string emailBody
                            , string sPathFileKey
                            , FileUpload FUpload
                            , MailPriority emailPriority)
        {
            bool res = false;
            string attch = string.Empty;
            string sFile = string.Empty;
            // Recuperamos del web config el servidor SSMTP
            string smtpServer = System.Configuration.ConfigurationSettings.AppSettings[smtpServerKey];

            // Recuperamos el fichero que vamos a enviar por mail
            string sPathFile = System.Configuration.ConfigurationSettings.AppSettings[sPathFileKey];

            if (FUpload.PostedFile.FileName != "")
            {
                sFile = FUpload.PostedFile.FileName;
                sFile = System.IO.Path.GetFileName(sFile);
                FUpload.PostedFile.SaveAs(sPathFile + "\\" + sFile);
                attch = sPathFile + "\\" + sFile;
            }

     
            MailMessage mail2 = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            mail2.To.Add(emailTo);

            if (emailCopy != String.Empty)
                mail2.CC.Add(emailCopy);

            if (emailHideCopy != String.Empty)
                mail2.Bcc.Add(emailHideCopy);

            mail2.Subject = emailSubject;
            mail2.From = new MailAddress(emailFrom);
            mail2.Body = emailBody;
            mail2.IsBodyHtml = true;
            //mail2.Priority = emailPriority;

            if (attch != string.Empty)
            {
                //mail.Attachments.Add(new Attachment(attch));
                mail2.Attachments.Add(new Attachment(attch));
            }
            SmtpClient smtpMail = new SmtpClient(smtpServer);
            //smtpMail.Credentials = new System.Net.NetworkCredential("jtarmeno", "xxxx");
            //smtpMail.Host = smtpServer;
            try
            {
                //smtpMail.Send(mail);
                smtp.Send(mail2);
                res = true;
            }
            catch (SmtpException ex)
            {
                res = false;
            }
            return res;
        }
    }
}
