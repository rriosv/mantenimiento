using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Criptografia
{
  /// http://forums.msdn.microsoft.com/en-US/netfxbcl/thread/08f2044c-d587-44bb-a31b-51c4a106e218/
  /// <summary>
  /// This class is used to provide cryptograph utility functions such as encrypting and decrypting strings, creating
  /// hashes etc.
  /// </summary>
  public sealed class Encriptar
  {
    static string key;
    static string iv;

    #region Operaciones
    /// <summary>
    /// This will generate a key suitable for use with our encrypt method.
    /// </summary>
    /// <returns>A key encoded using Unicode encoding.</returns>
    public static string GenerateKey()
    {
      // Create an instance of our crypto service provider
      TripleDESCryptoServiceProvider csp = new TripleDESCryptoServiceProvider();
      
      // Create a byte array to hold our key
      byte[] key;

      // Create our key
      csp.GenerateKey();
      key = csp.Key;

      // Now return our key as a string
      return EncodeString(key);
    }

    /// <summary>
    /// This will generate an IV (initialization vector) for use with our encrypt method.
    /// </summary>
    /// <returns>An initialization vector encoded using Unicode encoding.</returns>
    public static string GenerateIV()
    {
      // Create an instance of our crypto service provider
      TripleDESCryptoServiceProvider csp = new TripleDESCryptoServiceProvider();

      // Create a byte array to hold our iv
      byte[] iv;

      // Create our iv
      csp.GenerateIV();
      iv = csp.IV;

      // Now return our key as a string
      return EncodeString(iv);
    }

    /// <summary>
    /// This will encrypt the plain text and return it as encrypted text using the given key and IV
    /// (initialization vector)
    /// </summary>
    /// <param name="PlainText">The plain text to encrypt</param>
    /// <param name="Key">The key to use, assumed to be encoded using Unicode encoding</param>
    /// <param name="IV">The IV (initializtion vector) to use, assumed to be encoded using Unicode encoding</param>
    /// <returns>The encrypted version of plain text as a string encoded using Unicode encoding.</returns>
    public string EncriptaCadena(string Cadena)
    {
      key = GenerateKey();
      iv = GenerateIV();
      return EncryptString(Cadena, key, iv);
    }
    public static string EncryptString(string Cadena, string Key, string IV)
    {
      // Create an instance of our crypto service provider
      TripleDESCryptoServiceProvider csp = new TripleDESCryptoServiceProvider();

      // Set our key and iv
      csp.Key = DecodeString(Key);
      csp.IV = DecodeString(IV);

      // Now get our encryptor
      ICryptoTransform encryptor = csp.CreateEncryptor();

      // Create a byte area for our input
      byte[] input = Encoding.Unicode.GetBytes(Cadena);

      // Create a memory stream to use
      MemoryStream output = new MemoryStream();
      // Create a crypto stream
      CryptoStream encryptedOutput = new CryptoStream(output, encryptor, CryptoStreamMode.Write);

      // Now write our data
      encryptedOutput.Write(input, 0, input.Length);

      // Close our stream
      encryptedOutput.Close();

      // Now encode the encrypted bytes and return it
      return EncodeString(output.ToArray());
    }

    /// <summary>
    /// This will decrypt the EncryptedText and return it as plaintext using the the given key and IV (initialization
    /// vector)
    /// </summary>
    /// <param name="EncryptedText">Encrypted text to decrypt assumed to be Unicode encoded.</param>
    /// <param name="Key">The key to use, assumed to be encoded using Unicode encoding</param>
    /// <param name="IV">The IV (initialization vector) to use, assumed to be encoded using Unicode encoding</param>
    /// <returns>The plain text that was encrypted as a string</returns>
    public string DesencriptaCadena(string CadenaEncriptada)
    {
      //string key = GenerateKey();
      //string iv = GenerateIV();
      return DecryptString(CadenaEncriptada, key, iv);
    }

    public static string DecryptString(string CadenaEncriptada, string Key, string IV)
    {
      // Create an instance of our crypto service provider
      TripleDESCryptoServiceProvider csp = new TripleDESCryptoServiceProvider();

      // Set our key and iv
      csp.Key = DecodeString(Key);
      csp.IV = DecodeString(IV);

      // Now get our encryptor
      ICryptoTransform decryptor = csp.CreateDecryptor();

      // Create a memory string for our output
      MemoryStream output = new MemoryStream();
      // Create a crypto stream
      CryptoStream outputDecryptor = new CryptoStream(output, decryptor, CryptoStreamMode.Write);

      // Create a buffer for our EncryptedText as a byte array
      byte[] encryptedData = DecodeString(CadenaEncriptada);

      // Decrypt our output
      outputDecryptor.Write(encryptedData, 0, encryptedData.Length);
      outputDecryptor.Close();

      // Return it as a string
      return EncodeString(output.ToArray());
    }

    /// <summary>
    /// This will hash and base64 a string of text.
    /// </summary>
    /// <param name="TextToHash">Text to hash</param>
    /// <returns>A string representing the hashed value of the input base64 encoded.</returns>
    public static string GenerateBase64Hash(string TextToHash)
    {
      // Create an instance of our crypto service provider
      SHA1CryptoServiceProvider csp = new SHA1CryptoServiceProvider();

      // Now create a byte array for our text to hash
      byte[] toHash = DecodeString(TextToHash);

      // Return our hashed value
      return Convert.ToBase64String(csp.ComputeHash(toHash, 0, toHash.Length));
    }

    /// <summary>
    /// This will take a key in a unicode encoded string and base 64 encode it.
    /// </summary>
    /// <param name="Key">Key to encode assumed to be a unicode string</param>
    /// <returns>A Base64 encoded version of the key</returns>
    public static string EncodeKey(string Key)
    {
      return Convert.ToBase64String(Encoding.Unicode.GetBytes(Key));
    }

    /// <summary>
    /// This will take a key in a Base64 encoded format and return it Unicode encoded.
    /// </summary>
    /// <param name="Key">Key to decode assumed to be a Base64 string</param>
    /// <returns>A Unicode encoded version of the key</returns>
    public static string DecodeKey(string Key)
    {
      return Encoding.Unicode.GetString(Convert.FromBase64String(Key));
    }
    #endregion

    #region Logic
    /// <summary>
    /// This method will decode a string into a byte array using Unicode encoding.
    /// </summary>
    /// <param name="Text">Text to decode into bytes</param>
    /// <returns>A byte array</returns>
    private static byte[] DecodeString(string Text)
    {
      // Return a byte array from the Unicode string
      return Encoding.Unicode.GetBytes(Text);
    }

    /// <summary>
    /// This method will encode a byte array using unicode encoding returning a string.
    /// </summary>
    /// <param name="ByteArray">Bytes to encode</param>
    /// <returns>A Unicode encoded string</returns>
    private static string EncodeString(byte[] ByteArray)
    {
      // Return a Unicode string from the bytes
      return Encoding.Unicode.GetString(ByteArray);
    }
    #endregion
  
  }
}